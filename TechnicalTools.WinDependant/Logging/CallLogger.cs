﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Web;

namespace TechnicalTools.Tools.Logging
{
    public class CallLogger : IDisposable
    {
        public static readonly CallLogger Instance = new CallLogger();
        public event Action<CallLogger, LogContext, LogMemberCall> BeginMethod;
        public event Action<CallLogger, LogContext, LogMemberCall> EndMethod;
        public event Action<CallLogger, LogContext> ContextEnd;
        public event Action<CallLogger, LogContext> CommitLogContext;

        #region Configuration

        public eConfigureMode Mode { get; private set; }

        public enum eConfigureMode
        {
            None = 0,
            Application,
            Server
        }



        public CallLogger(eConfigureMode mode = eConfigureMode.None)
        {
            if (mode == eConfigureMode.Application)
                ConfigureForApplication();
            else if (mode == eConfigureMode.Server)
                ConfigureForServer();
        }

        public void ConfigureForApplication()
        {
            CheckNotUsed();
            Mode = eConfigureMode.Application;
        }
        public void ConfigureForServer()
        {
            CheckNotUsed();
            Debug.Assert(HttpContext.Current != null); // ca va petu etre thrower si on initialize dans global asax. Si c'est le cas, supprimer cette ligne (il faudrait tester en realite qu'il existe une HttpApplication courante
            Mode = eConfigureMode.Server;
        }
        void CheckNotUsed()
        {
            // On interdit parce qu'il faudrait nettoyer tout ce qui est en cours et que c'est un peu compliqué a faire et qu'on en a pas besoin pour le moment
            if (LogCount > 0)
                throw new Exception("Cannot configure CallLogger instance after it was used. Create another instance or configure it before first use.");
        }

        #endregion Configuration

        #region IDisposable


        public void Dispose(bool disposing)
        {
            if(!_disposed)
            {
                if (disposing)
                {
                    // Free other managed objects.
                }
                // Free unmanaged objects here.
                // Set large fields to null.
                _disposed = true;
            }
        }
        bool _disposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        ~CallLogger()
        {
            Dispose(false);
        }

        #endregion IDisposable


        [DebuggerHidden][DebuggerStepThrough]
        public void LogCall(Action action, [CallerMemberName] string methodName = "")
        {
            _LogMemberCall(null, () => { action(); return false; }, false, methodName);
        }
        //public async Task LogCall(Task task, [CallerMemberName] string methodName = "")
        //{
        //    _LogMemberCall(null, () => { await Taskaction(); return false; }, false, methodName);
        //}
        [DebuggerHidden][DebuggerStepThrough]
        public void LogCall(IEnumerable<KeyValuePair<string, object>> args, Action action, [CallerMemberName] string methodName = "")
        {
            _LogMemberCall(args, () => { action(); return false; }, false, methodName);
        }
        [DebuggerHidden][DebuggerStepThrough]
        public T LogCall<T>(IEnumerable<KeyValuePair<string, object>> args, Func<T> action, [CallerMemberName] string methodName = "")
        {
            return _LogMemberCall(args, action, true, methodName);
        }
        [DebuggerHidden][DebuggerStepThrough]
        public T LogCall<T>(Func<T> action, [CallerMemberName] string methodName = "")
        {
            return _LogMemberCall(null, action, true, methodName);
        }
        [DebuggerHidden][DebuggerStepThrough]
        private T _LogMemberCall<T>(IEnumerable<KeyValuePair<string, object>> args, Func<T> action, bool isFunction, [CallerMemberName] string methodName = "")
        {
            Debug.Assert(Mode != eConfigureMode.None, "Non configuré, appelez ConfigureForServer ou ConfigureForApplication avant la première utilisation");
            ++LogCount;
            Exception ex = null;
            T result = default(T);

            var ctx = GetCurrentContext();
            LogMemberCall log = LogMemberCall.Create();
            try
            {
                log.IsFunction = isFunction;
                log.MemberName = methodName;
                log.Args = args;
                if (ctx.CallStack.Count > 0)
                    ctx.CallStack.Peek().Calls.Add(log);
                else
                    ctx.RootCall = log;
                ctx.CallStack.Push(log);

                log.StartTime = DateTime.UtcNow; // cf https://blogs.msdn.microsoft.com/kirillosenkov/2012/01/10/datetime-utcnow-is-generally-preferable-to-datetime-now/
                if (BeginMethod != null)
                    try { BeginMethod(this, ctx, log); } catch { /* ignoré */}
                result = action();
                ctx.LastException = null;
                return result;
            }
            catch (Exception e)
            {
                ex = e;
                if (ctx.LastException == ex)
                    ex = SameAsBefore;
                else
                    ctx.LastException = ex;
                throw;
            }
            finally
            {
                log.EndTime = DateTime.UtcNow;

                if (ex == null)
                    log.Result = result;
                else
                {
                    log.Exception = ex;
                    if (ex != SameAsBefore)
                        ++ctx.ErrorCount;
                }

                if (EndMethod != null)
                    try { EndMethod(this, ctx, log); } catch { /* ignoré */}

                ctx.CallStack.Pop();
                if (ctx.CallStack.Count == 0)
                {
                    if (ContextEnd != null)
                        try { ContextEnd(this, ctx); } catch { /* ignoré */}
                    OnCommitLogContext(ctx);
                }
            }
        }
        public static readonly Exception SameAsBefore = new Exception("Same Exception");

        public int LogCount { get; private set; }

        [DebuggerHidden][DebuggerStepThrough]
        LogContext GetCurrentContext(bool doNotCreate = false)
        {
            LogContext ctx;
            if (Mode == eConfigureMode.Server)
            {
                Debug.Assert(HttpContext.Current.Items != null);
                ctx = (LogContext)HttpContext.Current.Items[LogContextKey];

                if (ctx != null)
                    return ctx;
                if (doNotCreate)
                    return null;
                ctx = LogContext.Create();
                HttpContext.Current.Items[LogContextKey] = ctx;
                var requestId = HttpContext.Current.Items[RequestIdKey];
                if (requestId == null)
                {
                    requestId = Interlocked.Increment(ref RequestIdCounter);
                    HttpContext.Current.Items[RequestIdKey] = requestId;
                }
                ctx.ThreadId = (int)requestId;
                // InstallHandlerForRequestTermination(ctx);
            }
            else
            {
                if (doNotCreate || _contextByThread.Value != null)
                    return _contextByThread.Value;
                ctx = LogContext.Create();
                ctx.ThreadId = Thread.CurrentThread.ManagedThreadId;
                _contextByThread.Value = ctx;
                //InstallHandlerForThreadTermination(ctx);
            }

            ctx.IncrementReferenceCounter(this);
            return ctx;
        }
        readonly ThreadLocal<LogContext> _contextByThread = new ThreadLocal<LogContext>();
        const string LogContextKey = "LogContextKey";
        const string RequestIdKey = "LogContextRequestId";
        static int RequestIdCounter;
        [DebuggerHidden][DebuggerStepThrough]
        void DeleteCurrentContext()
        {
            var ctx = GetCurrentContext(true);
            if (ctx == null)
                return; // deja fait

            if (Mode == eConfigureMode.Server)
                HttpContext.Current.Items[LogContextKey] = null;
            else if (_contextByThread.IsValueCreated)
                _contextByThread.Value = null;
        }

        #region Fin de Request / Thread

        // Installe un handler pour nettoyer automatiquement la requete du client a été servi
        //void InstallHandlerForRequestTermination(LogContext ctx)
        //{
        //    var curContext = HttpContext.Current;

        //    // Installe un handler pour nettoyer automatiquement le log.
        //    EventHandler onRequestEnd = null;
        //    onRequestEnd = (_, __) =>
        //    {
        //        if (curContext != HttpContext.Current)
        //            return; // une autre requête se termine qui ne nous intéresse pas
        //        HttpContext.Current.ApplicationInstance.EndRequest -= onRequestEnd;
        //        var ctxNow = GetCurrentContext(true); // Check if Context still exists
        //        if (ctxNow != null)
        //            OnCommitLogContext(ctxNow);
        //    };
        //    HttpContext.Current.ApplicationInstance.EndRequest += onRequestEnd;
        //}

        // Installe un handler pour nettoyer automatiquement le log quand le thread quitte.
        // Note : le développeur peut aussi forcer le nettoyage en appelant ThreadCommitLog
        //void InstallHandlerForThreadTermination(LogContext ctx)
        //{
        //    _threadAction.Value = () =>
        //    {
        //        if (_disposed) // trop tard ! On ne peut plus logger ! L'application est probablement en train de se terminer
        //            return;
        //        DeleteCurrentContext();
        //        OnCommitLogContext(ctx);
        //        ctx.DecrementReferenceCounter(this);
        //    };
        //}
        //readonly ThreadLocal<Action> _threadAction = new ThreadLocal<Action>();

        // A Appeler à la fin d'un thread si on veut s'assurer que les logs sont envoyés rapidement.
        //public void ThreadCommitLog()
        //{
        //    if (Mode != eConfigureMode.Application)
        //        throw new Exception("Current Logger mode is incompatible with this feature! You must configure logger for Application.");
        //    var ctx = GetCurrentContext(true);
        //    if (ctx == null)
        //        return; // rien à faire
        //    if (ctx.OwnerThreadId != Thread.CurrentThread.ManagedThreadId)
        //        throw new Exception("The calling thread cannot delete this log context because a different thread owns it!");
        //    if (ctx.CallStack.Count > 0)
        //        throw new Exception("Log is currently in progress in this thread, you cannot commit right now!");
        //    OnCommitLogContext(ctx);
        //}
        [DebuggerHidden][DebuggerStepThrough]
        void OnCommitLogContext(LogContext ctx)
        {
            DeleteCurrentContext();
            // TODO : Checker si l'application se termine... Dans ce cas c'est plus delicat à gérer
            // Il est possible de ne pas logger les logs pour eviter un crash qui parraitrai etrange et inquietant
            // if (Application.Shutdown) { return;} // nécessite une dépendance winform, je ne connais pas l'equivalent pour un webservice
            if (CommitLogContext != null)
                try { CommitLogContext(this, ctx); } catch { /* ignoré */ }
            ctx.DecrementReferenceCounter(this);
        }

        #endregion

    }
}
