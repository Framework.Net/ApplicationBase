﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Xml.Linq;

namespace TechnicalTools.Tools.Logging
{
    public class LogContext
    {
        public int OwnerCount { get; private set; }

        public LogMemberCall RootCall { get; internal set; }
        public readonly Stack<LogMemberCall> CallStack = new Stack<LogMemberCall>();
        public int ErrorCount { get; internal set; }
        public int ThreadId { get; internal set; }
        internal Exception LastException { get; set; }


        // Detect thread termination and auto commit log ? => Not Now
        //internal Action OnFinalization;
        //~LogContext()
        //{
        //    if (OnFinalization != null)
        //    {
        //        Debug.Assert(OwnerCount == 1, "Only 1 because CallLogger had a reference to it and is not able to know when the owning thread is terminated !");
        //        OnFinalization();
        //    }
        //}

        private LogContext() { }

        public void IncrementReferenceCounter(object owner)
        {
            ++OwnerCount;
        }
        public void DecrementReferenceCounter(object owner)
        {
            if (--OwnerCount == 0)
            {
                //ctx.OnFinalization = null;
                _logContextRepository.Push(this);
            }
        }

        static readonly ConcurrentStack<LogContext> _logContextRepository = new ConcurrentStack<LogContext>();
        internal static LogContext Create()
        {
            if (_logContextRepository.TryPop(out LogContext ctx))
                ctx.Recycle();
            else
                ctx = new LogContext();
            return ctx;
        }

        void Recycle()
        {
            foreach (var ctx in CallStack)
                LogMemberCall.Dispose(ctx);
            Clear();
        }
        void Clear()
        {
            RootCall = null;
            ErrorCount = 0;
            CallStack.Clear();
            ThreadId = 0;
            LastException = null;
        }



        public XElement ToXml()
        {
            return defaultSerializer.SerializeToXML(this);
        }
        static readonly LogXmlSerializer defaultSerializer = new LogXmlSerializer();
        public override string ToString()
        {
            return ToXml().ToString();
        }

    }
}
