﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;


namespace TechnicalTools.Tools.Logging
{
    public abstract class LogMemberCallBase
    {
        public string MemberName { get; set; }
        public IEnumerable<KeyValuePair<string, object>> Args;
        public bool? IsFunction { get; set; }
        public object Result { get; set; }
        public Exception Exception { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        public readonly List<LogMemberCallBase> Calls = new List<LogMemberCallBase>();

        protected LogMemberCallBase()
        {
        }

        public abstract void Dispose();

        protected void Recycle()
        {
            foreach (var log in Calls)
                log.Dispose();
            Clear();
        }

        protected virtual void Clear()
        {
            MemberName = null;
            Args = null;
            IsFunction = null;
            Result = null;
            Exception = null;
            StartTime = DateTime.MinValue;
            EndTime = null;
            Calls.Clear();
        }
    }
}
