﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;


namespace TechnicalTools.Tools.Logging
{
    public class LogMemberCall
    {
        public string MemberName { get; set; }
        public IEnumerable<KeyValuePair<string, object>> Args;
        public bool? IsFunction { get; set; }
        public object Result { get; set; }
        public Exception Exception { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        public readonly List<LogMemberCall> Calls = new List<LogMemberCall>();

        private LogMemberCall()
        {
        }

        static readonly ConcurrentStack<LogMemberCall> _instances = new ConcurrentStack<LogMemberCall>();
        internal static LogMemberCall Create()
        {
            if (_instances.TryPop(out LogMemberCall log))
            {
                log.Recycle();
                return log;
            }
            return log = new LogMemberCall();
        }
        internal static void Dispose(LogMemberCall log)
        {
            _instances.Push(log);
        }

        void Recycle()
        {
            foreach (var log in Calls)
                Dispose(log);
            Clear();
        }
        void Clear()
        {
            MemberName = null;
            Args = null;
            IsFunction = null;
            Result = null;
            Exception = null;
            StartTime = DateTime.MinValue;
            EndTime = null;
            Calls.Clear();

        }
    }
}
