﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;


namespace TechnicalTools
{
    public static class Image_Extensions
    {
        // From https://stackoverflow.com/a/26178389
        public static Bitmap[] SplitGIFFrames(this Bitmap animation)
        {
            // Get the number of animation frames to copy into a Bitmap array
            int length = animation.GetFrameCount(FrameDimension.Time);

            // Allocate a Bitmap array to hold individual frames from the animation
            Bitmap[] frames = new Bitmap[length];

            // Copy the animation Bitmap frames into the Bitmap array
            for (int index = 0; index < length; index++)
            {
                // Set the current frame within the animation to be copied into the Bitmap array element
                animation.SelectActiveFrame(FrameDimension.Time, index);

                // Create a new Bitmap element within the Bitmap array in which to copy the next frame
                frames[index] = new Bitmap(animation.Size.Width, animation.Size.Height);

                // Copy the current animation frame into the new Bitmap array element
                Graphics.FromImage(frames[index]).DrawImage(animation, new Point(0, 0));
            }

            // Return the array of Bitmap frames
            return frames;
        }

        public static Image GetResizedImage(this Image imgSrc, int? new_width, int? new_height)
        {
            if (!new_width.HasValue && !new_height.HasValue)
                throw new ArgumentException("new_width & new_height");

            if (!new_height.HasValue)
                new_height = imgSrc.Height * new_width / imgSrc.Width;
            if (!new_width.HasValue)
                new_width = imgSrc.Width * new_height / imgSrc.Height;
            Bitmap resizedImage = new Bitmap(new_width.Value, new_height.Value);
            using (Graphics gr = Graphics.FromImage(resizedImage))
            {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(imgSrc, new Rectangle(0, 0, new_width.Value, new_height.Value));
            }
            return resizedImage;
        }
    }
}
