﻿using System;
using System.Linq;

using System.Reflection;

using Microsoft.VisualBasic.FileIO;

namespace TechnicalTools.WinDependent
{
    public class Win32Garbage
    {
        /// <summary>
        /// This version of DeleteFile accepts to not display any dialog box (even for error dialog) 
        /// contrary to public API of VisualBasic FileSystem
        /// </summary>
        public static void DeleteFile(string file)
        {
            DeleteFileInternal.InvokeAndCleanException(null,
                new object[] { file,
                                noUI, // this is why we use reflection
                                RecycleOption.SendToRecycleBin,
                                UICancelOption.ThrowException });
        }
        static readonly MethodInfo DeleteFileInternal = typeof(FileSystem).GetMethod("DeleteFileInternal", BindingFlags.Static | BindingFlags.NonPublic);

        /// <summary>
        /// This version of DeleteDirectory accepts to not display any dialog box (even for error dialog) 
        /// contrary to public API of VisualBasic FileSystem
        /// </summary>
        public static void DeleteDirectory(string directory)
        {
            DeleteDirectoryInternal.InvokeAndCleanException(null, 
                new object[] { directory,
                                DeleteDirectoryOption.DeleteAllContents,
                                noUI, // this is why we use reflection
                                RecycleOption.SendToRecycleBin,
                                UICancelOption.ThrowException });
        }
        static readonly MethodInfo DeleteDirectoryInternal = typeof(FileSystem).GetMethod("DeleteDirectoryInternal", BindingFlags.Static | BindingFlags.NonPublic);

        static readonly object noUI =
            typeof(FileSystem).Assembly
            .GetType(typeof(FileSystem).FullName + "+" + "UIOptionInternal")
            .GetEnumValues().Cast<object>()
            .Single(v => v.ToString() == "NoUI");


        //using System.Diagnostics;
        //using System.Runtime.InteropServices;

        //// IN case we need to do internal stuff ourself
        //// Crash app when used on file on network drive
        //// According to this conversation we should remove Pack : https://stackoverflow.com/a/17648641
        //[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto, Pack = 1)]

        //public struct SHFILEOPSTRUCT
        //{
        //    public IntPtr hwnd;
        //    [MarshalAs(UnmanagedType.U4)]
        //    public int wFunc;
        //    public string pFrom;
        //    public string pTo;
        //    public short fFlags;
        //    [MarshalAs(UnmanagedType.Bool)]
        //    public bool fAnyOperationsAborted;
        //    public IntPtr hNameMappings;
        //    public string lpszProgressTitle;
        //}

        //[DllImport("shell32.dll", CharSet = CharSet.Auto)]
        //static extern int SHFileOperation(ref SHFILEOPSTRUCT FileOp);
        //const int FO_DELETE = 3;
        //const int FOF_ALLOWUNDO = 0x40;
        //const int FOF_NOCONFIRMATION = 0x10; //Don't prompt the user.;

        //public static void DeleteFilesOrFolderToRecycleBin(string filename)
        //{
        //    SHFILEOPSTRUCT shf = new SHFILEOPSTRUCT();
        //    shf.wFunc = FO_DELETE;
        //    shf.fFlags = FOF_ALLOWUNDO | FOF_NOCONFIRMATION;
        //    shf.pFrom = filename + "\0";

        //    int result = SHFileOperation(ref shf);

        //    if (result != 0)
        //        Console.WriteLine(string.Format("error: {0} while moving file {1} to recycle bin", result, filename));
        //}

        //public static void RestoreFilesOrFolderToRecycleBin(string filename)
        //{
        //    if (Debugger.IsAttached)
        //        Debugger.Break();
        //    throw new NotImplementedException();

        //// TODO : Voir l'article suivant pour ajouter la restoration
        //// https://stackoverflow.com/questions/6025311/how-to-restore-files-from-recycle-bin
        // /*
        //    using System;
        //    using System.Collections;
        //    using System.Windows.Forms;
        //    using System.IO;
        //    using Shell32; //Reference Microsoft Shell Controls And Automation on the COM tab.
        //    using System.Runtime.InteropServices;
        //    using Microsoft.VisualBasic.FileIO;
        //    namespace RecyclerCS
        //    {
        //      public partial class Form1 : Form
        //      {
        //        public Form1() {
        //          InitializeComponent();
        //        }
        //        private Shell Shl;
        //        private const long ssfBITBUCKET = 10;
        //        private const int recycleNAME = 0;
        //        private const int recyclePATH = 1;

        //        private void button1_Click(object sender, System.EventArgs e) {
        //          string S = "This is text in the file to be restored from the Recycle Bin.";
        //          string FileName = "C:\\Temp\\Text.txt";
        //          File.WriteAllText(FileName, S);
        //          Delete(FileName);
        //          MessageBox.Show(FileName + " has been moved to the Recycle Bin.");
        //          if (Restore(FileName))
        //            MessageBox.Show(FileName + " has been restored");
        //          else
        //            MessageBox.Show("Error");
        //          Marshal.FinalReleaseComObject(Shl);
        //        }
        //        private void Delete(string Item) {
        //          FileSystem.DeleteFile(Item, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
        //          //Gives the most control of dialogs.
        //        }
        //        private bool Restore(string Item) {
        //          Shl = new Shell();
        //          Folder Recycler = Shl.NameSpace(10);
        //          for (int i = 0; i < Recycler.Items().Count; i++) {
        //            FolderItem FI = Recycler.Items().Item(i);
        //            string FileName = Recycler.GetDetailsOf(FI, 0);
        //            if (Path.GetExtension(FileName) == "") FileName += Path.GetExtension(FI.Path);
        //            //Necessary for systems with hidden file extensions.
        //            string FilePath = Recycler.GetDetailsOf(FI, 1);
        //            if (Item == Path.Combine(FilePath, FileName)) {
        //              DoVerb(FI, "ESTORE");
        //              return true;
        //            }
        //          }
        //          return false;
        //        }
        //        private bool DoVerb(FolderItem Item, string Verb) {
        //          foreach (FolderItemVerb FIVerb in Item.Verbs()) {
        //            if (FIVerb.Name.ToUpper().Contains(Verb.ToUpper())) {
        //              FIVerb.DoIt();
        //              return true;
        //            }
        //          }
        //          return false;
        //        }
        //      }
        //    }
        // */
        //}
    }
}
