﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace My.Tools
{
    /// <summary>
    /// Gestion du presse papier, s'abonner à l'évènement ClipboardChange :)
    /// </summary>
    public class ClipboardWatcher
    {
        [DllImport("User32.dll")]
        protected static extern int
                  SetClipboardViewer(int hWndNewViewer);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool
               ChangeClipboardChain(IntPtr hWndRemove,
                                    IntPtr hWndNewNext);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hwnd, int wMsg,
                                             IntPtr wParam,
                                             IntPtr lParam);

        Form _parent = null;
        IntPtr nextClipboardViewer = IntPtr.Zero;

        public delegate void ClipboardChangeHandler(object sender, string text);
        public event ClipboardChangeHandler ClipboardChanged;

        /// <summary>
        /// </summary>
        /// <param name="handle">Needs Form's handle</param>
        public ClipboardWatcher(Form parent)
        {
            _parent = parent;
            nextClipboardViewer = (IntPtr)SetClipboardViewer((int)_parent.Handle);
        }

        public void Dispose(bool disposing)
        {
            if (disposing && (_parent != null))
            {
                ChangeClipboardChain(_parent.Handle, nextClipboardViewer);
                nextClipboardViewer = IntPtr.Zero;
                _parent = null;
            }
        }

        [DebuggerStepThrough]
        public bool WndProc(ref System.Windows.Forms.Message m)
        {
            // defined in winuser.h
            const int WM_DRAWCLIPBOARD = 0x308;
            const int WM_CHANGECBCHAIN = 0x030D;

            switch (m.Msg)
            {
                case WM_DRAWCLIPBOARD:

                    try
                    {
                        IDataObject iData = new DataObject();
                        iData = Clipboard.GetDataObject();
                        if (iData.GetDataPresent(DataFormats.Text))
                            if (ClipboardChanged != null)
                            {
                                string data = (string)iData.GetData(DataFormats.Text);
                                if (data != LastCopiedText)
                                    ClipboardChanged(this, data);
                            }

                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.ToString());
                    }


                    SendMessage(nextClipboardViewer, m.Msg, m.WParam,
                                m.LParam);
                    break;

                case WM_CHANGECBCHAIN:
                    if (m.WParam == nextClipboardViewer)
                        nextClipboardViewer = m.LParam;
                    else
                        SendMessage(nextClipboardViewer, m.Msg, m.WParam,
                                    m.LParam);
                    break;

                default:
                    return false;
            }
            return true;
        }

        public static string LastCopiedText { get; private set; }
        public static bool SetInClipboard(string text)
        {
            try
            {
                LastCopiedText = text;
                Clipboard.SetText(text, TextDataFormat.Text);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
