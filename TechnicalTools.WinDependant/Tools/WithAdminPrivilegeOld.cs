﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

using TechnicalTools.Diagnostics;


namespace TechnicalTools.WinDependant
{
    public sealed class AdminPrivilegeObject
    {
        public static readonly AdminPrivilegeObject Instance = new AdminPrivilegeObject();

        /// <summary>
        /// Cette méthode est à appeler en tout premier lieu dans le Main du programme.
        /// Elle verifie si le programme a ete rapppele depuis cette classe afin de permettre des modifications 
        /// nécessitant des privileges administrateurs.
        /// Si c'est le cas, une fois que la commande a ete executée, l'application est fermée.
        /// </summary>
        public bool LookForCommandToRun(string[] args)
        {
            LookForCommandToRunIsSetupInMain = true;
            return DetectAndRunCommand(args);
        }

        /// <summary>
        /// Permet de rappeler le programme afin d'executer une méthode statique avec les privilèges administrateurs.
        /// Les arguments seront passés à cette méthode. La methode peut donc prendre des arguments de type string et/ou (params) string[]
        /// </summary>
        /// <param name="met_info"></param>
        /// <param name="args"></param>
        /// <returns>Le code de retour de la fonction</returns>
        public int RunWithAdminPrivilege(MethodInfo met_info, params string[] args)
        {
            Debug.Assert(met_info != null);
            Debug.Assert(met_info.IsStatic, "The Method must be static in order to run it with admin privileges");

            CheckLookForCommandToRunIsSetupInMain();
            Process target = new Process();
            target.StartInfo = new ProcessStartInfo();
            target.StartInfo.FileName = Application.ExecutablePath;
            target.StartInfo.WorkingDirectory = Application.StartupPath;
            target.StartInfo.UseShellExecute = true; // Required for UAC to work
            target.StartInfo.Verb = "runas";         // Required for UAC to work

            // target.StartInfo.Arguments etant limité a 2003 caractère pour Window XP
            // et environ 32468 caracteres pour Windows Seven/Vista et superieur,
            // on transmet les arguments via un pipe anonyme.
            using (var pipeServer = new NamedPipeServerStream(Path.GetFileName(Application.ExecutablePath), PipeDirection.Out))
            {
                target.StartInfo.Arguments = "--runas"
                                           + " type=\"" + met_info.DeclaringType.AssemblyQualifiedName + "\""
                                           + " method=" + met_info.Name;
                target.Start();
                
                pipeServer.WaitForConnection();
                try
                {
                    using (StreamWriter sw = new StreamWriter(pipeServer))
                    {
                        sw.AutoFlush = true;
                        sw.WriteLine("SYNC");
                        pipeServer.WaitForPipeDrain();

                        string arguments;
                        using (MemoryStream s = new MemoryStream())
                        {
                            new BinaryFormatter().Serialize(s, args);
                            arguments = Convert.ToBase64String(s.ToArray());
                        }
                        sw.WriteLine(arguments);
                        pipeServer.WaitForPipeDrain();
                    }
                }
                // Catch the IOException that is raised if the pipe is broken or disconnected. 
                catch (IOException)
                {
                    DebugTools.Break();
                    throw;
                }
            }

            target.WaitForExit();
            int exit_code = target.ExitCode;
            target.Close();
            return exit_code;
        }


        #region Private part

        private AdminPrivilegeObject()
        {
        }



        private bool LookForCommandToRunIsSetupInMain;
        private void CheckLookForCommandToRunIsSetupInMain()
        {
            if (!LookForCommandToRunIsSetupInMain)
                throw new Exception(this.GetType().FullName + ".LookForCommandToRun(args) must be called from Main method !" + global::System.Environment.NewLine +
                                    "See LookForCommandToRun doc for more information");
        }


        private bool DetectAndRunCommand(string[] args)
        {
            for (int s = 0; s < args.Length; ++s)
                if (args[s] == "--runas") // On peut rencontrer d'autres arguments avant de tomber sur le notre
                {
                    string full_type_name = args[s + 1].Replace("type=", "").Replace("\"", "");
                    string method_name = args[s + 2].Replace("method=", "");

                    string base64_arguments;
                    using (var pipeClient = new NamedPipeClientStream(".", Path.GetFileName(Application.ExecutablePath), PipeDirection.In))
                    {
                        pipeClient.Connect();
                        using (StreamReader sr = new StreamReader(pipeClient))
                        {
                            string temp;
                            do
                            {
                                temp = sr.ReadLine();
                            }
                            while (temp != "SYNC");
                            base64_arguments = sr.ReadLine();
                        }
                    }
                    var bytes = Convert.FromBase64String(base64_arguments);

                    string[] arguments;
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        arguments = (string[])new BinaryFormatter().Deserialize(ms);
                    }
                    
                    //var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();

                    MethodInfo method = Type.GetType(full_type_name).GetMethod(method_name, 
                                                                               BindingFlags.NonPublic | 
                                                                               BindingFlags.Public | 
                                                                               BindingFlags.Static);

                    var invoking_args = new List<object>();
                    foreach (ParameterInfo pi in method.GetParameters())
                        if (pi.ParameterType == typeof(string))
                        {
                            Debug.Assert(arguments.Length > 0);
                            invoking_args.Add(arguments.First());
                            arguments = arguments.Skip(1).ToArray();
                        }
                        else if (pi.ParameterType == typeof(string[]))
                        {
                            invoking_args.Add(arguments);
                            arguments = null;
                        }

                    try
                    {
                        method.Invoke(null, invoking_args.ToArray());
                        global::System.Environment.Exit(0);
                    }
                    catch
                    {
                        global::System.Environment.Exit(-1);
                    }
                    return true;
                }
            return false;
        }

        #endregion
    }
}
