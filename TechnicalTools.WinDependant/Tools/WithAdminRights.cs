﻿using System;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;


namespace TechnicalTools.WinDependent
{
    /// <summary>
    /// To execute something with admin right:
    /// </summary>
    /// <example>
    /// <code>
    /// public sealed class InstallSomething : WithAdminPrivilege.BaseTraits
    /// {
    ///     public override void DoWithAdminRight(string request)
    ///     {
    ///         // Whatever you want with admin rights
    ///     }
    /// }
    /// 
    /// public class Program
    /// {
    ///     public void Main(string[] args)
    ///     {
    ///         if (WithAdminPrivilege.RunAsRequested(args))
    ///             return 0;
    ///         // Your normal program flow
    ///         // and then somewhere (a click handler)
    ///         WithAdminPrivilege.Run&lt;InstallSomething&gt;("some data"):
    ///     }
    /// }
    /// </code>
    /// </example>
    public sealed class WithAdminRights
    {
        public abstract class BaseTraits
        {
            protected internal abstract void DoWithAdminRight(string request);
        }

        /// <inheritdoc cref="WithAdminRights"/>
        public static bool Run<TBaseTraits>(string request = null)
            where TBaseTraits : BaseTraits, new()
        {
            if (!ProgramHasLookedForRequestInMain)
                throw new Exception($"You program must call {nameof(ExecuteRequestFoundIn)} from your Main method with Main's args!");

            request = request ?? string.Empty;
            Process target = new Process();
            target.StartInfo = new ProcessStartInfo();
            target.StartInfo.FileName = Application.ExecutablePath;
            target.StartInfo.WorkingDirectory = Application.StartupPath;
            target.StartInfo.UseShellExecute = true; // Required for UAC to work
            target.StartInfo.Verb = "runas";         // Required for UAC to work

            var data = typeof(TBaseTraits).AssemblyQualifiedName + "|" + request;
            data = Convert.ToBase64String(Encoding.UTF8.GetBytes(data));
            target.StartInfo.Arguments = "--runAs --adminRequest=" + data;

            // Limit  is 32468 caractères for Windows Seven/Vista or superior
            // (and 2003 characters for windows XP but we don't handle it here)
            if (target.StartInfo.Arguments.Length > 32468)
                throw new ArgumentException($"Request too long by {target.StartInfo.Arguments.Length - 32468} characters!");

            target.Start();

            target.WaitForExit();
            int exit_code = target.ExitCode;
            target.Close();
            return exit_code == 0;
        }

        static bool ProgramHasLookedForRequestInMain;

        /// <summary>
        /// Cette méthode est à appeler en tout premier lieu dans le Main du programme.
        /// Elle vérifie si le programme a été rappelé depuis cette classe afin de permettre des modifications 
        /// nécessitant des privileges administrateurs.
        /// Si c'est le cas, une fois que la commande a ete exécutée, l'application est fermée.
        /// </summary>
        /// <example><code>
        /// bool? doneWithAdmin = WithAdminRights.ExecuteRequestFoundIn(args);
        /// if (doneWithAdmin.HasValue)
        /// return doneWithAdmin.Value? 0 : 1;
        /// </code></example>
        public static bool? ExecuteRequestFoundIn(string[] args)
        {
            ProgramHasLookedForRequestInMain = true;

            for (int s = 0; s < args.Length - 1; ++s)
                if (args[s] == "--runAs" && (args[s + 1]?.StartsWith("--adminRequest=") ?? false))
                {
                    try
                    {
                        var tmp = args[s + 1].Substring("--adminRequest=".Length);
                        tmp = Encoding.UTF8.GetString(Convert.FromBase64String(tmp));
                        var index = tmp.IndexOf('|');
                        var type = Type.GetType(tmp.Remove(index));
                        var trait = (BaseTraits)Activator.CreateInstance(type); // TODO : use GetDefaultConstructorOptimized() later
                        var bytes = Convert.FromBase64String(tmp.Substring(index + 1));
                        var request = Encoding.UTF8.GetString(bytes);
                        trait.DoWithAdminRight(request);
                        return true;
                    }
                    catch
                    {
                        return false;
                    }
                }
            return null;
        }
    }
}
