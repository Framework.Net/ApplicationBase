﻿using System;
using System.Linq;
using System.Management;

namespace TechnicalTools.Tools
{
    public static class OsInfo
    {
        public enum eWindowsVersion
        {
            Unknown,
            WindowsServer2008R2,
            WindowsServer2008R2Standard,
        }

        public static eWindowsVersion GetOSInfo()
        {
            var name = (from x in new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem").Get().Cast<ManagementObject>()
                        select x.GetPropertyValue("Caption")).FirstOrDefault();
            if (name == null)
                return eWindowsVersion.Unknown;

            if (name.ToString().StartsWith("Microsoft Windows Server 2008 R2 Standard"))
                return eWindowsVersion.WindowsServer2008R2Standard;

            return eWindowsVersion.Unknown;
        }
    }
}
