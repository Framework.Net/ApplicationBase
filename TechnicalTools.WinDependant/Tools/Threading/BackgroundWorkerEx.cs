﻿using System;


namespace TechnicalTools.Tools.Threading
{
    /// <summary>
    /// See base class
    /// </summary>
    public class BackgroundWorkerEx : TechnicalTools.Tools.Threading.BackgroundWorker, BackgroundUITask.IBackgroundWorker
    {
        public BackgroundWorkerEx(string name)
            : base(name)
        {
        }
    }
}
