﻿using Microsoft.Win32;

using System;
using System.Runtime.InteropServices;
using System.Threading;


namespace TechnicalTools.Tools
{
    public class IdleDetection : IDisposable
    {
        public static readonly IdleDetection Instance = new IdleDetection();

        private IdleDetection()
        {
            _tmr = new Timer(_tmrIsScreenSaverDisplayed_Tick, null, TimeSpan.Zero, _CheckInterval);
            DetectSleepingComputer_Init();
        }
        readonly Timer _tmr;
        readonly object _lck = new object();

        #region IDisposable Support

        public void Dispose() { Dispose(true); }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                    _tmr.Dispose();
                disposedValue = true;
            }
        }
        private bool disposedValue;

        #endregion
        public TimeSpan CheckInterval
        {
            get { return _CheckInterval; }
            set { _CheckInterval = value; lock (_lck) UpdateTimerInterval(); }
        }
        TimeSpan _CheckInterval = new TimeSpan(0, 5, 0);

        void _tmrIsScreenSaverDisplayed_Tick(object _)
        {
            _lastTickUTC = DateTime.UtcNow;
            ScreenSaverDetection_Tick();
            IdleTimeDetection_Tick();
            UpdateTimerInterval();
        }
        DateTime _lastTickUTC;

        void UpdateTimerInterval()
        {
            var remainingTimeToConsidereUserAsIdle = DurationConsideredAsIdle - (DateTime.UtcNow - _lastInputKnownUTC);
            TimeSpan newCurrentInterval = _OnUserIdle == null && _OnUserGetBack == null && _OnScreenSaverDisplayed == null
                                        ? Timeout.InfiniteTimeSpan // developper dont want to know, so no check
                                        : _IsScreenSaverDisplayedAtLastCheck || _UserWasIdleAtLastCheck
                                        ? TimeSpan.FromSeconds(5) // increase the rate of check to know as soon as the user get back
                                        : _CheckInterval == TimeSpan.Zero // User want to disable ?
                                        ? Timeout.InfiniteTimeSpan
                                        : _OnUserIdle != null && _OnUserGetBack == null &&  // developper wants to know when user is idle
                                           // but according to last input we know, user is already idle
                                           remainingTimeToConsidereUserAsIdle <= TimeSpan.Zero
                                        ? _CheckInterval
                                        : _OnUserIdle != null && _OnUserGetBack == null &&  // developper wants to know when user is idle
                                           remainingTimeToConsidereUserAsIdle < _CheckInterval // remaining time to flag user as idle is between 0 and < check interval of developper
                                        ?  remainingTimeToConsidereUserAsIdle
                                        : _CheckInterval;

            if (_currentRealInterval == newCurrentInterval)
                return;

            _currentRealInterval = newCurrentInterval;
            lock (_lck)
                _tmr.Change(_currentRealInterval == Timeout.InfiniteTimeSpan
                            ? Timeout.InfiniteTimeSpan
                            : (_currentRealInterval - (DateTime.UtcNow - _lastTickUTC)).Max(TimeSpan.Zero),
                            _currentRealInterval);
        }
        TimeSpan _currentRealInterval;

        #region Detect Last User Input

        public TimeSpan DurationConsideredAsIdle { get { return _DurationConsideredAsIdle; }
                                                   set { _DurationConsideredAsIdle = value;  UpdateTimerInterval(); } }
        TimeSpan _DurationConsideredAsIdle = new TimeSpan(1, 0, 0);

        public event Action<TimeSpan> OnUserIdle
        {
            add
            {
                lock (_lck)
                {
                    _OnUserIdle += value;
                    UpdateTimerInterval();
                }
            }
            remove
            {
                lock (_lck)
                {
                    _OnUserIdle -= value;
                    UpdateTimerInterval();
                }
            }
        }
        Action<TimeSpan> _OnUserIdle;

        public event Action OnUserGetBack
        {
            add
            {
                lock (_lck)
                {
                    _OnUserGetBack += value;
                    UpdateTimerInterval();
                }
            }
            remove
            {
                lock (_lck)
                {
                    _OnUserGetBack -= value;
                    UpdateTimerInterval();
                }
            }
        }
        Action _OnUserGetBack;


        [DllImport("User32.dll")]
        static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);
        [DllImport("Kernel32.dll")]
        static extern uint GetLastError();

        [StructLayout(LayoutKind.Explicit)]
        struct LASTINPUTINFO
        {
            [FieldOffset(0)] public uint cbSize;
            [FieldOffset(4)] public uint dwTime;
        }

        public DateTime GetLastInputDateUTC()
        {
            LASTINPUTINFO lastInPut = new LASTINPUTINFO();
            lastInPut.cbSize = (uint)Marshal.SizeOf(lastInPut);
            if (!GetLastInputInfo(ref lastInPut))
                throw new Exception(GetLastError().ToString());

            var lastInputKnown = DateTime.UtcNow - TimeSpan.FromMilliseconds((uint)Environment.TickCount - lastInPut.dwTime);
            _lastInputKnownUTC = lastInputKnown.ToUniversalTime();
            return _lastInputKnownUTC;
        }
        DateTime _lastInputKnownUTC = DateTime.UtcNow;


        void IdleTimeDetection_Tick()
        {
            if (_OnUserIdle == null)
                return;
            var value = DateTime.UtcNow - GetLastInputDateUTC();
            bool isUserIdle = value >= DurationConsideredAsIdle;
            if (isUserIdle != _UserWasIdleAtLastCheck)
            {
                _UserWasIdleAtLastCheck = isUserIdle;
                if (isUserIdle)
                    _OnUserIdle?.Invoke(value);
                else
                    _OnUserGetBack?.Invoke();
            }
        }
        bool _UserWasIdleAtLastCheck;

        #endregion


        #region

        public bool ConsiderSleepingComputerAsIdle
        {
            get { return _ConsiderSleepingComputerAsIdle; }
            set { _ConsiderSleepingComputerAsIdle = value;
                // Note : PowerModeChanged seems not working when user go to sleep mode (volontarily)
                SystemEvents.SessionSwitch -= OnSessionSwitch;
                if (value)
                    SystemEvents.SessionSwitch += OnSessionSwitch;
            }
        }
        bool _ConsiderSleepingComputerAsIdle;

        void DetectSleepingComputer_Init()
        {
            ConsiderSleepingComputerAsIdle = true;
        }
        private void OnSessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            switch (e.Reason)
            {
                case SessionSwitchReason.SessionLock:
                    _dateBeforeSleep = GetLastInputDateUTC();
                    break;

                case SessionSwitchReason.SessionUnlock:
                    var now = DateTime.UtcNow;
                    var duration = now - _dateBeforeSleep;
                    if (duration > DurationConsideredAsIdle)
                    {
                        _OnUserIdle?.Invoke(duration);
                        _OnUserGetBack?.Invoke();
                    }
                    break;
            }
        }
        DateTime _dateBeforeSleep = DateTime.UtcNow;

        #endregion

        #region Detect if a screen saver is running  (not the preview !)

        public bool IsScreenSaverDisplayed { get { return GetIsScreenSaverDisplayed(); } }



        public event Action<bool> OnScreenSaverDisplayedChanged
        {
            add
            {
                lock (_lck)
                {
                    _OnScreenSaverDisplayed += value;
                    UpdateTimerInterval();
                }
            }
            remove
            {
                lock (_lck)
                {
                    _OnScreenSaverDisplayed -= value;
                    UpdateTimerInterval();
                }
            }
        }
        Action<bool> _OnScreenSaverDisplayed;


        const int SPI_GETSCREENSAVERRUNNING = 114;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool SystemParametersInfo(int uAction, int uParam, ref bool lpvParam, int flags);

        // Returns TRUE if the screen saver is actually running
        public bool GetIsScreenSaverDisplayed()
        {
            bool isRunning = false;
            if (!SystemParametersInfo(SPI_GETSCREENSAVERRUNNING, 0, ref isRunning, 0))
                throw new Exception(GetLastError().ToString());
            return isRunning;
        }

        void ScreenSaverDetection_Tick()
        {
            if (_OnScreenSaverDisplayed == null)
                return;
            bool isScreenSaverDisplayedAtLastCheck = GetIsScreenSaverDisplayed();
            if (isScreenSaverDisplayedAtLastCheck != _IsScreenSaverDisplayedAtLastCheck)
            {
                _IsScreenSaverDisplayedAtLastCheck = isScreenSaverDisplayedAtLastCheck;
                _OnScreenSaverDisplayed?.Invoke(isScreenSaverDisplayedAtLastCheck);
                if (isScreenSaverDisplayedAtLastCheck)
                    _tmr.Change(5000, 5000); // Check for user to return every 5s
                else // restore back the check interval
                    _tmr.Change(_CheckInterval == Timeout.InfiniteTimeSpan ? Timeout.InfiniteTimeSpan : TimeSpan.Zero, _CheckInterval);
            }
        }
        bool _IsScreenSaverDisplayedAtLastCheck;

        #endregion
    }
}
