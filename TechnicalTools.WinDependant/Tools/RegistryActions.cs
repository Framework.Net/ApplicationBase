﻿using System;
using System.IO;

using Microsoft.Win32;

using TechnicalTools.Tools;

namespace TechnicalTools.WinDependent
{
    
    /// <summary>
    /// Some helper for registry operations
    /// </summary>
    public static class RegistryActions
    {
        public static void AddContextMenuItemOnFileToRunProgramOnIt(string menuDisplayText, string forExtension = ".*", string exePath = null)
        {
            if (!forExtension.StartsWith("."))
                throw new ArgumentException("Must be in the form \".ext\"", nameof(forExtension));
            forExtension = forExtension == ".*" ? "*" : forExtension;
            exePath = exePath ?? GetCurrentExePath();
            var exeName = Path.GetFileNameWithoutExtension(exePath);
            
            var keyShell = Registry.ClassesRoot.OpenSubKey($@"*\shell", true);
            var keyExe = keyShell.OpenSubKey(exeName, true);
            if (keyExe == null)
                keyExe = keyShell.CreateSubKey(exeName, true);
            var keyCommand = keyExe.CreateSubKey("command", true);
            if (keyCommand == null)
                keyCommand = keyExe.CreateSubKey("command", true);
            keyCommand.SetValue(string.Empty, "\"" + exePath + "\" \"%1\"");
        }


        /// This method needs to be run with admin rights...<summary>
        /// See <see cref="WithAdminRights.Run{TBaseTraits}(string)"/> to call it
        /// Explorer needs to be restart (either by a proper a reboot or just manual kill & restart)
        /// (There is also proper way to notify shell about the change but not implemented)
        public static void AddContextMenuItemOnFolderToRunProgramOnIt(string menuDisplayText, string exePath = null)
        {
            exePath = exePath ?? GetCurrentExePath();
            RegistryKey key = Registry.ClassesRoot.OpenSubKey(@"Directory\shell", true);
            string clean_name = menuDisplayText.Replace(" ", "_");
            
            key.CreateSubKey(clean_name);
            key = key.OpenSubKey(clean_name, true);
            key.SetValue("", menuDisplayText); // Affecte la valeur par defaut

            key.CreateSubKey("command");
            key = key.OpenSubKey("command", true);
            key.SetValue("", $"\"{exePath}\" --source-folder \"%1\""); // Affecte la valeur par défaut
        }

        static string GetCurrentExePath()
        {
            // We could also use System.Windows.Forms.Application.ExecutablePath but it requires a UI dependency...
            var exePath = System.Reflection.Assembly.GetEntryAssembly().Location;
            if (Path.GetExtension(exePath).ToLowerInvariant() == ".dll") // true for .Net 7.0 for example where both dll and exe files are created
                exePath = Path.Combine(Path.GetDirectoryName(exePath), Path.GetFileNameWithoutExtension(exePath) + ".exe");
            return exePath;
        }
    }
}
