﻿using System;
using System.IO;
using System.Reflection;

using TechnicalTools.Automation;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;


namespace ApplicationBase.Common
{
    /// <summary>
    /// These switches are intended to be give more information or configure information to deliver
    /// These switches can be used on any user computer.
    /// </summary>
    public class DebugConfig : ICopyable
    {
        [Argument(Description = "Path to log4net configuration file ", 
                  DefaultValue = ".\\log4net.config (ie: same folder of executable")]
        public string LogConfigPath        { get; set; } = DefaultLogConfigPath;
        public static readonly string DefaultLogConfigPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) ?? Path.GetPathRoot(Assembly.GetEntryAssembly().Location), "log4net.config");

        [Argument(Description = "Enable display of all technical information (stack trace for example) to user when errors occur", 
                  DefaultValue = "false")]
        public bool   ShowRealErrorReason  { get; set; } = DebugTools.IsForDevelopper && DebugTools.IsDebug; // True by defaut when developper run the app inside visual studio in debug mode
        [Argument(Description = "Enable display of failing assertion", DefaultValue = "false")]
        public bool   ShowAssertion        { get; set; } = DebugTools.IsForDevelopper && DebugTools.IsDebug; // True by defaut when developper run the app inside visual studio in debug mode

        [Argument(Description = "Allow a developper to auto authenticate when application starts.\r\n" +
                                "This only works when application detects \"developper mode\" at runtime.\r\n" +
                                "To use it, specify autologin / autopassword on Visual studio => Project properties => Debug => command line.\r\n" +
                                "These data are stored in personal files, ignored by source versionning tools.")]
        public string AutoLoginWith        { get; set; }
        [Argument(Description = "See " + nameof(AutoLoginWith))]
        public string AutoPassword         { get; set; }

        [Argument(Description = "Allow application to track DAL object instances to study memory consumption.\r\n" + 
                                "When true, application may slow down a little", 
                  DefaultValue = "false")]
        public bool   TrackObjectInstances { get; set; }

        [Argument(Description = "Allow to log DB request to study them at startup.",
                  DefaultValue = "false")]
        public bool   TrackDbRequests { get; set; }

        public bool   PreventDistributedTransaction { get; set; }

        public void ShowAssertionTemporarly(Action a)
        {
            ShowAssertionTemporarly(() => { a(); return 0; });
        }
        public T ShowAssertionTemporarly<T>(Func<T> f)
        {
            var backup = ShowAssertion;
            try
            {
                ShowAssertion = true;
                return f();
            }
            finally
            {
                ShowAssertion = backup;
            }
        }

        void ICopyable.CopyFrom(ICopyable source)
        {
            CopyFrom((DebugConfig)source);
        }
        public virtual void CopyFrom(DebugConfig from)
        {
            LogConfigPath = from.LogConfigPath;

            ShowRealErrorReason = from.ShowRealErrorReason;
            ShowAssertion = from.ShowAssertion;

            AutoLoginWith = from.AutoLoginWith;
            AutoPassword = from.AutoPassword;

            TrackObjectInstances = from.TrackObjectInstances;
            TrackDbRequests = from.TrackDbRequests;

            PreventDistributedTransaction = from.PreventDistributedTransaction;
        }
    }
}
