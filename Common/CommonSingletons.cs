﻿using System;

using DataMapper;


namespace ApplicationBase.Common
{
    public class CommonSingletons : TechnicalTools.Model.SingletonFactory
    {
        // It is advised to read "Note 01" in file "Code Design Notes.txt"
        public static CommonSingletons Instance
        {
            get { return _Instance; }
            protected set { SetInstance(ref _Instance, value); }
        }
        static CommonSingletons _Instance = new CommonSingletons();

        protected CommonSingletons()
        {
        }

        protected virtual CommonFactory CreateCommonFactory() { return new CommonFactory(); }
        public CommonFactory GetCommonFactory() { return GetOrAdd(() => CreateCommonFactory()); }

        public DAL.DAO GetBaseDao() { return GetOrAddAndInvalidateDaoInstanceOnChange(() => GetCommonFactory().CreateBaseDao()); }

        // Management of Config is dirty (not compliant with how we are supposed to handle the instance)
        // To clean later
        public Config GetConnectionsConfig() { return _connections ?? (_connections = GetCommonFactory().CreateConnectionsConfig()); }
        Config _connections;
        public virtual void SetConnectionsConfig(Config cfg)
        {
            var old = _connections;
            _connections = cfg;
            ConnectionConfigChanged?.Invoke(old);
        }
        public event Action<Config> ConnectionConfigChanged;

        public TDao GetOrAddAndInvalidateDaoInstanceOnChange<TDao>(Func<TDao> createNew)
            where TDao : DbMapperWrapper
        {
            return GetOrAdd(() =>
            {
                var dao = createNew();
                void onConnectionConfigChanged(Config old)
                {
                    var potentialFuturDao = createNew();
                    if (potentialFuturDao.GetConnectionString() != dao.GetConnectionString())
                    {
                        Remove<TDao>();
                        ConnectionConfigChanged -= onConnectionConfigChanged;
                    }
                };
                ConnectionConfigChanged += onConnectionConfigChanged;
                return dao;
            });
        }
    }
}
