﻿using System;
using System.IO;
using System.Runtime.InteropServices;

using ApplicationBase.Deployment;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;

namespace ApplicationBase.Common
{
    /// <summary>
    /// Theses witches are intended to be used by developper when he runs application from his computer.
    /// </summary>
    public class DevelopperSwitches
    {
        /// <summary>
        /// By Default winform application does not display Console.
        /// This options allow developper to display console as a side window
        /// </summary>
        public bool ShowConsole { get; set; }
        /// <summary>
        /// Allow developper to put a delay before application continue execution.
        /// This alow developper to attach a debugger in live.
        /// The value is in seconds
        /// </summary>
        public int StartupWait { get; set; }
        /// <summary>
        /// Allow developper to override application's version
        /// This allow to bypass some checks annoying when developping.
        /// </summary>
        public Version AsVersion { get; set; }

        /// <summary>
        /// Allow developper to override application's version
        /// This allow to bypass some checks annoying when developping.
        /// </summary>
        public bool? AsDevelopper { get; set; }

        /// <summary>
        /// Limit the number of simultaneous thread executed by task.
        /// Sometimes, operation systems (OS) do not allow applications to open a lot of connection simultaneously.
        /// Or because user uses wifi which limits number of connection too.
        /// This settings allows, indirectly, to limit the number of concurrent connection opended by a task.
        /// It slows down the speed of execution but it may also make the task reach success instead of failing.
        /// </summary>
        public int? MaxConcurrentThreads { get; set; }

        public void Apply()
        {
            if (AsDevelopper.HasValue)
            {
                DebugTools.ResetIsForDevelopper();
                if (!DebugTools.IsForDevelopper)
                    throw new TechnicalException("Not allowed to change this settings if you are not a developper in the first place!", null);
                DebugTools.IsForDevelopper = AsDevelopper.Value;
            }
            if (ShowConsole)
            {
                AllocConsole();
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(OverrideRedirection);
            }
            if (MaxConcurrentThreads > 0)
                TechnicalTools.IList_Extensions.MaxConcurrentParallelBusinessWorkers = MaxConcurrentThreads.Value;

            if (StartupWait != 0)
                System.Threading.Thread.Sleep(StartupWait * 1000);
            if (AsVersion != null)
                Deployer.SetCurrentVersion(AsVersion);
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();

        #region Handle fact that when running in VS, console is visible but display nothing

        // From https://stackoverflow.com/a/37284208/294998
        // because in Visual studio it does not work
        // Often because settings "Project Properties -> Debug -> Enable Debuggers -> Enable Visual Studio hosting process" is unchecked.
        // Same problem with "Sql Server Debugging" (happens in VS 2013, no idea for other version)
        private static void OverrideRedirection()
        {
            var hOut = GetStdHandle(STD_OUTPUT_HANDLE);
            var hRealOut = CreateFile("CONOUT$", GENERIC_READ | GENERIC_WRITE, FileShare.Write, IntPtr.Zero, FileMode.OpenOrCreate, 0, IntPtr.Zero);
            if (hRealOut != hOut)
            {
                SetStdHandle(STD_OUTPUT_HANDLE, hRealOut);
                Console.SetOut(new StreamWriter(Console.OpenStandardOutput(), Console.OutputEncoding) { AutoFlush = true });
            }
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr GetStdHandle(int nStdHandle);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool SetStdHandle(int nStdHandle, IntPtr hHandle);

        public static readonly int STD_OUTPUT_HANDLE = -11;
        public static readonly int STD_INPUT_HANDLE = -10;
        public static readonly int STD_ERROR_HANDLE = -12;

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr CreateFile([MarshalAs(UnmanagedType.LPTStr)] string filename,
                                               [MarshalAs(UnmanagedType.U4)]     uint access,
                                               [MarshalAs(UnmanagedType.U4)]     FileShare share,
                                                                                 IntPtr securityAttributes,
                                               [MarshalAs(UnmanagedType.U4)]     FileMode creationDisposition,
                                               [MarshalAs(UnmanagedType.U4)]     FileAttributes flagsAndAttributes,
                                                                                 IntPtr templateFile);

        public static readonly uint GENERIC_WRITE = 0x40000000;
        public static readonly uint GENERIC_READ = 0x80000000;

        #endregion
    }
}
