﻿using System;


namespace ApplicationBase.Common
{
    public interface IExceptionHandler
    {
        IExceptionHandler DefaultExceptionManagement { get; set; }
        bool HandleException(string userActionSentanceError, Exception originalException);
    }
}
