﻿using System;
using System.Collections.Generic;
using System.Reflection;

using TechnicalTools;


namespace ApplicationBase.Common
{
    public class Deployer : ApplicationBase.Deployment.Deployer
    {
        protected internal Deployer(ApplicationBase.Deployment.Config cfg)
            : base(cfg)
        {
        }
        public override List<FileToDeploy> GetFilesToDeploy(Assembly mainAssembly = null)
        {
            var allDeployableFiles = base.GetFilesToDeploy(mainAssembly);
            foreach (var df in allDeployableFiles)
                df.ToDeploy &= !df.SourceLocalPath.In("de", "es", "ja", "ru");
            return allDeployableFiles;
        }
    }
}
