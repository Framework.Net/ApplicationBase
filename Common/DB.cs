﻿using System;
using System.Diagnostics;


namespace ApplicationBase.Common
{
    public class DB
    {
        protected DB() { }

        public static Config Config
        {
            get { return CommonSingletons.Instance.GetConnectionsConfig(); }
            //set { CommonSingletons.Instance.SetConnectionsConfig(value); }
        }

        // Base feature of application (Deployment, Authentication, Logging, UserSettings, others)
        public static DAL.DAO Dao_Base { [DebuggerStepThrough] get { return CommonSingletons.Instance.GetBaseDao(); } }
    }
}
