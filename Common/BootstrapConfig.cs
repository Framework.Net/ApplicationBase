﻿using System;
using System.Drawing;

using TechnicalTools.Automation;
using ApplicationBase.Deployment.Data;
using ApplicationBase.Deployment;

using ApplicationBase.DAL.Deployment;


namespace ApplicationBase.Common
{
    // This class / File is shared (by link) within project "<ApplicationName> Launcher", "<ApplicationName> Deployer" and "<ApplicationName>"
    public class BootstrapConfig : Deployment.Config
    {
        // It is advised to read "Note 01" in file "Code Design Notes.txt"
        public static BootstrapConfig Instance { get; set; }

        [Argument(IsMandatory = true,
                  Description = "Specify the domain part of unique couple (domain, environment name) the application must connect to.",
                  DefaultValue = nameof(eEnvironment.Dev),
                  AllowedValueRange = nameof(eEnvironment.Prod) + ", " + nameof(eEnvironment.Dev) + ", " + nameof(eEnvironment.PreProd) + ", " + nameof(eEnvironment.Demo) + ", " + nameof(eEnvironment.Sandbox) + ", " + nameof(eEnvironment.LocalNoDB))]
        public eEnvironment Domain  { get; protected set; } = Filter.Default.Domain.Value;
        [Argument(IsMandatory = true, 
                  Description = "Specify the environment name part of unique couple (domain, environment name) the application must connect to.\r\n" +
                                "For one domain (example : Dev) multiple environment names may exist.\r\n" +
                                "Application settings for one couple (domain, environment name) can be dependant of another couple.\r\n" + 
                                "Indeed \"inheritage\" is possible between couple. This is useful when following \"Convention over configuration\" paradigm." +
                                "For exemple all couple (Dev, Developper 1's Name), (Dev, Developper 2's Name), ... may inherit from a global couple (Dev, Default)",
                  DefaultValue = "Default")]
        public string       EnvName { get; protected set; } = Filter.Default.EnvName;

        public override IDataAccessor DataAccessor
        {
            get { return base.DataAccessor == null || !(base.DataAccessor is DataAccessor) 
                       ? (DataAccessor = new DataAccessor(this))
                       : base.DataAccessor; }
            set { base.DataAccessor = value; }
        }

        public BootstrapConfig(Type programType, string logoFileName)
            : this(GetIconFromCallingAssembly(programType, logoFileName))
        {
        }
        protected BootstrapConfig(Icon icon = null)
        {
            ApplicationIcon = icon;
        }
    }
}
