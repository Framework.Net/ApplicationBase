﻿using System;


namespace ApplicationBase.Common
{
    public class CommonFactory
    {
        public CommonFactory()
        {
        }

        public virtual Config CreateConnectionsConfig()
        {
            return new Config();
        }

        public virtual DAL.DAO CreateBaseDao()
        {
            //var cachePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            //cachePath = Path.Combine(cachePath, Deployer.CompanyName, Deployer.ProductName, "Cache");
            var cfg = CommonSingletons.Instance.GetConnectionsConfig();
            var dao = new DAL.DAO(cfg.ConnectionString_Base, AsDisconnected/*, cachePath, Deployer.GetCurrentVersion()*/);
            return dao;
        }
        protected static bool AsDisconnected { get { return CommonSingletons.Instance.GetConnectionsConfig().Environment.Domain == Deployment.Data.eEnvironment.LocalNoDB; } }
    }
}
