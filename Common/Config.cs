﻿using System;
using System.Data.SqlClient;
using System.Xml.Serialization;

using ApplicationBase.Deployment.Data;
using TechnicalTools.Model;

namespace ApplicationBase.Common
{
    /// <summary>
    /// In <see cref="Business.Config"/> the property View is declared as ViewConfig.
    /// However this property is often overriden in RealApplication.Business.Config class that inherits from ApplicationBase.Business.Config
    /// So an instance of the real ViewConfig application type is created at runtime. 
    /// And the value from ApplicationBase.UI.ViewConfig must be transfered to real instance.
    /// So it is the implementaiton of ICopyable
    /// CopyFrom must be overriden in child class
    /// </summary>
    public class Config : ICopyable
    {
        public EnvironmentConfig Environment { get; set; } = new EnvironmentConfig();

        public uint? BulkInsertTimeout { get; set; }

        public virtual string ConnectionString_Base
        {
            get { return _connectionString_Base; }
            set
            {
                _connectionString_Base = value;
                ConnectionBase = new SqlConnectionStringBuilder(_connectionString_Base);
            }
        } string _connectionString_Base;
        [XmlIgnore] public SqlConnectionStringBuilder ConnectionBase { get; private set; }

        public string ConnectionStringDialoguerListener
        {
            get { return _connectionStringDialoguerListener; }
            set
            {
                _connectionStringDialoguerListener = value;
                ConnectionDialoguerListener = new SqlConnectionStringBuilder(_connectionStringDialoguerListener);
            }
        }
        string _connectionStringDialoguerListener;
        [XmlIgnore]
        public SqlConnectionStringBuilder ConnectionDialoguerListener { get; private set; }

        public string ConnectionStringDialoguerStarter
        {
            get { return _connectionStringDialoguerStarter; }
            set
            {
                _connectionStringDialoguerStarter = value;
                ConnectionDialoguerStarter = new SqlConnectionStringBuilder(_connectionStringDialoguerStarter);
            }
        }
        string _connectionStringDialoguerStarter;
        [XmlIgnore]
        public SqlConnectionStringBuilder ConnectionDialoguerStarter { get; private set; }

        void ICopyable.CopyFrom(ICopyable source)
        {
            CopyFrom((Config)source);
        }
        protected virtual void CopyFrom(Config source)
        {
            Environment = source.Environment;
            _connectionString_Base = source.ConnectionString_Base;
            _connectionStringDialoguerListener = source.ConnectionStringDialoguerListener;
            _connectionStringDialoguerStarter = source.ConnectionStringDialoguerStarter;
        }
    }
}
