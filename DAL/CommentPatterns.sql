CREATE TABLE [Tools].[CommentPatterns]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SetId] [tinyint] NOT NULL,
	[Pattern] [nvarchar](max) NOT NULL,
	[KeyWord] [nvarchar](255) NOT NULL,
	[PreventAutomaticTreatment] [bit] NOT NULL,
	[TakeInPendingList] [bit] NULL,
 CONSTRAINT [PK__CommentPatterns] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
),
 CONSTRAINT [UQ__CommentPatterns__SetId__KeyWord] UNIQUE NONCLUSTERED 
(
	[SetId] ASC,
	[KeyWord] ASC
)
)

GO
