﻿using System;

using TechnicalTools.Model;

using DataMapper;


namespace ApplicationBase.DAL
{
    public partial class DAO : DbMapperWrapper
    {
        public DAO(string connectionString, bool asDisconnected, string name = "Base" /*, string localCachePath, Version softwareVersion*/)
            : base(BuildInnerMapper(connectionString, asDisconnected, new NamedObject(name)/*, string localCachePath, Version softwareVersion*/))
        {
        }
        static IDbMapper BuildInnerMapper(string connectionString, bool asDisconnected, IIsNamed owner)
        {
            return DbMapperFactory.CreateSqlMapperWithNotification(owner ?? new NamedObject("Base"), connectionString, asDisconnected/*, localCachePath, softwareVersion*/);
        }
        static readonly EnumMappingCheckingHelper<DAO> _enumChecker = new EnumMappingCheckingHelper<DAO>((dao) =>
        {
            Users.Feature.RecogniseExplicitNamedEnumValues(dao, dao.AsDisconnected);
            //Dialoguing.ActorType.RecogniseExplicitNamedEnumValues(dao, dao.AsDisconnected);
            //Dialoguing.Application.RecogniseExplicitNamedEnumValues(dao, dao.AsDisconnected);
            //Dialoguing.EntityType.RecogniseExplicitNamedEnumValues(dao, dao.AsDisconnected);
            //Dialoguing.EventType.RecogniseExplicitNamedEnumValues(dao, dao.AsDisconnected);
            //Dialoguing.Status.RecogniseExplicitNamedEnumValues(dao, dao.AsDisconnected);
        });
        public override void CheckEnumsAreLoaded()
        {
            _enumChecker.Check(this);
        }
    }
}
