CREATE TABLE [Optimization].[OptimizationData]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [varchar](256) NOT NULL,
	[Value] [varchar](max) NULL,
	[DateChangedUTC] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)

GO


