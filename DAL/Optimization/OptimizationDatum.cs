﻿using System;
using System.Diagnostics;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Optimization
    {
        [DbMappedTable(schemaName: "Optimization", tableName: "OptimizationData")]
        public sealed partial class OptimizationDatum : BaseDTO<IdTuple<int>>, HasPropertiesOf.Optimization.OptimizationDatum, IDALObject_Base
        {
            [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]                        public      int Id             { [DebuggerStepThrough] get { return                                  _Id; } [DebuggerStepThrough] set {                               SetTechnicalProperty(ref                   _Id, value, RaiseIdChanged); } }       int _Id;
            [DebuggerHidden][DbMappedField("Key"), DbMaxLength(256)]                      public   string Key            { get; set; }
            [DebuggerHidden][DbMappedField("Value", IsNullable = true), DbMaxLength(256)] public   string Value          { get; set; }
            [DebuggerHidden][DbMappedField("DateChangedUTC")]                             public DateTime DateChangedUTC { get; set; }

            protected override IdTuple<int> ClosedId
            {
                get { return new IdTuple<int>(Id); }
                set { Id = value.Id1; }
            }

            public OptimizationDatum()
               : this(true)
            {
            }
            public OptimizationDatum(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                Key = string.Empty;
                DateChangedUTC = DateTime.UtcNow;
            }


            public int CompareTo(HasPropertiesOf.Optimization.OptimizationDatum other)
            {
                if (other == null)
                    return 1;
                return string.CompareOrdinal(Key, other.Key);
            }

            int IComparable.CompareTo(object obj)
            {
                if (obj is HasPropertiesOf.Optimization.OptimizationDatum keyValue)
                    return CompareTo(keyValue);
                return 0;
            }

            #region Cloneable

            public new OptimizationDatum Clone() { return (OptimizationDatum)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new OptimizationDatum(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (HasPropertiesOf.Optimization.OptimizationDatum)source;
                (this as ICopyable<HasPropertiesOf.Optimization.OptimizationDatum>).CopyFrom(from);
            }
            void ICopyable<HasPropertiesOf.Optimization.OptimizationDatum>.CopyFrom(HasPropertiesOf.Optimization.OptimizationDatum source)
            {
                // Note : Les champs appartenant à la PK ne sont pas copiés
                Key = source.Key;
                Value = source.Value;
                DateChangedUTC = source.DateChangedUTC;
            }
            #endregion

        }
    }
    namespace HasPropertiesOf.Optimization
    {
        public partial interface OptimizationDatum : IHasTypedIdReadable, ICopyable<OptimizationDatum>, IComparable<OptimizationDatum>, IComparable
        {
             new int Id             { get; }
              string Key            { get; set; }
              string Value          { get; set; }
            DateTime DateChangedUTC { get; set; }
        }
    }
}

