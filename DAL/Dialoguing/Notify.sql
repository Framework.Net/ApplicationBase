-- This procedure provide an encapsulation of the rights which are needed to insert in table Dialogues
-- This way the user / login does not need all the exotic rights...
CREATE PROCEDURE [Dialoguing].[Notify] 
           @originProcessId int, -- not null
           @originHostname varchar(45), -- not null
           @originActorType tinyint, -- not null -- See table ActorTypes (ask to add your application / API in this table if needed)
           @originActorId smallint, -- not null -- Any Id that means something for your application (for exemple the current user id)
           @whenUTC datetime2(3), -- not null
           @eventType smallint, -- not null -- See table EventTypes (ask to add your custom event in this table if needed)
           @status tinyint, -- not null -- See table Statuses

           @partnerEnvironmentId int = null, -- if the notification is related to a single partner
           @entityType smallint = null, -- See table EntityTypes (ask to add your entity in this table if needed)
           @entityId bigint = null, -- Id that identify your entity of type entityType
           @additionalData nvarchar(1024) = null -- Any free text...
WITH EXECUTE AS 'app_lmt_dialoguer'
AS SET NOCOUNT ON
BEGIN
	if @originProcessId  is null raiserror('The value for @originProcessId cannot not be null', 15, 1)
	if @originHostname   is null raiserror('The value for @originHostname cannot not be null',  15, 1)
	if @originActorType  is null raiserror('The value for @originActorType cannot not be null', 15, 1)
	if @originActorId    is null raiserror('The value for @originActorId cannot not be null',   15, 1)
	if @whenUTC			 is null raiserror('The value for @whenUTC cannot not be null',         15, 1)
	if @eventType		 is null raiserror('The value for @eventType cannot not be null',       15, 1)
	if @status			 is null raiserror('The value for @status cannot not be null',		    15, 1)

	INSERT INTO [Dialoguing].[Dialogues]
			   ([OriginProcessId]
			   ,[OriginHostname]
			   ,[OriginActorType]
			   ,[OriginActorId]
			   ,[WhenUTC]
			   ,[EventType]
			   ,[Status]
			   ,[PartnerEnvironmentId]
			   ,[EntityType]
			   ,[EntityId]
			   ,[AdditionalData])
		 VALUES
			   (@originProcessId
			   ,@originHostname
			   ,@originActorType
			   ,@originActorId
			   ,@whenUTC
			   ,@eventType
			   ,@status
			   ,@partnerEnvironmentId
			   ,@entityType
			   ,@entityId
			   ,@additionalData)
END

GO


