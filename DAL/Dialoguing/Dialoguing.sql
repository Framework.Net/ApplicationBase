

CREATE schema [Dialoguing]
GO

CREATE TABLE [Dialoguing].[ActorTypes](
	[Id] [tinyint] NOT NULL PRIMARY KEY IDENTITY(1,1),
	[Name] [varchar](250) NOT NULL
)
GO


CREATE TABLE [Dialoguing].[Applications](
	[Id] [tinyint] NOT NULL PRIMARY KEY IDENTITY(1,1),
	[Name] [varchar](250) NOT NULL
)
GO


CREATE TABLE [Dialoguing].[Dialogues](
	[Id] [bigint] NOT NULL PRIMARY KEY IDENTITY(1,1),
	[OriginProcessId] [int] NOT NULL,
	[OriginHostname] [varchar](45) NOT NULL,
	[OriginActorType] [tinyint] NOT NULL,
	[OriginActorId] [smallint] NOT NULL,
	[WhenUTC] [datetime2](3) NOT NULL,
	[EventType] [smallint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[PartnerEnvironmentId] [int] NULL,
	[EntityType] [smallint] NULL,
	[EntityId] [bigint] NULL,
	[AdditionalData] [nvarchar](1024) NULL
)
GO


CREATE TABLE [Dialoguing].[EntityTypes](
	[Id] [smallint] NOT NULL PRIMARY KEY IDENTITY(1,1),
	[Type] [tinyint] NOT NULL,
	[Name] [varchar](250) NOT NULL
)
GO


CREATE TABLE [Dialoguing].[EventTypes](
	[Id] [smallint] NOT NULL PRIMARY KEY IDENTITY(1,1),
	[Family] [varchar](250) NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[TechnicalDescription] [varchar](max) NULL
)
GO


CREATE TABLE [Dialoguing].[Statuses](
	[Id] [tinyint] NOT NULL PRIMARY KEY IDENTITY(1,1),
	[Name] [varchar](250) NOT NULL
)
GO

SET IDENTITY_INSERT [Dialoguing].[ActorTypes] ON 
GO

INSERT [Dialoguing].[ActorTypes] ([Id], [Name]) VALUES (1, N'Service')
GO
INSERT [Dialoguing].[ActorTypes] ([Id], [Name]) VALUES (2, N'User')
GO
SET IDENTITY_INSERT [Dialoguing].[ActorTypes] OFF
GO
SET IDENTITY_INSERT [Dialoguing].[Applications] ON 
GO

INSERT [Dialoguing].[Applications] ([Id], [Name]) VALUES (1, N'LMT')
GO
INSERT [Dialoguing].[Applications] ([Id], [Name]) VALUES (0, N'Undefined')
GO
SET IDENTITY_INSERT [Dialoguing].[Applications] OFF
GO

SET IDENTITY_INSERT [Dialoguing].[EntityTypes] ON 
GO

INSERT [Dialoguing].[EntityTypes] ([Id], [Type], [Name]) VALUES (1, 0, N'None')
GO
SET IDENTITY_INSERT [Dialoguing].[EntityTypes] OFF
GO
SET IDENTITY_INSERT [Dialoguing].[EventTypes] ON 
GO

INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (1, N'Basics', N'Message To User', NULL)
GO
INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (2, N'Basics', N'Message And Forced Restart After Delay', NULL)
GO
INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (3, N'Basics', N'Discover Connected Actors', NULL)
GO
INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (4, N'Basics', N'Kill Instance', NULL)
GO
INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (5, N'Basics', N'Purge All Caches', NULL)
GO
INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (6, N'Basics', N'Capture Windows', NULL)
GO
INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (7, N'Basics', N'Capture Execution State', NULL)
GO
INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (8, N'Basics', N'Unused 1', NULL)
GO
INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (9, N'Basics', N'Unused 2', NULL)
GO
INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (10, N'Data Change', N'Entity Create', NULL)
GO
INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (11, N'Data Change', N'Entity Update', NULL)
GO
INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (12, N'Data Change', N'Entity Delete', NULL)
GO
INSERT [Dialoguing].[EventTypes] ([Id], [Family], [Name], [TechnicalDescription]) VALUES (13, N'Data Change', N'Entity Reset', NULL)
GO
SET IDENTITY_INSERT [Dialoguing].[EventTypes] OFF
GO

SET IDENTITY_INSERT [Dialoguing].[Statuses] ON 
GO
INSERT [Dialoguing].[Statuses] ([Id], [Name]) VALUES (0, N'Undefined')
GO
SET IDENTITY_INSERT [Dialoguing].[Statuses] OFF
GO


ALTER TABLE [Dialoguing].[ActorTypes] ADD  CONSTRAINT [UQ__Dialoguing.ActorTypes__Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)
GO

ALTER TABLE [Dialoguing].[Applications] ADD  CONSTRAINT [UQ__Dialoguing.Applications__Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)
GO

ALTER TABLE [Dialoguing].[EntityTypes] ADD  CONSTRAINT [UQ__Dialoguing.EntityTypes__Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)
GO
ALTER TABLE [Dialoguing].[Dialogues]  WITH CHECK ADD  CONSTRAINT [FK__Dialoguing.Dialogues__Dialoguing.ActorTypes__OriginActorType] FOREIGN KEY([OriginActorType])
REFERENCES [Dialoguing].[ActorTypes] ([Id])
GO
ALTER TABLE [Dialoguing].[Dialogues] CHECK CONSTRAINT [FK__Dialoguing.Dialogues__Dialoguing.ActorTypes__OriginActorType]
GO
ALTER TABLE [Dialoguing].[Dialogues]  WITH CHECK ADD  CONSTRAINT [FK__Dialoguing.Dialogues__Dialoguing.EntityTypes__EntityType] FOREIGN KEY([EntityType])
REFERENCES [Dialoguing].[EntityTypes] ([Id])
GO
ALTER TABLE [Dialoguing].[Dialogues] CHECK CONSTRAINT [FK__Dialoguing.Dialogues__Dialoguing.EntityTypes__EntityType]
GO
ALTER TABLE [Dialoguing].[Dialogues]  WITH CHECK ADD  CONSTRAINT [FK__Dialoguing.Dialogues__Dialoguing.EventTypes__EventType] FOREIGN KEY([EventType])
REFERENCES [Dialoguing].[EventTypes] ([Id])
GO
ALTER TABLE [Dialoguing].[Dialogues] CHECK CONSTRAINT [FK__Dialoguing.Dialogues__Dialoguing.EventTypes__EventType]
GO
ALTER TABLE [Dialoguing].[Dialogues]  WITH CHECK ADD  CONSTRAINT [FK__Dialoguing.Dialogues__Dialoguing.Statuses__Status] FOREIGN KEY([Status])
REFERENCES [Dialoguing].[Statuses] ([Id])
GO
ALTER TABLE [Dialoguing].[Dialogues] CHECK CONSTRAINT [FK__Dialoguing.Dialogues__Dialoguing.Statuses__Status]
GO
