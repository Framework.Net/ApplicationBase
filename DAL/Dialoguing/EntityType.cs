﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Dialoguing
    {
        [DbMappedTable(schemaName: "Dialoguing", tableName: "EntityTypes")]
        public sealed class EntityType : DynamicEnumWithId<short, EntityType>, IDALObject_Base, IEquatable<EntityType>
        {
            [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]    public  short Id   { [DebuggerStepThrough] get { return   _Id; } [DebuggerStepThrough] set {                               SetTechnicalProperty(ref  _Id, value, RaiseIdChanged); } }  short _Id;
            [DebuggerHidden][DbMappedField("Type")]                   public   byte Type { [DebuggerStepThrough] get { return _Type; } [DebuggerStepThrough] set {                                        SetProperty(ref                _Type, value); } }   byte _Type;
            [DebuggerHidden][DbMappedField("Name"), DbMaxLength(250)] public string Name { [DebuggerStepThrough] get { return _Name; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 250);          SetProperty(ref                _Name, value); } } string _Name;
    
            protected override long   EnumId   { [DebuggerStepThrough] get { return Id;   } [DebuggerStepThrough] set { Id = (byte)value;  } }
            protected override string EnumName { [DebuggerStepThrough] get { return Name; } [DebuggerStepThrough] set { Name = value; } }

            protected override IdTuple<short> ClosedId
            {
                get { return new IdTuple<short>(Id); }
                set { Id = value.Id1; }
            }


            public EntityType()
               : this(true)
            {
            }
            public EntityType(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Type = 0;
                _Name = string.Empty;
            }

            public bool Equals(EntityType other)
            {
                return Id == other?.Id;
            }
             
            #region Cloneable

            public new EntityType Clone() { return (EntityType)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new EntityType(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (EntityType)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Type = from._Type;
                _Name = from._Name;
            }

            #endregion


            #region Feature business

            [DbNameBound("None")]
            public static EntityType None { get; private set; }

            #endregion
        }
    }
}
