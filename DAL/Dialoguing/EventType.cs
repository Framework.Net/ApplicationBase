﻿using System;
using System.Diagnostics;
using System.Linq;
using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Dialoguing
    {
        [DbMappedTable(schemaName: "Dialoguing", tableName: "EventTypes")]
        public sealed class EventType : DynamicEnumWithId<short, EventType>, IDALObject_Base, IEquatable<EventType>
        {
            [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]      public  short Id                    { [DebuggerStepThrough] get { return                   _Id; } [DebuggerStepThrough] set {                               SetTechnicalProperty(ref   _Id, value, RaiseIdChanged); } }  short _Id;
            [DebuggerHidden][DbMappedField("Family"), DbMaxLength(250)] public string Family                { [DebuggerStepThrough] get { return               _Family; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 250);          SetProperty(ref               _Family, value); } } string _Family;
            [DebuggerHidden][DbMappedField("Name"), DbMaxLength(250)]   public string Name                  { [DebuggerStepThrough] get { return                 _Name; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 250);          SetProperty(ref                 _Name, value); } } string _Name;
            [DebuggerHidden][DbMappedField("TechnicalDescription")]     public string TechnicalDescription  { [DebuggerStepThrough] get { return _TechnicalDescription; } [DebuggerStepThrough] set {                                        SetProperty(ref _TechnicalDescription, value); } } string _TechnicalDescription;
    
            protected override long   EnumId   { [DebuggerStepThrough] get { return Id;   } [DebuggerStepThrough] set { Id = (short)value;  } }

            protected override string EnumName
            {
                [DebuggerStepThrough] get { return Family + "|" + Name; }
                [DebuggerStepThrough]
                set
                {
                    var parts = value.Split('|');
                    Family = parts.Length == 1 ? "" : parts[0];
                    Name = parts.Length == 1 ? parts[0] : parts[1];
                }
            }

            protected override IdTuple<short> ClosedId
            {
                get { return new IdTuple<short>(Id); }
                set { Id = value.Id1; }
            }


            public EventType()
               : this(true)
            {
            }
            public EventType(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Name = string.Empty;
    
            }

            public bool Equals(EventType other)
            {
                return Id == other?.Id;
            }
             
            #region Cloneable

            public new EventType Clone() { return (EventType)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new EventType(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (EventType)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Name = from._Name;
                _Family= from._Family;
                _TechnicalDescription = from._TechnicalDescription;
            }

            #endregion

            #region Feature business

            [DbNameBound("Basics|Message To User")]                        public static EventType MessageToUser { get; private set; }
            [DbNameBound("Basics|Message And Forced Restart After Delay")] public static EventType MessageAndForcedRestartAfterDelay { get; private set; }
            [DbNameBound("Basics|Discover Connected Actors")]              public static EventType DiscoverConnectedActors { get; private set; }
            [DbNameBound("Basics|Kill Instance")]                          public static EventType KillInstance { get; private set; }
            [DbNameBound("Basics|Purge All Caches")]                       public static EventType PurgeAllCaches { get; private set; }
            [DbNameBound("Basics|Capture Windows")]                        public static EventType CaptureWindows { get; private set; }
            [DbNameBound("Basics|Capture Execution State")]                public static EventType CaptureExecutionState { get; private set; }
            [DbNameBound("Basics|Unused 1")]                               public static EventType Unused1 { get; private set; }
            [DbNameBound("Basics|Unused 2")]                               public static EventType Unused2 { get; private set; }
            [DbNameBound("Data Change|Entity Create")]                     public static EventType EntityCreate { get; private set; }
            [DbNameBound("Data Change|Entity Update")]                     public static EventType EntityUpdate { get; private set; }
            [DbNameBound("Data Change|Entity Delete")]                     public static EventType EntityDelete { get; private set; }
            [DbNameBound("Data Change|Entity Reset")]                      public static EventType EntityReset  { get; private set; }
            
            #endregion
        }
    }
}
