﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Tools;
using TechnicalTools.Logs;


namespace ApplicationBase.DAL.Dialoguing
{
    public class Messenger : IUserInteractiveObject
    {
        public FixedBindingList<string> Messages { get; } = new FixedBindingList<string>();

        public Messenger(Dialoguer dialoguer)
        {
            _dialoguer = dialoguer;

            _dialoguer.ReceiveEvent += _notifier_ReceiveEvent;
            if (!_dialoguer.Start())
                BusEvents.Instance.RaiseBusinessWarning("Messenger won't work because dialoguer cannot start!", _log);
        }
        readonly Dialoguer _dialoguer;
        static readonly ILogger _log = LogManager.Default.GetLogger(typeof(Messenger));

        public Dialogue SendMessage(string msg, string to = null, uint? processId = null)
        {
            Debug.Assert(to == null || !to.Contains('|') && !to.Contains('>'));
            // Format message in the form "hostname>processiD|message to send" where hostname and processId are optional when we want to send to everyone
            string data = (to ?? "")
                       + ">" + processId.ToStringInvariant()
                       + "|" + msg;
            var d = _dialoguer.BuildDialogue(EventType.MessageToUser, additionalData: data);
            _dialoguer.Broadcast(d);
            return d;
        }
        void OnReceiveMessage(Dialogue e)
        {
            Debug.Assert(e.AdditionalData.Length >= 2);
            int index = e.AdditionalData.IndexOf('>');
            int index2 = e.AdditionalData.IndexOf('|');
            string hostname = e.AdditionalData.Remove(index);
            string processId = e.AdditionalData.Substring(index + 1, index2 - index - 1);
            // We are included in the recipient of this message
            if ((hostname.Length == 0 || hostname.ToUpperInvariant() == _dialoguer.Hostname) &&
                (processId.Length == 0 || int.Parse(processId, CultureInfo.InvariantCulture) == _dialoguer.ProcessId))
            {
                Messages.Add(e.AdditionalData.Substring(index2 + 1));
            }
        }

        /// <summary>
        /// Ask to all actors listening to respond.
        /// we get the response through callback as long as you keep a reference to Dialogue and callback
        /// </summary>
        /// <param name="callback"></param>
        /// <returns>instance of object as request handler, callback can be called until instance returned is not garbage collected</returns>
        public Dialogue ListAllConnectedActors(Action<Dialogue> callback)
        {
            var d = _dialoguer.BuildDialogue(EventType.DiscoverConnectedActors);
            requests.Add(d, callback);
            _dialoguer.Broadcast(d);
            return d;
        }
        readonly ConditionalWeakTable<Dialogue, Action<Dialogue>> requests = new ConditionalWeakTable<Dialogue, Action<Dialogue>>();
        
        public void ReceiveConnectedActorsResponses(Dialogue e)
        {
            // We receive a request sent by another application
            if (e.Status == Status.Undefined)
            {
                _log.Info($"Responding to discovering actors request id {e.Id} (hostname: {e.OriginHostname}, processid:{e.OriginProcessId}).");
                // So we respond to this application
                var d = _dialoguer.BuildDialogue(EventType.DiscoverConnectedActors, Status.GenericResponse, additionalData:e.Id.ToStringInvariant());
                _dialoguer.Broadcast(d);
            }
            // We receive a response
            else if (e.Status == Status.GenericResponse)
            {
                long requestId = long.Parse(e.AdditionalData, CultureInfo.InvariantCulture);
                Dialogue originalRequest = _dialoguer.TryGetDialogueById(requestId);
                if (requests.TryGetValue(originalRequest, out Action<Dialogue>  callback))
                    callback.Invoke(e);
            }
            else
                _log.Warn($"Unknown status {e.Status} for dialogue of type {e.EventType}!");
        }


        void _notifier_ReceiveEvent(Dialogue e)
        {
            Debug.Assert(e.EventOrigin == eEventOrigin.AnotherClient);
            if (e.EventType == EventType.MessageToUser)
                OnReceiveMessage(e);
            else if (e.EventType == EventType.DiscoverConnectedActors)
                ReceiveConnectedActorsResponses(e);
        }
    }
}
