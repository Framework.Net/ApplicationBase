﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;

using DataMapper;


namespace ApplicationBase.DAL.Dialoguing
{
    public class Dialoguer : IIsNamed
    {
        public bool     DisablePooling           { get; set; }
        public bool     IsInitialized            { get; private set; }
        public long     LastCheckedDialogueId    { get; private set; }
        public DateTime LastUpdateDateUTC        { get; private set; }
        public TimeSpan ServerTimeDrift          { get; private set; }

        readonly object _lock = new object();

        public int   ProcessId     { get; }
        public string Hostname      { get; }
        public short OriginActorId { get; }

        readonly ServerNotifier _notifier;
        readonly IDbMapper _mapper;
        readonly SynchronizationContext _context;

        public string Name { get { return "Notifier of " + Assembly.GetEntryAssembly().GetName().Name; } }
        
        public Dialoguer(string connectionStringListener, string connectionStringStarter, short originActorId, SynchronizationContext context)
        {
            _context = context;
            ProcessId = Process.GetCurrentProcess().Id;
            Hostname = Environment.MachineName;
            OriginActorId = originActorId;
            _mapper = DbMapperFactory.CreateSqlMapper(this, connectionStringListener);
            
            // Il est preferable d'avoir une connectionstring specifique pour les notification
            // Changer simplement le champs "Application Name" suffit a faire en sorte que cette connection soit seule dans son application pool
            // Cela evite peut permettre de ne pas avoir a se frotter à des problemes d'isolation level...
            _notifier = new ServerNotifier(connectionStringListener, connectionStringStarter);

            var dt1 = DateTime.UtcNow;
            var utcDateonServer = (DateTime)_mapper.GetDataTable("select getutcdate()").Rows[0][0];
            var dt2 = DateTime.UtcNow;
            ServerTimeDrift = (utcDateonServer - dt2).AddMilliSeconds(-(int)(dt2 - dt1).TotalMilliseconds / 2);
        }

        public event EventHandler Initialized;

        public bool Start()
        {
            if (IsInitialized)
                return true;

            LastCheckedDialogueId = _mapper.LoadCollection<Dialogue>().Max(log => (int?)log.Id) ?? 0; // Pas optimisé si beaucoup de log.. on s'en fout c'est du test

            LastUpdateDateUTC = DateTime.UtcNow;
            Initialized?.Invoke(this, EventArgs.Empty);

            if (!_notifier.IsReady && !_notifier.Start())
                return false;
            var mTable = _mapper.GetTableMapping(typeof(Dialogue));
            var colIdName = mTable.GetMappedColumnFromProperty((Dialogue d) => d.Id).ColumnName;
            _notifier.Listen(() => $"SELECT {colIdName} FROM {mTable.FullName} WHERE {colIdName} > {LastCheckedDialogueId}",
                                            _context, (_, e) =>
                                            {
                                                if (e.Info == SqlNotificationInfo.Insert)
                                                {
                                                    if (!_isCheckScheduled)
                                                    {
                                                        _isCheckScheduled = true;
                                                        // Post action sinon deadlock possible (en cas de timeout du DoCheckNow + interface principale ou autre thread qui veut envoyer un event (n'importe quelle modification)
                                                        DoCheckNow();
                                                    }
                                                }
                                            }, 750); // Plusieurs EventLog peuvent être inséré dans un laps de temps très cours ( < 750 msecondes) :
                                                    // On attend ce laps de temps avant de resouscrire au sqldependency, pour éviter un trop grand nombre de notifications inutiles.
            IsInitialized = true;
            return true;
        }
        bool _isCheckScheduled;

        public void Stop()
        {
            if (_notifier.IsReady)
                _notifier.Stop();
        }

        public void CheckNow()
        {
            var now = DateTime.UtcNow;
            if ((now - LastUpdateDateUTC).TotalSeconds < 1)
                return;

            DoCheckNow();
        }

        void DoCheckNow()
        {
            lock (_lock)
            {
                if (_checkingNow || DisablePooling) // Gère les problèmes de récursion au sein du même thread
                    return;
                _checkingNow = true;
                try
                {
                    _isCheckScheduled = false;
                    var events = _mapper.LoadCollection<Dialogue>($"Id > {LastCheckedDialogueId}");
                    LastUpdateDateUTC = DateTime.UtcNow;

                    foreach (var e in events.OrderBy(evt => evt.Id))
                        // Prevent to receive the dialogue we send ourself...
                        // However, this condition allows multiple instance of software, on same computer, to communicate each other
                        if (e.OriginHostname != Hostname || e.OriginProcessId != ProcessId) 
                        {
                            e.EventOrigin = eEventOrigin.AnotherClient;
                            LastCheckedDialogueId = e.Id;
                            RaiseReceive(e);
                        }
                }
                finally
                {
                    _checkingNow = false;
                }
            }
        }
        bool _checkingNow; // Empêche une récursion infini rare utilisant les portfolios

        public delegate void ClassEventHandler(Dialogue e);
        public event ClassEventHandler ReceiveEvent; void RaiseReceive(Dialogue e) { ReceiveEvent?.Invoke(e); }

        public event ClassEventHandler BroadcastEvent; void RaiseBroadcast(Dialogue e) { BroadcastEvent?.Invoke(e); }

        public delegate void BroadcastingEventHandler(Dialogue e, ref bool cancel);
        public event BroadcastingEventHandler BroadcastingEvent; void RaiseBroadcasting(Dialogue e, ref bool cancel) { BroadcastingEvent?.Invoke(e, ref cancel); }

        public Dialogue BuildDialogue(EventType eventType, Status status = null, EntityType entityType = null, long? entityId = null, string additionalData = null, ActorType originActorType = null, short? actorId = null)
        {
            return new Dialogue(eEventOrigin.LocalClient)
            {
                OriginHostname = Hostname,
                OriginProcessId = ProcessId,
                OriginActorType = originActorType ?? ActorType.User,
                OriginActorId = actorId ?? OriginActorId,

                WhenUTC = DateTime.UtcNow,
                EventType = eventType,
                Status = status ?? Status.Undefined,
                EntityType = entityType,
                EntityId = entityId,
                AdditionalData = additionalData
            };
        }

        public bool Broadcast(Dialogue e)
        {
            Debug.Assert(e.EventOrigin == eEventOrigin.LocalClient);

            bool cancel = false;
            lock (_lock)
            {
                RaiseBroadcasting(e, ref cancel);
                if (cancel)
                    return false;

                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => _mapper.CreateInDatabase(e, true));
                RaiseBroadcast(e);
            }
            return true;
        }

        public bool Broadcasts(List<Dialogue> es)
        {
            bool cancel = false;
            lock (_lock)
            {
                int i = 0;
                while (i < es.Count)
                {
                    Debug.Assert(es[i].EventOrigin == eEventOrigin.LocalClient);
                    RaiseBroadcasting(es[i], ref cancel);
                    if (cancel)
                        es.RemoveAt(i);
                    else
                        ++i;
                }

                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => _mapper.CreateInDatabaseCollection(es, true));
                foreach (var e in es)
                    RaiseBroadcast(e);
            }
            return true;
        }

        public Dialogue TryGetDialogueById(long id)
        {
            return _mapper.TryGetObject<Dialogue>(id);
        }
    }

}
