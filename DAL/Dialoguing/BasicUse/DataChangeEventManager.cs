﻿using System;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL.Dialoguing
{
    public class DataChangeEventManager
    {
        //protected readonly IDbMapper _mapper;
        protected readonly Dialoguer _dialoguer;

        protected internal DataChangeEventManager(IDbMapper mapper, Dialoguer dialoguer)
        {
            //_mapper = mapper;
            _dialoguer = dialoguer;
            mapper.DataChanged += mapper_DataChanged;
        }

        public bool Enable { get; set; }

        void mapper_DataChanged(IDbMapper mapper, DataChangedEventArgs e)
        {
            if (!Enable)
                return;
            if (!IsInteresting(e))
                return;

            var d = _dialoguer.BuildDialogue(ToEventType(e.ChangeType), Status.Undefined);

            var encodedReferences = EncodeEntityReference(e.EntityType, e.EntityIds);
            d.EntityType = encodedReferences.Item1;
            d.EntityId = encodedReferences.Item2;
            d.AdditionalData = encodedReferences.Item3;

            _dialoguer.Broadcast(d);
        }

        protected virtual bool IsInteresting(DataChangedEventArgs e)
        {
            return e.EntityType != typeof(Dialoguer);
        }

        protected static EventType ToEventType(eDataChangedType ct)
        {
            switch (ct)
            {
                case eDataChangedType.Created:  return EventType.EntityCreate;
                case eDataChangedType.Updated:  return EventType.EntityUpdate;
                case eDataChangedType.Deleted:  return EventType.EntityDelete;
                case eDataChangedType.Truncate: return EventType.EntityReset;
                default: throw new TechnicalException("Unknown case!", null);
            }
        }

        protected virtual Tuple<EntityType, long?, string> EncodeEntityReference(Type type, IIdTuple[] ids)
        {
            EntityType entityType = EntityType.None;
            long? entityId = null;

            // Note: We should enter all entityType in table and use id to save space;
            // However the volumetry of the expected number notification by day is low so ... no space problem
            // and we can/will probably clean the table regularly...
            string data = type.ToSmartString(true);
            //d.EntityType = // id for e.EntityType
            if (ids.Length > 0)
            {
                if (ids[0].Keys.Length == 1)
                    if (ids.Length == 1)
                    {
                        var id = ids[0].Keys[0];
                        if (id is int)
                            entityId = (int)id;
                        else if (id is uint)
                            entityId = (uint)id;
                        else if (id is long)
                            entityId = (long)id;
                        else if (id is ulong)
                            entityId = (long)(ulong)id;
                        else
                            data += ">" + ids[0].Keys[0].ToStringInvariant();
                    }
                    else
                    {
                        if (ids[0].Keys[0] is int)
                            data += ">" + ids.Select(id => (long)(int)id.Keys[0]).ToMappedIntegerSet().Intervals.Select(i => i.LowerBound.ToStringInvariant() + "-" + i.UpperBound.ToStringInvariant()).Join(";");
                        else if (ids[0].Keys[0] is uint)
                            data += ">" + ids.Select(id => (long)(uint)id.Keys[0]).ToMappedIntegerSet().Intervals.Select(i => i.LowerBound.ToStringInvariant() + "-" + i.UpperBound.ToStringInvariant()).Join(";");
                        else if (ids[0].Keys[0] is long)
                            data += ">" + ids.Select(id => (long)id.Keys[0]).ToMappedIntegerSet().Intervals.Select(i => i.LowerBound.ToStringInvariant() + "-" + i.UpperBound.ToStringInvariant()).Join(";");
                        else if (ids[0].Keys[0] is ulong)
                            data += ">" + ids.Select(id => (long)(ulong)id.Keys[0]).ToMappedIntegerSet().Intervals.Select(i => i.LowerBound.ToStringInvariant() + "-" + i.UpperBound.ToStringInvariant()).Join(";");
                        else
                            data += ">" + ids.Select(id => id.Keys[0].ToStringInvariant()).Join(";");
                    }
                else
                    data += ">" + ids.Select(id => id.Keys.Select(k => k.ToStringInvariant()).Join(",")).Join(";");
            }
            return Tuple.Create(entityType, entityId, data);
        }

        protected virtual Tuple<Type, IIdTuple[]> DecodeEntityReference(EntityType entityType, long? entityid, string data)
        {
            return Tuple.Create((Type)null, (IIdTuple[])null);
        }
    }
}
