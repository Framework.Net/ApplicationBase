﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Dialoguing
    {
        [DbMappedTable(schemaName: "Dialoguing", tableName: "ActorTypes")]
        public sealed class ActorType : DynamicEnumWithId<byte, ActorType>, IDALObject_Base, IEquatable<ActorType>
        {
            [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]    public   byte Id   { [DebuggerStepThrough] get { return   _Id; } [DebuggerStepThrough] set {                               SetTechnicalProperty(ref  _Id, value, RaiseIdChanged); } }   byte _Id;
            [DebuggerHidden][DbMappedField("Name"), DbMaxLength(250)] public string Name { [DebuggerStepThrough] get { return _Name; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 250);          SetProperty(ref                _Name, value); } } string _Name;
    
            protected override long   EnumId   { [DebuggerStepThrough] get { return Id;   } [DebuggerStepThrough] set { Id = (byte)value;  } }
            protected override string EnumName { [DebuggerStepThrough] get { return Name; } [DebuggerStepThrough] set { Name = value; } }

            protected override IdTuple<byte> ClosedId
            {
                get { return new IdTuple<byte>(Id); }
                set { Id = value.Id1; }
            }


            public ActorType()
               : this(true)
            {
            }
            public ActorType(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Name = string.Empty;
    
            }

            public bool Equals(ActorType other)
            {
                return Id == other?.Id;
            }
             
            #region Cloneable

            public new ActorType Clone() { return (ActorType)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new ActorType(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (ActorType)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Name = from._Name;
            }

            #endregion

            #region Feature business

            [DbNameBound("Service")]
            public static ActorType Service { get; private set; }

            [DbNameBound("User")]
            public static ActorType User { get; private set; }

            #endregion
        }
    }
}
