-- strongly inspired from https://www.codeproject.com/Articles/12862/Minimum-Database-Permissions-Required-for-SqlDepen
-- Revisited by comments (same links)


USE master

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'app_lmt_startUser')
DROP LOGIN [app_lmt_startUser]
IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'app_lmt_subscribeUser')
DROP LOGIN [app_lmt_subscribeUser]


-- Ensuring that Service Broker is enabled 
ALTER DATABASE [LMT] SET ENABLE_BROKER
GO 

-- Creating users
CREATE LOGIN [app_lmt_startUser] WITH PASSWORD=N'app_lmt_startUser', 
            DEFAULT_DATABASE=[LMT], 
            CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
CREATE LOGIN [app_lmt_subscribeUser] WITH PASSWORD=N'app_lmt_subscribeUser', 
            DEFAULT_DATABASE=[LMT], CHECK_EXPIRATION=OFF, 
            CHECK_POLICY=OFF
GO

-- Switching to our database
use [LMT]


/*
 * Creating the users in this database
 *
 * We're going to create two users. One called app_lmt_startUser. This is the user 
 * that is going to have sufficient rights to run SqlDependency.Start.
 * The other user is called app_lmt_subscribeUser, and this is the user that is 
 * going to actually register for changes on the Users-table created earlier.
 * Technically, you're not obligated to make two different users naturally, 
 * but I did here anyway to make sure that I know the minimal rights required
 * for both operations
 *
 * Pay attention to the fact that the app_lmt_startUser-user has a default schema set.
 * This is critical for SqlDependency.Start to work. Below is explained why.
 */
CREATE USER [app_lmt_startUser] FOR LOGIN [app_lmt_startUser] 
WITH DEFAULT_SCHEMA = [app_lmt_startUser]
GO

CREATE USER [app_lmt_subscribeUser] FOR LOGIN [app_lmt_subscribeUser]
--WITH DEFAULT_SCHEMA = [app_lmt_subscribeUser]
GO



/*
 * Creating the schema
 *
 * It is vital that we create a schema specifically for app_lmt_startUser and that we
 * make this user the owner of this schema. We also need to make sure that 
 * the default schema of this user is set to this new schema (we have done 
 * this earlier)
 *
 * If we wouldn't do this, then SqlDependency.Start would attempt to create 
 * some queues and stored procedures in the user's default schema which is
 * dbo. This would fail since app_lmt_startUser does not have sufficient rights to 
 * control the dbo-schema. Since we want to know the minimum rights app_lmt_startUser
 * needs to run SqlDependency.Start, we don't want to give him dbo priviliges.
 * Creating a separate schema ensures that SqlDependency.Start can create the
 * necessary objects inside this app_lmt_startUser schema without compromising 
 * security.
 */
CREATE SCHEMA [app_lmt_startUser] AUTHORIZATION [app_lmt_startUser]
GO

/*
 * Creating two new roles. We're not going to set the necessary permissions 
 * on the user-accounts, but we're going to set them on these two new roles.
 * At the end of this script, we're simply going to make our two users 
 * members of these roles.
 */

EXEC sp_addrole 'sql_dependency_subscriber' 
EXEC sp_addrole 'sql_dependency_starter' 

-- Permissions needed for [sql_dependency_starter]
GRANT CREATE PROCEDURE to [sql_dependency_starter] 
GRANT CREATE QUEUE to [sql_dependency_starter]
GRANT CREATE SERVICE to [sql_dependency_starter]
GRANT REFERENCES on 
CONTRACT::[http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]
  to [sql_dependency_starter] 
GRANT VIEW DEFINITION TO [sql_dependency_starter] 

-- Permissions needed for [sql_dependency_subscriber] 
GRANT SELECT to [sql_dependency_subscriber] 
GRANT SUBSCRIBE QUERY NOTIFICATIONS TO [sql_dependency_subscriber] 
GRANT RECEIVE ON QueryNotificationErrorsQueue TO [sql_dependency_subscriber] 
GRANT REFERENCES on 
CONTRACT::[http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]
  to [sql_dependency_subscriber] 

--GRANT CREATE PROCEDURE to [sql_dependency_subscriber] 
--GRANT CREATE QUEUE to [sql_dependency_subscriber] 
--GRANT CREATE SERVICE to [sql_dependency_subscriber] 


-- Making sure that my users are member of the correct role.
-- for SQL Server < 2012
EXEC sp_addrolemember 'sql_dependency_starter', 'app_lmt_startUser'
EXEC sp_addrolemember 'sql_dependency_subscriber', 'app_lmt_subscribeUser'

-- for SQL Server >= 2012
--ALTER ROLE [sql_dependency_starter] ADD_MEMBER [app_lmt_startUser]
--ALTER ROLE [sql_dependency_subscriber] ADD MEMBER [app_lmt_subscribeUser]
GO

																									  
/********* Finally we create a new login that will have the right of two other logins *********/
-- It seems to better work on 2008 < R2

--IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'app_lmt_dialoguer')
--   DROP LOGIN [app_lmt_dialoguer]
--GO
--CREATE LOGIN [app_lmt_subscribeUser] WITH PASSWORD=N'app_lmt_subscribeUser', 
--            DEFAULT_DATABASE=[LMT], CHECK_EXPIRATION=OFF, 
--            CHECK_POLICY=OFF
--GO
--CREATE SCHEMA dialoguing_dynamic
--GO
--CREATE USER [app_lmt_dialoguer] FOR LOGIN [app_lmt_dialoguer] WITH DEFAULT_SCHEMA=[dialoguing_dynamic]
--EXEC sp_addrolemember 'sql_dependency_starter', 'app_lmt_dialoguer' -- apparement pas obligatoire si version 2008 > R2
--EXEC sp_addrolemember 'sql_dependency_subscriber', 'app_lmt_dialoguer'
--GO

---- Reste du code pas utile apparement
----GRANT CREATE PROCEDURE to [app_lmt_startUser] 
----GRANT CREATE QUEUE to [app_lmt_startUser]
----GRANT CREATE SERVICE to [app_lmt_startUser]
----GRANT REFERENCES on CONTRACT::[http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification] to [app_lmt_startUser] 
------GRANT VIEW DEFINITION TO [app_lmt_startUser]

----GRANT CREATE PROCEDURE to [app_lmt_subscribeUser] 
----GRANT CREATE QUEUE to [app_lmt_subscribeUser]
----GRANT CREATE SERVICE to [app_lmt_subscribeUser]
----GRANT REFERENCES on CONTRACT::[http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification] to [app_lmt_subscribeUser] 
----GRANT VIEW DEFINITION TO [app_lmt_subscribeUser]
