﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Dialoguing
    {
        [DbMappedTable(schemaName: "Dialoguing", tableName: "Dialogues")]
        public sealed class Dialogue : BaseDTO<IdTuple<long>>, IDALObject_Base
        {
            [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]                                  public       long Id                   { [DebuggerStepThrough] get { return                   _Id; } [DebuggerStepThrough] set {                                                     SetTechnicalProperty(ref    _Id, value, RaiseIdChanged); } }       long _Id;
            [DebuggerHidden][DbMappedField("OriginHostname"), DbMaxLength(45)]                      public     string OriginHostname       { [DebuggerStepThrough] get { return       _OriginHostname; } [DebuggerStepThrough] set {                         ChkFieldLen(ref value, 45);          SetProperty(ref        _OriginHostname, value); } }     string _OriginHostname;
            [DebuggerHidden][DbMappedField("OriginProcessId")]                                      public        int OriginProcessId      { [DebuggerStepThrough] get { return      _OriginProcessId; } [DebuggerStepThrough] set {                                                              SetProperty(ref       _OriginProcessId, value); } }        int _OriginProcessId;
            [DebuggerHidden][DbMappedField("OriginActorType")]                                      public  ActorType OriginActorType      { [DebuggerStepThrough] get { return      _OriginActorType; } [DebuggerStepThrough] set {                                                              SetProperty(ref       _OriginActorType, value); } }  ActorType _OriginActorType;
            [DebuggerHidden][DbMappedField("OriginActorId")]                                        public      short OriginActorId        { [DebuggerStepThrough] get { return        _OriginActorId; } [DebuggerStepThrough] set {                                                              SetProperty(ref         _OriginActorId, value); } }      short _OriginActorId;
            [DebuggerHidden][DbMappedField("WhenUTC")]                                              public   DateTime WhenUTC              { [DebuggerStepThrough] get { return              _WhenUTC; } [DebuggerStepThrough] set {  ChkRange(value, MinValueOf.WhenUTC, MaxValueOf.WhenUTC);    SetProperty(ref               _WhenUTC, value); } }   DateTime _WhenUTC;
            [DebuggerHidden][DbMappedField("EventType")]                                            public  EventType EventType            { [DebuggerStepThrough] get { return            _EventType; } [DebuggerStepThrough] set {                                                              SetProperty(ref             _EventType, value); } }  EventType _EventType;
            [DebuggerHidden][DbMappedField("Status")]                                               public     Status Status               { [DebuggerStepThrough] get { return               _Status; } [DebuggerStepThrough] set {                                                              SetProperty(ref                _Status, value); } }     Status _Status;
            [DebuggerHidden][DbMappedField("PartnerEnvironmentId", IsNullable = true)]              public       int? PartnerEnvironmentId { [DebuggerStepThrough] get { return _PartnerEnvironmentId; } [DebuggerStepThrough] set {                                                              SetProperty(ref  _PartnerEnvironmentId, value); } }       int? _PartnerEnvironmentId;
            [DebuggerHidden][DbMappedField("EntityType", IsNullable = true)]                        public EntityType EntityType           { [DebuggerStepThrough] get { return           _EntityType; } [DebuggerStepThrough] set {                                                              SetProperty(ref            _EntityType, value); } } EntityType _EntityType;
            [DebuggerHidden][DbMappedField("EntityId", IsNullable = true)]                          public      long? EntityId             { [DebuggerStepThrough] get { return             _EntityId; } [DebuggerStepThrough] set {                                                              SetProperty(ref              _EntityId, value); } }      long? _EntityId;
            [DebuggerHidden][DbMappedField("AdditionalData", IsNullable = true), DbMaxLength(1024)] public     string AdditionalData       { [DebuggerStepThrough] get { return       _AdditionalData; } [DebuggerStepThrough] set {                       ChkFieldLen(ref value, 1024);          SetProperty(ref        _AdditionalData, value); } }     string _AdditionalData;

            public eEventOrigin EventOrigin { get; set; }

            protected override IdTuple<long> ClosedId
            {
                get { return new IdTuple<long>(Id); }
                set { Id = value.Id1; }
            }

            [Obsolete("Only for genericity, use Dialoguer.BuildEvent", true)]
            public Dialogue()
               : this(true)
            {
            }
            public Dialogue(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _OriginHostname = string.Empty;
    
            }
            public Dialogue(eEventOrigin origin)
                : this(true)
            {
                EventOrigin = origin;
            }
            #region Cloneable

            public new Dialogue Clone() { return (Dialogue)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new Dialogue(false); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (Dialogue)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _OriginProcessId = from._OriginProcessId;
                _OriginHostname = from._OriginHostname;
                _OriginActorType = from._OriginActorType;
                _OriginActorId = from._OriginActorId;
                _WhenUTC = from._WhenUTC;
                _EventType = from._EventType;
                _Status = from._Status;
                _PartnerEnvironmentId = from._PartnerEnvironmentId;
                _EntityType = from._EntityType;
                _EntityId = from._EntityId;
                _AdditionalData = from._AdditionalData;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
                public static readonly DateTime WhenUTC = new DateTime(0); // 0001-01-01T00:00:00.0000000
            }
    
            static class MaxValueOf
            {
                public static readonly DateTime WhenUTC = new DateTime(3155378975999990000); // 9999-12-31T23:59:59.9990000
            }
    
            #endregion
        }
    }
    public enum eEventOrigin
    {
        LocalClient = 0,
        AnotherClient = 1
    }
}
