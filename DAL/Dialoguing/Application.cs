﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Dialoguing
    {
        [DbMappedTable(schemaName: "Dialoguing", tableName: "Applications")]
        public sealed class Application : DynamicEnumWithId<byte, Application>, IDALObject_Base, IEquatable<Application>
        {
            [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]    public   byte Id   { [DebuggerStepThrough] get { return   _Id; } [DebuggerStepThrough] set {                               SetTechnicalProperty(ref  _Id, value, RaiseIdChanged); } }   byte _Id;
            [DebuggerHidden][DbMappedField("Name"), DbMaxLength(250)] public string Name { [DebuggerStepThrough] get { return _Name; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 250);          SetProperty(ref                _Name, value); } } string _Name;
    
            protected override long   EnumId   { [DebuggerStepThrough] get { return Id;   } [DebuggerStepThrough] set { Id = (byte)value;  } }
            protected override string EnumName { [DebuggerStepThrough] get { return Name; } [DebuggerStepThrough] set { Name = value; } }

            protected override IdTuple<byte> ClosedId
            {
                get { return new IdTuple<byte>(Id); }
                set { Id = value.Id1; }
            }


            public Application()
               : this(true)
            {
            }
            public Application(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Name = string.Empty;
    
            }

            public bool Equals(Application other)
            {
                return Id == other?.Id;
            }
             
            #region Cloneable

            public new Application Clone() { return (Application)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new Application(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (Application)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Name = from._Name;
            }

            #endregion

            #region Feature business

            [DbNameBound("Undefined")]
            public static Application Undefined { get; private set; }

            [DbNameBound("LMT")]
            public static Application LMT { get; private set; }

            #endregion
        }
    }
}
