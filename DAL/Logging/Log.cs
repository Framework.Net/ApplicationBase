﻿using System;
using System.Diagnostics;

using TechnicalTools;
using TechnicalTools.Logs;
using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Logging
    {
        [DbMappedTable(schemaName: "Logging", tableName: "Logs")]
        public sealed partial class Log : BaseDTO<IdTuple<long>>, IDALObject_Base, ILog
        {
            [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]        public        long Id                { [DebuggerStepThrough] get { return                                                 _Id; } [DebuggerStepThrough] set {                                                                     SetTechnicalProperty(ref  _Id, value, RaiseIdChanged); } }     long _Id;
            [DebuggerHidden][DbMappedField("Session_Id")]                 public        long Session_Id        { [DebuggerStepThrough] get { return                                         _Session_Id; } [DebuggerStepThrough] set {                                                                              SetProperty(ref          _Session_Id, value); } }     long _Session_Id;
                                                                          public    DateTime Timestamp         { [DebuggerStepThrough] get { return                         _TimestampUTC.ToLocalTime(); } [DebuggerStepThrough] set {                                                                                   _TimestampUTC = value.ToUniversalTime(); } }
            [DebuggerHidden][DbMappedField("TimestampUTC")]               public    DateTime TimestampUTC      { [DebuggerStepThrough] get { return                                       _TimestampUTC; } [DebuggerStepThrough] set { value = value.TruncateUnderMilliSeconds();                                   SetProperty(ref        _TimestampUTC, value); } } DateTime _TimestampUTC;
                                                                          public       Level Level             { [DebuggerStepThrough] get { return                                              _Level; } [DebuggerStepThrough] set {                                                                              SetProperty(ref               _Level, value); } }    Level _Level;
            [DebuggerHidden][DbMappedField("Level")]                      private        int __Level           { [DebuggerStepThrough] get { return                                        _Level.Value; } [DebuggerStepThrough] set {                                   Level = Level.GetByValue(value).ThrowIfNull("This value does not match a known level!"); } }
            [DebuggerHidden][DbMappedField("ThreadId")]                   public         int ThreadId          { [DebuggerStepThrough] get { return                                           _ThreadId; } [DebuggerStepThrough] set {                                                                              SetProperty(ref            _ThreadId, value); } }      int _ThreadId;
                                                                          public        int? ParentThreadId    { [DebuggerStepThrough] get { return                                     _ParentThreadId; } [DebuggerStepThrough] set {                                                                                   SetProperty(ref _ParentThreadId, value); } }     int? _ParentThreadId;
            [DebuggerHidden][DbMappedField("ParentThreadId")]             private        int __ParentThreadId  { [DebuggerStepThrough] get { return                                _ParentThreadId ?? 0; } [DebuggerStepThrough] set {                                                                          ParentThreadId = value == 0 ? (int?)null : value; } }
            [DebuggerHidden][DbMappedField("Type"), DbMaxLength(1024)]    public      string Type              { [DebuggerStepThrough] get { return                                               _Type; } [DebuggerStepThrough] set {                                       ChkFieldLen(ref value, 1024);          SetProperty(ref                _Type, value); } }   string _Type;
            [DebuggerHidden][DbMappedField("Message")]                    public      string Message           { [DebuggerStepThrough] get { return                                            _Message; } [DebuggerStepThrough] set {                                                                              SetProperty(ref             _Message, value); } }   string _Message;
            [DebuggerHidden][DbMappedField("Tags", IsNullable = true)]    public      string Tags              { [DebuggerStepThrough] get { return                                               _Tags; } [DebuggerStepThrough] set {                                                                              SetProperty(ref                _Tags, value); } }   string _Tags;

            // Not stored in DB because stored in Session
            string ILog.Hostname { get; set; }

            protected override IdTuple<long> ClosedId
            {
                get { return new IdTuple<long>(Id); }
                set { Id = value.Id1; }
            }
    
            public Log()
               : this(true)
            {
            }
            public Log(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Type = string.Empty;
                Level = Level.Info;
            }

#pragma warning disable 809
            [Obsolete("Use ToFormatedMessage", true)]
            public override string ToString()
            {
                return this.ToFormatedMessage();
            }
#pragma warning restore 809

            #region Cloneable

            public new Log Clone() { return (Log)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new Log(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (Log)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Session_Id = from._Session_Id;
                _TimestampUTC = from._TimestampUTC;
                _Level = from._Level;
                _ThreadId = from._ThreadId;
                _ParentThreadId = from._ParentThreadId;
                _Type = from._Type;
                _Message = from._Message;
                _Tags = from._Tags;
            }
    
            #endregion
        }
    }
}
