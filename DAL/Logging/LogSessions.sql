CREATE TABLE [Logging].[LogSessions]
(
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Login_Id] [bigint] NOT NULL,
	[Hostname] [varchar](255) NOT NULL,
	[Ips] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)

GO

ALTER TABLE [Logging].[LogSessions]  WITH CHECK ADD  CONSTRAINT [FK__TMT_LogSessions__Logins__Login_Id] FOREIGN KEY([Login_Id])
REFERENCES [users].[Logins] ([Person_Id])
GO

ALTER TABLE [Logging].[LogSessions] CHECK CONSTRAINT [FK__TMT_LogSessions__Logins__Login_Id]
GO


