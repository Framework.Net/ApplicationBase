﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Logging
    {
        [DbMappedTable(schemaName: "Logging", tableName: "LogSessions")]
        public sealed partial class LogSession : BaseDTO<IdTuple<long>>, IDALObject_Base
        {
            [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]        public      long Id               { [DebuggerStepThrough] get { return       _Id; } [DebuggerStepThrough] set {                    SetTechnicalProperty(ref  _Id, value, RaiseIdChanged); } }     long _Id;
            [DebuggerHidden][DbMappedField("Login_Id")]                   public      long Login_Id         { [DebuggerStepThrough] get { return _Login_Id; } [DebuggerStepThrough] set {                                        SetProperty(ref _Login_Id, value); } }     long _Login_Id;
            [DebuggerHidden][DbMappedField("Hostname"), DbMaxLength(255)] public    string Hostname         { [DebuggerStepThrough] get { return _Hostname; } [DebuggerStepThrough] set { ChkFieldLen(ref value,  255);          SetProperty(ref _Hostname, value); } }   string _Hostname;
            [DebuggerHidden][DbMappedField("Ips")]                        public    string Ips              { [DebuggerStepThrough] get { return      _Ips; } [DebuggerStepThrough] set {                                        SetProperty(ref      _Ips, value); } }   string _Ips;

            protected override IdTuple<long> ClosedId
            {
                get { return new IdTuple<long>(Id); }
                set { Id = value.Id1; }
            }
    
            #region Cloneable

            public new LogSession Clone() { return (LogSession)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new LogSession(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (LogSession)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Login_Id = from._Login_Id;
                _Hostname = from._Hostname;
                _Ips = from._Ips;
            }
    
            #endregion
        }
    }
}
