CREATE TABLE [Logging].[Logs]
(
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Session_Id] [bigint] NOT NULL,
	[TimestampUTC] [datetime2](3) NOT NULL,
	[Level] [int] NOT NULL,
	[ThreadId] [int] NOT NULL,
	[ParentThreadId] [int] NOT NULL,
	[Type] [varchar](250) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[Tags] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)

GO

ALTER TABLE [Logging].[Logs]  WITH CHECK ADD  CONSTRAINT [FK__TMT_Logs__TMT_LogSessions__Session_Id] FOREIGN KEY([Session_Id])
REFERENCES [Logging].[LogSessions] ([Id])
GO

ALTER TABLE [Logging].[Logs] CHECK CONSTRAINT [FK__TMT_Logs__TMT_LogSessions__Session_Id]
GO


