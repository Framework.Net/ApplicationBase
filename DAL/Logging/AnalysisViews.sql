create VIEW [Logging].[TaskRuns] WITH SCHEMABINDING
AS
select L.[Id], [Session_Id], S.Login_Id
      ,[TimestampUTC]
      ,[Level]
      ,[ThreadId]
      ,[ParentThreadId]
      ,[Type]
      ,[Message]
      ,[Tags]
from Logging.Logs L 
inner join  Logging.LogSessions S on S.Id = L.Session_Id
where [Message] like 'Task % started with arguments%' 
   or [Message] like 'Task %ends with return value %'
GO

-- https://docs.microsoft.com/fr-fr/sql/relational-databases/views/create-indexed-views?view=sql-server-ver15
CREATE UNIQUE CLUSTERED INDEX [IX_TaskRuns__Id] ON [Logging].[TaskRuns]
(
	[Id] ASC
)
GO

CREATE VIEW [Logging].[TaskRunInfos] WITH SCHEMABINDING
as
select Session_Id, Login_Id,
       -- "Task ends..." was the old format, we nullify it so the max return the message with started... and regex works
       max(case when [Message] like 'Task ends%' then null else substring([Message], 7, charindex('"', [Message], 7) - 7) end) as TaskName,
	   max(cast(
	   -- If clr is available 
				case when clrTools.RegEx_IsMatch('^Task.*return value (.*)$',       [Message]) = 1
			    then clrTools.RegEx_Replace('^Task.*return value (.*)$', '$1', [Message])
	   -- else
				--TODO c'est quoi ce bbbccc ? rechecker �a...
				--case when [Message] like 'Task%return value (%)'
			    --then substring([Message], PATINDEX('%return value %', [Message]) + len('return value '), LEN([Message]) - PATINDEX('%bbbccc%', [Message]) - len('return value '))
	   -- end
			    else null end as int)) as ReturnValue,
       min(TimestampUTC) as StartDate, max(TimestampUTC) as EndDate
from Logging.TaskRuns WITH (NOEXPAND)
group by Session_Id, Login_Id
having count(*) = 2
GO


CREATE VIEW [Logging].[TaskErrorInfos] WITH SCHEMABINDING
AS
	select Id, 
		   Session_Id,
		   TimestampUTC, 
		   [Level], 
		   ThreadId, 
		   ParentThreadId, 
		   [Type],
		   [Tags], 
		   [Message]
	from Logging.Logs L
	where
	-- Exception Tags are always set before other developpers' tags.
	-- So we are able to use '...%' instead of '%...%'
	(   Tags like 'Dummy Silenced%'
	 or Tags like 'Business Check%'
	 or Tags like 'Technical Error%'
	 or Tags like 'Detected Bug%'
	 or Tags like 'Potential Detected Bug%'
	 or Tags like 'Unknown Bug%')

GO


CREATE VIEW [Logging].[BiggestLogGeneratorsForAutomatedTasks] WITH SCHEMABINDING
AS
select L.Type, count_big(*) as 'Count'
from Logging.Logs L  
inner join  Logging.LogSessions S on S.Id = L.Session_Id
where L.Id > 10896068 -- real business condition here is actually TimestampUTC > '2019-06-30T22:00:00' 
  and s.Login_Id = -1
group by L.Type
GO

create VIEW [Logging].[LogByTypesAndLevels] WITH SCHEMABINDING
AS
	select [Type],
	       [Level],
	       count_big(*) as 'Count'
	from Logging.Logs L
	group by [Type], [Level]

GO



create VIEW [Logging].[LogsBugIds] WITH SCHEMABINDING
AS
	select Id, 
		   Session_Id,
		   case when isnumeric(substring([Message], charindex('Bug Id:', [Message]) + 7, 18)) = 1
		        then cast(substring([Message], charindex('Bug Id:', [Message]) + 7, 18) as bigint)
		        else 0 end as [BugId]
	from Logging.Logs L
	where
	--Tags <> '' and -- To speed up because there is an index, and because it is expected to have always a tag when a bug is found anyway
	[Message] like '%Bug Id:%' 
GO



