﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Logging
    {
        [DbMappedTable(schemaName: "Logging", tableName: "TaskRunInfos")]
        public sealed partial class TaskRunInfo : BaseDTO<IdTuple<long>>, IDALObject_Base
        {
            [DebuggerHidden][DbMappedField("Session_Id")] public     long SessionId    { get; set; }
            [DebuggerHidden][DbMappedField("Login_Id")]   public     long LoginId      { get; set; }
            [DebuggerHidden][DbMappedField("TaskName")]   public   string TaskName     { get; set; }
            [DebuggerHidden][DbMappedField("StartDate")]  public DateTime StartDateUtc { get; set; }
            [DebuggerHidden][DbMappedField("EndDate")]    public DateTime EndDateUtc   { get; set; }

            public TimeSpan Duration { get { return EndDateUtc - StartDateUtc; } }

            protected override IdTuple<long> ClosedId
            {
                get { return new IdTuple<long>(SessionId); }
                set { SessionId = value.Id1; }
            }
    
            #region Cloneable

            public new TaskRunInfo Clone() { return (TaskRunInfo)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new TaskRunInfo(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (TaskRunInfo)source;
                SessionId = from.SessionId;
                LoginId = from.LoginId;
                TaskName = from.TaskName;
                StartDateUtc = from.StartDateUtc;
                EndDateUtc = from.EndDateUtc;
            }
    
            #endregion
        }
    }
}
