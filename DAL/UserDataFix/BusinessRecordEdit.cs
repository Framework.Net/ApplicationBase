﻿using System;
using System.Diagnostics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace UserDataFix
    {
        [DbMappedTable(schemaName: "UserDataFix", tableName: "BusinessRecordEdits")]
        public class BusinessRecordEdit : BaseDTO<IdTuple<int>>, IDALObject_Base, HasPropertiesOf.UserDataFix.BusinessRecordEdit
        {
            [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]                                     public      int Id                   { [DebuggerStepThrough] get { return                   _Id; } [DebuggerStepThrough] set {                                  SetProperty(ref                    _Id, value); } }      int _Id;
            [DebuggerHidden][DbMappedField("WhoId")]                                                   public     long WhoId                { [DebuggerStepThrough] get { return                _WhoId; } [DebuggerStepThrough] set {                                  SetProperty(ref                 _WhoId, value); } }     long _WhoId;
            [DebuggerHidden][DbMappedField("WhenUTC")]                                                 public DateTime WhenUTC              { [DebuggerStepThrough] get { return              _WhenUTC; } [DebuggerStepThrough] set { ChkRange(value, Ranges.WhenUTC); SetProperty(ref               _WhenUTC, value); } } DateTime _WhenUTC;
            // To encode decode, use ToAssemblyQualifiedStringWithoutVersion & ToTypeFromAssemblyQualifiedStringWithoutVersion
            [DebuggerHidden][DbMappedField("TypedId_TypeFullName"), DbMaxLength(500)]                  public   string TypedId_TypeFullName { [DebuggerStepThrough] get { return _TypedId_TypeFullName; } [DebuggerStepThrough] set {     ChkFieldLen(ref value, 500); SetProperty(ref  _TypedId_TypeFullName, value); } }   string _TypedId_TypeFullName;
            // To encode decode, use ToAssemblyQualifiedStringWithoutVersion & ToTypeFromAssemblyQualifiedStringWithoutVersion
            [DebuggerHidden][DbMappedField("TypedId_IdTuple_Type"), DbMaxLength(500)]                  public   string TypedId_IdTuple_Type { [DebuggerStepThrough] get { return _TypedId_IdTuple_Type; } [DebuggerStepThrough] set {     ChkFieldLen(ref value, 500); SetProperty(ref  _TypedId_IdTuple_Type, value); } }   string _TypedId_IdTuple_Type;
            [DebuggerHidden][DbMappedField("TypedId_Keys"), DbMaxLength(500)]                          public   string TypedId_Keys         { [DebuggerStepThrough] get { return         _TypedId_Keys; } [DebuggerStepThrough] set {     ChkFieldLen(ref value, 500); SetProperty(ref          _TypedId_Keys, value); } }   string _TypedId_Keys;
            // To encode decode, use ToAssemblyQualifiedStringWithoutVersion & ToTypeFromAssemblyQualifiedStringWithoutVersion
            [DebuggerHidden][DbMappedField("PropertyType"), DbMaxLength(500)]                          public   string PropertyType         { [DebuggerStepThrough] get { return         _PropertyType; } [DebuggerStepThrough] set {     ChkFieldLen(ref value, 500); SetProperty(ref          _PropertyType, value); } }   string _PropertyType;
            [DebuggerHidden][DbMappedField("PropertyName"), DbMaxLength(500)]                          public   string PropertyName         { [DebuggerStepThrough] get { return         _PropertyName; } [DebuggerStepThrough] set {     ChkFieldLen(ref value, 500); SetProperty(ref          _PropertyName, value); } }   string _PropertyName;
            [DebuggerHidden][DbMappedField("OldValue", IsNullable = true), DbMaxLength(500)]           public   string OldValueSerialized   { [DebuggerStepThrough] get { return             _OldValue; } [DebuggerStepThrough] set {     ChkFieldLen(ref value, 500); SetProperty(ref              _OldValue, value); } }   string _OldValue;
            [DebuggerHidden][DbMappedField("NewValue", IsNullable = true), DbMaxLength(500)]           public   string NewValueSerialized   { [DebuggerStepThrough] get { return             _NewValue; } [DebuggerStepThrough] set {     ChkFieldLen(ref value, 500); SetProperty(ref              _NewValue, value); } }   string _NewValue;

            string HasPropertiesOf.UserDataFix.BusinessRecordEdit.OldValue { get { return OldValueSerialized; } }
            string HasPropertiesOf.UserDataFix.BusinessRecordEdit.NewValue { get { return NewValueSerialized; } }

            public object OldValue { get { return Deserialize(OldValueSerialized); }  set { OldValueSerialized = Serialize(value); } } 
            public object NewValue { get { return Deserialize(NewValueSerialized); }  set { NewValueSerialized = Serialize(value); } } 

            object Deserialize(string value)
            {
                if (value == null)
                    return null;
                var t = PropertyType.ToTypeFromAssemblyQualifiedStringWithoutVersion();
                if (t == typeof(string))
                    return value;
                t = t.TryGetNullableType() ?? t;
                if (t.IsEnum)
                    return Enum.Parse(t, value, true);
                if (t.IsNumericType())
                    return Convert.ChangeType(value, t);
                if (t == typeof(bool))
                    return bool.Parse(value);
                if (t == typeof(DateTime))
                    return new DateTime(long.Parse(value));
                if (t == typeof(TimeSpan))
                    return new TimeSpan(long.Parse(value));
                if (typeof(DynamicEnum).IsAssignableFrom(t))
                    return DynamicEnum.GetValueFor(value, t, OwnerMapper);
                throw new TechnicalException($"Don't know how to interpret value \"{value}\" with type \"{t.ToAssemblyQualifiedStringWithoutVersion()}\"!", null);
            }
            internal string Serialize(object value)
            {
                if (value == null)
                    return null;
                if (value is string)
                    return (string)value;
                var t = PropertyType.ToTypeFromAssemblyQualifiedStringWithoutVersion();
                t = t.TryGetNullableType() ?? t;
                if (t.IsEnum)
                    return value.ToString();
                if (t.IsNumericType())
                    return value.ToStringInvariant();
                if (t == typeof(bool))
                    return value.ToStringInvariant();
                if (t == typeof(DateTime))
                    return ((DateTime)value).Ticks.ToStringInvariant();
                if (t == typeof(TimeSpan))
                    return ((TimeSpan)value).Ticks.ToStringInvariant();
                if (typeof(DynamicEnum).IsAssignableFrom(t))
                    return DynamicEnum.GetEnumId((DynamicEnum)value).ToStringInvariant();
                throw new TechnicalException($"Don't know how to interpret value \"{value}\" with type \"{t.ToAssemblyQualifiedStringWithoutVersion()}\"!", null);
            }

            protected override IdTuple<int> ClosedId
            {
               get { return new IdTuple<int>(Id); }
               set { Id = value.Id1; }
            }

            #region Cloneable

            public new BusinessRecordEdit Clone() { return (BusinessRecordEdit)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new BusinessRecordEdit(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (HasPropertiesOf.UserDataFix.BusinessRecordEdit)source;
                (this as ICopyable<HasPropertiesOf.UserDataFix.BusinessRecordEdit>).CopyFrom(from);
            }
            void ICopyable<HasPropertiesOf.UserDataFix.BusinessRecordEdit>.CopyFrom(HasPropertiesOf.UserDataFix.BusinessRecordEdit source)
            {
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _WhoId = source.WhoId;
                _WhenUTC = source.WhenUTC;
                _TypedId_TypeFullName = source.TypedId_TypeFullName;
                _TypedId_IdTuple_Type = source.TypedId_IdTuple_Type;
                _TypedId_Keys = source.TypedId_Keys;
                _PropertyType = source.PropertyType;
                _PropertyName = source.PropertyName;
                _OldValue = source.OldValue;
                _NewValue = source.NewValue;
            }

            #endregion

            public static class Ranges
            {
                public static readonly RangeOf<DateTime> WhenUTC = new RangeOf<DateTime>(new DateTime(0),new DateTime(3155378975999990000)); // from 0001-01-01T00:00:00.0000000 to 9999-12-31T23:59:59.9990000
            }
        }
    }
    namespace HasPropertiesOf.UserDataFix
    {
        public interface BusinessRecordEdit : IHasTypedIdReadable, ICopyable<BusinessRecordEdit>//, IComparable<BusinessRecordEdit>, IComparable
        {
            new int  Id                   { get; }
            // BusinessRecordEdit are not editable so no "set" here
            long     WhoId                { get; }
            DateTime WhenUTC              { get; }
            string   TypedId_TypeFullName { get; }
            string   TypedId_IdTuple_Type { get; }
            string   TypedId_Keys         { get; }
            string   PropertyType         { get; }
            string   PropertyName         { get; }
            string   OldValue             { get; }
            string   NewValue             { get; }
        }
    }
}
