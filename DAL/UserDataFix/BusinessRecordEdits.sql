CREATE TABLE [UserDataFix].[BusinessRecordEdits]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WhoId] [bigint] NOT NULL,
	[WhenUTC] [datetime2](0) NOT NULL,
	[TypedId_TypeFullName] [nvarchar](500) NOT NULL,
	[TypedId_IdTuple_Type] [nvarchar](500) NOT NULL,
	[TypedId_Keys] [nvarchar](500) NOT NULL,
	[PropertyType] [nvarchar](500) NOT NULL,
	[PropertyName] [nvarchar](500) NOT NULL,
	[NewValue] [nvarchar](500) NULL,
	[OldValue] [nvarchar](500) NULL,
 CONSTRAINT [PK__BusinessRecordEdits] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)

GO

ALTER TABLE [UserDataFix].[BusinessRecordEdits]  WITH CHECK ADD  CONSTRAINT [FK__BusinessRecordEdits__WhoId] FOREIGN KEY([WhoId])
REFERENCES [users].[Logins] ([Person_Id])
GO

ALTER TABLE [UserDataFix].[BusinessRecordEdits] CHECK CONSTRAINT [FK__BusinessRecordEdits__WhoId]
GO


