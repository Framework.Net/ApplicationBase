﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace UsersPreferences
    {
        [DbMappedTable(schemaName: "UsersPreferences", tableName: "Settings")]
        public sealed partial class UserSetting : BaseDTO<IdTuple<long, string>>, IDALObject_Base
        {
            [DebuggerHidden][DbMappedField("Person_Id",    IsPK = true, KeyOrderIndex = 1)]                     public   long Person_Id                     { [DebuggerStepThrough] get { return                     _Person_Id; } [DebuggerStepThrough] set {                               SetProperty(ref                      _Person_Id, value); } }   long _Person_Id;
            [DebuggerHidden][DbMappedField("PropertyName", IsPK = true, KeyOrderIndex = 2), DbMaxLength(255)]   public string PropertyName                  { [DebuggerStepThrough] get { return                  _PropertyName; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 255); SetProperty(ref                   _PropertyName, value); } } string _PropertyName;
            [DebuggerHidden][DbMappedField("PropertyValue", IsNullable = true)]                                 public string PropertyValue                 { [DebuggerStepThrough] get { return                 _PropertyValue; } [DebuggerStepThrough] set {                               SetProperty(ref                  _PropertyValue, value); } } string _PropertyValue;
            [DebuggerHidden][DbMappedField("PropertyType"), DbMaxLength(255)]                                   public string PropertyType                  { [DebuggerStepThrough] get { return                  _PropertyType; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 255); SetProperty(ref                   _PropertyType, value); } } string _PropertyType;
            [DebuggerHidden][DbMappedField("PropertyAvailableValueForList", IsNullable = true)]                 public string PropertyAvailableValueForList { [DebuggerStepThrough] get { return _PropertyAvailableValueForList; } [DebuggerStepThrough] set {                               SetProperty(ref  _PropertyAvailableValueForList, value); } } string _PropertyAvailableValueForList;

            protected override IdTuple<long, string> ClosedId
            {
                get
                {
                    return new IdTuple<long, string>(Person_Id, PropertyName);
                }
                set
                {
                    Person_Id = value.Id1;
                    PropertyName = value.Id2;
                }
            }

            public UserSetting()
               : this(true)
            {
            }
            public UserSetting(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _PropertyName = string.Empty;
                _PropertyType = string.Empty;
            }
    
            #region Cloneable
    
            public new UserSetting Clone() { return (UserSetting)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new UserSetting(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (UserSetting)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Person_Id = from._Person_Id;
                _PropertyName = from._PropertyName;
                _PropertyValue = from._PropertyValue;
                _PropertyType = from._PropertyType;
                _PropertyAvailableValueForList = from._PropertyAvailableValueForList;
            }
    
            #endregion
        }
    }
}
