CREATE TABLE [UsersPreferences].[Settings]
(
	[Person_Id] [bigint] NOT NULL,
	[PropertyName] [nvarchar](255) NOT NULL,
	[PropertyValue] [nvarchar](max) NULL,
	[PropertyType] [nvarchar](255) NOT NULL,
	[PropertyAvailableValueForList] [nvarchar](max) NULL,
 CONSTRAINT [PK__UsersPreferences_Settings] PRIMARY KEY CLUSTERED 
(
	[Person_Id] ASC,
	[PropertyName] ASC
)
)
GO

ALTER TABLE [UsersPreferences].[Settings]  WITH CHECK ADD  CONSTRAINT [FK__Settings__Persons__Person_Id] FOREIGN KEY([Person_Id])
REFERENCES [users].[Persons] ([Id_Person])
GO

ALTER TABLE [UsersPreferences].[Settings] CHECK CONSTRAINT [FK__Settings__Persons__Person_Id]
GO

