﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace UsersPreferences
    {
        [DbMappedTable(schemaName: "UsersPreferences", tableName: "UserGridLayouts")]
        public sealed partial class UserGridLayout : BaseDTO<IdTuple<long, string, string>>, IDALObject_Base
        {
            [DebuggerHidden][DbMappedField("Person_Id",     IsPK = true, KeyOrderIndex = 1)]                   public   long    Person_Id     { [DebuggerStepThrough] get { return     _Person_Id; } [DebuggerStepThrough] set {  SetProperty(ref    _Person_Id,  value); } }      long _Person_Id;
            [DebuggerHidden][DbMappedField("GridKey"  ,     IsPK = true, KeyOrderIndex = 2), DbMaxLength(692)] public string    GridKey       { [DebuggerStepThrough] get { return       _GridKey; } [DebuggerStepThrough] set {  SetProperty(ref      _GridKey,  value); } }    string _GridKey;
            [DebuggerHidden][DbMappedField("LayoutName",    IsPK = true, KeyOrderIndex = 3), DbMaxLength(100)] public string    LayoutName    { [DebuggerStepThrough] get { return    _LayoutName; } [DebuggerStepThrough] set {  SetProperty(ref   _LayoutName,  value); } }    string _LayoutName;
            [DebuggerHidden][DbMappedField("DefaultSince")]                                                    public DateTime? DefaultSince  { [DebuggerStepThrough] get { return  _DefaultSince; } [DebuggerStepThrough] set {  SetProperty(ref _DefaultSince,  value); } } DateTime? _DefaultSince;
            
            [DebuggerHidden][DbMappedField("LayoutContent")]                                                   public byte[]    LayoutContent { [DebuggerStepThrough] get { return _LayoutContent; } [DebuggerStepThrough] set {  SetProperty(ref _LayoutContent, value); } } byte[] _LayoutContent;

            protected override IdTuple<long, string, string> ClosedId
            {
                get
                {
                    return new IdTuple<long, string, string>(Person_Id, GridKey, LayoutName);
                }
                set
                {
                    Person_Id = value.Id1;
                    GridKey = value.Id2;
                    LayoutName = value.Id3;
                }
            }

            //public UserGridLayout()
            //   : this(true)
            //{
            //}
            //public UserGridLayout(bool initializeModelValues)
            //{
            //    if (!initializeModelValues)
            //        return;
            //}
    
            #region Cloneable
    
            public new UserGridLayout Clone() { return (UserGridLayout)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new UserGridLayout(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (UserGridLayout)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Person_Id = from._Person_Id;
                _GridKey = from._GridKey;
                _LayoutName = from._LayoutName;
                _DefaultSince = from._DefaultSince;
                _LayoutContent = from._LayoutContent;
            }
    
            #endregion
        }
    }
}
