CREATE TABLE [UsersPreferences].[UserGridLayouts]
(
	[Person_Id] [bigint] NOT NULL,
	[GridKey] [varchar](300) NOT NULL,
	[LayoutName] [varchar](300) NOT NULL,
	[DefaultSince] [datetime2](2) NULL,
	[LayoutContent] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_UserGridLayouts] PRIMARY KEY CLUSTERED 
(
	[Person_Id] ASC,
	[GridKey] ASC,
	[LayoutName] ASC
)
)

GO


