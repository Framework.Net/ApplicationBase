﻿using System;
using System.Collections.Generic;
using TechnicalTools.Model.Cache;


namespace ApplicationBase.DAL
{
    public interface IHavingDynamicRule : IDALObject_Base, IHasTypedIdReadable
    {
        bool Disabled                         { get; set; }
        /// <summary> 
        /// Contain the Type (C#) on which the rule must be applied. 
        /// In case you want to store this in a database, look for these methods to handle easily conversions to/from string type.
        /// <see cref="TechnicalTools.Type_Extensions.ToTypeFromAssemblyQualifiedStringWithoutVersion(string)"/>
        /// <see cref="TechnicalTools.Type_Extensions.ToAssemblyQualifiedStringWithoutVersion(Type)"/>
        /// </summary>
        Type   TypeOnWhichRuleApplies         { get; set; }
        string DynamicMatchingRule            { get; set; }     // Real C# Code or another format later
        event Action DynamicMatchingRuleChanged;                // Raised when DynamicMatchingRule or TypeOnWhichRuleApplies or DefaultResult changed
        bool   DefaultResult                  { get; }          // Default result of the rule when it is empty
        
        // UI / Business representation of DynamicMatchingRule. Must be synchronized in UI layer
        string DynamicMatchingRuleView        { get; set; }     // language specific to UI (same meaning as DynamicMatchingRule) or NULL
    }

    public interface IDynamicRuleSet
    {
        IReadOnlyDictionary<IHavingDynamicRule, int> GetProductionRuleSet();
        IReadOnlyDictionary<IHavingDynamicRule, int> GetEditingRuleSet();
        bool RulesMustMatchEvaluatedItemType { get; }
        string GetResult(IHavingDynamicRule rule);
    }
}
