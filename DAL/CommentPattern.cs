using System;
using System.Diagnostics;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using DataMapper;

using ICommentPattern = ApplicationBase.DAL.HasPropertiesOf.Tools.CommentPattern;

namespace ApplicationBase.DAL
{
    namespace Tools
    { 
        [DbMappedTable(schemaName: "Tools", tableName:"CommentPatterns")]
        public sealed class CommentPattern : BaseDTO<IdTuple<int>>, IDALObject_Base, ICommentPattern
        {
            [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]           public    int      Id                            { [DebuggerStepThrough] get { return                        _Id; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref                        _Id, value, RaiseIdChanged); } }         int _Id;
            [DebuggerHidden][DbMappedField("SetId")]                         public byte        SetId                         { [DebuggerStepThrough] get { return                     _SetId; } [DebuggerStepThrough] set {            SetProperty(ref                     _SetId, value);                 } }        byte _SetId;
            [DebuggerHidden][DbMappedField("Pattern")]                       public string      Pattern                       { [DebuggerStepThrough] get { return                   _pattern; } [DebuggerStepThrough] set {            SetProperty(ref                   _pattern, value);                 } }      string _pattern;
            [DebuggerHidden][DbMappedField("PreventAutomaticTreatment")]     public bool        PreventAutomaticTreatment     { [DebuggerStepThrough] get { return _PreventAutomaticTreatment; } [DebuggerStepThrough] set {            SetProperty(ref _PreventAutomaticTreatment, value);                 } }        bool _PreventAutomaticTreatment;
            [DebuggerHidden][DbMappedField("KeyWord"), DbMaxLength(255)]     public string      KeyWord                       { [DebuggerStepThrough] get { return                   _KeyWord; } [DebuggerStepThrough] set {            SetProperty(ref                   _KeyWord, value);                 } }      string _KeyWord;

            protected override IdTuple<int> ClosedId
            {
                get { return new IdTuple<int>(Id); }
                set { Id = value.Id1; }
            }

            public CommentPattern()
                : this(true)
            {
            }
            public CommentPattern(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _pattern = string.Empty;
                _KeyWord = string.Empty;
            }

            #region Cloneable

            public new CommentPattern Clone() { return (CommentPattern)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new CommentPattern(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (ICommentPattern)source;
                CopyFrom(from);
            }
            public void CopyFrom(ICommentPattern source)
            {
                Id = source.Id; // Allow EditableDToList to work (saving changes)
                _SetId = source.SetId;
                _pattern = source.Pattern;
                _PreventAutomaticTreatment = source.PreventAutomaticTreatment;
                _KeyWord = source.KeyWord;
            }

            #endregion
        }
    }
    
    namespace HasPropertiesOf.Tools
    {
        public interface CommentPattern : IHasTypedIdReadable, ICopyable<CommentPattern>
        {
            new int     Id                        { get; }
               byte     SetId                     { get; set; }
             string     Pattern                   { get; set; }
               bool     PreventAutomaticTreatment { get; set; }
             string     KeyWord                   { get; set; }
        }
    }
}
