﻿using System;
using System.Diagnostics;

using TechnicalTools;
using TechnicalTools.Model.Cache;

using DataMapper;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace ApplicationBase.DAL.Reportings
{
    [DbMappedTable(schemaName: "Reportings", tableName: "SeriePoints")]
    public sealed partial class SeriePoint : BaseDTO<IdTuple<int>>
    {
        [DebuggerHidden][DbMappedField("Id_Point", IsAutoPK = true)]                                       public int    Id             { get; set; }
        [DebuggerHidden][DbMappedField("Serie_Id", FkTo = typeof(Serie))]                                  public int    Serie_Id       { get; set; }
        [DebuggerHidden][DbMappedField("Argument"), DbMaxLength(256, MinimumLength = 256)]                 public string ArgumentAsText { get; private set; }
        [DebuggerHidden][DbMappedField("Value", IsNullable = true), DbMaxLength(256, MinimumLength = 256)] public string ValueAsText    { get; private set; }
        [DebuggerHidden][DbMappedField("Color", IsNullable = true)]                                        public int?   Color          { get; set; }

        protected override IdTuple<int> ClosedId
        {
            get { return new IdTuple<int>(Id); }
            set { Id = value.Id1; }
        }

        #region Cloneable

        public new SeriePoint Clone() { return (SeriePoint)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new SeriePoint(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            var from = (SeriePoint)source;
            // Note : Les champs appartenant à la PK ne sont pas copiés

            Id = from.Id;
            ArgumentAsText = from.ArgumentAsText;
            ValueAsText = from.ValueAsText;
            Color = from.Color;
        }

        #endregion

        #region For single value

        public void SetArgument(string str)                       { ArgumentAsText = str; }
        public void SetArgument(decimal d)                        { ArgumentAsText = d.ToStringInvariant(); }
        public void SetArgument(double d)                         { ArgumentAsText = d.ToStringInvariant(); }
        public void SetArgument(float f)                          { ArgumentAsText = f.ToStringInvariant(); }
        public void SetArgument(int i)                            { ArgumentAsText = i.ToStringInvariant(); }
        public void SetArgument(uint ui)                          { ArgumentAsText = ((int)ui).ToStringInvariant(); }
        public void SetArgument(long l)                           { ArgumentAsText = l.ToStringInvariant(); }
        public void SetArgument(ulong ul)                         { ArgumentAsText = ((long)ul).ToStringInvariant(); }
        public void SetArgument(short s)                          { ArgumentAsText = s.ToStringInvariant(); }
        public void SetArgument(ushort us)                        { ArgumentAsText = us.ToStringInvariant(); }
        public void SetArgument(byte b)                           { ArgumentAsText = b.ToStringInvariant(); }
        public void SetArgument(sbyte sb)                         { ArgumentAsText = sb.ToStringInvariant(); }
        public void SetArgument(DateTime d, eSerieAxisDataType t = eSerieAxisDataType.DateTime) { ArgumentAsText = Convert(d, t); }
        public void SetArgument(TimeSpan ts)                      { ArgumentAsText = Convert(ts); }
        public void SetArgument(object o)                         { if (o == null) { ArgumentAsText = ""; return; }
                                                                    if (!_mSetArguments.TryGetValue(o.GetType(), out var m))
                                                                        throw new ArgumentException("The underlying type is not handled!", nameof(o));
                                                                    m.Invoke(this, new[] { o });
                                                                  }
        static Dictionary<Type, MethodInfo> _mSetArguments = typeof(SeriePoint).GetMethods()
                                                                               .Where(m => m.Name == nameof(SetArgument))
                                                                               .Where(m => m.GetParameters().First().ParameterType != typeof(object))
                                                                               .ToDictionary(m => m.GetParameters().First().ParameterType.RemoveNullability());

        public void SetValue(string str)                          { ValueAsText = str; }
        public void SetValue(decimal d)                           { ValueAsText = d.ToStringInvariant(); }
        public void SetValue(double d)                            { ValueAsText = d.ToStringInvariant(); }
        public void SetValue(float f)                             { ValueAsText = f.ToStringInvariant(); }
        public void SetValue(int i)                               { ValueAsText = i.ToStringInvariant(); }
        public void SetValue(uint ui)                             { ValueAsText = ((int)ui).ToStringInvariant(); }
        public void SetValue(long l)                              { ValueAsText = l.ToStringInvariant(); }
        public void SetValue(ulong ul)                            { ValueAsText = ((long)ul).ToStringInvariant(); }
        public void SetValue(short s)                             { ValueAsText = s.ToStringInvariant(); }
        public void SetValue(ushort us)                           { ValueAsText = us.ToStringInvariant(); }
        public void SetValue(byte b)                              { ValueAsText = b.ToStringInvariant(); }
        public void SetValue(sbyte sb)                            { ValueAsText = sb.ToStringInvariant(); }
        public void SetValue(DateTime d, eSerieAxisDataType t = eSerieAxisDataType.DateTime)    { ValueAsText = Convert(d, t); }
        public void SetValue(TimeSpan ts)                         { ValueAsText = Convert(ts); }
        public void SetValue(object o)                            { if (o == null) { ValueAsText = ""; return; }
                                                                    if (!_mSetValues.TryGetValue(o.GetType(), out var m))
                                                                        throw new ArgumentException("The underlying type is not handled!", nameof(o));
                                                                    m.Invoke(this, new[] { o });
                                                                  }
        static Dictionary<Type, MethodInfo> _mSetValues = typeof(SeriePoint).GetMethods()
                                                                            .Where(m => m.Name == nameof(SetValue))
                                                                            .Where(m => m.GetParameters().First().ParameterType != typeof(object))
                                                                            .ToDictionary(m => m.GetParameters().First().ParameterType.RemoveNullability());
        #endregion For single value

        #region For Intervals

        public void SetArgumentInterval(string str1, string str2)                          { ArgumentAsText = str1 + "|" + str2; }
        public void SetArgumentInterval(decimal? d1, decimal? d2)                          { ArgumentAsText = d1?.ToStringInvariant() + "|" + d2?.ToStringInvariant(); }
        public void SetArgumentInterval(double? d1, double? d2)                            { ArgumentAsText = d1?.ToStringInvariant() + "|" + d2?.ToStringInvariant(); }
        public void SetArgumentInterval(float? f1, float? f2)                              { ArgumentAsText = f1?.ToStringInvariant() + "|" + f2?.ToStringInvariant(); }
        public void SetArgumentInterval(int? i1, int? i2)                                  { ArgumentAsText = i1?.ToStringInvariant() + "|" + i2?.ToStringInvariant(); }
        public void SetArgumentInterval(uint? ui1, uint? ui2)                              { ArgumentAsText = (ui1 == null ? "" : ((int)ui1).ToStringInvariant()) + "|" + (ui2 == null ? "" : ((int)ui2).ToStringInvariant()); }
        public void SetArgumentInterval(long? l1, long? l2)                                { ArgumentAsText = l1?.ToStringInvariant() + "|" + l2?.ToStringInvariant(); }
        public void SetArgumentInterval(ulong? ul1, ulong? ul2)                            { ArgumentAsText = (ul1 == null ? "" : ((long)ul1).ToStringInvariant()) + "|" + (ul2 == null ? "" : ((long)ul2).ToStringInvariant()); }
        public void SetArgumentInterval(short? s1, short? s2)                              { ArgumentAsText = s1?.ToStringInvariant() + "|" + s2?.ToStringInvariant(); }
        public void SetArgumentInterval(ushort? us1, ushort? us2)                          { ArgumentAsText = us1?.ToStringInvariant() + "|" + us2?.ToStringInvariant(); }
        public void SetArgumentInterval(byte? b1, byte? b2)                                { ArgumentAsText = b1?.ToStringInvariant() + "|" + b2?.ToStringInvariant(); }
        public void SetArgumentInterval(sbyte? sb1, sbyte? sb2)                            { ArgumentAsText = sb1?.ToStringInvariant() + "|" + sb2?.ToStringInvariant(); }
        public void SetArgumentInterval(DateTime? d1, DateTime? d2, eSerieAxisDataType t = eSerieAxisDataType.DateTime)  { ArgumentAsText = (d1 == null ? "" : Convert(d1.Value, t)) + "|" + (d2 == null ? "" : Convert(d2.Value, t)); }
        public void SetArgumentInterval(TimeSpan? ts1, TimeSpan? ts2)                      { ArgumentAsText = (ts1 == null ? "" : Convert(ts1.Value)) + "|" + (ts2 == null ? "" : Convert(ts2.Value)); }
        public void SetArgumentInterval(object o1, object o2)                              { var t1 = o1?.GetType(); var t2 = o2?.GetType();
                                                                                             var tt = t1 ?? t2;
                                                                                             if (tt == null) { ArgumentAsText = "|"; return; }
                                                                                             if (t1 != null && t2 != null && t1 != t2)
                                                                                                 throw new ArgumentException("Type of interval bounds do not match!");
                                                                                             if (!_mSetArgumentInterval.TryGetValue(tt, out var m))
                                                                                                 throw new ArgumentException($"The underlying type {tt} is not handled!");
                                                                                             m.Invoke(this, new[] { o1, o2 });
                                                                                           }
        static Dictionary<Type, MethodInfo> _mSetArgumentInterval = typeof(SeriePoint).GetMethods()
                                                                                      .Where(m => m.Name == nameof(SetArgumentInterval))
                                                                                      .Where(m => m.GetParameters().First().ParameterType != typeof(object))
                                                                                      .ToDictionary(m => m.GetParameters().First().ParameterType.RemoveNullability());


        public void SetValueInterval(string str1, string str2)                          { ValueAsText = str1 + "|" + str2; }
        public void SetValueInterval(decimal? d1, decimal? d2)                          { ValueAsText = d1?.ToStringInvariant() + "|" + d2?.ToStringInvariant(); }
        public void SetValueInterval(double? d1, double? d2)                            { ValueAsText = d1?.ToStringInvariant() + "|" + d2?.ToStringInvariant(); }
        public void SetValueInterval(float? f1, float? f2)                              { ValueAsText = f1?.ToStringInvariant() + "|" + f2?.ToStringInvariant(); }
        public void SetValueInterval(int? i1, int? i2)                                  { ValueAsText = i1?.ToStringInvariant() + "|" + i2?.ToStringInvariant(); }
        public void SetValueInterval(uint? ui1, uint? ui2)                              { ValueAsText = (ui1 == null ? "" : ((int)ui1).ToStringInvariant()) + "|" + (ui2 == null ? "" : ((int)ui2).ToStringInvariant()); }
        public void SetValueInterval(long? l1, long? l2)                                { ValueAsText = l1?.ToStringInvariant() + "|" + l2?.ToStringInvariant(); }
        public void SetValueInterval(ulong? ul1, ulong? ul2)                            { ValueAsText = (ul1 == null ? "" : ((long)ul1).ToStringInvariant()) + "|" + (ul2 == null ? "" : ((long)ul2).ToStringInvariant()); }
        public void SetValueInterval(short? s1, short? s2)                              { ValueAsText = s1?.ToStringInvariant() + "|" + s2?.ToStringInvariant(); }
        public void SetValueInterval(ushort? us1, ushort? us2)                          { ValueAsText = us1?.ToStringInvariant() + "|" + us2?.ToStringInvariant(); }
        public void SetValueInterval(byte? b1, byte? b2)                                { ValueAsText = b1?.ToStringInvariant() + "|" + b2?.ToStringInvariant(); }
        public void SetValueInterval(sbyte? sb1, sbyte? sb2)                            { ValueAsText = sb1?.ToStringInvariant() + "|" + sb2?.ToStringInvariant(); }
        public void SetValueInterval(DateTime? d1, DateTime? d2, eSerieAxisDataType t = eSerieAxisDataType.DateTime)  { ValueAsText = (d1 == null ? "" : Convert(d1.Value, t)) + "|" + (d2 == null ? "" : Convert(d2.Value, t)); }
        public void SetValueInterval(TimeSpan? ts1, TimeSpan? ts2)                      { ValueAsText = (ts1 == null ? "" : Convert(ts1.Value)) + "|" + (ts2 == null ? "" : Convert(ts2.Value)); }
        public void SetValueInterval(object o1, object o2)                              { var t1 = o1?.GetType(); var t2 = o2?.GetType();
                                                                                          var tt = t1 ?? t2;
                                                                                          if (tt == null) { ValueAsText = "|"; return; }
                                                                                          if (t1 != null && t2 != null && t1 != t2)
                                                                                              throw new ArgumentException("Type of interval bounds do not match!");
                                                                                          if (!_mSetValueInterval.TryGetValue(tt, out var m))
                                                                                              throw new ArgumentException($"The underlying type {tt} is not handled!");
                                                                                          m.Invoke(this, new[] { o1, o2 });
                                                                                        }
        static Dictionary<Type, MethodInfo> _mSetValueInterval = typeof(SeriePoint).GetMethods()
                                                                                   .Where(m => m.Name == nameof(SetValueInterval))
                                                                                   .Where(m => m.GetParameters().First().ParameterType != typeof(object))
                                                                                   .ToDictionary(m => m.GetParameters().First().ParameterType.RemoveNullability());

        #endregion For Intervals

        static string Convert(DateTime d, eSerieAxisDataType t)
        {
            switch (t)
            {
                case eSerieAxisDataType.Date: return d.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                case eSerieAxisDataType.Time: return d.TimeOfDay.ToString("G", CultureInfo.InvariantCulture);
                case eSerieAxisDataType.DateTime: return d.TimeOfDay.ToString("o", CultureInfo.InvariantCulture);
                default: throw new ArgumentException("This value is not valid", nameof(t));
            }
        }
        static string Convert(TimeSpan ts)
        {
            return ts.ToString("G", CultureInfo.InvariantCulture);
        }
    }
}

