﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL.Reportings
{
    [DbMappedTable(schemaName: "Reportings", tableName: "Charts")]
    public sealed partial class Chart : BaseDTO<IdTuple<int>>
    {
        [DebuggerHidden][DbMappedField("Id_Chart", IsAutoPK = true)]        public int      Id                { get; set; }
        [DebuggerHidden][DbMappedField("BusinessModule"), DbMaxLength(256)] public string   BusinessModule    { get; set; }
        [DebuggerHidden][DbMappedField("Title"), DbMaxLength(256)]          public string   Title             { get; set; }
        [DebuggerHidden][DbMappedField("GenerationDateUTC")]                public DateTime GenerationDateUTC { get; set; }

        protected override IdTuple<int> ClosedId
        {
            get { return new IdTuple<int>(Id); }
            set { Id = value.Id1; }
        }


        #region Cloneable

        public new Chart Clone() { return (Chart)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new Chart(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            var from = (Chart)source;
            // Note : Les champs appartenant à la PK ne sont pas copiés

            BusinessModule = from.BusinessModule;
            Title = from.Title;
            GenerationDateUTC = from.GenerationDateUTC;
        }
    
        #endregion
   
    }
}
