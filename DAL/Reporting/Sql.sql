﻿create schema Reportings
GO
--GRANT INSERT, SELECT,  DELETE ON SCHEMA :: Reportings
--to app_name_admin
--GRANT SELECT  ON SCHEMA :: Reportings
--to app_name_user, app_name_user_readonly


CREATE  TABLE [Reportings].[Series]
(
	[Id_Serie] int PRIMARY KEY IDENTITY(0, 1) NOT NULL,
	[Title] nvarchar(256) NOT NULL,
	[SerieType] tinyint NOT NULL,
	GenerationDateUTC datetime2(0) NOT NULL,
	ArgumentDataType tinyint NOT NULL,
	ArgumentIntervalInfo tinyint NOT NULL,
	ValueDataType tinyint NOT NULL,
	ValueIntervalInfo tinyint NOT NULL,
	Color int NULL
)

CREATE  TABLE [Reportings].[SeriePoints]
(
    [Id_Point] int PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	[Serie_Id] int not null,
	Argument nchar(256) NOT NULL,
	Value nchar(256) NULL,
	Color int NULL
)
ALTER TABLE [Reportings].[SeriePoints]  WITH CHECK ADD FOREIGN KEY([Serie_Id])
REFERENCES [Reportings].[Series] ([Id_Serie])

create TABLE [Reportings].[Charts]
(
	[Id_Chart] int PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	[BusinessModule] nvarchar(256) NOT NULL,
	[Title] nvarchar(256) NOT NULL,
	GenerationDateUTC datetime2(0) NOT NULL,
)
ALTER TABLE [Reportings].[Charts]
ADD CONSTRAINT Reportings___Charts__BusinessModule_Title UNIQUE(BusinessModule, Title);

CREATE TABLE [Reportings].[ChartSeries]
(
	[Chart_Id] int NOT NULL,
	[Serie_Id] int NOT NULL,
)
ALTER TABLE [Reportings].[ChartSeries]
ADD CONSTRAINT Reportings___ChartSeries__Chart_Id_Serie_Id UNIQUE([Chart_Id], [Serie_Id]);

ALTER TABLE [Reportings].[ChartSeries]  WITH CHECK ADD FOREIGN KEY([Chart_Id])
REFERENCES [Reportings].[Charts] ([Id_Chart])
ALTER TABLE [Reportings].[ChartSeries]  WITH CHECK ADD FOREIGN KEY([Serie_Id])
REFERENCES [Reportings].[Series] ([Id_Serie])

