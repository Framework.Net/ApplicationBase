﻿using System;
using System.Diagnostics;

using TechnicalTools;
using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL.Reportings
{
    [DbMappedTable(schemaName: "Reportings", tableName: "Series")]
    public sealed partial class Serie : BaseDTO<IdTuple<int>>
    {
        [DebuggerHidden][DbMappedField("Id_Serie", IsAutoPK = true)] public int                Id                    { get; set; }
        [DebuggerHidden][DbMappedField("Title"), DbMaxLength(256)]   public string             Title                 { get; set; }
        [DebuggerHidden][DbMappedField("SerieType")]                 public eSerieType         SerieType             { get; set; }
        [DebuggerHidden][DbMappedField("GenerationDateUTC")]         public DateTime           GenerationDateUTC     { get; set; }
        [DebuggerHidden][DbMappedField("ArgumentDataType")]          public eSerieAxisDataType ArgumentDataType      { get; set; }
        [DebuggerHidden][DbMappedField("ArgumentIntervalInfo")]      public eSerieAxisInterval ArgumentIntervalInfo  { get; set; }
        [DebuggerHidden][DbMappedField("ValueDataType")]             public eSerieAxisDataType ValueDataType         { get; set; }
        [DebuggerHidden][DbMappedField("ValueIntervalInfo")]         public eSerieAxisInterval ValueIntervalInfo     { get; set; }
        [DebuggerHidden][DbMappedField("Color", IsNullable = true)]  public int?               Color                 { get; set; }

        protected override IdTuple<int> ClosedId
        {
            get { return new IdTuple<int>(Id); }
            set { Id = value.Id1; }
        }

        #region Cloneable

        public new Serie Clone() { return (Serie)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new Serie(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            var from = (Serie)source;
            // Note : Les champs appartenant à la PK ne sont pas copiés

            Title = from.Title;
            SerieType = from.SerieType;
            GenerationDateUTC = from.GenerationDateUTC;
            ArgumentDataType = from.ArgumentDataType;
            ArgumentIntervalInfo = from.ArgumentIntervalInfo;
            ValueDataType = from.ValueDataType;
            ValueIntervalInfo = from.ValueIntervalInfo;
            Color = from.Color;
        }

        #endregion

        public static class Ranges
        {
            public static readonly RangeOf<DateTime> GenerationDate = new RangeOf<DateTime>(new DateTime(0), new DateTime(3155378975999990000)); // from 0001-01-01T00:00:00.0000000 to 9999-12-31T23:59:59.9990000
        }
    }

    public enum eSerieType : byte
    {
        Unknown = 0,
        Bar = 1,
        Grid = 1
    }

    public enum eSerieAxisDataType : byte
    {
        Unknown = 0,
        String = 1,
        Decimal = 2,
        Integer = 4,
        BigInteger = 8,
        Date = 16,
        Time = 32,
        DateTime = 64,
    }
    [Flags]
    public enum eSerieAxisInterval : byte
    {
        NotInterval = 0,
        // This is the only value that kind be merged with other, 
        // when true this is the character '|' that is used to separate value, 
        // value before and after | can be empty text, that means the intervals is open.
        // Example "42|" means 42 to infinite. "|" is technically possible, yet not really useful for anyone
        LowerBoundIncluded = 1,
        LowerBoundExcluded = 2,
        UpperBoundIncluded = 4,
        UpperBoundExcluded = 8
    }
    public static class eSerieAxisDataType_Extensions
    {
        public static eSerieAxisDataType ToSerieAxisDataType(this Type t)
        {
            t = t.RemoveNullability();
            if (t == typeof(string))
                return eSerieAxisDataType.String;
            if (t == typeof(long) || t.RemoveNullability() == typeof(ulong))
                return eSerieAxisDataType.BigInteger;
            if (t.IsIntegerType())
                return eSerieAxisDataType.Integer;
            if (t == typeof(decimal) || t == typeof(double) || t == typeof(float))
                return eSerieAxisDataType.Decimal;
            if (t == typeof(DateTime))
                return eSerieAxisDataType.DateTime;
            if (t == typeof(TimeSpan))
                return eSerieAxisDataType.Time;
            return eSerieAxisDataType.Unknown;
        }
    }
}
