﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL.Reportings
{
    [DbMappedTable(schemaName: "Reportings", tableName: "ChartSeries")]
    public sealed partial class ChartSeries : BaseDTO<IdTuple<int, int>>
    {
        [DebuggerHidden][DbMappedField("Chart_Id", FkTo = typeof(Chart))]  public int Chart_Id { get; set; }
        [DebuggerHidden][DbMappedField("Serie_Id", FkTo = typeof(Serie))]  public int Serie_Id { get; set; }

        protected override IdTuple<int, int> ClosedId
        {
            get { return new IdTuple<int, int>(Chart_Id, Serie_Id); }
            set { Chart_Id = value.Id1; Serie_Id = value.Id2; }
        }

        #region Cloneable

        public new ChartSeries Clone() { return (ChartSeries)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new ChartSeries(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            var from = (ChartSeries)source;
            // Note : Les champs appartenant à la PK ne sont pas copiés

            Chart_Id = from.Chart_Id;
            Serie_Id = from.Serie_Id;
        }

        #endregion
    }
}
