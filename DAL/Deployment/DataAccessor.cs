﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

using ApplicationBase.Deployment.Data;
using ApplicationBase.Deployment;

using DataMapper;

using ApplicationBase.DAL.Users;


namespace ApplicationBase.DAL.Deployment
{
    // Wee keep the DTO object proposed by default in ApplicationBase.Deployment.Data
    // So this class just add a security layer
    public class DataAccessor : DefaultDataAccessor
    {
        public DataAccessor(Config config)
            : base(config)
        {
        }

        public override bool IsAuthenticationRequired { get { return true; } }

        public override List<EnvironmentSetting> Authenticate(EnvironmentConfig env, string login, string password)
        {
            var data = DAO.Authenticate(WrappedMapper.GetConnectionString(), new Login { Identifiant = login }.WithPassword(password), env);
            Debug.Assert(IDataAccessor_Extensions.FirstSettingReturnedIsBaseConnectionString != null);
            WrappedMapper = DbMapperFactory.CreateSqlMapper(WrappedMapper.Owner, data.Item2.First().Value);
            IsAuthenticated = true;
            return data.Item2;
        }

    }
}
