﻿using System;

using DataMapper;


namespace ApplicationBase.DAL
{
    /// <summary>
    /// Sert à marquer les classes appartenant à ApplicationBase.DAL
    /// => Permettra de faire des méthodes d'extension connaissant implicitement le dbmapper à utiliser
    /// </summary>
    public interface IDALObject_Base : IAutoMappedDbObject
    {
    }
}
