CREATE TABLE [users].[Profiles]
(
	[Id_Profile] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[InheritingProfile_Id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Profile] ASC
)
)

GO

ALTER TABLE [users].[Profiles]  WITH CHECK ADD  CONSTRAINT [FK__Profiles__Profiles__Id_Profile] FOREIGN KEY([InheritingProfile_Id])
REFERENCES [users].[Profiles] ([Id_Profile])
GO

ALTER TABLE [users].[Profiles] CHECK CONSTRAINT [FK__Profiles__Profiles__Id_Profile]
GO


