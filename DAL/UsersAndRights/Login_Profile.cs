﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Users
    {
        [DbMappedTable(schemaName: "Users", tableName: "Logins_Profiles")]
        public sealed partial class Login_Profile : BaseDTO<IdTuple<long>>, IDALObject_Base
        {
            [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]      public     long Id               { [DebuggerStepThrough] get { return               _Id; } [DebuggerStepThrough] set {                                                                   SetTechnicalProperty(ref       _Id, value, RaiseIdChanged); } }     long _Id;
            [DebuggerHidden][DbMappedField("Login_Id")]                 public     long Login_Id         { [DebuggerStepThrough] get { return         _Login_Id; } [DebuggerStepThrough] set {                                                                            SetProperty(ref                 _Login_Id, value); } }     long _Login_Id;
            [DebuggerHidden][DbMappedField("Profile_Id")]               public     long Profile_Id       { [DebuggerStepThrough] get { return       _Profile_Id; } [DebuggerStepThrough] set {                                                                            SetProperty(ref               _Profile_Id, value); } }     long _Profile_Id;
            [DebuggerHidden][DbMappedField("ChangedByLoginId")]         public     long ChangedByLoginId { [DebuggerStepThrough] get { return _ChangedByLoginId; } [DebuggerStepThrough] set {                                                                            SetProperty(ref         _ChangedByLoginId, value); } }     long _ChangedByLoginId;
            [DebuggerHidden][DbMappedField("ChangedDate")]              public DateTime ChangedDate      { [DebuggerStepThrough] get { return      _ChangedDate; } [DebuggerStepThrough] set {  ChkRange(value, MinValueOf.ChangedDate, MaxValueOf.ChangedDate);          SetProperty(ref              _ChangedDate, value); } } DateTime _ChangedDate;

            protected override IdTuple<long> ClosedId
            {
                get { return new IdTuple<long>(Id); }
                set { Id = value.Id1; }
            }

            #region Cloneable
    
            public new Login_Profile Clone() { return (Login_Profile)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new Login_Profile(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (Login_Profile)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Login_Id = from._Login_Id;
                _Profile_Id = from._Profile_Id;
                _ChangedByLoginId = from._ChangedByLoginId;
                _ChangedDate = from._ChangedDate;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
                public static readonly DateTime ChangedDate = new DateTime(0); // 0001-01-01T00:00:00.0000000
            }
    
            static class MaxValueOf
            {
                public static readonly DateTime ChangedDate = new DateTime(3155378975999990000); // 9999-12-31T23:59:59.9990000
            }
    
            #endregion
        }
    }
}
