CREATE PROCEDURE [users].[CreateUser] 
	@firstname nvarchar(256),
	@lastname nvarchar(256),
	@login nvarchar(256),
	@pwd_hash varchar(256) /*null is possible*/,
	@is_service bit
AS 
BEGIN
	DECLARE @Ids_Person TABLE (Id_Person bigint)

	INSERT INTO [users].[Persons]
			(Firstname
			,Lastname)
	OUTPUT INSERTED.Id_Person INTO @Ids_Person(Id_Person)
		VALUES
			(@firstname
			,@lastname);

	declare @Id_Person bigint;
	select @Id_Person = Id_Person from @Ids_Person;

	INSERT INTO [users].Logins
			(Person_Id,
			[Login]
			,PwdHash
			,IsEnabled
			,IsAdmin
			,IsService
			,CreationDate
			,LastLoginDate)
		VALUES
			(@Id_Person
			,@login
			,@pwd_hash,
			1,
			0,
			@is_service,
			getdate(),
			null);
	select @Id_Person;
END


GO


