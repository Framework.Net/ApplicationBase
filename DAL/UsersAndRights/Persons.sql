CREATE TABLE [users].[Persons]
(
	[Id_Person] [bigint] IDENTITY(1,1) NOT NULL,
	[Firstname] [nvarchar](256) NOT NULL,
	[Lastname] [nvarchar](256) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Person] ASC
)
)

GO


