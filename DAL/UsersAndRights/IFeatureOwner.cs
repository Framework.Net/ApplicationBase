﻿using System;
using System.Collections.Generic;


namespace ApplicationBase.DAL
{
    namespace Users
    {
        public interface IFeatureOwner
        {
            long   Id   { get; }
            string Name { get; }
            IEnumerable<IFeatureAssignment> GetAssignedLinks(DAO dao);
        }
    }
}
