﻿using System;


namespace ApplicationBase.DAL
{
    [Flags]
    public enum eFeatureAccess : byte
    {
        /// <summary>
        /// Means user cannot even acces data in read only. BUT the Gui / user control should be visible to let user _know_ this Gui / user control exists.
        /// </summary>
        None = 0,
        CanSee = 1,
        CanEdit = 2,
        /// <summary>
        /// When the application part is a not about data acces but the right to use a functionality this is the only flag to check
        /// </summary>
        CanUse = 4,
    }
}
