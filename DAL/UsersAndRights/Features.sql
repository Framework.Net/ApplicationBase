CREATE TABLE [users].[Features]
(
	[Id_Feature] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](512) NOT NULL,
	[Description] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Feature] ASC
)
)

GO


