﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Users
    {
        [DbMappedTable(schemaName: "Users", tableName: "Persons")]
        [DebuggerDisplay("{" + nameof(Firstname) + ",nq} {" + nameof(Lastname) + ",nq}")]
        public sealed partial class Person : BaseDTO<IdTuple<long>>, IDALObject_Base
        {
            [DebuggerHidden][DbMappedField("Id_Person", IsAutoPK = true)]  public   long Id        { [DebuggerStepThrough] get { return        _Id; } [DebuggerStepThrough] set {                               SetTechnicalProperty(ref  _Id,        value, RaiseIdChanged); } }   long _Id;
            [DebuggerHidden][DbMappedField("Firstname"), DbMaxLength(256)] public string Firstname { [DebuggerStepThrough] get { return _Firstname; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 256);          SetProperty(ref  _Firstname, value);                 } } string _Firstname;
            [DebuggerHidden][DbMappedField("Lastname"), DbMaxLength(256)]  public string Lastname  { [DebuggerStepThrough] get { return  _Lastname; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 256);          SetProperty(ref   _Lastname, value);                 } } string _Lastname;

            public string FullName { get { return Firstname + " " + Lastname; } }
            
            protected override IdTuple<long> ClosedId
            {
                get { return new IdTuple<long>(Id); }
                set { Id = value.Id1; }
            }

            public Person()
               : this(true)
            {
            }
            public Person(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Firstname = string.Empty;
                _Lastname = string.Empty;
    
            }

    
            #region Cloneable
    
            public new Person Clone() { return (Person)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new Person(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (Person)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Firstname = from._Firstname;
                _Lastname = from._Lastname;
            }
    
            #endregion
    
        }
    }
}
