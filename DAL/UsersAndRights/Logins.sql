CREATE TABLE [users].[Logins]
(
	[Person_Id] [bigint] NOT NULL,
	[Login] [nvarchar](256) NOT NULL,
	[PwdHash] [nvarchar](256) NULL,
	[IsEnabled] [bit] NOT NULL,
	[IsAdmin] [bit] NOT NULL,
	[CreationDate] [datetime2](7) NOT NULL,
	[LastLoginDate] [datetime2](7) NULL,
	[IsService] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Person_Id] ASC
),
UNIQUE NONCLUSTERED 
(
	[Login] ASC
)
)

GO

ALTER TABLE [users].[Logins]  WITH CHECK ADD  CONSTRAINT [FK__Logins__Persons__Person_Id] FOREIGN KEY([Person_Id])
REFERENCES [users].[Persons] ([Id_Person])
GO

ALTER TABLE [users].[Logins] CHECK CONSTRAINT [FK__Logins__Persons__Person_Id]
GO


