﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Users
    {
        [DbMappedTable(schemaName: "Users", tableName: "Profiles_Features")]
        public sealed partial class Profile_Feature : BaseDTO<IdTuple<long>>, IDALObject_Base, IFeatureAssignment
        {
            [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]  public           long Id               { [DebuggerStepThrough] get { return               _Id; } [DebuggerStepThrough] set {                                                                   SetTechnicalProperty(ref  _Id, value, RaiseIdChanged); } }     long _Id;
            [DebuggerHidden][DbMappedField("Profile_Id")]            public          long Profile_Id       { [DebuggerStepThrough] get { return       _Profile_Id; } [DebuggerStepThrough] set {                                                                            SetProperty(ref          _Profile_Id, value); } }     long _Profile_Id;
            [DebuggerHidden][DbMappedField("Feature_Id")]           public           long Feature_Id       { [DebuggerStepThrough] get { return       _Feature_Id; } [DebuggerStepThrough] set {                                                                            SetProperty(ref          _Feature_Id, value); } }     long _Feature_Id;
            [DebuggerHidden][DbMappedField("ChangedByLoginId")]     public           long ChangedByLoginId { [DebuggerStepThrough] get { return _ChangedByLoginId; } [DebuggerStepThrough] set {                                                                            SetProperty(ref    _ChangedByLoginId, value); } }     long _ChangedByLoginId;
            [DebuggerHidden][DbMappedField("ChangedDate")]          public       DateTime ChangedDate      { [DebuggerStepThrough] get { return      _ChangedDate; } [DebuggerStepThrough] set {  ChkRange(value, MinValueOf.ChangedDate, MaxValueOf.ChangedDate);          SetProperty(ref         _ChangedDate, value); } } DateTime _ChangedDate;
            [DebuggerHidden][DbMappedField("Access")]               public eFeatureAccess Access           { [DebuggerStepThrough] get { return           _Access; } [DebuggerStepThrough] set {                                                                            SetProperty(ref              _Access, value); } } eFeatureAccess _Access;

            long IFeatureAssignment.AssignedOwnerId { get { return Profile_Id; } set { Profile_Id = value; } }

            protected override IdTuple<long> ClosedId
            {
                get { return new IdTuple<long>(Id); }
                set { Id = value.Id1; }
            }

            #region Cloneable
    
            public new Profile_Feature Clone() { return (Profile_Feature)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new Profile_Feature(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (Profile_Feature)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Profile_Id = from._Profile_Id;
                _Feature_Id = from._Feature_Id;
                _Access = from._Access;
                _ChangedByLoginId = from._ChangedByLoginId;
                _ChangedDate = from._ChangedDate;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
                public static readonly DateTime ChangedDate = new DateTime(0); // 0001-01-01T00:00:00.0000000
            }
    
            static class MaxValueOf
            {
                public static readonly DateTime ChangedDate = new DateTime(3155378975999990000); // 9999-12-31T23:59:59.9990000
            }
    
            #endregion
        }
    }
}
