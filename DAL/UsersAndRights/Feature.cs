﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Users
    {
        [DbMappedTable(schemaName: "Users", tableName: "Features")]
        [DebuggerDisplay("{" + nameof(Name) + ",nq}")]
        [GeneratedClassIsDynamicEnum(businessNamePropertyName: nameof(Name), ListCanBeExtendedInDatabase = true)]
        public partial class Feature : DynamicEnumWithId<long, Feature>, IDALObject_Base
        {
            [DebuggerHidden][DbMappedField("Id_Feature", IsAutoPK = true)]    public long   Id          { [DebuggerStepThrough] get { return  _Id_Feature; } [DebuggerStepThrough] set {                              SetTechnicalProperty(ref _Id_Feature, value, RaiseIdChanged); } } long _Id_Feature;
            [DebuggerHidden][DbMappedField("Name"), DbMaxLength(512)]         public string Name        { [DebuggerStepThrough] get { return        _Name; } [DebuggerStepThrough] set { ChkFieldLen(ref value, 512); SetProperty(ref _Name,                value); } } string _Name;
            [DebuggerHidden][DbMappedField("Description", IsNullable = true)] public string Description { [DebuggerStepThrough] get { return _Description; } [DebuggerStepThrough] set {                              SetProperty(ref _Description,         value); } } string _Description;

            protected override long   EnumId   { [DebuggerStepThrough] get { return Id;   } [DebuggerStepThrough] set { Id = value; } }
            protected override string EnumName { [DebuggerStepThrough] get { return Name; } [DebuggerStepThrough] set { Name = value; } }

            protected override IdTuple<long> ClosedId
            {
                get { return new IdTuple<long>(Id); }
                set { Id = value.Id1; }
            }

            public Feature()
               : this(true)
            {
            }
            public Feature(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Name = string.Empty;

            }

            #region Cloneable

            public new Feature Clone() { return (Feature)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new Feature(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (Feature)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Name = from._Name;
                _Description = from._Description;
            }

            #endregion


            #region Values

            #region Feature plutôt pour les developpeurs

            [DbNameBound("Access to Environments & their Settings")]
            public static Feature AccessToEnvironmentsAndTheirSettings { get; private set; }

            [DbNameBound("Can Impersonate Logins")]
            public static Feature CanImpersonateLogins { get; private set; }

            [DbNameBound("Can See Logs")]
            public static Feature CanSeeServiceLogs { get; private set; }

            [DbNameBound("Can deploy Application")]
            public static Feature CanDeployApplication { get; private set; }

            [DbNameBound("Can access experimental parts")]
            public static Feature CanAccessExperimentalParts { get; private set; }

            #endregion

            #region Feature business

            [DbNameBound("Allow Everybody")]
            public static Feature AllowEverybody { get; private set; }

            [DbNameBound("Allow Admin Only")]
            public static Feature AllowAdminOnly { get; private set; }

            #endregion

            #endregion
        }
    }
}
