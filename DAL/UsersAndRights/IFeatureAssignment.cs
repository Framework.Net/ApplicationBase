﻿using System;


namespace ApplicationBase.DAL
{
    namespace Users
    {
        public interface IFeatureAssignment
        {
                      long Id               { get; set; }
                      long AssignedOwnerId  { get; set; }
                      long Feature_Id       { get; set; }
                      long ChangedByLoginId { get; set; }
                  DateTime ChangedDate      { get; set; }
            eFeatureAccess Access           { get; set; }

        }
    }
}
