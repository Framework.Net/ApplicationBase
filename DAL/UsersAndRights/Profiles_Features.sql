CREATE TABLE [users].[Profiles_Features]
(
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Profile_Id] [bigint] NOT NULL,
	[Feature_Id] [bigint] NOT NULL,
	[Access] [tinyint] NOT NULL,
	[ChangedByLoginId] [bigint] NOT NULL,
	[ChangedDate] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)

GO

ALTER TABLE [users].[Profiles_Features]  WITH CHECK ADD  CONSTRAINT [FK__Profiles_Features__Features__Feature_Id] FOREIGN KEY([Feature_Id])
REFERENCES [users].[Features] ([Id_Feature])
GO

ALTER TABLE [users].[Profiles_Features] CHECK CONSTRAINT [FK__Profiles_Features__Features__Feature_Id]
GO

ALTER TABLE [users].[Profiles_Features]  WITH CHECK ADD  CONSTRAINT [FK__Profiles_Features__Logins__ChangedByLoginId] FOREIGN KEY([ChangedByLoginId])
REFERENCES [users].[Logins] ([Person_Id])
GO

ALTER TABLE [users].[Profiles_Features] CHECK CONSTRAINT [FK__Profiles_Features__Logins__ChangedByLoginId]
GO

ALTER TABLE [users].[Profiles_Features]  WITH CHECK ADD  CONSTRAINT [FK__Profiles_Features__Profiles__Profile_Id] FOREIGN KEY([Profile_Id])
REFERENCES [users].[Profiles] ([Id_Profile])
GO

ALTER TABLE [users].[Profiles_Features] CHECK CONSTRAINT [FK__Profiles_Features__Profiles__Profile_Id]
GO


