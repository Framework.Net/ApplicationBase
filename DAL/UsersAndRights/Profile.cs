﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Users
    {
        [DbMappedTable(schemaName: "Users", tableName: "Profiles")]
        [DebuggerDisplay("{" + nameof(Name) + ",nq} ({" + nameof(Id) + ",nq}) inheriting from {" + nameof(InheritingProfile_Id) + ",nq}")]
        public sealed partial class Profile : BaseDTO<IdTuple<long>>, IDALObject_Base, IFeatureOwner
        {
            [DebuggerHidden][DbMappedField("Id_Profile", IsAutoPK = true)]             public   long Id                   { [DebuggerStepThrough] get { return                   _Id; } [DebuggerStepThrough] set {                               SetTechnicalProperty(ref  _Id,                  value, RaiseIdChanged); } }   long _Id;
            [DebuggerHidden][DbMappedField("Name"), DbMaxLength(256)]                  public string Name                 { [DebuggerStepThrough] get { return                 _Name; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 256);          SetProperty(ref  _Name,                value);                 } } string _Name;
            [DebuggerHidden][DbMappedField("InheritingProfile_Id", IsNullable = true)] public  long? InheritingProfile_Id { [DebuggerStepThrough] get { return _InheritingProfile_Id; } [DebuggerStepThrough] set {                                        SetProperty(ref  _InheritingProfile_Id, value);                 } }  long? _InheritingProfile_Id;

            public static readonly Profile DirectRight = new Profile() { Id = -1000, Name = "Direct Right" };
            public static readonly Profile NoProfile = new Profile() { Id = 0, Name = "No Profile" };

            protected override IdTuple<long> ClosedId
            {
                get { return new IdTuple<long>(Id); }
                set { Id = value.Id1; }
            }

            public Profile()
               : this(true)
            {
            }
            public Profile(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Name = string.Empty;
    
            }
    
            #region Cloneable
    
            public new Profile Clone() { return (Profile)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new Profile(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (Profile)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Name = from._Name;
                _InheritingProfile_Id = from._InheritingProfile_Id;
            }
    
            #endregion
    
            long IFeatureOwner.Id     { get { return Id; } }
            string IFeatureOwner.Name { get { return Name; } }
            IEnumerable<IFeatureAssignment> IFeatureOwner.GetAssignedLinks(DAO dao)
            {
                return dao.LoadCollection<Profile_Feature>()
                          .Where(link => link.Profile_Id == (this as IFeatureOwner).Id)
                          .ToList();
            }
        }
    }
}
