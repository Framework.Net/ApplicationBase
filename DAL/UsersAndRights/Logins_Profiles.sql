CREATE TABLE [users].[Logins_Profiles]
(
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Login_Id] [bigint] NOT NULL,
	[Profile_Id] [bigint] NOT NULL,
	[ChangedByLoginId] [bigint] NOT NULL,
	[ChangedDate] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)

GO

ALTER TABLE [users].[Logins_Profiles]  WITH CHECK ADD  CONSTRAINT [FK__Logins_Profiles__Logins__ChangedByLoginId] FOREIGN KEY([ChangedByLoginId])
REFERENCES [users].[Logins] ([Person_Id])
GO

ALTER TABLE [users].[Logins_Profiles] CHECK CONSTRAINT [FK__Logins_Profiles__Logins__ChangedByLoginId]
GO

ALTER TABLE [users].[Logins_Profiles]  WITH CHECK ADD  CONSTRAINT [FK__Logins_Profiles__Logins__Login_Id] FOREIGN KEY([Login_Id])
REFERENCES [users].[Logins] ([Person_Id])
GO

ALTER TABLE [users].[Logins_Profiles] CHECK CONSTRAINT [FK__Logins_Profiles__Logins__Login_Id]
GO

ALTER TABLE [users].[Logins_Profiles]  WITH CHECK ADD  CONSTRAINT [FK__Logins_Profiles__Profiles__Profile_Id] FOREIGN KEY([Profile_Id])
REFERENCES [users].[Profiles] ([Id_Profile])
GO

ALTER TABLE [users].[Logins_Profiles] CHECK CONSTRAINT [FK__Logins_Profiles__Profiles__Profile_Id]
GO


