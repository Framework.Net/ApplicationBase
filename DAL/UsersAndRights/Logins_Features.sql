CREATE TABLE [users].[Logins_Features]
(
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Login_Id] [bigint] NOT NULL,
	[Feature_Id] [bigint] NOT NULL,
	[Access] [tinyint] NOT NULL,
	[ChangedByLoginId] [bigint] NOT NULL,
	[ChangedDate] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)

GO

ALTER TABLE [users].[Logins_Features]  WITH CHECK ADD  CONSTRAINT [FK__Logins_Features__Logins__ChangedByLoginId] FOREIGN KEY([ChangedByLoginId])
REFERENCES [users].[Logins] ([Person_Id])
GO

ALTER TABLE [users].[Logins_Features] CHECK CONSTRAINT [FK__Logins_Features__Logins__ChangedByLoginId]
GO

ALTER TABLE [users].[Logins_Features]  WITH CHECK ADD  CONSTRAINT [FK__Logins_Features__Logins__Feature_Id] FOREIGN KEY([Feature_Id])
REFERENCES [users].[Features] ([Id_Feature])
GO

ALTER TABLE [users].[Logins_Features] CHECK CONSTRAINT [FK__Logins_Features__Logins__Feature_Id]
GO

ALTER TABLE [users].[Logins_Features]  WITH CHECK ADD  CONSTRAINT [FK__Logins_Features__Logins__Person_Id] FOREIGN KEY([Login_Id])
REFERENCES [users].[Logins] ([Person_Id])
GO

ALTER TABLE [users].[Logins_Features] CHECK CONSTRAINT [FK__Logins_Features__Logins__Person_Id]
GO


