CREATE PROCEDURE [users].[CreateProfile] 
	@name nvarchar(256),
	@inheritingProfileId bigint = null
AS 
BEGIN
	INSERT INTO [users].[Profiles]
			(Name
			,InheritingProfile_Id)
	OUTPUT INSERTED.Id_Profile
		VALUES
			(@name
			,@inheritingProfileId);
END

GO


