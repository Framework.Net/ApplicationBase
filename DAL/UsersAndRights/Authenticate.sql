CREATE PROCEDURE [Security].[Authenticate] 
	@login varchar(254),
	@pwd_hash varchar(64),
	@envId int,
	@pwd_hash2 varchar(64) = null -- not used, will be removed
WITH EXECUTE AS SELF

AS set nocount on
BEGIN
	DECLARE @id_login bigint;
	DECLARE @isAdmin bit;
	DECLARE @isEnabled bit;
	DECLARE @connectionString nvarchar(max);
	DECLARE @UserPwdHash nvarchar(max);

	select @id_login = L.Person_Id,
		   @isAdmin = L.IsAdmin,
		   @isEnabled = L.IsEnabled,
		   @UserPwdHash = L.PwdHash
	from [Users].[Logins] as L
	where L.[Login] = @login

	declare @errMsg nvarchar(max);
	if @id_login is null
		set @errMsg = 'Authentication failed!';
	else 
	begin
		-- if there is no hash in database for this login, we rely on another authentication management system
		if @UserPwdHash is null
		begin
		    -- set @pwd_hash = isnull(@pwd_hash2, @pwd_hash); -- To make authentication working with any password
 
			DECLARE @res INT
			-- current database needs to have setting trustworthy = on, 
			-- otherwise proc stoc's clause "EXECUTE AS SELF" make current execution context sandboxed to current database
			begin try

			-- database setting 'trustworthy' must be set to ON
			SELECT @res = [clrTools].[ExecuteScalarWithSkip] (/*@conString=*/'Data Source=localhost;Initial Catalog=DbHandlingAuthentication;User ID=login;Password=password;Application Name=MainDatabase;',
															  /*@request=*/'declare @res int; execute @res = Authentication.CheckLogin @login=''' + Replace(@login, '''', '''''') + ''', @unpurified_sha_256=''' + Replace(@pwd_hash, '''', '''''') + '''; select @res;',
															  /*@interestingResultSetIndex=*/1);
			end try
			begin catch
			   set @errMsg = 'Cannot connect to authentication service or authentication service unavailable! Try again later or ask an IT about this issue';
			end catch
			--REVERT;  

			if      @res = 5 set @errMsg = 'Your account has been closed!'
			else if @res = 4 set @errMsg = 'Your password is regenerating... Try again later.'
			-- 3 really means 'Incorrect password!' but we hide it to prevent attacker to have knowledge abount what's wrong
			else if @res = 3 set @errMsg = 'Incorrect login or password!'
			-- 2 really means 'Login not found!'    but we hide it to prevent attacker to have knowledge abount what's wrong
			else if @res = 2 set @errMsg = 'Incorrect login or password!'
			-- Case @res1 = 1 does not exist
			else if @res <> 0 set @errMsg = 'Unknown error!'
		end
		else if @UserPwdHash <> @pwd_hash
			set @errMsg = 'Authentication failed!';
	end
	
	IF @errMsg is not NULL  OR @isEnabled = 0  WAITFOR DELAY '00:00:01' -- security measure to prevent brute-force attack
	IF @errMsg IS not NULL 
	begin 
		RAISERROR (@errMsg, 16, 1);
		RETURN;
	end
	IF @isEnabled = 0      
	begin 
		RAISERROR ('Authentication failed (Account disabled)!', 16, 1);
		RETURN; 
	end
		
	UPDATE [Users].[Logins] set LastLoginDate = getdate() where Person_Id = @id_login

	declare @maximumRight int; -- 1 = readonly, 2 = writeonly, 3 = read & write, 4 = obsolete but like 2
	select @maximumRight  = case when max1 > max2 then max1 else max2 end
	from (		select isnull(max(LF.Access), 0) as max1
				from users.Logins L
				inner join users.Logins_Features LF on LF.Login_Id = L.Person_Id
				where L.Person_Id = @id_login
       ) as t1
cross join (	select isnull(max(PF.Access), 0) as max2
				from users.Logins L
				inner join users.Logins_Profiles LP on LP.Login_Id = L.Person_Id
				inner join users.Profiles P on P.Id_Profile = LP.Profile_Id
				inner join users.Profiles_Features PF on P.Id_Profile = PF.Profile_Id
				where L.Person_Id = @id_login
       ) as t2 
	declare @isMaximumRightReadOnly bit; select @isMaximumRightReadOnly = case when @maximumRight <= 1 then 1 else 0 end;
	
	select @id_login;
	select * from [Security].[FlattenizedEnvironmentSettingsEnriched](@envId, 'Connections.MainConnectionString', @isAdmin, @isMaximumRightReadOnly)
END



GO


