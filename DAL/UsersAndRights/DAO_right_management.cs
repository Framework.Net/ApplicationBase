﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

using DataMapper;
using DataMapper.Tools;

using ApplicationBase.Deployment.Data;
using ApplicationBase.DAL.Users;


namespace ApplicationBase.DAL
{
    public partial class DAO
    {
        /// <summary>
        /// Returns Login' id authenticated or null if authentication failed
        /// and a new connection string with write access
        /// </summary>
        public static Tuple<long, List<EnvironmentSetting>> Authenticate(string initialConnectionString, Login login, EnvironmentConfig env)
        {
            Debug.Assert(env != null);
            using (SqlConnection conn = SqlConnectionTracer.Create(initialConnectionString))
            using (var cmd = new SqlCommand("[Security].[Authenticate]", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@login", login.Identifiant ?? string.Empty));
                cmd.Parameters.Add(new SqlParameter("@pwd_hash", login.PwdHash ?? string.Empty));
                cmd.Parameters.Add(new SqlParameter("@envId", env.Id));

                conn.OpenOrWaitUntilOpen();

                // ReSharper  once AccessToDisposedClosure
                long login_id = 0;
                using (SqlDataReader rdr = SqlRetry.Instance.HandleRetry(cmd, true, () => cmd.ExecuteReader()))
                {
                    while (rdr.Read()) { login_id = rdr.GetInt64(0); }
                    rdr.NextResult();
                    var dt = new DataTable();
                    dt.Load(rdr);
                    var temp = DbMapperFactory.CreateSqlMapper(null, initialConnectionString);
                    var settings = temp.EnumerateConvertedDataRow<EnvironmentSetting>(dt.AsEnumerable()).ToList();
                    return Tuple.Create(login_id, settings);
                }
            }
        }

        public virtual Login CreateUser(Person person, Login login)
        {
            using (SqlConnection conn = SqlConnectionTracer.Create(GetConnectionString()))
            using (var cmd = new SqlCommand("[Users].[CreateUser]", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@firstname", person.Firstname));
                cmd.Parameters.Add(new SqlParameter("@lastname", person.Lastname));
                cmd.Parameters.Add(new SqlParameter("@login", login.Identifiant));
                if (login.PwdHash == null)
                    cmd.Parameters.Add(new SqlParameter("@pwd_hash", DBNull.Value));
                else
                    cmd.Parameters.Add(new SqlParameter("@pwd_hash", login.PwdHash));
                cmd.Parameters.Add(new SqlParameter("@is_service", login.IsService));

                conn.OpenOrWaitUntilOpen();

                // ReSharper disable once AccessToDisposedClosure
                var id = SqlRetry.Instance.HandleRetry(cmd, true, () => cmd.ExecuteScalar());
                var person_id = (long)id;
                return GetObjectWithId<Login>(person_id);
            }
        }

        public virtual Profile CreateProfile(Profile profile)
        {
            using (SqlConnection conn = SqlConnectionTracer.Create(GetConnectionString()))
            using (var cmd = new SqlCommand("[Users].[CreateProfile]", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@name", profile.Name));

                conn.OpenOrWaitUntilOpen();

                // ReSharper disable once AccessToDisposedClosure
                var id = SqlRetry.Instance.HandleRetry(cmd, true, () => cmd.ExecuteScalar());
                var profile_id = (long)id;
                return GetObjectWithId<Profile>(profile_id);
            }
        }
    }
}
