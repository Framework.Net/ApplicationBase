﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using DataMapper;


namespace ApplicationBase.DAL
{
    namespace Users
    {
        [DbMappedTable(schemaName: "Users", tableName: "Logins")]
        [DebuggerDisplay("{" + nameof(Identifiant) + ",nq}")]
        public sealed partial class Login : BaseDTO<IdTuple<long>>, IDALObject_Base, IFeatureOwner
        {
            [DebuggerHidden][DbMappedField("Person_Id", IsPK = true, FkTo = typeof(Person))]  public      long Person_Id     { [DebuggerStepThrough] get { return     _Person_Id; } [DebuggerStepThrough]         set {                                                              SetTechnicalProperty(ref  _Person_Id, value, RaiseIdChanged); } }      long _Person_Id;
            [DebuggerHidden][DbMappedField("Login"), DbMaxLength(256)]                        public    string Identifiant   { [DebuggerStepThrough] get { return   _Identifiant; } [DebuggerStepThrough]         set {                                          ChkFieldLen(ref value, 256); SetProperty(ref                _Identifiant, value); } }    string _Identifiant;
            /// <summary> Can be null if SQL handles password in another way (for example by delegating authentication to another system) </summary>
            [DebuggerHidden][DbMappedField("PwdHash", IsNullable = true), DbMaxLength(256)]   internal  string PwdHash       { [DebuggerStepThrough] get { return       _PwdHash; } [DebuggerStepThrough] private set {                                          ChkFieldLen(ref value, 256); SetProperty(ref                    _PwdHash, value); } }    string _PwdHash;
            [DebuggerHidden][DbMappedField("IsEnabled")]                                      public      bool IsEnabled     { [DebuggerStepThrough] get { return     _IsEnabled; } [DebuggerStepThrough]         set {                                                                       SetProperty(ref                  _IsEnabled, value); } }      bool _IsEnabled;
            [DebuggerHidden][DbMappedField("IsAdmin")]                                        public      bool IsAdmin       { [DebuggerStepThrough] get { return       _IsAdmin; } [DebuggerStepThrough]         set {                                                                       SetProperty(ref                    _IsAdmin, value); } }      bool _IsAdmin;
            [DebuggerHidden][DbMappedField("IsService")]                                      public      bool IsService     { [DebuggerStepThrough] get { return     _IsService; } [DebuggerStepThrough]         set {                                                                       SetProperty(ref                  _IsService, value); } }      bool _IsService;
            [DebuggerHidden][DbMappedField("CreationDate")]                                   public  DateTime CreationDate  { [DebuggerStepThrough] get { return  _CreationDate; } [DebuggerStepThrough]         set {    ChkRange(value, MinValueOf.CreationDate, MaxValueOf.CreationDate); SetProperty(ref               _CreationDate, value); } }  DateTime _CreationDate;
            [DebuggerHidden][DbMappedField("LastLoginDate", IsNullable = true)]               public DateTime? LastLoginDate { [DebuggerStepThrough] get { return _LastLoginDate; } [DebuggerStepThrough]         set {  ChkRange(value, MinValueOf.LastLoginDate, MaxValueOf.LastLoginDate); SetProperty(ref              _LastLoginDate, value); } } DateTime? _LastLoginDate;

            /// <summary> Official Hash function for password </summary>
            public Login WithPassword(string value)
            {
                if (value == null)
                    PwdHash = null;
                else
                {
                    if (value.Length < 8)
                        throw new UserUnderstandableException("Password must be at least 8 characters", null);
                    if (value.Length != Regex.Replace(value, @"\p{C}+", string.Empty).Length)
                        throw new UserUnderstandableException("Password is composed of forbidden control character(s)! Please change it", null);
                    if (!Regex.IsMatch(value, "^[^ '\"<>&\\r\\n]{8,}$")) // regex officielle de validation des passwords, vu avec Greg
                        throw new UserUnderstandableException("Password is composed of forbidden character(s)! Please change it", null);
                    if (value != value.Trim())
                        throw new UserUnderstandableException("Password must not contain spaces at the beginning or the end", null);
                    PwdHash = (PasswordHashSaltPrefix + value + PasswordHashSaltSuffix)?.ToSHA256Hash();
                }
                return this;
            }
            public static string PasswordHashSaltPrefix { get; set; }
            public static string PasswordHashSaltSuffix { get; set; }

            protected override IdTuple<long> ClosedId
            {
                get { return new IdTuple<long>(Person_Id); }
                set { Person_Id = value.Id1; }
            }

            public Login()
               : this(true)
            {
            }
            public Login(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Identifiant = string.Empty;
                _PwdHash = string.Empty;
    
            }

            #region Cloneable

            public new Login Clone() { return (Login)(this as ICloneable).Clone(); }
            protected override BaseDTO CreateNewInstance() { return new Login(); }
            public override void CopyAllFieldsFrom(BaseDTO source)
            {
                var from = (Login)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés
                _Identifiant = from._Identifiant;
                _PwdHash = from._PwdHash;
                _IsEnabled = from._IsEnabled;
                _IsAdmin = from._IsAdmin;
                _IsService = from._IsService;
                _CreationDate = from._CreationDate;
                _LastLoginDate = from._LastLoginDate;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
                public static readonly DateTime CreationDate = new DateTime(0); // 0001-01-01T00:00:00.0000000
                public static readonly DateTime LastLoginDate = new DateTime(0); // 0001-01-01T00:00:00.0000000
            }
    
            static class MaxValueOf
            {
                public static readonly DateTime CreationDate = new DateTime(3155378975999990000); // 9999-12-31T23:59:59.9990000
                public static readonly DateTime LastLoginDate = new DateTime(3155378975999990000); // 9999-12-31T23:59:59.9990000
            }

            #endregion


            
            long IFeatureOwner.Id   { get { return Person_Id; } }
            string IFeatureOwner.Name { get { return Identifiant; } }
            IEnumerable<IFeatureAssignment> IFeatureOwner.GetAssignedLinks(DAO dao)
            {
                return dao.LoadCollection<Login_Feature>()
                          .Where(link => link.Login_Id == (this as IFeatureOwner).Id)
                          .ToList();
            }
        }
    }
}
