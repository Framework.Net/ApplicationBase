create function [dbo].[DateMin](@val1 datetime2, @val2 datetime2)
returns datetime2
as
begin
  if @val1 < @val2
    return @val1
  return isnull(@val1,@val2)
end
GO


