CREATE FUNCTION [dbo].[AddWorkingDays](@addDate AS smalldatetime, @numDays AS INT)
RETURNS smalldatetime
AS
BEGIN
	WHILE @numDays > 0
	BEGIN
		SET @addDate = DATEADD(d, 1, @addDate)
		IF DATENAME(DW, @addDate) = 'sunday' SET @addDate = DATEADD(d, 1, @addDate)
		SET @numDays = @numDays - 1
	END
	WHILE @numDays < 0
	BEGIN
		SET @addDate = DATEADD(d, -1, @addDate)
		IF DATENAME(DW, @addDate) = 'sunday' SET @addDate = DATEADD(d, -1, @addDate)
		SET @numDays = @numDays + 1
	END
	RETURN @addDate
END
GO


