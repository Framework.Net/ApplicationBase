﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;


namespace TechnicalTools.Build.Extensions
{
    // TODO : replace these method by use XElement' extension method "*Named". Example:
    // Doc.SimpleXPath("//ProjectGuid")  => Doc.DescendantsNamed("ProjectGuid")
    public static class XElement_Extensions
    {
        // See https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ms256086(v=vs.100)
        // internal because need to be retested (& write tests)
        public static IEnumerable<XElement> SimpleXPath(this XNode node, string xpath_no_namespace)
        {
            var stdXpath = ConvertToStandardXPath(xpath_no_namespace);
            //var realxpath = stdXpath.Replace("/", "/empty:")
            //                        .Replace("/empty:/empty:", "//empty:");
            // thanks to https://stackoverflow.com/questions/6209841/how-to-use-xpath-with-xdocument
            var nodes = node.XPathSelectElements(stdXpath/*, xnm*/);
            // check because of refactoring
            //if (!node.XPathSelectElements(realxpath, ns).Any() && node.XPathSelectElements(realxpath, xnm).Any())
            return nodes;
        }
        static XmlNamespaceManager xnm = CreateDummyNameSpace();
        static XmlNamespaceManager CreateDummyNameSpace()
        {
            var xnm = new XmlNamespaceManager(new NameTable());
            //xnm.AddNamespace("empty", "http://schemas.microsoft.com/developer/msbuild/2003");
            //xnm.AddNamespace("urn", "urn:schemas-microsoft-com:asm.v1");
            return xnm;
        }

        static string ConvertToStandardXPath(string xpath_no_namespace)
        {
            var stdXpath = new StringBuilder(1000);
            // Sadly syntax like "//*:foo" does not work to say I want foo, anywhere ("//") in any namespace ("*:")
            // So we are using a tip from http://www.john-james-andersen.com/blog/programming/quick-tip-for-ignoring-namespaces-with-xpath.html
            // by converting xpath to "/*[local-name()='foo']"
            // it seems to work if user uses also []
            int i = -1;
            bool newElement = false;
            while (++i < xpath_no_namespace.Length)
            {
                char c = xpath_no_namespace[i];
                if (c == '/')
                {
                    newElement = true;
                    stdXpath.Append(c);
                    continue;
                }
                bool b = newElement;
                newElement = false;
                if (!b)
                {
                    stdXpath.Append(c);
                    continue;
                }
                var localName = new StringBuilder(100);
                --i;
                while (++i < xpath_no_namespace.Length)
                {
                    c = xpath_no_namespace[i];
                    // maybe other chars to add some day
                    if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9' || c == '.')
                        localName.Append(c);
                    else if (c == '/' || c == '[')
                    {
                        stdXpath.Append("*[local-name()='");
                        stdXpath.Append(localName);
                        localName = null;
                        stdXpath.Append("']");
                        --i;
                        break;
                    }
                    else if (c == ':')
                    {
                        stdXpath.Append(localName);
                        --i;
                        break; // do nothing, user wants to handle namespace on its own
                    }
                    else if (c == '*')
                    {
                        if (localName.Length == 0)
                        {
                            if (i + 1 >= xpath_no_namespace.Length || xpath_no_namespace[i + 1] == '/' || xpath_no_namespace[i + 1] == '[' || xpath_no_namespace[i + 1] == '*')
                                stdXpath.Append(c);
                            else if (xpath_no_namespace[i + 1] == ':')
                            {
                                // syntax "/*:foo" is not supported, we make it supported
                                // we just ignore this two characters and we fall back in '/' or '[' case
                                ++i; // skip '*', ':' is skipped because of next iteration
                            }
                            else
                                throw new Exception("Not handled!");
                        }
                        else
                            throw new Exception("unknown case.. is it valid ?");
                    }
                    else
                        throw new Exception("Not handled!");
                }
                if (localName != null)
                {
                    stdXpath.Append("*[local-name()='");
                    stdXpath.Append(localName);
                    stdXpath.Append("']");
                }
            }
            return stdXpath.ToString();
        }
    }
}
