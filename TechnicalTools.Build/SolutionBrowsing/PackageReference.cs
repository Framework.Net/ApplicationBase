﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

using TechnicalTools.Build.Extensions;


namespace TechnicalTools.Build
{
    public class PackageReference
    {
        public string PackageId { get; set; }

        /// <summary>
        /// Version number of package, WARNING it can contains bracket or parenthesis.
        /// See more about that here https://learn.microsoft.com/fr-fr/nuget/concepts/package-versioning
        /// You may want to use <see cref="GreaterVersion"/> to build path for example
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Take the greatest version explicitly indicated:
        /// - if version = "(a,b)" then GreaterVersion is "b"
        /// - if version = "(a,)" then GreaterVersion is "a"
        /// This property has the advantage to clean syntax character which is fine for building path in script.
        /// </summary>
        public string GreaterVersion => Version.Replace("[", "").Replace("]", "").Replace("(", "").Replace(")", "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Last();

        public string PackagePath
        { 
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                                    @".nuget\packages",
                                    PackageId.ToLowerInvariant(),
                                    GreaterVersion.ToLowerInvariant());
            } 
        }


        public string NuspecPath
        {
            get
            {
                return Path.Combine(PackagePath,
                                    PackageId.ToLowerInvariant() + ".nuspec");
            }
        }
        public XDocument NuspecDoc
        {
            get
            {
                return _NuspecDoc ?? (_NuspecDoc = XDocument.Load(NuspecPath));
            }
        }
        XDocument _NuspecDoc;
        public List<string> GetNuspecReferencedDllPaths()
        {
            return NuspecDoc.Root.SimpleXPath("//references").SingleOrDefault()
                                        ?.Elements() // <reference>
                                        .Select(xmlRef => xmlRef.Attribute("file").Value)
                                        .ToList();
        }

        public string PropsPath
        {
            get
            {
                return Directory.EnumerateFiles(PackagePath, "*.props", SearchOption.AllDirectories)
                                .Where(f => !f.Contains(@"\buildTransitive\"))
                                .Where(f => !f.Contains(@"\netcoreapp"))
                                .Where(f => !f.Contains(@"\buildMultiTargeting\"))
                                .Where(f => !f.Contains(@"\uap"))
                                .SingleOrDefault(); // Assuming only one, maybe wrong;
            }
        }
        public XDocument PropsDoc
        {
            get
            {
                return _PropsDoc ?? (PropsPath == null ? null : _PropsDoc = XDocument.Load(PropsPath));
            }
        }
        XDocument _PropsDoc;

        public string TargetPath
        {
            get
            {
                return Directory.EnumerateFiles(PackagePath, "*.targets", SearchOption.AllDirectories)
                                .SingleOrDefault(); // Assuming only one, maybe wrong;
            }
        }
        public XDocument TargetDoc
        {
            get
            {
                return _TargetDoc ?? (TargetPath == null ? null : _TargetDoc = XDocument.Load(TargetPath));
            }
        }
        XDocument _TargetDoc;
    }
}
