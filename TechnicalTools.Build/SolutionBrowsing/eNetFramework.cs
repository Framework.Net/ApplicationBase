﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using TechnicalTools.Extensions;
using TechnicalTools.Model;


namespace TechnicalTools.Build
{
    [DebuggerDisplay("{" + nameof(NormalizedName) + ",nq}")]
    // Enum in the java way for framework. Made with the help of https://learn.microsoft.com/en-us/dotnet/standard/frameworks
    public class eNetFramework
    {
        #region Useful frameworks (use AllFrameworks property to list them all)

        // Old framework <  Old Net Framework 4.5 not implemented on purpose: we don't need them
        // Other framework (Mono / Xamarin / etc) not implemented on purpose: we don't need them

        public static eNetFramework NetStandard10 { get; } = new eNetFramework("netstandard1.0");
        public static eNetFramework NetStandard11 { get; } = new eNetFramework("netstandard1.1");
        public static eNetFramework NetStandard12 { get; } = new eNetFramework("netstandard1.2");
        public static eNetFramework NetStandard13 { get; } = new eNetFramework("netstandard1.3");
        public static eNetFramework NetStandard14 { get; } = new eNetFramework("netstandard1.4");
        public static eNetFramework NetStandard15 { get; } = new eNetFramework("netstandard1.5");
        public static eNetFramework NetStandard16 { get; } = new eNetFramework("netstandard1.6");
        public static eNetFramework NetStandard20 { get; } = new eNetFramework("netstandard2.0");
        public static eNetFramework NetStandard21 { get; } = new eNetFramework("netstandard2.1");

        public static eNetFramework NetFramework20 { get; } = new eNetFramework("net2.0");
        public static eNetFramework NetFramework35 { get; } = new eNetFramework("net3.5");
        public static eNetFramework NetFramework40 { get; } = new eNetFramework("net4.0");
        public static eNetFramework NetFramework45 { get; } = new eNetFramework("net4.5");
        public static eNetFramework NetFramework451 { get; } = new eNetFramework("net4.5.1");
        public static eNetFramework NetFramework452 { get; } = new eNetFramework("net4.5.2");
        public static eNetFramework NetFramework46 { get; } = new eNetFramework("net4.6");
        public static eNetFramework NetFramework461 { get; } = new eNetFramework("net4.6.1");
        public static eNetFramework NetFramework462 { get; } = new eNetFramework("net4.6.2");
        public static eNetFramework NetFramework47 { get; } = new eNetFramework("net4.7");
        public static eNetFramework NetFramework471 { get; } = new eNetFramework("net4.7.1");
        public static eNetFramework NetFramework472 { get; } = new eNetFramework("net4.7.2");
        public static eNetFramework NetFramework48 { get; } = new eNetFramework("net4.8");
        public static eNetFramework NetFramework481 { get; } = new eNetFramework("net4.8.1");

        public static eNetFramework NetCore10 { get; } = new eNetFramework("netcoreapp1.0");
        public static eNetFramework NetCore11 { get; } = new eNetFramework("netcoreapp1.1");
        public static eNetFramework NetCore20 { get; } = new eNetFramework("netcoreapp2.0");
        public static eNetFramework NetCore21 { get; } = new eNetFramework("netcoreapp2.1");
        public static eNetFramework NetCore22 { get; } = new eNetFramework("netcoreapp2.2");
        public static eNetFramework NetCore30 { get; } = new eNetFramework("netcoreapp3.0");
        public static eNetFramework NetCore31 { get; } = new eNetFramework("netcoreapp3.1");

        public static eNetFramework Net50 { get; } = new eNetFramework("net5.0");
        public static eNetFramework Net60 { get; } = new eNetFramework("net6.0");
        public static eNetFramework Net70 { get; } = new eNetFramework("net7.0");
        public static eNetFramework Net80 { get; } = new eNetFramework("net8.0");

        #endregion

        public string DisplayName { get; }

        /// <summary> Name of framework lowercased without dot </summary>
        public string NormalizedName { get; }

        protected eNetFramework(string displayName, string normalizedName = null)
        {
            DisplayName = displayName;
            NormalizedName = Normalize(normalizedName ?? displayName);
        }

        public override string ToString()
        {
            return DisplayName;
        }

        public static IReadOnlyCollection<eNetFramework> AllFrameworks => _byNormalizedNames.Values;

        public static IReadOnlyCollection<eNetFramework> AllOldNetFrameworks => new[] {
            // Keep them in chronological order!
            NetFramework20,
            NetFramework35,
            NetFramework40,
            NetFramework45,
            NetFramework451,
            NetFramework452,
            NetFramework46,
            NetFramework461,
            NetFramework462,
            NetFramework47,
            NetFramework471,
            NetFramework472,
            NetFramework48,
            NetFramework481,
        };
        public static IReadOnlyCollection<eNetFramework> AllNetStandardFrameworks => new[] {
            // Keep them in chronological order!
            NetStandard10,
            NetStandard11,
            NetStandard12,
            NetStandard13,
            NetStandard14,
            NetStandard15,
            NetStandard16,
            NetStandard20,
            NetStandard21,
        };

        public static IReadOnlyCollection<eNetFramework> AllNetCoreFrameworks => new[] {
            // Keep them in chronological order!
            NetCore10,
            NetCore11,
            NetCore20,
            NetCore21,
            NetCore22,
            NetCore30,
            NetCore31,
        };

        public static IReadOnlyCollection<eNetFramework> AllModernNetFrameworks => new[] {
            // Keep them in chronological order!
            Net50,
            Net60,
            Net70,
            Net80,
        };

        public static eNetFramework  GetByNormalizedName(string name)
        {
            return TryGetByNormalizedName(name) 
                ?? throw new TechnicalException($"{name} not recognized as a valid framework (or not yet handled!)");
        }
        public static eNetFramework TryGetByNormalizedName(string name)
        {
            if (_byNormalizedNames.TryGetValue(Normalize(name).Replace("-windows", ""), out var result))
                return result;
            return null;
        }

        static string Normalize(string name)
        {
            return name.Replace(".", "").ToLowerInvariant();
        }

        static Dictionary<string, eNetFramework> _byNormalizedNames { get; } 
            = typeof(eNetFramework).GetProperties(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public)
                                   .Where(p => p.PropertyType == typeof(eNetFramework))
                                   .ToDictionary(p => ((eNetFramework)p.GetValue(null)).NormalizedName, p => (eNetFramework)p.GetValue(null));


        #region Compatibility Matrix

        [Obsolete("Use " + nameof(IsImplementedBy))]
        public virtual bool CanRunOn(eNetFramework runtimeFramework)
        {
            return IsImplementedBy(runtimeFramework);
        }
        public virtual bool IsImplementedBy(eNetFramework runtimeFramework)
        {
            return _compatibilityMatrix[this].Contains(runtimeFramework);
        }
        public virtual bool Implements(eNetFramework runtimeFramework)
        {
            return _compatibilityMatrix[runtimeFramework].Contains(this);
        }
        public virtual eNetFramework GetBestImplementedFrameworkIn(IReadOnlyCollection<eNetFramework> runtimeFrameworks)
        {
            eNetFramework best = null;
            foreach (var f in runtimeFrameworks)
                if (Implements(f) && (best == null || f.Implements(best)))
                    best = f;
            return best;
        }

        /// <summary>
        /// A matrix so that code written for a "key" framework (ex: netstandard2.0) is compatible with framework net461, net462, etc.
        /// or said in another way, "Key framework is implemented by values frameworks"
        /// Only intuitive relations are in this matrix.
        /// For example the fact that, at runtime, a net7.0 dll/project can use another dll/project compiled _only_ for net48 is not allowed in this matrix!
        /// It would mean framework net4.8 is implemented by framework net7.0
        /// </summary>
        /// <remarks> Made with help of https://learn.microsoft.com/en-us/dotnet/standard/net-standard + my knowledge </remarks>
        static readonly Dictionary<eNetFramework, HashSet<eNetFramework>> _compatibilityMatrix = new Dictionary<eNetFramework, eNetFramework[]>()
        {
            { NetStandard10, new[] { NetFramework45, NetFramework451, NetFramework452,
                                     NetFramework46, NetFramework461, NetFramework462,
                                     NetFramework47, NetFramework471, NetFramework472,
                                     NetFramework48, NetFramework481,
                                     NetCore10, NetCore11,
                                     NetCore20, NetCore21, NetCore22,
                                     NetCore30, NetCore31,
                                     Net50,
                                     Net60,
                                     Net70,
                                     Net80,
            } },
            { NetStandard11, new[] { NetFramework45, NetFramework451, NetFramework452,
                                     NetFramework46, NetFramework461, NetFramework462,
                                     NetFramework47, NetFramework471, NetFramework472,
                                     NetFramework48, NetFramework481,
                                     NetCore10, NetCore11,
                                     NetCore20, NetCore21, NetCore22,
                                     NetCore30, NetCore31,
                                     Net50,
                                     Net60,
                                     Net70,
                                     Net80,
            } },
            { NetStandard12, new[] { NetFramework451, NetFramework452,
                                     NetFramework46, NetFramework461, NetFramework462,
                                     NetFramework47, NetFramework471, NetFramework472,
                                     NetFramework48, NetFramework481,
                                     NetCore10, NetCore11,
                                     NetCore20, NetCore21, NetCore22,
                                     NetCore30, NetCore31,
                                     Net50,
                                     Net60,
                                     Net70,
                                     Net80,
            } },
            { NetStandard13, new[] { NetFramework46, NetFramework461, NetFramework462,
                                     NetFramework47, NetFramework471, NetFramework472,
                                     NetFramework48, NetFramework481,
                                     NetCore10, NetCore11,
                                     NetCore20, NetCore21, NetCore22,
                                     NetCore30, NetCore31,
                                     Net50,
                                     Net60,
                                     Net70,
                                     Net80,
            } },
            { NetStandard14, new[] { NetFramework461, NetFramework462,
                                     NetFramework47, NetFramework471, NetFramework472,
                                     NetFramework48, NetFramework481,
                                     NetCore10, NetCore11,
                                     NetCore20, NetCore21, NetCore22,
                                     NetCore30, NetCore31,
                                     Net50,
                                     Net60,
                                     Net70,
                                     Net80,
            } },
            { NetStandard15, new[] { /*NetFramework461 disabled because not enough safe in my opinion see microsoft doc, */ NetFramework462,
                                     NetFramework47, NetFramework471, NetFramework472,
                                     NetFramework48, NetFramework481,
                                     NetCore10, NetCore11,
                                     NetCore20, NetCore21, NetCore22,
                                     NetCore30, NetCore31,
                                     Net50,
                                     Net60,
                                     Net70,
                                     Net80,
            } },
            { NetStandard16, new[] { /*NetFramework461 disabled because not enough safe in my opinion see microsoft doc, */ NetFramework462,
                                     NetFramework47, NetFramework471, NetFramework472,
                                     NetFramework48, NetFramework481,
                                     NetCore10, NetCore11,
                                     NetCore20, NetCore21, NetCore22,
                                     NetCore30, NetCore31,
                                     Net50,
                                     Net60,
                                     Net70,
                                     Net80,
            } },
            { NetStandard20, new[] { /*NetFramework461 disabled because not enough safe in my opinion see microsoft doc, */ NetFramework462,
                                     NetFramework47, NetFramework471, NetFramework472,
                                     NetFramework48, NetFramework481,
                                     NetCore20, NetCore21, NetCore22,
                                     NetCore30, NetCore31,
                                     Net50,
                                     Net60,
                                     Net70,
                                     Net80,
            } },
            { NetStandard21, new[] { NetCore30, NetCore31,
                                     Net50,
                                     Net60,
                                     Net70,
                                     Net80,
            } },
        }.ToDictionary(kvp => kvp.Key, kvp => new[] { kvp.Key }.Concat(kvp.Value).ToHashSet())
         .OverwriteWith(AllOldNetFrameworks.Select((f, i) => (f, i))
                                           .ToDictionary(t => t.f, t => AllOldNetFrameworks.Skip(t.i).ToHashSet()))
         .OverwriteWith(AllNetCoreFrameworks.Select((f, i) => (f, i))
                                            .ToDictionary(t => t.f, t => AllNetCoreFrameworks.Skip(t.i).ToHashSet()))
         .OverwriteWith(AllModernNetFrameworks.Select((f, i) => (f, i))
                                              .ToDictionary(t => t.f, t => AllModernNetFrameworks.Skip(t.i).ToHashSet()));

        #endregion Compatibility Matrix
    }
}
