﻿using System;
using System.Diagnostics;


namespace TechnicalTools.Build
{
    [DebuggerDisplay("{" + nameof(Source) + "." + nameof(Project.NameInSln) + " => " + nameof(Package) + "." + nameof(PackageReference.PackageId) + " " + nameof(PackageReference.Version) + ",nq}")]
    public class ProjectToPackageReferenceDependency : ProjectDependency
    {
        /// <summary> Source depends on Target </summary>
        public Project Source { get; init; }
        /// <summary> Target is a dependency of Source </summary>
        public PackageReference Package { get; init; }

        public override int GetHashCode()
        {
            return (Source.FileName.GetHashCode() * 397) ^ Package.PackageId.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return obj is ProjectToPackageReferenceDependency pprd
                // works because Project instance are singletons
                && pprd.Source == Source
                && pprd.Package == Package
                && pprd.Package.Version == Package.Version;
        }
    }
}
