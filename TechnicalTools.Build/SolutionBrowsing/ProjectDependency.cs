﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

using TechnicalTools.Build.Extensions;


namespace TechnicalTools.Build
{
    public abstract class ProjectDependency
    {
        public static IEnumerable<ProjectDependency> ReadDependancies(Project project)
        {
            var lst = project.IsDocAboutWeb
                    ? ReadDependanciesInSln(project, project.Doc.Root).ToList()
                    : ReadDependanciesIn(project, project.Doc.Root).ToList();
            foreach (var choose in project.Doc.Root.Elements("Choose"))
            {
                var whens = choose.Elements("When").Concat(choose.Elements("Otherwise")).ToList();

                foreach (var platform in new[] { ePlatform.X64, ePlatform.AnyCpu })
                    foreach (var conf in new[] { eConfig.Debug, eConfig.Release })
                    {
                        var goodCase = whens.FirstOrDefault(when => IsElementValidFor(when, platform, conf));
                        if (goodCase != null)
                        {
                            var additional = ReadDependanciesIn(project, goodCase).ToList();
                            lst.AddRange(additional);
                            return lst;
                        }
                    }
            }
            return lst;
        }

        public static IEnumerable<ProjectDependency> ReadDependanciesIn(Project project, XElement root, ePlatform platform = null, eConfig conf = null)
        {
            platform = platform ?? ePlatform.X64;
            conf = conf ?? eConfig.Debug;
            // thanks to https://stackoverflow.com/questions/6209841/how-to-use-xpath-with-xdocument
            var sln = project.ProjectReference.Solution;
            var projectRefs = root.SimpleXPath("./ItemGroup/ProjectReference").ToList();
            foreach (var projectRef in projectRefs)
                if (IsElementValidFor(projectRef, platform, conf))
                {
                    var includeAtt = projectRef.Attribute("Include");
                    var refProjectFullPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(sln.FilePath),
                                                                           Path.GetDirectoryName(project.RelativeToSolutionPath), 
                                                                           includeAtt.Value));
                    refProjectFullPath = refProjectFullPath.Replace("mint_repo_dotnet", "mint2");
                    var refProject = sln.ProjectReferences
                                        .Single(p => Path.GetFullPath(Path.Combine(Path.GetDirectoryName(sln.FilePath), p.Project.RelativeToSolutionPath)).ToLowerInvariant() == refProjectFullPath.ToLowerInvariant())
                                        .Project;
                    yield return new ProjectToProjectDependency() { Source = project, Target = refProject };
                }

            foreach (var packageRef in root.SimpleXPath("./ItemGroup/PackageReference"))
                if (IsElementValidFor(packageRef, platform, conf))
                    yield return new ProjectToPackageReferenceDependency()
                    {
                        Source = project,
                        Package = new PackageReference()
                        {
                            PackageId = packageRef.Attribute("Include").Value,
                            Version = packageRef.Attribute("Version")?.Value 
                                   ?? packageRef.Attribute("version")?.Value 
                                   ?? packageRef.SimpleXPath("./Version").SingleOrDefault()?.Value
                        }
                    };

            if (root != project.Doc.Root) // because these one are well handled by roslyn
            {
                foreach (var refDep in GetConditionalReferences(root, Path.GetDirectoryName(project.FullPath), platform, conf))
                    yield return new ProjectToConditionalReferenceDependency()
                    {
                        Source = project,
                        Reference = refDep
                    };
            }
        }

        public static IEnumerable<ConditionalReference> GetConditionalReferences(XElement elt, string currentDirectory, ePlatform platform, eConfig conf = null, bool allowEmptyCondition = true)
        {
            conf = conf ?? eConfig.Release;
            var dynamicRef = elt.SimpleXPath("./ItemGroup/Reference")
                                .Where(r => IsElementValidFor(r, platform, conf, allowEmptyCondition))
                                .ToList();
            if (dynamicRef.Count > 0)
                foreach (var refElt in dynamicRef)
                {
                    var name = refElt.Attribute("Include").Value;
                    var hintPaths = refElt.SimpleXPath("./HintPath");
                    var path = hintPaths.SingleOrDefault()?.Value;
                    if (path == null)
                        continue; // not interesting, because outside solution folder
                    path = path.Replace("$(MSBuildThisFileDirectory)", currentDirectory);
                    path = Path.GetFullPath(Path.Combine(currentDirectory, path));
                    yield return new ConditionalReference()
                    {
                        Name = name,
                        Path = path
                    };
                }
        }
        public static bool IsElementValidFor(XElement elt, ePlatform platform, eConfig conf = null, bool allowEmptyCondition = true)
        {
            conf = conf ?? eConfig.Release;
            // This is the dirty list of all condition known
            // Is there a way to dynamically evaluate this ?
            return string.IsNullOrWhiteSpace(elt.Attribute("Condition")?.Value)
                || elt.Attribute("Condition")?.Value == "'$(Platform)' == '" + platform + "'"
                || elt.Attribute("Condition")?.Value == "'$(Configuration)' == '"+ conf + "' AND '$(Platform)' == '" + platform + "'"
                || elt.Attribute("Condition")?.Value == "'$(Platform)' == '" + platform + "' OR ('$(Platform)' == 'AnyCpu' AND '$(Prefer32Bit)' " + (platform == ePlatform.X64 ? "!=" : "==") + " 'true')";
        }

        public static IEnumerable<ProjectDependency> ReadDependanciesInSln(Project project, XElement root, ePlatform platform = null, eConfig conf = null)
        {
            platform = platform ?? ePlatform.X64;
            conf = conf ?? eConfig.Debug;

            var sln = project.ProjectReference.Solution;
            var lines = File.ReadAllLines(sln.FilePath)
                .SkipUntil(line => line.StartsWith("Project") && line.ToLowerInvariant().Contains(project.FileName) && line.ToUpper().Contains(project.Guid.ToString().ToUpper()), false)
                .TakeUntil(line => line.StartsWith("EndProject"), true)
                .ToList();
            var prLine = lines.SingleOrDefault(line => line.Contains("ProjectReferences = "));
            if (prLine != null)
                prLine = prLine.Remove(prLine.LastIndexOf('"')).Substring(prLine.IndexOf('"') + 1);
            var byGuids = sln.Projects.ToDictionary(p => p.Guid);
            var projectRefs = prLine.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                                    .Select(pr => Guid.Parse(pr.Split('|')[0]))
                                    .Select(guid => byGuids[guid])
                                    .Select(p => new ProjectToProjectDependency() { Source = project, Target = p })
                                    .ToList();
            IEnumerable<ProjectDependency> results = projectRefs;

            var packagesFile = Path.Combine(project.FullPath, "packages.config");
            if (File.Exists(packagesFile))
            {
                var xdoc = XDocument.Load(packagesFile, LoadOptions.PreserveWhitespace | LoadOptions.SetLineInfo);
                var packageRefs = xdoc.SimpleXPath("//package")
                                      .Select(p => new PackageReference() { PackageId = p.Attribute("id").Value, Version = p.Attribute("version").Value })
                                      .Select(p => new ProjectToPackageReferenceDependency() { Source = project, Package = p })
                                      .ToList();
                results = results.Concat(packageRefs);
            }
            // Quid des reference dll en dur ?
            // Quid du contenu de Web.Config ex: <compilation debug="true" targetFramework="4.8"> / < assemblies > / <add assembly="netstandard, Version=2.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51" />

            return results;
        }
    }
}
