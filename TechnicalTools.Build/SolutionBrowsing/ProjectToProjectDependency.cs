﻿using System;
using System.Diagnostics;


namespace TechnicalTools.Build
{
    [DebuggerDisplay("{" + nameof(Source) + "." + nameof(Project.FileName) + " + \" => \" + "  +
                           nameof(Target) + "." + nameof(Project.FileName) + ",nq}")]
    public class ProjectToProjectDependency : ProjectDependency
    {
        /// <summary> Source object references (or "depends on") Target object</summary>
        public Project Source { get; init; }
        /// <summary> Target object is a dependency of Source object</summary>
        public Project Target { get; init; }

        public override int GetHashCode()
        {
            return (Source.FileName.GetHashCode() * 397) ^ Target.FileName.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return obj is ProjectToProjectDependency ppd
                // works because Project instance are singletons
                && ppd.Source == Source
                && ppd.Target == Target;
        }
    }
}
