﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;


namespace TechnicalTools.Build
{
    [DebuggerDisplay("{" + nameof(Name) + ",nq}")]
    public class ProjectType
    {
        public string Name { get; }
        public Guid Guid { get; }

        private ProjectType(Guid guid, string name)
        {
            Guid = guid;
            Name = name;
        }
        private ProjectType(string guid, string name) : this(new Guid(guid), name) { }

        public static ProjectType ByGuid(Guid guid)
        {
            if (All.TryGetValue(guid, out ProjectType pt))
                return pt;
            return CreateAndAdd(guid);
        }
        static ProjectType CreateAndAdd(Guid guid)
        {
            lock (All)
            {
                if (All.TryGetValue(guid, out ProjectType pt))
                    return pt;
                var newName = "Unknown " + Interlocked.Increment(ref _unknownSeed).ToString(CultureInfo.InvariantCulture);
                pt = new ProjectType(guid, newName);
                All.TryAdd(pt.Guid, pt);
                return pt;
            }
        }
        static int _unknownSeed;

        public static readonly ProjectType SolutionFolder = new ProjectType("2150E333-8FDC-42A3-9474-1A3956D46DE8", "Solution Folder");
        // It seems 9A19103F is the prefix for all new projects types
        public static readonly ProjectType ModernProject = new ProjectType("9A19103F-16F7-4668-BE54-9A1E7A4F7556", "C# Project with modern SDK .csproj format");
        // It seems FAE04EC0 is the prefix for all old projects types
        public static readonly ProjectType OldProject = new ProjectType("FAE04EC0-301F-11D3-BF4B-00C04F79EFBC", "C# Project with old .csproj format");

        // From https://www.codeproject.com/Reference/720512/List-of-Visual-Studio-Project-Type-GUIDs
        // which is now https://github.com/JamesW75/visual-studio-project-type-guid?tab=readme-ov-file
        // Copy all values, epurate each line, then use regex
        //   (.*[^ \t])\s*\{([^}]*)\}
        // and replace with
        //   new ProjectType\("\2", "\1"\);
        // Now you have a array of ProjectType (duplicates are handled at the end of init),
        // do a foreach on it, and generate variable named like this:
        // foreach (var pt in ... )
        // {
        //   var asPublicStaticpropertyName = Regex.Replace(pt.Name.Replace("#", "Sharp")
        //                                                         .Replace(".", "")
        //                                                         .Replace("+", "p")
        //                                                         .Replace("(", "_")
        //                                                         .Replace("&", "And")
        //                                                         .Replace("/", "Or"),
        //                                                  "[^A-Za-z0-9_]", "")
        // }
        static readonly ConcurrentDictionary<Guid, ProjectType> All = new ConcurrentDictionary<Guid, ProjectType>(new [] {
            // By Description
            new ProjectType("8BB2217D-0F2D-49D1-97BC-3654ED321F3B", "ASP.NET 5"),
            new ProjectType("603C0E0B-DB56-11DC-BE95-000D561079B0", "ASP.NET MVC 1"),
            new ProjectType("F85E285D-A4E0-4152-9332-AB1D724D3325", "ASP.NET MVC 2"),
            new ProjectType("E53F8FEA-EAE0-44A6-8774-FFD645390401", "ASP.NET MVC 3"),
            new ProjectType("E3E379DF-F4C6-4180-9B81-6769533ABE47", "ASP.NET MVC 4"),
            new ProjectType("349C5851-65DF-11DA-9384-00065B846F21", "ASP.NET MVC 5"),
            OldProject,
            ModernProject,
            new ProjectType("8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942", "C++"),
            new ProjectType("A9ACE9BB-CECE-4E62-9AA4-C7E7C5BD2124", "Database"),
            new ProjectType("4F174C21-8C12-11D0-8340-0000F80270F8", "Database (other project types)"),
            new ProjectType("3EA9E505-35AC-4774-B492-AD1749C4943A", "Deployment Cab"),
            new ProjectType("06A35CCD-C46D-44D5-987B-CF40FF872267", "Deployment Merge Module"),
            new ProjectType("978C614F-708E-4E1A-B201-565925725DBA", "Deployment Setup"),
            new ProjectType("AB322303-2255-48EF-A496-5904EB18DA55", "Deployment Smart Device Cab"),
            new ProjectType("F135691A-BF7E-435D-8960-F99683D2D49C", "Distributed System"),
            new ProjectType("BF6F8E12-879D-49E7-ADF0-5503146B24B8", "Dynamics 2012 AX C# in AOT"),
            new ProjectType("F2A71F9B-5D33-465A-A702-920D77279786", "F#"),
            new ProjectType("E6FDF86B-F3D1-11D4-8576-0002A516ECE8", "J#"),
            new ProjectType("20D4826A-C6FA-45DB-90F4-C717570B9F32", "Legacy (2003) Smart Device (C#)"),
            new ProjectType("CB4CE8C6-1BDB-4DC7-A4D3-65A1999772F8", "Legacy (2003) Smart Device (VB.NET)"),
            new ProjectType("b69e3092-b931-443c-abe7-7e7b65f2a37f", "Micro Framework"),
            new ProjectType("F85E285D-A4E0-4152-9332-AB1D724D3325", "Model-View-Controller v2 (MVC 2)"),
            new ProjectType("E53F8FEA-EAE0-44A6-8774-FFD645390401", "Model-View-Controller v3 (MVC 3)"),
            new ProjectType("E3E379DF-F4C6-4180-9B81-6769533ABE47", "Model-View-Controller v4 (MVC 4)"),
            new ProjectType("349C5851-65DF-11DA-9384-00065B846F21", "Model-View-Controller v5 (MVC 5)"),
            new ProjectType("EFBA0AD7-5A72-4C68-AF49-83D382785DCF", "Mono for Android"),
            new ProjectType("6BC8ED88-2882-458C-8E55-DFD12B67127B", "MonoTouch"),
            new ProjectType("F5B4F3BC-B597-4E2B-B552-EF5D8A32436F", "MonoTouch Binding"),
            new ProjectType("786C830F-07A1-408B-BD7F-6EE04809D6DB", "Portable Class Library"),
            new ProjectType("66A26720-8FB5-11D2-AA7E-00C04F688DDE", "Project Folders"),
            new ProjectType("593B0543-81F6-4436-BA1E-4747859CAAE2", "SharePoint (C#)"),
            new ProjectType("EC05E597-79D4-47f3-ADA0-324C4F7C7484", "SharePoint (VB.NET)"),
            new ProjectType("F8810EC1-6754-47FC-A15F-DFABD2E3FA90", "SharePoint Workflow"),
            new ProjectType("A1591282-1198-4647-A2B1-27E5FF5F6F3B", "Silverlight"),
            new ProjectType("4D628B5B-2FBC-4AA6-8C16-197242AEB884", "Smart Device (C#)"),
            new ProjectType("68B1623D-7FB9-47D8-8664-7ECEA3297D4F", "Smart Device (VB.NET)"),
            SolutionFolder,
            new ProjectType("3AC096D0-A1C2-E12C-1390-A8335801FDAB", "Test"),
            new ProjectType("A5A43C5B-DE2A-4C0C-9213-0A381AF9435A", "Universal Windows Class Library"),
            new ProjectType("F184B08F-C81C-45F6-A57F-5ABD9991F28F", "VB.NET"),
            new ProjectType("C252FEB5-A946-4202-B1D4-9916A0590387", "Visual Database Tools"),
            new ProjectType("54435603-DBB4-11D2-8724-00A0C9A8B90C", "Visual Studio 2015 Installer Project Extension"),
            new ProjectType("A860303F-1F3F-4691-B57E-529FC101A107", "Visual Studio Tools for Applications (VSTA)"),
            new ProjectType("BAA0C2D2-18E2-41B9-852F-F413020CAA33", "Visual Studio Tools for Office (VSTO)"),
            new ProjectType("349C5851-65DF-11DA-9384-00065B846F21", "Web Application"),
            new ProjectType("E24C65DC-7377-472B-9ABA-BC803B73C61A", "Web Site"),
            new ProjectType("FAE04EC0-301F-11D3-BF4B-00C04F79EFBC", "Windows (C#)"),
            new ProjectType("F184B08F-C81C-45F6-A57F-5ABD9991F28F", "Windows (VB.NET)"),
            new ProjectType("8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942", "Windows (Visual C++)"),
            new ProjectType("3D9AD99F-2412-4246-B90B-4EAA41C64699", "Windows Communication Foundation (WCF)"),
            new ProjectType("76F1466A-8B6D-4E39-A767-685A06062A39", "Windows Phone 8/8.1 Blank/Hub/Webview App"),
            new ProjectType("C089C8C0-30E0-4E22-80C0-CE093F111A43", "Windows Phone 8/8.1 App (C#)"),
            new ProjectType("DB03555F-0C8B-43BE-9FF9-57896B3C5E56", "Windows Phone 8/8.1 App (VB.NET)"),
            new ProjectType("60DC8134-EBA5-43B8-BCC9-BB4BC16C2548", "Windows Presentation Foundation (WPF)"),
            new ProjectType("BC8A1FFA-BEE3-4634-8014-F334798102B3", "Windows Store (Metro) Apps & Components"),
            new ProjectType("14822709-B5A1-4724-98CA-57A101D1B079", "Workflow (C#)"),
            new ProjectType("D59BE175-2ED0-4C54-BE3D-CDAA9F3214C8", "Workflow (VB.NET)"),
            new ProjectType("32F31D43-81CC-4C15-9DE6-3FC5453562B6", "Workflow Foundation"),
            new ProjectType("EFBA0AD7-5A72-4C68-AF49-83D382785DCF", "Xamarin.Android"),
            new ProjectType("6BC8ED88-2882-458C-8E55-DFD12B67127B", "Xamarin.iOS"),
            new ProjectType("6D335F3A-9D43-41b4-9D22-F6F17C4BE596", "XNA (Windows)"),
            new ProjectType("2DF5C3F4-5A5F-47a9-8E94-23B4456F55E2", "XNA (XBox)"),
            new ProjectType("D399B71A-8929-442a-A9AC-8BEC78BB2433", "XNA (Zune)"),

            // By GUID
            new ProjectType("06A35CCD-C46D-44D5-987B-CF40FF872267", "Deployment Merge Module"),
            new ProjectType("14822709-B5A1-4724-98CA-57A101D1B079", "Workflow (C#)"),
            new ProjectType("20D4826A-C6FA-45DB-90F4-C717570B9F32", "Legacy (2003) Smart Device (C#)"),
            SolutionFolder,
            new ProjectType("2DF5C3F4-5A5F-47a9-8E94-23B4456F55E2", "XNA (XBox)"),
            new ProjectType("32F31D43-81CC-4C15-9DE6-3FC5453562B6", "Workflow Foundation"),
            new ProjectType("349C5851-65DF-11DA-9384-00065B846F21", "Web Application (incl. MVC 5)"),
            new ProjectType("3AC096D0-A1C2-E12C-1390-A8335801FDAB", "Test"),
            new ProjectType("3D9AD99F-2412-4246-B90B-4EAA41C64699", "Windows Communication Foundation (WCF)"),
            new ProjectType("3EA9E505-35AC-4774-B492-AD1749C4943A", "Deployment Cab"),
            new ProjectType("4D628B5B-2FBC-4AA6-8C16-197242AEB884", "Smart Device (C#)"),
            new ProjectType("4F174C21-8C12-11D0-8340-0000F80270F8", "Database (other project types)"),
            new ProjectType("54435603-DBB4-11D2-8724-00A0C9A8B90C", "Visual Studio 2015 Installer Project Extension"),
            new ProjectType("593B0543-81F6-4436-BA1E-4747859CAAE2", "SharePoint (C#)"),
            new ProjectType("603C0E0B-DB56-11DC-BE95-000D561079B0", "ASP.NET MVC 1.0"),
            new ProjectType("60DC8134-EBA5-43B8-BCC9-BB4BC16C2548", "Windows Presentation Foundation (WPF)"),
            new ProjectType("68B1623D-7FB9-47D8-8664-7ECEA3297D4F", "Smart Device (VB.NET)"),
            new ProjectType("66A26720-8FB5-11D2-AA7E-00C04F688DDE", "Project Folders"),
            new ProjectType("6BC8ED88-2882-458C-8E55-DFD12B67127B", "MonoTouch"),
            new ProjectType("6D335F3A-9D43-41b4-9D22-F6F17C4BE596", "XNA (Windows)"),
            new ProjectType("76F1466A-8B6D-4E39-A767-685A06062A39", "Windows Phone 8/8.1 Blank/Hub/Webview App"),
            new ProjectType("786C830F-07A1-408B-BD7F-6EE04809D6DB", "Portable Class Library"),
            new ProjectType("8BB2217D-0F2D-49D1-97BC-3654ED321F3B", "ASP.NET 5"),
            new ProjectType("8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942", "C++"),
            new ProjectType("978C614F-708E-4E1A-B201-565925725DBA", "Deployment Setup"),
            new ProjectType("A1591282-1198-4647-A2B1-27E5FF5F6F3B", "Silverlight"),
            new ProjectType("A5A43C5B-DE2A-4C0C-9213-0A381AF9435A", "Universal Windows Class Library"),
            new ProjectType("A860303F-1F3F-4691-B57E-529FC101A107", "Visual Studio Tools for Applications (VSTA)"),
            new ProjectType("A9ACE9BB-CECE-4E62-9AA4-C7E7C5BD2124", "Database"),
            new ProjectType("AB322303-2255-48EF-A496-5904EB18DA55", "Deployment Smart Device Cab"),
            new ProjectType("B69E3092-B931-443C-ABE7-7E7B65F2A37F", "Micro Frmework"),
            new ProjectType("BAA0C2D2-18E2-41B9-852F-F413020CAA33", "Visual Studio Tools for Office (VSTO)"),
            new ProjectType("BC8A1FFA-BEE3-4634-8014-F334798102B3", "Windows Store Apps (Metro Apps)"),
            new ProjectType("BF6F8E12-879D-49E7-ADF0-5503146B24B8", "C# in Dynamics 2012 AX AOT"),
            new ProjectType("C089C8C0-30E0-4E22-80C0-CE093F111A43", "Windows Phone 8/8.1 App (C#)"),
            new ProjectType("C252FEB5-A946-4202-B1D4-9916A0590387", "Visual Database Tools"),
            new ProjectType("CB4CE8C6-1BDB-4DC7-A4D3-65A1999772F8", "Legacy (2003) Smart Device (VB.NET)"),
            new ProjectType("D399B71A-8929-442a-A9AC-8BEC78BB2433", "XNA (Zune)"),
            new ProjectType("D59BE175-2ED0-4C54-BE3D-CDAA9F3214C8", "Workflow (VB.NET)"),
            new ProjectType("DB03555F-0C8B-43BE-9FF9-57896B3C5E56", "Windows Phone 8/8.1 App (VB.NET)"),
            new ProjectType("E24C65DC-7377-472B-9ABA-BC803B73C61A", "Web Site"),
            new ProjectType("E3E379DF-F4C6-4180-9B81-6769533ABE47", "ASP.NET MVC 4.0"),
            new ProjectType("E53F8FEA-EAE0-44A6-8774-FFD645390401", "ASP.NET MVC 3.0"),
            new ProjectType("E6FDF86B-F3D1-11D4-8576-0002A516ECE8", "J#"),
            new ProjectType("EC05E597-79D4-47f3-ADA0-324C4F7C7484", "SharePoint (VB.NET)"),
            new ProjectType("EFBA0AD7-5A72-4C68-AF49-83D382785DCF", "Xamarin.Android / Mono for Android"),
            new ProjectType("F135691A-BF7E-435D-8960-F99683D2D49C", "Distributed System"),
            new ProjectType("F184B08F-C81C-45F6-A57F-5ABD9991F28F", "VB.NET"),
            new ProjectType("F2A71F9B-5D33-465A-A702-920D77279786", "F#"),
            new ProjectType("F5B4F3BC-B597-4E2B-B552-EF5D8A32436F", "MonoTouch Binding"),
            new ProjectType("F85E285D-A4E0-4152-9332-AB1D724D3325", "ASP.NET MVC 2.0"),
            new ProjectType("F8810EC1-6754-47FC-A15F-DFABD2E3FA90", "SharePoint Workflow"),
            new ProjectType("FAE04EC0-301F-11D3-BF4B-00C04F79EFBC", "C#"),
        }.GroupBy(pt => pt.Guid).ToDictionary(grp => grp.Key, grp => grp.OrderByDescending(pt => pt.Name.Length).First()));
    }
}
