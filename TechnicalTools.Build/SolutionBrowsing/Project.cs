﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;

using TechnicalTools.Build.Extensions;


namespace TechnicalTools.Build
{
    [DebuggerDisplay("{" + nameof(FileName) + ",nq}")]
    public class Project
    {
        public Solution Solution { get { return ProjectReference.Solution; } }
        public SlnProjectReference ProjectReference { get; }

        /// <summary> Full path to csproj file </summary>
        public string FullPath { get { return Path.Combine(Solution.SolutionDir, RelativeToSolutionPath); } }
        /// <summary> Relative path to csproj file (including csproj file in path)</summary>
        public string RelativeToSolutionPath { get; }
        /// <summary> Relative path to folder containing csproj file (ie: csproj file is not in path)</summary>
        public string RelativeToSolutionFolderPath { get { return Path.GetDirectoryName(RelativeToSolutionPath); } }
        /// <summary> Name without extension of csproj file </summary>
        public string FileName => IsDocAboutWeb ? Path.GetFileName(Path.GetDirectoryName(FullPath)).ToLowerInvariant() : Path.GetFileNameWithoutExtension(RelativeToSolutionPath);

        /// <summary> Name defined inside sln file, as it is shown to user </summary>
        public string NameInSln { get { return ProjectReference?.ProjectName ?? AssemblyName; } }

        public IEnumerable<ProjectDependency> Dependencies { get { return ProjectDependency.ReadDependancies(this); } }
        public IEnumerable<ProjectToProjectDependency> ProjectDependencies
        {
            get { return _ProjectDependencies ?? (_ProjectDependencies = Dependencies.OfType<ProjectToProjectDependency>()); }
            set { _ProjectDependencies = value; }
        }
        IEnumerable<ProjectToProjectDependency> _ProjectDependencies;

        public IEnumerable<ProjectToPackageReferenceDependency> PackageDependencies
        {
            get { return _PackageDependencies ?? (_PackageDependencies = Dependencies.OfType<ProjectToPackageReferenceDependency>()); }
            set { _PackageDependencies = value; }
        }
        IEnumerable<ProjectToPackageReferenceDependency> _PackageDependencies;

        public IEnumerable<ProjectToConditionalReferenceDependency> ConditionalReferences
        {
            get { return _ConditionalReferences ?? (_ConditionalReferences = Dependencies.OfType<ProjectToConditionalReferenceDependency>()); }
            set { _ConditionalReferences = value; }
        }
        IEnumerable<ProjectToConditionalReferenceDependency> _ConditionalReferences;

        // Does not parse everything (it would be too much work).
        // We parse only thing we are interesting in
        public Project(SlnProjectReference slnRef, string relativeProjPath)
        {
            ProjectReference = slnRef;
            RelativeToSolutionPath = relativeProjPath;
            IsDocAboutWeb = false;
            if (Directory.Exists(FullPath))
            {
                IsDocAboutWeb = true;
                var webConfigFile = Path.Combine(FullPath, "Web.Config");
                if (File.Exists(webConfigFile))
                    Doc = Doc = XDocument.Load(webConfigFile, LoadOptions.PreserveWhitespace | LoadOptions.SetLineInfo);
                else
                    Doc = new XDocument();
            }
            else
                Doc = XDocument.Load(FullPath, LoadOptions.PreserveWhitespace | LoadOptions.SetLineInfo);
        }
        public bool IsDocAboutWeb { get; }

        /// <summary> Get the underlying xml parsing result of csproj file </summary>
        public XDocument Doc { get; }


        // Can be complex. See https://docs.microsoft.com/en-us/dotnet/core/project-sdk/overview
        public eNetSDK SDKType
        {
            get
            {
                var value = Doc.Root.Attribute("Sdk")?.Value ?? "";
                var eNetSDK = XmlValueAttribute_Extensions.FromXmlValue<eNetSDK>(value);

                //// If it crash, we can use this :
                //var seemsModern = Doc.Descendants().Any(elt => elt.Name.LocalName.ToLowerInvariant().In(specificXmlElementNameOfSdkFormat));
                //if (!seemsModern)
                //    foreach (var (file, root) in Solution.ForEachDirectoryBuildProps(this))
                //        if (root.Descendants().Any(elt => elt.Name.LocalName.ToLowerInvariant().In(specificXmlElementNameOfSdkFormat)))
                //        {
                //            seemsModern = true;
                //            break;
                //        }
                //if (seemsModern != (eNetSDK != eNetSDK.None))
                //    throw new NotImplementedException("Unexpected case never seen, to study!");
                return eNetSDK;
            }
        }
        // "classic" (ie: old) projects contain <TargetFrameworkVersion> instead
        static readonly string[] specificXmlElementNameOfSdkFormat = new[] { "TargetFramework".ToLowerInvariant(), "TargetFrameworks".ToLowerInvariant() };

        public Guid Guid
        {
            get
            {
                var projectGuid = Doc.SimpleXPath("//ProjectGuid").Select(r => r.Value).Distinct().SingleOrDefault();
                if (projectGuid == null) 
                {
                    if (ProjectReference == null)
                        throw new Exception($"is that possible ? Microsoft gave up guid for identifying project ?");
                    // Microsoft just dropped the guid being mandatory in csproj, they can be only in sln now
                    // For compatibility with old csproj parsing we return this one
                    return ProjectReference.Guid;
                }
                if (ProjectReference != null &&
                    new Guid(projectGuid) != ProjectReference.Guid)
                    throw new Exception($"Inconsistent guid detected in sln file {ProjectReference.Solution.FilePath} regarding project file {FileName}!");
                return new Guid(projectGuid);
            }
        }

        public IEnumerable<string> RootNamespace
        {
            get
            {
                var rootNamespace = Doc.SimpleXPath("//RootNamespace") // can exist multiple times in tag PropertyGroup (with condition = Configuration or Debug etc)
                                     .Select(r => r.Value)
                                     .Distinct()
                                     .SingleOrDefault();
                var res = rootNamespace == null && SDKType == eNetSDK.Microsoft_NET_Sdk
                        ? FileName.Split('.') // Copy behavior of visual studio: namespace is name of csproj if not in xml
                        : (rootNamespace == null ? new[] { FileName }
                                              : rootNamespace.Split('.'));  // root should exist on other case (but I met some project without rootnamespace)
                return res;
            }
        }

        public eProjectOutputType OutputType
        {
            get
            {
                var v = Doc.SimpleXPath("//OutputType") // can exist multiple time in tag PropertyGroup (with condition = Configuration or Debug etc)
                            .Select(r => r.Value)
                            .Distinct()
                            .SingleOrDefault();
                if (IsDocAboutWeb)
                    return eProjectOutputType.ConsoleApplication;
                if (v == null)
                    return eProjectOutputType.ClassLibrary;
                return XmlValueAttribute_Extensions.FromXmlValue<eProjectOutputType>(v);
            }
        }

        public string AssemblyName // Can be different than RootNamespace
        {
            get
            {
                return Doc.SimpleXPath("//AssemblyName").SingleOrDefault()?.Value
                    ?? FileName; // for dotnetcore
            }
        }

        /// <summary> Returns lower cased, without space, platforms defined in project </summary>
        public List<ePlatform> AvailableBuildPlatforms
        {
            get
            {
                // Tag Platforms can exist multiple times in "PropertyGroup" (with condition = Configuration or Debug etc)
                // We assume the platforms are the same no matter the configuration group (that would be weird anyway)...
                var value = Doc.SimpleXPath("//Platforms").ToList();
                if (value == null)
                    foreach (var tuple in Solution.ForEachDirectoryBuildProps(this))
                    {
                        value = tuple.rootNode.SimpleXPath("//Platforms").ToList();
                        if (value != null)
                            break;
                    }
                // ... So this SelectMany is okay with this assertion
                return value?.SelectMany(r => r.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                             .Select(strPlatform => ePlatform.GetByNormalizedName(strPlatform))
                             .DefaultIfEmpty(ePlatform.AnyCpu)
                             .Distinct()
                             .ToList();
            }
        }
        /// <summary> Get names of all targeted frameworks (without '.' and lowercased) </summary>
        public eNetFramework[] TargetFrameworks
        {
            get
            {
                if (IsDocAboutWeb)
                {
                    var fromWebConfig = "net".AsPrefixForIfNotBlank(Doc.SimpleXPath("//configuration/system.web/compilation").SingleOrDefault()?.Attribute("targetFramework")?.Value);
                    return IsDocAboutWeb ? new[] { eNetFramework.GetByNormalizedName(fromWebConfig) } : Array.Empty<eNetFramework>();
                }

                var value = Doc.SimpleXPath("//TargetFrameworks").SingleOrDefault()?.Value
                         ?? Doc.SimpleXPath("//TargetFramework").SingleOrDefault()?.Value;
                if (value == null)
                    foreach (var tuple in Solution.ForEachDirectoryBuildProps(this))
                    {
                        value = tuple.rootNode.SimpleXPath("//TargetFrameworks").SingleOrDefault()?.Value
                             ?? tuple.rootNode.SimpleXPath("//TargetFramework").SingleOrDefault()?.Value;
                        if (value != null)
                            break;
                    }
                if (value != null)
                    return value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                                .Select(eNetFramework.GetByNormalizedName)
                                .NotNull()
                                .ToArray();

                //// Trying with old csproj format now

                var grps = Doc.SimpleXPath("//TargetFrameworkVersion").GroupBy(n => n.Value).ToList();
                value = grps.SingleOrDefault()?.Key; // Assuming target framework is the same for all PropertyGroup (Debug / Release etc)
                if (value == null)
                    foreach (var tuple in Solution.ForEachDirectoryBuildProps(this))
                    {
                        grps = tuple.rootNode.SimpleXPath("//TargetFrameworkVersion").GroupBy(n => n.Value).ToList();
                        if (grps.Count > 0)
                        {
                            value = grps.Single().Key; // Assuming target framework is the same for all PropertyGroup (Debug / Release etc)
                            break;
                        }
                    }
                if (value != null)
                {
                    Debug.Assert(value.ToLowerInvariant().StartsWith("v"));
                    return new[] { eNetFramework.GetByNormalizedName("net" + value.Substring(1)) };
                }

                // If we get here there is probably a bug...
                return Array.Empty<eNetFramework>();
            }
        }
        public bool UseOldTargetFrameworkTag => Doc.SimpleXPath("//TargetFramework").SingleOrDefault() != null;

        public bool ReferenceWinforms
        {
            get
            {
                return Doc.SimpleXPath("//Reference[@Include='System.Windows.Forms']").Any()
                    || Doc.SimpleXPath("//UseWindowsForms[contains(translate(., 'TRUE', 'true'), 'true')]").Any();
            }
        }

        public bool ReferenceMicrosoftWPF
        {
            get
            {
                return Doc.SimpleXPath("//Reference[@Include='PresentationFramework']").Any()
                    || Doc.SimpleXPath("//UseWPF[contains(translate(., 'TRUE', 'true'), 'true')]").Any();
            }
        }

        public bool ReferenceDevExpress
        {
            get
            {
                return Doc.SimpleXPath("//Reference[contains(@Include,'DevExpress')]").Any()
                    || Doc.SimpleXPath("//PackageReference[contains(@Include,'DevExpress')]").Any();
            }
        }

        public bool RegisterForComInterop
        {
            get
            {
                return Doc.SimpleXPath("//RegisterForComInterop[contains(translate(., 'TRUE', 'true'), 'true')]").Any();
            }
        }

        public string[] DocumentationFilePaths
        {
            get
            {
                return Doc.SimpleXPath("//DocumentationFile").Select(n => n.Value).Distinct().ToArray();
            }
        }
        public bool HasDocumentationFileGenerated => DocumentationFilePaths.Length > 0;


        public string AssemblyOriginatorKeyFile
        {
            get
            {
                return Doc.SimpleXPath("//AssemblyOriginatorKeyFile").FirstOrDefault()?.Value;
            }
        }

        public List<string> GetResourceFiles()
        {
            return Doc.SimpleXPath("//Resource").Concat(Doc.SimpleXPath("//EmbeddedResource"))
                      .Select(node => node.Attribute("Include")?.Value)
                      .NotNull()
                      // from https://docs.microsoft.com/en-us/visualstudio/msbuild/msbuild-special-characters?view=vs-2022
                      .Select(filename => filename.Replace("%24", "$")
                                                  .Replace("%40", "@")
                                                  .Replace("%27", "'")
                                                  .Replace("%28", "(")
                                                  .Replace("%29", ")")
                                                  .Replace("%3B", ";")
                                                  .Replace("%3F", "?")
                                                  .Replace("%2A", "*")
                                                  .Replace("%25", "%"))
                      .ToList();
        }
    }


    public enum eProjectOutputType
    {
        [XmlValue("Library")]
        ClassLibrary,
        [XmlValue("Exe")]
        ConsoleApplication,
        [XmlValue("WinExe")]
        WindowsApplication,
    }

    public enum eNetSDK
    {
        [XmlValue("")]
        None,
        [XmlValue("Microsoft.NET.Sdk")]
        Microsoft_NET_Sdk, // https://github.com/dotnet/sdk
        [XmlValue("Microsoft.NET.Sdk.Web")]
        Microsoft_NET_Sdk_Web, // https://github.com/dotnet/sdk
        [XmlValue("Microsoft.NET.Sdk.BlazorWebAssembly")]
        Microsoft_NET_Sdk_BlazorWebAssembly,
        [XmlValue("Microsoft.NET.Sdk.Razor")]
        Microsoft_NET_Sdk_Razor,
        [XmlValue("Microsoft.NET.Sdk.Worker")]
        Microsoft_NET_Sdk_Worker, // The .NET Worker Service SDK
        /// <summary>
        /// The .NET Desktop SDK, which includes Windows Forms (WinForms) and Windows Presentation Foundation (WPF).*
        /// </summary>
        [XmlValue("Microsoft.NET.Sdk.WindowsDesktop")]
        [Obsolete("Use " + nameof(Microsoft_NET_Sdk) + "instead!")]
        Microsoft_NET_Sdk_WindowsDesktop,
    }
}
