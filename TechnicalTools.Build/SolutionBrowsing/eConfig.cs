﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using TechnicalTools.Model;


namespace TechnicalTools.Build
{
    [DebuggerDisplay("{" + nameof(DisplayName) + ",nq}")]
    // Enum in the java way for framework.
    public class eConfig
    {
        public static eConfig Debug { get; } = new eConfig("Debug");
        public static eConfig Release { get; } = new eConfig("Release");
        public string DisplayName { get; }

        /// <summary> Name of platform lowercased no duplicate space, trimmed </summary>
        public string NormalizedName { get; }

        /// <summary>
        /// Open constructor to add custom / weird config as static public property in a child class.
        /// </summary>
        protected eConfig(string displayName, string normalizedName = null)
        {
            DisplayName = displayName;
            NormalizedName = Normalize(normalizedName ?? displayName);
            _AllStandardConfigurations = _AllStandardConfigurations ?? new();
            _byNormalizedNames = _byNormalizedNames ?? new();

            if (null == TryGetByNormalizedName(NormalizedName))
            {
                _byNormalizedNames.Add(NormalizedName, this);
                _AllStandardConfigurations.Add(this);
            }
        }

        public override string ToString()
        {
            return DisplayName;
        }

        public static IReadOnlyCollection<eConfig> AllStandardConfigurations => _AllStandardConfigurations;
        static List<eConfig> _AllStandardConfigurations;
        static Dictionary<string, eConfig> _byNormalizedNames;

        public static eConfig GetByNormalizedName(string name)
        {
            return TryGetByNormalizedName(name) 
                ?? throw new TechnicalException($"{name} not recognized as a valid configuration (or not yet handled, use constructor for that!)");
        }
        public static eConfig TryGetByNormalizedName(string name)
        {
            if (_byNormalizedNames.TryGetValue(Normalize(name), out var result))
                return result;
            return null;
        }

        static string Normalize(string name)
        {
            while (name.Contains("  "))
                name = name.Replace("  ", " ");
            return name.Trim().ToLowerInvariant();
        }
    }
}
