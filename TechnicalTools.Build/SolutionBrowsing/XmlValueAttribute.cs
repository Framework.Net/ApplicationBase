﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;


namespace TechnicalTools.Build
{
    public class XmlValueAttribute : Attribute
    {
        public string XmlSerializedValue { get; }

        public XmlValueAttribute(string xmlSerializedValue)
        {
            XmlSerializedValue = xmlSerializedValue;
        }
    }

    public static class XmlValueAttribute_Extensions
    {
        public static string GetXmlValue<T>(this T value)
            where T : Enum
        {
            var att = (XmlValueAttribute)Attribute.GetCustomAttribute(typeof(T).GetField(value.ToString()), typeof(XmlValueAttribute));
            if (att == null)
                throw new Exception($"Enum value {typeof(T)}.{value} has no {typeof(XmlValueAttribute)} attribute on it!");
            return att.XmlSerializedValue;
        }


        public static T FromXmlValue<T>(string xmlValue)
            where T : Enum
        {
            xmlValue = xmlValue ?? throw new ArgumentNullException(nameof(xmlValue));
            var bySerializedValue = GetDictionary<T>();
            if (bySerializedValue.TryGetValue(xmlValue.ToLowerInvariant(), out T value))
                return value;
            throw new Exception($"Enum value {xmlValue} not mapped to type {typeof(T)}!");
        }
        static IReadOnlyDictionary<string, T> GetDictionary<T>()
            where T : Enum
        {
            var dico = _EnumValuesBySerializedValueByEnumTypes.GetOrAdd(typeof(T), t =>
            {
                IReadOnlyDictionary<string, T> result =
                    Enum.GetValues(typeof(T))
                        .Cast<T>()
                        .ToDictionary(v => v.GetXmlValue<T>().ToLowerInvariant());
                return result;
            });
            return (IReadOnlyDictionary<string, T>)dico;
        }
        static readonly ConcurrentDictionary<Type, object> _EnumValuesBySerializedValueByEnumTypes = new ConcurrentDictionary<Type, object>();
    }
}
