﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using TechnicalTools.Model;




namespace TechnicalTools.Build
{
    [DebuggerDisplay("{" + nameof(DisplayName) + ",nq}")]
    // Enum in the java way for framework.
    public class ePlatform
    {
        public static ePlatform X64 { get; } = new ePlatform("x64");
        public static ePlatform X86 { get; } = new ePlatform("x86");
        public static ePlatform AnyCpu { get; } = new ePlatform("Any Cpu");

        public string DisplayName { get; }

        /// <summary> Name of platform lowercased no duplicate space, trimmed </summary>
        public string NormalizedName { get; }

        /// <summary>
        /// Open constructor to add custom / weird config as static public property in a child class (like "win32" etc...).
        /// Use also <see cref="PlatformCompatibilities"/>
        /// </summary>
        protected ePlatform(string displayName, string normalizedName = null)
        {
            DisplayName = displayName;
            NormalizedName = Normalize(normalizedName ?? displayName);
            _AllStandardPlatforms = _AllStandardPlatforms ?? new();
            _byNormalizedNames = _byNormalizedNames ?? new();

            if (null == TryGetByNormalizedName(NormalizedName))
            {
                _byNormalizedNames.Add(NormalizedName, this);
                _AllStandardPlatforms.Add(this);
            }
        }

        public override string ToString()
        {
            return DisplayName;
        }

        public static IReadOnlyCollection<ePlatform> AllStandardPlatforms => _AllStandardPlatforms;
        static List<ePlatform> _AllStandardPlatforms;
        static Dictionary<string, ePlatform> _byNormalizedNames;

        public static ePlatform GetByNormalizedName(string name)
        {
            return TryGetByNormalizedName(name) 
                ?? throw new TechnicalException($"{name} not recognized as a valid platform (or not yet handled, use constructor for that!)");
        }
        public static ePlatform TryGetByNormalizedName(string name)
        {
            if (_byNormalizedNames.TryGetValue(Normalize(name), out var result))
                return result;
            return null;
        }

        static string Normalize(string name)
        {
            name = name.Replace(" ", "");
            return name.Trim().ToLowerInvariant();
        }
        public virtual bool CanReference(ePlatform platform)
        {
            if (!PlatformCompatibilities.TryGetValue(this, out var compatibleOnes))
                throw new TechnicalException($"No information available about platform {this} for compatibilities with other platform!" + Environment.NewLine +
                                             $"Use {nameof(PlatformCompatibilities)} in your derived custom class that extends {nameof(ePlatform)}!");
            return compatibleOnes.Contains(platform);
        }

        protected static Dictionary<ePlatform, ePlatform[]> PlatformCompatibilities = new Dictionary<ePlatform, ePlatform[]>()
            {
                {  AnyCpu, new[]{ AnyCpu } },
                {  X86, new[]{ AnyCpu, X86 } },
                {  X64, new[]{ AnyCpu, X64 } },
            };
    }
}
