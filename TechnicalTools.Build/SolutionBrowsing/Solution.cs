﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

using TechnicalTools.Algorithm.Graph;


namespace TechnicalTools.Build
{
    public class Solution
    {
        public string FilePath { get; }
        public string SolutionDir { get { return Path.GetDirectoryName(FilePath); } }
        public IEnumerable<SlnProjectReference> ProjectReferences { get { return _references.Where(p => p.ProjectType != ProjectType.SolutionFolder); } }
        public IEnumerable<Project> Projects { get { return ProjectReferences.Select(pr => pr.Project); } }

        public Solution(string slnFile)
        {
            FilePath = Path.GetFullPath(slnFile);
        }
        List<SlnProjectReference> _references { get; } = new List<SlnProjectReference>();

        public static Solution Read(string slnFilePath)
        {
            var sln = new Solution(slnFilePath);
            var slnLines = File.ReadAllLines(sln.FilePath).Select(line => line.Trim()).ToList();

            SlnProjectReference current = null;
            // According to https://docs.fileformat.com/programming/sln/
            //Project("{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}") = "MyAwsomeProject", "relative\path\to\your\project\MyAwsomeProject.csproj", "{234DDE94-E426-4589-AEF3-69EA3D9B054E}"
            var reProject = new Regex(@"Project \( ""\{(?<projectTypeGuid>[-A-Z0-9]+)\}"" \) = ""(?<projectName>[^""]+)"" , ""(?<pathToCsProj>[^""]*)"" , ""\{(?<projectGuid>[-A-Z0-9]+)\}"" ".Replace(" ", "\\s*"), RegexOptions.IgnoreCase | RegexOptions.Compiled);

            for (int lineIndex = 0; lineIndex < slnLines.Count; ++lineIndex)
            {
                var line = slnLines[lineIndex];
                if (line.StartsWith("Microsoft Visual Studio Solution File, Format Version") ||
                    line.StartsWith("#") ||
                    line.StartsWith("VisualStudioVersion = ") ||
                    line.StartsWith("MinimumVisualStudioVersion = ") ||
                    string.IsNullOrWhiteSpace(line))
                    continue;

                if (line.StartsWith("ProjectSection"))
                {
                    // TODO : Handle manual dependencies between projects ?
                    // see https://stackoverflow.com/questions/5629981/whats-the-purpose-of-this-string-in-my-visual-studio-sln-file
                    // add them in "current"
                    while (!line.StartsWith("EndProjectSection"))
                        line = slnLines[++lineIndex];
                    continue;
                }

                if (line.StartsWith("Project"))
                {
                    Match m = reProject.Match(line);
                    var errorMsg = "Unexpected behavior, check the regex... or the line!";
                    if (!m.Success)
                        throw new Exception(errorMsg);

                    var projectTypeGUID = m.Groups["projectTypeGuid"].Success ? ProjectType.ByGuid(new Guid(m.Groups["projectTypeGuid"].Value)) : throw new Exception(errorMsg);
                    var guid = m.Groups["projectGuid"].Success ? new Guid(m.Groups["projectGuid"].Value) : throw new Exception(errorMsg);
                    var projectName = m.Groups["projectName"].Success ? m.Groups["projectName"].Value : throw new Exception(errorMsg);
                    var projectPathExtension = m.Groups["pathToCsProj"].Success ? m.Groups["pathToCsProj"].Value : throw new Exception(errorMsg);

                    current = new SlnProjectReference()
                    {
                        Solution = sln,
                        ProjectType = projectTypeGUID,
                        Guid = guid,
                        ProjectName = projectName,
                        ProjectPathExtension = projectPathExtension,
                    };
                    sln._references.Add(current);
                    continue;
                }
                if (line.StartsWith("EndProject"))
                {
                    current = null;
                    continue;
                }

                if (line.StartsWith("Global"))
                {
                    while (!line.StartsWith("EndGlobal") || line.StartsWith("EndGlobalSection"))
                        line = slnLines[++lineIndex];
                    continue;
                }

                throw new Exception("Not handled line in sln file!");
            }
            return sln;
        }

        public Project FindProject(string projectNameWithoutExtension, List<Project> amongAllTheseProjects = null)
        {
            return GetMatchingProjects(projectNameWithoutExtension, amongAllTheseProjects).Single();
        }
        public List<Project> GetMatchingProjects(string projectNameWithoutExtension, List<Project> amongAllTheseProjects = null)
        {
            amongAllTheseProjects = amongAllTheseProjects ?? ProjectReferences.Select(pr => pr.Project).ToList();
            var projects = amongAllTheseProjects.Where(p => p.FileName.ToLowerInvariant() == projectNameWithoutExtension.ToLowerInvariant()).ToList();
            return projects;
        }
        /// <summary>
        /// Return all projects that depends directly or indirectly of project <paramref name="projectNameWithoutExtension"/>.
        /// The result list always contains project "<paramref name="projectNameWithoutExtension"/>".
        /// </summary>
        public List<Project> AllProjectsThatDependOn(string projectNameWithoutExtension, List<Project> amongAllTheseProjects = null)
        {
            var project = FindProject(projectNameWithoutExtension, amongAllTheseProjects);
            return AllProjectsThatDependOn(project, amongAllTheseProjects);
        }

        /// <inheritdoc cref="AllProjectsThatDependOn(string, List{Project})"/>
        public List<Project> AllProjectsThatDependOn(Project project, List<Project> amongAllTheseProjects = null)
        {
            amongAllTheseProjects = amongAllTheseProjects ?? ProjectReferences.Select(pr => pr.Project).ToList();

            var getParents = amongAllTheseProjects.SelectMany(p => p.ProjectDependencies)
                                                  .ToLookup(pd => pd.Target);
            var result = new HashSet<Project>();
            var toTreat = new Queue<Project>();
            toTreat.Enqueue(project);
            result.Add(project);
            while (toTreat.Count > 0)
            {
                var p = toTreat.Dequeue();
                foreach (var parent in getParents[p])
                    if (!result.Contains(parent.Source))
                    {
                        result.Add(parent.Source);
                        toTreat.Enqueue(parent.Source);
                    }
            }
            return result.ToList();
        }

        /// <summary>
        /// Return all projects that are directly or indirectly dependencies of project <paramref name="project"/>.
        /// The result list always contains project <paramref name="project"/>.
        /// </summary>
        public List<Project> OnlyProjectsThatAreDependenciesOf(Project project, List<Project> amongAllTheseProjects = null)
        {
            amongAllTheseProjects = amongAllTheseProjects ?? ProjectReferences.Select(pr => pr.Project).ToList();

            var result = new HashSet<Project>();
            var queue = new Queue<Project>();
            queue.Enqueue(amongAllTheseProjects.Single(p => p.FileName == project.FileName));
            result.Add(queue.Peek());
            while (queue.Count > 0)
            {
                var p = queue.Dequeue();
                foreach (var dep in p.ProjectDependencies)
                    if (!result.Contains(dep.Target))
                    {
                        queue.Enqueue(dep.Target);
                        result.Add(dep.Target);
                    }
            }
            return result.ToList();
        }

        /// <summary>
        /// Traverse all projects of a solution in post order and execute a function on them.
        /// </summary>
        public Dictionary<Project, Task<TResult>> PostOrderTraverseAndExecute<TResult>(Func<Project, IReadOnlyList<TResult>, TResult> execute)
        {
            var compilations = new Dictionary<Project, Task<TResult>>();
            Task<TResult> getRunExecutionOf(Project p)
            {
                lock (compilations)
                {
                    if (!compilations.TryGetValue(p, out Task<TResult> task))
                    {
                        task = run(p);
                        compilations[p] = task;
                    }
                    return task;
                }
            }

            async Task<TResult> run(Project p)
            {
                var depsTasks = p.ProjectDependencies.Select(pd => getRunExecutionOf(pd.Target)).ToList();
                var subResults = await Task.WhenAll(depsTasks).ConfigureAwait(false);
                TResult result = await Task.Run(() => execute(p, subResults)).ConfigureAwait(false);
                return result;
            };

            var results = ProjectReferences.ToDictionary(pr => pr.Project, pr => getRunExecutionOf(pr.Project));
            return results;
        }

        public IEnumerable<(string dirBuildPropsFolder, XElement rootNode)> ForEachDirectoryBuildProps(Project forThisProject)
        {
            return ForEachDirectoryBuildProps(forThisProject.RelativeToSolutionFolderPath);
        }

        public IEnumerable<(string pathOfDirBuildProps, XElement rootNode)> ForEachDirectoryBuildProps(string startingNestedPath)
        {
            var curDirPath = Path.GetFullPath(Path.Combine(SolutionDir, startingNestedPath));
            Debug.Assert(!SolutionDir.EndsWith("\\"));
            while (Directory.Exists(curDirPath)
                // We cannot write somehing like that because sln may be stored inside a folder of the repository not at the root
                /* && curDirPath.StartsWith(SolutionDir) */)
            {
                var file = Path.Combine(curDirPath, "Directory.Build.props");
                if (_directoryBuildProps.TryGetValue(curDirPath, out XDocument doc))
                    yield return (file, doc.Root);
                else if (File.Exists(file))
                {
                    doc = XDocument.Load(file, LoadOptions.PreserveWhitespace | LoadOptions.SetLineInfo);
                    _directoryBuildProps.TryAdd(file, doc);
                    yield return (file, doc.Root);
                }
                curDirPath = Path.GetDirectoryName(curDirPath);
            }
        }
        readonly ConcurrentDictionary<string, XDocument> _directoryBuildProps = new ConcurrentDictionary<string, XDocument>();

        public List<Project> KeepOnlyInterestingProjects(string[] whereProjectDependsOnAny, string[] whereProjectIsDependencyOfAny, bool whereProjectHasGui, bool reverseFilter)
        {
            var projectsHavingGui = new HashSet<Project>();
            foreach (var p in Projects)
                if (p.ReferenceWinforms || p.ReferenceMicrosoftWPF)
                    if (!projectsHavingGui.Contains(p))
                        projectsHavingGui.AddRangeInPlace(AllProjectsThatDependOn(p));

            var projectsOnTopOf = new HashSet<Project>(); // ... on top of WhereProjectDependsOnAny
            var dependencyProjects = whereProjectDependsOnAny.Select(projectName => FindProject(projectName)).ToList();
            foreach (var dep in dependencyProjects)
                projectsOnTopOf.AddRangeInPlace(AllProjectsThatDependOn(dep));

            var projectsThatConstituteAnyOf = new HashSet<Project>(); // ... any of WhereProjectDependsOnAny
            var targetProjects = whereProjectIsDependencyOfAny.Select(projectName => FindProject(projectName)).ToList();
            foreach (var target in targetProjects)
                projectsThatConstituteAnyOf.AddRangeInPlace(OnlyProjectsThatAreDependenciesOf(target));

            var projects = Projects.ToHashSet();
            var results = (whereProjectHasGui ? projectsHavingGui : projects)
                .Intersect(dependencyProjects.Count > 0 ? projectsOnTopOf : projects)
                .Intersect(targetProjects.Count > 0 ? projectsThatConstituteAnyOf : projects)
                .ToList();
            if (reverseFilter)
                results = projects.Except(results).ToList();

            return results;
        }

        /// <summary>
        /// Sort projects and simplify dependencies
        /// </summary>
        /// <param name="projects">List of projects whose dependencies need to be cleaned / simplified</param>
        /// <param name="log">Log when a link is considered useless</param>
        /// <returns>Same project, ordered, and updated</returns>
        public static List<Project> SortProjectAndSimplifyDependencies(IReadOnlyCollection<Project> projects, Action<string> log)
        {
            // Capture all dependencies so we will purify the collections
            var allDependencies = projects.SelectMany(p => p.ProjectDependencies)
                                          // not useless if project is a subset of all projects in solution
                                          .Where(d => projects.Contains(d.Target))
                                          .GroupBy(dep => dep.Source)
                                          .ToDictionary(grp => grp.Key, grp => grp.ToList());
            var empty = new List<Project>();
            var emptyDep = new List<ProjectToProjectDependency>();
            Func<Project, IEnumerable<Project>> getDeps = p => allDependencies.TryGetValue(p, out List<ProjectToProjectDependency> lst) ? lst.Select(ppd => ppd.Target) : empty;
            Func<Project, List<ProjectToProjectDependency>> getDeps2 = p => allDependencies.TryGetValue(p, out List<ProjectToProjectDependency> lst) ? lst : emptyDep;


            var projectsOrdered = TopologicalOrderSimple.DoTopologicalSort(projects, getDeps).AsEnumerable().Reverse().ToList();
            TransitiveReduction.Instance.Reduce(projectsOrdered,
                                                allDependencies.Values.SelectMany(x => x).ToList(),
                                                dep => dep.Source,
                                                dep => dep.Target,
                                                getDeps2,
                                                (projectS, projectT) =>
                                                {
                                                    int i = 0;
                                                    foreach (var ppd in getDeps2(projectS))
                                                        if (ppd.Target == projectT)
                                                        {
                                                            Debug.Assert(ppd.Source == projectS);
                                                            log($"Removing link \"{projectS.NameInSln}\" needs \"{projectT.NameInSln}\" because another (longer) path exists!");
                                                            allDependencies[projectS].RemoveAt(i);
                                                            break;
                                                        }
                                                        else
                                                            ++i;
                                                });
            foreach (var p in projectsOrdered)
                if (allDependencies.ContainsKey(p))
                    p.ProjectDependencies = allDependencies[p];

            return projectsOrdered;
        }
    }

    [DebuggerDisplay("{" + nameof(ProjectName) + " + \" as \" + " + nameof(ProjectType) + "." + nameof(Build.ProjectType.Name) + ",nq}")]
    public class SlnProjectReference
    {
        public Solution Solution { get; internal set; }

        /// <summary>
        /// The unique GUID of the project that differentiates it from other projects in the solution.
        /// The unique ID of a project in the solution makes it possible for other projects in the solution to access it.
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// The Project-Type-GUID is unique for different project types and is used by the solution reader to identify the type of project.
        /// F184B08F-C81C-45F6-A57F-5ABD9991F28F, for example, shows that it is a VB.NET project.
        /// </summary>
        public ProjectType ProjectType { get; set; }

        public string ProjectName { get; set; }

        public string ProjectPathExtension { get; set; }

        public Project Project
        {
            get
            {
                return _Project ?? (_Project = new Project(this, ProjectPathExtension));
            }
        }
        Project _Project;
    }

}
