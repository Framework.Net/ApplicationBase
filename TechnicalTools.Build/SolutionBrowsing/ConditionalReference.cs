﻿using System;


namespace TechnicalTools.Build
{
    public class ConditionalReference
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
