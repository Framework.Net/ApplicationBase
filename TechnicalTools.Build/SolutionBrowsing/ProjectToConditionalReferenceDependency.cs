﻿using System;


namespace TechnicalTools.Build
{
    public class ProjectToConditionalReferenceDependency : ProjectDependency
    {
        /// <summary> Source depends on Target </summary>
        public Project Source { get; init; }
        /// <summary> Reference is a dependency of Source </summary>
        public ConditionalReference Reference { get; init; }

        public override int GetHashCode()
        {
            return (Source.FileName.GetHashCode() * 397) ^ Reference.Name.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return obj is ProjectToConditionalReferenceDependency pcrd
                // works because Project instance are singletons
                && pcrd.Source == Source
                && pcrd.Reference.Name == Reference.Name
                && pcrd.Reference.Path == Reference.Path;
        }
    }
}
