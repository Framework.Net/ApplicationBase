﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;



namespace TechnicalTools.Build
{
    /// <summary>
    /// This class validates a solution (.sln file) and all related projects (.csproj)
    /// Error / Warnings messages are expected to be cristal clear
    /// </summary>
    public class SolutionValidator
    {
        public IReadOnlyCollection<string> Errors => _Errors; List<string> _Errors = new();
        public IReadOnlyCollection<string> Warnings => _Warnings; List<string> _Warnings = new();

        public SolutionValidator(Solution sln, Func<Project, bool> asWarningForTheseProjects = null, Func<Project, bool> ignoredProjects = null)
        {
            _sln = sln ?? throw new ArgumentNullException(nameof(sln));
            _asWarningForTheseProjects = asWarningForTheseProjects ?? Func_Extensions.ReturnFalse;
            _ignoredProjects = ignoredProjects ?? Func_Extensions.ReturnFalse;
        }
        readonly Solution _sln;
        readonly Func<Project, bool> _asWarningForTheseProjects;
        readonly Func<Project, bool> _ignoredProjects;
        void AddIssue(bool isTolerant, string issueMsg)
        {
            var container = isTolerant ? _Warnings : _Errors;
            container.Add(issueMsg);
        }

        /// <summary>
        /// Decide the order in which projects are analyzed.
        /// Thus this is also the order warnings / errors are found.
        /// </summary>
        protected virtual IEnumerable<Project> EnumerateProjects(Solution sln)
        {
            // By default alphabetically
            return sln.Projects.Where(p => !_ignoredProjects(p)).OrderBy(p => p.NameInSln);
        }

        public void Validate(bool asWarning = false)
        {
            ValidateProjectsAreInModernSDKFormat(asWarning);
            ValidateNoProjectIsUsingOldTargetFrameworkXmlElement(asWarning);
            ValidateTargetFrameworkConsistencyBetweenProjects(asWarning);
            ValidateTargetPlatformConsistencyBetweenProjects(asWarning);
            ValidateNoProjectHaveHardDllReference(asWarning);
            ValidateSlnBuildConsistency(asWarning);
        }
        public virtual void ValidateProjectsAreInModernSDKFormat(bool asWarning)
        {
            // Check that all projects (especially the new added ones) are in new SDK format!
            foreach (var project in EnumerateProjects(_sln))
            {
                var forcedWarning = asWarning || _asWarningForTheseProjects(project);
                if (project.SDKType == eNetSDK.None)
                    AddIssue(forcedWarning,
                            $"Project {project.NameInSln}'s csproj file must be in new SDK format!");
                else if (project.SDKType == eNetSDK.Microsoft_NET_Sdk)
                {
                    // Mismatch between project and sln, it happens when migrating an old project (added in solution) and migrating it to new format :
                    // the lsn part is not updated except if we remove project and add it back again to sln
                    if (project.ProjectReference.ProjectType == ProjectType.OldProject)
                        AddIssue(forcedWarning, $"Project {project.NameInSln} must have a guid starting by {ProjectType.ModernProject.Guid} instead of {ProjectType.OldProject.Guid} because the project csproj has been migrated to new SDK format." + Environment.NewLine +
                                                         "You can simply remove and add back the project to make VS fix that for you!");
                }
                else
                    AddIssue(true,
                            $"Project {project.NameInSln}'s csproj file is using a SDK type not yet handled by this tool!");
            }
        }

        public virtual void ValidateNoProjectIsUsingOldTargetFrameworkXmlElement(bool asWarning)
        {
            // Check that all projects use TargetFrameworks (plural form) and not singular one
            foreach (var project in EnumerateProjects(_sln))
            {
                var forcedWarning = asWarning || _asWarningForTheseProjects(project);
                if (project.UseOldTargetFrameworkTag)
                    AddIssue(forcedWarning,
                        $"Project {project.NameInSln}'s csproj MUST use \"<TargetFrameworks>\" tag (ie: the plural form, you forgot final 's')!");
            }
        }

        public virtual void ValidateTargetFrameworkConsistencyBetweenProjects(bool asWarning)
        {
            // target framework checks
            foreach (var project in EnumerateProjects(_sln)) // Check that all projects have...
            {
                var forcedWarning = asWarning || _asWarningForTheseProjects(project);

                foreach (var frameworkWished in project.TargetFrameworks) // ...target frameworks
                    foreach (var dependencyProject in project.ProjectDependencies.Select(pd => pd.Target).OrderBy(p => p.NameInSln)) // so that each dependency
                        if (dependencyProject.TargetFrameworks.All(depFramework => !depFramework.CanRunOn(frameworkWished))) // has a lower or equal compatible framework
                            AddIssue(forcedWarning,
                                     $"Project {project.NameInSln} wants to build using framework {frameworkWished} but dependency project {dependencyProject.NameInSln} is not built against any compatible framework !!");
            }
        }
        public virtual void ValidateTargetPlatformConsistencyBetweenProjects(bool asWarning)
        {
            foreach (var project in EnumerateProjects(_sln)) // Check that all projects have...
            {
                var forcedWarning = asWarning || _asWarningForTheseProjects(project);

                foreach (var platformWished in project.AvailableBuildPlatforms) // ...target platforms
                    foreach (var dependencyProject in project.ProjectDependencies.Select(pd => pd.Target).OrderBy(p => p.NameInSln)) // so that each dependency
                        if (!dependencyProject.AvailableBuildPlatforms.Any(depPlatform => platformWished.CanReference(depPlatform))) // has a AnyCpu or equal compatible platform
                            AddIssue(forcedWarning,
                                     $"Project \"{project.NameInSln}\" wants to build using Platform {platformWished} " + 
                                     $"but dependency project {dependencyProject.NameInSln} has only " + 
                                     $"{(dependencyProject.AvailableBuildPlatforms.Count > 1 ? "these platforms" : "this platform")}: {dependencyProject.AvailableBuildPlatforms.Join()}!");
            }
        }

        public virtual void ValidateNoProjectHaveHardDllReference(bool asWarning)
        {
            // Check about hard referenced dll 
            foreach (var project in EnumerateProjects(_sln))
            {
                var forcedWarning = asWarning || _asWarningForTheseProjects(project);
                // In modern csproj only, because there is already a warning/error for old csproj
                if (project.SDKType != eNetSDK.None)
                {
                    var hardReferences = ProjectDependency.GetConditionalReferences(project.Doc.Root, Path.GetDirectoryName(project.FullPath), ePlatform.X64, eConfig.Release);
                    foreach (var reference in hardReferences.Select(hr => hr.Path))
                        AddIssue(forcedWarning,
                                 $"Project \"{project.NameInSln}\" must not hard reference dll file \"{Path.GetDirectoryName(project.FullPath).GetRelativePathTo(reference)}\", find a nuget or nugetize them!");
                }
            }
        }

        // This code does not use any structured object data so i commented it more
        // It returns the configuration really built
        public HashSet<ProjectBuildConfig> ValidateSlnBuildConsistency(bool asWarning) => AnalyseSlnBuildConsistency(true, asWarning);

        public HashSet<ProjectBuildConfig> GetBuildConfigurations() => AnalyseSlnBuildConsistency(false, true);

        public record ProjectBuildConfig
        {
            public Project Project { get; set; }
            public eConfig Config { get; set; }
            public ePlatform Platform { get; set; }
        }
        HashSet<ProjectBuildConfig> AnalyseSlnBuildConsistency(bool withChecks, bool asWarning)
        {
            var lines = File.ReadLines(_sln.FilePath).ToList();

            var projectsByGuid = _sln.Projects.ToDictionary(p => p.Guid);

            // This is a regex to detect all lines about how to build project in solution
            // Basically they are in the second "Global" section of sln file and start by a project's guid surrounded by braces
            var reBuildConfig = new Regex(@"^\s*\{(?<project_guid>[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12})\}" +// for this project
                                    "[.](?<solution_config>[a-z0-9 ]+)" +  // when VS or msbuild want to build solution in this Config (Release/Debug)
                                    "[|](?<solution_platform>[a-z0-9 ]+)" + // For this platform (x86/x64/AnyCpu)
                                    "[.](?<config_key>[a-z0-9. ]+[a-z0-9])" + // we want to link the build to config inside csproj (value = ActiveCfg) or enable the build of project (value = Build.0)
                                 @"\s*=\s*(?<project_config>[a-z0-9 ]+)" + // to this project config
                                    "[|](?<project_platform>[a-z0-9 ]+)" + // to this project platform
                                    @"\s*",
                                    RegexOptions.IgnoreCase | RegexOptions.Compiled);

            var projectBuildConfigs = new HashSet<ProjectBuildConfig>();
            for (int i = 0; i < lines.Count; ++i)
            {
                var m = reBuildConfig.Match(lines[i]);
                if (!m.Success)
                    continue;
                var projectGuid = Guid.Parse(m.Groups["project_guid"].Value);
                var project = projectsByGuid[projectGuid];

                var config_key = m.Groups["config_key"].Value;
                var slnConfig = eConfig.GetByNormalizedName(m.Groups["solution_config"].Value);
                var slnPlatform = ePlatform.GetByNormalizedName(m.Groups["solution_platform"].Value);
                var project_config = eConfig.GetByNormalizedName(m.Groups["project_config"].Value);
                var project_platform = ePlatform.GetByNormalizedName(m.Groups["project_platform"].Value);

                if (!project.AvailableBuildPlatforms.Contains(project_platform) && withChecks && !_ignoredProjects(project))
                    AddIssue(asWarning, $"Line {i} in sln is talking about project {project.NameInSln} with platform {project_platform} but the project itself only allows {project.AvailableBuildPlatforms.Join()!}");
                if (!slnPlatform.CanReference(project_platform) && withChecks) // solution compiled in x64 can compile project in any cpu for example
                    AddIssue(asWarning, $"Line {i} in sln says that when solution is building in {slnPlatform}, the project {project.NameInSln} should be build in {project_platform}! This is too dirty association!");

                if (config_key == "Build.0")
                    projectBuildConfigs.Add(new ProjectBuildConfig() { Project = project, Config = project_config, Platform = project_platform });
                else if (config_key.NotIn("ActiveCfg", "Deploy.0") && withChecks)
                    _Warnings.Add($"This tools does not know how to handle config key \"{config_key}\"!");
            }

            // Check project that seems useless
            foreach (var projectNeverCompiled in EnumerateProjects(_sln).Except(projectBuildConfigs.Select(t => t.Project)))
                _Warnings.Add($"According to sln, project {projectNeverCompiled.NameInSln} is never compiled in any configuration!");

            // Check there is no missing project in chain of compilation (a missing Build.0) 
            foreach (var config in eConfig.AllStandardConfigurations)
                foreach (var platform in ePlatform.AllStandardPlatforms)
                {
                    // List projects actually scheduled to be compiled (according to sln) in the setup "(config, platform)"
                    var projectsCompiled = projectBuildConfigs.Where(t => t.Config == config && (t.Platform == platform || t.Platform == ePlatform.AnyCpu))
                                                              .Select(t => t.Project)
                                                              .Distinct()
                                                              .OrderBy(p => p.NameInSln)
                                                              .ToList();
                    // Complete these projects with their dependency projects (recursively) to get full tree
                    var projectsCompiledAndDependencies = new HashSet<Project>();
                    foreach (var project in projectsCompiled)
                        projectsCompiledAndDependencies.AddRangeInPlace(_sln.OnlyProjectsThatAreDependenciesOf(project));

                    // if the full tree is bigger than projectsCompiled it means the diff projects miss the compilation in sln
                    var missingProjects = projectsCompiledAndDependencies.Except(projectsCompiled)
                                                                         .OrderBy(p => p.NameInSln)
                                                                         .ToList();
                    // or it is the reverse: we compile some projects for nothing... so we get some example to users:
                    var projectsRequiring = new HashSet<Project>();
                    foreach (var project in missingProjects)
                        projectsRequiring.AddRangeInPlace(_sln.AllProjectsThatDependOn(project));
                    var projectCompileForNothingMaybe = projectsCompiled.Intersect(projectsRequiring)
                                                                        .Select(p => " - " + p.NameInSln)
                                                                        .OrderBy(x => x)
                                                                        .ToList();

                    if (missingProjects.Any())
                    {
                        AddIssue(asWarning, $"For compilation setup ({config}, {platform}), these projects are not marked to be compiled in sln:" + Environment.NewLine + 
                            missingProjects.Select(p => " - " + p.NameInSln).Join(Environment.NewLine) + Environment.NewLine +
                            " but these projects (depending of them) want to be built in this setup:" + Environment.NewLine +
                            projectCompileForNothingMaybe.Join(Environment.NewLine));
                    }
                }

            return projectBuildConfigs;
        }
    }
}
