﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;


namespace TechnicalTools.UI
{
    /// <summary>
    /// Permet de dessiner une fleche tout en gardant un fond transparent (et ceux sur plusieurs controle en meme temps)
    /// Ce control existe aussi en version Component (permet de dessiner en arriere plan, cad recouvert par les control) !
    /// </summary>
    [ToolboxItem(true)]
    [DesignTimeVisible(true)]
    [DesignerCategory("DefaultControl Controls")]
    [Designer(typeof(ArrowDesigner))]
    public partial class Arrow : TransparentGlassPanel
    {
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [TypeConverter(typeof(PointConverter))]
        [DefaultValue(typeof(Point), "10, 10")]
        [Category("My")]
        public Point From { get { return _From; } set { _From = value; _FromOriginal = _From; Invalidate(); } }
        Point _From = new Point(10, 10);

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [TypeConverter(typeof(PointConverter))]
        [DefaultValue(typeof(Point), "80, 10")]
        [Category("My")]
        public Point To { get { return _To; } set { _To = value; _ToOriginal = _To; Invalidate(); } }
        Point _To = new Point(80, 10);

        [DefaultValue(10)]
        [Category("My")]
        public int LineWidth { get { return _LineWidth; } set { _LineWidth = value; Invalidate(); } }
        int _LineWidth = 10;

        [DefaultValue(LineCap.Round)]
        [Category("My")]
        public LineCap FromShape { get { return _FromShape; } set { _FromShape = value; Invalidate(); } }
        LineCap _FromShape = LineCap.Square;

        [DefaultValue(LineCap.ArrowAnchor)]
        [Category("My")]
        public LineCap ToShape { get { return _ToShape; } set { _ToShape = value; Invalidate(); } }
        LineCap _ToShape = LineCap.ArrowAnchor;

        [DefaultValue(false)]
        [Category("My")]
        public bool UpdatePositionByRatioOnParentSize { get { return _UpdatePositionByRatioOnParentSize; } set { _UpdatePositionByRatioOnParentSize = value; _FromOriginal = _From; _ToOriginal = _To; } }
        bool _UpdatePositionByRatioOnParentSize;
        Point _FromOriginal = new Point(0, 0);
        Point _ToOriginal = new Point(0, 0);
        Size _SizeOriginal = new Size(0, 0);

        public Arrow()
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            Resizing += OnResizing;
        }

        protected override void OnForeColorChanged(EventArgs e)
        {
            Invalidate();
        }

        /// Il faut lire la note de la classe GlassLayer pour savoir pourquoi on override Draw et non OnPaint concernant leur superposition.
        protected override void Draw(Graphics g, Rectangle region_to_draw)
        {
            PaintArrow(g, region_to_draw);
        }

        private void OnResizing(object sender, ResizingEventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                _SizeOriginal = Parent.Size;
            else if (UpdatePositionByRatioOnParentSize)
            {
                if (e.OldSize.Width != 0 && e.OldSize.Height != 0) // Se produit sous Parallel Desktop, pas sous windows
                {
                    _From = new Point(Size.Width * _FromOriginal.X / e.OldSize.Width,
                                      Size.Height * _FromOriginal.Y / e.OldSize.Height);
                    _To = new Point(Size.Width * _FromOriginal.X / e.OldSize.Width,
                                    Size.Height * _FromOriginal.Y / e.OldSize.Height);
                }
                Invalidate();
            }
        }

        // Seulement si la classe de base etait TransparentPanel
        //
        //protected sealed override void OnPaintBackground(PaintEventArgs e)
        //{
        //    base.OnPaintBackground(e);
        //    // Nothing
        //}
        //protected sealed override void OnPaint(PaintEventArgs e)
        //{
        //    base.OnPaint(e);
        //    PaintArrow(e.Graphics, ClientRectangle);
        //}

        void PaintArrow(Graphics g, Rectangle region_to_draw)
        {
            var mode = g.SmoothingMode;
            g.SmoothingMode = SmoothingMode.AntiAlias;

            using (var p = new Pen(ForeColor, LineWidth))
            {
                p.StartCap = FromShape;
                p.EndCap = ToShape;
                g.DrawLine(p, region_to_draw.Location.Add(From), region_to_draw.Location.Add(To));
                g.SmoothingMode = mode;
            }
        }
    }

    public class ArrowDesigner : System.Windows.Forms.Design.ControlDesigner
    { // Thanks to Bob Powell, made with his example http://www.bobpowell.net/FilledGroupBox.aspx

        //True when the mouse is down
        bool _mouseDown;
        //True when the mouse is in the grab-handle.
        int _clic_location; // 0 = none, 1 = on adorner "From", 2 = on adorner "To"

        //Called when the designer is initialized
        public override void Initialize(IComponent component)
        {
            base.Initialize(component);
            //adds handlers to the mouse events of the control
            this.Control.MouseDown += new MouseEventHandler(Control_MouseDown);
            this.Control.MouseUp += new MouseEventHandler(Control_MouseUp);
            this.Control.MouseMove += new MouseEventHandler(Control_MouseMove);
        }

        protected override void Dispose(bool disposing)
        {
            //removes handlers from the mouse events of the control
            this.Control.MouseDown -= new MouseEventHandler(Control_MouseDown);
            this.Control.MouseUp -= new MouseEventHandler(Control_MouseUp);
            this.Control.MouseMove -= new MouseEventHandler(Control_MouseMove);
            base.Dispose(disposing);
        }


        protected override void OnPaintAdornments(PaintEventArgs pe)
        {
            //paints the grab-handle over the focus point.
            base.OnPaintAdornments(pe);
            Arrow fgb = (Arrow)this.Control;
            pe.Graphics.FillRectangle(Brushes.White, fgb.From.X - 4, fgb.From.Y - 4, 8, 8);
            pe.Graphics.DrawRectangle(Pens.Black, fgb.From.X - 4, fgb.From.Y - 4, 8, 8);
            pe.Graphics.FillRectangle(Brushes.White, fgb.To.X - 4, fgb.To.Y - 4, 8, 8);
            pe.Graphics.DrawRectangle(Pens.Black, fgb.To.X - 4, fgb.To.Y - 4, 8, 8);
        }

        /// <summary>Indique sur le clic de la souris au point specifié doit être géré par le control</summary>
        protected override bool GetHitTest(Point point)
        {
            //checks to see if the point is in the grab-handle
            Arrow arrow = (Arrow)this.Control;
            Point pnt = arrow.PointToClient(point);
            Rectangle rc = new Rectangle(arrow.From.X - 4, arrow.From.Y - 4, 8, 8);
            if (rc.Contains(pnt))
            {
                _clic_location = 1;
                return true;
            }
            rc = new Rectangle(arrow.To.X - 4, arrow.To.Y - 4, 8, 8);
            if (rc.Contains(pnt))
            {
                _clic_location = 2;
                return true;
            }
            _clic_location = 0;
            return base.GetHitTest(point);
        }

        protected override void OnSetCursor()
        {
            if (_clic_location != 0)
                Control.Cursor = Cursors.Hand;
            else
                base.OnSetCursor();
        }

        private void Control_MouseDown(object sender, MouseEventArgs e)
        {
            _mouseDown = true;
        }

        private void Control_MouseUp(object sender, MouseEventArgs e)
        {
            _mouseDown = false;
        }

        private void Control_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouseDown && _clic_location != 0)
            {
                //modifies the value in the control
                Arrow arrow = (Arrow)this.Control;
                string prop_name = _clic_location == 1
                                 ? nameof(Arrow.From)
                                 : nameof(Arrow.To);
                MemberDescriptor md = TypeDescriptor.GetProperties(arrow, null)[prop_name];
                PropertyDescriptor pd = md as PropertyDescriptor;
                Point oldPoint = (Point)pd.GetValue(arrow);
                if (pd != null)
                    pd.SetValue(arrow, new Point(e.X, e.Y));
                //update the designer host and the property grid
                this.RaiseComponentChanging(md);
                this.RaiseComponentChanged(md, oldPoint, new Point(e.X, e.Y));
                arrow.Invalidate();
            }
        }
    }
}
