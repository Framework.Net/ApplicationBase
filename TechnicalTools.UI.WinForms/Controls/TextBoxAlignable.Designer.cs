﻿namespace TechnicalTools.UI.Controls
{
    partial class TextBoxAlignable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOuterFormDesign = new System.Windows.Forms.TextBox();
            this.txtValue = new TechnicalTools.UI.Controls.TextBoxImproved();
            this.SuspendLayout();
            //
            // txtOuterFormDesign
            //
            this.txtOuterFormDesign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOuterFormDesign.Location = new System.Drawing.Point(0, 0);
            this.txtOuterFormDesign.Multiline = true;
            this.txtOuterFormDesign.Name = "txtOuterFormDesign";
            this.txtOuterFormDesign.Size = new System.Drawing.Size(1000, 300);
            this.txtOuterFormDesign.TabIndex = 1;
            this.txtOuterFormDesign.TabStop = false;
            //
            // txtValue
            //
            this.txtValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtValue.Location = new System.Drawing.Point(1, 139);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(998, 13);
            this.txtValue.TabIndex = 0;
            //
            // TextBoxAlignable
            //
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.txtOuterFormDesign);
            this.MaximumSize = new System.Drawing.Size(10000, 500);
            this.MinimumSize = new System.Drawing.Size(0, 22);
            this.Name = "TextBoxAlignable";
            this.Size = new System.Drawing.Size(1000, 300);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TechnicalTools.UI.Controls.TextBoxImproved txtValue;
        private System.Windows.Forms.TextBox txtOuterFormDesign;
    }
}
