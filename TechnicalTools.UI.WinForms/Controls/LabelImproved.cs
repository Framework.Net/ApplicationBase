﻿using System;
using System.ComponentModel;
using System.Windows.Forms;


namespace TechnicalTools.UI.Controls
{
    // A cause de http://stackoverflow.com/questions/2519587/is-there-any-way-to-disable-the-double-click-to-copy-functionality-of-a-net-l
    [DesignTimeVisible(true)]
    [DesignerCategory("TechnicalTools.UI")]
    public class LabelImproved : Label
    {
        protected override CreateParams CreateParams
        {
            get
            {
                //new SecurityPermission(SecurityPermissionFlag.UnmanagedCode).Demand();
                CreateParams cp = base.CreateParams;
                cp.ClassStyle &= ~0x0008;
                cp.ClassName = null;

                return cp;
            }
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
 	        base.OnMouseDoubleClick(e);
        }
        protected override void OnMouseDown(MouseEventArgs e)
        {
            _lastMouseDown2 = _lastMouseDown;
            _lastMouseDown = DateTime.UtcNow;

 	         base.OnMouseDown(e);
        }
        DateTime _lastMouseDown;
        DateTime _lastMouseDown2;

        public void ResetDoubleClickSimulationState()
        {
            _lastMouseDown = DateTime.MinValue;
        }
        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            if ((_lastMouseDown - _lastMouseDown2).TotalMilliseconds <= SystemInformation.DoubleClickTime)
                OnMouseDoubleClick(e);
        }
    }
}
