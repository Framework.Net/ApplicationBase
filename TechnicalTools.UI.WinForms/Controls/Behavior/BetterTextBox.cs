﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace TechnicalTools.UI.WinForms.Behavior
{
    /// <summary>
    /// Fonctionalité permettant de sélectionner tous le texte quand la textbox recoit le focus
    /// </summary>
    public partial class TextBox_SelectAllTextOnEnter
    {
        static Dictionary<TextBox, TextBox_SelectAllTextOnEnter> _behaviors = new Dictionary<TextBox, TextBox_SelectAllTextOnEnter>();

        public static bool IsInstalledOn(TextBox txt)
        {
            return _behaviors.ContainsKey(txt);
        }

        public static void InstallOn(TextBox txt)
        {
            if (IsInstalledOn(txt))
                return;
            var behavior = new TextBox_SelectAllTextOnEnter(txt);
            _behaviors.Add(txt, behavior);
        }
        public static void UninstallOn(TextBox txt)
        {
            if (!IsInstalledOn(txt))
                return;
            _behaviors[txt].Uninstall();
        }

        private TextBox_SelectAllTextOnEnter(TextBox txt)
        {
            _txt = txt;
            Install();
        }

        void Install()
        {
            _txt.GotFocus += txt_GotFocus;
            _txt.MouseUp += txt_MouseUp;
            _txt.Leave += txt_Leave;
        }
        void Uninstall()
        {
            _txt.GotFocus -= txt_GotFocus;
            _txt.MouseUp -= txt_MouseUp;
            _txt.Leave -= txt_Leave;
        }

        TextBox _txt;
        bool alreadyFocused;

        void txt_Leave(object sender, EventArgs e)
        {
            alreadyFocused = false;
        }

        void txt_GotFocus(object sender, EventArgs e)
        {
            // Select all text only if the mouse isn't down.
            // This makes tabbing to the textbox give focus.
            if (Form.MouseButtons == MouseButtons.None)
            {
                _txt.SelectAll();
                alreadyFocused = true;
            }
        }

        void txt_MouseUp(object sender, MouseEventArgs e)
        {
            // Web browsers like Google Chrome select the text on mouse up.
            // They only do it if the textbox isn't already focused,
            // and if the user hasn't selected all text.
            if (!alreadyFocused && _txt.SelectionLength == 0)
            {
                alreadyFocused = true;
                _txt.SelectAll();
            }
        }
    }
}
