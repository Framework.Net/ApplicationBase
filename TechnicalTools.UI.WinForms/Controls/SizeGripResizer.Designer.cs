﻿namespace TechnicalTools.UI.Controls
{
    partial class SizeGripResizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picSizeGripper = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picSizeGripper)).BeginInit();
            this.SuspendLayout();
            //
            // picSizeGripper
            //
            this.picSizeGripper.Cursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.picSizeGripper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picSizeGripper.Location = new System.Drawing.Point(0, 0);
            this.picSizeGripper.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.picSizeGripper.Name = "picSizeGripper";
            this.picSizeGripper.Size = new System.Drawing.Size(18, 20);
            this.picSizeGripper.TabIndex = 11;
            this.picSizeGripper.TabStop = false;
            this.picSizeGripper.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picSizeGripper_MouseDown);
            this.picSizeGripper.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picSizeGripper_MouseMove);
            this.picSizeGripper.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picSizeGripper_MouseUp);
            //
            // SizeGripResizer
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picSizeGripper);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "SizeGripResizer";
            this.Size = new System.Drawing.Size(18, 20);
            ((System.ComponentModel.ISupportInitialize)(this.picSizeGripper)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picSizeGripper;
    }
}
