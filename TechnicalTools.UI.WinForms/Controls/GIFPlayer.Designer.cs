﻿namespace TechnicalTools.UI.Controls
{
    partial class GIFPlayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bar = new System.Windows.Forms.Label();
            this.progress = new System.Windows.Forms.Label();
            this.lblPlay = new System.Windows.Forms.Label();
            this.lblStop = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblPause = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            //
            // bar
            //
            this.bar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bar.BackColor = System.Drawing.Color.White;
            this.bar.Location = new System.Drawing.Point(49, 0);
            this.bar.Name = "bar";
            this.bar.Size = new System.Drawing.Size(461, 23);
            this.bar.TabIndex = 26;
            this.bar.MouseClick += new System.Windows.Forms.MouseEventHandler(this.progress_MouseClick);
            //
            // progress
            //
            this.progress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.progress.BackColor = System.Drawing.Color.Blue;
            this.progress.Location = new System.Drawing.Point(49, 0);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(27, 23);
            this.progress.TabIndex = 27;
            this.progress.MouseClick += new System.Windows.Forms.MouseEventHandler(this.progress_MouseClick);
            //
            // lblPlay
            //
            this.lblPlay.BackColor = System.Drawing.Color.Black;
            this.lblPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlay.ForeColor = System.Drawing.Color.White;
            this.lblPlay.Location = new System.Drawing.Point(24, -2);
            this.lblPlay.Margin = new System.Windows.Forms.Padding(0);
            this.lblPlay.Name = "lblPlay";
            this.lblPlay.Size = new System.Drawing.Size(25, 26);
            this.lblPlay.TabIndex = 28;
            this.lblPlay.Tag = "⏸";
            this.lblPlay.Text = "▶";
            this.lblPlay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblPlay.Click += new System.EventHandler(this.lblPlay_Click);
            //
            // lblStop
            //
            this.lblStop.BackColor = System.Drawing.Color.Black;
            this.lblStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStop.ForeColor = System.Drawing.Color.White;
            this.lblStop.Location = new System.Drawing.Point(0, -3);
            this.lblStop.Name = "lblStop";
            this.lblStop.Size = new System.Drawing.Size(25, 26);
            this.lblStop.TabIndex = 29;
            this.lblStop.Text = "⏹";
            this.lblStop.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStop.Click += new System.EventHandler(this.lblStop_Click);
            //
            // panel1
            //
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.lblStop);
            this.panel1.Controls.Add(this.progress);
            this.panel1.Controls.Add(this.bar);
            this.panel1.Controls.Add(this.lblPlay);
            this.panel1.Controls.Add(this.lblPause);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 347);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(510, 23);
            this.panel1.TabIndex = 30;
            //
            // lblPause
            //
            this.lblPause.BackColor = System.Drawing.Color.Black;
            this.lblPause.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPause.ForeColor = System.Drawing.Color.White;
            this.lblPause.Location = new System.Drawing.Point(25, 0);
            this.lblPause.Margin = new System.Windows.Forms.Padding(0);
            this.lblPause.Name = "lblPause";
            this.lblPause.Size = new System.Drawing.Size(24, 23);
            this.lblPause.TabIndex = 31;
            this.lblPause.Tag = "⏸";
            this.lblPause.Text = "⏸";
            this.lblPause.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblPause.UseCompatibleTextRendering = true;
            this.lblPause.Click += new System.EventHandler(this.lblPause_Click);
            //
            // GIFPlayer
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "GIFPlayer";
            this.Size = new System.Drawing.Size(510, 370);
            this.Resize += new System.EventHandler(this.GIFPlayer_Resize);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label bar;
        private System.Windows.Forms.Label progress;
        private System.Windows.Forms.Label lblPlay;
        private System.Windows.Forms.Label lblStop;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblPause;
    }
}
