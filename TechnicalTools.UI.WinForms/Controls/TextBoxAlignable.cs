﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using TechnicalTools.Diagnostics;


namespace TechnicalTools.UI.Controls
{
    /// <summary>
    /// TextBox normal mais qui peut etre alignable verticalement
    /// </summary>
    [DesignTimeVisible(true)]
    [DesignerCategory("TechnicalTools.UI")]
    public partial class TextBoxAlignable : UserControl
    {
        [DefaultValue(ContentAlignment.TopLeft)]
        public virtual ContentAlignment TextAlign { get { return _TextAlign; } set { _TextAlign = value; OnTextAlignChanged(); } } ContentAlignment _TextAlign;

        [Bindable(true)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [DefaultValue("")]
        public override string Text { get { return txtValue.Text; } set { txtValue.Text = value; } }

        [DefaultValue(false)]
        public bool Multiline { get { return txtValue.Multiline; } set { txtValue.Multiline = value; } }

        public TextBoxAlignable()
        {
            InitializeComponent();
            Resize += (_, __) => UpdateTextBoxLocation();
            EventDispatch_Init(new[] { txtValue, txtOuterFormDesign});
            txtValue.TextChanged += (_, __) => OnTextChanged();
            txtValue.MultilineChanged += (_, __) => OnMultilineChanged();
        }

        private void OnTextAlignChanged()
        {
            if (txtValue != null) // Designer...
                txtValue.TextAlign = TextAlign.In(ContentAlignment.BottomLeft, ContentAlignment.MiddleLeft, ContentAlignment.TopLeft) ? HorizontalAlignment.Left
                                   : TextAlign.In(ContentAlignment.BottomRight, ContentAlignment.MiddleRight, ContentAlignment.TopRight) ? HorizontalAlignment.Right
                                   : HorizontalAlignment.Center;
            UpdateTextBoxLocation();
            Invalidate();
        }
        private void OnTextChanged()
        {
            if (txtValue != null) // Designer...
                txtValue.Text = Text;
            UpdateTextBoxLocation();
            Invalidate();
        }
        private void OnMultilineChanged()
        {
            DebugTools.Break("Non géré pour le moment, Voir dans UpdateTextBoxLocation");
            UpdateTextBoxLocation();
        }

        void UpdateTextBoxLocation()
        {
            if (txtValue == null)
                return;
            if (TextAlign.In(ContentAlignment.TopLeft, ContentAlignment.TopCenter, ContentAlignment.TopRight))
                txtValue.Top = 0;
            else if (TextAlign.In(ContentAlignment.BottomLeft, ContentAlignment.BottomCenter, ContentAlignment.BottomRight))
                txtValue.Top = Height - txtValue.Height;
            else
            {
                var txtValueHeight = txtValue.Height; // TODO : Calculer si multiline est a true :
                txtValue.Top = (Height - txtValueHeight) / 2;
            }
        }

        public override bool Focused { get { return txtValue.Focused; } }
        public new bool Capture { get { return txtValue.Capture;  } set { txtValue.Capture = value; } }
        public new event EventHandler MouseCaptureChanged { add { txtValue.MouseCaptureChanged += value; } remove { txtValue.MouseCaptureChanged -= value; } }
        public new event EventHandler LostFocus { add { txtValue.LostFocus += value; } remove { txtValue.LostFocus -= value; } }
        public new event EventHandler GotFocus { add { txtValue.GotFocus += value; } remove { txtValue.GotFocus -= value; } }
        public new event EventHandler TextChanged { add { txtValue.TextChanged += value; } remove { txtValue.TextChanged -= value; } }

        void EventDispatch_Init(Control[] subcontrols)
        {
            txtOuterFormDesign.GotFocus += (_, __) => txtValue.Focus();

            foreach (var ctl in subcontrols)
            {
                ctl.KeyDown += OnControlKeyDown;
                ctl.KeyPress += OnControlKeyPress;
                ctl.KeyUp += OnControlKeyUp;
                ctl.MouseDown += OnControlMouseDown;
                ctl.MouseMove += OnControlMouseMove;
                ctl.MouseUp += OnControlMouseUp;
                ctl.MouseClick += OnControlMouseClick;
                ctl.MouseWheel += OnControlMouseWheel;
                ctl.MouseDoubleClick += OnControlMouseDoubleClick;
                ctl.DoubleClick += OnControlDoubleClick;
                ctl.MouseEnter += OnControlMouseEnter;
                ctl.MouseLeave += OnControlMouseLeave;
            }
        }



        public new event KeyEventHandler KeyDown;
        public new event KeyEventHandler KeyUp;
        public new event KeyPressEventHandler KeyPress;

        public new event MouseEventHandler MouseDown;
        public new event MouseEventHandler MouseMove;
        public new event MouseEventHandler MouseUp;
        public new event MouseEventHandler MouseClick;
        public new event MouseEventHandler MouseDoubleClick;
        public new event MouseEventHandler MouseWheel;
        public new event EventHandler DoubleClick;
        public new event EventHandler MouseEnter;
        public new event EventHandler MouseLeave;

        void OnControlKeyUp(object sender, KeyEventArgs e) { KeyUp?.Invoke(this, e); }
        void OnControlKeyDown(object sender, KeyEventArgs e) { KeyDown?.Invoke(this, e); }
        void OnControlKeyPress(object sender, KeyPressEventArgs e) { KeyPress?.Invoke(this, e); }
        void OnControlMouseEnter(object sender, EventArgs e) { MouseEnter?.Invoke(this, e); }
        void OnControlMouseLeave(object sender, EventArgs e) { MouseLeave?.Invoke(this, e); }
        void OnControlDoubleClick(object sender, EventArgs e) { DoubleClick?.Invoke(this, e); }
        void OnControlMouseDown(object sender, MouseEventArgs e) { MouseDown?.Invoke(this, ConvertEventArgs(sender, e)); }
        void OnControlMouseMove(object sender, MouseEventArgs e) { MouseMove?.Invoke(this, ConvertEventArgs(sender, e)); }
        void OnControlMouseUp(object sender, MouseEventArgs e) { MouseUp?.Invoke(this, ConvertEventArgs(sender, e)); }
        void OnControlMouseClick(object sender, MouseEventArgs e) { MouseClick?.Invoke(this, ConvertEventArgs(sender, e)); }
        void OnControlMouseDoubleClick(object sender, MouseEventArgs e) { MouseDoubleClick?.Invoke(this, ConvertEventArgs(sender, e)); }
        void OnControlMouseWheel(object sender, MouseEventArgs e) { MouseWheel?.Invoke(this, ConvertEventArgs(sender, e)); }
        MouseEventArgs ConvertEventArgs(object sender, MouseEventArgs e)
        {
            var ctl = sender as Control;
            var loc = this.PointToClient(ctl.PointToScreen(e.Location));
            return new MouseEventArgs(e.Button, e.Clicks, loc.X, loc.Y, e.Delta);
        }

    }
}
