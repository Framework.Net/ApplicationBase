﻿namespace TechnicalTools.UI.Controls
{
    partial class LabelToTextBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblIcons = new System.Windows.Forms.Label();
            this.toolTipController = new System.Windows.Forms.ToolTip(this.components);
            this.lblValue = new TechnicalTools.UI.Controls.LabelImproved();
            this.txtValue = new TechnicalTools.UI.Controls.TextBoxAlignable();
            this.SuspendLayout();
            //
            // lblIcons
            //
            this.lblIcons.AutoEllipsis = true;
            this.lblIcons.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblIcons.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblIcons.Location = new System.Drawing.Point(0, 0);
            this.lblIcons.Name = "lblIcons";
            this.lblIcons.Size = new System.Drawing.Size(39, 100);
            this.lblIcons.TabIndex = 2;
            this.lblIcons.Text = "Icons";
            this.lblIcons.Paint += new System.Windows.Forms.PaintEventHandler(this.lblIcons_Paint);
            this.lblIcons.MouseLeave += new System.EventHandler(this.lblIcons_MouseLeave);
            this.lblIcons.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lblIcons_MouseMove);
            //
            // lblValue
            //
            this.lblValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValue.Location = new System.Drawing.Point(39, 0);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(264, 100);
            this.lblValue.TabIndex = 0;
            this.lblValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            //
            // txtValue
            //
            this.txtValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValue.Location = new System.Drawing.Point(0, 0);
            this.txtValue.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.txtValue.MaximumSize = new System.Drawing.Size(10000, 500);
            this.txtValue.MinimumSize = new System.Drawing.Size(0, 22);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(303, 100);
            this.txtValue.TabIndex = 1;
            this.txtValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtValue_KeyDown);
            //
            // LabelToTextBox
            //
            this.Controls.Add(this.lblValue);
            this.Controls.Add(this.lblIcons);
            this.Controls.Add(this.txtValue);
            this.MaximumSize = new System.Drawing.Size(10000, 500);
            this.MinimumSize = new System.Drawing.Size(0, 22);
            this.Name = "LabelToTextBox";
            this.Size = new System.Drawing.Size(303, 100);
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.Controls.LabelImproved lblValue;
        private TechnicalTools.UI.Controls.TextBoxAlignable txtValue;
        private System.Windows.Forms.Label lblIcons;
        private System.Windows.Forms.ToolTip toolTipController;
    }
}
