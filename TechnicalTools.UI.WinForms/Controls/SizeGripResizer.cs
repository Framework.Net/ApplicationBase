﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.IO;


namespace TechnicalTools.UI.Controls
{
    [DesignTimeVisible(true)]
    [DesignerCategory("TechnicalTools.UI")]
    public partial class SizeGripResizer : UserControl
    {
        public bool IsResizing { get { return _mouseDown.HasValue; } }

        public SizeGripResizer()
        {
            InitializeComponent();
            if (picSizeGripper != null)
                picSizeGripper.Image = GripperPng;
        }

        private void picSizeGripper_MouseDown(object sender, MouseEventArgs e)
        {
            _mouseDown = e.Location;
        }
        Point? _mouseDown;

        private void picSizeGripper_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouseDown.HasValue && Parent != null)
            {
                Parent.Size = new Size(Parent.Width + e.X - _mouseDown.Value.X,
                                       Parent.Height + e.Y - _mouseDown.Value.Y);
            }
        }

        private void picSizeGripper_MouseUp(object sender, MouseEventArgs e)
        {
            _mouseDown = null;
        }

        // http://www.dailycoding.com/Utils/Converter/ImageToBase64.aspx
        // http://codebeautify.org/base64-to-image-converter#
        public static readonly string GripperPngAsBase64 = "iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9sKDQw6Gt0mWFcAAACASURBVEjHY2AYBXQGacuWLVtBU8P///+fRlPDaWHBqOGDyPCioqIe9GTKRIbhTpGRkfsYGBgYGBkZZyEbzsDAwACTI8cCgob39vbeokmwFBUV9VASH6OGU89wmByhVER0aiEmJTERazgsA2EzHF0OJzh8+PBeXOGKL8wpjY8RDgBeY/1AreoK4wAAAABJRU5ErkJggg==";
        public static readonly Image GripperPng = Image.FromStream(new MemoryStream(System.Convert.FromBase64String(GripperPngAsBase64)));
    }
}
