﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

using TechnicalTools.UI.WinForms.Behavior;


namespace TechnicalTools.UI.WinForms
{
    [DefaultEvent("SelectionChanged")]
    public partial class FileFolderSelector : UserControl
    {
        public FileFolderSelector()
        {
            InitializeComponent();
            TextBox_SelectAllTextOnEnter.InstallOn(txtSelection);
            CanBeRecursive = false;
            InitEvents();
        }

        private void FileFolderSelector_Load(object sender, EventArgs e)
        {
            RefreshTitle();
        }

        public new void Focus()
        {
            txtSelection.Focus();
        }
        public override bool Focused { get { return txtSelection.Focused; } }
        public new event EventHandler Enter { add { txtSelection.Enter += value; } remove { txtSelection.Enter -= value; } }
        public new event EventHandler Leave { add { txtSelection.Leave += value; } remove { txtSelection.Leave -= value; } }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            DialogResult res;
            if (selector_type == SelectorType.File)
                res = openFileDialog.ShowDialog();
            else
                res = folderBrowserDialog.ShowDialog();

            if (res == DialogResult.Cancel)
                return;
            if (selector_type == SelectorType.File)
                txtSelection.Text = openFileDialog.FileName;
            else
                txtSelection.Text = folderBrowserDialog.SelectedPath;
        }

        private void txtType_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnBrowse_Click(null, null);
        }

        private void txtType_KeyPress(object sender, KeyPressEventArgs e)
        {
            var txt = sender as TextBox;
            if (e.KeyChar == '/')
                e.KeyChar = '\\';
            if (e.KeyChar == ':' && txt.Text.Contains(":"))
            {
                e.KeyChar = '\0';
                e.Handled = true;
            }
            char[] invalidChars = System.IO.Path.GetInvalidFileNameChars()
                                                .Where(c => c != '\b'  // backspace character
                                                         && c != ':'   // because handled above
                                                         && c != (char)22 // 22 == \u0016 => happens when using Ctrl-V
                                                         && c != (char)24 // 24 == \u0018 => happens when using Ctrl-C
                                                         && c != (char)25 // 25 == \u0019 => happens when using Ctrl-Y
                                                         && c != (char)26 // 26 == \u001A => happens when using Ctrl-Z
                                                         && (c != PathSeparator || !AllowMultiple))
                                                .ToArray();
            if (invalidChars.Contains(e.KeyChar))
            {
                e.KeyChar = '\0';
                e.Handled = true;
            }
        }

        public enum SelectorType { Folder, File };
        private SelectorType selector_type = SelectorType.Folder;
        
        [Category("TechnicalTools.UI.WinForms")]
        [DefaultValue(SelectorType.Folder)]
        public SelectorType SelectType
        {
            get { return selector_type; }
            set { selector_type = value; RefreshTitle(); }
        }

        /// <summary>
        /// Permet a l'utilisateur de cocher une case indiquant que les sous dossier doivent être pris en compte
        /// </summary>
        [Category("TechnicalTools.UI.WinForms")]
        [DefaultValue(false)]
        public bool CanBeRecursive
        {
            get { return chkRecursive.Visible; }
            set { chkRecursive.Visible = value; Height = 37 + (value ? 24 : 0); }
        }
        /// <summary>
        /// Indique si la case a cocher debloqué par CanBeRecursive est coché ou non
        /// </summary>
        [Category("TechnicalTools.UI.WinForms")]
        [DefaultValue(false)]
        public bool IsRecursive
        {
            get { return chkRecursive.Checked; }
            set { chkRecursive.Checked = value; }
        }

        [Category("TechnicalTools.UI.WinForms")]
        [DefaultValue(true)]
        public bool DisplayButton
        {
            get { return !splitContainer.Panel2Collapsed; }
            set { splitContainer.Panel2Collapsed = !(bool)value; }
        }

        public void RefreshTitle()
        {
            if (_Title != null)
                lblType.Text = Title;
            else if (SelectType == SelectorType.File)
                lblType.Text = "File";
            else
                lblType.Text = "Folder";
            lblType.Text += " :";
        }

        [Category("TechnicalTools.UI.WinForms")]
        [DefaultValue(null)]
        public string Title
        {
            get { return _Title; }
            set
            {
                _Title = value;
                if (string.IsNullOrWhiteSpace(value) && DesignTimeHelper.IsInDesignMode)
                    _Title = null; // pour restorer le comportement par défaut
                RefreshTitle();
            }
        }
        string _Title = null;

        [Category("TechnicalTools.UI.WinForms")]
        [DefaultValue(@"C:\")]
        public override string Text
        {
            get { return txtSelection.Text; }
            set { txtSelection.Text = value; }
        }

        [Category("TechnicalTools.UI.WinForms")]
        [DefaultValue(@"C:\")]
        public string Selection
        {
            get { return Text; }
            set { Text = value; }
        }

        /// <summary> Allow enter (manually, UI does not handle that) multiple folders using <see cref="PathSeparator"/></summary>
        [Category("TechnicalTools.UI.WinForms")]
        [DefaultValue(false)]
        public bool AllowMultiple { get; set; }
        public char PathSeparator { get { return '|'; } }

        #region Events
        private void InitEvents()
        {
            txtSelection.Validated += new EventHandler(txtType_Validated);
        }
        
        #endregion Events
        
        [BrowsableAttribute(true)]
        public event EventHandler SelectionChanged;
        public new event EventHandler TextChanged { add { SelectionChanged += value; } remove { SelectionChanged -= value; } }

        
        private void txtType_Validated(object sender, EventArgs e)
        {
            if (_txtSelectionHasChanged)
            {
                _txtSelectionHasChanged = false;
                if (SelectionChanged != null)
                    SelectionChanged(this, e);
            }
        }

        private void txtType_Enter(object sender, EventArgs e)
        {
            txtSelection.SelectionStart = 0;
            txtSelection.SelectionLength = txtSelection.Text.Length;
        }

        bool _txtSelectionHasChanged = false;
        private void txtSelection_TextChanged(object sender, EventArgs e)
        {
            _txtSelectionHasChanged = true;
        }

        private void chkRecursive_CheckedChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
                SelectionChanged(this, e);
        }

    }
}
