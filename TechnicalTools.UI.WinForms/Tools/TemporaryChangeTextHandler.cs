﻿using System;
using System.Windows.Forms;


namespace TechnicalTools.UI.Tools
{
    public class TemporaryChangeTextHandler : IDisposable
    {
        readonly Control _ctl;
        readonly string _textBefore;

        public TemporaryChangeTextHandler(Control ctl, string newText)
        {
            _ctl = ctl;
            _textBefore = _ctl.Text;
            _ctl.Text = newText;
            _ctl.Refresh();
        }

        void IDisposable.Dispose()
        {
            _ctl.Text = _textBefore;
        }
    }
}
