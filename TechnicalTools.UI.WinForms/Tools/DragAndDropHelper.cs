﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TechnicalTools.UI.Tools
{
    public class DragAndDropHelper
    {
        public static void SetupHandlerOnFileDragged(Control ctl, Action<IReadOnlyList<string>> doWithFiles, Func<IReadOnlyList<string>, bool> acceptDrag = null)
        {
            new DragAndDropHelper(ctl, doWithFiles, acceptDrag);
        }
        public static void SetupHandlerOnFileDragged(Control ctl, Func<IReadOnlyList<string>, IReadOnlyList<string>> doWithFiles, Func<IReadOnlyList<string>, bool> acceptDrag = null)
        {
            new DragAndDropHelper(ctl, files => doWithFiles(files), acceptDrag);
        }
        private DragAndDropHelper(Control ctl, Action<IReadOnlyList<string>> doWithFiles, Func<IReadOnlyList<string>, bool> acceptDrag)
        {
            _ctl = ctl;
            _doWithFiles = doWithFiles ?? throw new ArgumentNullException(nameof(doWithFiles));
            _acceptDrag = acceptDrag;
            if (_acceptDrag == null)
                _acceptDrag = _ => true;

            ctl.AllowDrop = true;
            ctl.DragEnter += OnDragEnter;
            ctl.DragDrop += OnDragDrop;
        }
        readonly Control _ctl;
        readonly Action<IReadOnlyList<string>> _doWithFiles;
        readonly Func<IReadOnlyList<string>, bool> _acceptDrag;

        void OnDragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.FileDrop))
                return;
            var files = (string[])e.Data.GetData(DataFormats.FileDrop);

            if (files != null && _acceptDrag(files))
                e.Effect = DragDropEffects.Move;
            else
                e.Effect = DragDropEffects.None;
        }

        void OnDragDrop(object sender, DragEventArgs e)
        {
            var files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files == null) // happens if names are too long
                return;
            // To avoid freeing explorer.exe we use this complex code 
            // using just _ctl.BeginInvoke alone does not work (even called a second time nested)
            Task.Run(() =>
            {
                // The "Wait()" seems to do the trick!
                // Maybe by forcing app to process internal event first
                // (calling Application.DoEvents in the first place does not work either)
                Task.Delay(1).Wait();
                _ctl.BeginInvoke((Action)(() =>
                {
                    _doWithFiles(files);
                }));
            });
        }

        
    }
}
