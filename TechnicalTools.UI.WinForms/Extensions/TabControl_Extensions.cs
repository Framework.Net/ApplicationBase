﻿using System;
using System.Linq;
using System.Windows.Forms;


namespace TechnicalTools.UI.Tools
{
    public static class TabControl_Extensions
    {
        public static void ForceControlCreations(this TabControl control)
        {
            ForceControlCreations((Control)control);
            foreach (Control subcontrol in control.Controls)
            {
                ForceControlCreations(subcontrol);
            }
        }
        public static void ForceControlCreations(Control control)
        {
            var method = control.GetType().GetMethod("CreateControl", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            var parameters = method.GetParameters();
            method.Invoke(control, new object[] { true });
        }

    }
}
