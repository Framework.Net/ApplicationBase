﻿using System;
using System.Drawing;


namespace TechnicalTools.UI
{
    public static class Rectangle_Extensions
    {
        public static Rectangle Translate(this Rectangle a, Point vector) { return new Rectangle(a.X + vector.X, a.Y + vector.Y, a.Width, a.Height); }

        public static Rectangle MoveLeftBy(this Rectangle rect, int shift)
        {
            shift = Math.Min(shift, rect.Width);
            return new Rectangle(rect.X + shift, rect.Y, rect.Width - shift, rect.Height);
        }
        public static Rectangle MoveRightBy(this Rectangle rect, int shift)
        {
            shift = Math.Min(shift, rect.Width);
            return new Rectangle(rect.X, rect.Y, rect.Width - shift, rect.Height);
        }

        public static Point[] GetPoints(this Rectangle rect)
        {
            return new Point[] { new Point(rect.Left, rect.Top),
                                 new Point(rect.Right, rect.Top),
                                 new Point(rect.Right, rect.Bottom),
                                 new Point(rect.Left, rect.Bottom)};
        }
        public static PointF[] GetPoints(this RectangleF rect)
        {
            return new PointF[] { new PointF(rect.Left, rect.Top),
                                  new PointF(rect.Right, rect.Top),
                                  new PointF(rect.Right, rect.Bottom),
                                  new PointF(rect.Left, rect.Bottom)};
        }
        public static RectangleF CreateFromPoints(this PointF[] pts)
        {
            float minX = pts[0].X;
            float maxX = pts[0].X;
            float minY = pts[0].Y;
            float maxY = pts[0].Y;
            foreach (var pt in pts)
            {
                minX = Math.Min(minX, pt.X);
                maxX = Math.Max(maxX, pt.X);
                minY = Math.Min(minY, pt.Y);
                maxY = Math.Max(maxY, pt.Y);
            }
            return new RectangleF(minX, minY, maxX - minX, maxY - minY);
        }

    }
}
