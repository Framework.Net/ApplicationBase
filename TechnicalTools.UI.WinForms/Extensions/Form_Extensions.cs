﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;


namespace TechnicalTools.UI
{
    public static class Form_Extensions
    {
        public static Form CenterForm(this Form child, Form parent)
        {
            child.StartPosition = FormStartPosition.Manual;
            child.Location = new Point(parent.Location.X + (parent.Width - child.Width) / 2, parent.Location.Y + (parent.Height - child.Height) / 2);
            return child;
        }

        public static Tuple<SynchronizationContext, Form> ShowInOtherGuiThread(Func<Form> createForm)
        {
            Form form = null;
            SynchronizationContext ctx = null;

            var evt = new ManualResetEvent(false);
            var th = new Thread(() =>
            {
                var frm = createForm();
                form = frm;
                frm.HandleCreated += (_, __) =>
                {
                    ctx = SynchronizationContext.Current;
                    evt.Set();
                };
                Application.Run(frm);
            });
            th.SetApartmentState(ApartmentState.STA); // Sinon certain controle graphique plante (exemple http://stackoverflow.com/questions/135803/dragdrop-registration-did-not-succeed)
            th.IsBackground = true;
            th.Start();
            evt.WaitOne();
            evt.Dispose();
            return Tuple.Create(ctx, form);
        }

        private static void Frm_Shown(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
