﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TechnicalTools.UI.WinForms.Forms

{
    /// <summary>
    /// Summary description for InputBoxForm.
    /// </summary>
    public partial class InputBoxForm : Form
    {
        public InputBoxForm()
        {
            // Required for Windows Form Designer support
            InitializeComponent();
            this.strReturnValue = "";
        }
        string strReturnValue;
        Point pntStartLocation;

        public new DialogResult ShowDialog()
        {
            txtResult.Focus();
            return base.ShowDialog();
        }

        private void InputBoxForm_Load(object sender, System.EventArgs e)
        {
            if (!this.pntStartLocation.IsEmpty)
            {
                this.Top = this.pntStartLocation.X;
                this.Left = this.pntStartLocation.Y;
            }
        }

        private void btnOK_Click(object sender, System.EventArgs e)
        {
            this.strReturnValue = this.txtResult.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public string Title
        {
            set
            {
                this.Text = value;
            }
        }

        public string Prompt
        {
            set
            {
                this.lblText.Text = value;
            }
        }

        public string ReturnValue
        {
            get
            {
                return strReturnValue;
            }
        }

        public string DefaultResponse
        {
            set
            {
                this.txtResult.Text = value;
                this.txtResult.SelectAll();
            }
        }

        public Point StartLocation
        {
            set
            {
                this.pntStartLocation = value;
            }
        }
    }
}
