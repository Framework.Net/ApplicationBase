﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace TechnicalTools.UI.WinForms
{
    /// <summary>
    /// Cette classe permet de n'autoriser qu'une seule instance de l'application à l'exécution.
    /// Il suffit pour cela de remplacer dans Program.cs :
    ///   Application.Run(new frmMain());
    /// par
    ///   OneInstanceApp.Run(new frmMain());
    /// </summary>
    public class OneInstanceApp : IMessageFilter
    {
        #region Importations d'API
        // Permet d'envoyer un message windows sans attendre sa réception
        [DllImport("user32.dll")]
        private static extern int PostMessageA(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam);

        // Permet de récupérer un numéro de message personnalisé unique
        [DllImport("user32.dll")]
        internal static extern int RegisterWindowMessage(StringBuilder lpString);

        // Permet d'ajouter une chaîne de caractères dans la table des Atomes globaux
        [DllImport("kernel32.dll")]
        internal static extern int GlobalAddAtom(StringBuilder lpString);

        // Permet de récupérer la valeur d'un Atome global
        [DllImport("kernel32.dll")]
        internal static extern int GlobalGetAtomName(int atomID, StringBuilder lpString, int nSize);

        // Permet de supprimer un Atome global
        [DllImport("kernel32.dll")]
        internal static extern int GlobalDeleteAtom(int atomID);

        // Permet de récupérer le handle de la fenêtre de l'application
        [DllImport("user32.dll")]
        internal static extern IntPtr FindWindow(string className, string formName);
        #endregion

        // Nom du message windows personnalisé permettant d'envoyer la ligne de commandes
        internal const string ONE_INSTANCE_WM_NAME = "WM_ONE_INST_APP";

        static Mutex oneInstanceMutex;

        protected void Check_(string MainFormTitle)
        {
            _MainFormTitle = MainFormTitle;

            // Nom du mutex utilisé pour détecter si l'application est déjà lancée
            string ONE_INSTANCE_MUTEX_NAME;
            // Obtient le nom du mutex
            StackTrace st = new StackTrace(true);
            int i = 1;
            while (i < st.FrameCount && st.GetFrame(i).GetMethod().ReflectedType.Name == typeof(SystrayManager).Name)
                ++i;
            Debug.Assert(i < st.FrameCount, "");

            ONE_INSTANCE_MUTEX_NAME = st.GetFrame(i).GetMethod().ReflectedType.Assembly.FullName;
            ONE_INSTANCE_MUTEX_NAME = ONE_INSTANCE_MUTEX_NAME.Remove(ONE_INSTANCE_MUTEX_NAME.IndexOf(','));

            // Crée un mutex nommé et essaye de prendre la main dessus
            bool isNewMutex;
            oneInstanceMutex = new Mutex(true, ONE_INSTANCE_MUTEX_NAME, out isNewMutex);

            // Le mutex n'est pas libre (l'application a déjà été lancé)
            if (!isNewMutex)
            {
                // Récupère un numéro de message windows unique
                StringBuilder sBuilder = new StringBuilder(ONE_INSTANCE_WM_NAME);
                int wmUserId = RegisterWindowMessage(sBuilder);

                // Récupère le handle de la fenêtre principale de la première instance
                IntPtr hWnd = FindWindow(null, _MainFormTitle);
                if (hWnd != IntPtr.Zero)
                {
                    // La ligne de commandes contient des arguments (en dehors du chemin vers l'exe)
                    string[] args = System.Environment.GetCommandLineArgs();
                    if (args.Length > 1)
                    {
                        // Parcourt les arguments de la ligne de commandes
                        for (int argIndex = 1; argIndex < args.Length; argIndex++)
                        {
                            // Crée un atome dans la table globale
                            int atomID = GlobalAddAtom(new StringBuilder(args[argIndex]));

                            // Poste un message windows contenant l'identifiant de l'atome
                            if (atomID != 0)
                                PostMessageA(hWnd, wmUserId, IntPtr.Zero, new IntPtr(atomID));
                        }
                    }

                    // Poste un message windows indiquant que la première instance de l'application doit s'activer
                    PostMessageA(hWnd, wmUserId, IntPtr.Zero, IntPtr.Zero);
                }

                // Indique qu'il ne faut pas lancer l'application
                System.Environment.Exit(0);
            }
        }
        string _MainFormTitle;

        protected void Run_(Form form)
        {
            _main_form = form;

            if (_MainFormTitle == null)
                Check_(form.Text);

            // Ajoute un filtre des messages windows
            Application.AddMessageFilter(this);

            // Récupère un identifiant unique pour les messages windows personnalisés
            StringBuilder sBuilder = new StringBuilder(ONE_INSTANCE_WM_NAME);
            _wmUserId = RegisterWindowMessage(sBuilder);

            // L'application a été lancé avec des arguments
            List<string> args = new List<string>(System.Environment.GetCommandLineArgs());
            args.RemoveAt(0);
            if (args.Count > 0)
                OnCommandLine(args);

            // Force l'utilisateur à se rendre compte qu'il y a un probleme
            // si il tente de modifier la propriete Text dans le code de la form.
            _main_form.Text = _MainFormTitle;
            Application.Run(_main_form);

            // Supprime le filtre de messages
            Application.RemoveMessageFilter(this);

            // Relâche le mutex
            oneInstanceMutex.ReleaseMutex();
        }
        private int _wmUserId; // Contient l'identifiant unique des messages personnalisés
        Form _main_form;

        #region public
        // Redéfinie juste les noms des fonctions pour éviter d'avoir à écrire Instance
        /// <summary>
        /// Démarre l'application normalement en étant sur qu'une autre instance n'existe pas.
        /// Appelle Check avec le titre de la form afin de vérifier l'unicite de ce programme.
        /// Pour eviter de creer la form avant la vérification, utilisez la fonction Check avant Run.
        /// </summary>
        /// <param name="form">Form principale du programme. Si Check a été appelé par vous même, sa propriété Text doit être égale à l'argument fournit à Check</param>
        public static void Run(Form form) { Instance.Run_(form); }

        /// <summary>
        /// Vérifie qu'une autre instance de ce programme n'est pas déjà demarré.
        /// Si c'est le cas l'application transfert sa ligne de commande au programme déja lancé, 
        /// puis se termine immédiatement.
        /// </summary>
        /// <param name="MainFormTitle">Nom de la fenêtre principale du programme, sinon la vérification ne marchera pas</param>
        public static void Check(string MainFormTitle) { Instance.Check_(MainFormTitle); }
        #endregion

        // Obligé d'avoir une instance pour implémenter IMessageFilter
        #region Singleton
        private OneInstanceApp() { }
        static OneInstanceApp instance = null;
        protected static OneInstanceApp Instance { get { return instance ?? (instance = new OneInstanceApp()); } }
        #endregion

        #region IMessageFilter Membres

        bool IMessageFilter.PreFilterMessage(ref Message msg)
        {
            try
            {
                // Il s'agit d'un message personnalisé
                if (_main_form != null && msg.HWnd == _main_form.Handle && msg.Msg == _wmUserId)
                {
                    // Un identifiant d'atome a été transmis
                    if (msg.LParam != IntPtr.Zero)
                    {
                        // Récupère la valeur de l'Atome
                        StringBuilder sBuilder = new StringBuilder(256);
                        if (GlobalGetAtomName(msg.LParam.ToInt32(), sBuilder, sBuilder.Capacity) > 0)
                        {
                            // Ajoute l'argument transmis à la liste
                            List<string> args = new List<string>();
                            args.Add(sBuilder.ToString());
                            OnCommandLine(args);

                            // Supprime l'atome de la table globale
                            GlobalDeleteAtom(msg.LParam.ToInt32());
                        }
                    }
                    else
                        OnCommandLine(new List<string>());

                    // Indique que le message a été traité
                    msg.Result = new IntPtr(1);
                    return true;
                }
            }
            catch
            {
            }
            // Indique que le message n'a pas été traité
            return false;
        }


        public delegate void OnCommandLineEvent(List<string> args);
        public static event OnCommandLineEvent OnCommandLine = delegate { };
        #endregion
    }
}
