﻿using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Reflection;

using System.Diagnostics;

namespace TechnicalTools.UI.WinForms
{
    /// <summary>
    /// Code from ~2012
    /// Pour ajouter une gestion du systray dans l'application : 
    /// - Définir l'attribut : SystrayManager _systray_manager dans votre Form principale
    /// - Ecrire dans le constructeur de la form principale : _systray_manager = new SystrayManager(this /*, TODO : chemin vert l'icone à mettre */);
    /// - Définir si besoin un gestionnaire pour _systray_manager.OnApplicationClosing += ... (aller voir le commentaire pour la raison)
    /// </summary>
    public class SystrayManager : Component
    {
        public SystrayManager(Form mainForm)
            : this(mainForm, "") { }
        public SystrayManager(Form mainForm, string relative_embedded_icon_filename)
        {
            Assembly main_assembly;
            if (relative_embedded_icon_filename == "")
            {
                main_assembly = Assembly.GetExecutingAssembly();
                relative_embedded_icon_filename = "images.icons.gear16.gif";
            }
            else
            {
                StackTrace st = new StackTrace(true);
                int i = 1;
                while (i < st.FrameCount && st.GetFrame(i).GetMethod().ReflectedType.Name == typeof(SystrayManager).Name)
                    ++i;
                Debug.Assert(i < st.FrameCount, "");
                main_assembly = st.GetFrame(i).GetMethod().ReflectedType.Assembly;
            }

            _mainForm = mainForm;

            InitializeComponents();

            _mainForm.FormClosing += new FormClosingEventHandler(mainForm_FormClosing);
            _mainForm.FormClosed +=new FormClosedEventHandler(mainForm_FormClosed);
            
            // Récupère le nom de l'assembly principal
            string assembly_name = main_assembly.FullName;
            assembly_name = assembly_name.Remove(assembly_name.IndexOf(','));

            Icon icon = null;
            // On charge en priorité l'icone calculé à la main parce qu'il est beaucoup plus joli
            if (relative_embedded_icon_filename.EndsWith(".ico"))
                icon = new Icon(main_assembly.GetManifestResourceStream(assembly_name + "." + relative_embedded_icon_filename));
            else
                try
                {
                    Stream imgStream = main_assembly.GetManifestResourceStream(assembly_name + "." + relative_embedded_icon_filename);
                    Bitmap bmp = new Bitmap(imgStream);
                    icon = Icon.FromHandle(bmp.GetHicon());
                }
                catch
                {
                }
            _mainForm.Icon = icon;
            ntfAppIcon.Icon = _mainForm.Icon;
        }

        
        public bool AlwaysShowIconInSystray
        {
            get
            {
                return _AlwaysShowIconInSystray;
            }
            set
            {
                _AlwaysShowIconInSystray = value;
                if (AlwaysShowIconInSystray && !ntfAppIcon.Visible)
                    ntfAppIcon.Visible = true;
            }
        }
        bool _AlwaysShowIconInSystray = false;

        public bool AlwaysHideInTaskBar
        {
            get
            {
                return _AlwaysHideInTaskBar;
            }
            set
            {
                // Si l'icone dans le systray n'est pas visible et que la form non plus
                if (!ntfAppIcon.Visible && AlwaysHideInTaskBar && ! value)
                    _mainForm.ShowInTaskbar = true; // code dupliqué depuis ActivateForm
                _AlwaysHideInTaskBar = value;
            }
        }
        bool _AlwaysHideInTaskBar = false;

        public void ShowToolTip(string text, int timeout)
        {
            ntfAppIcon.ShowBalloonTip(timeout, Path.GetFileNameWithoutExtension(Application.ExecutablePath), text, ToolTipIcon.Info);
        }

        /// <summary>Rend la fenêtre visible et l'active</summary>
        public void ActivateForm()
        {
            // Affiche l'application dans la barre des tâches
            _mainForm.ShowInTaskbar = ! AlwaysHideInTaskBar; // code dupliqué dans le setter de NeverShowInTaskBar

            // Cache l'icône du systray
            ntfAppIcon.Visible = AlwaysShowIconInSystray;

            // Affiche la fenêtre dans son état normal
            _mainForm.WindowState = FormWindowState.Normal;
            if (AlwaysHideInTaskBar) // Evite l'effet de maximisation
                _mainForm.Show();

            // Active la fenêtre
            _mainForm.Activate();
        }

        /// <summary>Rend la fenêtre invisible</summary>
        public void DesactivateForm()
        {
            // Réduit la fenêtre dans la barre des tâches
            if (AlwaysHideInTaskBar) // Evite l'effet de minimization
                _mainForm.Hide();
            _mainForm.WindowState = FormWindowState.Minimized;
            
            // Empêche l'application d'être visible dans la barre des tâches
            _mainForm.ShowInTaskbar = false;

            // Affiche l'icône du systray
            ntfAppIcon.Visible = true;
        }

        int user_menu_item_count = 0;
        public void AddMenuItem(ToolStripMenuItem item)
        {
            this.contextMenu.Items.Insert(user_menu_item_count, item);
            ++user_menu_item_count;
        }

        // L'appel à Application.Exit(), lors du click sur le menu Fermer du systray, provoque un bug sur les forms 
        // dont on a mis ShowInTaskBar a false au runtime (car si on le met a false en mode design c'est bon),
        // Ces forms n'éxecutent pas, dans ce cas uniquement, l'evenement FormClosing 
        // Reference du bug ici : https://connect.microsoft.com/VisualStudio/feedback/details/222643/showintaskbar-causes-stops-formclosing-event
        // Donc on permet au fenetre d'ajouter leur gestionnaire via cet évènement.
        public event FormClosingEventHandler OnApplicationClosing;

        /// <summary>
        /// Etant donné que Application.Exit est buggué <see cref="OnApplicationClosing"/>, il est preferable d'utiliser cette méthode
        /// </summary>
        public void ApplicationExit()
        {
            _quitting = true;

            if (OnApplicationClosing != null)
            {
                var ee = new FormClosingEventArgs(CloseReason.ApplicationExitCall, false);
                OnApplicationClosing(this, ee);
                if (ee.Cancel)
                    return;
            }
            Application.Exit();
        }
        #region Private

        #region Fields
        readonly Form _mainForm;
        bool _quitting = false;

        private NotifyIcon ntfAppIcon;
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem itmShow;
        private ToolStripSeparator itmSeparator;
        private ToolStripMenuItem itmClose;
        #endregion Fields


        void InitializeComponents()
        {
            this.itmShow = new System.Windows.Forms.ToolStripMenuItem();
            this.itmSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.itmClose = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip();
            this.ntfAppIcon = new System.Windows.Forms.NotifyIcon();
            this.contextMenu.SuspendLayout();
            // 
            // itmShow
            // 
            this.itmShow.Name = "itmShow";
            this.itmShow.Size = new Size(123, 22);
            this.itmShow.Text = "Afficher";
            this.itmShow.Click += this.itmShow_Click;
            // 
            // itmSeparator
            // 
            this.itmSeparator.Name = "itmSeparator";
            this.itmSeparator.Size = new System.Drawing.Size(120, 6);
            // 
            // itmClose
            // 
            this.itmClose.DisplayStyle = ToolStripItemDisplayStyle.Text;
            this.itmClose.Name = "itmClose";
            this.itmClose.Size = new Size(123, 22);
            this.itmClose.Text = "Quitter";
            this.itmClose.Click += this.itmClose_Click;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new ToolStripItem[] 
            {
                this.itmShow,
                this.itmSeparator,
                this.itmClose
            });
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new Size(124, 54);
            // 
            // ntfAppIcon
            // 
            this.ntfAppIcon.ContextMenuStrip = this.contextMenu;
            this.ntfAppIcon.Text = _mainForm.Text;
            this.ntfAppIcon.MouseDown += this.ntfAppIcon_MouseDown;

            this.contextMenu.ResumeLayout(false);
        }

        /// <summary>A la fermeture de l'application</summary>
        void mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_quitting)
                return;
            // Annule la fermeture
            e.Cancel = true;

            // Met l'appli dans la zone du systray
            DesactivateForm();
        }

        void mainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Efface l'icône du systray
            ntfAppIcon.Visible = false;
        }
        
        /// <summary>L'utilisateur enfonce un boutton au-dessus de l'icône du systray</summary>
        void ntfAppIcon_MouseDown( object sender, MouseEventArgs e )
        {
            // Affiche la fenêtre
            if( e.Button != MouseButtons.Right )
                ActivateForm();
        }

        /// <summary>L'utilisateur demande à fermer l'application</summary>
        void itmClose_Click(object sender, EventArgs e)
        {
            ApplicationExit();
        }

        /// <summary>L'utilisateur demande à afficher la fenêtre</summary>
        void itmShow_Click( object sender, System.EventArgs e )
        {
            ActivateForm();
        }
        #endregion Private
        /*
        private void btnReduce_Click(object sender, EventArgs e)
        {
            EventHandler handler = new EventHandler(delegate(object sender_, EventArgs e_)
            {
                if (not_icon != null)
                    not_icon.Visible = false;
                form.WindowState = FormWindowState.Normal;
                form.Show();
            });
            if (not_icon != null)
            {
                not_icon.Click += handler;
                not_icon.DoubleClick += handler;
            }
            form.Hide();
            if (not_icon != null)
                not_icon.Visible = true;
        }
        */
    }
}
