﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;

using TechnicalTools.MetaProgramming.ExpressionVisitors;


namespace ApplicationBase.Business.MetaProgramming.DecisionTree
{
    // TODO : this class could be removed and replaced by  ReplaceNodeVisitor
    class ReplaceParameterVisitor : ConditionalExpressionVisitor
    {
        public ReplaceParameterVisitor(string paramNameToSubstitute, ParameterExpression substitute)
        {
            _paramNameToSubstitute = paramNameToSubstitute;
            _substitute = substitute;
        }
        protected readonly string _paramNameToSubstitute;
        protected readonly ParameterExpression _substitute;

        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (node.Name == _paramNameToSubstitute)
                return _substitute;
            else 
                return base.VisitParameter(node);
        }
    }

    class ReplaceNodeVisitor : ConditionalExpressionVisitor
    {
        public ReplaceNodeVisitor(Expression toReplace, Expression substitute)
        {
            _toReplace = toReplace;
            _substitute = substitute;
        }
        protected readonly Expression _toReplace;
        protected readonly Expression _substitute;

        public override Expression Visit(Expression node)
        {
            if (node == _toReplace)
                return _substitute;
            return base.Visit(node);
        }
    }

    class OrSplitterVisitor : ConditionalExpressionVisitor
    {
        public virtual List<Expression> ExtractPathOfConditions(LambdaExpression exp)
        {
            _ConditionPaths = new List<Expression>();
            Debug.Assert(_capture);
            VisitAndConvert(exp.Body, nameof(ExtractPathOfConditions));
            return _ConditionPaths;
        }

        protected List<Expression> _ConditionPaths;
        protected bool _capture = true;

        public override Expression Visit(Expression node)
        {
            // We consider Or & OrElse the same here
            if (node is BinaryExpression bNode && bNode.Conversion == null &&
                (bNode.NodeType == ExpressionType.Or || bNode.NodeType == ExpressionType.OrElse))
            {
                return base.Visit(node);
            }
            else
            {
                if (_capture)
                    _ConditionPaths.Add(node);
                var c = _capture;
                _capture = false;
                try
                {
                    return base.Visit(node);
                }
                finally
                {
                    _capture = c;
                }
            }
        }
    }

    class AndSplitterVisitor : ConditionalExpressionVisitor
    {
        // Extract Mandatory Equality Conditions so the all expression is true
        public virtual IReadOnlyList<Expression> ExtractMandatoryEqualityConditions(Expression exp)
        {
            _MandatoryEqualityConditions = new List<Expression>();
            Debug.Assert(_capture);
            VisitAndConvert(exp, nameof(ExtractMandatoryEqualityConditions));
            return _MandatoryEqualityConditions;
        }
        protected List<Expression> _MandatoryEqualityConditions;
        protected bool _capture = true;
            
        public override Expression Visit(Expression node)
        {
            // We consider And & AndAlso the same here
            if (node is BinaryExpression bNode && bNode.Conversion == null &&
                (bNode.NodeType == ExpressionType.And || bNode.NodeType == ExpressionType.AndAlso))
            {
                return base.Visit(node);
            }
            else
            {
                if (_capture && // this is true if all parents nodes are binary "And[Also]" expression
                    node.NodeType == ExpressionType.Equal)
                    _MandatoryEqualityConditions.Add(node);
                // inhibit capture so nested conditions which are equality won't be captured
                // for example : /line.foo = 42 and (line.bar == 85 and line.toto == "hello" or something else)/
                // /line.bar == 85/ and /line.toto == "hello"/ won't be captured, only /line.foo = 42/ is sure in this case
                var c = _capture;
                _capture = false;
                try
                {
                    return base.Visit(node);
                }
                finally
                {
                    _capture = c;
                }
            }
        }
    }


    class SimplifyBooleanExpressionVisitor : ConditionalExpressionVisitor
    {
        public virtual Expression RemoveAndSimplify(Expression expression, Expression subExpressionToConsiderTrue)
        {
            var v = new ReplaceNodeVisitor(subExpressionToConsiderTrue, True);
            var replaced = v.Visit(expression);
            return Simplify(replaced);
        }
        public virtual Expression Simplify(Expression expression)
        {
            return Visit(expression);
        }
        public static readonly Expression True = Expression.Constant(true, typeof(bool));
        public static readonly Expression False = Expression.Constant(false, typeof(bool));

        public override Expression Visit(Expression node)
        {
            return base.Visit(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            var res = (BinaryExpression)base.VisitBinary(node);
            if (node.Conversion != null)
                throw new NotImplementedException();
            if (res.NodeType == ExpressionType.And ||
                res.NodeType == ExpressionType.AndAlso)
                if (res.Left == True)
                    return res.Right;
                else if (res.Right == True)
                    return res.Left;
                else
                    return res;
            else if (res.NodeType == ExpressionType.Or ||
                        res.NodeType == ExpressionType.OrElse)
                if (res.Left == True || res.Right == True)
                    return True;
                else
                    return res;
            else if (res.NodeType == ExpressionType.Equal)
                if (res.Left == res.Right)
                    return True;
                else if (res.Left is ConstantExpression cLeft && res.Right is ConstantExpression cRight)
                    return Equals(cLeft.Value, cRight) ? True : False;
                else
                    return res;
            else if (res.NodeType == ExpressionType.NotEqual)
                if (res.Left == res.Right)
                    return False;
                else if (res.Left is ConstantExpression cLeft && res.Right is ConstantExpression cRight)
                    return Equals(cLeft.Value, cRight) ? False : True;
                else
                    return res;
            else if (res.NodeType == ExpressionType.Coalesce)
                if (res.Left is ConstantExpression cLeft && !ReferenceEquals(cLeft, null))
                    return res.Left;
                else
                    return res;
            else if (res.NodeType == ExpressionType.LessThan || 
                        res.NodeType == ExpressionType.LessThanOrEqual ||
                        res.NodeType == ExpressionType.GreaterThan ||
                        res.NodeType == ExpressionType.GreaterThanOrEqual)
                if (res.Left is ConstantExpression cLeft && !ReferenceEquals(cLeft, null))
                    return res.Left;
                else
                    return res;
            throw new NotImplementedException();
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            var res = (UnaryExpression)base.VisitUnary(node);

            if (res.NodeType == ExpressionType.Not)
            {
                if (res.Operand == True)
                    return False;
                else if (res.Operand == False)
                    return True;
                else
                    return res;
            }
            else if (res.NodeType == ExpressionType.Convert)
            {
                return res;
            }
            throw new NotImplementedException(); // Possibily ok, but just to intercept and see...
        }
    }
}
