﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Tools;

using DataMapper;

using ApplicationBase.DAL;


namespace ApplicationBase.Business.MetaProgramming.DecisionTree
{
    // @Dev : it is recommended to install https://marketplace.visualstudio.com/items?itemName=vs-publisher-1232914.ReadableExpressionsVisualizers
    // So debug it much more easy...
    public partial class Builder
    {
        /// <summary>
        /// Build a decision tree which are valid only for the lifetime of program.
        /// </summary>
        /// <returns>A lambda of type : (object input) => (object output) where
        /// - the type of input is any type in grpRulesByEntryType.Keys
        /// - the output is null (no match), or a IHavingDynamicRule(one match) or a List of IHavingDynamicRule(multiple matches)
        /// You just have to call Compile() on it to have a callable lambda
        /// </returns>
        public Expression<Func<object, object>> BuildDecisionTreeFromRule<TRule>(IReadOnlyCollection<TRule> rules, IReadOnlyDictionary<TRule, int> ruleUniqueIds, bool useExactMatchForType = true)
            where TRule : IHavingDynamicRule
        {
            var abstractGrpRules = (IReadOnlyCollection<IHavingDynamicRule>)rules;
            IReadOnlyDictionary<IHavingDynamicRule, int> abstractRulesIds = ruleUniqueIds.ToDictionary(kvp => (IHavingDynamicRule)kvp.Key, kvp => kvp.Value);
            return BuildDecisionTreeFromRule(abstractGrpRules, abstractRulesIds, useExactMatchForType);
        }

        public virtual Expression<Func<object, object>> BuildDecisionTreeFromRule(IReadOnlyCollection<IHavingDynamicRule> rules, IReadOnlyDictionary<IHavingDynamicRule, int> ruleUniqueIds = null, bool useExactMatchForType = true)
        {
            var enabledRules = rules.Where(r => !r.Disabled);
            enabledRules.CompileAll();

            var grpRulesByEntryType = enabledRules.GroupByToDictionary(r => r.TypeOnWhichRuleApplies);

            // Check & data preparation
            if (ruleUniqueIds != null)
            {
                foreach (var kvp in grpRulesByEntryType)
                    foreach (var rule in kvp.Value)
                        if (!ruleUniqueIds.ContainsKey(rule))
                            throw new Exception("rule " + rule + " is not in " + nameof(ruleUniqueIds));
                if (ruleUniqueIds.Values.Distinct().Count() != ruleUniqueIds.Count())
                    throw new Exception("Id assigned to rule must be unique");
                RuleIds = ruleUniqueIds; 
                RuleByIds = ruleUniqueIds.ToDictionary(kvp => kvp.Value, kvp => kvp.Key);
            }
            else
            {
                RuleIds = null;
                RuleByIds = null;
            }

            ParameterExpression untypedInput = Expression.Parameter(typeof(object), "input");
            ParameterExpression untypedInputType = null;

            var blocksStatements = new List<Expression>();
            var matching_rules = Expression.Variable(typeof(object), "matching_rules");
            blocksStatements.Add(Expression.Assign(matching_rules, Expression.Constant(null)));
            var allVariables = new ParameterExpression[] { matching_rules };

            if (useExactMatchForType)
            {
                untypedInputType = Expression.Variable(typeof(Type), "inputType");
                blocksStatements.Add(Expression.Assign(untypedInputType, Expression.Call(untypedInput, typeof(object).GetMethod(nameof(GetType)))));
                allVariables = allVariables.Concat(untypedInputType).ToArray();
            }

            Expression cascadingIfThenElses = null; // consider it is of type ConditionalExpression
            foreach (var grp in grpRulesByEntryType.OrderBy(grp => grp.Value.Count)) // form tiny to big reverse because we build the if-then-else from last "else"
            {
                var varTypedLine = Expression.Variable(grp.Key, "typedInput");
                var grpBlockStatements = new List<Expression>();
                grpBlockStatements.Add(varTypedLine);
                grpBlockStatements.Add(Expression.Assign(varTypedLine, Expression.Convert(untypedInput, grp.Key)));

                var rulePaths = new List<RulePath>();

                foreach (var r in grp.Value)
                {
                    var c = IHavingDynamicRule_Extensions.GetChecker(r);

                    // Debug the original expression tree
                    // (just consider this for later https://github.com/albahari/ExpressionFormatter)
                    //var g = new ToGraphvizVisitor();
                    //g.Visit(c.CheckAsExpression);
                    //string dot = g.Result; // Paste this text here : https://edotor.net/

                    // Replace the rule' lambda parameter by our variable, so all rule expression tree depends on our variable and/or static references
                    // Static references are not a problem because this are things like enum which already exists at runtime so "considered" as constant.
                    // We can recycle any subpart of expression tree of this rule which make the following easier
                    var replacer = new ReplaceParameterVisitor(c.CheckAsExpression.Parameters.Single().Name, varTypedLine);
                    var reboundRuleExpression = (LambdaExpression)replacer.Visit(c.CheckAsExpression);
                    
                    // Just a way of clean up rule of user in case user wanted to merge different rule into one
                    // For example user wrote rule like "A and B or C and D or C", where he could have write 3 rule instead :
                    // - A and B
                    // - C and D
                    // - C
                    // This visitor split the original rule to this three rules.
                    // We call this a "path" because the condition A B C D will be used in the decision tree.
                    // Our work is now to build a tree based on all paths
                    var splitVisitor = new OrSplitterVisitor();
                    var paths = splitVisitor.ExtractPathOfConditions(reboundRuleExpression);
                    foreach (var path in paths)
                        rulePaths.Add(new RulePath()
                        {
                            Rule = r,
                            Path = path
                        });
                }

                var statements = BuildTreeRecursively(untypedInput, matching_rules, rulePaths);
                grpBlockStatements.AddRange(statements);

                var codeBlock = Expression.Block(typeof(void), new[] { varTypedLine }, grpBlockStatements.ToArray());
                var cond = useExactMatchForType 
                         ? Expression.Equal(untypedInputType, Expression.Constant(grp.Key))
                         : (Expression)Expression.TypeIs(untypedInput, grp.Key);
                if (cascadingIfThenElses == null)
                    cascadingIfThenElses = Expression.IfThen(cond, codeBlock);
                else if (useExactMatchForType)
                    cascadingIfThenElses = Expression.IfThenElse(cond, codeBlock, cascadingIfThenElses);
                else
                    cascadingIfThenElses = Expression.Block(Expression.IfThen(cond, codeBlock), cascadingIfThenElses);
            }
            if (cascadingIfThenElses != null)
                blocksStatements.Add(cascadingIfThenElses); // cascadingIfThenElses return in all paths

            LabelTarget returnTarget = Expression.Label(typeof(object));
            //GotoExpression returnExpression = Expression.Return(returnTarget, matching_rules, typeof(object));
            LabelExpression returnLabel = Expression.Label(returnTarget, matching_rules);
            //blocksStatements.Add(returnExpression);
            blocksStatements.Add(returnLabel);

            //LabelTarget returnTarget = Expression.Label(typeof(IHavingDynamicRule));
            // we just need to set the end of method
            Expression blockExpr = Expression.Block(allVariables, blocksStatements);
            var finalLambda = Expression.Lambda<Func<object, object>>(blockExpr, untypedInput);
            return finalLambda;
        }
        protected IReadOnlyDictionary<IHavingDynamicRule, int> RuleIds;
        protected IReadOnlyDictionary<int, IHavingDynamicRule> RuleByIds;

        [DebuggerDisplay("{" + nameof(Path) + ".ToString(),nq}")]
        protected class RulePath
        {
            public IHavingDynamicRule Rule;
            public Expression Path;
            public RulePath Parent;

            public bool IsTrueOrEmpty()
            {
                return Path == null
                    || Path is ConstantExpression cExpr && cExpr.Value is bool b && b;
            }
        }

        static readonly ConstantExpression Null = Expression.Constant(null);

        /// <summary>
        /// Build a list of statements that fill resultRules.
        /// </summary>
        protected virtual List<Expression> BuildTreeRecursively(ParameterExpression line, ParameterExpression resultRules, List<RulePath> rulePaths)
        {
            var statements = new List<Expression>();

            // We want to build a switch case here which can be interpreted as a step in decision tree we are trying to build
            // All the next steps will be built recursively.

            Dictionary<MemberExpression, PropertyAccessStats> stats = GetEqualityCriteriaStats(rulePaths);
            // Get the most used criteria in the form "line.Foo.Bar == a_constant_value_or_assimilated_as"
            var bestStats = stats.Where(kvp => kvp.Value.RulesUsingWeirdlyPropertyPath.Count == 0)
                                 .OrderByDescending(kvp => kvp.Value.TestedValuesForEqualities.Count)
                                 .FirstOrDefault()
                                 .Value;
            
            // If no criterias useable for optimization, we have to write the remaining part of paths manually, "as is"
            // Read the remaining code and the code in this if in last
            if (bestStats != null && bestStats.TestedValuesForEqualities.Count >= 2)
            {
                // First we built the tested expression in the switch
                Expression exprTested = bestStats.PropertyPath;
                if (typeof(IStaticEnum).IsAssignableFrom(exprTested.Type))
                {
                    Debug.Assert(typeof(IStaticEnum).GetProperty(nameof(IStaticEnum.Code)).PropertyType == typeof(string));
                    exprTested = Expression.Condition(Expression.Equal(exprTested, Null), Expression.Default(typeof(string)),  // just a manual liftting
                                    Expression.PropertyOrField(exprTested, nameof(IStaticEnum.Code)));
                }
                // this test in second position in case expression is DynamicEnum AND implement IStaticEnum (should not happens in theory though)
                else if (typeof(DynamicEnum).IsAssignableFrom(exprTested.Type))
                {
                    var m = typeof(DynamicEnum).GetMethod(nameof(DynamicEnum.GetEnumId));
                    Debug.Assert(m.ReturnType == typeof(long));
                    var result = Expression.Call(m, exprTested);
                    exprTested = Expression.Condition(Expression.Equal(exprTested, Null), Expression.Default(typeof(long?)), // just a manual liftting
                                                      Expression.Convert(result, typeof(long?)));
                }
                else if (!IsPrimitiveSwitchableType(exprTested.Type))
                    throw new NotImplementedException();

                // Then all the switch cases 
                // (the more there are cases the best the generated code is)
                // the first step of decision tree is the switch with the maximum of case and so on
                var cases = new List<SwitchCase>();
                foreach (var kvp in bestStats.TestedValuesForEqualities)
                {
                    var switchCaseTestedValue = kvp.Key;

                    if (IsConstant(switchCaseTestedValue)) // Should be already well typed (otherwise the original expression would be wrong)
                    {
                        // Nothing
                    }
                    else if (IsStaticEnum(switchCaseTestedValue))
                    {
                        // We compile and immediately access to the value.. so double "()"
                        // This is possible because this part of expression is done on a static readonly field (which can be considered as a constant here!)
                        // so no dependency to outer code
                        var se = Expression.Lambda<Func<IStaticEnum>>(switchCaseTestedValue).Compile()();
                        switchCaseTestedValue = Expression.Constant(se.Code);
                    }
                    else if (IsDynamicEnum(switchCaseTestedValue))
                    {
                        // We compile and immediately access to the value.. so double "()"
                        // This is possible because this part of expression is a static call with a constant in argument, 
                        // so no dependency to outer code
                        var de = Expression.Lambda<Func<DynamicEnum>>(switchCaseTestedValue).Compile()();
                        if (typeof(string) == exprTested.Type)
                        {
                            // ToString is supposed to call the real string EnumName... 
                            // not a really good assertion but we don't have other choice currently
                            switchCaseTestedValue = Expression.Constant(de.ToString());
                        }
                        else if (typeof(char) == exprTested.Type.RemoveNullability())
                        {
                            var v = de.ToString();
                            if (v.Length != 1)
                                throw new Exception("Probably invalid rule!");
                            switchCaseTestedValue = Expression.Constant(v[0]);
                        }
                        else if (exprTested.Type.IsIntegerTypeOrNullable())
                        {
                            // ToString is supposed to call the real string EnumName... 
                            // not a really good assertion but we don't have other choice currently
                            switchCaseTestedValue = Expression.Constant(DynamicEnum.GetEnumId(de));
                        }
                        else
                            throw new Exception("Probably invalid rule!");
                    }
                    else
                        throw new NotImplementedException();

                    // Recursively build code with simplified rulePaths (kvp.Key is replaced in rulepath by "true")
                    var simplifiedRulePaths = SimplifyRulePaths(rulePaths, bestStats, kvp.Key, kvp.Value.Keys);
                    var switchCaseBlockStatements = BuildTreeRecursively(line, resultRules, simplifiedRulePaths);
                    var switchCaseBlock = Expression.Block(typeof(void), switchCaseBlockStatements);

                    var switchCaseValue = exprTested.Type.RemoveNullability() != exprTested.Type
                                        ? Expression.Convert(switchCaseTestedValue, exprTested.Type)
                                        : switchCaseTestedValue;
                    var @case = Expression.SwitchCase(switchCaseBlock, switchCaseValue);
                    cases.Add(@case);
                }

                var @switch = Expression.Switch(exprTested, cases.ToArray());
                statements.Add(@switch);

                var rulesOutsideOfSwitch = rulePaths.Except(bestStats.TestedValuesForEqualities.SelectMany(kvp => kvp.Value.Keys)).ToList();
                var rulesOutsideOfSwitchStatements = BuildTreeRecursively(line, resultRules, rulesOutsideOfSwitch);
                if (rulesOutsideOfSwitchStatements.Count > 0)
                    statements.Add(Expression.Block(rulesOutsideOfSwitchStatements));
            }
            else  // terminal case : we don't know how to do more optimization
            {
                // Serialize remaining rule (we aggregate back paths of rule) to avoid duplicate in results
                var remainingRules = rulePaths.Where(rp => !rp.IsTrueOrEmpty())
                                              .GroupBy(rp => rp.Rule)
                                              .Select(grp => new
                                              {
                                                  Rule = grp.Key,
                                                  Expression = grp.Aggregate(null, (Expression full, RulePath path) =>
                                                                             full == null ? path.Path : Expression.MakeBinary(ExpressionType.Or, full, path.Path))
                                              })
                                              .ToList();
                foreach (var ruleUniquePath in remainingRules)
                {
                    var addResult = CreateMergeRuleInResultStatement(resultRules, ruleUniquePath.Rule);
                    statements.Add(Expression.IfThen(ruleUniquePath.Expression, addResult));
                }

                //Type delegateType = typeof(Func<,>).MakeGenericType(new[] { line.Type, typeof(bool) });
                //MethodInfo mCreateLambda = gCreateLambda.MakeGenericMethod(new[] { delegateType });

                //var decisionTree = (LambdaExpression)mCreateLambda.Invoke(null, new object[] { Expression.Constant(null), new[] { line } });
                //return decisionTree;
                // we have to return "res"
            }

            // Handle empty rules (should not exist in first loop but who knows...)
            // We always do it in last thus more specific rule (which test more things) will be in first position in result list
            // This is better for user to handle ambiguity
            foreach (var grp in rulePaths.Where(rp => rp.IsTrueOrEmpty())
                                         .GroupBy(r => r.Rule)) // Just in case multiple paths exist for same rule (or duplicate rules)
            {
                statements.Add(CreateMergeRuleInResultStatement(resultRules, grp.Key));
            }

            return statements;
        }

        protected Expression CreateMergeRuleInResultStatement(ParameterExpression ruleResultsVar, IHavingDynamicRule matchedRule)
        {
            var ruleValue = RuleByIds == null
                       ? (Expression)Expression.Constant(matchedRule)
                       // The advantage is that we see Id in generated code
                       : Expression.Property(Expression.Constant(RuleByIds), "Item", Expression.Constant(RuleIds[matchedRule]));
            var newResult = Expression.Call(null, mMergeRuleInResult, ruleResultsVar, ruleValue);
            var statement = Expression.Assign(ruleResultsVar, newResult);
            return statement;
        }
        static readonly MethodInfo mMergeRuleInResult = typeof(Builder).GetMethod(nameof(MergeRuleInResult), BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
        // Merge rule that match.
        // In the ideal world, the list is never needed. because they are no ambiguity
        // so this code is not type to generated code allocation free in 99% of cases
        public static object MergeRuleInResult(object ruleResults, IHavingDynamicRule matchedRule)
        {
            // 99% of cases
            if (ruleResults == null)
                return matchedRule;
            // 0.99% of cases
            List<IHavingDynamicRule> lst;
            if (ruleResults is IHavingDynamicRule existingResult)
            {
                lst = new List<IHavingDynamicRule>(3);
                lst.Add(existingResult);
            }
            else // 0.01% of cases
                lst = (List<IHavingDynamicRule>)ruleResults;
            lst.Add(matchedRule);
            return lst;
        }


        protected static bool IsPrimitiveSwitchableType(Type type)
        {
            return type == typeof(string)
                || type.RemoveNullability() == typeof(bool)
                || type.RemoveNullability() == typeof(char)
                || type.IsIntegerTypeOrNullable();
        }

        protected static bool IsNullAcceptableFor(Type type)
        {
            return type.IsClass
                || type.IsInterface
                || type.RemoveNullability() != type; // This only work for nullable struct actually, not reference type
        }

        #region Get Property Access Stat

        protected class PropertyAccessStats
        {
            // The expression on which we do stats.
            // this value is used as a key in a dictionary
            public MemberExpression PropertyPath;

            // Key = The expression tested for equality 
            // Value: The path and condition in path (multiple condition can exist ofr one path, especially if user is silly)
            //  Exemple line.Foo == "A" and line.Foo == "B" or dummy_test : dumb but possible
            public Dictionary<Expression, Dictionary<RulePath, Expression>> TestedValuesForEqualities = new Dictionary<Expression, Dictionary<RulePath, Expression>>(ExpressionEqualityComparer.Instance);

            public void AddEquality(Expression valueTested, RulePath path, Expression cond)
            {
                if (!TestedValuesForEqualities.TryGetValue(valueTested, out Dictionary<RulePath, Expression> occurences))
                {
                    occurences = new Dictionary<RulePath, Expression>(ReferenceEqualityComparer<RulePath>.Default);
                    TestedValuesForEqualities.Add(valueTested, occurences);
                }
                if (occurences.TryGetValue(path, out Expression existingCond))
                {
                    IEqualityComparer<Expression> comparer = ExpressionEqualityComparer.Instance;
                    if (!comparer.Equals(existingCond, cond))
                        throw new Exception("Unknown case!");
                }
                else
                    occurences.Add(path, cond);
            }

            // Rules using the property in an unknown check case which could conflict with decision tree building
            public List<RulePath> RulesUsingWeirdlyPropertyPath = new List<RulePath>();

            public static Dictionary<MemberExpression, PropertyAccessStats>  CreateDictionary() { return new Dictionary<MemberExpression, PropertyAccessStats>(ExpressionEqualityComparer.Instance); }

            // Helper so Expression can be use as a key in dictionary in an intuitive way
            public sealed class ExpressionEqualityComparer : System.Collections.IEqualityComparer, IEqualityComparer<Expression>
            {
                public static readonly ExpressionEqualityComparer Instance = new ExpressionEqualityComparer();

                private ExpressionEqualityComparer() { }

                int System.Collections.IEqualityComparer.GetHashCode(object obj)
                {
                    return obj is Expression exp
                        ? exp.ToString().GetHashCode()
                        : 0;
                }
                public int GetHashCode(Expression obj)
                {
                    return obj.ToString().GetHashCode();
                }
                bool System.Collections.IEqualityComparer.Equals(object x, object y)
                {
                    return x is Expression xe
                        && y is Expression ye
                        && x.ToString() == y.ToString();
                }
                bool IEqualityComparer<Expression>.Equals(Expression x, Expression y)
                {
                    return x.ToString() == y.ToString();
                }
            }
        }

        // Return for each MemberAccess on ParameterExpression (whch is the object test by the rule)
        // all the immediate parent which is a boolean test.
        // Example if the rules are : 
        // - line.Foo == MT940Code.CHK and line.Bar == "hello" and OtherCondition
        // - line.Foo == MT940Code.CHG and line.Bar == "world" and AnotherConditionAgain
        // - line.Foo.Label == "weird_cond" and line.Bar == "world" and AnotherConditionAgain
        // It return dictionary with expression "line.Foo", and as a value a dictionary with two expressions as key : MT940Code.CHK and MT940Code.CHG
        // And also  expression "line.Foo", with a dictonary with keys : "hello", and "world"
        protected static Dictionary<MemberExpression, PropertyAccessStats> GetEqualityCriteriaStats(List<RulePath> rulePaths)
        {
            // Count the number of time some path are tested with some value in equality comparison
            var propPathTestedAndDistinctValues = PropertyAccessStats.CreateDictionary();
            foreach (var rulePath in rulePaths)
            {
                // Debug the original expression tree
                // (just consider this for later https://github.com/albahari/ExpressionFormatter)
                //var g = new ToGraphvizVisitor();
                //g.Visit(c.CheckAsExpression);
                //var dot = g.Result; // Paste this text here : https://edotor.net/

                // The conditions in path are expected to be very often in the form "Cond1 and Cond2 and Cond3A or (Cond3B and Cond4)"
                // and each Cond is expected to be often equality comparison too
                // We extract condition expressions separated by And or AndAlso (on the first level only)
                // In the example we have only 3 expressions : 
                // - Cond1
                // - Cond2
                // - Cond3A or (Cond3B and Cond4)
                var condVisitor = new AndSplitterVisitor();
                var conditions = condVisitor.ExtractMandatoryEqualityConditions(rulePath.Path);

                // We expect these subexpression to be often simple equality comparison.
                // So we do stats on these ones to build an optimized decision tree.

                // (for example condition "Account.Bank == Bank.BNP" will add 1 for path "Account.Bank")
                foreach (var cond in conditions)
                    if (cond is BinaryExpression bCond && bCond.NodeType == ExpressionType.Equal)
                    {
                        // Because user can write line.Something == "foo" or "foo" == line.Something
                        // We need to try both ways
                        // We consider that a == b is executed the same way as b == a,
                        // which is not always true, but I consider this case as a bad behavior design code in the first place
                        //Expression<Func<EnrichedCfonbOperation, bool>> f = o => o.SwiftCode == MT940Code.BNK;
                        //bCond = f.Body as BinaryExpression;
                        foreach (var sides in new[] { new { asLeft = bCond.Left,  asRight = bCond.Right },
                                                        new { asLeft = bCond.Right, asRight = bCond.Left }})
                        {
                            var propPath = GetPropertyPath(sides.asLeft);
                            if (sides.asLeft is MemberExpression pCond &&
                                pCond.Member is PropertyInfo pProp)
                            {
                                //var hashCode = left.GetHashCode();
                                //var hashCodeStr = left.ToString();
                                if (!propPathTestedAndDistinctValues.TryGetValue(pCond, out PropertyAccessStats stats))
                                {
                                    stats = new PropertyAccessStats();
                                    stats.PropertyPath = pCond;
                                    propPathTestedAndDistinctValues.Add(stats.PropertyPath, stats);
                                }

                                if (IsConstant(sides.asRight) ||
                                    IsDynamicEnum(sides.asRight) || // We consider all these expressions as "constant" 
                                    IsStaticEnum(sides.asRight)) // because even if the are runtime processed, they are readonly
                                {
                                    stats.AddEquality(sides.asRight, rulePath, bCond);
                                    break;
                                }
                                else // For property used in a unknown pattern (for example if user test equality of property path.... with another property path of same object !)
                                    stats.RulesUsingWeirdlyPropertyPath.Add(rulePath); 
                            }
                        }
                    }
            }
            return propPathTestedAndDistinctValues;
        }

        protected static bool IsConstant(Expression exp)
        {
            return exp is ConstantExpression;
        }

        protected static bool IsDynamicEnum(Expression exp)
        {
            return exp is MethodCallExpression callExp
                && typeof(DynamicEnum).IsAssignableFrom(callExp.Method.DeclaringType) 
                && callExp.Method.Name == nameof(DynamicEnum.GetValueFor);
        }

        protected static bool IsStaticEnum(Expression exp)
        {
            return exp is MemberExpression fExp 
                && (fExp.Member is FieldInfo || fExp.Member is PropertyInfo) 
                && fExp.Expression == null // static call on property or field
                && typeof(IStaticEnum).IsAssignableFrom(fExp.Type);
        }

        protected static string GetPropertyPath(Expression expr)
        {
            if (expr is MemberExpression pCond && pCond.Member is PropertyInfo pProp)
            {
                if (pCond.Expression is ParameterExpression paramExpr)
                    return pProp.Name;
                var parent = GetPropertyPath(pCond.Expression);
                if (parent == null)
                    return null;
                return parent + "." + pProp.Name;
            }
            return null;
        }

        #endregion Get Stat

        protected virtual List<RulePath> SimplifyRulePaths(List<RulePath> allRulePaths, PropertyAccessStats bestStats, Expression testValueAsTrue, IReadOnlyCollection<RulePath> potentialMatchingRulePaths)
        {
            var simplifier = new SimplifyBooleanExpressionVisitor();
            //var ruleWithConditionsToRemove = bestStats.TestedValuesForEqualities;
            var simplifiedPaths = potentialMatchingRulePaths
                                           .Select(rp => new RulePath()
                                           {
                                               Rule = rp.Rule,
                                               Path = simplifier.RemoveAndSimplify(rp.Path, bestStats.TestedValuesForEqualities[testValueAsTrue][rp]),
                                               Parent = rp
                                           })
                                           .ToList();
            //var notConcernedRulePaths = allRulePaths.Except(bestStats.TestedValuesForEqualities.SelectMany(kvp => kvp.Value.Keys)).ToList();
            //return simplifiedPaths.Concat(notConcernedRulePaths).ToList();
            return simplifiedPaths;
        }
    }
}
