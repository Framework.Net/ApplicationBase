﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.MetaProgramming.ExpressionVisitors;

using ApplicationBase.Business.GraphVisualization.Graphviz;


namespace ApplicationBase.Business.MetaProgramming.DecisionTree
{
    public partial class Builder
    {
        protected class GraphvizExporterVisitor : ExpressionConverterVisitor<DecisionTreeGraphNode>
        {
            public DecisionTreeGraph Convert(Expression<Func<object, object>> lambda, bool useExactMatchForType,
                                             IReadOnlyDictionary<DAL.IHavingDynamicRule, int> prodRules, Func<DAL.IHavingDynamicRule, string> getRuleResult,
                                             eLayoutEngine engine)
            {
                if (!useExactMatchForType)
                    throw new TechnicalException("Not yet handled!", null);

                _ruleByIds = prodRules.ToDictionary(kvp => kvp.Value, kvp => kvp.Key);
                _getRuleResult = getRuleResult;
                _graph = new DecisionTreeGraph(_ruleByIds);
                _graph.LayoutEngine = engine;
                _graph.GraphVizGraphName = "decisionTree";
                _graph.IsDirectedGraph = true;
                _graph.Splines = eSplines.Line;
                _graph.NodeDefaultProperties.FontName = "Courier new";
                _graph.NodeDefaultProperties.Shape = eNodeShape.Box;
                if (_graph.LayoutEngine == eLayoutEngine.Dot)
                    _graph.Overlap = eOverlap.False; // not supported by Neato (make it crash)
                _graph.RankDir = eRankDir.LeftToRight;
                _graph.Ratio = "auto";

                var mainBlock = lambda.Body as BlockExpression;
                if (useExactMatchForType)
                    _inputType = (mainBlock.Expressions[1] as BinaryExpression).Left as ParameterExpression;

                var rootNode = Visit(mainBlock);
                if (rootNode != null)
                    _graph.Roots.Add(rootNode);

                if (_graph.AllRuleMergeNodes.Any() && _graph.LayoutEngine == eLayoutEngine.Dot)
                    _graph.OtherStatements.Add("{ rank = same; " + _graph.AllRuleMergeNodes.Select(n => n.GraphVizNodeName).Join("; ") + "}");

                return _graph;
            }
            DecisionTreeGraph _graph;
            IReadOnlyDictionary<int, DAL.IHavingDynamicRule> _ruleByIds;
            Func<DAL.IHavingDynamicRule, string> _getRuleResult;
            ParameterExpression _inputType;


            public override DecisionTreeGraphNode Visit(Expression node)
            {
                return base.Visit(node);
            }

            protected override DecisionTreeGraphNode VisitBlock(BlockExpression block)
            {
                var nodes = Visit(block.Expressions).NotNull().ToArray();
                var current = nodes.FirstOrDefault();
                foreach (var n in nodes.Skip(1))
                {
                    var e = _graph.NewEdge(current, n);
                    e.Color = Color.Red;
                    // we do not set e.HeadPortCompass & e.TailPortCompass here 
                    // because we do not know which orientation layout engine will choose
                    // It should vertical anyway
                    current.Next = n;
                    current = n;
                }
                if (nodes.Length > 1 && _graph.LayoutEngine == eLayoutEngine.Dot)
                    _graph.OtherStatements.Add("{ rank = same; " + nodes.Select(n => n.GraphVizNodeName).Join("; ") + "}");
                return nodes.FirstOrDefault();
            }
            protected override DecisionTreeGraphNode VisitConditional(ConditionalExpression @if)
            {
                // Check for the beginning of code in lambda
                // when tree is built using useExactMatchForType = true
                if ((@if.Test as BinaryExpression)?.Left == _inputType &&
                    (@if.Test as BinaryExpression)?.NodeType == ExpressionType.Equal &&
                    typeof(Type).IsAssignableFrom((@if.Test as BinaryExpression).Right.Type))
                {
                    // We make edge and node like a switch because this is exactly what it is: 
                    // A switch on evaluated item's type
                    var nodeSwitch = _graph.NewNode();
                    nodeSwitch.Label = "Which Type ?";

                    var currentIf = @if;
                    while (currentIf != null)
                    {
                        var type = (Type)((currentIf.Test as BinaryExpression).Right as ConstantExpression).Value;
                        var label = TechnicalTools.Annotations.BusinessNameAttribute.GetNameFor(type) ?? type.Name.Uncamelify();
                        var ifTrue = Visit(@if.IfTrue);
                        Debug.Assert(ifTrue != null); // decision tree does not contains useless code!
                        var e = _graph.NewEdge(nodeSwitch, ifTrue);
                        nodeSwitch.Children.Add(ifTrue);
                        e.HeadLabel = label;
                        e.HeadPortCompass = eCompassPort.W;
                        e.TailPortCompass = eCompassPort.E;
                        if (currentIf.IfFalse is DefaultExpression de)
                        {
                            Debug.Assert(de.Type == typeof(void));
                            break;
                        }
                        currentIf = (ConditionalExpression)currentIf.IfFalse;
                    }
                    return nodeSwitch;
                }
                else
                {
                    // Check we do not forgot something
                    if ((@if.IfFalse as DefaultExpression)?.Type != typeof(void))
                        throw new TechnicalException("Unhandled case!", null);
                    var node = _graph.NewNode();
                    node.Label = new ConditionalExpressionBeautifierVisitor().ConvertIfCondition(@if.Test);
                    var subNode = Visit(@if.IfTrue);
                    var e = _graph.NewEdge(node, subNode);
                    node.Children.Add(subNode);
                    e.HeadLabel = "true";
                    e.HeadPortCompass = eCompassPort.W;
                    e.TailPortCompass = eCompassPort.E;
                    return node;
                }
            }
            protected override DecisionTreeGraphNode VisitSwitch(SwitchExpression @switch)
            {
                var node = _graph.NewNode();
                node.Label = new ConditionalExpressionBeautifierVisitor().ConvertSwitchValue(@switch.SwitchValue);
                foreach (var @case in @switch.Cases)
                {
                    var caseNode = Visit(@case.Body);
                    var e = _graph.NewEdge(node, caseNode);
                    node.Children.Add(caseNode);
                    e.HeadLabel = @case.TestValues.Select(v => new ConditionalExpressionBeautifierVisitor().ConvertSwitchCaseValue(v)).Join(Environment.NewLine);
                    e.HeadPortCompass = eCompassPort.W;
                    e.TailPortCompass = eCompassPort.E;
                }
                if (@switch.DefaultBody != null)
                {
                    var caseNode = Visit(@switch.DefaultBody);
                    var e = _graph.NewEdge(node, caseNode);
                    node.Children.Add(caseNode);
                    e.HeadLabel = "<otherwise>";
                    e.HeadPortCompass = eCompassPort.W;
                    e.TailPortCompass = eCompassPort.E;
                }
                return node;
            }
            protected override DecisionTreeGraphNode VisitDefault(DefaultExpression node)
            {
                if (node.NodeType != ExpressionType.Default)
                    throw new TechnicalException("Unknown case!", null);
                return base.VisitDefault(node);
            }
            protected override DecisionTreeGraphNode VisitBinary(BinaryExpression mergeResult)
            {
                if (mergeResult.NodeType == ExpressionType.Assign &&
                    mergeResult.Left is ParameterExpression pe &&
                    pe.Name == "inputType")
                    return null;
                if (mergeResult.NodeType == ExpressionType.Assign &&
                    (pe = mergeResult.Left as ParameterExpression) != null &&
                    pe.Name == "typedInput")
                    return null;
                if (mergeResult.NodeType == ExpressionType.Assign &&
                    (pe = mergeResult.Left as ParameterExpression) != null &&
                    pe.Name == "matching_rules")
                {
                    var callMerge = mergeResult.Right as MethodCallExpression;
                    if (callMerge == null) // First line of lambda
                        return null;
                    var argIndex = (callMerge.Arguments[1] as IndexExpression).Arguments[0] as ConstantExpression;
                    var indexValue = (int)argIndex.Value;

                    var matchNode = _graph.GetNodeForRuleMatch(mergeResult, indexValue);
                    matchNode.Label = indexValue + ": " + _getRuleResult(_ruleByIds[indexValue]);
                    matchNode.Color = Color.Lime;
                    return matchNode;
                }
                throw new TechnicalException("Unknown case!", null);
            }


            protected override DecisionTreeGraphNode VisitConstant(ConstantExpression node)
            {
                return base.VisitConstant(node);
            }

            protected override DecisionTreeGraphNode VisitLambda<TFunc>(Expression<TFunc> node)
            {
                return base.VisitLambda(node);
            }

            protected override DecisionTreeGraphNode VisitMember(MemberExpression node)
            {
                return base.VisitMember(node);
            }

            protected override DecisionTreeGraphNode VisitMethodCall(MethodCallExpression node)
            {
                return base.VisitMethodCall(node);
            }

            protected override DecisionTreeGraphNode VisitNewArray(NewArrayExpression node)
            {
                return base.VisitNewArray(node);
            }
            protected override DecisionTreeGraphNode VisitParameter(ParameterExpression node)
            {
                return base.VisitParameter(node);
            }

            protected override DecisionTreeGraphNode VisitUnary(UnaryExpression node)
            {
                return base.VisitUnary(node);
            }

            protected override DecisionTreeGraphNode VisitNew(NewExpression node)
            {
                // for value like "new DateTime(...)"
                return base.VisitNew(node);
            }



            protected override DecisionTreeGraphNode VisitCatchBlock(CatchBlock node)
            {
                throw new Exception();
                //return base.VisitCatchBlock(node);
            }
            protected override DecisionTreeGraphNode VisitElementInit(ElementInit node)
            {
                throw new Exception();
                //return base.VisitElementInit(node);
            }
            protected override DecisionTreeGraphNode VisitLabelTarget(LabelTarget node)
            {
                throw new Exception();
                //return base.VisitLabelTarget(node);
            }
            protected override DecisionTreeGraphNode VisitMemberAssignment(MemberAssignment node)
            {
                throw new Exception();
                //return base.VisitMemberAssignment(node);
            }
            protected override DecisionTreeGraphNode VisitMemberBinding(MemberBinding node)
            {
                throw new Exception();
                //return base.VisitMemberBinding(node);
            }
            protected override DecisionTreeGraphNode VisitMemberListBinding(MemberListBinding node)
            {
                throw new Exception();
                //return base.VisitMemberListBinding(node);
            }
            protected override DecisionTreeGraphNode VisitMemberMemberBinding(MemberMemberBinding node)
            {
                throw new Exception();
                //return base.VisitMemberMemberBinding(node);
            }
            protected override DecisionTreeGraphNode VisitSwitchCase(SwitchCase node)
            {
                throw new Exception();
                //return base.VisitSwitchCase(node);
            }
            protected override DecisionTreeGraphNode VisitDebugInfo(DebugInfoExpression node)
            {
                throw new Exception();
                //return base.VisitDebugInfo(node);
            }
            protected override DecisionTreeGraphNode VisitDynamic(DynamicExpression node)
            {
                throw new Exception();
                //return base.VisitDynamic(node);
            }
            protected override DecisionTreeGraphNode VisitGoto(GotoExpression node)
            {
                throw new Exception();
                //return base.VisitGoto(node);
            }
            protected override DecisionTreeGraphNode VisitIndex(IndexExpression node)
            {
                throw new Exception();
                //return base.VisitIndex(node);
            }
            protected override DecisionTreeGraphNode VisitInvocation(InvocationExpression node)
            {
                throw new Exception();
                //return base.VisitInvocation(node);
            }
            protected override DecisionTreeGraphNode VisitLabel(LabelExpression node)
            {
                return null;
            }
            protected override DecisionTreeGraphNode VisitListInit(ListInitExpression node)
            {
                throw new Exception();
                //return base.VisitListInit(node);
            }
            protected override DecisionTreeGraphNode VisitLoop(LoopExpression node)
            {
                throw new Exception();
                //return base.VisitLoop(node);
            }
            protected override DecisionTreeGraphNode VisitMemberInit(MemberInitExpression node)
            {
                throw new Exception();
                //return base.VisitMemberInit(node);
            }
            protected override DecisionTreeGraphNode VisitRuntimeVariables(RuntimeVariablesExpression node)
            {
                throw new Exception();
                //return base.VisitRuntimeVariables(node);
            }
            protected override DecisionTreeGraphNode VisitTry(TryExpression node)
            {
                throw new Exception();
                //return base.VisitTry(node);
            }
            protected override DecisionTreeGraphNode VisitTypeBinary(TypeBinaryExpression node)
            {
                throw new Exception();
                //return base.VisitTypeBinary(node);
            }
        }
    }
}
