﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;

using TechnicalTools;

using ApplicationBase.Business.GraphVisualization.Graphviz;


namespace ApplicationBase.Business.MetaProgramming.DecisionTree
{
    public partial class Builder
    {
        public Graph ConvertDecisionTreeToGraphviz(Expression<Func<object, object>> lambda, bool useExactMatchForType, IReadOnlyDictionary<DAL.IHavingDynamicRule, int> prodRules, Func<DAL.IHavingDynamicRule, string> getRuleResult, 
                                                                               eLayoutEngine engine = eLayoutEngine.Neato)
        {
            if (engine.NotIn(eLayoutEngine.Neato, eLayoutEngine.Dot))
                throw new ArgumentException(nameof(engine));
            var exporter = CreateExportToGraphvizVisitor();
            var graphvizGraph = exporter.Convert(lambda, useExactMatchForType, prodRules, getRuleResult, engine);
            // Not mandatory here but the result is far better than the tool
            if (graphvizGraph.LayoutEngine == eLayoutEngine.Neato)
                ProcessNodePositionsSmartly(graphvizGraph);
            return graphvizGraph;
        }

        protected virtual GraphvizExporterVisitor CreateExportToGraphvizVisitor()
        {
            return new GraphvizExporterVisitor();
        }

        #region Adding Smart positioning of node to substitue the default placement

        // Not mandatory method but VERY useful actually
        void ProcessNodePositionsSmartly(DecisionTreeGraph tree)
        {
            _maxX = 0;
            tree.NodeDefaultProperties.MinWidth = 0.1m;
            tree.NodeDefaultProperties.MinHeight = 0.1m;
            // First step is usefull to calculate _maxX
            // Second step build again with _maxX well initialized
            for (int i = 0; i < 2; ++i)
            {
                decimal yPos = 0;
                foreach (var root in tree.Roots)
                    yPos = ProcessNodePositionsSmartlyRec(root, 0, yPos);
            }
        }
        float _maxX;
        // yPos is expected to be the position the bottom edge the rendring of node (and it subtree)
        // if node has no children this is the bottom edge of "node", if node has children .. this is more complex
        decimal ProcessNodePositionsSmartlyRec(DecisionTreeGraphNode node, decimal xPosCenter, decimal yPosBottomEdge)
        {
            const decimal epsilon = 0.0000001m;
            const decimal nextArrowMinHeight = 0.3m;
            const decimal nodeVerticalDistance = 0.1m;
            const decimal nodeHorizontalDistance = 6;


            var splitParts = (node.Label ?? "").Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            var nodeWidthInChar = Math.Max(1, splitParts.Select(line => line.Length).DefaultIfEmpty(0).Max())
                                + 1; // add for missing space to join bounding box' borders
            var nodeWidth = nodeWidthInChar * 0.002m; // 0.002 is an empirical value

            // These values have been empirically found value when 
            // - Node.fontname ="Courier New" 
            // - Graph.MinWidth=0.1
            // - Graph.MinHeight=0.1 
            var nodeLineCount = Math.Max(1, splitParts.Length);
            var nodeHeight = nodeLineCount * 0.207m + 0.12m;

            decimal yPos = yPosBottomEdge;
            foreach (var child in node.Children)
            {
                yPos = ProcessNodePositionsSmartlyRec(child, xPosCenter + nodeHorizontalDistance + node.Children.Count / 5, yPos);
                yPos += nodeVerticalDistance;
            }
            if (node.Children.Count > 0)
                yPos -= nodeVerticalDistance;
            var subTreeHeight = yPos - yPosBottomEdge;



            decimal nodeAndSubtreeHeighDifference = nodeHeight - subTreeHeight;
            if (nodeAndSubtreeHeighDifference > epsilon)
            { // if current node is bigger that height of subtree !
                if (node.Children.Count > 0)
                {
                    // Rebuild the subtree to set location higher than before
                    yPos = yPosBottomEdge  // reset
                         + nodeAndSubtreeHeighDifference / 2; // but higher
                    foreach (var child in node.Children)
                    {
                        yPos = ProcessNodePositionsSmartlyRec(child, xPosCenter + nodeHorizontalDistance + node.Children.Count / 5, yPos);
                        yPos += nodeVerticalDistance;
                    }
                    yPos -= nodeVerticalDistance;
                }
                // Set position accordingly to method description : bottom edge of node will be at yPosBottomEdge
                node.Pos = new PointF((float)xPosCenter, (float)(yPosBottomEdge + nodeHeight / 2));

                // Update the futur yPosBottomEdge for the next sibling tree 
                yPos = yPosBottomEdge + nodeHeight;
            }
            else // subTree is bigger, we just set the pos position to the middle of subtree
                node.Pos = new PointF((float)xPosCenter, (float)(yPosBottomEdge + subTreeHeight / 2));

            if (node.Next != null)
            {
                var minHeightForArrow = -Math.Min(0, nodeAndSubtreeHeighDifference / 2) < nextArrowMinHeight
                                      ? nextArrowMinHeight + Math.Min(0, nodeAndSubtreeHeighDifference / 2)
                                      : 0;
                yPos += Math.Max(node.Children.Count > 0 ? nodeVerticalDistance : 0, minHeightForArrow);
                yPos = ProcessNodePositionsSmartlyRec(node.Next, xPosCenter, yPos);
            }

            _maxX = Math.Max(_maxX, node.Pos.Value.X);
            if (node.Children.Count == 0) // Make leaf nodes at the same position
                node.Pos = new PointF(_maxX, node.Pos.Value.Y);

            return yPos;
        }
        #endregion 
    }
    public partial class Builder
    {
        protected class DecisionTreeGraphNode : Node
        {
            public readonly List<DecisionTreeGraphNode> Children = new List<DecisionTreeGraphNode>();
            public DecisionTreeGraphNode Next;

            public DecisionTreeGraphNode(BaseGraph parent)
                : base(parent)
            { }
        }
        protected class DecisionTreeGraph : Graph
        {
            public List<DecisionTreeGraphNode> Roots { get; } = new List<DecisionTreeGraphNode>();

            public DecisionTreeGraph(IReadOnlyDictionary<int, DAL.IHavingDynamicRule> ruleByIds)
            {
                _ruleByIds = ruleByIds;
            }
            readonly IReadOnlyDictionary<int, DAL.IHavingDynamicRule> _ruleByIds;

            public new DecisionTreeGraphNode NewNode() { return (DecisionTreeGraphNode)base.NewNode(); }
            protected override Node CreateNode(BaseGraph parent)
            {
                return new DecisionTreeGraphNode(parent);
            }
            public DecisionTreeGraphNode GetNodeForRuleMatch(Expression exp, int ruleId)
            {
                var n = NewNode();
                _ruleMathNodes.Add(n);
                return n;
            }
            public IReadOnlyList<DecisionTreeGraphNode> AllRuleMergeNodes { get { return _ruleMathNodes; } }

            List<DecisionTreeGraphNode> _ruleMathNodes = new List<DecisionTreeGraphNode>();
        }
    }

}
