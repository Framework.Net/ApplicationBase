﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using DataMapper;

using ApplicationBase.DAL;


namespace ApplicationBase.Business.MetaProgramming.DecisionTree
{
    partial class Builder
    {
        partial class Tests
        {
            public static void Run()
            {
                var cultureCulture = Thread.CurrentThread.CurrentCulture;
                var cultureUICulture = Thread.CurrentThread.CurrentUICulture;
                // To get error message in english
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
                try
                {
                    RuntimeHelpers.RunClassConstructor(typeof(FooDynamicEnum).TypeHandle);

                    var lineParameterDecl = "(" + typeof(InputObject).ToSmartString(true) + " line) => ";
                    var rules = new[]
                    {
                        new FooRule() { Id = 1, TypeOnWhichRuleApplies = typeof(InputObject),
                                        DynamicMatchingRule = lineParameterDecl + "line.Number == 42 && line.Number == 51",  }, // non-sense condition but still writeable by user
                        new FooRule() { Id = 2, TypeOnWhichRuleApplies = typeof(InputObject),
                                        DynamicMatchingRule = lineParameterDecl + "line.Number == 51 && line.Number == 42" // non-sense condition too (in reverse to test robustness of algorithm)
                                                                                + " && line.EnumDynamic == " + typeof(FooDynamicEnum).ToSmartString(true) + ".DynamicEnumValue1" },
                        new FooRule() { Id = 3, TypeOnWhichRuleApplies = typeof(InputObject),
                                        DynamicMatchingRule = lineParameterDecl + "line.Number == 52 && line.Text.Contains(\"hello world\")" }
                    };
                    // to test 
                    //Func<InputObject, bool> f = line => line.Number == 42 && line.EnumDynamic == FooDynamicEnum.DynamicEnumValue1;
                    var lambda = new Builder().BuildDecisionTreeFromRule(rules, rules.ToDictionary(r => r, r => r.Id));
                    var func = lambda.Compile();
                    var tt = func(new InputObject() { Number = 42, EnumDynamic = FooDynamicEnum.DynamicEnumValue1 });
                }
                finally
                {
                    // To get error message in english
                    Thread.CurrentThread.CurrentCulture = cultureCulture;
                    Thread.CurrentThread.CurrentUICulture = cultureUICulture;
                }
            }

            public class InputObject
            {
                public string Text { get; set; }
                public int Number { get; set; }
                public DateTime Date { get; set; }
                public eClassicEnum EnumClassic { get; set; }
                public FooStaticEnum EnumStatic { get; set; }
                public FooDynamicEnum EnumDynamic { get; set; }
            }

            public class FooRule : IHavingDynamicRule
            {
                public int Id { get; set; }
                public bool Disabled { get; set; }
                public Type TypeOnWhichRuleApplies { get; set; }
                public string DynamicMatchingRule { get; set; }
                public string DynamicMatchingRuleView { get; set; }
                bool IHavingDynamicRule.DefaultResult { get { return false; } }

                // not implemented so when FooRule are created and initialized, the rule must not change
                event Action IHavingDynamicRule.DynamicMatchingRuleChanged { add { } remove { } }

                IIdTuple IHasClosedIdReadable.Id => new IdTuple<int>(Id);

                #region not needed for test

                ITypedId IHasTypedIdReadable.TypedId => throw new NotImplementedException();





                void ICopyable<IAutoMappedDbObject>.CopyFrom(IAutoMappedDbObject source)
                {
                    throw new NotImplementedException();
                }

                void ICopyable.CopyFrom(ICopyable source)
                {
                    throw new NotImplementedException();
                }

                ITypedId IHasTypedIdReadable.MakeTypedId(IIdTuple id)
                {
                    throw new NotImplementedException();
                }
                #endregion not needed for test
            }

            public enum eClassicEnum
            {
                eUnknown = 0,
                eValue1 = 1,
                eValue2 = 2,
                eValue3 = 3,
                eValue4 = 4,
                eValue5 = 5,
            }
            [DbNotMappedEnum]
            public class FooDynamicEnum : DynamicEnum<byte, FooDynamicEnum>
            {
                static FooDynamicEnum() { RecogniseExplicitNamedEnumValues(DbMapperFactory.CreateSqlMapper(null, ""), true); }
                // We instanciate ourself the values (usually values are mapped with DB, see below)
                public static readonly FooDynamicEnum DynamicEnumValue1 = new FooDynamicEnum() { EnumId = 1, EnumName = "Value 1" };
                public static readonly FooDynamicEnum DynamicEnumValue2 = new FooDynamicEnum() { EnumId = 2, EnumName = "Value 2" };
                public static readonly FooDynamicEnum DynamicEnumValue3 = new FooDynamicEnum() { EnumId = 3, EnumName = "Value 3" };
                public static readonly FooDynamicEnum DynamicEnumValue4 = new FooDynamicEnum() { EnumId = 4, EnumName = "Value 4" };
                public static readonly FooDynamicEnum DynamicEnumValue5 = new FooDynamicEnum() { EnumId = 5, EnumName = "Value 5" };

                protected override long EnumId { get; set; }
                protected override string EnumName { get; set; }

                public override void CopyAllFieldsFrom(BaseDTO source) { throw new NotImplementedException(); }
                protected override BaseDTO CreateNewInstance() { throw new NotImplementedException(); }

                // Usually DynamicEnum are loaded using a DBmapper, 
                // here we rely on FooDynamicEnum_ValuesHolder singletons to override the initialization
                //class FooDynamicEnum_ValuesHolder : DynamicEnum_ValuesHolder
                //{
                //    static FooDynamicEnum_ValuesHolder()
                //    {
                //        Instance = new FooDynamicEnum_ValuesHolder();
                //    }

                //    public override ICollection<TEnumClass> GetAll<TKey, TEnumClass>(IDbMapper mapper = null)
                //    {
                //        if (typeof(TEnumClass) == typeof(FooDynamicEnum))
                //            return new List<TEnumClass>()
                //        {
                //            (TEnumClass)(object)FooDynamicEnum.DynamicEnumValue1,
                //            (TEnumClass)(object)FooDynamicEnum.DynamicEnumValue2,
                //            (TEnumClass)(object)FooDynamicEnum.DynamicEnumValue3,
                //            (TEnumClass)(object)FooDynamicEnum.DynamicEnumValue4,
                //            (TEnumClass)(object)FooDynamicEnum.DynamicEnumValue5
                //        };
                //        throw new NotImplementedException("Not in test!");
                //    }
                //}
                //static FooDynamicEnum() { RuntimeHelpers.RunClassConstructor(typeof(FooDynamicEnum_ValuesHolder).TypeHandle); }
            }

            public class FooStaticEnum : IComparable<FooStaticEnum>, IComparable, IStaticEnum
            {
                public string Code { get; private set; }
                public string Label { get; private set; }

                IIdTuple IHasClosedIdReadable.Id { get { return new IdTuple<string>(Code); } }
                ITypedId IHasTypedIdReadable.TypedId { get { return (this as IHasTypedIdReadable).MakeTypedId((this as IHasClosedIdReadable).Id); } }
                ITypedId IHasTypedIdReadable.MakeTypedId(IIdTuple id) { return id.ToTypedId(GetType()); }

                private FooStaticEnum(string code, string label)
                {
                    Code = code;
                    Label = label;
                }

                public override string ToString() { return Code + " — " + Label; }
                public int CompareTo(FooStaticEnum other) { return Code.CompareTo(other.Code); }
                public int CompareTo(object obj) { return obj is FooStaticEnum ? CompareTo(obj as FooStaticEnum) : -1; }

                #region All Codes

                public static readonly FooStaticEnum Value1 = new FooStaticEnum("Value1", "The first value");
                public static readonly FooStaticEnum Value2 = new FooStaticEnum("Value2", "The second value");
                public static readonly FooStaticEnum Value3 = new FooStaticEnum("Value3", "The third value");
                public static readonly FooStaticEnum Value4 = new FooStaticEnum("Value4", "The fourth value");
                public static readonly FooStaticEnum Value5 = new FooStaticEnum("Value5", "The fifth value");

                public static readonly IEnumerable<FooStaticEnum> All = new List<FooStaticEnum>()
                {
                    Value1,
                    Value2,
                    Value3,
                    Value4,
                    Value5
                };
                #endregion All Codes
            }
        }
    }
}
