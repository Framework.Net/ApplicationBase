﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.MetaProgramming.ExpressionVisitors;

using DataMapper;


namespace ApplicationBase.Business.MetaProgramming.DecisionTree
{
    /// <summary> Internal only </summary>
    class ConditionalExpressionBeautifierVisitor : ExpressionConverterVisitor<string>
    {
        public string ConvertIfCondition(Expression condition)
        {
            //return "TODO (IF COND)";
            return Visit(condition);
        }
        public string ConvertSwitchCaseValue(Expression condition)
        {
            //return " TODO (CASE VALUE)";
            return Visit(condition);
        }
        public string ConvertSwitchValue(Expression @switch)
        {
            //return "TODO (SWITCH VALUE)";
            return Visit(@switch); // + node.SwitchValue.ToString().Truncate(20);
        }

        public override string Visit(Expression node)
        {
            return base.Visit(node);
        }

        protected override string VisitBlock(BlockExpression block)
        {
            throw new TechnicalException("Unknown case!", null);
        }
        protected override string VisitConditional(ConditionalExpression @if)
        {
            // Detect code as "exp == null ? null : exp.Property" 
            // which is also exp?.Code
            if ((@if.Test is BinaryExpression be) &&
                (@be.Right is ConstantExpression bece) && bece.Value == null &&
                @be.Left.ToString() == (@if.IfFalse as MemberExpression)?.Expression.ToString() &&
                (@if.IfTrue is DefaultExpression de) && de.Type == @if.IfFalse.Type)
            {
                return Visit(@if.IfFalse);
            }
            // Detect code like this 
            // exp == null ? null : (long?)DynamicEnum.GetEnumId(exp)
            if ((@if.Test is BinaryExpression be2) &&
                (@be2.Right is ConstantExpression bece2) && bece2.Value == null &&
                (@if.IfFalse is UnaryExpression ue) && ue.NodeType == ExpressionType.Convert &&
                ue.Operand is MethodCallExpression mce &&
                mce.Method.Name == nameof(DynamicEnum.GetEnumId) &&
                typeof(DynamicEnum).IsAssignableFrom(mce.Method.DeclaringType))
            {
                return Visit(mce);
            }
            return Visit(@if.Test) + " ? "
                    + Visit(@if.IfTrue) + " : "
                    + Visit(@if.IfFalse);
        }
        protected override string VisitSwitch(SwitchExpression @switch)
        {
            throw new TechnicalException("Unknown case!", null);
        }
        protected override string VisitDefault(DefaultExpression node)
        {
            if (node.NodeType == ExpressionType.Default && node.Type == typeof(string))
                return "\"\"";
            if (node.NodeType == ExpressionType.Default && node.Type.Implements(typeof(Nullable<>)))
                return "<empty>";
            throw new TechnicalException("Unknown case!", null);
        }
        protected override string VisitBinary(BinaryExpression binExp)
        {
            string left;
            string right;
            // Detect form like "(int)a == 42" // where a is actually a char
            if (binExp.Left is UnaryExpression ue && ue.NodeType == ExpressionType.Convert
                && ue.Type == typeof(int) && binExp.Right is ConstantExpression ce && ce.Type == typeof(int) &&
                ue.Operand.Type == typeof(char))
            {
                left = Visit(ue.Operand);
                right = "'" + (char)(int)ce.Value + "'";
            }
            else
            {
                left = Visit(binExp.Left);
                right = Visit(binExp.Right);
            }
            var nl = Environment.NewLine;
            switch (binExp.NodeType)
            {
                case ExpressionType.Equal: return left + " = " + right;
                case ExpressionType.Coalesce: if (right == "\"\"") return left; else throw new TechnicalException("Unknown case!", null);
                case ExpressionType.OrElse:
                    if (binExp.Left is BinaryExpression beLeft)
                        if (beLeft.NodeType == ExpressionType.OrElse)
                            left = "   " + left.Replace(nl, nl + "   "); 
                        else if (beLeft.NodeType != ExpressionType.AndAlso)
                            left = "   (" + left.Replace(nl, nl + "    ") + ")";
                    right = right.Replace(nl, nl + "   ");
                    return left + nl + "OR " + right;
                case ExpressionType.AndAlso:
                    if (binExp.Left is BinaryExpression beLeft2)
                        if (beLeft2.NodeType == ExpressionType.OrElse)
                            left = "    (" + left.Replace(nl, nl + "     ") + ")";
                        else if (beLeft2.NodeType != ExpressionType.AndAlso)
                            left = "    " + left.Replace(nl, nl + "    ");

                    if (binExp.Right is BinaryExpression beRight &&
                        beRight.NodeType == ExpressionType.OrElse)
                        right = "(" + right.Replace(nl, nl + "     ") + ")";
                    else
                        right = right.Replace(nl, nl + "    ");
                    return left + nl + "AND " + right;
                case ExpressionType.LessThan: return left + " > " + right;
                case ExpressionType.LessThanOrEqual: return left + " <= " + right;
                case ExpressionType.GreaterThan: return left + " > " + right;
                case ExpressionType.GreaterThanOrEqual: return left + " >= " + right;
                default:
                    throw new TechnicalException("Unknown case!", null);
            }
        }


        protected override string VisitConstant(ConstantExpression node)
        {
            if (node.Value is string)
                return "\"" + node.Value + "\"";
            if (node.Value is bool b)
                return b.ToString();
            if (node.Value is IFormattable vf)
                return vf.ToString(null, System.Globalization.CultureInfo.InvariantCulture);
            if (node.Value == null)
                return "<empty>";
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitConstant(node);
        }

        protected override string VisitLambda<TFunc>(Expression<TFunc> node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitLambda(node);
        }

        protected override string VisitMember(MemberExpression node)
        {
            if ((node.Expression is ParameterExpression pe) &&
                pe.Name == "typedInput"&&
                node.Member is PropertyInfo pi)
                return node.Member.Name;

            if ((node.Member is FieldInfo fi) &&
                fi.IsStatic && 
                typeof(IStaticEnum).IsAssignableFrom(fi.FieldType))
            {
                var se = (IStaticEnum)fi.GetValue(null);
                return se.Code + " | " + se.Label;
            }
            if (node.Member.Name == nameof(string.Empty) && node.Member.DeclaringType == typeof(string))
                return "\"\"";
            if (node.Expression is MemberExpression)
                return base.VisitMember(node);
            throw new TechnicalException("Unknown case!", null);
        }

        protected override string VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.Name == nameof(DynamicEnum.GetValueFor) && node.Method.IsStatic &&
                typeof(DynamicEnum).IsAssignableFrom(node.Method.DeclaringType))
            {
                var id = (int)(node.Arguments[0] as ConstantExpression).Value;
                var de = (DynamicEnum)node.Method.Invoke(null, new object[] { id, null });
                return de.Caption;
            }
            if (node.Method.Name == nameof(DynamicEnum.GetEnumId) && node.Method.IsStatic &&
                typeof(DynamicEnum).IsAssignableFrom(node.Method.DeclaringType))
            {
                return Visit(node.Arguments[0]);
            }
            if (node.Method.DeclaringType == typeof(string) && node.Method.Name == nameof(string.StartsWith))
            {
                Debug.Assert(node.Arguments.Count == 1);
                return Visit(node.Object) + " starts with " + Visit(node.Arguments[0]);
            }
            if (node.Method.DeclaringType == typeof(string) && node.Method.Name == nameof(string.Contains))
            {
                Debug.Assert(node.Arguments.Count == 1);
                return Visit(node.Object) + " contains " + Visit(node.Arguments[0]);
            }
                
            if (node.Method.DeclaringType == typeof(Object_Extensions) && node.Method.Name == nameof(Object_Extensions.In))
            {
                Debug.Assert(node.Object == null, $"\"{nameof(Object_Extensions.In)}\" is an extension method");
                Debug.Assert(node.Arguments.Count == 2);
                var left = Visit(node.Arguments[0]) + " is any of ";

                var nae = node.Arguments[1] as NewArrayExpression;
                Debug.Assert(nae != null);
                string args = "";
                foreach (var exp in nae.Expressions)
                {
                    if (args.Length > 15)
                        args += "," + Environment.NewLine + new string(' ', left.Length);
                    else if (args.Length > 0)
                        args += ", ";
                    args += Visit(exp);
                }
                return left + args;
            }
                

            throw new TechnicalException("Unknown case!", null);
            //return base.VisitMethodCall(node);
        }

        protected override string VisitNewArray(NewArrayExpression node)
        {
            return node.Expressions.Select(exp => Visit(exp)).Join(", ");
        }
        protected override string VisitParameter(ParameterExpression node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitParameter(node);
        }

        protected override string VisitUnary(UnaryExpression node)
        {
            if (node.NodeType == ExpressionType.Convert)
                return Visit(node.Operand);
            if (node.NodeType == ExpressionType.Not)
                return "Not(" + Visit(node.Operand) + ")";
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitUnary(node);
        }

        protected override string VisitNew(NewExpression node)
        {
            // for value like "new DateTime(...)"
            return base.VisitNew(node);
        }



        protected override string VisitCatchBlock(CatchBlock node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitCatchBlock(node);
        }
        protected override string VisitElementInit(ElementInit node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitElementInit(node);
        }
        protected override string VisitLabelTarget(LabelTarget node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitLabelTarget(node);
        }
        protected override string VisitMemberAssignment(MemberAssignment node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitMemberAssignment(node);
        }
        protected override string VisitMemberBinding(MemberBinding node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitMemberBinding(node);
        }
        protected override string VisitMemberListBinding(MemberListBinding node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitMemberListBinding(node);
        }
        protected override string VisitMemberMemberBinding(MemberMemberBinding node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitMemberMemberBinding(node);
        }
        protected override string VisitSwitchCase(SwitchCase node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitSwitchCase(node);
        }
        protected override string VisitDebugInfo(DebugInfoExpression node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitDebugInfo(node);
        }
        protected override string VisitDynamic(DynamicExpression node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitDynamic(node);
        }
        protected override string VisitGoto(GotoExpression node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitGoto(node);
        }
        protected override string VisitIndex(IndexExpression node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitIndex(node);
        }
        protected override string VisitInvocation(InvocationExpression node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitInvocation(node);
        }
        protected override string VisitLabel(LabelExpression node)
        {
            return null;
        }
        protected override string VisitListInit(ListInitExpression node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitListInit(node);
        }
        protected override string VisitLoop(LoopExpression node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitLoop(node);
        }
        protected override string VisitMemberInit(MemberInitExpression node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitMemberInit(node);
        }
        protected override string VisitRuntimeVariables(RuntimeVariablesExpression node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitRuntimeVariables(node);
        }
        protected override string VisitTry(TryExpression node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitTry(node);
        }
        protected override string VisitTypeBinary(TypeBinaryExpression node)
        {
            throw new TechnicalException("Unknown case!", null);
            //return base.VisitTypeBinary(node);
        }
    }
}
