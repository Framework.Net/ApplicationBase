﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools;
using ApplicationBase.Deployment;
using ApplicationBase.Deployment.Data;
using TechnicalTools.Model;


namespace ApplicationBase.Business
{
    public class ReleaseNotifier : IUserInteractiveObject
    {
        public IFixedBindingList<ReleaseNote> Notes { get { return _notes; } }
        readonly FixedBindingList<ReleaseNote> _notes = new FixedBindingList<ReleaseNote>();

        protected internal ReleaseNotifier(Deployment.Config config, bool canSeeTechnicalRelease)
        {
            _config = config;
            _canSeeTechnicalRelease = canSeeTechnicalRelease;
        }
        readonly Deployment.Config _config;
        readonly bool              _canSeeTechnicalRelease;

        public virtual void Load()
        {
            var notes = Load(null);
            _notes.Clear();
            _notes.AddRange(notes);
        }

        public UnseenReleaNoteByUser GetNewReleaseNotesForUser(ConfigOfUser userConfig)
        {
            return new UnseenReleaNoteByUser(this, userConfig, Load(userConfig));
        }
        public class UnseenReleaNoteByUser : List<ReleaseNoteWithContent>, IUserInteractiveObject
        {
            readonly ReleaseNotifier _owner;
            readonly ConfigOfUser _userConfig;

            internal UnseenReleaNoteByUser(ReleaseNotifier owner, ConfigOfUser userConfig, IEnumerable<ReleaseNoteWithContent> notes) : base(notes)
            {
                _owner = owner;
                _userConfig = userConfig;
            }

            public void SetLastReleaseNoteSeen(ReleaseNoteWithContent releaseNoteWithContent)
            {
                _owner.SetLastReleaseNoteSeen(_userConfig, releaseNoteWithContent);
            }
        }

        protected virtual void SetLastReleaseNoteSeen(ConfigOfUser userConfig, ReleaseNoteWithContent releaseNoteWithContent)
        {
            userConfig.Application_LastReleaseNoteSeen = releaseNoteWithContent.Id;
        }

        protected virtual List<ReleaseNoteWithContent> Load(ConfigOfUser userConfig)
        {
            if (Config.Instance.Environment.Domain == eEnvironment.LocalNoDB)
                return new List<ReleaseNoteWithContent>() { new ReleaseNoteWithContent() { IsPublic = true, Version = Deployer.GetCurrentVersion(), Note = "This version of application is running disconnected!" } };

            // Note : userConfig is null when we want to load all
            if (userConfig == null)
            {
                return _config.DataAccessor.LoadAllReleaseNotes(_canSeeTechnicalRelease);
            }
            if (string.IsNullOrWhiteSpace(userConfig.FirstLogin_ApplicationVersion))
                userConfig.FirstLogin_ApplicationVersion = Deployer.GetCurrentVersion().ToString();
            if (userConfig.Application_LastReleaseNoteSeen == null)
            {
                var lastNote = _config.DataAccessor.LoadLastReleaseNote(_canSeeTechnicalRelease).SingleOrDefault();
                // lastNote may be null on initial release if developper did not say anything
                userConfig.Application_LastReleaseNoteSeen = lastNote?.Id;
            }

            var v = userConfig.Application_LastReleaseNoteSeen == null
                  ? Version.Parse(userConfig.FirstLogin_ApplicationVersion)
                  : null;
            var notes = _config.DataAccessor.LoadReleaseNotesAfter(_canSeeTechnicalRelease, v, userConfig.Application_LastReleaseNoteSeen);
            return notes;
        }
    }
}
