﻿using System;


namespace ApplicationBase.Business.Dialoguing
{
    public class Messenger : DAL.Dialoguing.Messenger
    {
        protected internal Messenger(Dialoguer dialoguer)
            : base(dialoguer)
        {
        }
    }

}
