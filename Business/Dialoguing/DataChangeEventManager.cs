﻿using System;

using TechnicalTools.Model;

using DataMapper;

using ApplicationBase.DAL.Logging;


namespace ApplicationBase.Business.Dialoguing
{
    public class DataChangeEventManager : DAL.Dialoguing.DataChangeEventManager, IUserInteractiveObject
    {
        protected internal DataChangeEventManager(IDbMapper mapper, Dialoguer dialoguer)
            : base(mapper, dialoguer)
        {
        }

        protected override bool IsInteresting(DataChangedEventArgs e)
        {
            return e.EntityType != typeof(Log) 
                && e.EntityType != typeof(LogSession) 
                && base.IsInteresting(e);
        }
    }
}
