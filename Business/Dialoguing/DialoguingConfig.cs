﻿using System;


namespace ApplicationBase.Business.Dialoguing
{
    public class DialoguingConfig
    {
        public bool EnableDataChangeNotification { get; set; }
    }
}
