﻿using System;
using System.Threading;

using TechnicalTools.Model;

using ApplicationBase.DAL.Dialoguing;


namespace ApplicationBase.Business.Dialoguing
{
    public class Dialoguer : DAL.Dialoguing.Dialoguer, IUserInteractiveObject
    {
        protected internal Dialoguer(string connectionStringListener, string connectionStringStarter, short originActorId, SynchronizationContext context)
            : base(connectionStringListener, connectionStringStarter, originActorId, context)
        {
        }

        public Dialogue BuildDialogue(EventType eventType, Status status = null, EntityType entityType = null, long? entityId = 0, string additionalData = null, ActorType originActorType = null, short? actorId = null, int? partnerId = null)
        {
            var e = base.BuildDialogue(eventType, status, entityType, entityId, additionalData, originActorType, actorId);
            e.PartnerEnvironmentId = partnerId;
            return e;
        }

    }

}
