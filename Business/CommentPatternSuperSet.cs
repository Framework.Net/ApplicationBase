﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using TechnicalTools;
using TechnicalTools.Model;

using DataMapper.Helpers;

using ApplicationBase.DAL;
using ApplicationBase.DAL.Tools;
using ApplicationBase.Common;

using ICommentPattern = ApplicationBase.DAL.HasPropertiesOf.Tools.CommentPattern;


namespace ApplicationBase.Business
{
    // Contains all comment pattern for all use (because volumetry is low so we keep all in cache here)
    public class CommentPatternSuperSet : EditableDTOSet<CommentPattern, ICommentPattern>, IUserInteractiveObject //, ICommentPatternSet
    {
        protected internal CommentPatternSuperSet()
            : base(DB.Dao_Base, false, true)
        {
            AllowNew = true;
            AllowEdit = true;
            AllowRemove = true;
            Refresh();
        }

        public new FixedBindingList<CommentPattern> OriginalSet
        {
            get { return base.OriginalSet; }
        }

        protected override List<CommentPattern> GetFreshItems(IProgress<string> pr)
        {
            return DB.Dao_Base.LoadCollection<CommentPattern>();
        }

        protected override CommentPattern CreateNew()
        {
            return new CommentPattern();
        }

        protected override Action ApplyToDatabaseAndModel(IProgress<string> pr = null)
        {
            //var logs = GetAllChangesAsLog();
            //_log.Audit(logs);

            Action baseApply = base.ApplyToDatabaseAndModel(pr);
            return baseApply;
        }

        protected override string ValidateProperty(ICommentPattern item, Type interfaceType, PropertyInfo property)
        {
            if (property.Name == nameof(ICommentPattern.Pattern))
            {
                if (string.IsNullOrWhiteSpace(item.Pattern))
                    return "Comment cannot be empty!";
                var res = CommentPattern_Extensions.GetEntryNamesOrException(item.Pattern);
                if (res.Item2 != null)
                    return res.Item2.Message;
            }
            else if (property.Name == nameof(ICommentPattern.KeyWord))
            {
                if (string.IsNullOrWhiteSpace(item.KeyWord))
                    return "Comment cannot be empty!";
            }
            return base.ValidateProperty(item, interfaceType, property);
        }
        protected override string ValidateSet(IEnumerable<ICommentPattern> set, IProgress<string> pr = null)
        {
            // ReSharper disable PossibleMultipleEnumeration
            var duplicate = set.GroupBy(p => new { p.SetId, p.Pattern })
                               .Where(grp => grp.Count() >= 2)
                               .ToList();
            if (duplicate.Any())
                return "This comment cannot be used more than once: " + duplicate.Select(grp => grp.Key.Pattern).Join();
            var duplicate2 = set.GroupBy(p => new { p.SetId, p.KeyWord })
                                .Where(grp => grp.Count() >= 2)
                                .ToList();
            if (duplicate2.Any())
                return "This keyword cannot be used more than once: " + duplicate.Select(grp => grp.Key.Pattern).Join();

            return base.ValidateSet(set);
            // ReSharper restore PossibleMultipleEnumeration
        }
    }

    // base class for filtering and create sub set
    public class CommentPatternSubSet : EditableDTOSubSetDependentOnSimpleFilter<CommentPattern, ICommentPattern>, IUserInteractiveObject
    {
        readonly byte _setId;
        public bool HasRelatedAutomaticAction { get; }

        protected CommentPatternSubSet(byte setId, bool hasRelatedAutomaticAction, bool doNotLoadItems = false)
            : base(BusinessSingletons.Instance.GetCommentPatternSuperSet(), p => p.SetId == setId, doNotLoadItems)
        {
            _setId = setId;
            HasRelatedAutomaticAction = hasRelatedAutomaticAction;
        }

        public new FixedBindingList<CommentPattern> OriginalSet
        {
            get { return base.OriginalSet; }
        }
        
        protected override CommentPattern CreateNew()
        {
            return new CommentPattern()
            {
                SetId = _setId,
            };
        }
    }
}
