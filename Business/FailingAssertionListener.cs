﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Tools;


namespace ApplicationBase.Business
{
    public class FailingAssertionListener : TraceListener
    {
        static readonly ILogger _log = LogManager.Default.GetLogger(typeof(FailingAssertionListener));

        readonly List<TraceListener> _originalListeners;

        public FailingAssertionListener(IEnumerable<TraceListener> originalListeners)
        {
            _originalListeners = originalListeners.ToList();
            DebugTools.ShouldAssertFail += msg => Fail(msg, null, true);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public override void Fail(string msg, string detailedMsg)
        {
            Fail(msg, detailedMsg, false);
        }
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public virtual void Fail(string msg, string detailedMsg, bool isAssertShouldNotMust)
        {
            //!Environment.HasShutdownStarted &&
            //!AppDomain.CurrentDomain.IsFinalizingForUnload()

            if (DebugTools.IsForDevelopper && !isAssertShouldNotMust)
            {
                // Break necessaire pour capturer l'etat du programme a cet instant 
                // Details ici : http://stackoverflow.com/questions/18044707/debug-assert-has-unexpected-side-effects-searching-for-alternatives
                DebugTools.Break();

                // Execute le comportement par defaut
                // foreach (TraceListener listener in _originalListeners)
                //     listener.Fail(msg, detailedMsg);
            }
            else
            {
                if (isAssertShouldNotMust)
                    msg += Environment.NewLine + Environment.NewLine + "This message is JUST a warning (not a real assert)! You can continue your work";
                try
                {
                    var stack = new StackTrace(1, true);
                    LogAssertion(stack, msg, detailedMsg);
                }
                catch // Ne devrait jamais arriver, la raison est inexplicable (NullReferenceException alors que toute les variables sont OK)
                {
                    DebugTools.Break();
                }
                if (Business.Config.Instance != null && Business.Config.Instance.Debug.ShowAssertion && !isAssertShouldNotMust)
                    foreach (TraceListener listener in _originalListeners)
                        listener.Write(msg + Environment.NewLine + "Details: " + Environment.NewLine + detailedMsg);
            }
        }

        // Ce code est mis dans une autre methode car si je le met apres la declaration de stack 
        // ca plante avec une null reference exception sans raison !
        [MethodImpl(MethodImplOptions.NoInlining)]
        static void LogAssertion(StackTrace stack, string msg, string detailedMsg)
        {
            // Lance un thread afin d'avoir le message dans la langue anglaise et non francaise ou autre
            // Attention il faut tester ce code car l'utilisation de thread dans un contexte d'assertion echoué semble etre problematique
            // (exemple : il ne faut pas factoriser pour utiliser une seule variable au lieu de th et th2)
            // Au cas ou l'erreur logguée ne serait quand même pas en anglais, on peut chercher ici : http://unlocalize.com/
            var th2 = new Thread(() =>
            {
                var th = Thread.CurrentThread;
                var cc = th.CurrentCulture;
                var cuc = th.CurrentUICulture;
                using (new GenericDisposableContainer(() => th.CurrentCulture = CultureInfo.InvariantCulture, () => th.CurrentCulture = cc))
                using (new GenericDisposableContainer(() => th.CurrentUICulture = CultureInfo.InvariantCulture, () => th.CurrentUICulture = cuc))
                {
                    var lines = stack.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    int skipped_frames = 0;
                    while (lines[skipped_frames].StartsWith("   at System.Diagnostics"))
                        ++skipped_frames;

                    _log.Debug("ASSERTION FAILED: " + (msg ?? "<no message>") + Environment.NewLine +
                               (detailedMsg == null ? "" : " (details: " + detailedMsg + ")" + Environment.NewLine) +
                               string.Join(Environment.NewLine, lines.Skip(skipped_frames)));
                }
            });
            th2.Start();
            th2.Join();
        }

        [DebuggerHidden, DebuggerStepThrough]
        public override void Write(string message)
        {
            foreach (TraceListener listener in _originalListeners)
                listener.Write(message);
        }

        [DebuggerHidden, DebuggerStepThrough]
        public override void WriteLine(string message)
        {
            foreach (TraceListener listener in _originalListeners)
                listener.WriteLine(message);
        }
    }

}
