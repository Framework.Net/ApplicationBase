﻿using System;
using System.Diagnostics;

namespace ApplicationBase.Business.Treasury
{
    /// <summary>
    /// Business Identifier Code
    /// Follow ISO 9362. See also revision "ISO 9362:2014"
    /// There are over 7,500 "live" codes (for partners actively connected to the BIC network) and an estimated 10,000 additional BIC codes which can be used for manual transactions.
    /// </summary>
    public struct BIC : IEquatable<BIC>
    {
        public static readonly BIC None = new BIC();

        // Official data
        public string BankCode                  { get { return _biccode?.Remove(4); } }
        // Code ISO 3166-1 alpha-2
        public string CountryCode               { get { return _biccode?.Substring(4, 2); } }
        public string LocalizationCode          { get { return _biccode?.Substring(6, 2); } }
        public string BranchCode                { get { return _biccode?.Substring(8, 3); } }
        public bool   IsBranchCodePrimaryOffice { get { return BranchCode == "XXX"; } }

        // To know the type of BIC : BIC8 or BIC11
        public int    Length                    { get { return _biccode?.Length ?? 0; } }

        public BIC(string biccode)
        {
            var err = IsValidBIC(biccode);
            if (err != null)
                throw new ArgumentException(err, nameof(biccode));
            if (char.IsUpper(biccode[0]) && char.IsUpper(biccode[1]) && char.IsUpper(biccode[2]) && char.IsUpper(biccode[3]) &&
                char.IsUpper(biccode[4]) && char.IsUpper(biccode[5]) && char.IsUpper(biccode[6]) && char.IsUpper(biccode[7]) &&
                (biccode.Length == 8 ||
                 char.IsUpper(biccode[8]) && char.IsUpper(biccode[9]) && char.IsUpper(biccode[10])))
                _biccode = biccode;
            else
                _biccode = biccode.ToUpperInvariant();
        }
        readonly string _biccode;

        public static string IsValidBIC(string biccode)
        {
            if (biccode == null)
                return "BIC code is empty!";
            if (biccode.Length != 8 && biccode.Length != 11)
                return "Length of BIC code is expected to be 8 or 11 character!";
            return null;
        }
        public static implicit operator BIC(string biccode)
        {
            if (biccode == null)
                return None;
            return new BIC(biccode);
        }

        public static implicit operator string(BIC bic)
        {
            return bic._biccode;
        }

        public override string ToString()
        {
            return _biccode;
        }

        public bool Equals(BIC other)
        {
            return _biccode == other._biccode;
        }
        public override bool Equals(object obj)
        {
            return obj is BIC bic
                 ? Equals(bic)
                 : _biccode == null;
        }
        public override int GetHashCode()
        {
            return _biccode.GetHashCode();
        }
    }
}
