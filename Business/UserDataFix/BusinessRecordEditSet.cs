﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using DataMapper;
using DataMapper.Helpers;

using ApplicationBase.DAL.Users;
using ApplicationBase.DAL.UserDataFix;

using IBusinessRecordEdit = ApplicationBase.DAL.HasPropertiesOf.UserDataFix.BusinessRecordEdit;


namespace ApplicationBase.Business.UserDataFix
{
    public partial class BusinessRecordEditSet : EditableDTOSet<BusinessRecordEdit, IBusinessRecordEdit>, IUserInteractiveObject
    {
        public new IEnumerable<BusinessRecordEdit> OriginalSet { get { return base.OriginalSet; } }

        public BusinessRecordEditSet(IDbMapper dbMapper, AuthenticationManager manager, Feature featureRequiredToEdit)
            : base(dbMapper, false, true, true)
        {
            _dbMapper = dbMapper;
            _manager = manager;
            _featureRequiredToEdit = featureRequiredToEdit;
            AllowNew = true;
            AllowRemove = false;
            AllowEdit = false;
            Refresh();
        }
        readonly IDbMapper _dbMapper;
        readonly AuthenticationManager _manager;
        readonly Feature _featureRequiredToEdit;

        protected override List<BusinessRecordEdit> GetFreshItems(IProgress<string> pr)
        {
            return _dbMapper.LoadCollection<BusinessRecordEdit>()
                            // Sort is important;
                            .OrderByDescending(e => e.WhenUTC)
                            .ToList();
        }

        public override int Refresh(IProgress<string> pr = null)
        {
            var res = base.Refresh(pr);
            RefreshValues();
            return res;
        }
        protected override BusinessRecordEdit CreateNew()
        {
            var newInstance = BusinessSingletons.Instance.GetBusinessFactory().CreateBusinessRecordEdit();
            newInstance.WhenUTC = DateTime.UtcNow.TruncateUnderSeconds();
            return newInstance;
        }

        protected override string ValidateProperty(IBusinessRecordEdit item, Type interfaceType, PropertyInfo property)
        {
            var value = property.GetValue(item);
            Debug.Assert(interfaceType == typeof(IBusinessRecordEdit));
            switch (property.Name)
            {
                case nameof(IBusinessRecordEdit.WhoId): if ((long)value == 0) return "User Id must be set!"; break;
                case nameof(IBusinessRecordEdit.WhenUTC): if ((DateTime)value == DateTime.MinValue) return "Date of change must be set!"; break;
                case nameof(IBusinessRecordEdit.TypedId_TypeFullName): if (string.IsNullOrWhiteSpace((string)value)) return "TypedId_TypeFullName must be set!"; break;
                case nameof(IBusinessRecordEdit.TypedId_Keys): if (string.IsNullOrWhiteSpace((string)value)) return "TypedId_Keys must be set!"; break;
                case nameof(IBusinessRecordEdit.TypedId_IdTuple_Type): if (string.IsNullOrWhiteSpace((string)value)) return "TypedId_IdTuple_Type must be set!"; break;
                case nameof(IBusinessRecordEdit.PropertyType): if (string.IsNullOrWhiteSpace((string)value)) return "PropertyType must be set!"; break;
                case nameof(IBusinessRecordEdit.PropertyName): if (string.IsNullOrWhiteSpace((string)value)) return "PropertyName must be set!"; break;
                case nameof(IBusinessRecordEdit.NewValue): break; // nothing
            }
            return null;
        }




        protected override string ValidateSet(IEnumerable<IBusinessRecordEdit> accounts, IProgress<string> pr = null)
        {
            return base.ValidateSet(accounts, pr);
        }

        protected override void OnPropertyAssigned(IBusinessRecordEdit item, Type interfaceType, string propertyName)
        {
            base.OnPropertyAssigned(item, interfaceType, propertyName);
        }

        protected override string OnPropertyAssigning(IBusinessRecordEdit item, Type interfaceType, string propertyName, object value)
        {
            var error = base.OnPropertyAssigning(item, interfaceType, propertyName, value);
            if (error != null)
                return error;
            /* Nothing to do yet */
            return null;
        }

        #region Manage edits

        // Edit are returned from the most recent to the older ones
        public IReadOnlyCollection<BusinessRecordEdit> GetAllUserBusinessEdits(ITypedId objTid, string propertyName)
        {
            var edits = GetOrAddListOfEdits(objTid, propertyName, null);
            return edits;
        }
        /// <summary>
        /// Note : lack of covariace in IReadOnlyDictionary prevent us to have value of type IReadOnlyCollection
        /// So we return List to avoid memory work (not ideal !)
        /// </summary>
        public IReadOnlyDictionary<string, List<BusinessRecordEdit>> GetAllUserBusinessEdits(ITypedId objTid)
        {
            var edits = GetOrAddListOfEdits(objTid, null);
            return edits;
        }
        public bool GetValue(ITypedId objTid, out object value, [CallerMemberName] string propertyName = null)
        {
            var edits = GetOrAddListOfEdits(objTid, propertyName, null);
            var edit = edits?.FirstOrDefault();
            value = edit?.NewValue;
            return edit != null;
        }
        public void SetValue<TProperty>(ITypedId objTid, TProperty newValue, TProperty oldValue, [CallerMemberName] string propertyName = null)
        {
            // Best way to compare value for both reference and value type, without boxing. Handle null too
            if (EqualityComparer<TProperty>.Default.Equals(newValue, oldValue))
                return;
               
            _manager.CheckFeatureAccess(_featureRequiredToEdit, DAL.eFeatureAccess.CanEdit);
            var edits = GetOrAddListOfEdits(objTid, propertyName, newValue?.GetType() ?? oldValue?.GetType() ??  typeof(TProperty));
            var edit = edits.First();
            edit.NewValue = newValue;
            edit.OldValue = oldValue;
            try
            {
                _dbMapper.CreateInDatabase(edit, true);
            }
            catch
            {
                lock (_values)
                    edits.Remove(edit);
                throw;
            }
        }
        Dictionary<string, List<BusinessRecordEdit>> GetOrAddListOfEdits(ITypedId objTid, Type propertyTypeToCreate)
        {
            var idTuple = objTid.Key as IIdTuple;
            lock (_values)
            {
                var byObjects = _values.TryGetValueClass(objTid.Type);
                if (byObjects == null)
                    if (propertyTypeToCreate == null)
                        return null;
                    else
                        _values.Add(objTid.Type, byObjects = new Dictionary<IIdTuple, Dictionary<string, List<BusinessRecordEdit>>>());
                var byProperty = byObjects.TryGetValueClass(idTuple);
                if (byProperty == null)
                    if (propertyTypeToCreate == null)
                        return null;
                    else
                        byObjects.Add(idTuple, byProperty = new Dictionary<string, List<BusinessRecordEdit>>());
                return byProperty;
            }
        }
        List<BusinessRecordEdit> GetOrAddListOfEdits(ITypedId objTid, string propertyName, Type propertyTypeToCreate)
        {
            var idTuple = objTid.Key as IIdTuple;
            lock (_values)
            {
                var byProperty = GetOrAddListOfEdits(objTid, propertyTypeToCreate);
                if (byProperty == null)
                    return null;
                var edits = byProperty.TryGetValueClass(propertyName);
                if (edits == null)
                    if (propertyTypeToCreate == null)
                        return null;
                    else
                        byProperty.Add(propertyName, edits = new List<BusinessRecordEdit>());
                if (propertyTypeToCreate != null)
                {
                    var edit = CreateNew();
                    edit.WhoId = _manager.CurrentUser.Person_Id;
                    edit.TypedId_TypeFullName = objTid.Type.ToAssemblyQualifiedStringWithoutVersion();
                    Debug.Assert(objTid.Type == edit.TypedId_TypeFullName.ToTypeFromAssemblyQualifiedStringWithoutVersion());
                    edit.TypedId_IdTuple_Type = idTuple.GetType().ToAssemblyQualifiedStringWithoutVersion();
                    Debug.Assert(idTuple.GetType() == edit.TypedId_IdTuple_Type.ToTypeFromAssemblyQualifiedStringWithoutVersion());
                    edit.TypedId_Keys = IdTuple.Serialize(idTuple);
                    edit.PropertyType = propertyTypeToCreate.ToAssemblyQualifiedStringWithoutVersion();
                    Debug.Assert(propertyTypeToCreate == edit.PropertyType.ToTypeFromAssemblyQualifiedStringWithoutVersion());
                    edit.PropertyName = propertyName;
                    edits.Insert(0, edit);
                }
                return edits;
            }
        }
        public override Action ApplyCreatedItemToDatabase(IProgress<string> pr = null)
        {
            var commit = base.ApplyCreatedItemToDatabase(pr);
            return () =>
            {
                commit();
                RefreshValues();
            };
        }
        readonly Dictionary<Type, Dictionary<IIdTuple, Dictionary<string, List<BusinessRecordEdit>>>> _values = new Dictionary<Type, Dictionary<IIdTuple, Dictionary<string, List<BusinessRecordEdit>>>>();
        void RefreshValues()
        {
            lock (_values)
            {
                var values = new Dictionary<Type, Dictionary<IIdTuple, Dictionary<string, List<BusinessRecordEdit>>>>();
                foreach (var editsByIdType in OriginalSet.GroupBy(edit => edit.TypedId_TypeFullName))
                    foreach (var editsByIdTupleType in editsByIdType.GroupBy(edit => edit.TypedId_IdTuple_Type)) // should be only one group..
                    {
                        // Because "SetValue" method has constraint "new()" on TObject, the following line is always true
                        var idType = editsByIdType.Key.ToTypeFromAssemblyQualifiedStringWithoutVersion();
                        Debug.Assert(idType != null);
                        var idTupleTemplate = editsByIdTupleType.Key.ToTypeFromAssemblyQualifiedStringWithoutVersion();
                        Debug.Assert(idTupleTemplate != null);

                        var byObjects = new Dictionary<IIdTuple, Dictionary<string, List<BusinessRecordEdit>>>();
                        values.Add(idType, byObjects);
                        foreach (var byIdTuple in editsByIdTupleType.GroupBy(e => e.TypedId_Keys))
                        {
                            IIdTuple idTuple = IdTuple.Deserialize(byIdTuple.Key, idTupleTemplate.GetGenericArguments());
                            var byProperty = new Dictionary<string, List<BusinessRecordEdit>>();
                            byObjects.Add(idTuple, byProperty);
                            foreach (var valueByProperty in byIdTuple.GroupBy(e => e.PropertyName))
                            {
                                // Currently we don't handle "path" lie Property.Nestedproperty... only simple properties
                                var edits = valueByProperty.ToList();
                                byProperty.Add(valueByProperty.Key, edits);
                            }
                        }
                    }
                _values.Clear();
                foreach (var kvp in values)
                    _values.Add(kvp.Key, kvp.Value);
            }
        }

        #endregion Manage edits
        // Because this class (BusinessRecordEditSet) uses reflection, here is a class to autocheck that all values work
        public class StaticAutoCheck : BusinessRecordEditSet
        {
            public static void CheckDataInDB(IDbMapper dbMapper)
            {
                // Actually the real test is to call RefreshValues() on checker but it is already done in constructor
                // If test fails, code throws
                new StaticAutoCheck(dbMapper);
            }

            private StaticAutoCheck(IDbMapper dbMapper) 
                : base(dbMapper, null /* this will be use only in readonly */, Feature.AllowAdminOnly)
            {
            }

            protected override List<BusinessRecordEdit> GetFreshItems(IProgress<string> pr)
            {
                var mtable = _dbMapper.GetTableMapping(typeof(BusinessRecordEdit));
                // We just need to check for reflection so need only one item for each type, no need to load more
                var sqlQuery = "SELECT " + mtable.GetSqlFieldNames(mtable.AllFields)
                             + "FROM (SELECT ROW_NUMBER() OVER(PARTITION BY " + _dbMapper.GetColumnNameFor((BusinessRecordEdit e) => e.TypedId_TypeFullName) + " ORDER BY " + _dbMapper.GetColumnNameFor((BusinessRecordEdit e) => e.Id) + ") AS RowNum, "
                             +         mtable.GetSqlFieldNames(mtable.AllFields)
                             + " FROM " + mtable.FullNameProtected + " WITH (NOLOCK)"
                             + ") as A"
                             + " where RowNum = 1";
                return _dbMapper.EnumerateCollectionFromSqlQuery<BusinessRecordEdit>(sqlQuery).ToList();
            }
        }
        
    }

}
