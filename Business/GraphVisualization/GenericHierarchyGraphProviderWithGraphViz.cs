﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Tools;

using ApplicationBase.Business.GraphVisualization.Graphviz;


namespace ApplicationBase.Business.GraphVisualization
{
    public abstract partial class GenericHierarchyGraphProviderWithGraphViz : GenericHierarchyGraphProvider
    {
        protected internal GenericHierarchyGraphProviderWithGraphViz(TypeSizeEstimator estimator = null)
            : base(estimator)
        {
        }

        public bool HaveInterestingLegend { get { return GetLegendColors().Count > 1; } }

        /// <summary>
        /// Generate the legend to display as another graph
        /// </summary>
        public virtual Graph GraphUniverseToGraphvizDigraphLegend(IReadOnlyDictionary<string, Color> colors = null, bool displayVertically = true)
        {
            colors = colors ?? GetLegendColors();
            var g = new Graph();
            g.LayoutEngine = eLayoutEngine.Dot;
            g.NodeDefaultProperties.Shape = eNodeShape.Box;
            g.RankDir = displayVertically ? eRankDir.LeftToRight : eRankDir.TopToBottom;
            var sg = g.NewSubGraph();
            sg.Label = "Legend";
            foreach (var kvp in colors)
            {
                var n = sg.NewNode();
                n.Label = kvp.Key;
                n.FillColor = kvp.Value;
            }
            return g;
        }

        /// <summary>
        /// Generate the graph to display (layout engines recommended are dot and circo)
        /// </summary>
        public virtual Graph GraphUniverseToGraphvizDigraph(bool groupByAssembly = true, bool groupByNamespace = false, eLayoutEngine? layoutEngine = null)
        {
            var g = new Graph();
            if (layoutEngine != null)
                g.LayoutEngine = layoutEngine.Value;

            g.Splines = eSplines.Ortho;
            g.NodeSep = 1;
            g.RankSep = "3";
            g.NodeDefaultProperties.Shape = eNodeShape.Box;
            // Because node labels are long, this is better to layout in this direction
            g.RankDir = eRankDir.LeftToRight;

            var hdGraph = new HierarchyDescriptionGraph();
            var allTypes = new HashSet<Type>();
            foreach (var kvp in Constructors)
                foreach (var cons in kvp.Value)
                {
                    var rel = cons(hdGraph);
                    allTypes.Add(rel.For);
                    allTypes.Add(rel.ChildType);
                }
            allTypes = allTypes.Where(DrawType).ToHashSet();
            groupByAssembly &= allTypes.GroupBy(t => t.Assembly).Count() > 1;

            var nodeByTypes = new Dictionary<Type, Node>();
            foreach (var typeByAssembly in allTypes.GroupBy(t => t.Assembly))
            {
                BaseGraph sg = g;
                if (groupByAssembly)
                {
                    var assemblyName = typeByAssembly.Key.GetName().Name;
                    sg = g.NewSubGraph();
                    sg.Comment = assemblyName;
                    sg.Label = assemblyName;
                }

                foreach (var typesBySchema in typeByAssembly.GroupBy(GetNamespaceCaption))
                {
                    var doNameSpacespaceCluster = groupByNamespace
                                               && !string.IsNullOrWhiteSpace(typesBySchema.Key)
                                               && typesBySchema.Key.ToLowerInvariant() != DefaultNamespace.ToLowerInvariant()
                                               && typesBySchema.Key.ToLowerInvariant() != "dal";
                    var ssg = sg;
                    if (doNameSpacespaceCluster)
                    {
                        ssg = sg.NewSubGraph();
                        ssg.Comment = typesBySchema.Key;
                        ssg.Label = typesBySchema.Key;
                    }
                    foreach (var type in typesBySchema)
                    {
                        var n = ssg.NewNode();
                        n.Label = GetTypeCaption(type, groupByAssembly, doNameSpacespaceCluster);
                        n.FillColor = GetTypeColor(type);
                        nodeByTypes.Add(type, n);
                    }
                }
            }

            hdGraph = new HierarchyDescriptionGraph();
            foreach (var kvp in Constructors)
                foreach (var cons in kvp.Value)
                {
                    var rel = cons(hdGraph);
                    Debug.Assert(kvp.Key == rel.For);
                    var typeFrom = kvp.Key;
                    var typeTo = rel.ChildType;
                    if (!allTypes.Contains(typeFrom) ||
                        !allTypes.Contains(typeTo))
                        continue;
                    if (!DrawRelation(rel))
                        continue;

                    var edge = g.NewEdge(nodeByTypes[typeFrom], nodeByTypes[typeTo]);
                    edge.TailLabel = GetRelationCaption(rel);
                    // greater value make the node close, so we try to keep close the node from same assembly,
                    // by a factor of 5. this factor is just an arbitrary/empirical value
                    edge.Weight = typeFrom.Assembly == typeTo.Assembly ? 5 : 1;
                    edge.Color = GetRelationColor(rel);
                }
            return g;
        }
        const string DefaultNamespace = "Dbo";

        protected virtual bool DrawType(Type t)
        {
            return true;
        }
        protected virtual string GetNamespaceCaption(Type t)
        {
            return t.Namespace.Split('.')
                    .Skip(1) // skip assembly
                    .Where(part => part.ToLowerInvariant().NotIn("dbo", "dal"))
                    .LastOrDefault();
        }
        protected virtual string GetTypeCaption(Type t, bool groupByAssembly, bool groupByNamespace)
        {
            if (groupByAssembly)
                if (groupByNamespace)
                    return t.Name;
                else
                    return GetNamespaceCaption(t) + " " + t.Name;
            else
                if (groupByNamespace)
                return t.Assembly.GetName().Name + " " + t.Name;
            else
                return t.Assembly.GetName().Name + " " + GetNamespaceCaption(t).IfNotBlankAddSuffix(" ") + t.Name;
        }
        protected virtual bool DrawRelation(HierarchyDescriptionTyped rel)
        {
            return true;
        }
        protected virtual string GetRelationCaption(HierarchyDescriptionTyped rel)
        {
            return rel.LevelName;
        }
        protected virtual Color GetTypeColor(Type t)
        {
            return Color.White;
        }
        public virtual IReadOnlyDictionary<string, Color> GetLegendColors()
        {
            return new Dictionary<string, Color>()
            {
                { "<unknown>", GetTypeColor(typeof(void)) }
            };
        }
        protected virtual Color GetRelationColor(HierarchyDescriptionTyped rel)
        {
            return Color.Black;
        }
    }
}
