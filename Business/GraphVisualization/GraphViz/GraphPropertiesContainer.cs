﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using TechnicalTools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class GraphPropertiesContainer : PropertiesContainer
    {
        public string                   Label       { get; set; }
        /// <summary>This value is inherit in subgraph</summary>
        public eGraphLabelLoc?          LabelLoc    { get; set; }
        /// <summary> For <see cref="eLayoutEngine.Dot"/> only. Default is <see cref="eRankDir.TopToBottom"/> </summary>
        public eRankDir?                RankDir     { get; set; }
        public (decimal? w, decimal? h) Sep         { get; set; }
        public eSplines?                Splines     { get; set; }
        public eOverlap?                Overlap     { get; set; }
        public int?                     PrismValue  { get; set; }
        public string                   Ratio       { get; set; }
        /// <summary> In dot, this specifies the minimum space between two adjacent nodes in the same rank, in inches.
        ///           For other layouts, this affects the spacing between loops on a single node, or multiedges between a pair of nodes.
        ///           Default is 0.25, Minimal Value is 0.02 </summary>
        public decimal?                 NodeSep     { get; set; }
        /// <summary>
        /// In dot, this gives the desired rank separation, in inches. This is the minimum vertical distance between
        /// the bottom of the nodes in one rank and the tops of nodes in the next. If the value contains "equally",
        /// the centers of all ranks are spaced equally apart. Note that both settings are possible, e.g.,
        /// ranksep = "1.2 equally".
        /// In twopi, this attribute specifies the radial separation of concentric circles. For twopi, ranksep
        /// can also be a list of doubles (separated by ':'). The first double specifies the radius of the inner circle;
        /// the second double specifies the increase in radius from the first circle to the second; etc.
        /// If there are more circles than numbers, the last number is used as the increment for the remainder.
        /// </summary>
        /// <remarks>Default value are 0.5(dot), 1.0(twopi). Minimal value is 0.02</remarks>
        public string                   RankSep     { get; set; }

        public GraphPropertiesContainer(BaseGraph parent)
            : base(parent)
        {
        }
        
        /// <summary> Return true is all values are the default ones </summary>
        public override bool PropertiesHaveDefaultValues
        {
            get
            {
                return Label == null
                    && LabelLoc == null
                    && RankDir == null
                    && Sep.w == null && Sep.h == null
                    && Splines == null
                    && Overlap == null
                    && PrismValue == null
                    && Ratio == null
                    && NodeSep == null
                    && RankSep == null;
            }
        }

        public             bool? LeftAlignLabel        { get; set; }
        protected internal bool  LeftAlignLabelCurrent { get { return LeftAlignLabel ?? true; } }

        protected internal override string EscapeRawLabel(string rawLabel)
        {
            if (LeftAlignLabel == null && Parent != null)
                return Parent.EscapeRawLabel(rawLabel);
            return rawLabel.Replace("\"", "\\\"")
                           .Replace(Environment.NewLine, LeftAlignLabelCurrent ? "\\l" : "\\n"); // we use \l instead of \n to left-align lines, 
        }

        protected override IEnumerable<(string attribute, string value)> GetPropertyValues(eLayoutEngine layout)
        {
            if (Label != null)
                yield return ("label", Label);
            if (LabelLoc.HasValue)
                yield return ("labelloc", ((char)(byte)LabelLoc).ToStringInvariant());
            if (RankDir != null)
                yield return ("rankdir", RankDir.Value.ToDot());
            if (Sep.w != null || Sep.h != null)
                yield return ("sep", "+" + (Sep.w ?? 4).ToStringInvariant() + "." + (Sep.h ?? 4).ToStringInvariant());
            if (Splines != null)
                yield return ("splines", Splines.Value.ToString().ToLowerInvariant());
            if (Overlap.HasValue)
            {
                if (layout == eLayoutEngine.Neato && Overlap != eOverlap.False && Graph.DisplayWarning)
                    Console.Error.WriteLine("Using attribute Overlap in this case may make dot to crash if graphviz is built without any triangulation library (delaunay_tri)!");
                yield return ("overlap", Overlap.Value.ToString().ToLowerInvariant());
            }
            if (PrismValue != null)
                yield return ("prismvalue", PrismValue.Value.ToStringInvariant());
            if (Ratio != null)
                yield return ("ratio", Ratio);
            if (NodeSep != null)
                yield return ("nodesep", NodeSep.Value.ToStringInvariant());
            if (RankSep != null)
                yield return ("ranksep", RankSep);
        }
    }

    /// <summary>
    /// Sets direction of graph layout. For example, if rankdir="LR", and barring cycles, an edge T -> H; will go from left to right. By default, graphs are laid out from top to bottom.
    /// This attribute also has a side-effect in determining how record nodes are interpreted (see https://graphviz.org/doc/info/shapes.html#record)
    /// </summary>
    public enum eRankDir : byte
    {
        TopToBottom,
        LeftToRight,
        BottomToTop,
        RightToLeft
    }
    static class eRankDir_Extensions
    {
        public static string ToDot(this eRankDir rank)
        {
            switch (rank)
            {
                case eRankDir.TopToBottom: return "TB";
                case eRankDir.LeftToRight: return "LR";
                case eRankDir.BottomToTop: return "BT";
                case eRankDir.RightToLeft: return "RL";
                default: throw new InvalidEnumArgumentException();
            }
        }
    }

    /// <summary>
    /// Vertical placement of labels for nodes, root graphs and clusters.
    /// </summary>
    public enum eGraphLabelLoc : byte
    {
        /// <summary> Default value for root graph label </summary>
        Bottom = (byte)'b',
        /// <summary> Default value for cluster graph label </summary>
        Top = (byte)'t',
    }

    /// <summary>
    /// Controls how, and if, edges are represented. If true, edges are drawn as splines routed around nodes; if false, edges are drawn as line segments. If set to none or "", no edges are drawn at all.
    /// See <see cref="https://graphviz.org/doc/info/attrs.html#a:splines">here</see>
    /// </summary>
    public enum eSplines
    {
        /// <summary>This is the default for all except for <see cref="eLayoutEngine.Dot"/> and <see cref="eLayoutEngine.Fdp"/></summary>
        Line, False = Line, 
        Polyline,
        Curved,
        Ortho,
        /// <summary>This is the default for <see cref="eLayoutEngine.Dot"/>.
        /// For other layout the property <see cref="GraphPropertiesContainer.Overlap"/> must be set</summary>
        Spline, True = Spline,
        /// <summary>For <see cref="eLayoutEngine.Fdp"/> only</summary>
        Compound
    }

    /// <summary>
    /// See <see cref="https://graphviz.org/doc/info/attrs.html#a:overlap">here</see>
    /// Determines if and how node overlaps should be removed. Nodes are first enlarged using the
    /// <see cref="GraphPropertiesContainer.Sep"/> attribute. If <see cref="True"/> , overlaps are
    /// retained. If the value is <see cref="Scale"/>, overlaps are removed by uniformly scaling in x and y.
    /// If the value converts to <see cref="False"/>, and it is available, Prism, a proximity graph-based
    /// algorithm, is used to remove node overlaps. This can also be invoked explicitly with
    /// <see cref="Prism"/>.
    /// This technique starts with a small scaling up, controlled by the <see cref="GraphPropertiesContainer.OverlapScaling"/>
    /// attribute, which can remove a significant portion of the overlap. The prism option also accepts 
    /// an optional non-negative integer suffix. This can be used to control the number of attempts
    /// made at overlap removal. By default, <see cref="Prism"/>, use 1000 for <see cref="GraphPropertiesContainer.PrismValue"/>.
    /// Setting <see cref="GraphPropertiesContainer.PrismValue"/> to zero causes only the scaling phase to be run.
    /// If Prism is not available, or the version of Graphviz is earlier than 2.28, <see cref="False"/>
    /// uses a Voronoi-based technique. This can always be invoked explicitly with <see cref="Voronoi"/>.
    /// 
    /// If the value is <see cref="ScaleXY"/>, x and y are separately scaled to remove overlaps.
    /// 
    /// If the value is <see cref="Compress"/>, the layout will be scaled down as much as possible
    /// without introducing any overlaps, obviously assuming there are none to begin with.
    /// 
    /// 
    /// N.B.The remaining allowed values of overlap correspond to algorithms which, at present,
    /// can produce bad aspect ratios. In addition, we deprecate the use of the "ortho*" and "portho*".
    /// 
    /// If the value is <see cref="Vpsc"/>, overlap removal is done as a quadratic optimization to
    /// minimize node displacement while removing node overlaps.
    /// 
    /// If the value is <see cref="OrthoXY"/> or <see cref="OrthoYX"/>, overlaps are moved by optimizing 
    /// two constraint problems, one for the x axis and one for the y. The suffix indicates which axis
    /// is processed first. If the value is <see cref="Ortho"/>, the technique is similar to 
    /// <see cref="OrthoXY"/> except a heuristic is used to reduce the bias between the two passes.
    /// If the value is <see cref="Ortho_XY"/>, the technique is the same as <see cref="Ortho"/>, 
    /// except the roles of x and y are reversed. The values <see cref="POrtho"/>, <see cref="POrthoXY"/>,
    /// <see cref="POrthoYX"/>, and <see cref="Ortho_XY"/> are similar to the previous four,
    /// except only pseudo-orthogonal ordering is enforced.
    /// 
    /// If the layout is done by neato with mode="ipsep", then one can use overlap=<see cref="IpSep"/>.
    /// In this case, the overlap removal constraints are incorporated into the layout algorithm itself.
    /// N.B. At present, this only supports one level of clustering.
    /// 
    /// 
    /// Except for <see cref="eLayoutEngine.Fdp"/> and <see cref="eLayoutEngine.Sfdp"/>, the layouts
    /// assume overlap=<see cref="True"/> as the default. Fdp first uses a number of passes using a
    /// built-in, force-directed technique to try to remove overlaps. Thus, <see cref="eLayoutEngine.Fdp"/>
    /// accepts overlap with an integer prefix followed by a colon, specifying the number of tries.
    /// If there is no prefix, no initial tries will be performed. If there is nothing following a colon,
    /// none of the above methods will be attempted. By default, <see cref="eLayoutEngine.Fdp"/> uses
    /// <see cref="_9Prism"/>
    /// 
    /// By default, sfdp uses 0 for <see cref="GraphPropertiesContainer.PrismValue"/>.
    /// 
    /// Except for the <see cref="Voronoi"/> and <see cref="Prism"/> methods, all of these transforms
    /// preserve the orthogonal ordering of the original layout. That is, if the x coordinates of two
    /// nodes are originally the same, they will remain the same, and if the x coordinate of one node
    /// is originally less than the x coordinate of another, this relation will still hold in the
    /// transformed layout. The similar properties hold for the y coordinates. This is not quite true
    /// for the "porth*" cases. For these, orthogonal ordering is only preserved among nodes related
    /// by an edge.
    /// </summary>
    public enum eOverlap
    {
        /// <summary>This is the default for all except for <see cref="eLayoutEngine.Dot"/> and <see cref="eLayoutEngine.Fdp"/></summary>
        False,
        Prism,
        True,
        Scale,
        Voronoi,
        ScaleXY,
        Compress,
        Vpsc,
        Ortho, OrthoXY, OrthoYX,
        Ortho_XY,
        POrtho, POrthoXY, POrthoYX, Ortho_YX,
        IpSep,
        [Description("9:prism")]
        _9Prism // Must be serialized as "9:prism"
    }
}
