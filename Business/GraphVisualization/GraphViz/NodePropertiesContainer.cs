﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using TechnicalTools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class NodePropertiesContainer : PropertiesContainer
    {
        public string         Label           { get; set; }
        public string         ToolTip         { get; set; }
        /// <summary>This value is inherited in subgraph</summary>
        public eNodeLabelLoc? LabelLoc        { get; set; }
        public string         FontName        { get; set; }
        public float?         FontSize        { get; set; }
        public Color?         FillColor       { get; set; }
        public Color?         FillColor2      { get; set; }
        public decimal?       GradientAngle   { get; set; }
        public Color?         Color           { get; set; }
        public eNodeShape?    Shape           { get; set; }
        public bool           Invisible       { get; set; }
        public decimal?       MinWidth        { get; set; }
        public decimal?       MinHeight       { get; set; }

        /// <summary> Effective only for <see cref="eLayoutEngine.Neato"/> and <see cref="eLayoutEngine.Fdp"/> engines </summary>
        public PointF?        Pos             { get; set; }

        public NodePropertiesContainer(BaseGraph parent)
            : base(parent)
        {
        }

        protected string CurrentFontName         { get { return FontName ?? Parent?.Parent?.NodeDefaultProperties.CurrentFontName; }}
        protected bool   CurrentFontIsMonoSpaced { get { return MonoSpacedFontNames.Contains(CurrentFontName?.ToLowerInvariant()); } }

        /// <summary> Return true is all values are the default ones </summary>
        public override bool PropertiesHaveDefaultValues
        {
            get
            {
                return string.IsNullOrEmpty(Label)
                    && LabelLoc == null
                    && FontName == null
                    && FontSize == null
                    && FillColor == null
                    && Color == null
                    && !Invisible
                    && Shape == null
                    && MinWidth == null
                    && MinHeight == null
                    && Pos == null;
            }
        }
        protected override IEnumerable<(string attribute, string value)> GetPropertyValues(eLayoutEngine layout)
        {
            if (Pos != null)
                yield return ("pos", Pos.Value.X.ToStringInvariant() + "," + Pos.Value.Y.ToStringInvariant() + "!");
            if (Shape != null)
                yield return ("shape", Shape.ToString().ToLower());
            if (Invisible)
                yield return ("label", "");
            else if (Label != null)
            {
                var effectiveLabel = Label.Split(new[] { Environment.NewLine }, StringSplitOptions.None)
                                           // I do not know why but a space is needed to align text of each lines when they end with "\l"
                                          .Select(str => (Parent.LeftAlignLabelCurrent && 
                                                          CurrentFontIsMonoSpaced && 
                                                          str.StartsWith(" ") ? " " : "") 
                                                       + str)
                                          .Join(Environment.NewLine);
                // Without an ending "\l", the last line is not justified to left, 
                // Note laso that if there is only one newline or \l, there is no empty line displayed at the end of the label. 
                if (Parent.LeftAlignLabelCurrent && !effectiveLabel.EndsWith("\\n"))
                    effectiveLabel += Environment.NewLine;
                yield return ("label", effectiveLabel);
            }
            if (ToolTip != null)
                yield return ("tooltip", ToolTip);
            if (LabelLoc.HasValue)
                yield return ("labelloc", ((char)(byte)LabelLoc).ToStringInvariant());
            if (!string.IsNullOrEmpty(FontName))
                yield return ("fontname", FontName);
            if (FontSize != null)
                yield return ("fontsize", FontSize.ToStringInvariant());
            if (Invisible)
                yield return ("style", "invisible");
            else
            {
                if (Color.HasValue)
                    yield return ("color", Color.Value.ToHexString());
                if ((FillColor ?? FillColor2).HasValue)
                {
                    yield return ("style", "filled");
                    yield return ("fillcolor", (FillColor ?? FillColor2).Value.ToHexString() 
                                             + (FillColor2 != null ? ":" + FillColor2.Value.ToHexString() : ""));
                    if (GradientAngle.HasValue)
                        yield return ("gradientangle", GradientAngle.ToStringInvariant());
                }
            }
            if (MinWidth.HasValue)
                yield return ("width", MinWidth.ToStringInvariant());
            if (MinHeight.HasValue)
                yield return ("height", MinHeight.ToStringInvariant());
        }
    }

    /// <summary>
    /// Vertical placement of labels for nodes.
    /// </summary>
    public enum eNodeLabelLoc : byte
    {
        /// <summary> Default value</summary>
        Centered = (byte)'c',
        Bottom = (byte)'b',
        Top = (byte)'t',
    }

    /// <summary>
    /// See more here https://graphviz.org/doc/info/shapes.html
    /// </summary>
    public enum eNodeShape
    {
        Ellipse = 0, Oval = Ellipse, // synonyms
        Box = 1,     Rect = Box, Rectangle = Box, // synonyms
        None = 2,    Plaintext = None, // synonyms
        /// <summary>
        /// Similar to plaintext, except that it also enforces width = 0 height = 0 margin = 0,
        /// which guarantees that the actual size of the node is entirely determined by the label.
        /// This is useful, for example, when using HTML-like labels
        /// </summary>
        Plain = 3,
        /// <summary> Like Plai but with text underlinexd </summary>
        Underline = 4,
        Circle,
        DoubleCircle,
        Egg,
        /// <summary> Affected by the peripheries, width and height attributes </summary>
        Point,
        /// <summary> Make use of properties "sides", "skew" and "distortion" (respectively default to 4.0, 0.0, 0.0) </summary>
        Polygon,
        Triangle,
        Diamond,
        Trapezium,
        Parallelogram,
        /// <summary> Kid of a pentagon with long flat side at the bottom </summary>
        House, 
        Pentagon,
        Hexagon,
        Septagon,
        Octagon,
        DoubleOctagon,
        TripleOctagon,
        InvTriangle,
        InvTrapezium,
        InvHouse,
        MDiamond,
        MSquare,
        MCircle,
        Square,
        Star,
        Cylinder,
        Note,
        Tab,
        Folder,
        Box3d,
        Component,
        Promoter,
        Cds,
        Terminator,
        Utr,
        Primersite,
        Restrictionsite,
        Fivepoverhang,
        Threepoverhang,
        NOverhang,
        Assembly,
        Signature,
        Insulator,
        Ribosite,
        RNastab,
        Proteasesite,
        Proteinstab,
        RPromoter,
        RArrow,
        LArrow,
        LPromoter,
    }
}
