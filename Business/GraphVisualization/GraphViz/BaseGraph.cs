﻿using System;
using System.Collections.Generic;
using System.Text;

using TechnicalTools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public abstract class BaseGraph : GraphPropertiesContainer
    {
        public BaseGraph RootGraph         { get { return Parent == null ? this : Parent.RootGraph; } }
        public int       Id                { get; }
        public string    GraphVizGraphName { get { return _GraphVizNodeName ?? "n" + Id.ToStringInvariant(); } set { _GraphVizNodeName = value; } } string _GraphVizNodeName;


        public IReadOnlyList<Node>     Nodes     { get { return _nodes; } } readonly List<Node> _nodes = new List<Node>();
        public IReadOnlyList<Edge>     Edges     { get { return _edges; } } readonly List<Edge> _edges = new List<Edge>();
        public IReadOnlyList<SubGraph> SubGraphs { get { return _subGraphs; } } readonly List<SubGraph> _subGraphs = new List<SubGraph>();

        public NodeDefaultProperties  NodeDefaultProperties  { get; protected set; }
        public EdgeDefaultProperties  EdgeDefaultProperties  { get; protected set; }


        public string                 Comment              { get; set; }
        public string                 EdgeLabelHeadPadding { get { return _EdgeLabelHeadPadding ?? Parent?.EdgeLabelHeadPadding ?? "    "; } set { _EdgeLabelHeadPadding = value; } } string _EdgeLabelHeadPadding;
        public string                 EdgeLabelTailPadding { get { return _EdgeLabelTailPadding ?? Parent?.EdgeLabelTailPadding ?? "    "; } set { _EdgeLabelTailPadding = value; } } string _EdgeLabelTailPadding;
        public List<string>           OtherStatements      { get; } = new List<string>();

        protected BaseGraph(BaseGraph parent)
            : base(parent)
        {
            if (parent == null && !(this is Graph))
                throw new ArgumentNullException(nameof(SubGraph) + " needs a parent!", nameof(parent));
            Id = System.Threading.Interlocked.Increment(ref RootGraph._subGraphsIdSeed);
            NodeDefaultProperties = new NodeDefaultProperties(this);
            EdgeDefaultProperties = new EdgeDefaultProperties(this);
        }
        internal int _nodeIdSeed = 0;
        internal int _edgeIdSeed = 0;
        internal int _subGraphsIdSeed = 0;


        public Node NewNode()
        {
            var n = CreateNode(this);
            _nodes.Add(n);
            return n;
        }
        protected virtual Node CreateNode(BaseGraph parent)
        {
            return new Node(parent);
        }
        public virtual void Remove(Node n)
        {
            foreach (var e in _edges)
                if (e.From == n || e.To == n)
                    Remove(e);
            _nodes.Remove(n);
        }

        public Edge NewEdge(Node from, Node to)
        {
            var e = CreateEdge(this, from, to);
            _edges.Add(e);
            return e;
        }
        protected virtual Edge CreateEdge(BaseGraph owner, Node from, Node to)
        {
            return new Edge(owner, from, to);
        }
        public virtual void Remove(Edge e)
        {
            _edges.Remove(e);
        }


        public SubGraph NewSubGraph()
        {
            var g = CreateSubGraph(this);
            _subGraphs.Add(g);
            return g;
        }
        protected virtual SubGraph CreateSubGraph(BaseGraph owner)
        {
            return new SubGraph(owner);
        }
        public virtual void Remove(SubGraph g)
        {
            _subGraphs.Remove(g);
        }


        bool IsSubGraph(SubGraph g)
        {
            if (g == null)
                throw new ArgumentNullException(nameof(g));
            if (g == this)
                return false;
            var bg = g.Parent;
            while (bg != null && bg != this)
                bg = bg.Parent;
            return bg != null;
        }

        bool IsSubNode(Node n)
        {
            return n != null 
                && n.Parent is SubGraph sg 
                && IsSubGraph(sg);
        }

        #region Writing out to dot language

        protected int    IndentationLevel { get; set; }
        protected string IndentationElement = "\t";
        protected internal string Indentation
        {
            get
            {
                var indent = string.Empty;
                for (int i = 0; i < IndentationLevel; ++i)
                    indent += IndentationElement;
                return indent;
            }
        }

        /// <summary>
        /// Write the whole description in dot format.
        /// Basically it write the wrapper "graph { ... }" and call ToDotContent for the content (the 3 dots)
        /// </summary>
        /// <param name="sb"></param>
        public virtual void ToDot(eLayoutEngine layout, StringBuilder sb)
        {
            if (!string.IsNullOrWhiteSpace(Comment))
                foreach (var line in Comment.Split(new[] { Environment.NewLine }, StringSplitOptions.None))
                    sb.AppendLine(Indentation + "// " + line);
        }
        /// <summary>
        /// Write the contnet between { and }
        /// </summary>
        /// <param name="sb"></param>
        public virtual void ToDotContent(eLayoutEngine layout, StringBuilder sb)
        {
            if (!PropertiesHaveDefaultValues)
            {
                sb.Append(Indentation + "graph ");
                base.Write(layout, sb);
                sb.AppendLine(";");
            }
            if (!NodeDefaultProperties.PropertiesHaveDefaultValues)
                NodeDefaultProperties.Write(layout, sb);
            if (!EdgeDefaultProperties.PropertiesHaveDefaultValues)
                EdgeDefaultProperties.Write(layout, sb);

            if (Nodes.Count > 0)
            {
                sb.AppendLine();
                sb.AppendLine();
                foreach (var node in Nodes)
                    if (!IsSubNode(node))
                        node.Write(layout, sb);
            }

            if (SubGraphs.Count > 0)
            {
                sb.AppendLine();
                foreach (var sg in SubGraphs)
                {
                    sb.AppendLine();
                    sg.IndentationLevel = IndentationLevel;
                    sg.ToDot(layout, sb);
                }
            }

            if (Edges.Count > 0)
            {
                sb.AppendLine();
                sb.AppendLine();
                foreach (var e in Edges)
                    e.Write(layout, sb);
            }

            if (OtherStatements.Count > 0)
            {
                sb.AppendLine();
                sb.AppendLine();
                foreach (var otherStatement in OtherStatements)
                    if (!string.IsNullOrWhiteSpace(otherStatement))
                        sb.AppendLine(Indentation + otherStatement);
            }
        }

        #endregion Writing out to dot language
    }
}
