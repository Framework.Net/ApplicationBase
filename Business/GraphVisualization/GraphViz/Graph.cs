﻿using System;
using System.Text;
using ApplicationBase.Common;

using TechnicalTools;
using TechnicalTools.Model;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    /// <summary>
    /// see https://edotor.net/ to debug and use it with result of this.ToDot()
    /// </summary>
    public class Graph : BaseGraph
    {
        public eLayoutEngine LayoutEngine { get; set; } = eLayoutEngine.Dot;
        public static bool DisplayWarning { get; set; } = true;

        public bool IsDirectedGraph { get; set; } = true;

        public Graph()
            : base(null)
        {
            GraphVizGraphName = "g";
        }

        #region Writing out to dot language

        public string ToDot(eLayoutEngine? layout = null)
        {
            var sb = new StringBuilder();
            ToDot(layout ?? LayoutEngine, sb);
            return sb.ToString();
        }
        public override void ToDot(eLayoutEngine layout, StringBuilder sb)
        {
            base.ToDot(layout, sb);
            sb.AppendLine((IsDirectedGraph ? "di" : "") + "graph " + GraphVizGraphName);
            sb.AppendLine("{");
            ++IndentationLevel;
            ToDotContent(layout, sb);
            --IndentationLevel;
            sb.AppendLine("}");
        }
        public static bool CanRender { get { return new GraphVizGenerator().IsGraphVizInstalled; } }
        public static void CheckCanRender()
        {
            if (!CanRender)
                throw new UserUnderstandableException($"Graphviz has not been shipped with this release of {BootstrapConfig.Instance.ApplicationName}", null);
        }

        #endregion Writing out to dot language

        public string ToSvg(eLayoutEngine? layout = null, bool preserveXmlDeclaration = false)
        {
            CheckCanRender();

            layout = layout ?? LayoutEngine;
            var dot = ToDot(layout);
            var generator = new GraphVizGenerator();
            string svg;
            try
            {
                svg = generator.Run(layout.Value, eOutputType.svg, dot);
            }
            catch (Exception ex) when (ex.EnrichDiagnosticWithAndReturnFalse("An error occured while rendering graph! Ask support to fix it!", eExceptionEnrichmentType.UserUnderstandable, true, true))
            {
                throw; // Unreachable code!
            }

            if (!preserveXmlDeclaration)
            {
                int n = 3;
                for (int i = 0; i < svg.Length; ++i)
                    if (svg[i] == '\n' && --n == 0)
                        return svg.Substring(i + 1);
            }
            return svg;
        }
    }
}
