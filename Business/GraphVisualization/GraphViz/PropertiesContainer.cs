﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public abstract class PropertiesContainer
    {
        public BaseGraph Parent { get; }

        public PropertiesContainer(BaseGraph parent)
        {
            Parent = parent;
        }

        /// <summary> Return true is all values are the default ones </summary>
        public abstract bool PropertiesHaveDefaultValues { get; }

        protected internal virtual string EscapeRawLabel(string rawLabel)
        {
            return Parent.EscapeRawLabel(rawLabel);
        }
        public virtual void Write(eLayoutEngine layout, StringBuilder sb)
        {
            sb.Append("[");
            string comma = string.Empty;
            foreach (var keyValuePair in GetPropertyValues(layout))
            {
                sb.Append(comma);
                comma = ", ";
                sb.Append(keyValuePair.attribute);
                sb.Append("=\"");
                sb.Append(EscapeRawLabel(keyValuePair.value));
                sb.Append("\"");
            }
            sb.Append("]");
        }

        protected abstract IEnumerable<(string attribute, string value)> GetPropertyValues(eLayoutEngine layout);

        // from https://en.wikipedia.org/wiki/List_of_monospaced_typefaces
        // Font names are lower cased
        protected HashSet<string> MonoSpacedFontNames { get; } = new string[]
        {
            "Courier", "Courier New",
            "Monospace", "Monospace (Unicode)", // not sure about this last one
            "Andalé Mono", "Andale Mono",
            "Cascadia Code",
            "Consolas",
            "Cutive Mono",
            "DejaVu Sans Mono",
            "Droid Sans Mono",
            "Everson Mono",
            "Fira Mono",
            "Fixed",
            "Fixedsys",
            "FreeMono",
            "Go Mono",
            "HyperFont",
            "IBM Plex Mono",
            "Inconsolata",
            "Iosevka",
            "Letter Gothic",
            "Liberation Mono",
            "Lucida Console",
            "Menlo",
            "Monaco",
            "Monofur",
            "Nimbus Mono L",
            "Noto Mono",
            "OCR-A",
            "OCR-B",
            "Overpass Mono",
            "Oxygen Mono",
            "PragmataPro",
            "Prestige Elite",
            "ProFont",
            "PT Mono",
            "Roboto Mono",
            "Source Code Pro",
            "Terminus",
            "Tex Gyre Cursor",
            "UM Typewriter"
        }.Select(fontname => fontname.ToLowerInvariant()).ToHashSet();
    }
}
