﻿using System;
using System.Text;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class Edge : EdgePropertiesContainer
    {
        public int       Id              { get; }

        public Node      From            { get; set; }
        public Node      To              { get; set; }

        public string    Comment         { get; set; }

        public Edge(BaseGraph parent, Node from, Node to)
            : base(parent)
        {
            Id = System.Threading.Interlocked.Increment(ref parent.RootGraph._edgeIdSeed);
            From = from;
            To = to;
        }

        public void Remove() { Parent.Remove(this); }

        public override void Write(eLayoutEngine layout, StringBuilder sb)
        {
            var hasComment = !string.IsNullOrWhiteSpace(Comment);
            var commentIsMultiline = hasComment && Comment.Contains("\n");
            if (hasComment && commentIsMultiline)
                foreach (var line in Comment.Split(new[] { Environment.NewLine }, StringSplitOptions.None))
                    sb.AppendLine(Parent.Indentation + "// " + line);

            sb.Append(Parent.Indentation + From.GraphVizNodeName + " -> " + To.GraphVizNodeName);
            sb.Append(" ");
            base.Write(layout, sb);
            sb.Append(";");

            if (hasComment && !commentIsMultiline)
                sb.Append(" // " + Comment);
            sb.AppendLine();
        }
    }
}
