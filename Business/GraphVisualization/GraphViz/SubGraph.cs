﻿using System;
using System.Text;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class SubGraph : BaseGraph
    {
        public SubGraph(BaseGraph owner)
             : base(owner)
        {
            GraphVizGraphName = "cluster_" + GraphVizGraphName;
        }

        #region Writing out to dot language

        public override void ToDot(eLayoutEngine layout, StringBuilder sb)
        {
            base.ToDot(layout, sb);
            sb.AppendLine(Indentation + "subgraph " + GraphVizGraphName);
            sb.AppendLine(Indentation + "{");
            ++IndentationLevel;
            ToDotContent(layout, sb);
            --IndentationLevel;
            sb.AppendLine(Indentation + "}");
        }

        #endregion Writing out to dot language
    }
}
