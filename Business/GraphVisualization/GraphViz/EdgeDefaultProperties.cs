﻿using System;
using System.Text;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class EdgeDefaultProperties : EdgePropertiesContainer
    {
        public EdgeDefaultProperties(BaseGraph parent)
            : base(parent)
        {
        }

        public override void Write(eLayoutEngine layout, StringBuilder sb)
        {
            if (PropertiesHaveDefaultValues)
                return;
            sb.Append(Parent.Indentation + "edge ");
            base.Write(layout, sb);
            sb.AppendLine(";");
        }
    }
}
