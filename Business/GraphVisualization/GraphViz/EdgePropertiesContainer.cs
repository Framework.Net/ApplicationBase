﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;

using TechnicalTools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class EdgePropertiesContainer : PropertiesContainer
    {
        public string        Label            { get; set; }
        public string        ToolTip          { get; set; }
        public string        HeadLabel        { get; set; }
        public string        TailLabel        { get; set; }
        public Color?        Color            { get; set; }
        public bool          Invisible        { get; set; }
        public string        HeadPortName     { get; set; }
        public eCompassPort? HeadPortCompass  { get; set; }
        public string        TailPortName     { get; set; }
        public eCompassPort? TailPortCompass  { get; set; }
        public eArrowType?   HeadArrow        { get; set; }
        public eArrowType?   TailArrow        { get; set; }
        /// <summary> Default value is 1. Minimal value 0 for dot & twopi and 1 for neato & fdp. Dot and twopi accept only an integer value </summary>
        public decimal?      Weight           { get; set; }

        public EdgePropertiesContainer(BaseGraph parent)
            : base(parent)
        {
        }

        /// <summary> Return true is all values are the default ones </summary>
        public override bool PropertiesHaveDefaultValues
        {
            get
            {
                return Label == null
                    && ToolTip == null
                    && HeadLabel == null
                    && TailLabel == null
                    && Color == null
                    && !Invisible
                    && HeadPortName == null
                    && HeadPortCompass == null
                    && TailPortName == null
                    && TailPortCompass == null
                    && HeadArrow == null
                    && TailArrow == null
                    && Weight == null;
            }
        }

        protected override IEnumerable<(string attribute, string value)> GetPropertyValues(eLayoutEngine layout)
        {
            if (Label != null)
                yield return ("label", Label);
            if (ToolTip != null)
                yield return ("tooltip", ToolTip);
            if (HeadLabel != null)
                yield return ("headlabel", HeadLabel + Parent.EdgeLabelHeadPadding);
            if (TailLabel != null)
                yield return ("taillabel", Parent.EdgeLabelTailPadding + TailLabel);
            if (Color.HasValue)
                yield return ("color", Color.Value.ToHexString());
            if (Invisible)
            {
                yield return ("style", "invisible");
                yield return ("arrowhead", "no");
            }
            if (HeadPortName != null || HeadPortCompass.HasValue)
                yield return ("headport", new[] { HeadPortName, HeadPortCompass.ToStringInvariant().ToLowerInvariant() }.NotBlank().Join(":"));
            if (TailPortName != null || TailPortCompass.HasValue)
                yield return ("tailport", new[] { TailPortName, TailPortCompass.ToStringInvariant().ToLowerInvariant() }.NotBlank().Join(":"));
            if (HeadArrow.HasValue && !Invisible)
                yield return ("arrowhead", HeadArrow.ToStringInvariant().ToLowerInvariant());
            if (TailArrow.HasValue && !Invisible)
            {
                yield return ("arrowtail", TailArrow.ToStringInvariant().ToLowerInvariant());
                yield return ("dir", "both"); // otherwise the tail end is not displayed
            }
            if (Weight.HasValue)
                yield return ("weight", ((double)Weight).ToStringInvariant());
        }
    }

    public enum eCompassPort
    {
        /// <summary>
        /// Specifies that an appropriate side of the port adjacent to the exterior of the node should be used, 
        /// if such exists. Otherwise, the center is used.
        /// </summary>
        [Description("Center")]
        _,
        /// <summary>
        /// The edge is aimed towards the center of the node, and then clipped at the node boundary
        /// </summary>
        [Description("Center")]
        C,
        [Description("North")]
        N,
        [Description("North-East")]
        NE,
        [Description("East")]
        E,
        [Description("South-East")]
        SE,
        [Description("South")]
        S,
        [Description("South-West")]
        SW,
        [Description("West")]
        W,
        [Description("North-West")]
        NW,
    }

    /// <summary>
    /// https://graphviz.org/doc/info/attrs.html#k:arrowType
    /// </summary>
    public enum eArrowType
    {
        normal,		inv,
        dot,		invdot,	
        odot,		invodot,	
        none,		tee,	
        empty,		invempty,	
        diamond,	odiamond,	
        ediamond,	crow,	
        box,		obox,	
        open,		halfopen,	
        vee,	
    }

}
