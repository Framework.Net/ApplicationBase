﻿using System;
using System.Text;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class NodeDefaultProperties : NodePropertiesContainer
    {
        public NodeDefaultProperties(BaseGraph parent)
            : base(parent)
        {
        }

        public override void Write(eLayoutEngine layout, StringBuilder sb)
        {
            if (PropertiesHaveDefaultValues)
                return;
            sb.Append(Parent.Indentation + "node ");
            base.Write(layout, sb);
            sb.AppendLine(";");
        }
    }
}
