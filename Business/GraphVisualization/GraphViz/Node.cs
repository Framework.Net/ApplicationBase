﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TechnicalTools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class Node : NodePropertiesContainer
    {
        public int       Id               { get; }
        public string    GraphVizNodeName { get { return _GraphVizNodeName ?? "n" + Id.ToStringInvariant(); } set { _GraphVizNodeName = value; } } string _GraphVizNodeName;

        public string    Comment          { get; set; }

        /// <summary> Return edges where this is the source of edge.</summary>
        public IEnumerable<Edge> EdgesStarting { get { return Parent.Edges.Where(e => e.From == this); } }
        /// <summary> Return edges where this is the source of edge.</summary>
        public IEnumerable<Edge> EdgesArriving { get { return Parent.Edges.Where(e => e.To == this); } }
        /// <summary> Return nodes targeted by this node, though edges.</summary>
        public IEnumerable<Node> NodesTargeting { get { return Parent.Edges.Where(e => e.From == this).Select(e => e.To); } }
        /// <summary> Return nodes pointing to "this".</summary>
        public IEnumerable<Node> NodesSourcing { get { return Parent.Edges.Where(e => e.To == this).Select(e => e.From); } }

        public Node(BaseGraph parent)
            : base(parent)
        {
            Id = System.Threading.Interlocked.Increment(ref parent.RootGraph._nodeIdSeed);
        }

        public void Remove() { Parent.Remove(this); }

        public override void Write(eLayoutEngine layout, StringBuilder sb)
        {
            var hasComment = !string.IsNullOrWhiteSpace(Comment);
            var commentIsMultiline = hasComment && Comment.Contains("\n");
            if (hasComment && commentIsMultiline)
                foreach (var line in Comment.Split(new[] { Environment.NewLine }, StringSplitOptions.None))
                    sb.AppendLine(Parent.Indentation + "// " + line);

            sb.Append(Parent.Indentation + GraphVizNodeName);
            sb.Append(" ");
            base.Write(layout, sb);
            sb.Append(";");

            if (hasComment && !commentIsMultiline)
                sb.Append(" // " + Comment);
            sb.AppendLine();
        }
    }
}
