using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

using TechnicalTools;
using ApplicationBase.Deployment.Data;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.Tools;

using DataMapper;

using ApplicationBase.DAL;
using ApplicationBase.DAL.Users;
using ApplicationBase.Common;


namespace ApplicationBase.Business
{
    public class AuthenticationManager : IUserInteractiveObject
    {
        protected internal AuthenticationManager(EnvironmentConfig env)
        {
            _env = env;
        }
        protected readonly EnvironmentConfig _env;
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(AuthenticationManager));

        /// <summary>
        /// Give the newConnectionString when authentic_envated
        /// </summary>
        internal event Action<List<EnvironmentSetting>> Authenticated;
        public event Action CurrentUserChanged;


        public Login NoRightLogin { get; } = new Login() { Person_Id = 0, Identifiant = "default login" };

        // When impersonation is used, this property allows to get the real user not the simulated one
        public Login CurrentUserReal
        {
            get
            {
                if (_CurrentUserReal != null)
                    return _CurrentUserReal;
                if (!CurrentUserReal_Id.HasValue)
                    return NoRightLogin;
                _CurrentUserReal = DB.Dao_Base.GetObjectWithId<Login>(CurrentUserReal_Id.Value);
                return _CurrentUserReal;
            }
        }
        Login _CurrentUserReal; // Cache
        long? CurrentUserReal_Id { get { return _CurrentUserReal_Id; } set { _CurrentUserReal_Id = value; _CurrentUserReal = null; } } long? _CurrentUserReal_Id;

        // When impersonation is used, this property represent the impersonated user or else the real user
        public Login CurrentUser
        {
            get
            {
                if (_CurrentUser != null)
                    return _CurrentUser;
                if (!CurrentUser_Id.HasValue)
                    return CurrentUserReal;
                _CurrentUser = DB.Dao_Base.GetObjectWithId<Login>(CurrentUser_Id.Value);
                return _CurrentUser;
            }
        }
        Login _CurrentUser; // Cache
        long? CurrentUser_Id { get { return _CurrentUser_Id; } set { _CurrentUser_Id = value; _CurrentUser = null; } } long? _CurrentUser_Id;

        public static string LastLogin
        {
            get
            {
                try
                {
                    return Settings.Default.LastLogin; // "LastLogin" doit être mis dans Properties/Settings !
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
                {
                    value = value?.Trim();
                    // First we try to prevent write access on application file settings 
                    // because it can lead (rarely though) to a race condition on file access 
                    // when multiple instances of same application is run.
                    // (See https://stackoverflow.com/questions/6436157/configuration-system-failed-to-initialize)
                    if (Settings.Default.LastLogin == value) 
                        return;
                    Settings.Default.LastLogin = value;
                    Settings.Default.Save();
                });
            }
        }

        public virtual void Authenticate(string user_login, string user_password, bool allowAutoLogin = false)
        {
            Tuple<long, List<EnvironmentSetting>> loginIdAndSettings;
            Login login = null;
            if (_env.Domain == eEnvironment.LocalNoDB)
            {
                loginIdAndSettings = Tuple.Create(0L, new List<EnvironmentSetting>() { new EnvironmentSetting()
                {
                    // This Key will force good instance type for Application.Business.Config.Load : DeserializerWithDependencyInversion.AssignKeyValuePairs
                    FullKey = nameof(Config.Connections) + "." + nameof(Config.Connections.ConnectionString_Base),
                    Value = string.Empty
                } });
                _CurrentUserReal = login = new Login() { Identifiant = user_login, IsEnabled = true };
            }
            else if (!string.IsNullOrWhiteSpace(user_password) || !allowAutoLogin)
            {
                login = new Login() { Identifiant = user_login }.WithPassword(user_password);
                loginIdAndSettings = DAO.Authenticate(Config.Instance.BaseConnections.ConnectionString_Base, login, _env);
            }
            else
            {
                loginIdAndSettings = ExceptionManager.Instance.OnExceptionEnrichWith(eExceptionEnrichmentType.Technical,
                                     "For AutoLogin feature, make sure you added the argument InitialConnectionString with a connection that has already right to select on all table",
                    () =>
                    {
                        string adminConnectionString = Config.Instance.BaseConnections.ConnectionString_Base;
                        var mapper = DbMapperFactory.CreateSqlMapper(new NamedObject("Connection For Authentication Auto"), adminConnectionString);
                        login = mapper.LoadCollection<Login>(mapper.GetColumnNameFor((Login l) => l.Identifiant) + " = " + DbMappedField.ToSql(user_login)).FirstOrDefault();
                        if (login == null)
                            throw new UserUnderstandableException($"Login {user_login} does not exist in database!", null);

                        var temp = DbMapperFactory.CreateSqlMapper(null, adminConnectionString, mapper.AsDisconnected, null);
                        var settings = temp.EnumerateCollectionFromSqlQuery<EnvironmentSetting>($"select * from [Security].[FlattenizedEnvironmentSettingsEnriched]({_env.Id.ToStringInvariant()}, '{nameof(Config.Connections) + "." + nameof(Common.Config.ConnectionString_Base)}', 0, " + login.Person_Id.ToStringInvariant() + ")").ToList();
                        return Tuple.Create(login.Person_Id, settings);
                    });
            }
            SetLogin(user_login, loginIdAndSettings, login);
        }

        protected void SetLogin(string user_login, Tuple<long, List<EnvironmentSetting>> loginIdAndSettings, Login newLogin)
        {
            // This line is here instead of in the beginning of the method (which would be better for end-user)
            // to know if the logged user is a service or not (before authentication is done we cannot know)
            // So when the user is a service we do NOT remember the LastLogin because it would modify Application setting file
            // Because when application is run multiple times in parallel, modifying application setting file 
            // can lead to a race condition when accessing this file
            if (!newLogin.IsService)
                LastLogin = user_login;

            _CurrentUserReal_Id = loginIdAndSettings.Item1;
            CurrentUser_Id = null;

            Authenticated?.Invoke(loginIdAndSettings.Item2);
            var appName = Assembly.GetEntryAssembly().GetName().Name;
            _log.Info($"User {CurrentUserReal.Identifiant} is now authenticated as {CurrentUser.Identifiant} on {appName} " + ApplicationBase.Deployment.Deployer.GetCurrentVersion().ToString());
            CurrentUserChanged?.Invoke();
        }

        void CheckForDisconnectedMode()
        {
            if (_env.Domain == eEnvironment.LocalNoDB)
                throw new UserUnderstandableException("This feature is unavailable in disconnected mode!", null);
        }

        public void Impersonate(string user_login)
        {
            var login = DB.Dao_Base.LoadCollection<Login>(DB.Dao_Base.GetColumnNameFor((Login log) => log.Identifiant) + " = " + DbMappedField.ToSql(user_login)).SingleOrDefault();
                
            if (login == null)
                throw new UserUnderstandableException($"Login \"{user_login}\" does not match any existing user!", null);
            Impersonate(login);
        }

        public void Impersonate(Login login)
        {
            Debug.Assert(login != null);
            if (!GetAccessFor(Feature.CanImpersonateLogins, CurrentUserReal).HasFlag(eFeatureAccess.CanUse))
                throw new BusinessException($"{CurrentUserReal.Identifiant} is not allowed to impersonate!", null);

            if (!_CurrentUserReal_Id.HasValue)
                throw new TechnicalException("A real authentication is first needed before impersonation is allowed!", null);

            CurrentUser_Id = login.Person_Id;
            var appName = Assembly.GetEntryAssembly().GetName().Name;
            _log.Info($"User {CurrentUserReal.Identifiant} is impersonated as {CurrentUser.Identifiant} on {appName} " + ApplicationBase.Deployment.Deployer.GetCurrentVersion().ToString());
            CurrentUserChanged?.Invoke();
        }

        public Login CreateUser(string firstname, string lastname, string loginName, string password)
        {
            firstname = firstname?.Trim();
            lastname = lastname?.Trim();
            var person = new Person() { Firstname = firstname, Lastname = lastname };

            if (string.IsNullOrEmpty(firstname))
                throw new ArgumentException("Firstname must be set", nameof(firstname));
            if (string.IsNullOrEmpty(lastname))
                throw new ArgumentException("Lastname must be set", nameof(lastname));

            if (loginName != loginName.Trim())
                throw new ArgumentException("Login must be clean and not containing spaces at begining of ends", nameof(loginName));
            var login = new Login() { Identifiant = loginName }.WithPassword(password?.IfBlankUse(null)); // make other validations check

            return DB.Dao_Base.CreateUser(person, login);
        }

        public List<Profile> GetCurrentUserProfiles()
        {
            var profileIds = DB.Dao_Base.LoadCollection<Login_Profile>(DB.Dao_Base.GetColumnNameFor((Login_Profile lp) => lp.Login_Id) + " = " + CurrentUser.Person_Id.ToStringInvariant())
                                        .Select(lp => lp.Profile_Id)
                                        .Distinct()
                                        .ToList();
            return DB.Dao_Base.GetObjectsWithIds<Profile>(profileIds);
        }
        public ILookup<Profile, Login> GetUsersByProfiles(IEnumerable<Login> logins = null)
        {
            if (_env.Domain == eEnvironment.LocalNoDB)
                return Profile.NoProfile.Yield().ToLookup(p => p, _ => CurrentUser);
            var profilesById = DB.Dao_Base.LoadCollection<Profile>().ToDictionary(p => p.Id);
            profilesById.Add(Profile.NoProfile.Id, Profile.NoProfile );
            var loginsById = (logins ?? AllLogins().Keys).ToDictionary(p => p.Person_Id);
            var links = DB.Dao_Base.LoadCollection<Login_Profile>();
            links.AddRange(loginsById.Keys.Except(links.Select(lp => lp.Login_Id))
                                          .Select(loginId => new Login_Profile() { Login_Id = loginId, Profile_Id = Profile.NoProfile.Id }));
            var loginsByProfiles = links.Where(lp => loginsById.ContainsKey(lp.Login_Id))
                                        .ToLookup(lp => profilesById[lp.Profile_Id], lp => loginsById[lp.Login_Id]);
            return loginsByProfiles;
        }
        public Profile CreateProfile(string profileName)
        {
            if (_env.Domain == eEnvironment.LocalNoDB)
                throw new ArgumentException("This feature is unavailable in disconected mode!");
            if (string.IsNullOrWhiteSpace(profileName))
                throw new ArgumentException("The name must be set", nameof(profileName));
            var profile = new Profile { Name = profileName };
            return DB.Dao_Base.CreateProfile(profile);
        }
        
        public eFeatureAccess GetAccessFor(Feature feature)
        {
            return GetAccessFor(feature, CurrentUser);
        }

        public eFeatureAccess GetAccessFor(Feature feature, Login login)
        {
            if (login == NoRightLogin && feature == Feature.CanImpersonateLogins)
                return Debugger.IsAttached ? eFeatureAccess.CanUse : eFeatureAccess.None;

            if (login == null || !login.IsEnabled)
                return eFeatureAccess.None; // this is obvious

            if (login.IsAdmin || feature == Feature.AllowEverybody || _env.Domain == eEnvironment.LocalNoDB)
                return eFeatureAccess.CanSee | eFeatureAccess.CanEdit | eFeatureAccess.CanUse;

            // If feature is null and the value si from a property of "Feature" class it means this feature has not been matched against feature name in DB.
            // Usually, developpers should automaticaly know about this when they run the application in Visual studio (an assertion fails)
            // So by default, if someone changes any feature name in DB without knowing about this strict mapping, the functionality is not accessible (for security reason)
            // We dont rely on ID to do the mapping
            if (feature == null)
                return eFeatureAccess.None;

            var accesses = _accesses.GetOrAdd(login, GetAllAccessFor);
            return accesses.ContainsKey(feature) ? accesses[feature] : eFeatureAccess.None;
        }
        readonly ConcurrentDictionary<Login, Dictionary<Feature, eFeatureAccess>> _accesses = new ConcurrentDictionary<Login, Dictionary<Feature, eFeatureAccess>>();
        Dictionary<Feature, eFeatureAccess> GetAllAccessFor(IFeatureOwner login)
        {
            // Etant donné la petite quantité de donné, on fait les jointures dans le code plutot qu'en SQL
            var P = DB.Dao_Base.LoadCollection<Profile>();
            var F = Feature.All.ToList();
            var LP = DB.Dao_Base.LoadCollection<Login_Profile>();
            var PF = DB.Dao_Base.LoadCollection<Profile_Feature>();
            var LF = DB.Dao_Base.LoadCollection<Login_Feature>();
            return GetAllAccessFor(login, P, F, LP, PF, LF)
                    .ToDictionary(kvp => kvp.Key, kvp => kvp.Value.CumulatedAccess);
        }
        // Etant donné la petite quantité de donné, on fait les jointures dans le code plutot qu'en SQL
        Dictionary<Feature, DetailedAccess> GetAllAccessFor(IFeatureOwner owner, List<Profile> P, List<Feature> F, List<Login_Profile> LP, List<Profile_Feature> PF, List<Login_Feature> LF)
        {
            // Récupère les profils attachés au login
            List<Profile> assignedProfiles;

            if (owner is Login)
            {
                assignedProfiles = LP.Where(lp => lp.Login_Id == owner.Id)
                    .Join(P, lp => lp.Profile_Id, p => p.Id, (lp, p) => p)
                    .Concat(Profile.DirectRight)
                    .ToList();
            }
            else
                assignedProfiles = ((Profile) owner).WrapInList();

            List<Profile> allProfiles = assignedProfiles.ToList();
            // Ajoute les profils hérités des profils attachés (peu fréquent)
            foreach (var p in assignedProfiles.ToList())
            {
                var p2 = p;
                while (p2.InheritingProfile_Id.HasValue)
                {
                    p2 = P.First(p3 => p3.Id == p2.InheritingProfile_Id.Value);
                    if (!allProfiles.Contains(p2))
                        allProfiles.Add(p2);
                }
            }
            var inherited_features = allProfiles.Join(PF, p => p.Id, pf => pf.Profile_Id, (p, pf) => new { pf, p })
                                                .Join(F, pf => pf.pf.Feature_Id, f => f.Id, (pf, f) => new { Feature = f, pf.pf.Access, Profile = pf.p })
                                                .ToList();

            var assigned_features = owner.GetAssignedLinks(DB.Dao_Base)
                                     .Join(F, lf => lf.Feature_Id, f => f.Id, (lf, f) => new { Feature = f, lf.Access, Profile = Profile.DirectRight })
                                     .ToList();
            var all_features = inherited_features.Concat(assigned_features)
                                                 .GroupBy(fa => fa.Feature)
                                                 .ToDictionary(grp_fa => grp_fa.Key, 
                                                               grp_fa => new DetailedAccess(grp_fa.Key, 
                                                                                            grp_fa.Aggregate(eFeatureAccess.None, (a, fa) => a | fa.Access),
                                                                                            grp_fa.GroupBy(fa => fa.Profile).Where(grp => assignedProfiles.Contains(grp.Key))
                                                                                                  .ToDictionary(grp => grp.Key, grp => grp.Aggregate(eFeatureAccess.None, (a, fa) => a | fa.Access)),
                                                                                            grp_fa.GroupBy(fa => fa.Profile).Where(grp => !assignedProfiles.Contains(grp.Key))
                                                                                                  .ToDictionary(grp => grp.Key, grp => grp.Aggregate(eFeatureAccess.None, (a, fa) => a | fa.Access))));
            // In case assigned profile does not have any right other than from the inherited one
            // We have to give this in order to indicate this profile is assigned
            all_features.Add(Feature.AllowEverybody, new DetailedAccess(Feature.AllowEverybody, eFeatureAccess.CanSee | eFeatureAccess.CanEdit | eFeatureAccess.CanUse, new Dictionary<Profile, eFeatureAccess>(), new Dictionary<Profile, eFeatureAccess>()));
            return all_features;
        }

        /// <summary>
        /// Return all logins in alphabetical order except the current one which is in first top position
        /// </summary>
        public virtual Dictionary<Login, Person> AllLogins()
        {
            if (_env.Domain == eEnvironment.LocalNoDB)
                return _CurrentUserReal.Yield().ToDictionary(l => l, l => new Person() { Firstname = "You" });
            using (new MemoryHolder(DB.Dao_Base.LoadCollection<Person>()))
                return new[] { CurrentUserReal }.NotNull()
                        .Concat(DB.Dao_Base.LoadCollection<Login>()
                                           .Where(log => (CurrentUser == null || log.Identifiant != CurrentUser.Identifiant)
                                                      && (CurrentUserReal == null || log.Identifiant != CurrentUserReal.Identifiant)))
                        // CurrentUser could be different than CurrentUserReal if using impersonation
                        .Concat(CurrentUser == CurrentUserReal ? new Login[] { } : new[] { CurrentUser }.NotNull())
                        .ToDictionary(login => login, login => DB.Dao_Base.GetObjectWithId<Person>(login.Person_Id));
        }

        public void CheckFeatureAccess(Feature feature, eFeatureAccess right)
        {
            if (right == eFeatureAccess.None)
                return; // Yes ! We have the right to do nothing ;-)
            var access = GetAccessFor(feature);
            if (access.HasFlag(right))
                return;
            if (right == eFeatureAccess.CanSee)
                throw new UnauthorizedAccessException($"You are not allowed to see data related to feature \"{feature.Name}\"!");
            if (right == eFeatureAccess.CanEdit)
                throw new UnauthorizedAccessException($"You are not allowed to edit data related to feature \"{feature.Name}\"!");
            if (right == eFeatureAccess.CanUse)
                throw new UnauthorizedAccessException($"You are not allowed to use the feature \"{feature.Name}\"!");
            if (right == (eFeatureAccess.CanSee | eFeatureAccess.CanEdit))
                throw new UnauthorizedAccessException($"You are not allowed to see and edit data related to feature \"{feature.Name}\"!");
            
            // possible but rare
            throw new UnauthorizedAccessException($"You are not allowed to access the feature \"{feature.Name}\"!");
        }

        /// <summary>
        /// Give all rights in a dictionary (user or profiles) => rights in a detailed way
        /// </summary>
        /// <param name="forProfile">Indicate that the key of the report must be profile and not user</param>
        /// <param name="withTechnicalFeatures">Include technical rights in the reports</param>
        public Dictionary<IFeatureOwner, Dictionary<Feature, DetailedAccess>> GetAllRights(bool forProfile, bool withTechnicalFeatures)
        {
            // Etant donné la petite quantité de donné, on fait les jointures dans le code plutot qu'en SQL
            var P = DB.Dao_Base.LoadCollection<Profile>().Where(p => withTechnicalFeatures || p.Id > 0).ToList();
            var F = Feature.All.ToList();
            var LP = DB.Dao_Base.LoadCollection<Login_Profile>();
            var PF = DB.Dao_Base.LoadCollection<Profile_Feature>();
            var LF = DB.Dao_Base.LoadCollection<Login_Feature>();

            if (forProfile)
                return P.ToDictionary(profile => (IFeatureOwner)profile,
                                    profile => GetAllAccessFor(profile, P, F, LP, PF, LF));
            return DB.Dao_Base.LoadCollection<Login>()
                              .ToDictionary(login => (IFeatureOwner)login, 
                                            login => GetAllAccessFor(login, P, F, LP, PF, LF));
        }
        public class DetailedAccess// : Tuple<eFeatureAccess, Dictionary<Profile, eFeatureAccess>>
        {
            public Feature                             Feature                  { get; } 
            public eFeatureAccess                      CumulatedAccess          { get; }
            public Dictionary<Profile, eFeatureAccess> AssignedAccessByProfile  { get; }
            public Dictionary<Profile, eFeatureAccess> InheritedAccessByProfile { get; }

            public DetailedAccess(Feature feature, eFeatureAccess cumulatedAccess, Dictionary<Profile, eFeatureAccess> assignedAccessByProfile, Dictionary<Profile, eFeatureAccess> inheritedAccessByProfile) 
            {
                Feature = feature;
                CumulatedAccess = cumulatedAccess;
                AssignedAccessByProfile = assignedAccessByProfile;
                InheritedAccessByProfile = inheritedAccessByProfile;
            }
            public override string ToString() { return ToDxHtmlString(); }
            public string ToDxHtmlString()
            {
                if (_asHtmlString != null)
                    return _asHtmlString;
                var cumulatedColored = ToColoredAccess(CumulatedAccess);
                // See here : https://documentation.devexpress.com/#WindowsForms/CustomDocument4874
                _asHtmlString = "<b>" + cumulatedColored + " </b><br/>"
                                + "<nbsp><nbsp><nbsp><nbsp><i>* Details *</i><br/>"
                                + "<size=8>"
                                + AssignedAccessByProfile.Concat(InheritedAccessByProfile)
                                                         .Select(detail => ReferenceEquals(detail.Key, Profile.DirectRight)
                                                                         ? "<b><span style=\"color:red\">Direct user right : </span></b>" + detail.Value
                                                                         : "Profile \"" + detail.Key.Name + "\" : " + ToColoredAccess(detail.Value))
                                                         .Join("</br>")
                                + "</size>";
                return _asHtmlString;
            }
            string _asHtmlString;
            static string ToColoredAccess(eFeatureAccess access)
            {
                return (access.HasFlag(eFeatureAccess.CanSee) ? "<span style=\"color:green\">See</span>" : "")
                      + " "
                      + (access.HasFlag(eFeatureAccess.CanEdit) ? "<span style=\"color:orange\">Edit</span>" : "")
                      + " "
                      + (access.HasFlag(eFeatureAccess.CanUse) ? "<span style=\"color:blue\">Use</span>" : "");
            }

            public string ToCommonString(string padding = "")
            {
                return AssignedAccessByProfile.Concat(InheritedAccessByProfile)
                                             .Select(detail => detail.Key == Profile.DirectRight
                                                             ? "Direct user right : " + detail.Value
                                                             : "Profile \"" + detail.Key.Name + "\" : " + detail.Value)
                                             .Join(Environment.NewLine + padding);
            }
        }

        public string GetAllRightsAsString(bool withTechnicalFeatures)
        {
            var sb = new StringBuilder();
            foreach (var loginRights in GetAllRights(false, withTechnicalFeatures))
                if (loginRights.Key is Profile || ((Login)loginRights.Key).IsEnabled)
            {
                sb.AppendLine(loginRights.Key.Name + " : " );
                foreach (var featureRight in loginRights.Value)
                {
                    string featureName = "   " + featureRight.Key.Name.Replace("Data Access on ", "") + " : " + featureRight.Value.CumulatedAccess + "  (details: ";
                    sb.Append(featureName);
                    sb.Append(featureRight.Value.ToCommonString(new string(' ', featureName.Length)));
                    sb.AppendLine(")");
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }

        public virtual StringBuilder GenerateFileWithUserRightsAndProfiles(bool includeTechnicalRights)
        {
            var persons = DB.Dao_Base.LoadCollection<Person>().ToDictionary(p => p.Id);
            var profiles = DB.Dao_Base.LoadCollection<Profile>().ToDictionary(p => p.Id);
            var features = DB.Dao_Base.LoadCollection<Feature>().ToDictionary(p => p.Id);
            var login_profiles = DB.Dao_Base.LoadCollection<Login_Profile>().ToLookup(p => p.Login_Id);
            var login_features = DB.Dao_Base.LoadCollection<Login_Feature>().ToLookup(p => p.Login_Id);

            var userRights = GetAllRights(false, includeTechnicalRights);
            var sb = new StringBuilder();
            sb.Append('\uFEFF'); // U+FEFF is the byte-order mark character

            #region Titles

            sb.Append("Firstname");
            sb.Append("\t");
            sb.Append("Lastname");
            sb.Append("\t");
            sb.Append("Login");
            sb.Append("\t");
            sb.Append("Is Enabled");
            sb.Append("\t");
            sb.Append("Is Admin");
            sb.Append("\t");
            sb.Append("Last Login Date");
            sb.Append("\t");
            sb.Append("Creation Date");
            foreach (var profile in profiles.Values)
            {
                sb.Append("\t");
                sb.Append("Has profile: " + profile.Name);
            }
            foreach (var feature in features.Values)
                if (includeTechnicalRights || feature.Id > 0)
                {
                    sb.Append("\t");
                    sb.Append("Has Feature: " + feature.Name);
                }
            sb.AppendLine();
            #endregion

            foreach (var kvp in userRights)
            {
                var login = kvp.Key as Login;
                var person = persons[login.Person_Id];
                sb.Append(person.Firstname);
                sb.Append("\t");
                sb.Append(person.Lastname);
                sb.Append("\t");
                sb.Append(login.Identifiant);
                sb.Append("\t");
                sb.Append(login.IsEnabled ? "yes" : "no");
                sb.Append("\t");
                sb.Append(login.IsAdmin ? "yes" : "no");
                sb.Append("\t");
                sb.Append(login.LastLoginDate);
                sb.Append("\t");
                sb.Append(login.CreationDate);
                foreach (var profile in profiles.Values)
                {
                    sb.Append("\t");

                    var link = login_profiles[login.Person_Id].SingleOrDefault(lnk => lnk.Profile_Id == profile.Id);
                    if (link != null)
                        sb.Append("Added by " + persons[link.ChangedByLoginId].Firstname + " " + persons[link.ChangedByLoginId].Lastname + " on " + link.ChangedDate.ToString("dd/MM/yyyy HH:mm:ss"));
                    else
                        sb.Append("");
                }
                foreach (var feature in features.Values)
                    if (includeTechnicalRights || feature.Id > 0)
                    {
                        sb.Append("\t");
                        var link = login_features[login.Person_Id].SingleOrDefault(lnk => lnk.Feature_Id == feature.Id);
                        if (link != null)
                            sb.Append("\"" + link.Access + "\n" +
                                "Added by " + persons[link.ChangedByLoginId].Firstname + " " + persons[link.ChangedByLoginId].Lastname + " on " + link.ChangedDate.ToString("dd/MM/yyyy HH:mm:ss") +
                                "\"");
                        else
                            sb.Append("");
                    }
                sb.AppendLine();
            }
            return sb;
        }

        public virtual StringBuilder GenerateFileWithProfileRights(bool includeTechnicalRights)
        {
            var persons = DB.Dao_Base.LoadCollection<Person>().ToDictionary(p => p.Id);
            var profiles = DB.Dao_Base.LoadCollection<Profile>().ToDictionary(p => p.Id);
            var features = DB.Dao_Base.LoadCollection<Feature>().ToDictionary(p => p.Id);
            var profile_features = DB.Dao_Base.LoadCollection<Profile_Feature>().ToLookup(p => p.Profile_Id);

            var profileRights = GetAllRights(true, includeTechnicalRights);
            var sb = new StringBuilder();
            sb.Append('\uFEFF'); // U+FEFF is the byte-order mark character

            #region Titles

            sb.Append("Profile Name");
            sb.Append("\t");
            sb.Append("Inherited Profile");
            sb.Append("\t");
            foreach (var feature in features.Values)
                if (includeTechnicalRights || feature.Id > 0)
                {
                    sb.Append("\t");
                    sb.Append("Has Feature: " + feature.Name);
                }
            sb.AppendLine();
            #endregion

            foreach (var kvp in profileRights)
            {
                var profile = kvp.Key as Profile;
                sb.Append(profile.Name);
                sb.Append("\t");
                sb.Append(profile.InheritingProfile_Id.HasValue ? profiles[profile.InheritingProfile_Id.Value].Name : "");
                foreach (var feature in features.Values)
                    if (includeTechnicalRights || feature.Id > 0)
                    {
                        sb.Append("\t");
                        var link = profile_features[profile.Id].SingleOrDefault(lnk => lnk.Feature_Id == feature.Id);
                        if (link != null)
                            sb.Append("\"" + link.Access + "\n" +
                                      "Added by " + persons[link.ChangedByLoginId].Firstname + " " + persons[link.ChangedByLoginId].Lastname + " on " + link.ChangedDate.ToString("dd/MM/yyyy HH:mm:ss") +
                                      "\"");
                        else
                            sb.Append("");
                    }
                sb.AppendLine();
            }
            return sb;
        }
    }
}
