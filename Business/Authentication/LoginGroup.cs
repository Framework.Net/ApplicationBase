﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

using TechnicalTools.Model;

using ApplicationBase.DAL.Users;
using System.Linq;

namespace ApplicationBase.Business
{
    /// <summary>
    /// Allow to group login env and make some treatment by group of login
    /// </summary>
    public interface ILoginGroup : IReadOnlyCollection<Login>, IIsNamed
    {
    }

    [DebuggerDisplay("{" + nameof(Name) + ",nq}")]
    public class LoginGroup : ILoginGroup, IReadOnlyCollection<Login>, ITreeNodeWithParentReadable
    {
        public string                     Name   { get; }
        public IReadOnlyCollection<Login> Logins { get; }
        public int                        Count  { get { return Logins.Count; } }

        public LoginGroup(string name, IReadOnlyCollection<Login> logins)
        {
            Name = name;
            Logins = logins;
            _minId = logins.Select(login => login.Person_Id).DefaultIfEmpty(long.MinValue).Min();
        }
        public IEnumerator<Login> GetEnumerator()
        {
            return Logins.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator() { return Logins.GetEnumerator(); }
        

        public override string ToString() { return Name; }

        ITreeNodeWithParentReadable ITreeNodeWithParentReadable.ParentNode { get { return null; } }

        public override int GetHashCode()
        {
            return Logins.Count ^ (int)_minId ^ Name.GetHashCode();
        }
        readonly long _minId;
        public override bool Equals(object obj)
        {
            var grp = obj as LoginGroup;
            if (grp == null)
                return false;
            if (Name != grp.Name)
                return false;
            return Logins.OrderBy(login => login.Person_Id)
                         .SequenceEqual(grp.OrderBy(login => login.Person_Id));
        }
    }
}
