﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;

using System.Xml.Linq;

using TechnicalTools;
using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.Tools;
using ApplicationBase.Deployment;
using ApplicationBase.Deployment.Data;

using DataMapper;

using ApplicationBase.Common;
using ApplicationBase.Business.Support;


namespace ApplicationBase.Business
{
    public class Config
    {
        public                    EnvironmentConfig         Environment          { get { return Connections.Environment; } set { Connections.Environment = value; } }

        public                    bool                      IsLoaded             { get; private set; }
        public virtual            bool                      WarnBadMappedSetting { get { return true; } }

        public virtual            DebugConfig               Debug                { get; protected set; } = new DebugConfig();

        public virtual            ConfigGlobalOptimizations GlobalOptimizations  { get; protected set; } = new ConfigGlobalOptimizations();

        public virtual     Common.Config                    Connections          { get; protected set; } = new Common.Config();

        public             Common.Config                    BaseConnections      { get { return Connections; } }

        public virtual Dialoguing.DialoguingConfig          DialoguingConfig     { get; protected set; } = new Dialoguing.DialoguingConfig();

        // ConfigType allow us to map the real type we want to use, in our case ApplicationBase.UI.ViewConfig inherit from ViewConfig
        // We cannot use the ui type here because of project dependencies (which are good by the way, no need to change them)
        // The deserializer will replace the current container by the good specialized one (it will copy the values in current container)
        [ConfigType(FullTypeName = nameof(ApplicationBase) + ".UI.ViewConfig")]
        public virtual ViewConfig View { get; protected set; } = new ViewConfig();

        public virtual SupportConfig Support { get; protected set; } = new SupportConfig();


        public static Config Instance
        {
            get { return _Instance; }
            set
            {
                _Instance = value;
                if (value != null)
                    if (value.IsLoaded)
                        value.Apply();
                    else
                        BusinessSingletons.Instance.OnObjectExistsDo<AuthenticationManager>((mgr) =>
                        {
                            mgr.Authenticated += newSettings =>
                             {
                                 if (!value.IsLoaded && value == Instance)
                                 {
                                     value.Load(newSettings);
                                     value.Apply();
                                 }
                             };
                        });
            }
        }
        static Config _Instance;

        public Config() { }

        public virtual void ShallowCopyFrom(Config source)
        {
            Environment = source.Environment;
            IsLoaded = source.IsLoaded;
            Debug = source.Debug;
            Connections = source.Connections;
            DialoguingConfig = source.DialoguingConfig;
            View = source.View;
            Support = source.Support;
        }

        protected virtual void Apply()
        {
            if (Debug.PreventDistributedTransaction)
            {
                //Let the application throw a "DTC not available" exception by stopping the MSDTC service. This only works with SQL Server!
                //This does not work... sc.Status throw an invalid operation exception
                //using (var sc = new System.ServiceProcess.ServiceController("Distributed Transaction Coordinator"))
                //    if (sc.Status == System.ServiceProcess.ServiceControllerStatus.Running)
                //        sc.Stop();

                // So... we try manually detect it...
                //DbConnection_Extensions.OnDistributedTransactionDetected -= DbConnection_Extensions_OnDistributedTransactionDetected;
                //DbConnection_Extensions.OnDistributedTransactionDetected += DbConnection_Extensions_OnDistributedTransactionDetected;
            }

            IDbMapperSharedSettings.Instance.BulkInsertTimeout = Connections.BulkInsertTimeout;

            BaseDTO.TrackInstances = Debug.TrackObjectInstances;
            CommonSingletons.Instance.SetConnectionsConfig(BaseConnections);

            DB.Dao_Base.CheckEnumsAreLoaded();
        }

        static void DbConnection_Extensions_OnDistributedTransactionDetected(System.Data.Common.DbConnection obj)
        {
            throw new InvalidOperationException($"MSDTC prevented because of {nameof(Debug)}.{nameof(DebugConfig.PreventDistributedTransaction)} configuration!");
        }

        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(Config));

        /// <summary>
        /// Read configuration from file in path "/config/gateway.cfg"
        /// </summary>
        /// <returns>Object Config containing the configuration read</returns>
        public static T ReadConfigurationFile<T>(string envFile)
            where T : Config
        {
            if (envFile[1] != ':' && !envFile.StartsWith(@"\\"))
                envFile = @"\\" + envFile;
            var config = DeserializerWithDependencyInversion.DeserializeFromFile<T>(envFile);
            return config;
        }

        public static EnvironmentConfig GetDefaultEnvironmentsFromDb(IDataAccessor accessor, Filter filter = null)
        {
            var envs = accessor.GetAvailableEnvironments(ApplicationBase.Deployment.Deployer.GetCurrentVersion(), filter);
            if (envs.Count == 1)
                return envs.Single();
            if (envs.Count > 1)
                throw new TechnicalException("Too much environment match these filter!", null);
            return null;
        }

        
        public virtual void Load(List<EnvironmentSetting> envSettings)
        {
            var values = envSettings.Take(1).ToDictionary(s => s.FullKey, s => s.Value);
            var before = Connections.ConnectionString_Base;
            var log = WarnBadMappedSetting
                ? (Action<string, string>)((string badKey, string msg) => _log.Warn(msg))
                : (Action<string, string>)((string badKey, string msg) => _log.Info(msg));
            // For local db (of type .mdf) we are nice and handle local path
            if (values.First().Value.ToLowerInvariant().Contains("AttachDbFileName".ToLowerInvariant()))
            {
                var builder = new System.Data.SqlClient.SqlConnectionStringBuilder(values.First().Value);
                if (builder.AttachDBFilename.ToLowerInvariant().EndsWith(".mdf"))
                {
                    // This allow to handle not supported scenario where connectionstring contains a relative path
                    // if path is full, this line has no effect, otherwise it fix it
                    builder.AttachDBFilename = Path.GetFullPath(Path.Combine(System.Environment.CurrentDirectory, builder.AttachDBFilename));
                }
                values[values.Keys.First()] = builder.ToString();
            }

            DeserializerWithDependencyInversion.AssignKeyValuePairs(this, values, log);
            var realConnectionString = Connections.ConnectionString_Base;
            System.Diagnostics.Debug.Assert(IDataAccessor_Extensions.FirstSettingReturnedIsBaseConnectionString != null &&
                                            realConnectionString != before, "Security measure. The default conection can only authenticate, so the new one should be different to have more rights");

            var mapper = DbMapperFactory.CreateSqlMapper(new NamedObject("Temp Init Connection"), realConnectionString);

            if (Environment.Domain != eEnvironment.LocalNoDB)
            {
                var env = mapper.GetObjectWithId<EnvironmentConfig>(Environment.Id);
                if (env?.FullName != Environment?.FullName)
                    Environment.Id = -1; // no risk of confusion later
                else
                    Environment = env;
            }

            values = envSettings.Skip(1).ToDictionary(s => s.FullKey, s => s.Value);
            AdaptSettings(values);
            DeserializerWithDependencyInversion.AssignKeyValuePairs(this, values, log);

            IsLoaded = true;
        }

        protected virtual void AdaptSettings(Dictionary<string, string> values)
        {
            // Nothing
        }

        public static XElement AsXmlConfigFile(EnvironmentConfig env, string rootNodeName = null)
        {
            var flattenizedSettings = GetFlattenizedSettings(env);
            var nodes = new Dictionary<string, XElement>();
            foreach (var s in flattenizedSettings.Values)
            {
                var n = new XElement(s.Key, s.Value);
                var curFullName = s.FullKey;
                nodes.Add(curFullName, n);
                int index = curFullName.LastIndexOf('.');
                while (index != -1)
                {
                    var parentFullKey = curFullName.Remove(index);
                    if (!nodes.TryGetValue(parentFullKey, out XElement nParent))
                    {
                        var parentKey = parentFullKey.Contains('.') ? parentFullKey.Substring(parentFullKey.LastIndexOf('.') + 1) : parentFullKey;
                        nParent = new XElement(parentKey);
                        nodes.Add(parentFullKey, nParent);
                        nParent.Add(n);
                        n = nParent;
                        curFullName = parentFullKey;
                        index = curFullName.LastIndexOf('.');
                    }
                    else
                    {
                        nParent.Add(n);
                        break;
                    }
                }
            }

            if (string.IsNullOrEmpty(rootNodeName))
                return nodes.Values.SingleOrDefault(n => n.Parent == null);

            return new XElement(rootNodeName, nodes.Values.Where(n => n.Parent == null));
        }

        public static Dictionary<string, EnvironmentSetting> GetFlattenizedSettings(EnvironmentConfig env)
        {
            var dt = env.OwnerMapper.GetDataTable("select * from [Deployment].[FlattenizedEnvironmentSettings](" + env.Id.ToString(CultureInfo.InvariantCulture) + ")");
            var curSettings = env.OwnerMapper.EnumerateConvertedDataRow<EnvironmentSetting>(dt.AsEnumerable());
            return curSettings.ToDictionary(s => s.FullKey);
            //var curSettings = env.OwnerMapper.LoadCollection<EnvironmentSetting>(env.OwnerMapper.GetColumnNameFor((EnvironmentSetting s) => s.Environment_Id) + " = " + env.Id);

            //if (env.InheritedEnvironment_Id == null)
            //    return curSettings.ToDictionary(s => s.FullKey);

            //var settings = GetFlattenizedSettings(env.OwnerMapper.GetObjectWithId<EnvironmentConfig>(env.InheritedEnvironment_Id.Value));
            //foreach (var curSetting in curSettings)
            //    settings[curSetting.FullKey] = curSetting;

            //return settings;
        }


        public static bool FillDefaultEnvironment(Config config, ApplicationBase.Deployment.Config dcfg, Filter filter = null)
        {
            // Les arguments en ligne de commande sont prioritaires
            if (filter?.IsFilled ?? false)
            {
                if (filter.Domain == eEnvironment.LocalNoDB)
                {
                    config.Environment = new EnvironmentConfig() { Id = -1, Domain = eEnvironment.LocalNoDB, Name = "Disconnected", IsHiddenFromUser = true };
                    return true;
                }
                var env = GetDefaultEnvironmentsFromDb(dcfg.DataAccessor, filter); // Clean args, which is needed
                if (env != null)
                {
                    config.Environment = env;
                    config.BaseConnections.ConnectionString_Base = dcfg.InitialConnectionString;
                    return true;
                }
            }

            // Sinon la config en base par default est prioritaires
            if (filter?.IsEmpty ?? true)
            {
                var env = GetDefaultEnvironmentsFromDb(dcfg.DataAccessor, Filter.Default);
                if (env != null)
                {
                    config.Environment = env;
                    config.BaseConnections.ConnectionString_Base = dcfg.InitialConnectionString;
                    return true;
                }
            }
            return false;
        }
    }
}
