﻿using System;
using System.Linq;

using ApplicationBase.Common;


namespace ApplicationBase.Business
{
    // It is strongly advised to know how to manage diamond inheritance problem in C++
    // to understand what we do here.
    // If application is "well" designed, this class should be the only thing that has a static Instance property
    // and all assemblies should be able to "inherit" from each other.....
    // Singletons are sometimes needed in application and are are often accessed by using one code path (ie: often "YourSingletonClass.Instance")
    // But sometimes we want to create another assembly that override the first one.
    // This problem is like dependency-injection but at an other scale, a "bigger" scale.
    // So to solve the problem, assemblies should designed as big class (where its "method" are in fact the classes)
    // To initialize an assembly we inject a SingletonFactory (or null, the assembly will create the default singletonfactory)
    // Then the asembly will be able to create all singletons
    // One thing remains though : all the problem with class-inheritance will exist with assembly inheritance
    // For example Multi-inheritance can lead to diamond problem. To solve this : 
    // 1) make you SingletonFactory simple
    // 2) "Inherit" the property Instance of your SingletonFactory, this way (we use the keyword "new" because "override" is not possible, the concept does not exist) :
    // to understand the comment, consider "Base" as the base assembly, "A" & "B" assemblies that extend "Base",
    // and C an assembly that wants to use "A" & "B".. so we want to use A.SingletonsFactory and B.SingletonsFactory
    // Both A.SingletonsFactory & B.SingletonsFactory want to overwrite Base.Instance...
    //
    // public new static BusinessSingletons Instance
    // {
    //     get { return _Instance; }
    //     protected set
    //     {
    //         _Instance = value; // We always follow order
    //         // However we don't always override the Base.Singletons...
    //         // What if we have Proejct Base, A and D where D inehrit A and A inherit Base ?
    //         // Base has been initialized, A too, now D must be ok to replace its base instance (A)
    //         // And if we are in A, we must be ok to replace Base.instance by the new value too
    //         // It is only when we detect a diamond problem we dont override the instance.
    //         // The conclusion is : always initialzie the "main" assembly (A or D) before the satellite assembly "B"
    //         if (TechnicalTools.Model.SingletonFactory.Instance == null ||
    //             value != null && TechnicalTools.Model.SingletonFactory.Instance.GetType().IsAssignableFrom(value.GetType()))
    //             TechnicalTools.Model.SingletonFactory.Instance = value;
    //     }
    // }
    // // Because of the diamond inheritance case, we can rely on Base.Assembly to be the current class singletons.
    // // if we are in B.SingletonsFactory class, maybe Base.instance has already been overwritten by instance of A.SingletonFactory
    // // So we store the true part of B Factory here.
    // static BusinessSingletons _Instance; 
    //
    // static BusinessSingletons()
    // {
    //     // Initiate initialization of SingletonFactory singleton...
    //     // The "check code" is not here, but in the public property because other code (assembly) may replace the instance later
    //     Instance = new BusinessSingletons(); 
    // }
    public class BusinessSingletons : TechnicalTools.Model.SingletonFactory
    {
        // It is advised to read "Note 01" in file "Code Design Notes.txt" 
        public static BusinessSingletons Instance
        {
            get { return _Instance; }
            protected set { SetInstance(ref _Instance, value); }
        }
        static BusinessSingletons _Instance = new BusinessSingletons();

        protected BusinessSingletons()
        {
        }

        // Minimal method to override
        protected virtual                  BusinessFactory         CreateBusinessFactory()         { return new BusinessFactory(); }
        public                             BusinessFactory         GetBusinessFactory()            { return GetOrAdd(() => CreateBusinessFactory()); }

        public                             string                  GetRecommendedLocalDataFolder() { return GetOrAdd(() => GetBusinessFactory().CreateRecommendedLocalDataFolder(null)); }
        public                        Logs.DefaultDbLogSender      GetDbLogSender()                { return GetOrAdd(() => GetBusinessFactory().CreateDbSenderLogger()); }
        public                             AuthenticationManager   GetAuthenticationManager()      { return GetOrAdd(() => GetBusinessFactory().CreateAuthenticationManager(Config.Instance.Environment)); }
        public                             ConfigOfUser            GetConfigOfUser()               { return GetOrAdd(() => GetBusinessFactory().CreateConfigOfUser(GetAuthenticationManager())); }
        public                  Deployment.Deployer                GetDeployer()                   { return GetOrAdd(() => GetBusinessFactory().CreateDeployer(BootstrapConfig.Instance)); }
        public                             ReleaseNotifier         GetReleaseNotifier()            { return GetOrAdd(() => GetBusinessFactory().CreateReleaseNotifier(GetDeployer().Config, GetAuthenticationManager().CurrentUser.IsAdmin)); }
        public                        Logs.LogSource               GetUserLogSource()              { return GetOrAdd(() => GetBusinessFactory().CreateLogSource(GetAuthenticationManager(), GetAuthenticationManager().CurrentUser)); }
        public                        Logs.LogSource               GetAdminLogSource()             { return GetOrAdd(() => GetBusinessFactory().CreateLogSource(GetAuthenticationManager(), GetAuthenticationManager().AllLogins().Keys.ToArray())); }
        
        public                  Dialoguing.Dialoguer               GetDialoguer()                  { return GetOrAdd(() => GetBusinessFactory().CreateDialoguer(Config.Instance.BaseConnections.ConnectionStringDialoguerListener, Config.Instance.BaseConnections.ConnectionStringDialoguerStarter, GetAuthenticationManager(), null)); }
        public                  Dialoguing.Messenger               GetMessenger()                  { return GetOrAdd(() => GetBusinessFactory().CreateMessenger(GetDialoguer())); }
        public                  Dialoguing.DataChangeEventManager  GetDataChangeEventManager()     { return GetOrAdd(() => GetBusinessFactory().CreateDataChangeEventManager(DB.Dao_Base, GetDialoguer())); }

        internal                           CommentPatternSuperSet  GetCommentPatternSuperSet()     { return GetOrAdd(() => GetBusinessFactory().CreateCommentPatternSuperSet()); }
        public                 UserDataFix.BusinessRecordEditSet   GetBusinessRecordEditSet()      { return GetOrAdd(() => GetBusinessFactory().CreateBusinessRecordEditSet(DB.Dao_Base, GetAuthenticationManager())); }

        public                Optimization.OptimizationDataManager GetOptimizationDataManager()    { return GetOrAdd(() => GetBusinessFactory().CreateOptimizationDataManager(DB.Dao_Base)); }
        public                Optimization.MetadataAnalyser        GetMetadataAnalyser()           { return GetOrAdd(() => GetBusinessFactory().CreateMetadataAnalyser(DB.Dao_Base)); }
    }
}
