﻿using System;
using System.Linq.Expressions;

using TechnicalTools.Model;

using DataMapper;
using DataMapper.Helpers;


namespace ApplicationBase.Business.Optimization
{
    public class MetadataAnalyser : IRelationshipDataProvider
    {
        public MetadataAnalyser(IDbMapper mapper = null, DtoTypeSizeEstimator estimator = null)
        {
            _mapper = mapper;
            _estimator = estimator ?? new DtoTypeSizeEstimator(mapper);
        }
        readonly IDbMapper _mapper;
        readonly DtoTypeSizeEstimator _estimator;

        public int GetMeanRelationCount<TEntity, TProperty>(Expression<Func<TEntity, TProperty>> expr)
        {
            // Return 2 until we implement...
            return (typeof(TEntity) != null ? 1 : 0)
                 + (typeof(TProperty) != null ? 1 : 0);
        }

        public int GetMeanRecordSize<TEntity>()
        {
            return _estimator.GetMeanInstanceSize(typeof(TEntity));
        }
    }
}
