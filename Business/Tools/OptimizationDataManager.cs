﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools;

using DataMapper;

using ApplicationBase.DAL.Optimization;

using IOptimizationDatum = ApplicationBase.DAL.HasPropertiesOf.Optimization.OptimizationDatum;


namespace ApplicationBase.Business.Optimization
{
    public class OptimizationDataManager
    {
        protected internal OptimizationDataManager(IDbMapper mapper)
        {
            _mapper = mapper;
        }
        readonly IDbMapper _mapper;
        readonly object _lck = new object();
        Dictionary<string, OptimizationDatum> _settings = new Dictionary<string, OptimizationDatum>(100);
        DateTime _lastRefreshUTC = DateTime.MinValue;

        public void Refresh(IProgress<string> pr = null)
        {
            lock (_lck)
            {
                // Clone instances before we update them so we can keep potentially most recent settings update
                var oldSettings = _settings.Select(kvp => kvp.Value.Clone());
                // This line actually loads new settings
                // but also updates instances already in _settings.Values (because of how IDbMapper works)
                var settings = _mapper.LoadCollection<OptimizationDatum>().ToDictionary(od => od.Key);
                _lastRefreshUTC = DateTime.UtcNow;
                foreach (var os in oldSettings)
                {
                    OptimizationDatum cs = settings.TryGetValueClass(os.Key);
                    if (cs != null && cs.DateChangedUTC < os.DateChangedUTC)
                    {
                        cs.Value = os.Value;
                        cs.DateChangedUTC = os.DateChangedUTC;
                    }
                }
                foreach (var cs in settings.Values)
                    if (cs.DateChangedUTC > _lastRefreshUTC)
                        cs.DateChangedUTC = _lastRefreshUTC;
                _settings = settings;
            }
        }

        public void SaveToDatabase(IProgress<string> pr = null)
        {
            lock (_lck)
            {
                foreach (var cs in _settings.Values)
                    if (cs.DateChangedUTC > _lastRefreshUTC)
                        if (cs.Id > 0)
                            _mapper.UpdateToDatabase(cs);
                        else
                            _mapper.CreateInDatabase(cs);
            }
        }

        public IOptimizationDatum GetByKey(string key)
        {
            lock (_lck)
                return _settings.TryGetValueClass(key);
        }
        public void SetByKey(string key, string value)
        {
            lock (_lck)
            {
                var pair = GetByKey(key);
                if (pair?.Value == value)
                    return;
                var notPresent = pair == null;
                if (notPresent)
                    pair = new OptimizationDatum() { Key = key };
                pair.Value = value;
                pair.DateChangedUTC = DateTime.UtcNow;
                if (notPresent)
                    _settings.Add(pair.Key, (OptimizationDatum)pair);
                SaveToDatabase();
            }
        }
    }
}
