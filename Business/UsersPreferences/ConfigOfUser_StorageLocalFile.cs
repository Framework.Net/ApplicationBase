﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

using TechnicalTools;

using ApplicationBase.DAL.UsersPreferences;


namespace ApplicationBase.Business
{
    public partial class ConfigOfUser
    {
        public class StorageLocalFile : StorageTraits
        {
            public StorageLocalFile(string repositoryFilenameFullPath = null)
            {
                _repositoryFilenameFullPath = repositoryFilenameFullPath ?? DefaultRepositoryFileName;
                _xdoc = File.Exists(_repositoryFilenameFullPath)
                      ? XDocument.Load(_repositoryFilenameFullPath)
                      : new XDocument(new XElement("UserSettings"));
            }
            readonly string _repositoryFilenameFullPath;
            readonly XDocument _xdoc;
            public static string DefaultRepositoryFileName { get; set; } = "UserSettings.xml";

            public override List<UserSetting> LoadUserSettingsFromStorage(bool userIsAuthenticated, long person_id)
            {
                var allSettings = AllUserSetting();
                var settings = allSettings.Where(e => e.setting.Person_Id == person_id).ToList();
                if (settings.Count == 0)
                    settings = allSettings.Where(e => e.setting.Person_Id == 0).ToList();
                return settings.Select(e => e.setting).ToList();
            }
            protected List<(XElement node, UserSetting setting)> AllUserSetting()
            {
                var allSettings = _xdoc.Root.Elements("Setting").Select(e => (e, ToUserSetting(e))).ToList();
                return allSettings;
            }
            protected static UserSetting ToUserSetting(XElement e)
            {
                return new UserSetting()
                {
                    Person_Id = long.Parse(e.Attribute("Person_Id").Value.IfBlankUse("0")),
                    PropertyName = e.Attribute("PropertyName").Value,
                    PropertyValue = e.Attribute("PropertyValue")?.Value,
                    PropertyType = e.Attribute("PropertyType").Value,
                    PropertyAvailableValueForList = e.Attribute("PropertyAvailableValueForList")?.Value,
                };
            }
            // Create or update
            protected static XElement ToXmlElement(UserSetting us, XElement e = null)
            {
                e = e ?? new XElement("Setting");
                e.SetAttributeValue("Person_Id", us.Person_Id.ToStringInvariant());
                e.SetAttributeValue("PropertyName", us.PropertyName);
                e.SetAttributeValue("PropertyValue", us.PropertyValue);
                e.SetAttributeValue("PropertyType", us.PropertyType);
                e.SetAttributeValue("PropertyAvailableValueForList", us.PropertyAvailableValueForList);
                return e;
            }

            public override void CreateStorage(bool userIsAuthenticated, long person_id, UserSetting us)
            {
                CreateOrUpdateStorage(userIsAuthenticated, person_id, us);
            }
            public override void UpdateStorage(bool userIsAuthenticated, long person_id, UserSetting us)
            {
                CreateOrUpdateStorage(userIsAuthenticated, person_id, us);
            }
            void CreateOrUpdateStorage(bool userIsAuthenticated, long person_id, UserSetting us)
            {
                var allSettings = AllUserSetting();
                var ns = allSettings.SingleOrDefault(e => e.setting.Person_Id == us.Person_Id
                                                       && e.setting.PropertyName == us.PropertyName);
                if (ns.setting != null &&
                    us.PropertyValue == ns.setting.PropertyValue &&
                    us.PropertyType == ns.setting.PropertyType &&
                    us.PropertyAvailableValueForList == ns.setting.PropertyAvailableValueForList)
                    return; // nothing to do
                var node = ToXmlElement(us, ns.node);
                if (ns.setting == null)
                    _xdoc.Root.Add(node);
                var parentPath = Path.GetDirectoryName(_repositoryFilenameFullPath);
                if (!string.IsNullOrWhiteSpace(parentPath)) // protect against for relative path
                    Directory.CreateDirectory(parentPath);
                _xdoc.Save(_repositoryFilenameFullPath);
            }
        }
    }
}
