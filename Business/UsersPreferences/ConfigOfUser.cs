﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.Tools;

using ApplicationBase.DAL.UsersPreferences;


namespace ApplicationBase.Business
{
    // To add a new user setting, just : 
    // Add a new property like other, add a line in CopyFrom, and
    public partial class ConfigOfUser : INotifyPropertyChanged, IUserInteractiveObject
    {
        public string FirstLogin_ApplicationVersion                    { get { return _FirstLogin_ApplicationVersion;                    } set { SetUserSetting(ref _FirstLogin_ApplicationVersion,                    value); } } string _FirstLogin_ApplicationVersion;
        public string Application_DefaultSkin                          { get { return _Application_DefaultSkin;                          } set { SetUserSetting(ref _Application_DefaultSkin,                          value); } } string _Application_DefaultSkin;
        public int?   Application_LastReleaseNoteSeen                  { get { return _Application_LastReleaseNoteSeen;                  } set { SetUserSetting(ref _Application_LastReleaseNoteSeen,                  value); } } int?   _Application_LastReleaseNoteSeen;
        // Note : Attributes & Description allow the view ConfigOfUserView to autodetect settings that user can edit
        [Category("Application")][DisplayName("Max Concurrent Parallel Business Workers")]
        [Description("This setting allows you to slown down parallel business code execution" + 
                     " so less errors occur when using limited resources on your computer or outside this application.")]
        public int?   Application_MaxConcurrentParallelBusinessWorkers { get { return _Application_MaxConcurrentParallelBusinessWorkers; } set { SetUserSetting(ref _Application_MaxConcurrentParallelBusinessWorkers, value); } } int?   _Application_MaxConcurrentParallelBusinessWorkers;

        [Category("GridView / Options Behavior")][DisplayName("Auto Expand All Groups")]
        [Description("Not fully functional yet!\r\nThe idea is to prevent grid view to reset their layout when something change.")]
        public bool?  GridView_OptionsBehavior_AutoExpandAllGroups     { get { return _GridView_OptionsBehavior_AutoExpandAllGroups;     } set { SetUserSetting(ref _GridView_OptionsBehavior_AutoExpandAllGroups,     value); } } bool?  _GridView_OptionsBehavior_AutoExpandAllGroups;

        ConcurrentDictionary<string, UserSetting> _UserSettings = new ConcurrentDictionary<string, UserSetting>();

        protected internal ConfigOfUser(AuthenticationManager manager, StorageTraits traits = null)
            : this(traits)
        {
            _manager = manager;
            _manager.CurrentUserChanged += ReloadUserConfig;
            ReloadUserConfig();
        }
        protected ConfigOfUser(StorageTraits traits = null)
        {
            _traits = traits ?? new StorageLocalFile();
        }
        readonly AuthenticationManager _manager;
        readonly StorageTraits _traits;
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(ConfigOfUser));

        protected bool IsConnected {  get { return Config.Instance.Environment.Domain != Deployment.Data.eEnvironment.LocalNoDB; } }

        public abstract class StorageTraits
        {
            public abstract List<UserSetting> LoadUserSettingsFromStorage(bool userIsAuthenticated, long person_id);
            public abstract void CreateStorage(bool userIsAuthenticated, long person_id, UserSetting us);
            public abstract void UpdateStorage(bool userIsAuthenticated, long person_id, UserSetting us);
        }

        public virtual ConfigOfUser Clone()
        {
            var clone = CreateNewInstance(_traits);
            clone.CopyFrom(this);
            return clone;
        }
       
        protected internal virtual ConfigOfUser CreateNewInstance(StorageTraits traits)
        {
            return new ConfigOfUser(traits);
        }
        protected virtual void CopyFrom(ConfigOfUser from)
        {
            _UserSettings = from._UserSettings;
            FirstLogin_ApplicationVersion = from.FirstLogin_ApplicationVersion;
            Application_DefaultSkin = from.Application_DefaultSkin;
            Application_LastReleaseNoteSeen = from.Application_LastReleaseNoteSeen;
            Application_MaxConcurrentParallelBusinessWorkers = from.Application_MaxConcurrentParallelBusinessWorkers;
            GridView_OptionsBehavior_AutoExpandAllGroups = from.GridView_OptionsBehavior_AutoExpandAllGroups;
        }
        protected virtual void ReloadUserConfig()
        {
            List<UserSetting> settings = _traits.LoadUserSettingsFromStorage(IsConnected, _manager.CurrentUser.Person_Id);
            var settingsByKey = settings.ToConcurrentDictionary(s => s.PropertyName.Replace(".", "_"));
            // Load user values (default overwritten by db values)
            var cfg = CreateNewInstance(_traits);
            cfg._disableSaving = true;
            Debug.Assert(Config.Instance.IsLoaded);
            var log = Config.Instance.WarnBadMappedSetting
                ? (Action<string, string>)((string badKey, string msg) => _log.Warn(msg))
                :                         ((string badKey, string msg) => _log.Info(msg));
            DeserializerWithDependencyInversion.AssignKeyValuePairs(cfg, settingsByKey.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.PropertyValue), log);
            cfg._UserSettings = settingsByKey;

            // Assign cfg to this
            _disableSaving = true;
            CopyFrom(cfg);
            _disableSaving = false;
        }
        bool _disableSaving;

        /// <summary>
        /// Check that all values in DB are mappable on ConfigOfUser
        /// </summary>
        public static void CheckAllValues()
        {
            if (Config.Instance.Environment.Domain != Deployment.Data.eEnvironment.LocalNoDB &&
                DebugTools.IsForDevelopper) // Allow to test all
            {
                var currentInstance = BusinessSingletons.Instance.GetConfigOfUser();
                var traits = currentInstance._traits;
                var virginInstance = currentInstance.CreateNewInstance(traits);
                virginInstance._UserSettings = traits.LoadUserSettingsFromStorage(true, BusinessSingletons.Instance.GetAuthenticationManager().NoRightLogin.Person_Id)
                                                     .GroupBy(s => s.PropertyName)
                                                     .Select(grp => grp.First())
                                                     .ToConcurrentDictionary(s => s.PropertyName.Replace(".", "_"));
                virginInstance._disableSaving = true;
                DeserializerWithDependencyInversion.AssignKeyValuePairs(virginInstance, virginInstance._UserSettings.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.PropertyValue));
            }
        }
        

        public void WithoutSaving(Action action)
        {
            try
            {
                _disableSaving = true;
                action();
            }
            finally
            {
                _disableSaving = false;
            }
        }

        protected void SetUserSetting(ref string curValue, string newValue, [CallerMemberName] string propName = null)
        {
            if (curValue == null && newValue == null ||
                curValue?.CompareTo(newValue) == 0)
                return; // Nothing to do !
            UpdateAndSaveUserSetting(ref curValue, newValue, propName);
        }
        protected void SetUserSetting<T>(ref T curValue, T newValue, [CallerMemberName] string propName = null) where T : struct, IComparable
        {
            if (curValue.CompareTo(newValue) == 0)
                return; // Nothing to do !
            UpdateAndSaveUserSetting(ref curValue, newValue, propName);
        }
        protected void SetUserSetting<T>(ref T? curValue, T? newValue, [CallerMemberName] string propName = null) where T : struct, IComparable
        {
            if (curValue == null && newValue == null ||
                curValue != null && curValue.Value.CompareTo(newValue) == 0)
                return; // Nothing to do !
            UpdateAndSaveUserSetting(ref curValue, newValue, propName);
        }
        protected void UpdateAndSaveUserSetting<T>(ref T curValue, T newValue, string propName)
        {
            var v = curValue;
            curValue = newValue;
            try
            {
                SaveUserSetting(newValue, propName);
            }
            catch
            {
                curValue = v;
                throw;
            }
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        protected void SaveUserSetting<T>(T newValue, string propName)
        {
            Debug.Assert(propName != null, nameof(propName) + " != null");

            if (_disableSaving)
                return;
            if (!_UserSettings.TryGetValue(propName, out UserSetting us))
            { 
                us = new UserSetting
                {
                    Person_Id = _manager.CurrentUser.Person_Id,
                    PropertyType = typeof(T).Name,
                    PropertyName = propName.Replace("_", "."),
                    PropertyValue = newValue?.ToStringInvariant()
                };
                _traits.CreateStorage(IsConnected, BusinessSingletons.Instance.GetAuthenticationManager().NoRightLogin.Person_Id, us);
                _UserSettings[propName] = us;
            }
            else
            {
                us.PropertyValue = newValue?.ToStringInvariant();
                _traits.UpdateStorage(IsConnected, BusinessSingletons.Instance.GetAuthenticationManager().NoRightLogin.Person_Id, us);
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;




        #region Helper
        protected const string DescriptionFooter = "  \\n : break the line and start a new one\r\n" +
                                                   "You can force any character by preceding it by \"\\\"\r\n" +
                                                   "Example : To display character \"%\", you have to write \"\\%\",\r\n" +
                                                   "          To display \"\\\" you have to write \"\\\\\"\r\n" +
                                                   "Any other literal character is added \"as is\"";
        protected Func<T, string> BuildPartnerEnvironmentDisplay<T>(string pattern, Dictionary<char, Func<T, string>> mappings)
        {
            int i = 0;
            var parts = new List<Func<T, string, string>>();

            while (i < pattern.Length)
            {
                string literal = GetLiteral(pattern, ref i, until: '%');
                if (literal != null)
                    parts.Add((pe, cur) => cur + literal);
                if (i >= pattern.Length)
                    break;
                Debug.Assert(pattern[i] == '%');
                ++i;
                if (i >= pattern.Length)
                {
                    parts.Add((pe, acc) => acc + "%");
                    break;
                }
                if (pattern[i] == '%')
                    parts.Add((pe, acc) => acc + "%");
                else
                {
                    if (mappings.TryGetValue(pattern[i], out Func<T, string>  f))
                        parts.Add((pe, acc) => acc + f(pe));
                    else
                    {
                        var j = i; // fix the value for lambda
                        parts.Add((pe, acc) => acc + "%" + pattern[j]);
                    }
                }
                ++i;
            }
            return BuildOptimizedFunc(parts);
        }
        string GetLiteral(string pattern, ref int i, char until)
        {
            string literal = "";
            while (i < pattern.Length && pattern[i] != until)
            {
                if (pattern[i] == '\\')
                {
                    ++i;
                    if (i >= pattern.Length)
                        literal += '\\';
                    else
                    {
                        if (pattern[i] == 'n')
                            literal += Environment.NewLine;
                        else
                            literal += pattern[i];
                    }
                }
                else
                    literal += pattern[i];
                ++i;
            }
            return literal.Length == 0
                ? null
                : literal;
        }
        Func<T, string> BuildOptimizedFunc<T>(List<Func<T, string, string>> parts)
        {
            if (parts.Count == 1)
                return (T pe) => parts[0](pe, string.Empty);
            if (parts.Count == 2)
                return (T pe) => parts[1](pe, parts[0](pe, string.Empty));
            if (parts.Count == 3)
                return (T pe) => parts[2](pe, parts[1](pe, parts[0](pe, string.Empty)));
            if (parts.Count == 4)
                return (T pe) => parts[3](pe, parts[2](pe, parts[1](pe, parts[0](pe, string.Empty))));
            if (parts.Count == 5)
                return (T pe) => parts[4](pe, parts[3](pe, parts[2](pe, parts[1](pe, parts[0](pe, string.Empty)))));
            return (T pe) =>
            {
                string acc = string.Empty;
                foreach (var p in parts)
                    acc = p(pe, acc);
                return acc.Trim();
            };
        }
        
        #endregion
    }
}
