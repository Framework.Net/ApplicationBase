﻿using System;
using System.Collections.Generic;

using DataMapper;

using ApplicationBase.DAL.UsersPreferences;


namespace ApplicationBase.Business
{
    public partial class ConfigOfUser
    {
        public class StorageDb : StorageTraits
        {
            public StorageDb(DAL.DAO dao)
            {
                _dao = dao;
            }
            readonly DAL.DAO _dao;

            public override List<UserSetting> LoadUserSettingsFromStorage(bool userIsAuthenticated, long person_id)
            {
                if (!userIsAuthenticated)
                    return new List<UserSetting>();
                var filter = person_id == BusinessSingletons.Instance.GetAuthenticationManager().NoRightLogin.Person_Id ? null
                           : _dao.GetColumnNameFor((UserSetting us) => us.Person_Id) + " = " + DbMappedField.ToSql(person_id);
                var settings = _dao.LoadCollection<UserSetting>(filter);
                return settings;
            }

            public override void CreateStorage(bool userIsAuthenticated, long person_id, UserSetting us)
            {
                if (userIsAuthenticated)
                    _dao.CreateInDatabase(us);
            }

            public override void UpdateStorage(bool userIsAuthenticated, long person_id, UserSetting us)
            {
                if (userIsAuthenticated)
                    _dao.UpdateToDatabase(us);
            }
        }
    }
}
