﻿using System;

using TechnicalTools.Model;


namespace ApplicationBase.Business
{
    /// <summary>
    /// In <see cref="Business.Config"/> the property View is declared as ViewConfig.
    /// However this property is often overriden in RealApplication.Business.Config class that inherits from ApplicationBase.Business.Config
    /// So an instance of the real ViewConfig application type is created at runtime. 
    /// And the value from ApplicationBase.UI.ViewConfig must be transfered to real instance.
    /// So it is the implementaiton of ICopyable
    /// CopyFrom must be overriden in child class
    /// </summary>
    public class ViewConfig : ICopyable
    {
        void ICopyable.CopyFrom(ICopyable source)
        {
            CopyFrom((ViewConfig)source);
        }
        protected virtual void CopyFrom(ViewConfig source)
        {
        }
    }
}
