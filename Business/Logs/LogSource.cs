﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Tools;

using DataMapper;

using ApplicationBase.DAL;
using ApplicationBase.DAL.Users;
using ApplicationBase.DAL.Logging;
using ApplicationBase.Common;


namespace ApplicationBase.Business.Logs
{
    public class LogSource : IUserInteractiveObject
    {
        public bool CanChangeDateFrom { get; protected set; } = true;
        public bool CanChangeDateTo { get; protected set; } = true;
        public bool CanChangeLogin { get; protected set; } = true;
        public IReadOnlyDictionary<Login, Person> SelectableLogins { get; protected set; }

        public AuthenticationManager AuthenticationManager { get; }

        internal protected LogSource(AuthenticationManager authenticationManager, params Login[] selectableLogins)
        {
            AuthenticationManager = authenticationManager;
            var canSeeServiceLogs = AuthenticationManager.GetAccessFor(Feature.CanSeeServiceLogs).HasFlag(eFeatureAccess.CanSee);
            var allLogins = AuthenticationManager.AllLogins();
            SelectableLogins = selectableLogins.Where(login => login.Person_Id == AuthenticationManager.CurrentUser.Person_Id
                                                            || canSeeServiceLogs && IsAutomatedTask(login)
                                                            || AuthenticationManager.CurrentUser.IsAdmin)
                                               .ToDictionary(login => login, login => allLogins[login]);
        }
        static readonly TechnicalTools.Logs.ILogger _log = TechnicalTools.Logs.LogManager.Default.CreateLogger(typeof(LogSource));

        protected virtual bool IsAutomatedTask(Login login)
        {
            return login.IsService;
        }

        public Dictionary<LogSession, IReadOnlyCollection<Log>> GetLogs(LogFilter filter)
        {
            filter.Correct(this).ThrowIfNotNull();

            if (Config.Instance.Environment.Domain == Deployment.Data.eEnvironment.LocalNoDB)
            {
                var sender = BusinessSingletons.Instance.GetDbLogSender();
                var res = sender.GetLastLogs();
                if (res.TotalLogCountCaptured > res.Logs.Length)
                    BusEvents.Instance.RaiseBusinessMessage($"Because of big number of logs, the {res.TotalLogCountCaptured - res.Logs.Length} olders ones have been removed!", _log);
                return sender.Session.Yield().ToDictionary(s => s, _ => (IReadOnlyCollection<Log>)res.Logs);
            }
                
            var logs = filter.SessionId.HasValue
                        ? DB.Dao_Base.LoadCollection<Log>(DB.Dao_Base.GetColumnNameFor((Log log) => log.Session_Id) + " = " + DbMappedField.ToSql(filter.SessionId.Value))
                        : DB.Dao_Base.EnumerateCollectionFromInnerJoinSql<Log>(
                            innerJoin: DB.Dao_Base.GetTableNameFor<LogSession>() + " as S",
                            joinCondition: "S." + DB.Dao_Base.GetColumnNameFor((LogSession s) => s.Id) + " = " +
                                            "A." + DB.Dao_Base.GetColumnNameFor((Log log) => log.Session_Id),
                            whereCondition: DB.Dao_Base.GetColumnNameFor((Log log) => log.TimestampUTC) + " > " + DbMappedField.ToSql(filter.DateUtcFrom) + " and "
                                        + DB.Dao_Base.GetColumnNameFor((Log log) => log.TimestampUTC) + " < " + DbMappedField.ToSql(filter.DateUtcToIncluded.AddDays(1))
                                        + " and " + filter.ForLogins.Select(login => login.Person_Id)
                                                                    .ToMappedIntegerSet()
                                                                    .AsSqlCondition("S." + DB.Dao_Base.GetColumnNameFor((LogSession s) => s.Login_Id)))
                                    .ToList();
            var sessions = DB.Dao_Base.GetObjectsWithIds<LogSession>(logs.Select(log => log.Session_Id).Distinct())
                                        .ToDictionary(session => session.Id);
            return logs.GroupBy(log => log.Session_Id)
                        .ToDictionary(grp => sessions[grp.Key], grp => (IReadOnlyCollection<Log>)grp.ToList());
        }
    }
}
