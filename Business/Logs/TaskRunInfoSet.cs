﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model;

using DataMapper;

using ApplicationBase.DAL.Logging;
using ApplicationBase.Common;


namespace ApplicationBase.Business.Logs
{
    public class TaskRunInfoSet : IUserInteractiveObject
    {
        public TaskRunInfoSet(LogSource source)
        {
            Source = source;
        }
        public LogSource Source { get; }

        public IReadOnlyDictionary<string, List<TaskRunInfo>> Load(LogFilter filter)
        {
            filter.Correct(Source).ThrowIfNotNull();
            var to = filter.DateUtcToIncluded.AddDays(1);
            var sqlFilter = DB.Dao_Base.GetColumnNameFor((TaskRunInfo tri) => tri.StartDateUtc)
                          + " >= " + DbMappedField.ToSql(filter.DateUtcFrom)
                          + " and " + DB.Dao_Base.GetColumnNameFor((TaskRunInfo tri) => tri.StartDateUtc)
                          + " < " + DbMappedField.ToSql(filter.DateUtcToIncluded.AddDays(1))
                          + " and " + filter.ForLogins.Select(login => login.Person_Id)
                                                      .ToMappedIntegerSet()
                                                      .AsSqlCondition(DB.Dao_Base.GetColumnNameFor((TaskRunInfo tri) => tri.LoginId));
            var runInfos = DB.Dao_Base.LoadCollection<TaskRunInfo>(sqlFilter);

            var res = runInfos.GroupBy(ri => ri.TaskName)
                              .OrderBy(grp => grp.Key)
                              .ToSortedDictionary(grp => grp.Key.Replace(".", TaskNameSeparator), grp => grp.ToList());

            return res;
        }
        public static string TaskNameSeparator { get; } = " / ";
    }
}
