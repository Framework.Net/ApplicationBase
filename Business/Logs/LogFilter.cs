﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools.Model;

using ApplicationBase.DAL.Users;
using TechnicalTools;

namespace ApplicationBase.Business.Logs
{
    public class LogFilter : IUserInteractiveObject
    {
        public DateTime     DateUtcFrom       { get; set; } = DateTime.UtcNow.Date;
        public DateTime     DateUtcToIncluded { get; set; } = DateTime.UtcNow.Date;
        public List<Login>  ForLogins         { get; } = new List<Login>();
        public long?        SessionId         { get; } // not editable yet because not handled in Correct()

        public LogFilter(LogSource source, long? sessionsId)
        {
            Source = source 
                       ?? throw new ArgumentNullException(nameof(source));
            SessionId = sessionsId;
        }
        public LogSource Source { get; }

        public void Correct()
        {
            Correct(Source).ThrowIfNotNull();
        }
        internal Exception Correct(LogSource source)
        {
            if (Source != source)
                return new InvalidOperationException();
            // Security concern : we do not allow user to acces to other users' logs
            for (int i = ForLogins.Count - 1; i >= 0; --i)
                if (!Source.SelectableLogins.ContainsKey(ForLogins[i]))
                    ForLogins.RemoveAt(i);
            return null;
        }
    }

}
