﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.Tools;

using DataMapper.Tools;

using ApplicationBase.DAL.Logging;
using ApplicationBase.Common;


namespace ApplicationBase.Business.Logs
{
    /// <summary>
    /// Default class that send Log to DB
    /// </summary>
    public class DefaultDbLogSender : DeferredLogHandler<DAL.Logging.Log>
    {
        public DefaultDbLogSender(LogManager manager, object mainOwner, params ITinyService[] dependencies)
            : base(manager, mainOwner, dependencies)
        {
            IsReady = false;
            AggregateSequentialAndSimilarExceptions = true;

            Session = new LogSession()
            {
                Hostname = Environment.MachineName
            };
            ExceptionManager.Instance.IgnoreExceptionAndBreak(() =>
            {
                Session.Ips = NetworkInterface.GetAllNetworkInterfaces()
                                              .Where(ni => ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                                              .Select(ni => ni.Name + ": " + ni.GetIPProperties()
                                                                               .UnicastAddresses
                                                                               .Where(ip => ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                                                                               .Select(ip => ip.Address.ToString())
                                                                               .Join())
                                              .Join("|");
            });

            // We are kind of stuck with dependencies here...
            // DefaultLogSender should technically exist before authentication manager to be able to capture all interesting logs (about versionning, etc)
            // But in our case we want log sender to be dependent of authentication manager.
            // This is why we use OnObjectExistsDo : to capture the dependency as soon as it is available
            BusinessSingletons.Instance.OnObjectExistsDo<AuthenticationManager>((mgr) =>
            {
                // As soon as the authentication manager exit, we subscribe to event CurrentUserChanged
                // So we can eventually save logs objects
                _manager = mgr;
                if (_manager.CurrentUserReal == _manager.NoRightLogin) // means _session.Login_Id is not right yet
                    _manager.CurrentUserChanged += AuthenticationManager_CurrentUserChanged;
                else
                    AuthenticationManager_CurrentUserChanged();
            });
        }
        AuthenticationManager _manager;
        public LogSession Session { get; }

        void AuthenticationManager_CurrentUserChanged()
        {
            if (_manager.CurrentUserReal == _manager.NoRightLogin) // just to be safe
                return;

            Session.Login_Id = _manager.CurrentUserReal.Person_Id;

            // No need to keep the subscribing anymore
            _manager.CurrentUserChanged -= AuthenticationManager_CurrentUserChanged;

            // Indicate to the base class we are ready to handle all logs (using method Handle)
            IsReady = true;
        }

        protected override void BeforeThreadStart()
        {
            SqlRetry.ThreadInstance = new SqlRetry() { MaxTry = 1 };
        }

        /// <summary>
        /// Any operation to handle the log
        /// This method can throw (ex: network unavailable) but the exception will be ignored (and, obviously, not logged...).
        /// When an exception is raised, the method will be call again (with same argument) a little time later.
        /// Reminder about this method: It must do all work or nothing (ie: must be atomic). 
        /// </summary>
        /// <remarks>This method is executed in another thread !</remarks>
        /// <param name="logsToSend">The logs to handle, atomically</param>
        protected override int Handle(IReadOnlyCollection<DAL.Logging.Log> logsToSend)
        {
            Debug.Assert(IsReady);

            if (Config.Instance.Connections.Environment.Domain != Deployment.Data.eEnvironment.LocalNoDB)
            {
                if (Session.Id == 0)
                {
                    // If this fails _session.Id is not set so we can try again later (according to definition of base.Handle)
                    DB.Dao_Base.CreateInDatabase(Session);
                }
                foreach (var log in logsToSend)
                    log.Session_Id = Session.Id;
                // This is completely atomic (all logs are saved or none)
                DB.Dao_Base.CreateInDatabaseCollection(logsToSend);
            }
            AddRangeToRemember(logsToSend);

            return logsToSend.Count;
        }

        #region Remember the last logs

        public int LastLogCountToKeep { get { return _LastLogCountToKeep; } set { Resize(value); _LastLogCountToKeep = value; } } int _LastLogCountToKeep = DefaultLastLogCountToKeep;
        public static int DefaultLastLogCountToKeep { get { return 42000; } }

        readonly object _lck = new object();
        readonly Queue<DAL.Logging.Log> _lastLogs = new Queue<DAL.Logging.Log>(DefaultLastLogCountToKeep + 1);
        long _totalLogCountCaptured;

        public int LastLogCurrentCount { get { lock (_lck) { return _lastLogs.Count; } } }

        void AddRangeToRemember(IReadOnlyCollection<DAL.Logging.Log> logs)
        {
            lock (_lck)
            {
                foreach (var log in logs)
                {
                    _lastLogs.Enqueue(log);
                    if (_lastLogs.Count > _LastLogCountToKeep)
                        _lastLogs.Dequeue();
                }
                _totalLogCountCaptured += logs.Count;
            }
        }

        void Resize(int newSize)
        {
            lock (_lck)
                while (_lastLogs.Count > _LastLogCountToKeep)
                    _lastLogs.Dequeue();
        }
        
        public (DAL.Logging.Log[] Logs, long TotalLogCountCaptured) GetLastLogs()
        {
            lock (_lck)
                return (_lastLogs.ToArray(), _totalLogCountCaptured);
        }


        #endregion
    }
}
