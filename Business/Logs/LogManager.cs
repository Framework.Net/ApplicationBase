﻿using System;

using TechnicalTools.Logs;


namespace ApplicationBase.Business.Logs
{
    // All these classes are just to override CreateLog needed by DefaultLogSender
    public class LogManager : TechnicalTools.Logs.LogManager
    {
        protected override ILogger CreateLogger(string name)
        {
            return new Logger(this, name);
        }
        public override ILogger CreateLogger(Type type)
        {
            return new Logger(this, type);
        }
    }
    public class Logger : TechnicalTools.Logs.Logger
    {
        protected internal Logger(LogManager manager, string name)
            : base(manager, name)
        {
        }
        protected internal Logger(LogManager manager, Type type, string name = null)
            : base(manager, type, name)
        {
        }
        protected override ILog CreateLog()
        {
            return new DAL.Logging.Log(); // The most important part
        }
    }
}
