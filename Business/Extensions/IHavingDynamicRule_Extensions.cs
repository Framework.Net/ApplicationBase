﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.CSharp;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;

using ApplicationBase.DAL;


namespace ApplicationBase.Business
{
    public interface IDynamicObjectValidator
    {
        LambdaExpression    CheckAsExpression { get; }
        bool Check(object obj);
    }

    public interface IDynamicObjectValidator<in T> : IDynamicObjectValidator
    {
        new Func<T, bool> Check             { get; }
    }

    public static class IHavingDynamicRule_Extensions
    {
        public static string DefaultDynamicMatchingRuleVarName { get; set; } = "line";
        public static string DefaultDynamicMatchingRule(string varname = null, bool defaultResult = false)
        {
            varname = varname ?? DefaultDynamicMatchingRuleVarName;
            return varname + " => " + defaultResult.ToString().ToLowerInvariant();
        }

        public static IDynamicObjectValidator<T> GetChecker<T>(this IHavingDynamicRule rule)
        {
            return rule.BPart<T>().DynamicObjectTypingValidator;
        }
        public static IDynamicObjectValidator GetChecker(this IHavingDynamicRule rule)
        {
            var mGetChecker = gGetChecker.MakeGenericMethod(rule.TypeOnWhichRuleApplies);
            return (IDynamicObjectValidator)mGetChecker.Invoke(null, new object[] { rule });
        }
        static readonly MethodInfo gGetChecker = typeof(IHavingDynamicRule_Extensions).GetMethods().Single(m => m.Name == nameof(GetChecker) && m.GetGenericArguments().Length == 1).ThrowIfNull();

        /// <summary>
        /// Reset the compiled method related to this instance of dynamic rule.
        /// This method is automatically called when IHavingDynamicRule_Extensions.DynamicMatchingRuleChanged is called.
        /// So it should be called manually only for substitution object that implement IHavingDynamicRule
        /// </summary>
        public static void ResetChecker<T>(this IHavingDynamicRule rule)
        {
            rule.BPart<T>().ResetChecker_Impl();
        }
        public static void ResetChecker(this IHavingDynamicRule rule)
        {
            var mResetChecker = gResetChecker.MakeGenericMethod(rule.TypeOnWhichRuleApplies);
            mResetChecker.Invoke(null, new object[] { rule });
        }
        static readonly MethodInfo gResetChecker = typeof(IHavingDynamicRule_Extensions).GetMethods().Single(m => m.Name == nameof(ResetChecker) && m.GetGenericArguments().Length == 1).ThrowIfNull();


        public static Exception GetCompilationException<T>(this IHavingDynamicRule rule)
        {
            return rule.BPart<T>().CompilationException;
        }
        public static Exception GetCompilationException(this IHavingDynamicRule rule)
        {
            var mGetCompilationException = gGetCompilationException.MakeGenericMethod(rule.TypeOnWhichRuleApplies);
            return (Exception)mGetCompilationException.Invoke(null, new object[] { rule });
        }
        static readonly MethodInfo gGetCompilationException = typeof(IHavingDynamicRule_Extensions).GetMethods().Single(m => m.Name == nameof(GetCompilationException) && m.GetGenericArguments().Length == 1).ThrowIfNull();

        public static void CompileAll<T>(IEnumerable<IHavingDynamicRule> rules)
        {
            var bparts = rules.Select(r => r.BPart<T>());
            BusinessPart<T>.CompileAll_Impl(bparts);
        }
        public static void CompileAll(this IEnumerable<IHavingDynamicRule> rules)
        {
            foreach (var grp in rules.GroupBy(r => r.TypeOnWhichRuleApplies))
            {
                var mCompileAll = gCompileAll.MakeGenericMethod(grp.Key);
                mCompileAll.Invoke(null, new object[] { grp });
            }
        }
        static readonly MethodInfo gCompileAll = typeof(IHavingDynamicRule_Extensions).GetMethods().Single(m => m.Name == nameof(CompileAll) && m.GetGenericArguments().Length == 1).ThrowIfNull();



        static BusinessPart<T> BPart<T>(this IHavingDynamicRule rule)
        {
            var bPart = For<T>.Rules.GetValue(rule, r => new BusinessPart<T>(rule));
            return bPart;
        }
        static class For<T>
        {
            public static readonly ConditionalWeakTable<IHavingDynamicRule, BusinessPart<T>> Rules = new ConditionalWeakTable<IHavingDynamicRule, BusinessPart<T>>();
        }

        abstract class BusinessPart
        {
            public abstract IDynamicObjectValidator DynamicObjectTypingValidatorBase { get; }
        }
        class BusinessPart<T> : BusinessPart
        {
            readonly IHavingDynamicRule Extending;

            public BusinessPart(IHavingDynamicRule rule)
            {
                Extending = rule;
                Extending.DynamicMatchingRuleChanged += ResetChecker_Impl;
            }

            public override IDynamicObjectValidator DynamicObjectTypingValidatorBase
            {
                get
                {
                    return DynamicObjectTypingValidator;
                }
            }
            public IDynamicObjectValidator<T> DynamicObjectTypingValidator
            {
                get
                {
                    if (MustBeCompiled)
                        lock (_compilationLock)
                            if (MustBeCompiled)
                                TryCompile();
                    return _dynamicObjectTypingValidator;
                }
                private set { _dynamicObjectTypingValidator = value; }
            }
            IDynamicObjectValidator<T> _dynamicObjectTypingValidator;
            public Exception CompilationException
            {
                get
                {
                    if (MustBeCompiled)
                        lock (_compilationLock)
                            if (MustBeCompiled)
                                TryCompile();
                        
                    return _CompilationException;
                }
                private set { _CompilationException = value; }
            }
            Exception _CompilationException;
            readonly object _compilationLock = new object();

            bool MustBeCompiled
            {
                get { return _dynamicObjectTypingValidator == null && !string.IsNullOrWhiteSpace(Extending.DynamicMatchingRule); }
            }

            internal static void CompileAll_Impl(IEnumerable<BusinessPart<T>> bparts)
            {
                var partsToTreat = bparts.Where(bpart => bpart.MustBeCompiled).ToList();
                if (partsToTreat.Count == 0)
                    return;
                var results = Compile(partsToTreat);
                foreach (var result in results)
                    lock (result.Key._compilationLock)
                    {
                        result.Key._CompilationException = null;
                        result.Key._dynamicObjectTypingValidator = result.Value;
                        Debug.Assert(!result.Key.MustBeCompiled);
                    }
            }

            public void ResetChecker_Impl()
            {
                CompilationException = null;
                DynamicObjectTypingValidator = null;
            }

            void TryCompile()
            {
                try
                {
                    DynamicObjectTypingValidator = string.IsNullOrWhiteSpace(Extending.DynamicMatchingRule)
                                          ? null
                                          : Compile(this.Yield()).First().Value;
                    CompilationException = null;
                    //return true;
                }
                catch (Exception ex)
                {
                    CompilationException = ex;
                    //return false;
                }
            }

            static readonly HashSet<Type> TypeDependencies = new HashSet<Type>
            {
                typeof(int), // For System
                typeof(IList<int>), // For System.Collections.Generic
                typeof(Enumerable), // For System.Linq
                typeof(System.Text.RegularExpressions.Regex),

                typeof(DebugTools),
                typeof(T),
                typeof(IDynamicObjectValidator<T>)
            };
            static string[] ProcessAssembliesToReferLocations()
            {
                var assembliesToRefer = TypeDependencies.Select(t => t.Assembly).Distinct().ToArray();
                var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                var res = assembliesToRefer
                    .Concat(TypeDependencies.Where(t => !t.IsMicrosoftType())
                                            .SelectMany(t => t.Assembly.GetReferencedAssemblies()
                                                                        .Select(refAss => loadedAssemblies.SingleOrDefault(a => a.FullName == refAss.FullName)))
                                                                        .NotNull())
                    .Distinct()
                    .Select(ass => //ass.IsMicrosoftAssembly() // Cause problem with ReferenceAssembly
                                    //? Path.GetFileName(ass.Location) :
                                    ass.Location)
                    .Distinct()
                    .ToArray();
                return res;
            }
            static string[] _AssembliesToReferLocations;
            static CompilerParameters _parameters;
            static Dictionary<BusinessPart<T>, IDynamicObjectValidator<T>> Compile(IEnumerable<BusinessPart<T>> bparts)
            {
                var provider = new CSharpCodeProvider();
                if (_parameters == null)
                {
                    _parameters = new CompilerParameters
                    {
                        GenerateExecutable = false, // True - exe file generation, false - dll file generation
                                                    // Ces trois lignes de code permettent de voir la ligne qui a buggé en live dans Visual Studio!
                        GenerateInMemory = !DebugTools.IsDebug || !DebugTools.IsForDevelopper, // True - memory generation, false - external file generation
                        TempFiles = new TempFileCollection(Environment.GetEnvironmentVariable("TEMP"), false),
                        IncludeDebugInformation = DebugTools.IsDebug,
                        TreatWarningsAsErrors = true
                    };
                    if (_AssembliesToReferLocations == null)
                        _AssembliesToReferLocations = ProcessAssembliesToReferLocations();
                    //else
                    //    DebugTools.Assert(_AssembliesToReferLocations.SequenceEqual(ProcessAssembliesToReferLocations()));
                    _parameters.ReferencedAssemblies.AddRange(_AssembliesToReferLocations);
                }
                var parameters = new CompilerParameters(_AssembliesToReferLocations) // CompilerParameters is not sharable / thread safe
                {
                    GenerateExecutable = _parameters.GenerateExecutable,
                    GenerateInMemory = _parameters.GenerateInMemory,
                    TempFiles = new TempFileCollection(_parameters.TempFiles.TempDir, _parameters.TempFiles.KeepFiles),
                    IncludeDebugInformation = _parameters.IncludeDebugInformation,
                    TreatWarningsAsErrors = _parameters.TreatWarningsAsErrors
                };

                string assemblyName = "DynamicObjectValidator_GeneratedCode";
                string source = $@"
                using System; 
                using System.Collections.Generic;
                using System.Diagnostics;
                using System.Linq;
                using System.Linq.Expressions;
                using System.Text.RegularExpressions; // To allow user to use Regex

                using ApplicationBase.Business;

                using TechnicalTools;


                namespace {assemblyName}
                {{
#pragma warning disable 612, 618 // Type or member is obsolete
";
                var funcType = typeof(IDynamicObjectValidator<T>).GetProperty(nameof(IDynamicObjectValidator<T>.Check)).PropertyType.ToSmartString(true).Replace("System.Func<", "Func<");
                var exprType = typeof(Expression).Name + "<" + funcType + ">";
                var classNames = new Dictionary<BusinessPart<T>, string>();
                foreach (var bpart in bparts)
                {
                    var ruleId = ((IdTuple<int>)bpart.Extending.Id).Id1;
                    string className = nameof(IDynamicObjectValidator).Substring(1) + ruleId.ToString().Replace("-", "_") + "_" + _AttemptByRuleIds.AddOrUpdate(ruleId, _ => 0, (_, i) => i + 1);
                    classNames.Add(bpart, className);

                    // ReSharper disable once PossibleNullReferenceException
                    source += $@"
                    public class {className} : {typeof(IDynamicObjectValidator<T>).ToSmartString(true).Replace("+", ".")}
                    {{
                        public {funcType} {nameof(IDynamicObjectValidator<T>.Check)} {{ get; private set; }}
                        bool {nameof(IDynamicObjectValidator)}.{nameof(IDynamicObjectValidator.Check)}(object obj) {{ return Check(({typeof(T).ToSmartString(true)})obj); }}
                        public {exprType} {nameof(IDynamicObjectValidator.CheckAsExpression)} {{ get; private set; }}
                               {typeof(IDynamicObjectValidator).GetProperty(nameof(IDynamicObjectValidator.CheckAsExpression)).PropertyType.Name} {nameof(IDynamicObjectValidator)}.{nameof(IDynamicObjectValidator.CheckAsExpression)} {{ get {{ return {nameof(IDynamicObjectValidator.CheckAsExpression)}; }} }}

                        public {className}()
                        {{
                            {nameof(IDynamicObjectValidator<T>.Check)} = {bpart.Extending.DynamicMatchingRule};
                            CheckAsExpression = ExpressionBuilder.BuiltExpression;
                        }}
                        //static {funcType} Cast<TDerived>(Func<TDerived, bool> func)
                        //    where TDerived : {typeof(T).ToSmartString(true)}
                        //{{
                        //    return DynamicObjectValidatorFeature.Cast<{typeof(T).ToSmartString(true)}, TDerived>(func);
                        //}}

                        static class ExpressionBuilder {"" /* Allow compiler to link differently the "same" code */ }
                        {{
                            public static {exprType} BuiltExpression
                            {{
                                get
                                {{
                                    return {bpart.Extending.DynamicMatchingRule};
                                }}
                            }}
                            //static {exprType} Cast<TDerived>(Expression<Func<TDerived, bool>> expr)
                            //    where TDerived : {typeof(T).ToSmartString(true)}
                            //{{
                            //    return {nameof(DynamicObjectValidatorFeatureForExpression)}.{nameof(DynamicObjectValidatorFeatureForExpression.Cast)}<{typeof(T).ToSmartString(true)}, TDerived>(expr);
                            //}}
                        }}
                    }}";
                }
                source += @"
#pragma warning restore 612, 618 // Type or member is obsolete
                }";

                // The method CompileAssemblyFromSource internally calls System.Threading.WaitHandle.WaitOne(int millisecondsTimeout, bool exitContext)
                // which can cause message pumping and call OnPaint event of some controls...
                // Sometimes the compilation is done inside the constructor of a singleton, and this singleton is used by handler called through OnPaint event
                // (for example the event CustomUnboundColumnData). Because we are in same thread, this cause a null exception
                // So we let the responsability to handle any UI refresh issues to caller, and not here by having code that cause some Application.DoEvents evil stuff inside technical and business code
                CompilerResults results;
                using (var mre = new ManualResetEvent(false))
                {
                    // ReSharper disable once AccessToDisposedClosure
                    var task = Task.Run(() => { try { return provider.CompileAssemblyFromSource(parameters, source); } finally { mre.Set(); } });
                    mre.WaitOneNonAlertable(); // This does not cause message pumping of some event (tested :))
                    results = task.Result;
                    results.TempFiles?.Delete();
                }

                if (results.Errors.HasErrors)
                {
                    string errMsg = "Error compiling script !" + Environment.NewLine +
                                    results.Errors.Cast<CompilerError>().Where(error => !error.IsWarning).Select(error => $"Error {error.ErrorNumber} (Line {error.Line}, Column {error.Column}) : {error.ErrorText}").Join(Environment.NewLine) +
                                    Environment.NewLine +
                                    results.Errors.Cast<CompilerError>().Where(error => error.IsWarning).Select(error => $"Warning {error.ErrorNumber} (Line {error.Line}, Column {error.Column}) : {error.ErrorText}").Join(Environment.NewLine);
                    throw new CompilingException(errMsg, null);
                }

                
                var result = classNames.ToDictionary(kvp => kvp.Key,
                    kvp =>
                    {
                        Assembly assembly = results.CompiledAssembly;
                        var type = assembly.GetType(assemblyName + "." + classNames[kvp.Key]);
                        var cons = type.GetConstructor(Type.EmptyTypes);
                        Debug.Assert(cons != null, nameof(cons) + " != null");
                        var checker = (IDynamicObjectValidator<T>)cons.Invoke(null);
                        return checker;
                    });
                return result;
            }
            // ReSharper disable once StaticMemberInGenericType
            static readonly ConcurrentDictionary<int, int> _AttemptByRuleIds = new ConcurrentDictionary<int,int>();
        }

        public class CompilingException : TechnicalException
        {
            public CompilingException(string message, Exception innerException)
               : base(message, innerException)
            { }
        }

        public readonly static string ChangedByDevelopperToken = "/*# Changed By Developper #*/";
        public readonly static string RecompilationErrorToken = "/*# Recompilation Error #*/";
    }

    // These classes are here because methods inside them are static to be referenced by any transpiler / visitor later.
    // Developper can use them in rule too
    #region 
    public static class DynamicObjectValidatorFeature
    {
        public static Func<TFrom, bool> Cast<TFrom, TTo>(Func<TTo, bool> func)
            where TTo : TFrom
        {
            return obj => func((TTo)obj);
        }
    }
    
    public static class DynamicObjectValidatorFeatureForExpression
    {
        public static Expression<Func<TFrom, bool>> Cast<TFrom, TTo>(Expression<Func<TTo, bool>> expr)
            where TTo : TFrom
        {            
            var p = Expression.Parameter(typeof(TFrom));
            var conversion = Expression.Convert(p, typeof(TTo));
            var call = Expression.Invoke(expr, conversion);
            return Expression.Lambda<Func<TFrom, bool>>(call, p.WrapInList());

        }
    }
    #endregion
}
