﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

using TechnicalTools;
using TechnicalTools.Automation;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.Tools;

using ApplicationBase.Deployment;
using ApplicationBase.Common;


namespace ApplicationBase.Business
{
    /// <summary>
    /// Base class for your Program class in console app or windows form app.
    /// <p>
    /// Important issues/gotcha to know with windows environement: 
    /// When running of windows form application type (OutputType=WinExe in csproj) from windows shell (cmd / cmdr / powershell / etc)
    /// The shell does not wait for the return of application (when shell is interactive).
    /// Assuming your winform application is named "App.exe", the followwing command line :
    /// $  App.exe --execute=RaiseException --Domain=LocalNoDB
    /// would not update %errorlevel% expansion variable so your script would probably fails to check for error.
    /// there is a fix howerver : prefixing command line by "start /wait ". Example:
    /// $ start /wait  App.exe --execute=RaiseException --Domain=LocalNoDB
    /// After this execution, the command "echo %errorlevel%" write "1" on output as expected (instead of "0")
    /// </p>
    /// <p>
    /// But there is still a problem... The command RaiseException is expected to write the exception message on error output stream 
    /// (because exception generate log and log are redirect to Console.{Out|Error}.Write by default
    /// There is some hackish possible thing using AllocCOnsole and argument --ShowConsole=true with winforms app
    /// But we cannot capture the output easily it in shell script. Same with --execute=Help
    /// In C#, using class System.Diagnostics.Process, we cna howerver get the return value of program but also all stream.
    /// </p>
    /// Because all of this problem, i strongly recommend to create an application App.exe of type "Console" (OutputType=Exe in csproj), put all your business logic in there
    /// And create a second project called for example "App.Gui.exe" which contains only the programm class with the folowwing code.
    /// We still return the exit code to make it usable by C# Process class, and maybe for UI testing automation tool.
    /// <code>
    /// [STAThread]
    /// static int Main(string[] args)
    /// {
    ///     // Read ApplicationBase.Business to understand the aim of this project that seems a useless wrapper
    ///     return CLI.Program.Main(args);
    /// }
    /// </code>
    /// </summary>
    public abstract class Program
    {
        public bool LogTaskStartAndEnd { get; set; }

        protected virtual  FailingAssertionListener CreateFailingAssertionListener(IEnumerable<TraceListener> listeners) { return new FailingAssertionListener(listeners); }
        protected virtual  DebugConfig              CreateDebugConfig() { return new DebugConfig(); }
        protected abstract BootstrapConfig          CreateBootstrapConfig();
        protected virtual  AuthenticationManager    GetAuthenticationManager() { return BusinessSingletons.Instance.GetAuthenticationManager(); }

        protected virtual void Initialize()
        {
            RuntimeHelpers.RunClassConstructor(typeof(ExceptionManager).TypeHandle);
            RuntimeHelpers.RunClassConstructor(typeof(AssertionManager).TypeHandle);

            LogManager.Default = new Logs.LogManager();
            _log = LogManager.Default.GetLogger(typeof(Program));
            CreateDefaultLogHandler();

            var listener = CreateFailingAssertionListener(Debug.Listeners.Cast<TraceListener>());
            Debug.Listeners.Clear();
            Debug.Listeners.Add(listener);

            // This line force the BusinessSingletons type to be initialized too, this is what we want (at least)
            var dbLogSender = BusinessSingletons.Instance.GetDbLogSender();
            dbLogSender.AddOwner(this);
        }

        protected virtual void CreateDefaultLogHandler()
        {
        }
        protected virtual void ConfigureLogHandler(string config)
        {
        }


        public virtual int Run(string[] args)
        {
            Initialize();

            var remainingArgs = ReadTechnicalDataFromCommandLine(args, out BootstrapConfig bcfg, out DebugConfig dcfg);
            ConfigureLogHandler(dcfg.LogConfigPath);
            BootstrapConfig.Instance = bcfg;
            var moduleName = GetType().Assembly.ManifestModule.Name;
            Debug.Assert(moduleName == bcfg.CliExecutableName || moduleName == bcfg.GuiExecutableName);

            // This line is expected to be the first log of application
            _log.Info(bcfg.ApplicationName + ": Starting");

            // Begin to work..
            var envFilter = new Filter() { Domain = bcfg.Domain, EnvName = bcfg.EnvName };
            var cfg = CreateConfig();
            LoadConfig(cfg, bcfg, envFilter);
            if (cfg == null)
            {
                // no default env, 
                // TODO : Use GetDefaultEnvironmentsFromDb and GetAvailableEnvironmentFromFiles and display possibilities to user
                throw new NotImplementedException("Environment selection not implemented in splashscreen... yet !" + Environment.NewLine +
                                                  "Make sure there is only one .cfg file or set IsDefaultEnvToStart value to true for one of them");
            }
            cfg.Debug.CopyFrom(dcfg);

            OnConfigLoaded(cfg, bcfg);

            try
            {
                var res = RecogniseAndRunTask(remainingArgs, cfg);
                _log.Info(bcfg.ApplicationName + ": Closing");
                return res;
            }
            finally
            {
                FinalizeAssemblies(cfg, bcfg);
            }
        }
        protected ILogger _log; // NOT initialized here !

        protected virtual List<string> ReadTechnicalDataFromCommandLine(string[] args_, out BootstrapConfig bcfg, out DebugConfig dcfg)
        {
            var args = args_.Where(a => !a.StartsWith("#"))
                            .Select(a => a.Replace("%USERNAME%", Environment.UserName))
                            .ToList();
            if (DebugTools.IsForDevelopper)
            {
                // Direct launch without argument makes application to end immediately
                // This is to force user to run application through launcher...
                // This way Launcher can force user to update application.
                // and we, developpers, are sure they always execute a valid version of application.
                //
                // Developpers bypass this check when they are running form Visual Studio
                // Because the build version is not official
                //if (args.All(a => !a.ToLowerInvariant().StartsWith("--execute=")))
                //    args.Add("--execute=" + DefaultCommandType.FullName);
                if (args.All(a => !a.ToLowerInvariant().StartsWith("--" + nameof(Filter.Domain).ToLowerInvariant() + "=")))
                    args.Add("--" + nameof(Filter.Domain) + "=" + Filter.DefaultDev.Domain);
                if (args.All(a => !a.ToLowerInvariant().StartsWith("--" + nameof(Filter.EnvName).ToLowerInvariant() + "=")))
                    args.Add("--" + nameof(Filter.EnvName) + "=" + Filter.DefaultDev.EnvName);
            }
            var parser = new CommandLineParser(true, true);
            var switches = parser.ParseAndHydrate<DevelopperSwitches>(args);
            switches.Apply();

            dcfg = CreateDebugConfig();
            parser.ParseAndHydrate(args, dcfg);

            bcfg = CreateBootstrapConfig();
            parser.ParseAndHydrate(args, bcfg);

            if (!string.IsNullOrWhiteSpace(dcfg.AutoLoginWith) &&
                bcfg.DefaultInitConnectionString == bcfg.InitialConnectionString && dcfg.AutoPassword == null)
                throw new TechnicalException($"When argument \"{nameof(DebugConfig.AutoLoginWith)}\" is defined, argument \"{nameof(DebugConfig.AutoPassword)}\" must be defined too." + Environment.NewLine + 
                                             $"Or you can specify \"{nameof(BootstrapConfig.InitialConnectionString)}\" granted to read logins table!", null);
            if (bcfg.Domain == Deployment.Data.eEnvironment.LocalNoDB && string.IsNullOrWhiteSpace(dcfg.AutoLoginWith))
                dcfg.AutoLoginWith = Environment.UserName;

            return args;
        }

        protected string[] SetDefaultEnvNameAsArgument(string[] args, string envName)
        {
            return DefineIfDontExist(args, nameof(Filter.EnvName), envName);
        }
        protected string[] SetDefaultDomainAsArgument(string[] args, Deployment.Data.eEnvironment env)
        {
            return DefineIfDontExist(args, nameof(Filter.Domain), env.ToString());
        }
        protected string[] SetDefaultCommandAsArgument(string[] args, Type type)
        {
            return DefineIfDontExist(args, nameof(AutomationTaskBuilder.Execute), type.Name);
        }
        string[] DefineIfDontExist(string[] args, string argName, string value)
        {
            string fullArgName = "--" + argName.ToLowerInvariant() + "=";
            if (args.All(a => !a.ToLowerInvariant().StartsWith(fullArgName)))
                return args.Concat(new[] { fullArgName + value }).ToArray();
            return args;
        }

        protected virtual Config CreateConfig()
        {
            RuntimeHelpers.RunClassConstructor(typeof(BusinessSingletons).TypeHandle);
            return new Config();
        }

        protected virtual void LoadConfig(Config cfg, BootstrapConfig bcfg, Filter envFilter)
        {
            var success = Config.FillDefaultEnvironment(cfg, bcfg, envFilter);
            if (!success)
                throw new UserUnderstandableException("No allowed environment to connect to!", null);
        }

        protected virtual void OnConfigLoaded(Config cfg, BootstrapConfig bcfg)
        {
            BootstrapConfig.Instance = bcfg;
            Config.Instance = cfg;

            if (!string.IsNullOrWhiteSpace(cfg.Debug.LogConfigPath) &&
                 Path.GetFullPath(cfg.Debug.LogConfigPath) != DebugConfig.DefaultLogConfigPath)
                ConfigureLogHandler(cfg.Debug.LogConfigPath);

            bool firstTime = true;
            GetAuthenticationManager().CurrentUserChanged += () =>
            {
                if (firstTime)
                {
                    DB.Dao_Base.CheckEnumsAreLoaded();
                    BootstrapConfig.Instance.InitialConnectionString = cfg.BaseConnections.ConnectionString_Base;
                }
                firstTime = false;
            };
        }

        protected virtual Automation.AutomationTaskBuilder CreateAutomationTaskBuilder(List<string> args, Config cfg)
        {
            return new Automation.AutomationTaskBuilder(args, cfg);
        }
        protected virtual int RecogniseAndRunTask(List<string> args, Config cfg)
        {
            var recogniser = CreateAutomationTaskBuilder(args, cfg);
            var res = recogniser.Run();
            if (res != CommandLineTask.SUCCESS)
            {
                _log.Error(recogniser.InvalidTaskCommandLineReason);
                return res;
            }
            if (LogTaskStartAndEnd)
                _log.Audit(GetTaskStartMessage(recogniser.RecognisedTask.GetType().FullName) + Environment.NewLine + 
                            args.Select(a => a.ToLowerInvariant().Contains("-pwd")
                                          || a.ToLowerInvariant().Contains("-pass") ? a.Remove(Math.Max(0, a.IndexOf("="))) + "=<a password>" : a).Join(" "));
            res = recogniser.RecognisedTask.Run();
            if (LogTaskStartAndEnd)
                _log.Audit(GetTaskEndMessage(recogniser.RecognisedTask.GetType().FullName, res));
            return res;
        }
        public virtual string GetTaskStartMessage(string taskName)                  { return $"Task \"{taskName}\" started with arguments"; }
        public virtual string GetTaskEndMessage(string taskName, int resultValue)   { return $"Task \"{taskName}\" ends with return value {resultValue.ToStringInvariant()}"; }
        public static Regex TaskEndMessagePattern { get; } = new Regex("Task \"(?<taskName>[^\"]*)\" ends with return value (?<resultValue>[^\"]*)", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Compiled);

        protected virtual void FinalizeAssemblies(Config cfg, BootstrapConfig bcfg)
        {
            BusinessSingletons.Instance.GetDbLogSender().Release(this);
        }
    }
}
