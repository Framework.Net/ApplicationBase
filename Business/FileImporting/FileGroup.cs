﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using TechnicalTools.Model;


namespace ApplicationBase.Business.FileImporting
{
    /// <summary>
    /// Allow to group file and make some treatment by group of file
    /// </summary>
    public interface IFileSource : IReadOnlyCollection<string>, IIsNamed
    {
    }

    [DebuggerDisplay("{" + nameof(Name) + ",nq}")]
    public class FileGroup : IFileSource, IReadOnlyCollection<string>, ITreeNodeWithParentReadable
    {
        public string Name { get; }
        public IReadOnlyCollection<string> Files { get; }
        public int Count { get { return Files.Count; } }

        public FileGroup(string name, IReadOnlyCollection<string> files)
        {
            Name = name;
            Files = files;
            _firstFile = files.OrderBy(f => f).FirstOrDefault() ?? "";
        }
        public IEnumerator<string> GetEnumerator()
        {
            return Files.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator() { return Files.GetEnumerator(); }


        public override string ToString() { return Name; }

        ITreeNodeWithParentReadable ITreeNodeWithParentReadable.ParentNode { get { return null; } }

        public override int GetHashCode()
        {
            return Files.Count ^ _firstFile.GetHashCode() ^ Name.GetHashCode();
        }
        readonly string _firstFile;
        public override bool Equals(object obj)
        {
            var grp = obj as FileGroup;
            if (grp == null)
                return false;
            if (Name != grp.Name)
                return false;
            return Files.OrderBy(file => file)
                         .SequenceEqual(grp.OrderBy(file => file));
        }
    }

}
