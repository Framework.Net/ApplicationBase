﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Dynamic;
using System.Text;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Tools;

using DataMapper;


namespace ApplicationBase.Business.FileImporting
{
    public class GenericDataCSVFileImporter : FileImport
    {
        IReadOnlyCollection<ExpandoObject>        _items;
        IReadOnlyList<CSVSmartParser.IColumnInfo> _cols;

        public GenericDataCSVFileImporter(string filename)
            : base("Any CSV Data with first line as Title")
        {
            Filename = filename;
        }
        
        protected internal override void DoRead()
        {
            var parser = new CSVSmartParser();
            (var records, var titles, var warnings) = parser.Read(Filename, true);
            _WarningsOnRead.Clear();
            _WarningsOnRead.AddRange(warnings);
            var captions = titles.ToDictionary(col => col.FieldName, col => col.Title);
            var superCaptions = titles.ToDictionary(col => col.FieldName, col => col.SuperTitle);

            _cols = titles;
            _items = records.ToList();
            ParsedItems = _items;
            Captions = captions;
            SuperCaptions = superCaptions;
        }

        protected override void DoSend()
        {
            throw new NotImplementedException();
        }


        public override bool CanGenerateCodeFromThisFile { get { return true; } }
        public override Dictionary<string, string> GenerateCodeFromThisFile(string tableOrClassName, string schemaOrNamespace = null)
        {
            if (_items == null || _items.Count == 0)
                throw new UserUnderstandableException("There is no line to deduce type of columns", null);

            var files = new Dictionary<string, string>
            {
                { "SqlTable",      GenerateSqlTableCode   (tableOrClassName, schemaOrNamespace).ToString() },
                { "C# POCO class", GenerateCSharpClassCode(tableOrClassName, schemaOrNamespace).ToString() }
            };

            return files;
        }

        const String_Extensions.ePadding right = String_Extensions.ePadding.Right;
        const String_Extensions.ePadding left = String_Extensions.ePadding.Left;
        const String_Extensions.ePadding none = String_Extensions.ePadding.None;

        StringBuilder GenerateSqlTableCode(string tableName, string schema)
        {
            var sql = new StringBuilder();
            sql.Append($"CREATE TABLE ");
            sql.Append("[".AsPrefixForIfNotBlank(schema).IfNotBlankAddSuffix("]."));
            sql.AppendLine("[" + tableName + "]");
            sql.AppendLine("{");
            var columns = new List<string[]>
            {
                new[]
                {
                    "[Id]",
                    "int",
                    "NOT NULL ",
                    "IDENTITY(1,1) PRIMARY KEY,",
                },
                new[]
                {
                    "[date_insert]",
                    "datetime2(2)",
                    "NOT NULL,",
                    "",
                }
            };
            sql.AppendLine();
            foreach (var col in _cols)
            {
                string sqlType;
                if (col.SupposedType == typeof(string))
                    sqlType = "nvarchar(" + (col.MaxLength > 4000 ? "max" : col.MaxLength.ToStringInvariant()) + ")";
                else if (col.SupposedType == typeof(decimal))
                    sqlType = "numeric(18,4)";
                else if (col.SupposedType == typeof(long))
                    sqlType = "bigint";
                else if (col.SupposedType == typeof(short))
                    sqlType = "smallint";
                else if (col.SupposedType == typeof(bool))
                    sqlType = "bit";
                else if (col.SupposedType == typeof(TimeSpan))
                    sqlType = "datetime2(7)";
                else if (col.SupposedType == typeof(DateTime))
                {
                    int type = -1;
                    var ticksFor1ms = new TimeSpan(0, 0, 0, 0, 1).Ticks;
                    var ticksFor10ms = new TimeSpan(0, 0, 0, 0, 10).Ticks;
                    foreach (var item in _items)
                    {
                        var rawValue = ((IDictionary<string, object>)item)[col.FieldName];
                        if (rawValue == null)
                            continue;
                        var value = (DateTime)rawValue;
                        if (type <= -1 && value != value.Date)
                            type = 0;
                        if (type <= -1 && value != value.TruncateUnderSeconds())
                            type = 2;
                        if (type <= 1 && (value.Ticks % ticksFor10ms) != 0)
                            type = 3;
                        if (type <= 2 && (value.Ticks % ticksFor1ms) != 0)
                            type = 7;
                    }
                    if (type == -1)
                        sqlType = "date";
                    else
                        sqlType = "datetime2(" + type + ")";
                }
                else
                    sqlType = "unknown";
                var nullable = _items.Any(item => ((IDictionary<string, object>)item)[col.FieldName] == null);
                columns.Add(new[]
                {
                    "[" + ToPropertyName(col.Title) + "]",
                    sqlType,
                    (nullable ? "" : "NOT ") + "NULL,",
                    "",
                });
            }
            columns.AlignAndMerge(new[] { right, left, left, none }, sql, "    ");
            sql.Length = sql.Length - ",".Length; // Remove trailing comma
            sql.AppendLine();
            sql.AppendLine();
            sql.AppendLine("}");
            return sql;
        }

        StringBuilder GenerateCSharpClassCode(string className, string @namespace)
        {
            var cs = new StringBuilder();
            string indentation = "";
            string indentationElt = "    ";
            cs.Append(indentation); cs.AppendLine("using System;");
            cs.Append(indentation); cs.AppendLine("using System.Diagnostics;");
            cs.AppendLine();
            cs.Append(indentation); cs.AppendLine("using TechnicalTools.Model.Cache;");
            cs.AppendLine();
            cs.Append(indentation); cs.AppendLine("using DataMapper;");
            cs.AppendLine();
            cs.AppendLine();
            if (!string.IsNullOrEmpty(@namespace))
            {
                cs.AppendLine("namespace " + @namespace);
                cs.AppendLine("{");
                indentation = indentationElt;
            }
            cs.Append(indentation); cs.AppendLine("[" + nameof(DbMappedTable) + "(" + (string.IsNullOrEmpty(@namespace) ? "" : "schemaName: \"" + @namespace + "\", ") + "tableName: \"" + className + "\")]");
            cs.Append(indentation); cs.AppendLine("public sealed class " + className + " : BaseDTO<IdTuple<int>>");
            cs.Append(indentation); cs.AppendLine("{");

            indentation += indentationElt;
            var propElts = new List<string[]>
            {
                new[]
                {
                    "[DebuggerHidden][DbMappedField(\"Id\", IsAutoPK = true)]",
                    "public",
                    typeof(int).ToSmartString(),
                    "Id",
                    "{ get; set; }"
                },
                new[]
                {
                    "[DebuggerHidden][DbMappedField(\"date_insert\")]",
                    "public",
                    typeof(DateTime).ToSmartString(),
                    "DateInsert",
                    "{ get; set; }"
                }
            };
            foreach (var col in _cols)
            {
                var propName = ToPropertyName(col.Title);
                propElts.Add(new[]
                {
                    "[DebuggerHidden][DbMappedField\"" + propName + "\""
                        + (col.IsNullable ? ", IsNullable = true" : "") + ")"
                        + (col.MaxLength != 0 && col.MaxLength <= 4000 ? ", DbMaxLength(" + col.MaxLength.ToStringInvariant() + ")" : "")
                        + "]",
                    "public",
                    col.SupposedType.ToSmartString(),
                    propName,
                    "{ get; set; }"
                });
            }
            propElts.AlignAndMerge(new[] { right, none, right, right, none }, cs, indentation); cs.AppendLine();
            cs.AppendLine();
            cs.AppendLine();
            cs.Append(indentation); cs.AppendLine("protected override IdTuple<int> ClosedId");
            cs.Append(indentation); cs.AppendLine("{");
            cs.Append(indentation); cs.AppendLine("    get { return new IdTuple<int>(Id); }");
            cs.Append(indentation); cs.AppendLine("    set { Id = value.Id1; }");
            cs.Append(indentation); cs.AppendLine("}");
            cs.AppendLine();
            cs.AppendLine();
            cs.Append(indentation); cs.AppendLine("#region Cloneable");
            cs.AppendLine();
            cs.Append(indentation); cs.AppendLine($"public new {className} Clone() {{ return ({className})(this as ICloneable).Clone(); }}");
            cs.Append(indentation); cs.AppendLine($"protected override BaseDTO CreateNewInstance() {{ return new {className}(); }}");
            cs.Append(indentation); cs.AppendLine($"public override void CopyAllFieldsFrom(BaseDTO source)");
            cs.Append(indentation); cs.AppendLine("{");
            cs.Append(indentation); cs.AppendLine("    var from = (" + className + ")source;");
            foreach (var col in _cols)
            {
                var propName = ToPropertyName(col.Title);
                cs.Append(indentation); cs.AppendLine($"    {propName} = from.{propName};");
            }
            cs.Append(indentation); cs.AppendLine("}");
            cs.AppendLine();
            cs.Append(indentation); cs.AppendLine("#endregion");
            cs.AppendLine();

            indentation = indentation.Remove(indentation.Length - indentationElt.Length);
            cs.Append(indentation); cs.AppendLine("}"); // end of class

            if (!string.IsNullOrEmpty(@namespace))
                cs.AppendLine("}");  // end of namespace
            return cs;
        }
        protected virtual string ToPropertyName(string columnTitle)
        {
            return new string(columnTitle.Capitalize()
                                         .Where(c => char.IsLetterOrDigit(c)).ToArray());
        }
    }
}
