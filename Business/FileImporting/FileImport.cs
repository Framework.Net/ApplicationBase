﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using TechnicalTools.Logs;
using TechnicalTools.Model;


namespace ApplicationBase.Business.FileImporting
{
    public abstract class FileImport
    {
        public   string    Name          { get; private set; }
        public   string    Filename      { get; protected set; }
        /// <summary>
        /// Target filename if <see cref="Filename"/> needs to be moved after Send/DoSend action is sucessful
        /// </summary>
        public string    FilenameMoved { get; protected set; }

        public void Read()
        {
            if (Sent)
                throw new WontBeReadAgainException("File won't be read again because its content has been sent.");

            _log.Audit($"Reading/Parsing {Name} (file \"{Filename}\")...");
            DoRead();
            if (ParsedItems == null)
                throw new TechnicalException(nameof(ParsedItems) + " must be filled!", null);
            _log.Audit($"Reading of file {Filename} is OK. ({ParsedItems.Count} interesting items have been found in file).");
        }
        public class WontBeReadAgainException : UserUnderstandableException
        {
            public WontBeReadAgainException(string message)
                : base(message, null)
            {
            }
        }
        /// <summary> Must fill property <see cref="ParsedItems"/> </summary>
        protected internal abstract void DoRead();
        public IReadOnlyCollection<object>         ParsedItems      { get; protected set; }
        public IReadOnlyCollection<string>         WarningsOnRead   { get { return _WarningsOnRead; } } protected readonly List<string> _WarningsOnRead = new List<string>();  
        public IReadOnlyDictionary<string, string> Captions         { get; protected set; }
        public IReadOnlyDictionary<string, string> SuperCaptions    { get; protected set; }
        

        public virtual IReadOnlyList<HierarchyDescription> ChildrenDescriptions { get { return null; } }

        /// <summary>
        /// Send content of <see cref="ParsedItems"/> (or a subset) to DB.
        /// <see cref="ParsedItems"/> should be untouched after.
        /// </summary>
        public void Send()
        {
            if (ParsedItems == null)
                throw new UserUnderstandableException($"The {nameof(Read)} operation must be successful before you can send!", null);
            if (Sent)
                return;

            _log.Audit($"Sending file \"{Filename}\" content ({ParsedItems.Count} items) to DB.");
            DoSend();
            Sent = true;
            _log.Audit($"Sending done.");
            if (!string.IsNullOrWhiteSpace(FilenameMoved))
                MoveFile(Filename, FilenameMoved);
        }
        protected abstract void DoSend();
        public   bool      Sent          { get; private set; }
        

        protected virtual void MoveFile(string fileFrom, string fileTo)
        {
            _log.Audit($"Moving file \"{fileFrom}\" to \"{fileTo}\"...");
            File.Move(fileFrom, fileTo);
            _log.Audit($"Moving done.");
        }

        protected FileImport(string name)
        {
            Name = name;
            _log = LogManager.Default.GetLogger(GetType());
        }
        protected readonly ILogger _log;

        public virtual bool CanGenerateCodeFromThisFile { get { return false; } }
        // Returns filename => content
        public virtual Dictionary<string, string> GenerateCodeFromThisFile(string singularName, string schema = null) { throw new NotImplementedException(); }
    }
}
