﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TechnicalTools;
using TechnicalTools.Model;


namespace ApplicationBase.Business.FileImporting
{
    public class AnyDataFileReader : IUserInteractiveObject
    {
        public string         Filename                { get; private set; }
        public Interpretation DisplayedInterpretation { get; private set; }

        public Func<Dictionary<string, string>> GenerateSourceCode { get; set; }

        public AnyDataFileReader()
        {
        }

        public AnyDataFileReader(string filename, Interpretation interpretation)
        {
            Filename = filename;
            DisplayedInterpretation = interpretation;
        }



        public virtual AnyDataFileReader CreateNew()
        {
            return new AnyDataFileReader();
        }

        

        public async Task<List<Interpretation>> GetDataSourceFrom(string filepath, IProgress<string> pr)
        {
            var res = await ReadActions.ForEachParallelSafelyAsDictionary("Analysing", pr, readAction => readAction(filepath)).ConfigureAwait(false);
            if (res.Item1.Any())
                Filename = filepath;
            return res.Item1.Values.ToList();
        }

        public class Interpretation : IHierarchyDescription
        {
            public FileImport                                 Importer             { get; set; }
            public string                                     Name                 { get; set; }
            public IReadOnlyCollection<object>                DataSource           { get; set; }
            public IReadOnlyDictionary<string, string>        Captions             { get; set; }
            public IReadOnlyDictionary<string, string>        SuperCaptions        { get; set; }
            public IReadOnlyCollection<IHierarchyDescription> ChildrenDescriptions { get; set; }

            Type                                             IHierarchyDescription.For                  { get { return null; } }
            Type                                             IHierarchyDescription.ChildType            { get { return null; } }
            HierarchyDescriptionGraph                        IHierarchyDescription.OwnerGraph           { get; set; }
            Func<Stack<object>, IReadOnlyCollection<object>> IHierarchyDescription.GetChildren          { get { return  (_) => DataSource; } }
            Func<Stack<object>, int>                         IHierarchyDescription.GetChildrenCount     { get { return (_) => DataSource.Count; } }

            string IHierarchyDescription.LevelName
            {
                get { return DataSource.GetType().GetGenericArguments()[0].Name; }
            }
        }

        public void Accept(Interpretation interpretation)
        {
            DisplayedInterpretation = interpretation;
            if (interpretation.Importer is GenericDataCSVFileImporter csvImporter)
                GenerateSourceCode = () => csvImporter.GenerateCodeFromThisFile("ClassName", "Dbo");
        }

        protected virtual IEnumerable<Func<string, Interpretation>> ReadActions
        {
            get
            {
                return AllReadActions;
            }
        }
        static readonly Func<string, Interpretation>[] AllReadActions =
        {
            filepath => FromFileImport(new GenericDataCSVFileImporter(filepath)),
        };

        protected static Interpretation FromFileImport(FileImport importer)
        {
            importer.DoRead();
            return new Interpretation()
            {
                Importer = importer,
                Name = importer.Name,
                DataSource = importer.ParsedItems,
                Captions = importer.Captions,
                SuperCaptions = importer.SuperCaptions,
                ChildrenDescriptions = importer.ChildrenDescriptions
            };
        }
    }
  
}
