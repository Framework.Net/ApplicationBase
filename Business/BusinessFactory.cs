﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

using TechnicalTools.Logs;

using DataMapper;

using ApplicationBase.DAL.Users;
using ApplicationBase.Common;


namespace ApplicationBase.Business
{
    public class BusinessFactory
    {
        public BusinessFactory()
        {
        }

        public virtual string CreateRecommendedLocalDataFolder(string wishedFolderName)
        {
            wishedFolderName = wishedFolderName ?? "Data";
            return !string.IsNullOrWhiteSpace(wishedFolderName) && Directory.Exists(wishedFolderName) ?
                  wishedFolderName // local folder exist we take it (take precedence)
                  : Debugger.IsAttached ?
                  @"..\..\..\..\"  + wishedFolderName // if we are in debug mode we get the folder at root
                  : wishedFolderName; // otherwise we are in prod, we pick the local path
        }

        public virtual                      Logs.DefaultDbLogSender      CreateDbSenderLogger()                                       { return new Logs.DefaultDbLogSender((Logs.LogManager)LogManager.Default, null /* Because current method is just a factory */); }
        public virtual                           AuthenticationManager   CreateAuthenticationManager(Deployment.Data.EnvironmentConfig cfg)
                                                                                                                                      { return new AuthenticationManager(cfg); }
        public virtual                           ConfigOfUser            CreateConfigOfUser(AuthenticationManager m, 
                                                                                            ConfigOfUser.StorageTraits traits = null) { return new ConfigOfUser(m, traits); }
        public virtual                Deployment.Deployer                CreateDeployer(Common.BootstrapConfig c)                     { return new SingletonDeployer(c); }
        public virtual                           ReleaseNotifier         CreateReleaseNotifier(Deployment.Config c,
                                                                                               bool canSeeTechnicalRelease)           { return new ReleaseNotifier(c, canSeeTechnicalRelease); }
        public virtual                      Logs.LogSource               CreateLogSource(AuthenticationManager m, params Login[] selectableLogins)
                                                                                                                                      { return new Logs.LogSource(m, selectableLogins); }

        public virtual                Dialoguing.Dialoguer               CreateDialoguer(string ConnectionStringDialoguerListener, 
                                                                                         string ConnectionStringDialoguerStarter,
                                                                                         AuthenticationManager m,
                                                                                         SynchronizationContext context)              { return new Dialoguing.Dialoguer(ConnectionStringDialoguerListener, ConnectionStringDialoguerStarter, (short)m.CurrentUserReal.Person_Id, context); }
        public virtual                Dialoguing.Messenger               CreateMessenger(Dialoguing.Dialoguer d)                      { return new Dialoguing.Messenger(d); }
        public virtual                Dialoguing.DataChangeEventManager  CreateDataChangeEventManager(IDbMapper m,
                                                                                                      Dialoguing.Dialoguer d)         { return new Dialoguing.DataChangeEventManager(m, d); }
        public virtual                           CommentPatternSuperSet  CreateCommentPatternSuperSet()                               { return new CommentPatternSuperSet(); }

        public virtual               UserDataFix.BusinessRecordEditSet   CreateBusinessRecordEditSet(IDbMapper mapper, AuthenticationManager m)
                                                                                                                                      { return new UserDataFix.BusinessRecordEditSet(mapper, m, Feature.AllowAdminOnly); }
        public virtual           DAL.UserDataFix.BusinessRecordEdit      CreateBusinessRecordEdit()                                   { return new DAL.UserDataFix.BusinessRecordEdit(); }

        public virtual              Optimization.OptimizationDataManager CreateOptimizationDataManager(IDbMapper mapper)              { return new Optimization.OptimizationDataManager(mapper); }
        public virtual              Optimization.MetadataAnalyser        CreateMetadataAnalyser(IDbMapper mapper, 
                                                                                                DataMapper.Helpers.DtoTypeSizeEstimator estimator = null)
                                                                                                                                      { return new Optimization.MetadataAnalyser(mapper, estimator); }

        
        class SingletonDeployer : Common.Deployer
        {
            protected internal SingletonDeployer(Deployment.Config cfg)
                : base(cfg)
            {
            }
        }
    }
}
