﻿using System;

using TechnicalTools.Automation;

using ApplicationBase.DAL.Logging;
using ApplicationBase.Business.Support;


namespace ApplicationBase.Business.Automation
{
    [TaskDescription(BusinessBehaviorAndDescription = "In our case it is ryver.")]
    public class ListenAndRedirectLogsToRyver : ListenAndRedirectLogs
    {
        [Argument(Description = "URL to post logs on. This url is created from ryver application.",
          IsMandatory = true)]
        public string PostChannelURL { get; set; }

        [Argument(Description = "Name of bot when it post on ryver",
                  IsMandatory = true)]
        public string BotDisplayedName { get; set; }

        [Argument(Description = "People to notify explicitely on channel.\n" +
                                "Written in the same way we would write in ryver to talk to one or multiple persons.",
                  DefaultValue = "@team")]
        public string ToRyverUsers { get; set; }


        public ListenAndRedirectLogsToRyver(Config cfg, DateTime atDate)
            : base(cfg, atDate)
        {
        }
        
        public override int DoWork()
        {
            // To make code proper, we should add DbLogSender (created by Program class) to dependencies paramters in this constructor call.
            // So the working state of DbLogSender would be always guaranteed to handle all _ryverSender's requests (simple log or errors etc).
            // Not done, because :
            // - It is hard, currently, to pass DbLogSender instance to "this", in a proper and generic way,
            //   (maybe later when a "Dependency" interface will be created)
            // - This is not really required since "this" is always created after Program creates DbLogSender, 
            //   and stopped before Program stops DbLogSender.
            _ryverSender = new MessageSenderToRyver(PostChannelURL, BotDisplayedName, this);
            return base.DoWork();
        }
        MessageSenderToRyver _ryverSender;

        public override void StopAndJoin()
        {
            //  Stop the source of log
            base.StopAndJoin();
            // Then stop ryver sender (no mor request will be done)
            if (_ryverSender != null)
                _ryverSender.Release(this);
        }

        protected override string ToFormattedMessage(Log log, bool isContextualLogOnly)
        {
            string msg = !isContextualLogOnly && !string.IsNullOrWhiteSpace(ToRyverUsers)
                       ? ToRyverUsers + Environment.NewLine
                       : "";
            msg += TechnicalTools.Logs.ILog_Extensions.ToFormatedMessage(log, DisplayLogApplicationInfoToo);
            return msg;
        }
        protected override void SendToElsewhere(string msg)
        {
            _ryverSender.ScheduleHandle(msg);
        }
    }
}
        