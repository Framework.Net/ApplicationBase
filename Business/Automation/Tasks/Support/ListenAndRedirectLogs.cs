﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using TechnicalTools;
using TechnicalTools.Automation;
using TechnicalTools.Logs;

using ApplicationBase.Business.Support;


namespace ApplicationBase.Business.Automation
{
    [TaskDescription(BusinessBehaviorAndDescription = "Filter some interesting logs and send them to elsewhere.")]
    public abstract class ListenAndRedirectLogs : CommandLineTask
    {
        [Argument(Description = "Comma separated list of user's id to listen to",
          DefaultValue = "Null / Empty (all users are listened)")]
        public string FilterUserIds
        {
            get { return _FilterUserIds.Join() ?? ""; }
            set { _FilterUserIds = (value ?? "").Split(',').Select(v => long.Parse(v.Trim())).ToList(); }
        }
        List<long> _FilterUserIds = new List<long>();

        [Argument(Description = "Comma (or pipe: '|') separated list of tag(s) to listen to.\n"
                              + "This list acts as a big OR, not a AND.",
                  DefaultValue = "Null / Empty (means all logs are listened not none)")]
        public string FilterIncludeTags { get { return _FilterIncludeTags.Join() ?? ""; }
                                          set { _FilterIncludeTags = (value ?? "").Split(new[] { '|', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList(); } }
        List<string> _FilterIncludeTags = new List<string>();

        [Argument(Description = "Comma (or pipe: '|') separated list of tag(s) to filter out (ignore).\n"
                              + "This list acts as a big OR, not a AND.",
                  DefaultValue = "Null / Empty")]
        public string FilterExcludeTags
        {
            get { return _FilterExcludeTags.Join() ?? ""; }
            set { _FilterExcludeTags = (value ?? "").Split(new[] { '|', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList(); }
        }
        List<string> _FilterExcludeTags = new List<string>();

        [Argument(Description = "Filter the minimum level of log listened and tee-ed. Use names from " + nameof(TechnicalTools) + "." + nameof(TechnicalTools.Logs) + "." + nameof(Level),
                  DefaultValue = "Info")]
        public Level FilterMinLevel { get; set; } = Level.Info;

        [Argument(Description = "Filter (include) logs to send. This is C# Regex format.\n" +
                                "Just one match needs to be found by regex anywhere in log line to include it.\n" +
                                "Null / empty value make inclusion to be ignored (so all logs will be tee-ed))",
                  DefaultValue = "<null>")]
        public string LogPatternIncludeFilter
        {
            get { return _reLogPatternIncludeFilter?.ToString() ?? ""; }
            set { _reLogPatternIncludeFilter = string.IsNullOrEmpty(value) ? null : new Regex(value, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture); }
        }
        Regex _reLogPatternIncludeFilter;

        [Argument(Description = "Filter out logs to send. This is C# Regex format.\n" + 
                                "Just one match needs to be found by regex anywhere in log line to exclude it.\n" +
                                "Null / empty value make exclusion to be ignored",
                  DefaultValue = "<null>")]
        public string LogPatternExcludeFilter
        {
            get { return _reLogPatternExcludeFilter?.ToString() ?? ""; }
            set { _reLogPatternExcludeFilter = string.IsNullOrEmpty(value) ? null : new Regex(value, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture); }
        }
        Regex _reLogPatternExcludeFilter;

        [Argument(Description = "Allow to hide Hostname and other data related to machine.\n" +
                                "These data are pretty useless when listening for a unique bot executing on one known machine.",
                                DefaultValue = "false")]
        public bool DisplayLogApplicationInfoToo { get; set; }

        [Argument(Description = "Allow to display 'n' logs before the one matching all filters.\n" +
                                "Line are not displayed twice when considering " + nameof(ContextLogAfter) + " argument.",
                  DefaultValue = "0")]
        public int ContextLogBefore { get; set; }

        [Argument(Description = "Allow to display 'n' logs after the one matching all filters.\n" +
                                "Line are not displayed twice when considering " + nameof(ContextLogBefore) + " argument.",
                  DefaultValue = "0")]
        public int ContextLogAfter { get; set; }

        [Argument(Description = "Tell the time of day where the task stops.",
          DefaultValue = "End of day (midnight)")]
        public TimeSpan TimeOfDayToStop { get; set; } = new TimeSpan(1, 0, 0, 0);

        [Argument(Description = "Tell the time of day where the task stops.",
          DefaultValue = "End of day (midnight)")]
        public TimeSpan PoolingIntervalTime { get; set; } = new TimeSpan(0, 0, 60);



        protected ListenAndRedirectLogs(Config cfg, DateTime atDate)
            : base(cfg, atDate)
        {
        }
        public override int DoWork()
        {
            _nextLogToDisplay = 0;
            _previousLogs = new Queue<DAL.Logging.Log>(ContextLogBefore + 1);

            _dbLogListener = new DatabaseLogListener(DateTime.UtcNow.Date, FilterAndSend, PoolingIntervalTime);
            _dbLogListener.Run();

            var durationToWait = GetTaskDuration();
            System.Threading.Thread.Sleep(durationToWait);
            StopAndJoin();
            return SUCCESS;
        }
        DatabaseLogListener _dbLogListener;
        Queue<DAL.Logging.Log> _previousLogs; // to handle ContextLogBefore
        int _nextLogToDisplay; // to handle ContextLogAfter

        public virtual void StopAndJoin()
        {
            _dbLogListener.StopAndJoin();
        }
        
        protected virtual void FilterAndSend(DAL.Logging.Log log)
        {
            if (IsInteresting(log))
            {
                string msg = "";
                while (_previousLogs.Count > 0)
                {
                    var oldLog = _previousLogs.Dequeue();
                    msg = (msg == null ? "" : msg + Environment.NewLine)
                        + ToFormattedMessage(oldLog, true);
                }
                msg = (msg == null ? "" : msg + Environment.NewLine)
                    + ToFormattedMessage(log, false);
                _nextLogToDisplay = ContextLogAfter;
                SendToElsewhere(msg);
            }
            else if (--_nextLogToDisplay > 0)
            {
                string msg = ToFormattedMessage(log, true);
                SendToElsewhere(msg);
            }
            else if (ContextLogAfter > 0)
            {
                _previousLogs.Enqueue(log);
                if (_previousLogs.Count == ContextLogAfter + 1)
                    _previousLogs.Dequeue();
            }
        }

        protected abstract void SendToElsewhere(string msg);

        protected virtual bool IsInteresting(DAL.Logging.Log log)
        {
            if (_FilterUserIds.Count != 0 && !_FilterUserIds.Contains(log.Id))
                return false;
            if (log.Level.Value < FilterMinLevel.Value)
                return false;
            if (_FilterIncludeTags.Count != 0 && log.Tags.Split(',').All(tag => !_FilterIncludeTags.Contains(tag)))
                return false;
            if (_FilterExcludeTags.Count != 0 && log.Tags.Split(',').Any(tag => _FilterExcludeTags.Contains(tag)))
                return false;
            if (_reLogPatternIncludeFilter != null && !_reLogPatternIncludeFilter.Match(log.Message).Success)
                return false;
            if (_reLogPatternExcludeFilter != null && _reLogPatternExcludeFilter.Match(log.Message).Success)
                return false;

            return true;
        }

        protected virtual string ToFormattedMessage(DAL.Logging.Log log, bool isContextualLogOnly)
        {
            string msg = isContextualLogOnly
                       ? "[CONTEXT] "
                       : "";
            msg += log.ToFormatedMessage(DisplayLogApplicationInfoToo);
            return msg;
        }

        protected virtual TimeSpan GetTaskDuration()
        {
            // Process the time to wait before stopping
            // This code takes care about daylight saving time.
            var now = DateTime.Now;
            DateTimeOffset dtNow = new DateTimeOffset(now, TimeZoneInfo.Local.GetUtcOffset(now));
            var toDate = DateTime_Extensions.MinOrNull(now.Date.AddDays(1), now.Date + TimeOfDayToStop).Value;
            DateTimeOffset dtEndOfTask = new DateTimeOffset(toDate, TimeZoneInfo.Local.GetUtcOffset(toDate));
            TimeSpan toWait = dtEndOfTask - dtNow; // takes care of daylight saving time :)
            if (toWait.TotalSeconds < 0)
                toWait = new TimeSpan(0, 0, 0);
            return toWait;
        }
    }
}
        