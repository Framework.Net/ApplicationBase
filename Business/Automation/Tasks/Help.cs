﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using TechnicalTools;
using TechnicalTools.Automation;
using TechnicalTools.Automation.AutoDoc;


namespace ApplicationBase.Business.Automation
{
    public class Help : TechnicalTools.Automation.AutoDoc.Help
    {
        public Help(DateTime atDate)
            : base(atDate)
        {
        }

        protected override IEnumerable<DocBlock> BeforeTasksDescriptions()
        {
            foreach (var b in base.BeforeTasksDescriptions())
                yield return b;

            yield return new DocParagraph("Now, if application needs to connect to a DB, there is some minimal arguments to know too:");

            // Continue list of argument needed to know 
            var bct = typeof(Common.BootstrapConfig);
            var argsProps = bct.GetProperties().Where(p => p.GetCustomAttribute<ArgumentAttribute>() != null).ToList();

            var bullets = argsProps.Select(ap => new DocContainer(
                                                    new DocBold(ap.Name + ": ").WrapInList()
                                                    .Concat(DocumentedPropertyArgumentToDoc(ap))))
                                   .ToList();
            yield return new DocBulletList(bullets);
            
            foreach (var b in OtherTechnicalSwitches())
                yield return b;
        }

        protected virtual IEnumerable<DocBlock> OtherTechnicalSwitches()
        {
            return OtherTechnicalSwitchesOnClass<Common.DebugConfig>();
        }
        protected IEnumerable<DocBlock> OtherTechnicalSwitchesOnClass<TDebugConfig>()
            where TDebugConfig : Common.DebugConfig
        {
            yield return new DocParagraph("Here are some other general, technical, optional arguments you can use:");
            var dct = typeof(TDebugConfig);
            var argsProps = dct.GetProperties().Where(p => p.GetCustomAttribute<ArgumentAttribute>() != null).ToList();
            var bullets = argsProps.Select(ap => new DocContainer(
                                                new DocBlock[] { new DocBold(ap.Name + ": ") }
                                                .Concat(DocumentedPropertyArgumentToDoc(ap))))
                                   .ToList();
            yield return new DocBulletList(bullets);
        }
    }
}
