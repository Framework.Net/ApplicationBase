﻿using System;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Automation;
using ApplicationBase.Deployment.Data;
using TechnicalTools.Model;


namespace ApplicationBase.Business.Automation
{
    [TaskDescription(BusinessBehaviorAndDescription = "Deploy the current version of application to database.")]
    public class Deploy : CommandLineTask
    {
        [Argument(IsMandatory = true, Description = "Indicate for which domain / environment the current version of application must be deployed and allowed\r\n" +
                                                    "Format is: \"DomainName" + DomainEnvNameSeparator + "EnvName\". Use separator '" + MultipleSeparator + "' in case of multiple target.")]
        public string DomainAndEnvNamesWhereToDeploy { get; set; }

        [Argument(Description = "Indicate a comment that will be visible by user. Very often this is a change log entry.")]
        public string PublicComment { get; set; }

        [Argument(Description = "Indicate a technical comment that won't be visible by user.")]
        public string TechnicalComment { get; set; }

        public Deploy(Config cfg, DateTime atDate)
            : base(cfg, atDate)
        {
        }

        public override int DoWork()
        {
            var deployer = BusinessSingletons.Instance.GetDeployer();

            var availableEnvs = deployer.GetExistingEnvironments();
            var envWhereToDeploy = DomainAndEnvNamesWhereToDeploy.Split(new[] { MultipleSeparator }, StringSplitOptions.RemoveEmptyEntries)
                                                                 .Select(domainAndName => new
                                                                 {
                                                                     Domain = (eEnvironment)Enum.Parse(typeof(eEnvironment), domainAndName.Split(new[] { DomainEnvNameSeparator }, StringSplitOptions.RemoveEmptyEntries)[0]),
                                                                     Name = domainAndName.Split(new[] { DomainEnvNameSeparator }, StringSplitOptions.RemoveEmptyEntries)[1]
                                                                 })
                                                                 .Select(env => availableEnvs.SingleOrDefault(refEnv => refEnv.Domain == env.Domain && refEnv.Name == env.Name))
                                                                 .ToList();
            if (envWhereToDeploy.NotNull().Count() != envWhereToDeploy.Count)
                throw new TechnicalException("Domain or environment name not recognised!" + Environment.NewLine +
                                             "Format must be \"Domain,Environment name|Domain2,Environment name2|...\".", null);
            foreach (var env in envWhereToDeploy)
                env.Release = true;

            var filesToDeploy = deployer.GetFilesToDeploy();
            var launcherBinPath = deployer.Config.LauncherRelativePathToBinFromMainProjectOutputFolder;
            Version v = deployer.GetNextAvailableVersionFromVersionNumberingSystem();
            _log.Audit($"Deploying {deployer.Config.ApplicationName} version {v}...");

            var files = filesToDeploy.Where(f => f.ToDeploy).ToList();
            var envs = envWhereToDeploy.Select(env => env.Env).ToList();
            var pr = CreateProgressRedirect<string>(str => _log.Info(str));
            deployer.Deploy(files, envs, deployer.Config.ManifestFileName,
                                    PublicComment ?? "",
                                    TechnicalComment ?? "",
                                    null, pr);
            _log.Audit($"Deploying done.");
            return SUCCESS;
        }
        const string MultipleSeparator = "|";
        const string DomainEnvNameSeparator = ",";
        public static char[] InvalidDomainAndNameChars { get; } = MultipleSeparator.ToCharArray()
                                                          .Concat(DomainEnvNameSeparator.ToCharArray())
                                                          .Distinct().ToArray();
    }
}
