﻿using System;

using TechnicalTools.Automation;
using TechnicalTools.Model;


namespace ApplicationBase.Business.Automation
{
    [TaskDescription(BusinessBehaviorAndDescription = "Check how the application framework reacts when an exception occurs.\r\n" + 
                                                      "The exception will contains an inner exception of type \"" + nameof(UserUnderstandableException) + "\".")]
    public class RaiseException : CommandLineTask
    {
        [Argument(Description = "Tell a delay before throwing exception.", DefaultValue = "0")]
        protected int DelayInSeconds { get; set; }
        [Argument(Description = "Tell to task which type of exception to throw.",
                  DefaultValue = "Exception",
                  AllowedValueRange = nameof(Exception) + ", " + nameof(BusinessException) + ", " + 
                                      nameof(UserUnderstandableException) + ", " + nameof(TechnicalException) + ", " +
                                      nameof(SilentBusinessException) + ", " + nameof(BaseException) + ", " +
                                      nameof(AggregateException))]
        protected string Type { get; set; }

        [Argument(Description = "Message to inject in (inner) exception.",
                  DefaultValue = DefaultMessage)]
        protected string Message { get; set; } = DefaultMessage;
        const string DefaultMessage = "Test Exception";

        public RaiseException(Config cfg, DateTime atDate)
            : base(cfg, atDate)
        {
        }
        public override int DoWork()
        {
            System.Threading.Thread.Sleep(DelayInSeconds * 1000);

            Exception ex;
            try
            {
                throw new UserUnderstandableException("Inner " + Message, null);
            }
            catch (Exception exception)
            {
                ex = exception;
            }
            if (Type?.ToLowerInvariant() == nameof(BusinessException).ToLowerInvariant())
                throw new BusinessException(Message, ex);
            if (Type?.ToLowerInvariant() == nameof(UserUnderstandableException).ToLowerInvariant())
                throw new UserUnderstandableException(Message, ex);
            if (Type?.ToLowerInvariant() == nameof(TechnicalException).ToLowerInvariant())
                throw new TechnicalException(Message, ex);
            if (Type?.ToLowerInvariant() == nameof(SilentBusinessException).ToLowerInvariant())
                throw new SilentBusinessException(Message, ex);
            if (Type?.ToLowerInvariant() == nameof(BaseException).ToLowerInvariant())
                throw new BaseException(Message, ex);
            if (Type?.ToLowerInvariant() == nameof(AggregateException).ToLowerInvariant())
                throw new AggregateException(Message, ex);
            throw new TechnicalException(Message, ex);
        }
    }
}
        