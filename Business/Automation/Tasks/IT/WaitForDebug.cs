﻿using System;

using TechnicalTools.Automation;


namespace ApplicationBase.Business.Automation
{
    [TaskDescription(BusinessBehaviorAndDescription = "Just wait some seconds and exit.")]
    public class WaitForDebug : CommandLineTask
    {
        [Argument(Description = "Tell a delay before exiting.", DefaultValue = "60")]
        protected int DelayInSeconds { get; set; } = 60;

        [Argument(Description = "Indicate the exit value to use.", DefaultValue = SUCCESS)]
        protected int ExitValue { get; set; } = SUCCESS;

        public WaitForDebug(Config cfg, DateTime atDate)
            : base(cfg, atDate)
        {
        }
        public override int DoWork()
        {
            System.Threading.Thread.Sleep(DelayInSeconds * 1000);
            return ExitValue;
        }
    }
}
        