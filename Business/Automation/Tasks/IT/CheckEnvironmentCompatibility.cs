﻿using System;

using TechnicalTools.Automation;
using ApplicationBase.Deployment;


namespace ApplicationBase.Business.Automation
{
    [TaskDescription(BusinessBehaviorAndDescription = "Check if the current version of Application can connect to domain / environment name specified on command line.\r\n" +
                                                      "This task use the default internal connection (see BootstrapConfig.cs) string to connect to DB and check this.")]
    public class CheckEnvironmentCompatibility : CommandLineTask
    {
        public CheckEnvironmentCompatibility(Config cfg, DateTime atDate)
            : base(cfg, atDate)
        {
        }
        public override int DoWork()
        {
            var version = Deployer.GetCurrentVersion();
            var deployer = BusinessSingletons.Instance.GetDeployer();
            return deployer.IsCompatible(version, Cfg.Environment.Domain, Cfg.Environment.Name)
                 ? SUCCESS
                 : ERROR;
        }
    }
}
        