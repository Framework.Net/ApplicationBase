﻿using System;
using System.Diagnostics;

using TechnicalTools.Automation;


namespace ApplicationBase.Business.Automation
{
    [TaskDescription(BusinessBehaviorAndDescription = "Check how the application framework reacts when a failing assertion occurs.")]
    public class RaiseFailedAssertion : CommandLineTask
    {
        [Argument(Description = "Tell a delay before raising the failing assertion.", DefaultValue = "0")]
        protected int DelayInSeconds { get; set; }

        public RaiseFailedAssertion(Config cfg, DateTime atDate)
            : base(cfg, atDate)
        {
        }
        public override int DoWork()
        {
            System.Threading.Thread.Sleep(DelayInSeconds * 1000);
            Debug.Assert(false, "Test of assertion failing!");
            return SUCCESS;
        }
    }
}
        