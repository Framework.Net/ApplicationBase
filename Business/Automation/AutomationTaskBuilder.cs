﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools.Automation;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;


namespace ApplicationBase.Business.Automation
{
    /// <summary>
    /// Class able to parse command line argument, instanciate the good task and run it.
    /// </summary>
    public class AutomationTaskBuilder : TechnicalTools.Automation.AutomationTaskBuilder
    {
        public AutomationTaskBuilder(List<string> args, Config mainConfig)
            : base(args)
        {
            _mainConfig = mainConfig;
        }
        readonly Config _mainConfig;

        [Argument(Description ="Not mandatory for some rare task (for exemple \"" + nameof(Help) + "\"), otherwise it is mandatory.")]
        protected string   Login      { get; set; }

        [Argument(Description = "Not mandatory for some rare task (for exemple \"" + nameof(Help) + "\"), otherwise it is mandatory.")]
        protected string   Password   { private get; set; }
         
        protected override TechnicalTools.Automation.CommandLineTask BuildTask(Type cmdType)
        {
            if (Login != null)
                ExceptionManager.Instance.InterceptException<Exception>(
                    () => BusinessSingletons.Instance.GetAuthenticationManager().Authenticate(Login, Password),
                    ex => InvalidTaskCommandLineReason = ExceptionManager.Instance.Format(ex));
            var constructors = cmdType.GetConstructors()
                                      .Where(c => c.GetParameters().Length == 2
                                               && typeof(Config).IsAssignableFrom(c.GetParameters()[0].ParameterType)
                                               && c.GetParameters()[1].ParameterType == typeof(DateTime))
                                      .ToList();
            if (constructors.Count == 1)
                return (CommandLineTask)constructors[0].Invoke(new object[] { _mainConfig, Date });
            if (constructors.Count > 1)
                throw new TechnicalException($"Internal Error: Task {Execute} has multiple constructor matching signature for a {nameof(CommandLineTask)}!", null);

            return base.BuildTask(cmdType);
        }
    }
}
