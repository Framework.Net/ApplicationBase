﻿using System;
using System.Diagnostics;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;

using ApplicationBase.Common;


namespace ApplicationBase.Business.Automation
{
    public class DefaultExceptionHandler : IExceptionHandler, IDisposable
    {
        public IExceptionHandler DefaultExceptionManagement { get; set; } // == null on mainform
        
        public DefaultExceptionHandler()
        {
            UnexpectedExceptionManager.NewUnhandledException += OnNewUnhandledException;
        }
        public virtual void Dispose()
        {
            UnexpectedExceptionManager.NewUnhandledException -= OnNewUnhandledException;
        }

        void OnNewUnhandledException(object sender, UnexpectedExceptionManager.ExceptionInfo e)
        {
             HandleException(UnknownAction, e.Exception);
        }


        public static readonly string UnknownAction = "Unknown action or background task";
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(DefaultExceptionHandler));

        protected string GetDefaultFullMessage(string actionName)
        {
            return "Error during " + (actionName == UnknownAction ? UnknownAction : $"action \"{actionName}\"");
        }
        public virtual bool HandleException(string actionName, Exception ex)
        {
            if (IsAlreadyHandled(ex))
                return false;
            _log.Error(GetDefaultFullMessage(actionName), ex);
            return true;
        }
        protected bool IsAlreadyHandled(Exception ex)
        {
            // Prevent the same exception to be handle twice
            // This can happens when exception has come here through two different execution flow
            // (ex: BusyForm in UI project catch, handle it and rethrow it
            //      If the method that called BusyForm did it using await keyword, the exception is recatch by ExceptionManager (Assembly events...)
            if (ex.Data.Contains(nameof(DefaultExceptionHandler) + ".Handled"))
                return true;
            ex.Data[nameof(DefaultExceptionHandler) + ".Handled"] = true;
            return false;
        }

    }

}
