﻿using System;


namespace ApplicationBase.Business.Automation
{
    public abstract class CommandLineTask : TechnicalTools.Automation.CommandLineTask
    {
        protected Config       Cfg      { get; }

        /// <remarks>Constructor signature is interdependant to code inside <see cref="AutomationTaskBuilder.BuildTask"/>
        protected CommandLineTask(Config cfg, DateTime atDate)
            : base(atDate)
        {
            Cfg = cfg;

            DataMapper.BaseDTO.TrackInstances = false; // speed up tasks..
        }
    }
}
