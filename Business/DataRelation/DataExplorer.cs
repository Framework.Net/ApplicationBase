﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.Tools;

using DataMapper;

using ApplicationBase.Common;
using ApplicationBase.Business.GraphVisualization.Graphviz;
using ApplicationBase.Business.GraphVisualization;


namespace ApplicationBase.Business.DataRelation
{
    public class DataExplorer : IUserInteractiveObject
    {
        public DataExplorer(IDbMapperReadAccess mapper = null)
        {
            _mapper = mapper;
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(DataExplorer));
        readonly IDbMapperReadAccess _mapper;

        public class MappersTypes
        {
            public IReadOnlyCollection<IDbMapperReadAccess> Mappers       { get { return MapperByIds.Values; } }
            public Dictionary<int, IDbMapperReadAccess>     MapperByIds   { get; set; }
            public IReadOnlyCollection<Type>                ProvidedTypes { get; set; }
        }

        public virtual IReadOnlyDictionary<string, MappersTypes> GetDatabaseInfo()
        {
            if (_mapper != null)
                return new Dictionary<string, MappersTypes>()
                {
                    {
                        string.Empty,
                        new MappersTypes()
                        {
                            MapperByIds = new Dictionary<int, IDbMapperReadAccess>() { { 0, _mapper } } ,
                            ProvidedTypes = GetMappedTypesIn(_mapper.GetType().Assembly)
                        }
                    }
                };
            return new Dictionary<string, MappersTypes>();
        }

        protected virtual List<Type> GetMappedTypesIn(Assembly ass, Type tagInterface = null)
        {
            var res = ass.GetTypes().Where(t => IsInterestingType(ass, t, tagInterface))
                                    .OrderBy(t => t.Namespace)
                                    .ThenBy(t => t.Name)
                                    .ToList();
            return res;
        }
        protected virtual bool IsInterestingType(Assembly ass, Type t, Type tagInterface = null)
        {
            return typeof(IAutoMappedDbObject).IsAssignableFrom(t)
                && !t.IsAbstract
                && !t.IsNested
                && t.GetConstructor(Type.EmptyTypes) != null
                && (tagInterface == null || tagInterface.IsAssignableFrom(t));
        }
        public virtual IEnumerable<IVirtualProperty> GetVirtualColumns(Request source)
        {
            if (source.Mappers.Count > 1 && typeof(BaseDTO).IsAssignableFrom(source.ItemType))
                return new VirtualProperty<BaseDTO>()
                {
                    Caption = "DB Name",
                    Type = typeof(string),
                    IsImportant = true,
                    GetValue = dto => dto.OwnerMapper.Owner.Name
                }.WrapInList();
            return Enumerable.Empty<IVirtualProperty>();
        }

        public GenericHierarchyGraphProviderWithGraphViz HierarchyProvider
        {
            get { return _HierarchyProvider ?? (_HierarchyProvider = CreateHierarchyProvider()); }
        }
        GenericHierarchyGraphProviderWithGraphViz _HierarchyProvider;
        protected virtual GenericHierarchyGraphProviderWithGraphViz CreateHierarchyProvider()
        {
            return new DtoGenericHierarchyProvider(this);
        }

        public class Request
        {
            public string                                   DbType            { get; set; }
            public IReadOnlyCollection<IDbMapperReadAccess> Mappers           { get; set; }
            public Type                                     ItemType          { get; set; }
            public string                                   SqlWhereCondition { get; set; }
            
            public void CheckConsistency()
            {
                if (!typeof(IAutoMappedDbObject).IsAssignableFrom(ItemType))
                    throw new TechnicalException("This type is not available!", null);
                if (Mappers.Count > 0 && Mappers.All(m => m.GetType() != Mappers.First().GetType()))
                    throw new TechnicalException("All mappers must be of same type!", null);
            }
        }



        public virtual Dictionary<IDbMapperReadAccess, int> GetMatchingItemsCount(Request request, IProgress<string> pr)
        {
            request.CheckConsistency();
            var mgCount = typeof(IDbMapperReadAccess)
                           .GetMethods()
                           .Single(m => m.Name == nameof(IDbMapperReadAccess.Count));
            var mCount = mgCount.MakeGenericMethod(request.ItemType);

            var countResults = ForEachMappers(request.Mappers, pr, mapper =>
            {
                var cnt = (int)mCount.InvokeAndCleanException(mapper, new object[] { request.SqlWhereCondition, null });
                return cnt;
            });
            return countResults;
        }

        /// <summary>
        /// Load data accordingly to where / which data it is.
        /// Return a strongly typed List (you can use <see cref="IEnumerableGeneric_Extensions.ToTypedList{T}(IEnumerable{T}, Type)"/> for this).
        /// </summary>
        public System.Collections.IList Load(Request request, IProgress<string> pr)
        {
            pr = pr ?? ProgressEx<string>.Default;
            request.CheckConsistency();
            var mgLoadCollection = typeof(IDbMapperReadAccess)
                                                 .GetMethods()
                                                 .Single(m => m.Name == nameof(IDbMapperReadAccess.LoadCollection)
                                                           && m.GetGenericArguments().Length == 1
                                                           && m.GetParameters().Length == 3
                                                           && m.GetParameters().All(p => p.ParameterType == typeof(string)));
            var mLoadCollection = mgLoadCollection.MakeGenericMethod(request.ItemType);

            var dsByMappers = ForEachMappers(request.Mappers, pr, mapper =>
            {
                var ds = (IReadOnlyCollection<object>)mLoadCollection.InvokeAndCleanException(mapper, new object[] { request.SqlWhereCondition, null, null });
                return ds;
            });
            var res = dsByMappers.SelectMany(kvp => kvp.Value).ToTypedList(request.ItemType);
            return res;
        }

        protected Dictionary<IDbMapperReadAccess, T> ForEachMappers<T>(IReadOnlyCollection<IDbMapperReadAccess> mappers, IProgress<string> pr, Func<IDbMapperReadAccess, T> func)
        {
            var results = mappers.ForEachParallelSafelyAsDictionary("For each mapper", pr, func).Result;
            if (results.Item2.Any())
                WarnUser(results.Item2);
            return results.Item1;
        }
        protected void WarnUser(Dictionary<IDbMapperReadAccess, Exception> exceptionByMappers)
        {
            if (exceptionByMappers.Any())
                BusEvents.Instance.RaiseBusinessWarning("Cannot get operations from some partner environment(s) because:" + Environment.NewLine +
                                                         Environment.NewLine +
                                                         exceptionByMappers.Select(error => error.Key.Owner.Name + " :" + Environment.NewLine
                                                                                          + ExceptionManager.Instance.Format(error.Value, true, false))
                                                                           .Join(Environment.NewLine + Environment.NewLine), _log);
        }
    }
}
