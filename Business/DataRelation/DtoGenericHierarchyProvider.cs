﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

using TechnicalTools;
using TechnicalTools.Annotations;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;
using TechnicalTools.UI;

using DataMapper;
using DataMapper.Helpers;

using ApplicationBase.Business.GraphVisualization;


namespace ApplicationBase.Business.DataRelation
{
    public class DtoGenericHierarchyProvider : GenericHierarchyGraphProviderWithGraphViz
    {
        protected internal DtoGenericHierarchyProvider(DataExplorer explorer, IReadOnlyDictionary<object, IDbMapperReadAccess> keyToMappers = null, TypeSizeEstimator estimator = null)
            : base(estimator ?? new DtoTypeSizeEstimator())
        {
            _keyToMappers = keyToMappers;
            DefaultMaxRelationRecursivity = 1;

            var dbInfos = explorer.GetDatabaseInfo();
            DataExplorer.MappersTypes multiple = null;
            foreach (DataExplorer.MappersTypes mapperTypes in dbInfos.Values)
                if (mapperTypes.Mappers.Count > 1)
                    if (multiple == null)
                        multiple = mapperTypes;
                    else
                        // Actually this error is also true when only one multiple set of mapper exist (ie multiple is set)
                        // No other mapper of other database type must reference this multiple set.
                        throw new TechnicalException("Not handled! Algorithm does not know how to interconnect multiple database of two different types." +
                                                     "It would lead too a set of graph configuration containing at least one bad grap...", null);

            foreach (DataExplorer.MappersTypes mapperTypes in dbInfos.Values)
                foreach (var mapper in mapperTypes.Mappers.Take(1))
                    foreach (var type in mapperTypes.ProvidedTypes)
                    {
                        // Minimal condition that match the reflected methods called above
                        bool fromTypeRespectGenericConstraints = typeof(BaseDTO).IsAssignableFrom(type)
                                                              && typeof(IAutoMappedDbObject).IsAssignableFrom(type)
                                                              && typeof(IHasTypedIdReadable).IsAssignableFrom(type);
                        if (!fromTypeRespectGenericConstraints)
                            continue;

                        var mtable = mapper.GetTableMapping(type);
                        foreach (var fk in mtable.ForeignKeys)
                        {
                            bool toTypeRespectGenericConstraints = typeof(BaseDTO).IsAssignableFrom(fk.To)
                                                                && fk.To.Implements<IAutoMappedDbObject>()
                                                                && fk.To.Implements<IHasTypedIdReadable>();
                            if (!toTypeRespectGenericConstraints)
                                continue;

                            #region Other generic filtering (by choice, because it is complex to handle currently)
                            var hasTypedIdType = fk.To.GetOneImplementationOf(typeof(IHasTypedIdReadable<>));
                            if (hasTypedIdType == null)
                                continue;
                            var idTupleType = hasTypedIdType.GetGenericArguments().Single();
                            if (!idTupleType.IsGenericType ||
                                idTupleType.GetGenericArguments().Length > 1)
                                continue;
                            if (idTupleType.GetGenericArguments().Any(keyType => !typeof(IEquatable<>).MakeGenericType(keyType).IsAssignableFrom(keyType) ||
                                                                                 !typeof(IComparable<>).MakeGenericType(keyType).IsAssignableFrom(keyType) ||
                                                                                 !typeof(IComparable).IsAssignableFrom(keyType)))
                                continue;
                            var concreteIdTupleType = typeof(IdTuple<>).MakeGenericType(idTupleType.GetGenericArguments());
                            if (!typeof(IHasTypedIdReadable<>).MakeGenericType(concreteIdTupleType).IsAssignableFrom(fk.To))
                                continue;
                            var mtableRef = mapper.GetTableMapping(fk.To);

                            // We consider all enum kind as 1-0 or 1-1 relationship that contains nothing interesting but the name, 
                            // which is already used to implement ToString.
                            // This assertion is, of course, not 100% true but unlikely anyway.
                            // If these data are really needed by user at runtime, he can browse and display them anyway by using UI feature in EnhancedGridView_BrowsableNestedProperties.
                            // There is work to do here to handle at least the classic enum that could be converted easily to int or tinyint etc
                            if (fk.Keys.Any(fkf => fkf.PropertyInfo.PropertyType.RemoveNullability().IsEnum
                                                || typeof(DynamicEnum).IsAssignableFrom(fkf.PropertyInfo.PropertyType.RemoveNullability())
                                                || typeof(IStaticEnum).IsAssignableFrom(fkf.PropertyInfo.PropertyType.RemoveNullability())))
                                continue;

                            #endregion Other generic filtering (by choice)

                            if (fk.Keys.Count == 1 && fk.ConditionalPropertyName == null) // other relations are not handled yet
                            {
                                Debug.Assert(fk.Keys[0].PropertyInfo.PropertyType.IsValueType || fk.Keys[0].PropertyInfo.PropertyType == typeof(string));
                                var mBuildBiDirectionalRelationShips = typeof(DtoGenericHierarchyProvider)
                                                                    .GetMethods(BindingFlags.Instance | BindingFlags.NonPublic)
                                                                    .Single(m => m.Name == nameof(BuildBiDirectionalRelationShips))
                                                                    .MakeGenericMethod(mtable.MappedType, mtableRef.MappedType, fk.Keys[0].PropertyInfo.PropertyType.RemoveNullability());
                                mBuildBiDirectionalRelationShips?.InvokeAndCleanException(this, new object[] { mapper, mtable, mtableRef, fk });
                            }
                        }
                    }
        }
        readonly IReadOnlyDictionary<object, IDbMapperReadAccess> _keyToMappers;

        class BiDirectionalBuildingMemory
        {
            public object DirectRelation;
            public object ReverseRelation;
        }
        void BuildBiDirectionalRelationShips<TObject, TPointed, TPointedKey>(IDbMapperReadAccess mapper, DbMappedTable mtable, DbMappedTable mtablePointed, DbForeignKey fk)
            where TObject : BaseDTO, IAutoMappedDbObject, IHasTypedIdReadable
            where TPointed : BaseDTO, IAutoMappedDbObject, IHasTypedIdReadable<IdTuple<TPointedKey>>
            where TPointedKey : IEquatable<TPointedKey>, IComparable<TPointedKey>, IComparable
        {
            var relationshipName = BusinessNameAttribute.GetNameFor(mtablePointed.MappedType)
                                ?? mtablePointed.MappedType.Name.Uncamelify();
            relationshipName = fk.Keys[0].PropertyInfo.Name.Uncamelify() + " " + UnicodeChars.HEAVY_RIGHT­WARDS_ARROW_WITH_EQUILATERAL_ARROWHEAD + " "
                             + relationshipName;
            relationshipName = fk.RelationName ?? relationshipName;

            var reverseRelationshipName = BusinessNameAttribute.GetNameFor(mtable.MappedType)
                                       ?? mtable.MappedType.Name.Uncamelify();
            reverseRelationshipName = UnicodeChars.HEAVY_LEFT­WARDS_ARROW_WITH_EQUILATERAL_ARROWHEAD
                                    + " " + reverseRelationshipName + "." + fk.Keys[0].PropertyInfo.Name.Uncamelify();
            reverseRelationshipName = "Reverse of ".AsPrefixForIfNotBlank(fk.RelationName) ?? reverseRelationshipName;


            // With old architecture, we would just need to build two hierarchies, with builder that use only parameters,
            // But to enjoy the new field HierarchyDescription.ReverseDescription we do some black magic here...
            // We just creation another "dimension" (_memories) where we can store contextual data to link the two hierarchies created.
            // We, actually, need to remember, while a  graph g is building, that we already met one way of the bidirectional edge.
            // So when the second one is asked to be creatd, we already send the one created in advance (and linked to the first one).
            // Keep in mind a double relation can exist... (a type "Body" has two FK/relation to a type "Hand")
            // I surely want to punch myself in the face, in the future, for this esoteric/blasphemous code :/ but I need to get fast results.
            ConditionalWeakTable<HierarchyDescriptionGraph, Dictionary<PropertyInfo, BiDirectionalBuildingMemory>> _memories = new ConditionalWeakTable<HierarchyDescriptionGraph, Dictionary<PropertyInfo, BiDirectionalBuildingMemory>>();

            BiDirectionalBuildingMemory GetOrBuildBiDirectionalForCurrentGraphBuilding(HierarchyDescriptionGraph g)
            {
                var dico = _memories.GetOrCreateValue(g);
                return dico.GetOrCreate(fk.Keys[0].PropertyInfo, () => 
                {
                    var couple = new 
                    {
                        DirectRelation = BuildCachedHierarchy(relationshipName, (IEnumerable<TObject> objs) =>
                        {
                            Dictionary<TObject, List<TPointed>> res = null;
                            try
                            {
                                var data = mapper.EnumerateCollectionMatching<TPointed, TPointedKey>(objs.Select(o => fk.Keys[0].PropertyInfo.GetValue(o)).Cast<TPointedKey>().Distinct(), mtablePointed.PkField).ToList();
                                var pointeds = data.ToDictionary(p => p.TypedId.Key.Id1);
                                res = objs.Distinct().ToDictionary(o => o, o => pointeds.TryGetValueClass((TPointedKey)fk.Keys[0].PropertyInfo.GetValue(o))?.WrapInList() ?? List_Extensions<TPointed>.Empty);
                            }
                            catch when (DebugTools.IsForDevelopper) // Let a chance to developper to debug in live
                            {
                                DebugTools.Break();
                                res = new Dictionary<TObject, List<TPointed>>();
                                if (res == null)
                                    throw;
                            }
                            return res;
                        }, 3), // 3 = no idea actually, this is too generic to know
                        ReverseRelation = BuildCachedHierarchy(reverseRelationshipName, (IEnumerable<TPointed> pointeds) =>
                        {
                            Dictionary<TPointed, List<TObject>> res = null;
                            try
                            {
                                var data = mapper.EnumerateCollectionMatching<TObject, TPointedKey>(pointeds.Select(p => p.TypedId.Key.Id1).Distinct(), fk.Keys[0]).ToList();
                                var objs = data.GroupByToDictionary(o => (TPointedKey)fk.Keys[0].PropertyInfo.GetValue(o));
                                res = pointeds.Distinct().ToDictionary(p => p, p => objs.TryGetValueClass(p.TypedId.Key.Id1) ?? List_Extensions<TObject>.Empty);
                            }
                            catch when (DebugTools.IsForDevelopper) // Let a chance to developper to debug in live
                            {
                                DebugTools.Break();
                                res = new Dictionary<TPointed, List<TObject>>();
                                if (res == null)
                                    throw;
                            }
                            return res;
                        }, 3) // 3 = no idea actually, this is too generic to know
                    };
                    // We can finally do what we want
                    couple.DirectRelation.ReverseDescription = couple.ReverseRelation;
                    couple.ReverseRelation.ReverseDescription = couple.DirectRelation;
                    return new BiDirectionalBuildingMemory() { DirectRelation = couple.DirectRelation, ReverseRelation = couple.ReverseRelation };
                });
            };
            AddConstructor((HierarchyDescriptionGraph g) =>
            {
                var bidi = GetOrBuildBiDirectionalForCurrentGraphBuilding(g);
                var h = (HierarchyDescriptionTyped<TObject, TPointed>)bidi.DirectRelation; 
                return h;
            }); 

            
            AddConstructor((HierarchyDescriptionGraph g) =>
            {
                var bidi = GetOrBuildBiDirectionalForCurrentGraphBuilding(g);
                var h = (HierarchyDescriptionTyped<TPointed, TObject>)bidi.ReverseRelation;
                return h;
            });  
        }

        ///// <summary>
        ///// Version of BuildCachedHierarchy customized for multiple mappers
        ///// <seealso cref="BuildCachedHierarchy{TParent, TChild}(string, Func{IEnumerable{TParent}, Dictionary{TParent, List{TChild}}}, int, int?)"/>
        ///// </summary>
        ///// <typeparam name="TParent">The class that is the parent (in term of relation you want to display)</typeparam>
        ///// <typeparam name="TChild">The class that is child (in term of relation you want to display)</typeparam>
        //protected HierarchyDescriptionTyped<TParent, TChild> BuildCachedHierarchy<TParent, TChild>(string name, Func<IDbMapperReadAccess, IReadOnlyCollection<TParent>, Dictionary<TParent, List<TChild>>> getDataForMapperGroupByParentKey, int averageChildrenCount, int? averageChildSizeInMemory = null)
        //    where TParent : BaseDTO
        //    where TChild : class
        //{
        //    // internal cache
        //    Dictionary<IDbMapperReadAccess, Dictionary<TParent, List<TChild>>> childrenByMapperAndParent = null;
        //    HashSet<IDbMapperReadAccess> parentWithIssues = null;

        //    // We strongly type because a small mishap can happen very quickly with generic type
        //    // This is the signature to match to make other classes working
        //    Func<IEnumerable<TParent>, IReadOnlyCollection<TChild>> getAllChildren = allParents =>
        //    {
        //        var byParents = allParents.GroupByToDictionary((p) => (IDbMapperReadAccess)p.OwnerMapper);
        //        (childrenByMapperAndParent,
        //                      parentWithIssues) = GetDataForEachMapper(byParents.Keys,
        //                                                               m => getDataForMapperGroupByParentKey(m, byParents[m]));
        //        return childrenByMapperAndParent.Values.SelectMany(childrenByParent => childrenByParent.Values.SelectMany()).ToList();
        //    };
        //    Func<Stack<object>, IReadOnlyCollection<TChild>> getChildren = parentsStack =>
        //    {
        //        var parent = (TParent)parentsStack.Peek();
        //        var pe = parent.OwnerMapper;
        //        if (childrenByMapperAndParent == null || parentWithIssues.Contains(pe))
        //            return getDataForMapperGroupByParentKey(pe, parent.WrapInList())
        //                        ?.TryGetValueClass(parent);
        //        return childrenByMapperAndParent.TryGetValueClass(pe)
        //                    ?.TryGetValueClass(parent);
        //    };

        //    var h = new HierarchyDescriptionTyped<TParent, TChild>(name,
        //                getChildren, allParents => getAllChildren(allParents.Cast<TParent>()),
        //                averageChildrenCount: averageChildrenCount,
        //                averageChildSizeInMemory: averageChildSizeInMemory ?? BusinessSingletons.Instance.GetMetadataAnalyser().GetMeanRecordSize<TChild>());
        //    return h;
        //}

        //// Helper method that handle all annoying stuff
        //// Return : 
        //// dictionary of result for each Mapper,
        //// Collection of Mapper where retrieving has failed
        //protected (Dictionary<IDbMapperReadAccess, T> results, HashSet<IDbMapperReadAccess> MapperFails) GetDataForEachMapper<T>(IReadOnlyCollection<IDbMapperReadAccess> Mappers, Func<IDbMapperReadAccess, T> getData)
        //{
        //    // Do this for each Mapper, we warn and discard user if somethings is wrong
        //    var results = Mappers.ForEachMapper(ProgressEx<string>.Default,
        //                            (pe, _) => getData(pe)).Result;
        //    if (results.Item2.Count > 0)
        //    {
        //        BusEvents.Instance.RaiseBusinessWarning("Cannot load all details data due to issue for some Mapper environments' DB!" + Environment.NewLine +
        //                                                "Note though data will be available and loaded in live so it may take more time.",
        //                                                _log, results.Item2.Select(error => error.Value));
        //    }
        //    return (results.Item1, results.Item2.Select(kvp => kvp.Key).ToHashSet());
        //}
    }
}
