﻿using System;


namespace ApplicationBase.Business.DataRelation
{
    public interface IVirtualProperty
    {
        string               Caption     { get; }
        Type                 Type        { get; }
        bool                 IsImportant { get; }
        Func<object, object> GetValue    { get; }
    }

    public class VirtualProperty<TItem> : IVirtualProperty
    {
        public string               Caption                   { get; set; }
        public Type                 Type                      { get; set; }
        public bool                 IsImportant               { get; set; }
        public Func<TItem, object>  GetValue                  { get; set; }
        Func<object, object>        IVirtualProperty.GetValue { get { return _GetValue; } }

        object _GetValue(object obj)
        {
            return GetValue((TItem)obj);
        }
    }
}
