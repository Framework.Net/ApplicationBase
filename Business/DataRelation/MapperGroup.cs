﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

using TechnicalTools.Model;

using DataMapper;


namespace ApplicationBase.Business.DataRelation
{
    [DebuggerDisplay("{" + nameof(Name) + ",nq}")]
    public class MapperGroup : IReadOnlyCollection<IDbMapperReadAccess>, ITreeNodeWithParentReadable, IIsNamed
    {
        public string Name             { get; }

        public int    Count            { get { return _getMappers().Count; } }
        readonly Func<IReadOnlyCollection<IDbMapperReadAccess>> _getMappers;

        public MapperGroup(string name, Func<IReadOnlyCollection<IDbMapperReadAccess>> getMappers)
        {
            Name = name;
            _getMappers = getMappers;
        }
        public IEnumerator<IDbMapperReadAccess> GetEnumerator()
        {
            return _getMappers().GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator() { return _getMappers().GetEnumerator(); }

        public override string ToString() { return Name; }

        ITreeNodeWithParentReadable ITreeNodeWithParentReadable.ParentNode { get { return null; } }
    }
}
