﻿using System;
using System.Collections.Generic;

using TechnicalTools.Model;

using ICommentPattern = ApplicationBase.DAL.HasPropertiesOf.Tools.CommentPattern;


namespace ApplicationBase.Business
{
    public class PatternEntry
    {
        public string Name       { get; internal set; }
        public string Comment    { get; internal set; }
        public int    StartIndex { get; internal set; }
        public int    EndIndex   { get; internal set; }

        public ICommentPattern Pattern { get; internal set; }
    }

    public static class CommentPattern_Extensions
    {
        public static List<PatternEntry> GetEntryNames(this ICommentPattern pattern)
        {
            var lst = GetEntryNames(pattern.Pattern);
            foreach (var entry in lst)
                entry.Pattern = pattern;
            return lst;
        }

        public static List<PatternEntry> GetEntryNames(string commentPattern)
        {
            var res = GetEntryNamesOrException(commentPattern);
            if (res.Item2 != null)
                throw res.Item2;
            return res.Item1;
        }

        public static Tuple<List<PatternEntry>, Exception> GetEntryNamesOrException(string commentPattern)
        {
            var lst = new List<PatternEntry>();
            int index = -1;
            while (-1 != (index = commentPattern.IndexOf('{', index + 1)))
            {
                var entry = new PatternEntry() { StartIndex = index };
                entry.EndIndex = commentPattern.IndexOf('}', index + 1);
                if (entry.EndIndex == -1)
                    return Tuple.Create(lst, (Exception)new TechnicalException("Invalid comment pattern! Missing '}'!", null));
                var str = commentPattern.Substring(index + 1, entry.EndIndex - index + 1 -"{".Length -"}".Length);
                var commentIndex = str.IndexOf('#');
                if (commentIndex >= 0)
                {
                    entry.Comment = str.Substring(commentIndex + 1).Trim();
                    entry.Name = str.Remove(commentIndex).Trim();
                    if (string.IsNullOrWhiteSpace(entry.Name))
                        return Tuple.Create(lst, (Exception)new TechnicalException("Invalid comment pattern! Name of entry cannot be blank!", null));
                }
                else
                    entry.Name = str.Trim();
                lst.Add(entry);
            }
            return Tuple.Create(lst, (Exception)null);
        }
    }
}
