﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

using TechnicalTools.Logs;
using TechnicalTools.Tools;
using TechnicalTools.Model;


namespace ApplicationBase.Business.Support
{
    /// <summary>
    /// Helper class that send all messages we want to Ryver
    /// </summary>
    public class MessageSenderToRyver : MessageSender
    {
        public MessageSenderToRyver(string postChannelURL, string botDisplayedName, object mainOwner, params ITinyService[] dependencies)
            : base(mainOwner, dependencies)
        {
            _postChannelURL = postChannelURL;
            _botDisplayedName = botDisplayedName;
        }
        static readonly ILogger _log = LogManager.Default.GetLogger(typeof(MessageSenderToRyver));
        readonly string _postChannelURL;
        readonly string _botDisplayedName;


        protected override int Handle(IReadOnlyCollection<string> messagesToSend)
        {
            const int stringStreamLimitLength = 8192; // found by empirical test
            return SplitAndPost(messagesToSend, stringStreamLimitLength, MessageToStream, Post);
        }

        MemoryStream MessageToStream(string msg)
        {
            var requestObject = new RyverRequestObject()
            {
                body = msg,
                createSource = new RyverRequestObject.CreateSource { displayName = _botDisplayedName }
            };

            var stream = new MemoryStream();
            var ser = new DataContractJsonSerializer(requestObject.GetType());
            ser.WriteObject(stream, requestObject);
            stream.Position = 0;
            return stream;
        }
        
        void Post(MemoryStream stream)
        {
            using (var sr = new StreamReader(stream))
            {
                using (var httpContent = new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json"))
                using (var httpClient = new HttpClient())
                {
                    try
                    {
                        // This PostAsync is supposed atomic
                        var httpResponseMessage = httpClient.PostAsync(_postChannelURL, httpContent).Result;
                        // Result of this task return "responseContent" that need to be deserialized with
                        // Newtonsoft.Json.JsonConvert.DeserializeObject(responseContent)
                        httpResponseMessage.Content.ReadAsStringAsync().Wait();
                    }
                    catch (Exception ex)
                    {
                        // Requalify in silent exception so it is logged but don't bother support
                        throw new SilentBusinessException("Cannot sent a message to ryver (will try again though)", ex);
                    }
                }
            }
        }
        // Class defined only to be JSON-ified
        [DataContract]
        class RyverRequestObject
        {
            [DataMember]
            public string body { get; set; }
            [DataMember]
            public CreateSource createSource { get; set; }

            [DataContract]
            public class CreateSource
            {
                [DataMember]
                public string displayName { get; set; }
            }
        }
    }
}
