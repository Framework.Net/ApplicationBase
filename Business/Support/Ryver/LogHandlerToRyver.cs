﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Logs;
using TechnicalTools.Tools;


namespace ApplicationBase.Business.Support
{
    /// <summary> 
    /// Capture and send all bad messages to Ryver 
    /// </summary> 
    public class LogHandlerToRyver : DeferredLogHandler<DAL.Logging.Log>
    {
        public LogHandlerToRyver(LogManager logManager, AuthenticationManager manager, SupportNotificationsRyverConfig cfg, 
                                 object mainOwner, params ITinyService[] dependencies)
            : base(logManager, null /* we delay the owner assignement ...*/, dependencies)
        {
            _cfg = cfg;
            var logSender = dependencies == null ? null : dependencies.OfType<Logs.DefaultDbLogSender>().ToArray();
            _sender = new MessageSenderToRyver(_cfg.PostChannelURL, cfg.BotDisplayName.IfBlankUse(manager.CurrentUserReal.Identifiant), null /*must be done by next line*/, logSender);
            AddDependencies(_sender); //... so all dependencies are added before ...
            if (mainOwner != null)
                AddOwner(mainOwner); //... we do it ourself
        }
        readonly SupportNotificationsRyverConfig _cfg;
        readonly MessageSenderToRyver _sender;
        static readonly ILogger _log = LogManager.Default.GetLogger(typeof(LogHandlerToRyver));

        protected override bool IsObjectInteresting(DAL.Logging.Log obj)
        {
            return obj.Level == Level.Error
                && _cfg.TriggeringTags.Any(kvp => kvp.Value
                                               && obj.Tags.Contains(kvp.Key))
                && !obj.Tags.ToLowerInvariant().Contains(LogTags.Ignored);
        }

        protected override int Handle(IReadOnlyCollection<DAL.Logging.Log> logsToHandle)
        {
            foreach (var log in logsToHandle) 
            { 
                var ryverFormattedMsg = _cfg.NotifiedUsers + ": **Something requires your attention!**" + Environment.NewLine
                                        + log.ToFormatedMessage(false)
                                        + (_cfg.AlertImageUrl == null ? "" : Environment.NewLine + "[](" + _cfg.AlertImageUrl + ")");
                _sender.ScheduleHandle(ryverFormattedMsg);
            }
            return logsToHandle.Count;
        }
    }
}
