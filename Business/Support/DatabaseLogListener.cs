﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;

using TechnicalTools.Tools;

using DataMapper;

using ApplicationBase.Common;
using ApplicationBase.DAL.Logging;


namespace ApplicationBase.Business.Support
{
    /// <summary>
    /// This class just load all logs from database like a linux-tail command using pooling.
    /// And it passes log instances to a callback (consume) in chronological order.
    /// Only logs that have been created after time "from" will be loaded.
    /// </summary>
    public class DatabaseLogListener : IDisposable
    {
        public DatabaseLogListener(DateTime from, Action<Log> consume, TimeSpan? poolingInterval = null, TimeSpan? retryInterval = null)
        {
            _from = from;
            _consume = consume;
            _poolingIntervalInMs = poolingInterval ?? new TimeSpan(0, 0, 60);
            _retryInterval = retryInterval ?? new TimeSpan(0, 0, 2);
        }
        readonly DateTime _from;
        readonly Action<Log> _consume;
        readonly TimeSpan _poolingIntervalInMs;
        readonly TimeSpan _retryInterval;

        #region IDisposable Support

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _mustExit = true;
                }
                disposedValue = true;
            }
        }
        bool disposedValue = false;
        public void Dispose() { Dispose(true); }

        #endregion

        public void Run()
        {
            if (_thLoop != null)
                if (_mustExit)
                    _thLoop?.Join(); 
                else
                    return;
            _thLoop = new Thread(ThreadLoop_LoadDbLog);
            _thLoop.Start();
        }
        Thread _thLoop;

        public void StopAndJoin()
        {
            _mustExit = true;
            if (!_exited)
                _reExited.WaitOne();
        }
        volatile bool _mustExit;
        volatile bool _exited;
        readonly ManualResetEvent _reExited = new ManualResetEvent(false);

        void ThreadLoop_LoadDbLog()
        {
            DateTime _lastCheckUTC = DateTime.Today.Date.ToUniversalTime();
            try
            {
                while (!_mustExit)
                {
                    List<Log> logs = null;
                    var sessions = new List<LogSession>();
                    using (new MemoryHolder(sessions))
                    {
                        while (!_mustExit)
                        {
                            try
                            {
                                DateTime dtUntil = DateTime.UtcNow;
                                logs = DB.Dao_Base.LoadCollection<Log>(DB.Dao_Base.GetColumnNameFor((Log l) => l.TimestampUTC) + " >  " + DbMappedField.ToSql(_lastCheckUTC) +
                                                             " and " + DB.Dao_Base.GetColumnNameFor((Log l) => l.TimestampUTC) + " <= " + DbMappedField.ToSql(dtUntil))
                                                  .OrderBy(log => log.TimestampUTC)
                                                  .ToList();
                                sessions = DB.Dao_Base.GetObjectsWithIds<LogSession>(logs.Select(log => log.Session_Id));
                                _lastCheckUTC = dtUntil;
                                break;
                            }
                            catch
                            {
                                // Because sometimes there are issues 
                                // (for example if objects are sent to a database, tools like LogStash / Kibana / some users can lock tables...)
                                Thread.Sleep(_retryInterval);
                            }
                        }
                        if (!_mustExit)
                            try
                            {
                                foreach (var log in logs)
                                    _consume(log);
                            }
                            catch
                            {
                                // ignored
                            }
                    }
                    Thread.Sleep(_poolingIntervalInMs);
                }
            }
            finally
            {
                _exited = true;
                _reExited.Set();
            }
        }
    }
}
        