﻿using System;
using System.Collections.Concurrent;


namespace ApplicationBase.Business.Support
{
    public abstract class SupportNotificationsConfigBase
    {
        /// <summary> Tell if we listen error from user of type "Service" or all user types. </summary>
        public bool                               ListenOnlyServiceUserType { get; set; }
        /// <summary> Tags that make log relevant for posting to messenger. </summary>
        public ConcurrentDictionary<string, bool> TriggeringTags            { get; } = new ConcurrentDictionary<string, bool>();
    }
}
