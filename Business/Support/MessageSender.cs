﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using TechnicalTools;
using TechnicalTools.Tools;
using TechnicalTools.Model;


namespace ApplicationBase.Business.Support
{
    /// <summary>
    /// Helper class (as parent class)
    /// </summary>
    public abstract class MessageSender : ThreadDeferredObjectHandler<string>
    {
        protected MessageSender(object mainOwner, params ITinyService[] dependencies)
            : base(mainOwner, dependencies)
        {
            AggregateSequentialAndSimilarExceptions = true;
        }

        internal static int SplitAndPost(IReadOnlyCollection<string> messagesToSend, int stringStreamLimitLength, Func<string, MemoryStream> streamify, Action<MemoryStream> post)
        {
            var streamEmpty = streamify("");
            var emptyMessageStreamLength = streamEmpty.Length;
            streamEmpty.Dispose();

            int take = messagesToSend.Count;
            while (true)
            {
                const string ToContinueMsg = " [To continue]";
                var fullMessage = messagesToSend.Take(take).Join(Environment.NewLine)
                                + (messagesToSend.Count != take ? ToContinueMsg : "");
                using (var stream = streamify(fullMessage))
                {
                    var msgLengthInByte = stream.Length - emptyMessageStreamLength;
                    if (msgLengthInByte > stringStreamLimitLength)
                    {
                        if (take <= 1)
                            throw new NotImplementedException($"Not expecting message more longer than {stringStreamLimitLength} characters!");
                        take = (int)(take * stringStreamLimitLength / msgLengthInByte);
                    }
                    else
                    {
                        post(stream);
                        break;
                    }
                }
            }
            return take;
        }
    }
}
