﻿using System;
using System.Collections.Concurrent;


namespace ApplicationBase.Business.Support
{
    public class SupportConfig
    {
        public SupportNotificationsConfig Notifications { get; } = new SupportNotificationsConfig(); 
    }
    public class SupportNotificationsConfig
    {
        public SupportNotificationsRyverConfig Ryver { get; } = new SupportNotificationsRyverConfig();
        public SupportNotificationsSlackConfig Slack { get; } = new SupportNotificationsSlackConfig();
    }
}
