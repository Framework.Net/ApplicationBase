﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Logs;
using TechnicalTools.Tools;


namespace ApplicationBase.Business.Support
{
    /// <summary> 
    /// Capture and send all bad messages to Slack 
    /// </summary> 
    public class LogHandlerToSlack : DeferredLogHandler<DAL.Logging.Log>
    {
        public LogHandlerToSlack(LogManager logManager, AuthenticationManager manager, SupportNotificationsSlackConfig cfg, 
                                 object mainOwner, params ITinyService[] dependencies)
            : base(logManager, null /* we delay the owner assignement ...*/, dependencies)
        {
            _cfg = cfg;
            var logSender = dependencies == null ? null : dependencies.OfType<Logs.DefaultDbLogSender>().ToArray();
            _sender = new MessageSenderToSlack(_cfg.PostChannelURL, null /*must be done by next line*/, logSender);
            AddDependencies(_sender); //... so all dependencies are added before ...
            if (mainOwner != null)
                AddOwner(mainOwner); //... we do it ourself
        }
        readonly SupportNotificationsSlackConfig _cfg;
        readonly MessageSenderToSlack _sender;
        static readonly ILogger _log = LogManager.Default.GetLogger(typeof(LogHandlerToSlack));

        protected override bool IsObjectInteresting(DAL.Logging.Log obj)
        {
            return obj.Level == Level.Error
                && _cfg.TriggeringTags.Any(kvp => kvp.Value
                                               && obj.Tags.Contains(kvp.Key))
                && !obj.Tags.ToLowerInvariant().Contains(LogTags.Ignored);
        }

        protected override int Handle(IReadOnlyCollection<DAL.Logging.Log> logsToHandle)
        {
            foreach (var log in logsToHandle) 
            { 
                var SlackFormattedMsg = _cfg.NotifiedUsers + ": **Something requires your attention!**" + Environment.NewLine
                                        + log.ToFormatedMessage(false);
                _sender.ScheduleHandle(SlackFormattedMsg);
            }
            return logsToHandle.Count;
        }
    }
}
