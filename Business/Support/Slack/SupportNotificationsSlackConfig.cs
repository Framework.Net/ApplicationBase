﻿using System;


namespace ApplicationBase.Business.Support
{
    public class SupportNotificationsSlackConfig : SupportNotificationsConfigBase
    {
        /// <summary> URL generated by slack, see https://api.slack.com/apps, adcreate an app, then click on it, then add feature Incoming webhook</summary>
        public string                             PostChannelURL            { get; set; }
        /// <summary> Written in the same way we would write in slack, if blank "@channel" will be used. </summary>
        public string                             NotifiedUsers             { get; set; }
    }
}
