﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

using TechnicalTools.Logs;
using TechnicalTools.Tools;
using TechnicalTools.Model;


namespace ApplicationBase.Business.Support
{
    /// <summary>
    /// Helper class that send all messages we want to Slack
    /// </summary>
    public class MessageSenderToSlack : MessageSender
    {
        public MessageSenderToSlack(string postChannelURL, object mainOwner, params ITinyService[] dependencies)
            : base(mainOwner, dependencies)
        {
            _postChannelURL = postChannelURL;
        }
        static readonly ILogger _log = LogManager.Default.GetLogger(typeof(MessageSenderToSlack));
        readonly string _postChannelURL;

        // From https://api.slack.com/docs/rate-limits
        protected override int Handle(IReadOnlyCollection<string> messagesToSend)
        {
            const int stringStreamLimitLength = 4000; // From https://api.slack.com/docs/rate-limits
            return SplitAndPost(messagesToSend, stringStreamLimitLength, MessageToStream, Post);
        }

        MemoryStream MessageToStream(string msg)
        {
            var requestObject = new Payload()
            {
                text = msg,
            };

            var stream = new MemoryStream();
            var ser = new DataContractJsonSerializer(requestObject.GetType());
            ser.WriteObject(stream, requestObject);
            stream.Position = 0;
            return stream;
        }
        
        void Post(MemoryStream stream)
        {
            using (var sr = new StreamReader(stream))
            {
                using (var httpContent = new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json"))
                using (var httpClient = new HttpClient())
                {
                    try
                    {
                        // This PostAsync is supposed atomic
                        var httpResponseMessage = httpClient.PostAsync(_postChannelURL, httpContent).Result;
                        // Result of this task return "responseContent" that need to be deserialized with
                        // Newtonsoft.Json.JsonConvert.DeserializeObject(responseContent)
                        httpResponseMessage.Content.ReadAsStringAsync().Wait();
                    }
                    catch (Exception ex)
                    {
                        // Requalify in silent exception so it is logged but don't bother support
                        throw new SilentBusinessException("Cannot sent a message to slack (will try again though)", ex);
                    }
                }
            }
        }

        // Class defined only to be JSON-ified
        [DataContract]
        class Payload
        {
            [DataMember]
            public string text  { get; set; }
        }
    }
}
