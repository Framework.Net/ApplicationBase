using System;

// Hack to allow C# 9.0 in solution 8-J
// See https://stackoverflow.com/a/64749403
// Do not change the namespace (remove or disable code with pragma if code is compiled on core framework)
// TODO : to get C# 11 read/apply this https://stackoverflow.com/a/74447498/294998
namespace System.Runtime.CompilerServices
{
    internal static class IsExternalInit { }
}