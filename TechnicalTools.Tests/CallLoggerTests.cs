﻿using System;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using TechnicalTools.Tools.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TechnicalTools.Tests
{
    [TestClass]
    public class CallLoggerTests
    {
        [ClassInitialize]
        public static void ClassInitialize(TestContext ctx)
        {
            CallLogger.Instance.ConfigureForApplication();
        }


        [TestMethod]
        public void SimpleTest()
        {
            Check(codeToTest: () =>
                {
                    // if we write this code
                    int i = CallLogger.Instance.LogCall(() =>
                    {
                        return 42;
                    }, "Simple");
                    Assert.AreEqual(i, 42);
                }, onEventBeginMethod: (ctx, log) =>
                {
                    Assert.AreEqual(log, ctx.RootCall);

                    Assert.AreEqual(log.MemberName, "Simple");
                    Assert.IsTrue(log.IsFunction.HasValue);
                    Assert.IsTrue(log.IsFunction.Value);
                    Assert.IsNull(log.Args);
                    Assert.IsNull(log.Exception);

                    Assert.IsNull(log.Result);
                    Assert.IsFalse(log.EndTime.HasValue);
                },
                onEventEndContext: ctx => // event BeginMethod would alow to check this
                {
                    var log = ctx.RootCall;
                    Assert.AreEqual(log.MemberName, "Simple");
                    Assert.IsTrue(log.IsFunction.HasValue);
                    Assert.IsTrue(log.IsFunction.Value);
                    Assert.IsNull(log.Args);
                    Assert.IsNull(log.Exception);

                    Assert.AreEqual(log.Result, 42);
                    Assert.IsTrue(log.EndTime.HasValue);
                    Assert.AreEqual(0, ctx.ErrorCount);

                    var xml = new LogXmlSerializer() { IgnoreNonDeterministicValue = true }.SerializeToXML(ctx);
                    Assert.AreEqual(xml.ToString(),
                        "<CallLog>" + Environment.NewLine +
                        "  <Function Name=\"Simple\">" + Environment.NewLine +
                        "    <Result>42</Result>" + Environment.NewLine +
                        "  </Function>" + Environment.NewLine +
                        "</CallLog>");
                });
        }

        [TestMethod]
        public void Complex()
        {
            Check(codeToTest: () =>
            {
                try
                {
                    // if we write this code
                    int i = CallLogger.Instance.LogCall(() =>
                    {
                        int j = CallLogger.Instance.LogCall(() =>
                        {
                            return 51;
                        }, "SecondMethod");

                        CallLogger.Instance.LogCall(() =>
                        {
                            throw new Exception("Error Msg");
                        }, "ThirdMethodThatThrow");
                        return 42;
                    }, "FirstMethod");
                    Assert.Fail("Should throw !");
                }
                catch
                {
                }
            }, onEventBeginMethod:null,
            onEventEndContext: ctx => // event BeginMethod would allow to check this
            {
                // Transform result to XML and remove/modify some useless data for test
                var xml = new LogXmlSerializer() { IgnoreNonDeterministicValue = true }.SerializeToXML(ctx);
                Assert.AreEqual(xml.ToString(),
                                "<CallLog>" + Environment.NewLine +
                                "  <Function Name=\"FirstMethod\">" + Environment.NewLine +
                                "    <Function Name=\"SecondMethod\">" + Environment.NewLine +
                                "      <Result>51</Result>" + Environment.NewLine +
                                "    </Function>" + Environment.NewLine +
                                "    <Sub Name=\"ThirdMethodThatThrow\">" + Environment.NewLine +
                                "      <Exception Message=\"Error Msg\" />" + Environment.NewLine +
                                "    </Sub>" + Environment.NewLine +
                                "    <Exception Message=\"Same Exception\" />" + Environment.NewLine +
                                "  </Function>" + Environment.NewLine +
                                "</CallLog>");
            });
        }


        void Check(Action codeToTest, Action<LogContext, LogMemberCall> onEventBeginMethod, Action<LogContext> onEventEndContext)
        {
            Exception ex = null;
            Action<CallLogger, LogContext, LogMemberCall> h1 = (CallLogger _, LogContext ctx, LogMemberCall log)
                => { try { onEventBeginMethod(ctx, log); } catch (Exception e) { ex = e; } };

            if (onEventBeginMethod != null)
                CallLogger.Instance.BeginMethod += h1;
            Action<CallLogger, LogContext> h2 = (CallLogger _, LogContext ctx)
                => { try { onEventEndContext(ctx); } catch (Exception e) { ex = e; } };
            if (onEventEndContext != null)
                CallLogger.Instance.CommitLogContext += h2;
            try
            {
                codeToTest();
            }
            finally
            {
                CallLogger.Instance.BeginMethod -= h1;
                CallLogger.Instance.CommitLogContext -= h2;
            }
            if (ex != null)
                throw ex; // Pour que le test foire !
        }
        T Check<T>(Func<T> codeToTest, Action<LogContext, LogMemberCall> onEventBeginMethod, Action<LogContext> onEventEndContext)
        {
            Action<CallLogger, LogContext, LogMemberCall> h1 = (CallLogger _, LogContext ctx, LogMemberCall log) => onEventBeginMethod(ctx, log);
            if (onEventBeginMethod != null)
                CallLogger.Instance.BeginMethod += h1;
            Action<CallLogger, LogContext> h2 = (CallLogger _, LogContext ctx) => onEventEndContext(ctx);
            if (onEventEndContext != null)
                CallLogger.Instance.CommitLogContext += h2;
            try
            {
                return codeToTest();
            }
            finally
            {
                CallLogger.Instance.BeginMethod -= h1;
                CallLogger.Instance.CommitLogContext -= h2;
            }
        }


    }
}
