﻿using System;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace TechnicalTools.Tests.Extensions
{
    [TestClass()]
    public class String_Extensions
    {
        [TestMethod()]
        public void Tests()
        {
            Assert.AreEqual("abc".Uncamelify(), "abc");
            Assert.AreEqual("Abc".Uncamelify(), "Abc");
            Assert.AreEqual("ABc".Uncamelify(), "A Bc");
            Assert.AreEqual("ABC".Uncamelify(), "ABC");
            Assert.AreEqual("abcDef".Uncamelify(), "abc Def");
            Assert.AreEqual("AbcDef".Uncamelify(), "Abc Def");
            Assert.AreEqual("abc_def".Uncamelify(), "abc def");
            Assert.AreEqual("ABC_def".Uncamelify(), "ABC def");
            Assert.AreEqual("abc__def".Uncamelify(), "abc  def");
            Assert.AreEqual("ABC__def".Uncamelify(), "ABC  def");
            Assert.AreEqual("IsHTTPRequest".Uncamelify(), "Is HTTP Request");
            Assert.AreEqual("This is HTTP".Uncamelify(), "This is HTTP");
            Assert.AreEqual("Is3DS".Uncamelify(), "Is 3DS");
            Assert.AreEqual("ThereIs11Dimensions".Uncamelify(), "There Is 11 Dimensions");
        }
    }
}
