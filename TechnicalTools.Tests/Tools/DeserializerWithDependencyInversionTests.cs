﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace TechnicalTools.Tools.Tests // Do not change sub namespace !
{
    public class Config
    {
        [ConfigType(FullTypeName = "TechnicalTools.Tools.Tests.Sub.OtherConfig", AssemblyName = "TechnicalTools.Tests")]
        public object Foo;

        public int asUsual; // this value will be deserialiazed as usual
    }
    namespace Sub
    {
        public class OtherConfig // Consider this is declared in an other assembly "OtherConfig" _WHICH_ _DEPENDS_ _ON_ _A_
        {
            public int bar;
        }
    }

    [TestClass]
    public class DeserializerWithDependencyInversionTests
    {
        [TestMethod]
        public void SimpleTest()
        {
            var cfg = DeserializerWithDependencyInversion.DeserializeFromXmlString<Config>(" <Config>  <Foo><bar>51</bar></Foo>   <asUsual>42</asUsual></Config>");
            Assert.AreEqual(42, cfg.asUsual);
            Assert.IsInstanceOfType(cfg.Foo, typeof(Sub.OtherConfig));
            Assert.AreEqual(51, (cfg.Foo as Sub.OtherConfig).bar);

            var cfg2 = DeserializerWithDependencyInversion.DeserializeFromXmlString<Config>("<Config>  <Foo></Foo>  </Config>");
            Assert.IsInstanceOfType(cfg.Foo, typeof(Sub.OtherConfig));
        }
    }
}
