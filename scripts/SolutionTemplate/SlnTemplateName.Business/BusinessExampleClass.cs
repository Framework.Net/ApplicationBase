﻿using System;

using TechnicalTools.Model;


namespace SlnTemplateName.Business
{
    public class BusinessExampleClass : IUserInteractiveObject
    {
        public object DoAnyOperation()
        {
            return new object(); // any result
        }
    }
}
