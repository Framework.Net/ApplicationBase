﻿using System;

using SuperBase = ApplicationBase.Common;
using Base = ApplicationBase.Common;


namespace SlnTemplateName.Common
{
    public class CommonFactory : Base.CommonFactory
    {
        public CommonFactory()
        {
        }

        public override SuperBase.Config CreateConnectionsConfig()
        {
            return new Config();
        }
    }
}
