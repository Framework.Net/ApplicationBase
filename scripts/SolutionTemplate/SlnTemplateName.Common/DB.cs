﻿using System;
using System.Diagnostics;

namespace SlnTemplateName.Common
{
    public partial class DB : ApplicationBase.Common.DB
    {
        public new static Config Config
        {
            get { return (Config)ApplicationBase.Common.DB.Config; }
        }
    }
}
