﻿using System;
using System.Windows.Forms;


namespace SlnTemplateName.UI
{
    public class ShowMainGui : ApplicationBase.UI.ShowMainGuiBase
    {
        public ShowMainGui(ApplicationBase.Business.Config cfg, DateTime atDate)
            : base(cfg, atDate)
        {
        }

        protected override Form CreateMainForm()
        {
            return new MainForm();
        }
    }
}
        