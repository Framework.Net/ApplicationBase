﻿using System;
using System.Drawing;

using TechnicalTools;

using ApplicationBase.Deployment.Data;

using SlnTemplateName.Common;


namespace SlnTemplateName.UI
{
    // Tool to use: http://paletton.com
    public static class GraphicalChart
    {
        static readonly Color StealPurple = Color.FromArgb(75, 64, 136); // #4B4088

        public static readonly Color MainAppColor = StealPurple;

        public static Color GetColor(eEnvironment env)
        {
            return DB.Config.Environment.Domain == eEnvironment.Prod ? MainAppColor
                 : DB.Config.Environment.Domain == eEnvironment.Sandbox ? Color.Goldenrod
                 : DB.Config.Environment.Domain == eEnvironment.PreProd ? Color.DodgerBlue
                 : DB.Config.Environment.Domain == eEnvironment.Dev ? Color.SlateGray
                 : DB.Config.Environment.Domain == eEnvironment.LocalNoDB ? Color.LightSkyBlue
                 : ((Color?)null).ThrowIfNull($"Unknown Domain {DB.Config.Environment.Domain}!");
        }
    }
}
