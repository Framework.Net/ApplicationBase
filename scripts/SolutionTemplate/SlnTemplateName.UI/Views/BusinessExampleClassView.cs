﻿using System;
using System.Windows;

using TechnicalTools.UI.DX;

using ApplicationBase.DAL.Users;
using ApplicationBase.UI;


namespace SlnTemplateName.UI.Views
{
    public sealed partial class BusinessExampleClassView : ApplicationBaseView
    {
        readonly Business.BusinessExampleClass _businessManager;

        public BusinessExampleClassView(Business.BusinessExampleClass businessManager = null)
             : base(businessManager, Feature.AllowEverybody)
        {
            _businessManager = businessManager;
            InitializeComponent();
            if (_businessManager == null)
                return;
            Text = nameof(Business.BusinessExampleClass) + "View";
        }

        private void btnAnyActionExample_Click(object sender, EventArgs e)
        {
            this.ShowBusyWhileDoing("Doing action example", pr => _businessManager.DoAnyOperation())
                .ContinueWithUIWorkOnSuccess(t => MessageBox.Show("Done!"));
        }
    }
}
