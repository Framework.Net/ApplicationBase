REM ******** Action à faire manuellement avant de continuer ce script ******** 

REM - Dans "Visual studio installer" il faut cocher le workload "Desktop development with C++"
REM   Dans VS 2019 on peut vérifier rapidement / installer ce package en utilisant le menu "Tools" => "Get Tools and features".

REM - Installer Java "11" (une version superieur à 11 provoque des problèmes de compatibilite avec sonarQube)
REM   La dernière version supportée, à ma connaissance, est la 11.0.11 disponible ici : https://www.oracle.com/java/technologies/javase-jdk11-downloads.html
REM   ou ici (pas de compte necessaire) : https://www.softpedia.com/get/Programming/SDK-DDK/Sun-Java-JDK.shtml

REM - Télécharger SonarQube (Community Edition) ici (https://www.sonarqube.org/downloads/) 
REM   et dezipper le dans un dossier (ex : D:\Automation\Bins\sonarqube\)
REM   Créer un fichier "Version 8.7.1.42226.txt" à l'intérieur du dossier pour consulter rapidement la version.
REM   Ne gardez pas le numero de version dans le nom du dossier pour des update simple plus tard

REM - Telecharger et dezipper Sonar scanner C# (version .NET Framework 4.6+) ici (https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-msbuild/) 
REM   Attention! il faut faire "unzip to folder..." 
REM   Dezipper le en tant quesous dossier de sonarqube par exemple (ex: D:\Automation\Bins\sonarqube\sonar-scanner-msbuild)
REM   Créer un fichier "Version 5.1.0.28487-net46.txt" à l'intérieur du dossier pour consulter rapidement la version.
REM   Ne gardez pas le numero de version dans le nom du dossier pour des update simple plus tard

REM - Ajouter le repertoire D:\Automation\Bins\sonar-scanner-msbuild a la variable d'environnement windows "PATH"
REM   (en le faisant manuellement, via la fenetre de windows, en cherchant "variable d'environnement", cela évite d'avoir à redémarrer windows
REM   (Mais il faudra redemarrer ce script)

REM - Dans le fichier <sonarqube>\sonar-scanner-msbuild\SonarQube.Analysis.xml, décommentez  la ligne <Property Name="sonar.host.url">http://localhost:9000</Property>
REM   Et UNIQUEMENT le login (pas le password !!!)

REM - Dans le fichier <sonarqube>\conf\wrapper.conf changez 
REM   wrapper.java.command=java
REM   par (a ajuster en fonction de la versio nde java installé)
REM   wrapper.java.command=C:\Program Files\Java\jdk-11.0.11\bin\java

REM - Lancer le serveur sonar qube en executant simplement D:\Automation\Bins\sonarqube\bin\windows-x86-64\StartSonar.bat 
REM   Attendre 20 secondes avant que "SonarQube is up" n'apparaisse
REM   Connectez vous ensuite sur http://localhost:9000 avec comme login: admin et mot de passe:admin
REM   cliquer sur l'image en haut à droite (le "A") et cliquer sur "My Account' dans le menu deroulant
REM   Dans l'onglet Security, générez un token (pue importe le nom du token)
REM   Copiez le et remplissez  le "Login" dans le fichier ou vous l'avez decommentez (<sonarqube>\sonar-scanner-msbuild\SonarQube.Analysis.xml)
REM   (rappel: Ne decomentez PAS le password)
REM   Recopiez ce token egalement dans la variable SONAR_LOGIN_TOKEN ci-dessous (sans les "'")

REM - Penser à déplacer temporairement le dossier .git des sous modules (si vous en avez) le temps de faire l'analyse car les métriques du sous module sont compté,
REM   mais les fichiers non accessibles (via l'interface web) et ca provoque ce type de warning sur la console :
REM   WARN: File 'C:\...\oneFile.cs' referenced by the protobuf 'TokenTypeInfo' does not exist in the analysis context
REM   De plus, au cas ou, si des projets sont compté comme des projets de test ils ne seront pas incorporé non plus
REM   Il faut explicitement écrire <SonarQubeTestProject>false</SonarQubeTestProject> dans le csproj

REM **** Appuyer sur entrer (ou redemarrer le script) quand les taches ci-dessus sont faites pour lancer l'analyse ****
pause

set SolutionFile=ApplicationBase [No DX].sln
set SolutionName=ApplicationBase
set SONAR_LOGIN_TOKEN='<a 40 character long token>'

if "%SONAR_LOGIN_TOKEN%" == "" (
  echo Variable SONAR_LOGIN_TOKEN not set!
  echo Press any key to quit
  pause
)

REM Clear an environment variable that may exist which trigger error in nuget
set VERSION=
REM Télécharge les packages de la solution
./scripts/tools/nuget.exe restore "%SolutionFile%"

REM Charge l'environnement de Visual Studio pour avoir ses outils  dans le PATH
REM For VS 2015
REM call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x86_amd64
REM For VS 2017
REM call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x86_amd64
REM For VS 2019
REM Must not be called twice or cause error like "Too long line input" (this error message isn ot accurate)
if NOT "%vcvarsall_called%" == "yes" (
 call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x86_amd64
 set vcvarsall_called=yes
)


REM Genere un num de version basé sur la date au pseudo format "yyyy-MM-dd__hh'h'mm'm'ss's'"
REM Get the time from WMI - At least that's a format we can work with
set X=
for /f "skip=1 delims=" %%x in ('wmic os get localdatetime') do if not defined X set X=%%x
REM echo.%X%

rem dissect into parts
set DATE.YEAR=%X:~0,4%
set DATE.MONTH=%X:~4,2%
set DATE.DAY=%X:~6,2%
set DATE.HOUR=%X:~8,2%
set DATE.MINUTE=%X:~10,2%
set DATE.SECOND=%X:~12,2%
set DATE.FRACTIONS=%X:~15,6%
set DATE.OFFSET=%X:~21,4%

set version=%DATE.YEAR%-%DATE.MONTH%-%DATE.DAY%_%DATE.HOUR%h%DATE.MINUTE%m%DATE.SECOND%s


REM  Ready to start sonarQube
REM : Ajouter /d:sonar.cs.vstest.reportsPaths="%CD%\TestResults\*.trx" a la ligne  de commande, puis lancer les test (xunit?) avant le "...msbuild.exe end" (cf https://docs.sonarqube.org/pages/viewpage.action?pageId=6389772
REM https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-msbuild/)
SonarScanner.MSBuild.exe begin /k:"%SolutionName%" /n:"%SolutionName%" /v:"%version%" /d:sonar.host.url="http://localhost:9000" /d:sonar.login="%SONAR_LOGIN_TOKEN%"

REM  Ready to run compilation (it needs about ~4 min)
REM Le switch /m semble faire bugguer la compilation (lock de fichier entre les différent msbuild en parallèle)
msbuild "%SolutionFile%" /p:Configuration="Debug" /p:Platform="x64" /t:Rebuild

REM  Ready to gather SonarQube results
SonarScanner.MSBuild.exe end /d:sonar.login="%SONAR_LOGIN_TOKEN%"

REM All is done Done. Go to localhost:9000 now and browse the result
