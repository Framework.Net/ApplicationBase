@echo off

setlocal 
set nopause=0
set skipGit=

:parsing_arg_loop
      :: Thanks to https://stackoverflow.com/a/34552964
      ::-------------------------- has argument ?
	  :: in %~1 - the ~ removes any wrapping " or '.
      if ["%~1"]==[""] (
        goto parsing_arg_end
      )
      ::-------------------------- argument exist ?
	  if ["%~1"]==["--nopause"] set nopause=1
	  if ["%~1"]==["--skipGit"] set "skipGit=--skipGit"

      ::--------------------------
      shift
      goto parsing_arg_loop


:parsing_arg_end

echo.
if ["%skipGit%"] == [""] (
  echo  ** Loading git submodule
  echo git submodule update --init --recursive
  git submodule update --init --recursive
)

echo ** Generating "No DX" version of solution/project files **
"scripts\AutonomousCSharpScripter.exe" --run "scripts\Generate No DX solution files (Gui).c#s"

if ["%skipGit%"] == [""] (
  echo ** Make all git repositories "standalone" **
  "scripts\AutonomousCSharpScripter.exe" --run "scripts\Git Submodule undo absorbgitdirs (Gui).c#s"
)


if ["%nopause%"] == ["0"] (
  echo DONE! Press any key to leave this script
  pause
)
endlocal
