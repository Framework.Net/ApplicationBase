﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.UI;

using DataMapper;

using ApplicationBase.Business.DataRelation;


namespace ApplicationBase.UI.Controls
{
    // Class based on LoginSetSelector
    [ToolboxItem(true)]
    public partial class MapperSetSelector : TechnicalTools.UI.DX.Controls.CheckedTreeListLookUpEdit
    {
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IReadOnlyCollection<IDbMapperReadAccess> SelectedMappers
        {
            get
            {
                return ItemsSelected.Cast<MapperGroup>().SelectMany(set => set).Distinct().ToList();
            }
            set
            {
                var selected = new FixedBindingList<MapperGroup>();
                var toSelect = (value ?? Enumerable.Empty<IDbMapperReadAccess>()).ToHashSet();
                // Try to match big known groups instead of showing all mappers in "value" individually
                // at the end, each mapper has its own group so everything in value is matched
                foreach (var grp in _allMapperGroups.OrderByDescending(grp => grp.Count)) //_allMapperGroups is expected to already sorted, but just in case...
                    if (toSelect.Count >= grp.Count &&
                        grp.All(pe => toSelect.Contains(pe)))
                    {
                        selected.Add(grp);
                        foreach (var pe in grp)
                            toSelect.Remove(pe);
                    }
                Debug.Assert(toSelect.Count == 0, $"Maybe method {nameof(Fill)} has not been called previously!");
                if (ItemsSelected != null)
                    ItemsSelected.ListChanged -= ItemsSelected_ListChanged;
                ItemsSelected = selected;
                if (ItemsSelected != null)
                    ItemsSelected.ListChanged += ItemsSelected_ListChanged;
            }
        }
        public event ListChangedEventHandler SelectionChanged;

        private void ItemsSelected_ListChanged(object sender, ListChangedEventArgs e)
        {
            SelectionChanged?.Invoke(this, e);
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IReadOnlyCollection<IDbMapperReadAccess> UnavailableMappers
        {
            get
            {
                return _UnavailableMapper;
            }
            set
            {
                _UnavailableMapper = value.ToList();
                var forbidden  = (value ?? Enumerable.Empty<IDbMapperReadAccess>()).ToHashSet();
                var disabled = new List<MapperGroup>();
                foreach (var grp in _allMapperGroups.OrderByDescending(grp => grp.Count)) //_allMapperGroups is expected to already sorted, but just in case...
                    if (grp.Any(pe => forbidden.Contains(pe)))
                        disabled.Add(grp);
                ItemsDisabled = disabled;
            }
        }
        List<IDbMapperReadAccess> _UnavailableMapper;

        public MapperSetSelector()
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            popupContainerEdit.BeforePopup += popupContainerEdit_BeforePopup;
        }

        public void Fill(IReadOnlyCollection<IDbMapperReadAccess> mappers)
        {
            _allMapperGroups.Clear();
            if (mappers.Count > 1)
            {
                var all = new MapperGroup("All", () => mappers);
                _allMapperGroups.Add(all);
            }
            foreach (IDbMapperReadAccess mapper in mappers)
            {
                var lst = new[] { mapper }; // prevent the creation of array each time lambda will be accessed
                _allMapperGroups.Add(new MapperGroup(mapper.Owner.Name, () => lst));
            }
            SelectedMappers = null;
            ItemsAvailablesSet(_allMapperGroups.Where(grp => grp.Count > 0));
            SelectedMappers = _allMapperGroups.FirstOrDefault();
            _firstPopupSizingDone = false;
        }
        readonly List<MapperGroup> _allMapperGroups = new List<MapperGroup>();

        private void popupContainerEdit_BeforePopup(object _, EventArgs __)
        {
            if (!_firstPopupSizingDone)
            {
                _firstPopupSizingDone = true; // not a big deal if the following does not work

                var minItemDisplay = 30;
                minItemDisplay = Math.Min(minItemDisplay, ItemsAvailables.Count());
                var minSizeBefore = popupContainerEdit.Properties.PopupFormMinSize;
                // This code is not accurate because the calcul should a + b * nb_item
                // and because of skin that can change
                // But that's not a problem
                int heightByItem = 21;
                var newSize = new Size(minSizeBefore.Width,
                                       Math.Max(65 + minItemDisplay * heightByItem, minSizeBefore.Height));
                popupContainerEdit.Properties.PopupFormSize = newSize;
            }
        }
        bool _firstPopupSizingDone;
    }
}
