﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.UI;

using ApplicationBase.DAL.Users;
using ApplicationBase.Business;


namespace ApplicationBase.UI.Controls
{
    [ToolboxItem(true)]
    public partial class LoginSetSelector : TechnicalTools.UI.DX.Controls.CheckedTreeListLookUpEdit
    {
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IReadOnlyCollection<Login> SelectedLogins
        {
            get
            {
                return ItemsSelected.Cast<LoginGroup>().SelectMany(set => set).Distinct().ToList();
            }
            set
            {
                var selected = new FixedBindingList<LoginGroup>();
                var toSelect = (value ?? Enumerable.Empty<Login>()).ToHashSet();
                // Try to match big known groups instead of showing all login in "value" individually
                // at the end, each login has its own group so everything in value is matched
                foreach (var grp in _allLoginGroups.OrderByDescending(grp => grp.Count)) //_allLoginGroups is expected to already sorted, but just in case...
                    if (toSelect.Count >= grp.Count &&
                        grp.All(pe => toSelect.Contains(pe)) &&
                        (toSelect.Count > 1) == grp.Name.Contains("{")) // prevent use of group if there is only one login
                    {
                        selected.Add(grp);
                        foreach (var pe in grp)
                            toSelect.Remove(pe);
                    }
                Debug.Assert(toSelect.Count == 0);
                ItemsSelected = selected;
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IReadOnlyCollection<Login> UnavailableLogins
        {
            get
            {
                return _UnavailableLogins;
            }
            set
            {
                _UnavailableLogins = value.ToList();
                var forbidden  = (value ?? Enumerable.Empty<Login>()).ToHashSet();
                var disabled = new List<LoginGroup>();
                foreach (var grp in _allLoginGroups.OrderByDescending(grp => grp.Count)) //_allLoginGroups is expected to already sorted, but just in case...
                    if (grp.Any(pe => forbidden.Contains(pe)))
                        disabled.Add(grp);
                ItemsDisabled = disabled;
            }
        }
        List<Login> _UnavailableLogins;

        public LoginSetSelector()
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            popupContainerEdit.BeforePopup += popupContainerEdit_BeforePopup;
        }
        AuthenticationManager _manager;

        public void Fill(bool withDisabledLogins = false, bool withGroups = true, AuthenticationManager manager = null, IReadOnlyDictionary<Login, Person> logins = null)
        {
            _manager = manager ?? _manager ?? BusinessSingletons.Instance.GetAuthenticationManager();
            
            // The order of group in _allAuthenticationManagers are from the biggest to the smallest
            // Basically wehn user will set a selected group of logins.
            // we will "tick" the bigger group first to match his set
            var allLoginPersonPairs = logins ?? _manager.AllLogins();

            _allLoginGroups.Clear();
            List<Login> allLogins = allLoginPersonPairs.Keys.Where(login => withDisabledLogins || login.IsEnabled).ToList();
            if (withGroups)
            {
                _allLoginGroups.Add(new LoginGroup($"{{ All Logins ({allLogins.Count}) }}", allLogins));
                var tmp = allLogins.Where(login => login.IsEnabled).ToList();
                _allLoginGroups.Add(new LoginGroup($"{{ All Logins Enabled ({tmp.Count}) }}", tmp));
                if (withDisabledLogins)
                {
                    tmp = allLogins.Where(login => !login.IsEnabled).ToList();
                    _allLoginGroups.Add(new LoginGroup($"{{ All Logins Disabled ({tmp.Count}) }}", tmp));
                }
                tmp = allLogins.Where(login => login.IsEnabled && login.IsService).ToList();
                _allLoginGroups.Add(new LoginGroup($"{{ All Enabled Services ({tmp.Count}) }}", tmp));
                tmp = allLogins.Where(login => login.IsEnabled && !login.IsService).ToList();
                _allLoginGroups.Add(new LoginGroup($"{{ All Enabled Persons ({tmp.Count}) }}", tmp));
                foreach (var loginsByProfile in _manager.GetUsersByProfiles(allLogins))
                    if (loginsByProfile.Key == Profile.NoProfile)
                        _allLoginGroups.Add(new LoginGroup($"{{ All users with no profile ({loginsByProfile.Count()}) }}", loginsByProfile.ToList()));
                    else
                        _allLoginGroups.Add(new LoginGroup($"{{ All in profile \"{loginsByProfile.Key.Name}\" ({loginsByProfile.Count()}) }}", loginsByProfile.ToList()));
            }
            foreach (var kvp in allLoginPersonPairs)
                _allLoginGroups.Add(new LoginGroup(kvp.Value.FullName + " (" + kvp.Key.Identifiant + ")" + (kvp.Key.IsService ? " [Service]" : ""), new[] { kvp.Key }));

            for (int g = _allLoginGroups.Count - 1; g >= 0; --g)
                if (_allLoginGroups[g].Count == 0)
                    _allLoginGroups.RemoveAt(g);
            if (SelectedLogins.Count > 0)
                SelectedLogins = SelectedLogins.Intersect(_allLoginGroups.Flatten()).ToList();
            ItemsAvailablesSet(_allLoginGroups.Where(grp => grp.Count > 0));
            if (allLoginPersonPairs.ContainsKey(_manager.CurrentUser))
                SelectedLogins = _manager.CurrentUser.WrapInArray();
            else
                SelectedLogins = allLoginPersonPairs.Keys.Take(1).ToList();
        }
        readonly List<LoginGroup> _allLoginGroups = new List<LoginGroup>();

        
        private void popupContainerEdit_BeforePopup(object _, EventArgs __)
        {
            if (!_firstPopupSizingDone)
            {
                _firstPopupSizingDone = true; // not a big deal if the following does not work
               
                var nbRealGroup = _allLoginGroups.Count(grp => grp.Name.Contains("{"));
                var nbSingleItemtoDisplay = 5;
                var minItemDisplay = nbRealGroup + nbSingleItemtoDisplay;
                minItemDisplay = Math.Min(minItemDisplay, ItemsAvailables.Count());
                var minSizeBefore = popupContainerEdit.Properties.PopupFormMinSize;
                // This code is not accurate because the calcul should a + b * nb_item
                // and because of skin that can change
                // But that's not a problem
                int heightByItem = 22;
                var newSize = new Size(minSizeBefore.Width,
                                       Math.Max(minItemDisplay * heightByItem, minSizeBefore.Height));
                popupContainerEdit.Properties.PopupFormSize = newSize;
            }
        }
        bool _firstPopupSizingDone;
    }
}
