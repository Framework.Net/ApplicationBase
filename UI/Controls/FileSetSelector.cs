﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.UI;

using ApplicationBase.Business;
using ApplicationBase.Business.FileImporting;


namespace ApplicationBase.UI.Controls
{
    [ToolboxItem(true)]
    public partial class FileSetSelector : TechnicalTools.UI.DX.Controls.CheckedTreeListLookUpEdit
    {
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IReadOnlyCollection<string> SelectedFiles
        {
            get
            {
                return ItemsSelected.Cast<FileGroup>().SelectMany(set => set).Distinct().ToList();
            }
            set
            {
                var selected = new FixedBindingList<FileGroup>();
                var toSelect = (value ?? Enumerable.Empty<string>()).ToHashSet();
                // Try to match big known groups instead of showing all login in "value" individually
                // at the end, each file has its own group so everything in value is matched
                foreach (var grp in _allFileGroups.OrderByDescending(grp => grp.Count)) //_allFileGroups is expected to already sorted, but just in case...
                    if (toSelect.Count >= grp.Count &&
                        grp.All(pe => toSelect.Contains(pe)) &&
                        (toSelect.Count > 1) == grp.Name.Contains("{")) // prevent use of group if there is only one file
                    {
                        selected.Add(grp);
                        foreach (var pe in grp)
                            toSelect.Remove(pe);
                    }
                Debug.Assert(toSelect.Count == 0);
                ItemsSelected = selected;
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IReadOnlyCollection<string> UnavailableFiles
        {
            get
            {
                return _UnavailableFiles;
            }
            set
            {
                _UnavailableFiles = value.ToList();
                var forbidden  = (value ?? Enumerable.Empty<string>()).ToHashSet();
                var disabled = new List<FileGroup>();
                foreach (var grp in _allFileGroups.OrderByDescending(grp => grp.Count)) //_allFileGroups is expected to already sorted, but just in case...
                    if (grp.Any(pe => forbidden.Contains(pe)))
                        disabled.Add(grp);
                ItemsDisabled = disabled;
            }
        }
        List<string> _UnavailableFiles;

        public FileSetSelector()
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            popupContainerEdit.BeforePopup += popupContainerEdit_BeforePopup;
        }

        public void Fill(List<string> allFiles)
        {
            _allFileGroups.Clear();
            _allFileGroups.Add(new FileGroup($"{{ All Files ({allFiles.Count}) }}", allFiles));
            foreach (var grp in allFiles.GroupBy(f => Path.GetDirectoryName(f)))
            {
                _allFileGroups.Add(new FileGroup($"{{ All in folder \"{grp.Key}\" ({grp.Count()}) }}", grp.ToList()));
            }
            foreach (var file in allFiles)
                _allFileGroups.Add(new FileGroup(file, new[] { file }));

            if (SelectedFiles.Count > 0)
                SelectedFiles = SelectedFiles.Intersect(_allFileGroups.Flatten()).ToList();
            ItemsAvailablesSet(_allFileGroups.Where(grp => grp.Count > 0));
            SelectedFiles = allFiles.ToList();
        }
        readonly List<FileGroup> _allFileGroups = new List<FileGroup>();

        
        private void popupContainerEdit_BeforePopup(object _, EventArgs __)
        {
            if (!_firstPopupSizingDone)
            {
                _firstPopupSizingDone = true; // not a big deal if the following does not work
               
                var nbRealGroup = _allFileGroups.Count(grp => grp.Name.Contains("{"));
                var nbSingleItemtoDisplay = 5;
                var minItemDisplay = nbRealGroup + nbSingleItemtoDisplay;
                minItemDisplay = Math.Min(minItemDisplay, ItemsAvailables.Count());
                var minSizeBefore = popupContainerEdit.Properties.PopupFormMinSize;
                // This code is not accurate because the calcul should a + b * nb_item
                // and because of skin that can change
                // But that's not a problem
                int heightByItem = 22;
                var newSize = new Size(minSizeBefore.Width,
                                       Math.Max(minItemDisplay * heightByItem, minSizeBefore.Height));
                popupContainerEdit.Properties.PopupFormSize = newSize;
            }
        }
        bool _firstPopupSizingDone;
    }
}
