﻿using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Style", "IDE1006:Naming Styles",
                           Scope = "Module", //Target = "TechnicalTools.UI.DX",
                           Justification = "VS itself does not generate UI event handler which are compliant to the rule." +
                                           "And don't know how to write rule about enum starting by 'e' (in lowercase) yet." +
                                           "Code style rule seems not woring because VS ask for capitalzed enum name")]