﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools.UI.DX;


namespace ApplicationBase.UI.Helpers
{
    public static class DXExcelExport
    {
        public static void ExportExcel<T>(IEnumerable<T> lst, string filename = null)
        {
            var gc = new EnhancedGridControl();
            var gv = new EnhancedGridView(gc);
            gv.PopulateColumns(typeof(T));
            gc.DataSource = lst is IList ? lst : lst.ToList();
            gc.ForceInitialize();
            gc.BindingContext = new System.Windows.Forms.BindingContext();
            EnhancedGridView_ExportExcel.DefaultExportToExcel(gv, false, true, filename);
        }
    }
}
