﻿using System;
using System.Drawing;
using System.Linq;

using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

using ApplicationBase.Business;


namespace ApplicationBase.UI.Helpers
{
    public static class GridViewHelper
    {
        public static RepositoryItemButtonEdit CreateButtonEditor(string caption)
        {
            var repo = new RepositoryItemButtonEdit();
            repo.Buttons[0].Caption = caption;
            //repo.Buttons[0].Appearance.BackColor = GraphicalChart.Green; repo.Buttons[0].Appearance.Options.UseBackColor = true;
            //repo.Buttons[0].Appearance.ForeColor = GraphicalChart.Text; repo.Buttons[0].Appearance.Options.UseForeColor = true;
            repo.Buttons[0].Appearance.Font = new Font(repo.Buttons[0].Appearance.Font, FontStyle.Bold);

            repo.Buttons[0].Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph;
            repo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            repo.LookAndFeel.UseDefaultLookAndFeel = false;
            return repo;
        }


        #region Make a text cell editable by using an ergonomic way to enter a lot of text with hole (data) in it

        public static void SetupCommentPatternEditor(GridColumn col, CommentPatternSubSet set, bool userCanEdit, Action<int, string> setCommentForRowHandle)
        {
            var gv = (GridView)col.View;

            void customRowCellEdit(object _, CustomRowCellEditEventArgs e)
            {
                if (!gv.IsDataRow(e.RowHandle))
                    return;

                if (e.Column == col)
                {
                    // OptionsView.RowAutoHeight 
                    var repo = new RepositoryItemMemoEdit()
                    {
                        NullText = null,
                        AutoHeight = true,
                        WordWrap = true
                    };
                    repo.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
                    // Dot NOT set value of ReadOnly after having e.RepositoryItem assigned or it can cause serious violation exception (crash)
                    // Moreover when typing in filter row, it can make focus is lost and reaquired so user lost what he typed first
                    // BUG to submit to DevExpress
                    repo.ReadOnly = !userCanEdit;
                    e.RepositoryItem = repo;
                }
            }
            gv.CustomRowCellEdit += customRowCellEdit;

            void customRowCellEditForEditing(object _, CustomRowCellEditEventArgs e)
            {
                if (!gv.IsDataRow(e.RowHandle))
                    return;
                if (e.Column == col)
                {
                    var repoItemMRUComment = new RepositoryItemMRUEdit
                    {
                        ReadOnly = !userCanEdit,
                        AllowRemoveMRUItems = false,
                        Appearance =
                        {
                            Options = {UseTextOptions = true},
                            TextOptions = {WordWrap = WordWrap.Wrap}
                        },
                        AutoHeight = false,
                    };
                    repoItemMRUComment.Items.Clear();
                    repoItemMRUComment.Items.AddRange(set.OriginalSet.Select(p => p.KeyWord).ToList());
                    repoItemMRUComment.UseCtrlScroll = false;
                    repoItemMRUComment.EditValueChanged += (sender, __) => RepoItemMRUComment_EditValueChanged(sender as MRUEdit, set);
                    e.RepositoryItem = repoItemMRUComment;
                }
            }
            gv.CustomRowCellEditForEditing += customRowCellEditForEditing;

            gv.PopupMenuShowing += (_, e) =>
            {
                if (e.Menu == null || !e.HitInfo.InDataRow)
                    return;
                var rowHandle = e.HitInfo.RowHandle;

                if (e.HitInfo.Column == col)
                {
                    var frm = new CommentPatternSelectorPopup(set) { ReadOnly = !userCanEdit };
                    frm.ShowAsPopup(gv.GridControl);
                    frm.UserSelect += (__, ___) =>
                    {
                        setCommentForRowHandle(rowHandle, frm.FinalComment);
                        gv.RefreshRow(rowHandle);
                        gv.LayoutChanged(); // met à jour la hauteur des rows (car certaines cellules peuvent s'afficher sur plusieurs lignes)
                    };
                    e.Menu = null;
                    gv.GridControl.BeginInvoke((Action)(() => { e.Menu?.HidePopup(); })); // in case the menu would be recreated
                }
            };
        }

        static void RepoItemMRUComment_EditValueChanged(MRUEdit mru, CommentPatternSubSet commentPatternsSet)
        {
            foreach (var pattern in commentPatternsSet.OriginalSet)
                if (pattern.KeyWord.ToLowerInvariant() == mru.Text.ToLowerInvariant())
                {
                    mru.BeginInvoke((Action)(() =>
                    {
                        if (!mru.IsDisposed)
                        {
                            mru.EditValue = pattern.Pattern;
                            mru.Text = pattern.Pattern;
                            mru.SelectionStart = pattern.Pattern.Length;
                        }
                    }));
                    break;
                }
        }

        #endregion
    }
}
