﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;

using DevExpress.Utils;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;

using TechnicalTools;
using TechnicalTools.Model.Cache;

using ApplicationBase.DAL.Users;
using ApplicationBase.DAL.UserDataFix;
using ApplicationBase.Common;
using ApplicationBase.Business.UserDataFix;


namespace ApplicationBase.UI.Helpers
{
    public class BusinessRecordEditOnGridViewDisplayer<TDataSourceItemAsBusinessItem> 
        : BusinessRecordEditOnGridViewDisplayer<TDataSourceItemAsBusinessItem, TDataSourceItemAsBusinessItem>
        where TDataSourceItemAsBusinessItem : class, IHasTypedIdReadable
    {
        public BusinessRecordEditOnGridViewDisplayer(BusinessRecordEditSet editSet, GridView view)
            : base(editSet, view, obj => obj)
        {
        }
    }
    public class BusinessRecordEditOnGridViewDisplayer<TDataSourceItem, TBusinessItem>
        where TDataSourceItem : class
        where TBusinessItem : class, IHasTypedIdReadable
    {
        public bool   EditedValue_ShowCorrectorFluid { get; set; } = true;
        public Color? EditedValue_ForeColor;

        public BusinessRecordEditOnGridViewDisplayer(BusinessRecordEditSet editSet, GridView view,
                                                     Func<TDataSourceItem, TBusinessItem> getNestedRecord,
                                                     IReadOnlyDictionary<string, string> columnFieldnameToRealDisplayFieldName = null)
        {
            _editSet = editSet;
            _view = view;
            _getNestedRecord = getNestedRecord;
            _columnFieldnameToRealDisplayFieldName = columnFieldnameToRealDisplayFieldName;
        }
        readonly BusinessRecordEditSet _editSet;
        readonly GridView _view;
        readonly Func<TDataSourceItem, TBusinessItem> _getNestedRecord;
        readonly IReadOnlyDictionary<string, string> _columnFieldnameToRealDisplayFieldName;
        ToolTipController _ttc;
        bool _ttcCreated;

        public void Install()
        {
            Uninstall();
            // Remarks: What is the difference between view.GridControl.ToolTipController and view.GridControl.GetToolTipController() ?
            _ttc = _view.GridControl.ToolTipController;
            _ttcCreated = _ttc == null;
            if (_ttcCreated)
                _view.GridControl.ToolTipController = _ttc = new ToolTipController();

            _ttc.GetActiveObjectInfo += ttc_GetActiveObjectInfo;
            _view.GridControl.Disposed += GridControl_Disposed;
            _view.Disposed += view_Disposed;
            // Postpone the subscribing so the RowCellStyle handler will be excuted after other ones
            // This is usually the behavior we want
            _view.GridControl.BeginInvoke((Action)(() =>
            {
                _view.CustomDrawCell += View_CustomDrawCell;
                // This event allows color & formatting to be taken in consideration when exporting to excel
                //_view.RowCellStyle += view_RowCellStyle;
            }));
        }

        public void Uninstall()
        {
            _view.Disposed -= view_Disposed;
            _view.CustomDrawCell -= View_CustomDrawCell;
            //_view.RowCellStyle -= view_RowCellStyle;
            if (_view.GridControl != null)
                UninstallTooltipController(_view.GridControl);
        }
        void UninstallTooltipController(GridControl gc)
        {
            if (_ttc != null)
            {
                _ttc.GetActiveObjectInfo -= ttc_GetActiveObjectInfo;
                if (_ttcCreated && gc.ToolTipController == _ttc)
                    gc.ToolTipController = null;
                if (gc.ToolTipController == null)
                    _ttc.Dispose();
            }
            _ttcCreated = false;
            _ttc = null;
        }

        void view_Disposed(object sender, EventArgs e)
        {
            Uninstall();
        }
        void GridControl_Disposed(object sender, EventArgs e)
        {
            var gc = sender as GridControl;
            UninstallTooltipController(gc);
        }

        void ttc_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.SelectedControl != _view.GridControl ||
                _view.GridControl.FocusedView != _view)
                return;

            // Get the view's element information that resides at the current position
            GridHitInfo hi = _view.CalcHitInfo(e.ControlMousePosition);
            if (!hi.InDataRow || !hi.InRowCell)
                return;
            var rowHandle = hi.RowHandle;
            var col = hi.Column;

            var edits = GetEditsOrNothing(_view, rowHandle, col);
            if (edits == null)
                return;

            string text = string.Empty;
            foreach (var edit in edits)
            {
                var who = DB.Dao_Base.GetObjectWithId<Person>(edit.WhoId);
                var type = edit.PropertyType.ToTypeFromAssemblyQualifiedStringWithoutVersion();
                text += $"{who.Firstname} {who.Lastname} has changed this value on {edit.WhenUTC.ToLocalTime().ToString("F", enUs)} to \"{edit.NewValue}\" from \"{edit.OldValue}\"." 
                        + Environment.NewLine;
            }
            var dsItem = _view.GetRow(rowHandle) as TDataSourceItem;
            var businessItem = _getNestedRecord(dsItem);
            string propName = _columnFieldnameToRealDisplayFieldName?.TryGetValueClass(col.FieldName)
                            ?? col.FieldName;
            string toolTipUID = "business edits of " + businessItem.TypedId.ToString() + "." + propName;
            e.Info = new ToolTipControlInfo(toolTipUID, text, $"Original value was {edits.Last().OldValue}")
            {
                ToolTipImage = Properties.Resources.CorrectorPot_svg48x48
            };
        }
        static readonly CultureInfo enUs = CultureInfo.CreateSpecificCulture("en-US");
       
        void View_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (!EditedValue_ShowCorrectorFluid)
                return;
            var cellInfo = e.Cell as GridCellInfo;
            if (!(cellInfo.ViewInfo is TextEditViewInfo editViewInfo)) // We dont handle other ViewInfo yet (like CheckEditViewInfo )
                return;
            var edits = GetEditsOrNothing((GridView)sender, e.RowHandle, e.Column);
            if (edits == null)
                return;
            // MaskBoxRect is where text is displayed, its location is relative to e.Bounds
            var drawableArea = editViewInfo.MaskBoxRect;
            drawableArea.X += e.Bounds.X;
            drawableArea.Y += e.Bounds.Y;

            //e.DefaultDraw(); // Just for the border
            e.Appearance.DrawBackground(e.Cache, drawableArea);

            var format = e.Appearance.TextOptions.GetStringFormat();
            format.Trimming = StringTrimming.EllipsisCharacter;
            var strLength = (int)e.Graphics.MeasureString(e.DisplayText, e.Appearance.Font, 10000, format).Width;

            int margin = 1; // margin between text and pot
            var potSize = new Size(16, 16);
            Rectangle potRect = new Rectangle(Point.Empty, potSize);
            var left = e.Appearance.HAlignment == HorzAlignment.Far
                     // Text is aligned at right, so we draw pot at left of string or at worse at cell's left border
                     ? Math.Max(drawableArea.Left, drawableArea.Right - strLength - potSize.Width)
                     : e.Appearance.HAlignment == HorzAlignment.Near
                     // Text is aligned at left , so we draw pot at right of string or at worse at cell's right border
                     ? Math.Min(drawableArea.Right - potSize.Width, drawableArea.Left + strLength + margin)
                     // Text is centered, so we draw pot at right of string or at worse at cell's right border
                     : Math.Min(drawableArea.Right - potSize.Width, drawableArea.Left + (drawableArea.Width + strLength) / 2 + margin);
            potRect.Offset(left, drawableArea.Top);

            var textRect = drawableArea;
            textRect.Width = Math.Min(strLength, drawableArea.Width - potSize.Width - margin);
            textRect.X = e.Appearance.HAlignment == HorzAlignment.Far
                        ? Math.Max(drawableArea.Left + potSize.Width + margin, drawableArea.Right - strLength)
                        : e.Appearance.HAlignment == HorzAlignment.Near
                        ? drawableArea.Left
                        : drawableArea.Left + (drawableArea.Width - strLength) / 2;

            // Aggrandit un peu l'espace pour simuler l'étalement du fluide correcteur
            textRect.Inflate(editViewInfo.MaskBoxRect.X, 0);
            e.Graphics.DrawImage(Properties.Resources.CorrectionOld, textRect);
            textRect.Inflate(-editViewInfo.MaskBoxRect.X, 0);
            e.Graphics.DrawImage(Properties.Resources.CorrectorPot_svg16x16, potRect);

            if (textRect.Width > 0)
                e.Appearance.DrawString(e.Cache, e.DisplayText, textRect, format);

            e.Handled = true;
        }

        void view_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (!EditedValue_ForeColor.HasValue)
                return;
            var edits = GetEditsOrNothing((GridView)sender, e.RowHandle, e.Column);
            if (edits != null)
            {
                e.Appearance.ForeColor = EditedValue_ForeColor.Value;
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private List<BusinessRecordEdit> GetEditsOrNothing(GridView gv, int rowHandle, GridColumn col)
        {
            if (rowHandle < 0 || col == null)
                return null;
            // Can happens if all items are not of the same type
            // It happens also sometimes when some code remove the last line,
            // DevExpress triggers RowCellStyle event and GetRow() returns null even with no cast
            if (!(gv.GetRow(rowHandle) is TDataSourceItem dsItem))
                return null;
            var businessItem = _getNestedRecord(dsItem);
            if (businessItem == null)
                return null;
            var objEdits = _editSet.GetAllUserBusinessEdits(businessItem.TypedId);
            if (objEdits == null)
                return null;
            string propName = _columnFieldnameToRealDisplayFieldName?.TryGetValueClass(col.FieldName)
                           ?? col.FieldName;
            var edits = objEdits.TryGetValueClass(propName);
            if (edits == null || edits.Count == 0)
                return null;
            return edits;
        }
    }
}
