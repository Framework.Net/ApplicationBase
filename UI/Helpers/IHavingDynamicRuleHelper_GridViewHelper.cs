using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Data.Filtering;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;

using TechnicalTools;
using TechnicalTools.Annotations;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Controls;
using TechnicalTools.UI.DX.Helpers;
using TechnicalTools.UI.DX.Forms;

using DataMapper;
using DataMapper.Helpers;

using ApplicationBase.DAL;
using ApplicationBase.DAL.Users;
using ApplicationBase.Business;
using ApplicationBase.Business.MetaProgramming.DecisionTree;
using ApplicationBase.Business.GraphVisualization.Graphviz;


namespace ApplicationBase.UI.Helpers
{
    public abstract class IHavingDynamicRuleHelper_GridViewHelper<TRule, IRule> : IHavingDynamicRuleHelper_GridViewHelper
        where TRule : BaseDTO, IRule, IHasTypedIdReadable, ICloneable, new()
        where IRule : class, ICopyable<IRule>, IHasTypedIdReadable, IHavingDynamicRule
    {
        public IHavingDynamicRuleHelper_GridViewHelper(GridView gv, GridViewForEditableSet<TRule, IRule> helper, EditableDTOSet<TRule, IRule> set, Feature feature)
            : base(typeof(TRule), typeof(IRule), gv, helper, set, feature)
        {
        }

        protected override IHavingDynamicRule CreateClone(IHavingDynamicRule rule)
        {
            IRule clone = new TRule();
            clone.CopyFrom((IRule)rule);
            return clone;
        }

        public override void Setup()
        {
            base.Setup();
            _gv.PopupMenuShowing += OnPopupMenuShowing;
        }
        protected override void OnPopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            base.OnPopupMenuShowing(sender, e);
            if (e.Menu == null) // Se produit quand on clique en bas de la zone du footer (1 pixel au dessus de la limite du bas) + une autre condition inconnu encore
                return;
            var view = sender as GridView; Debug.Assert(view != null, nameof(view) + " != null");

            if (_set is IDynamicRuleSet ruleSet)
            {
                var selectedRules = view.GetSelectedBusinessObjects<IRule>()
                                        .Where(r => !r.Disabled)
                                        .Except(_set.Removed.Cast<IRule>())
                                        .ToDictionary(r => (IHavingDynamicRule)r, r => (int)r.Id.Keys[0]);
                e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, new DXMenuItem($"Show decision tree for these {selectedRules.Count} selected rules (edits are taken into account)", (_, __) =>
                {
                    ShowGraph("Decision tree for selected rules (with edition)", selectedRules, ruleSet.RulesMustMatchEvaluatedItemType, ruleSet.GetResult);
                })
                { Enabled = selectedRules.Count > 0 });
                var allRules = ruleSet.GetProductionRuleSet();
                e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, new DXMenuItem("Show full decision tree currently used in production (without edits unsaved)", (_, __) =>
                {
                    ShowGraph("Full decision tree currently used in production", allRules, ruleSet.RulesMustMatchEvaluatedItemType, ruleSet.GetResult);
                })
                { Enabled = allRules.Count > 0 });
            }
        }

        protected void ShowGraph(string popupTitle, IReadOnlyDictionary<IHavingDynamicRule, int> prodRules, bool rulesMustMatchEvaluatedItemType, Func<IHavingDynamicRule, string> getResult)
        {
            var owner = _gv.GridControl.FindForm();
            owner.ShowBusyWhileDoing("Converting...", pr =>
                {
                    var generator = new GraphVizGenerator();
                    if (!generator.IsGraphVizInstalled)
                        throw new UserUnderstandableException("Cannot show graph because GraphViz has not been shipped in this release!", null);

                    var builder = new Builder();
                    pr.Report("Building decision tree");
                    var lambda = builder.BuildDecisionTreeFromRule(prodRules.Keys.ToList(), prodRules, rulesMustMatchEvaluatedItemType);
                    pr.Report("Converting decision tree to visualizable graph");
                    var graphvizGraph = builder.ConvertDecisionTreeToGraphviz(lambda, rulesMustMatchEvaluatedItemType, prodRules, getResult);
                    pr.Report("Make a beautiful display...");
                    try
                    {
                        return (graphvizGraph.ToDot(), generator.Run(graphvizGraph.LayoutEngine, eOutputType.svg, graphvizGraph));
                    }
                    catch (Exception ex) when (ex.EnrichDiagnosticWithAndReturnFalse("An error occured while rendering graph! Ask support to fix it!", eExceptionEnrichmentType.UserUnderstandable, true, true))
                    {
                        throw; // Unreachable code!
                    }
                })
                .ContinueWithUIWorkOnSuccess((dotSvg) =>
                {
                    var svgGraph = new SVGViewer();
                    var frm = EnhancedXtraForm.CreateWithInner(svgGraph, owner);
                    frm.Size = new Size(owner.Width * 95 / 100, owner.Height * 95 / 100);
                    frm.Text = popupTitle;
                    svgGraph.Svg = dotSvg.Item2;
                    frm.Show(owner);

                    svgGraph.PopupMenuShowing += (_, e) =>
                    {
                        e.PopupMenu.AddToGroup(ePopupMenuHeaderType.Standard, new DXMenuItem("See GraphViz description",
                            (__, ___) => svgGraph.ShowInMemoForm(() => dotSvg.Item1, "GraphViz description")));
                    };
                });
        }

    }
    // Common : Edition of Rules (IHavingDynamicRule)
    public abstract class IHavingDynamicRuleHelper_GridViewHelper : IDisposable
    {
        protected readonly Type _ruleType;
        protected readonly Type _iruleType;
        protected readonly GridView _gv;
        protected readonly GridViewForEditableSet _helper;
        protected readonly IEditableDTOSet _set;
        protected readonly Feature _feature;

        internal IHavingDynamicRuleHelper_GridViewHelper(Type ruleType, Type iruleType, GridView gv, GridViewForEditableSet helper, IEditableDTOSet set, Feature feature)
        {
            _ruleType = ruleType;
            _iruleType = iruleType;
            _gv = gv;
            _helper = helper;
            _set = set;
            _feature = feature;
        }

        protected abstract IHavingDynamicRule CreateClone(IHavingDynamicRule rule);

        public virtual void Setup()
        {
            Debug.Assert(_iruleType.GetProperty(nameof(IHavingDynamicRule.DynamicMatchingRuleView)) != null);
            _gv.OptionsView.RowAutoHeight = true; // allow grid to show multiline

            _set.PropertyChanging += set_PropertyChanging;


            // We convert this column to unbound...
            var colRuleView = _gv.Columns[nameof(IHavingDynamicRule.DynamicMatchingRuleView)];
            colRuleView.FieldName += "*";
            colRuleView.Caption = "Dynamic Rule";
            _helper.DefineSubstituteEditableColumn(colRuleView, nameof(IHavingDynamicRule.DynamicMatchingRuleView));
            colRuleView.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            (colRuleView as IEnhancedGridColumn).UnboundValueGetter = (row, _) => Tuple.Create((IHavingDynamicRule)row, (row as IHavingDynamicRule).DynamicMatchingRuleView);
            (colRuleView as IEnhancedGridColumn).UnboundValueSetter = (row, newValue) => { var t = newValue as Tuple<IHavingDynamicRule, string>; t.Item1.DynamicMatchingRuleView = t.Item2; };
            // ... so we can create a specific repository for this column...
            var repoRuleView = new RepositoryItemMultiLineButtonEdit { TextEditStyle = TextEditStyles.DisableTextEditor };
            repoRuleView.ButtonClick += BusinessCodeColumn_CellEditorRepository_ButtonClick;
            // where repo is able to acces row instance (rule) and not only code business in this event
            repoRuleView.CustomDisplayText += (object _, CustomDisplayTextEventArgs e) =>
            {
                if (e.Value == null)
                    return;
                var rule = e.Value as Tuple<IHavingDynamicRule, string>;
                e.DisplayText = GetBeautifulBusinessDynamicRule(rule.Item1.TypeOnWhichRuleApplies, rule.Item2);
            };

            colRuleView.ColumnEdit = repoRuleView;
            colRuleView.FilterMode = ColumnFilterMode.DisplayText;

            _gv.Columns[nameof(IHavingDynamicRule.DynamicMatchingRule)].ColumnEdit = new RepositoryItemMultiLineButtonEdit()
            {
                TextEditStyle = TextEditStyles.DisableTextEditor
            };

            // Convert column to list of Type
            var colType = _gv.Columns[nameof(IHavingDynamicRule.TypeOnWhichRuleApplies)];
            colType.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            colType.Caption = "Type/Format On Which Rule Applies";
            colType.ConfigureForObjects(GetSubjectTypesAllowed(), t => BusinessNameAttribute.GetNameFor(t) ?? t.Name.Uncamelify());
            colType.OptionsColumn.AllowSort = DefaultBoolean.True;
            (colType.ColumnEdit as RepositoryItemLookUpEdit).AllowNullInput = DefaultBoolean.False;
            colType.InsertAfter(_gv.Columns[nameof(IHavingDynamicRule.Id)]);
            void colType_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
            {
                if (e.IsForGroupRow || e.ListSourceRowIndex < 0)
                    return;
                if (e.Column.FieldName == nameof(IHavingDynamicRule.TypeOnWhichRuleApplies) &&
                    e.Value != null)
                {
                    var t = (Type)e.Value;
                    e.DisplayText = BusinessNameAttribute.GetNameFor(t)
                                 ?? t.Name.ToString().Uncamelify();
                }
            }
            _gv.CustomColumnDisplayText += colType_CustomColumnDisplayText;

            // Remove because this a complete technical/code design column
            _gv.Columns.Remove(_gv.Columns[nameof(IHavingDynamicRule.DefaultResult)]);
            // Just hide because code is currently generated, but a smart user could edit directly later.
            _gv.Columns[nameof(IHavingDynamicRule.DynamicMatchingRule)].Visible = false;

            _gv.ShowingEditor += gvIHavingDynamicRule_ShowingEditor;
            _gv.RowCellStyle += gvIHavingDynamicRule_RowCellStyle;
        }

        protected abstract IReadOnlyCollection<Type> GetSubjectTypesAllowed();

        public void Dispose() { Dispose(true); }
        protected virtual void Dispose(bool disposing)
        {
            if (_disposedValue)
                return;
            if (disposing)
                _set.PropertyChanging -= set_PropertyChanging;
            _disposedValue = true;
        }
        bool _disposedValue; // To detect redundant calls

        string GetBeautifulBusinessDynamicRule(Type type, string businessCode)
        {
            if (businessCode == null)
                return null;
            var criteria = ParseCriteria(type, businessCode);
            if (ReferenceEquals(criteria, null))
                return null;
            return FilterCriteriaToBeautifulExpressionVisitor.Convert(criteria, type);
        }

        public static CriteriaOperator ParseCriteria(Type type, string businessCode)
        {
            if (!_initializedTypes.Contains(type))
            {
                // Necessaire car contient des effets de bord (souscription a des event static) 
                // permettant à CriteriaOperator.Parse refonctionner
                CriteriaParserInitializer.Instance.BuildFilterColumns(type, true);
                _initializedTypes.Add(type);
            }
            var criteria = CriteriaOperator.Parse(businessCode);
            return criteria;
        }
        static readonly ConcurrentBag<Type> _initializedTypes = new ConcurrentBag<Type>();

        public static void FillCSharpCode(IHavingDynamicRule rule, CriteriaOperator criteria)
        {
            if (ReferenceEquals(criteria, null)) // It happens when copying (clone) a rule using contextual menu
            {
                rule.DynamicMatchingRule = IHavingDynamicRule_Extensions.DefaultDynamicMatchingRule(defaultResult: rule.DefaultResult);
                return;
            }
            var result = FilterCriteriaToCodeCSharpVisitor.ConvertToCSharp(criteria, rule.TypeOnWhichRuleApplies, IHavingDynamicRule_Extensions.DefaultDynamicMatchingRuleVarName);
            if (result.Item2.Any())
                throw new UserUnderstandableException("Some error(s) occured:" + Environment.NewLine +
                                                      result.Item2.Select(str => "  - " + str).Join(Environment.NewLine), null);
            var cSharpCode = result.Item1;
            var codeBefore = rule.DynamicMatchingRule?.Replace(IHavingDynamicRule_Extensions.RecompilationErrorToken, "").Trim() ?? "";
            rule.DynamicMatchingRule = $"{IHavingDynamicRule_Extensions.DefaultDynamicMatchingRuleVarName} => {cSharpCode}";
            
            // If compilation fails we add a token so developper will find it fast
            var compilationError = rule.GetCompilationException();
            if (compilationError != null)
                throw new UserUnderstandableException(Application.ProductName + " has issue to understand this rule.. :-( !" + Environment.NewLine + 
                                                      "Please make sure you did not forgot to fill some value in rule... if no, contact an IT", compilationError);
        }

        void gvIHavingDynamicRule_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0) // gere les autofilter row etc
                return;
            if (e.Column.FieldName == nameof(IHavingDynamicRule.DynamicMatchingRule) ||
                e.Column.FieldName == nameof(IHavingDynamicRule.DynamicMatchingRuleView) + "*")
            {
                e.Appearance.Font = FixedFont;
            }
        }
        static readonly Font FixedFont = new Font("Consolas", 12f);

        void gvIHavingDynamicRule_ShowingEditor(object sender, CancelEventArgs e)
        {
            var view = (GridView)sender;
            var row = (IHavingDynamicRule)view.GetRow(view.FocusedRowHandle);
            if (nameof(IHavingDynamicRule.DynamicMatchingRuleView) + "*" == _gv.FocusedColumn.FieldName)
                // if there is only a developper version of criteria and no user version, we forbid edition
                // Because user will not be able to do the same thing
                e.Cancel |= !UserCanEdit(row);
            else if (nameof(IHavingDynamicRule.DynamicMatchingRule) == _gv.FocusedColumn.FieldName)
                // Developper version of criteria
                e.Cancel |= !BusinessSingletons.Instance.GetAuthenticationManager().CurrentUser.IsAdmin;
            
        }

        bool UserCanEdit(IHavingDynamicRule row)
        {
            var adminHasProbablyEditedCodeDirectly = !string.IsNullOrWhiteSpace(row.DynamicMatchingRule)
                                                  && string.IsNullOrWhiteSpace(row.DynamicMatchingRuleView)
                                                  && row.DynamicMatchingRule != IHavingDynamicRule_Extensions.DefaultDynamicMatchingRule(defaultResult: row.DefaultResult);
            return !adminHasProbablyEditedCodeDirectly
                && BusinessSingletons.Instance.GetAuthenticationManager().GetAccessFor(_feature).HasFlag(eFeatureAccess.CanEdit);
        }

        void BusinessCodeColumn_CellEditorRepository_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var editor = (ButtonEdit)sender;
            var grid = (GridControl)editor.Parent;
            var view = (GridView)grid.FocusedView;
            var row = (IHavingDynamicRule)view.GetRow(view.FocusedRowHandle);
            OnEditorButtonClick(grid, view, editor, row);
        }

        protected virtual void OnEditorButtonClick(GridControl grid, GridView view, ButtonEdit editor, IHavingDynamicRule row)
        {
            var before = (Tuple<IHavingDynamicRule, string>)editor.EditValue;
            var frm = new FilterForm(row.TypeOnWhichRuleApplies, before.Item2, true)
            {
                Text = row?.GetType().Name ?? "Filter"
            };

            ConfigureFilterForm(frm, row);
            

            while (DialogResult.OK == frm.ShowDialog())
            {
                editor.EditValue = Tuple.Create(before.Item1, frm.FilterString);
                // PostEditor does not retunr false when the setter throw an exception...
                // So we have to capture the event...
                _errorSettingCriteria = null;
                view.InvalidValueException += View_InvalidValueException;
                var posted = view.PostEditor();
                view.InvalidValueException -= View_InvalidValueException;
                if (posted && _errorSettingCriteria == null)
                    return;
                string userMessage = _errorSettingCriteria is IUserUnderstandableException
                                   ? ExceptionManager.Instance.Format(_errorSettingCriteria, true, true)
                                   : "An unexpected error occured! Please contact IT and take note about what you did exactly...";
                frm.SetMessage(userMessage);
            }
            editor.EditValue = before;
        }

        protected virtual void ConfigureFilterForm(FilterForm frm, IHavingDynamicRule row)
        {
        }

        Exception _errorSettingCriteria;
        void View_InvalidValueException(object sender, InvalidValueExceptionEventArgs e)
        {
            var exception = e.Exception;
            if (exception is EditorValueException)
                exception = (exception as EditorValueException).SourceException;
            _errorSettingCriteria = exception;
        }

        // This method can throw UserUnderstandableException
        void set_PropertyChanging(object sender, TechnicalTools.Model.PropertyChangingEventArgs e)
        {
            if (_settingViewCode)
                return;
            _settingViewCode = true;
            try
            {
                var havingDynamicRule = (IHavingDynamicRule)sender;
                // If we edit C# rule directly, we remove view display 
                // because we cannot currently map the code with a user representation
                // so edition through user representation will be disable (see gvIHavingDynamicRule_ShowingEditor)
                if (e.PropertyName.EndsWith("." + nameof(IHavingDynamicRule.DynamicMatchingRule)))
                {
                    havingDynamicRule.DynamicMatchingRuleView = null;
                }
                // If user change the rule representation we try to transform it in C#
                // If it's a success (it should be always the case !) both changes (code and representation are accepted )
                else if (e.PropertyName.EndsWith("." + nameof(IHavingDynamicRule.DynamicMatchingRuleView)))
                {
                    var clone = CreateClone(havingDynamicRule);
                    clone.DynamicMatchingRuleView = (string)e.NewValue;
                    var criteria = ParseCriteria(clone.TypeOnWhichRuleApplies, clone.DynamicMatchingRuleView);
                    FillCSharpCode(clone, criteria);
                    havingDynamicRule.DynamicMatchingRule = clone.DynamicMatchingRule;
                    // Save also business code because current column is not mapped anymore on the code property, but on the rule (see Setup)
                    // So here we do what DX should do in PropertyChanged (ie: we assign to model) if we would have mapped column correctly
                    havingDynamicRule.DynamicMatchingRuleView = (string)e.NewValue;
                }
            }
            finally
            {
                _settingViewCode = false;
            }
        }
        bool _settingViewCode;


        protected virtual void OnPopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null) // Se produit quand on clique en bas de la zone du footer (1 pixel au dessus de la limite du bas) + une autre condition inconnu encore
                return;
            if (e.Menu.Items.Count > 0)
                e.Menu.Items[0].BeginGroup = true;

            if (e.HitInfo.InDataRow)
            {
                e.Menu.AddToGroup(ePopupMenuHeaderType.Custom, new DXMenuItem("Clone this rule", (_, __) =>
                {
                    var view = (GridView)sender;
                    var rule = (IHavingDynamicRule)view.GetRow(e.HitInfo.RowHandle);
                    var clone = (IHavingDynamicRule)_set.CloneObject(rule);
                }) { Enabled = _set.AllowNew });
            }

            if (e.HitInfo.Column?.FieldName == nameof(IHavingDynamicRule.DynamicMatchingRuleView) + "*")
            {
                var view = (GridView)sender;
                var clickedRule = e.HitInfo.InRowCell ? (IHavingDynamicRule)view.GetRow(e.HitInfo.RowHandle) : null;
                e.Menu.AddToGroup(ePopupMenuHeaderType.Custom, new DXMenuItem("Copy expression", (_, __) =>
                {
                    _copiedExpression = Tuple.Create(clickedRule.TypeOnWhichRuleApplies, clickedRule.DynamicMatchingRuleView);
                })
                { Enabled = clickedRule != null });
                e.Menu.AddToGroup(ePopupMenuHeaderType.Custom, new DXMenuItem("Paste expression", (_, __) =>
                {
                    try
                    {
                        clickedRule.DynamicMatchingRuleView = _copiedExpression.Item2;
                    }
                    catch (Exception ex)
                        when (clickedRule.TypeOnWhichRuleApplies != _copiedExpression.Item1)
                    {
                        var errMsg = $"The condition you copied applies to type: {BusinessNameAttribute.GetNameFor(_copiedExpression.Item1) ?? _copiedExpression.Item1.Name}," + Environment.NewLine + 
                                     $"while rhe rule you want to edit applies on type: {BusinessNameAttribute.GetNameFor(clickedRule.TypeOnWhichRuleApplies) ?? clickedRule.TypeOnWhichRuleApplies.Name}." + Environment.NewLine + 
                                     "This causes technical problems (below) that are normal if the rule has no sense for this type." + Environment.NewLine;
                        ex.EnrichDiagnosticWith(errMsg, eExceptionEnrichmentType.UserUnderstandable, true, true);
                        throw;
                    }

                    view.RefreshData();
                })
                { Enabled = _copiedExpression != null && clickedRule != null && UserCanEdit(clickedRule) });
            }
        }
        Tuple<Type, string> _copiedExpression;

        protected virtual void CustomizeFlattenizationOfCondition(Dictionary<string, string> allKeys, IHavingDynamicRule rule, ExpandoObject eo)
        {
        }
    }



}
