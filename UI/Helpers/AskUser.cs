﻿using System;
using System.Windows.Forms;

using DevExpress.XtraEditors;

using TechnicalTools.UI.DX;


namespace ApplicationBase.UI.Helpers
{
    public static class AskUser
    {
        public static string Comment(Control owner, string comment = null)
        {
            DialogResult userStatus = DialogResult.None;
            do
            {
                if (userStatus != DialogResult.None)
                    XtraMessageBox.Show("Comment cannot be empty!");
                userStatus = InputBox.ShowDialog(owner, ref comment, "Enter comment");
                if (userStatus != DialogResult.OK)
                    return null;
            } while (string.IsNullOrWhiteSpace(comment));
            return comment.Trim();
        }
    }
}
