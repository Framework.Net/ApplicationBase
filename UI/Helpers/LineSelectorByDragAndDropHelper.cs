﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using TechnicalTools.UI.DX;

namespace ApplicationBase.UI.Helpers
{
    // inspired from https://www.devexpress.com/Support/Center/Question/Details/A1444
    public class LineSelectorByDragAndDropHelper<TRowItem>
        where TRowItem : class
    {
        public IEnumerable<TRowItem> SelectedItems { get { return _SelectedItems.ToList(); } }
        BindingList<TRowItem> _SelectedItems = new BindingList<TRowItem>();
        readonly Dictionary<TRowItem, GridView> _originsOfItems = new Dictionary<TRowItem, GridView>();

        public event EventHandler SelectedItemChanged;

        GridControl _toGC;
        GridView    _toGV;
        readonly Dictionary<GridView, bool> _ShouldItemMustBeRemovedFromViewDataSource = new Dictionary<GridView, bool>();

        public void Install(GridControl toGC, BindingList<TRowItem> selected)
        {
            _toGC = toGC;
            _toGV = _toGC.MainView as GridView;
            _SelectedItems = selected ?? new BindingList<TRowItem>();

            _toGC.DataSource = _SelectedItems;

            _toGC.AllowDrop = true;
            _toGC.DragOver += toGC_DragOver;
            _toGC.DragDrop += toGC_DragDrop;

            var form = _toGC.FindForm();
            form.KeyPreview = true; // PreviewKeyDown won't be fired otherwise !
            Debug.Assert(form.KeyPreview, "PreviewKeyDown won't be fired otherwise !");
            _toGC.PreviewKeyDown += GridControl_PreviewKeyDown;

            _SelectedItems.ListChanged += _SelectedItems_ListChanged;
        }

        void _SelectedItems_ListChanged(object sender, ListChangedEventArgs e)
        {
            SelectedItemChanged?.Invoke(this, EventArgs.Empty);
        }

        public void AddSource(GridView gv, bool removeFromSource)
        {
            _ShouldItemMustBeRemovedFromViewDataSource.Add(gv, removeFromSource);

            Debug.Assert(gv.DataSource == null || gv.DataSource is IEnumerable<TRowItem>);
            gv.DataSourceChanged += (_, __) => Debug.Assert(gv.DataSource == null || gv.DataSource is IEnumerable<TRowItem>);

            gv.MouseDown += gv_MouseDown;
            gv.MouseMove += gv_MouseMove;
        }

        private void gv_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;

            downHitInfos[view] = null;

            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));

            if (Control.ModifierKeys != Keys.None)
                return;
            if (e.Button == MouseButtons.Left && (view.IsDataRow(hitInfo.RowHandle) || view.IsGroupRow(hitInfo.RowHandle)))
                downHitInfos[view] = hitInfo;
        }


        private void gv_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;

            if (e.Button == MouseButtons.Left && 
                downHitInfos.TryGetValue(view, out GridHitInfo downHitInfo) &&
                downHitInfo != null)
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(downHitInfo.HitPoint.X - dragSize.Width / 2,
                                                             downHitInfo.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    if (view.GetSelectedRows().Contains(downHitInfo.RowHandle))
                    {
                        var selectedItems = view.GetSelectedBusinessObjects<TRowItem>().ToList();
                        view.GridControl.DoDragDrop(new ViewSelection() { Items = selectedItems, View = view }, DragDropEffects.Move);
                        DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = true;
                    }
                }
            }
        }
        class ViewSelection
        {
            public GridView View { get; set; }
            public List<TRowItem> Items { get; set; }
        }

        readonly Dictionary<GridView, GridHitInfo> downHitInfos = new Dictionary<GridView, GridHitInfo>();

        private void toGC_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ViewSelection)))
                e.Effect = DragDropEffects.Move;
            else
                e.Effect = DragDropEffects.None;
        }



        private void toGC_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetData(typeof(ViewSelection)) is ViewSelection selection)
            {
                var shouldItemMustBeRemovedFromViewDataSource = _ShouldItemMustBeRemovedFromViewDataSource[selection.View];
                var blst = (IBindingList)selection.View.DataSource;
                foreach (var item in selection.Items)
                {
                    if (_originsOfItems.ContainsKey(item))
                        continue;
                    _originsOfItems.Add(item, selection.View);
                    _SelectedItems.Add(item);
                    if (shouldItemMustBeRemovedFromViewDataSource)
                        blst.Remove(item);
                }
            }
        }

        private void GridControl_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (_toGC.Focused && e.KeyCode == Keys.Delete)
            {
                // Capture the business bojects to remove, because rowhandle reference can move after removing the first item in gridview...
                var itemsToUnselected = _toGV.GetSelectedBusinessObjects<TRowItem>().ToList();

                foreach (var item in itemsToUnselected)
                {
                    GridView view;
                    if (_originsOfItems.ContainsKey(item))
                    {
                        view = _originsOfItems[item];
                        _originsOfItems.Remove(item);
                    }
                    else
                    {
                        Debug.Assert(_ShouldItemMustBeRemovedFromViewDataSource.Count == 1);
                        view = _ShouldItemMustBeRemovedFromViewDataSource.First().Key;
                    }
                    if (_ShouldItemMustBeRemovedFromViewDataSource[view])
                    {
                        var blist = view.DataSource as IBindingList;
                        if (!blist.Contains(item))
                            (view.DataSource as IBindingList).Add(item);
                    }
                    _SelectedItems.Remove(item);
                }
            }
        }
    }

}
