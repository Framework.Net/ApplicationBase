﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools;
using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.UI.DX;


namespace ApplicationBase.UI.Helpers
{
    public class GridView_ConfiguratorProxy
    {
        public static GridView_ConfiguratorProxy Instance { get; protected set; } = new GridView_ConfiguratorProxy();

        static readonly ILogger _log = LogManager.Default.GetLogger(typeof(GridView_ConfiguratorProxy));

        /// <summary>
        /// Configure a gridview based on a an object and return an action allowing to discard dynamic behavior installed on gridview (event, etc)
        /// The action is useful to call before a grid's datasource is completely changed (ie: item type are different)
        /// </summary>
        public Delegate ConfigureFor(GridView view, GenericHierarchyGraphProvider provider, object parentObject = null)
        {
            var type = view.DataSource?.GetType()
                                      ?.GetOneImplementationOf(typeof(IReadOnlyCollection<>))
                                      ?.GetGenericArguments().First();
            if (type == null || type == typeof(object))
                type = (view.DataSource as IReadOnlyCollection<object>)?.FirstOrDefault()?.GetType() ?? type ?? typeof(object);
            view.BeginUpdate();
            var undo = _ConfigureFor(type, view, null, provider, parentObject);
            view.EndUpdate();
            return undo;
        }
        /// <summary>
        /// Same than <see cref="ConfigureFor(GridView, object)" /> but with specifying a band where to put new columns
        /// </summary>
        public Delegate ConfigureFor<T>(GridView view, GenericHierarchyGraphProvider provider, T parentObject = null)
            where T : class
        {
            view.BeginUpdate();
            var undo = _ConfigureFor(typeof(T), view, null, provider, parentObject);
            view.EndUpdate();
            return undo;
        }
        public Delegate ConfigureFor(Type type, GridView view, GenericHierarchyGraphProvider provider, object parentObject = null)
        {
            view.BeginUpdate();
            var undo = _ConfigureFor(type, view, null, provider, parentObject);
            view.EndUpdate();
            return undo;
        }

        public Delegate ConfigureFor<T>(BandedGridView view, GridBand band, GenericHierarchyGraphProvider provider, T parentObject = null)
            where T : class
        {
            view.BeginUpdate();
            var undo = _ConfigureFor(typeof(T), view, band, provider, parentObject);
            view.EndUpdate();
            return undo;
        }
        public Delegate ConfigureFor(Type type, BandedGridView view, GridBand band, GenericHierarchyGraphProvider provider, object parentObject = null)
        {
            view.BeginUpdate();
            var undo = _ConfigureFor(type, view, band, provider, parentObject);
            view.EndUpdate();
            return undo;
        }

        Delegate _ConfigureFor(Type rowTypeToDisplay, GridView view, GridBand band, GenericHierarchyGraphProvider provider, object parentObject = null)
        {
            var eview = view as EnhancedGridView;
            var ebview = view as EnhancedBandedGridView;
            if (eview == null && ebview == null)
                return null;
            Debug.Assert(band == null || ebview != null);
            Action unconfigure = ConfigureAndReturnCancelAction(rowTypeToDisplay, view, band);
            FlattenizerInstallation flattenizerInstallationHelper = null;

            var hierarchies = provider?.GetHierarchies(rowTypeToDisplay).ToList();
            if (hierarchies != null && FlattenizerInstallation.CanInstallOn(view))
            {
                if (eview != null)
                    flattenizerInstallationHelper = new FlattenizerInstallation(eview, rowTypeToDisplay, hierarchies);
                else if (ebview != null)
                    flattenizerInstallationHelper = new FlattenizerInstallation(ebview, rowTypeToDisplay, hierarchies);

                unconfigure = Combine(flattenizerInstallationHelper.Undo, unconfigure);
            }
            return unconfigure;
        }

        
        protected void ClearColumnAndBands(GridView view, GridBand band)
        {
            var eview = view as EnhancedGridView;
            var ebview = view as EnhancedBandedGridView;
            if (eview != null)
                eview.Columns.Clear();
            else if (band != null)
                foreach (BandedGridColumn col in band.Columns)
                {
                    band.Columns.Remove(col);
                    ebview.Columns.Remove(col);
                }
            else
                ebview.Columns.Clear();
        }

        protected TObject GetObject<TObject>(GridView view, int rowHandle)
            where TObject : class
        {
            if (rowHandle < 0)
                return null;
            var flattenizer = (view as IEnhancedGridView)?.DataSourceFlattenizer;
            if (flattenizer != null)
                return (TObject)flattenizer.GetRowBusinessObject(rowHandle);
            return (TObject)view.GetRow(rowHandle);
        }

        /// <summary>
        /// Change the configuration of gridview according to type of object to display
        /// And return a way to undo what has been done. 
        /// This is especially the even subscribing that need to be returned in the event, 
        /// not all assigned formatting stuff or column created.
        /// To combine multiple Action into one you can use <see cref="Combine(Action, Action)"/>;
        /// </summary>
        /// <returns></returns>
        protected virtual Action ConfigureAndReturnCancelAction(Type rowTypeToDisplay, GridView view, GridBand band)
        {
            return null; // null means "nothing at all has been modified"
        }

        // Seems useless but actually it prevents user to cast its lambda to Action
        protected static Action Combine(Action a, Action b) { return (Action)Delegate.Combine(a, b); }
    }
}
