﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Helpers;


namespace ApplicationBase.UI.Helpers
{
    // This class basically help to create & install a GridViewDataSourceFlattenizer on a view
    // It waits for the good time (datasource assigned etc) to do it
    // So this class allows developper to call FlattenizerInstallation directly in constructor (or load event) of form or user control
    // Besides it handles the step of saving restoring layout when original datasource change
    public class FlattenizerInstallation
    {
        public static bool CanInstallOn(GridView viewToConfigure)
        {
            // Check to prevent recursivity
            var eview = viewToConfigure as EnhancedGridView;
            var ebview = viewToConfigure as EnhancedBandedGridView;
            var pv = viewToConfigure;
            while (pv != null &&
                   ((pv as IEnhancedGridView)?.DataSourceFlattenizer == null))
            {
                // Check for an ultra weird case if in hierarchy we find a patern view instead of a cloned view
                // we are probably in a stacktrace with MasterRowGetLevelDefaultView instead of MasterRowExpanded !
                // This is a developper mistakes
                if (pv.ParentView != null && // this a sub detail view
                    pv.SourceView == null) // but a pattern one, not a cloned view...
                    return false;
                pv = pv.ParentView as GridView;
            }
            if (pv != null)  // some ancestor already have a flattenizer
                return false; // This is wayyyy too complex to handle for now
            var gc = viewToConfigure.GridControl as EnhancedGridControl;
            if (gc == null)
                return false;
            return true;
        }
        public FlattenizerInstallation(EnhancedGridView viewToConfigure, Type rowTypeToDisplay, IReadOnlyCollection<IHierarchyDescription> hierarchies)
            : this ((GridView)viewToConfigure, rowTypeToDisplay, hierarchies)
        { }
        public FlattenizerInstallation(EnhancedBandedGridView viewToConfigure, Type rowTypeToDisplay, IReadOnlyCollection<IHierarchyDescription> hierarchies)
            : this ((GridView)viewToConfigure, rowTypeToDisplay, hierarchies)
        { }
        private FlattenizerInstallation(GridView viewToConfigure, Type rowTypeToDisplay, IReadOnlyCollection<IHierarchyDescription> hierarchies)
        {
            _configuredView = viewToConfigure;
            _gc = (EnhancedGridControl)viewToConfigure.GridControl;
            _rowTypeToDisplay = rowTypeToDisplay;

            if (!CanInstallOn(viewToConfigure))
                throw new TechnicalException("Cannot install, it would cause a recursive issue", null);

            var eview = viewToConfigure as EnhancedGridView;
            var ebview = viewToConfigure as EnhancedBandedGridView;
            if (eview != null)
                _flattenizer = new GridViewDataSourceFlattenizer(eview, hierarchies);
            else if (ebview != null)
                _flattenizer = new GridViewDataSourceFlattenizer(ebview, hierarchies);

            // Choose the right constructor
            InstallFlattenizerIfDataSourceReady();
        }
        readonly GridViewDataSourceFlattenizer _flattenizer;
        readonly GridView                      _configuredView;
        readonly EnhancedGridControl           _gc;
        readonly Type                          _rowTypeToDisplay;

        

        public void Undo()
        {
            _gc.DataSourceChanging -= CheckReadynessOfDataSource;
            _gc.DataSourceRefreshed -= CheckReadynessOfDataSource;
            _gc.DataSourceChanging -= OnDataSourceChanging_RememberLayout;
            _gc.DataSourceChanged -= OnDataSourceChanged_ApplyAgainRememberedLayout;
            _flattenizer.Uninstall();
        }


        bool InstallFlattenizerIfDataSourceReady()
        {
            var ds = _gc.DataSource as IReadOnlyCollection<object>;
            var isDataSourceReadyForEventSetup = ds?.Count > 0;

            _gc.DataSourceChanging -= CheckReadynessOfDataSource;
            _gc.DataSourceRefreshed -= CheckReadynessOfDataSource;

            if (!isDataSourceReadyForEventSetup)
            {
                _gc.DataSourceChanging += CheckReadynessOfDataSource;
                _gc.DataSourceRefreshed += CheckReadynessOfDataSource;
                return false;
            }

            if (GridView_ConfiguratorProxy.Instance != null)
            {
                _flattenizer.PopulateColumnFor -= PopulateColumnFor;
                _flattenizer.PopulateColumnFor += PopulateColumnFor;
            }

            // Install flattenizer (can be called/redo without problem)
            if (!_flattenizer.IsInstalled)
                _flattenizer.Install(ds);

            // Install event handlers to detect and handle datasource changes
            _gc.DataSourceChanging -= OnDataSourceChanging_RememberLayout;
            _gc.DataSourceChanged -= OnDataSourceChanged_ApplyAgainRememberedLayout;
            _gc.DataSourceChanging += OnDataSourceChanging_RememberLayout;
            _gc.DataSourceChanged += OnDataSourceChanged_ApplyAgainRememberedLayout;

            ApplyAgainRememberedLayout(_gc);
            return true;
        }

        void PopulateColumnFor(object sender, GridViewDataSourceFlattenizer.PopulateEventArgs e)
        {
            var undo = e.View is BandedGridView bgv
                     ? GridView_ConfiguratorProxy.Instance.ConfigureFor(e.Type, bgv, e.Band, null)
                     : GridView_ConfiguratorProxy.Instance.ConfigureFor(e.Type, e.View, null);
            e.Handled = undo != null;
        }

        void CheckReadynessOfDataSource(object _, EventArgs __)
        {
            InstallFlattenizerIfDataSourceReady();
        }

        
        void OnDataSourceChanging_RememberLayout(object _, PropertyChangingEventArgs e)
        {
            if (_configuredView.Columns.Count == 0)
                return; // Nothing to save (!)
            var oldItemType = e.OriginalValue?.GetType().GetOneImplementationOf(typeof(IEnumerable<>))?.GetGenericArguments().First();
            if (oldItemType == null)
                return; // we don't know how to save
            var newItemType = e.NewValue?.GetType().GetOneImplementationOf(typeof(IEnumerable<>))?.GetGenericArguments().First();
            if (oldItemType == newItemType)
                return; // saving is not meaningful, the row has changed but not their types, thus neither the layout...

            var ms = new MemoryStream();
            _configuredView.SaveLayoutToStream(ms);
            _layouts[oldItemType] = ms;
        }
        readonly Dictionary<Type, MemoryStream> _layouts = new Dictionary<Type, MemoryStream>();
        void OnDataSourceChanged_ApplyAgainRememberedLayout(object sender, EventArgs e)
        {
            ApplyAgainRememberedLayout((GridControl)sender);
            InstallFlattenizerIfDataSourceReady();
        }
        void ApplyAgainRememberedLayout(GridControl gridControl)
        {
            var view = gridControl.FocusedView;
            if (view.DataSource != null)
            {
                var newItemType = view.DataSource.GetType().GetOneImplementationOf(typeof(IEnumerable<>))?.GetGenericArguments().First();
                if (newItemType != null)
                {
                    var ms = _layouts.TryGetValueClass(newItemType);
                    if (ms != null)
                    {
                        ms.Position = 0;
                        view.RestoreLayoutFromStream(ms);
                    }
                }
            }
        }
    }
}
