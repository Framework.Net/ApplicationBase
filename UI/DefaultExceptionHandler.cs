﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.Tools;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Forms.Errors;
using TechnicalTools.UI.DX.Forms.Diagnostics;
using TechnicalTools.UI.DX.Forms;

using DataMapper.Tools;

using ApplicationBase.Common;


namespace ApplicationBase.UI
{
    public class DefaultExceptionHandler : Business.Automation.DefaultExceptionHandler
    {
        public bool MakeTextOfDialogSelectable { get; set; }

        public DefaultExceptionHandler(DebugConfig cfg, Control owner)
        {
            _cfg = cfg;
            _owner = owner;
            BusyForm.DefaultExceptionHandling += BusyForm_HandleException;
            BusEventHandling();
            TimeoutHandling();
        }
        readonly DebugConfig _cfg;
        readonly Control _owner;
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(DefaultExceptionHandler));

        public override void Dispose()
        {
            base.Dispose();
            BusyForm.DefaultExceptionHandling -= BusyForm_HandleException;
            BusEventHandling_Dispose();
            TimeoutHandling_Dispose();
        }
        

        public virtual Control GetDefaultOwner()
        {
            var mainForm = _owner ?? EnhancedXtraForm.MainForm;
            Debug.Assert(mainForm != null, nameof(mainForm) + " != null");
            Debug.Assert(!(mainForm is DebugFormBase), "Because often in another thread");
            return mainForm;
        }

        #region Unexpected exception handling

        public override bool HandleException(string actionName, Exception ex)
        {
            if (!base.HandleException(actionName, ex))
                return false;
            HandleExceptionInUI(actionName, ex);
            return true;
        }
        void BusyForm_HandleException(string actionName, Exception ex)
        {
            HandleException(actionName, ex);
        }


        public virtual void HandleExceptionInUI(string actionName, Exception ex)
        {
            // there is no need to log here because 
            TechnicalErrorPopup.ProvideMaximumErrorData = _cfg.ShowRealErrorReason;

            var owner = GetDefaultOwner();
            if (owner?.InvokeRequired ?? false)
            {
                // This test does not prevent from crashing but help to not have exception in dev environement...
                if (!owner.IsDisposed && !owner.Disposing)
                    // We dont use method like ExceptionManager.Instance.IgnoreException* because we are _already_ in an event handler of ExceptionManager
                    try { owner.BeginInvoke((Action)(() => { HandleExceptionInUI(actionName, ex); })); } catch { /* nothing, app is probably closing */ };
                return;
            }

            // Note : we reinvoke UI thread to postpone the error handling and let business work to be done (just in case)
            if (ex.IsFileAlreadyOpened())
                owner.BeginInvoke((Action)(() => HandleLockedFileError(ex, actionName, owner)));
            else if (ex is IBusinessException)
                owner.BeginInvoke((Action)(() => HandleBusinessError(ex, owner)));
            else if (ex is IUserUnderstandableException)
                owner.BeginInvoke((Action)(() => HandleUserUnderstandableError(ex, owner)));
            else
                owner.BeginInvoke((Action)(() => HandleAnyOtherError(ex, actionName, owner)));
        }


        protected virtual void HandleBusinessError(Exception ex, Control owner)
        {
            var frm = new BusinessErrorPopup(ex, false);
            frm.Show(owner);
        }
        protected virtual void HandleUserUnderstandableError(Exception ex, Control owner)
        {
            var frm = new UserUnderstandableErrorPopup(ex, false);
            frm.Show(owner);
        }
        protected virtual void HandleLockedFileError(Exception ex, string actionName, Control owner)
        {
            var frm = new TechnicalErrorPopup(GetDefaultFullMessage(actionName) + Environment.NewLine + GetFileLockMessage(ex));
            frm.Show(owner);
        }
        protected virtual string GetFileLockMessage(Exception ex)
        {
            Debug.Assert(ex.IsFileAlreadyOpened());
            return "Cannot open a file, check if it is not currently open and locked by another program!";
        }
        protected virtual void HandleAnyOtherError(Exception ex, string actionName, Control owner)
        {
            _log.Error(GetDefaultFullMessage(actionName), ex);
            var frm = ex is ICriticalException ? new CriticalErrorPopup(ex) : new TechnicalErrorPopup(ex);
            // Ne mettre a true QUE si on detecte deux fois la strictement même erreur dans un laps de temps tres court
            // Cela est souvent dû a des bug dans un des handlers d'evenement de la couche graphique (par exemple Paint ou CustomColumnDisplayText)
            frm.ForceStopPopupButtonVisible = false;  // DebugTools.IsDebug && DebugTools.IsForDevelopper || 

            // When there is some probleme in unbound columns in GridView, 
            // the user constantly has a message each time gridview tries to refresh pixels...
            // So the user is blocked...
            // TODO : find a way to prevent the user to be blocked and kill application
            // Potential solutions:
            // 1) DX GridView seems to throw on CalcRowHeight and CalcRowCellDrawInfoCore.
            //    When trying to ignore the exception and return an arbitrary value for heigh, 
            //    the gridview still seems  completely broken (scrollbar does not works etc)
            // 2) Another Way is to tell user gridview is maybe broken, but it can choose to ignore the issue (the messagewon't appear)
            //    it seems the gridview is still broken too, but at least user cna close the window withouit closing application
            // 3) Send a ticket to DX Support asking a way to handle this (no support currently :'()
            // Solution chosen : we dont change anything and let user kill application (we hope for a new licence that will allow us to chose solution 3)
            //if (ex.StackTrace.Contains(".CalcRowHeight("))
            //{

            //}
            //else if (ex.StackTrace.Contains(".CalcRowCellDrawInfoCore("))
            //{

            //}
            //else
            frm.Show(owner);
            //var th = new Thread(() => ExceptionManager.EnsureNoExceptionButIgnoreIt(() => AssemblyConfig.ExceptionReporter.Show("Unexpected error :-( !", ex)));
            //th.SetApartmentState(ApartmentState.STA); // Sinon certain controle graphique plante (exemple http://stackoverflow.com/questions/135803/dragdrop-registration-did-not-succeed)
            //th.Start();        
        }

        #endregion Unexpected exception handling

        #region Bus Event messages

        void BusEventHandling()
        {
            BusEvents.Instance.BusinessWarning += BusinessEvents_BusinessWarning;
            BusEvents.Instance.BusinessMessage += BusinessEvents_BusinessMessage;
            BusEvents.Instance.UserUnderstandableMessage += BusinessEvents_UserUnderstandableMessage;
            BusEvents.Instance.TechnicalError += BusinessEvents_TechnicalError;
        }

        void BusEventHandling_Dispose()
        {
            BusEvents.Instance.BusinessWarning -= BusinessEvents_BusinessWarning;
            BusEvents.Instance.BusinessMessage -= BusinessEvents_BusinessMessage;
            BusEvents.Instance.UserUnderstandableMessage -= BusinessEvents_UserUnderstandableMessage;
            BusEvents.Instance.TechnicalError -= BusinessEvents_TechnicalError;
        }

        void BusinessEvents_BusinessWarning(object sender, BusinessWarningEventArgs e)
        {
            // Note: e has already been logged by CommandLineTask class
            var because = ""; // formatting beautifuly for user
            if (e.Exceptions.Count > 0)
            {
                var ae = e.Exceptions.Count == 1
                       ? new AggregateException("", e.Exceptions)
                       : new AggregateException(e.Exceptions);
                because = Environment.NewLine
                        + ExceptionManager.Instance.Format(ae, true, true);
            }
            if (MakeTextOfDialogSelectable)
                ShowMessage(frm => new MemoForm(e.Problem + because, "Not an emergency, but please do note that... !").InlineInit(mf => {
                    mf.Memo.ReadOnly = true;
                    mf.ShowIcon = false;
                    mf.ShowInTaskbar = false;
                }).Show(frm));
            else
                ShowMessage(frm => NonblockingXtraMessageBox.Show(frm, e.Problem + because, "Not an emergency, but please do note that... !"));
        }

        void BusinessEvents_BusinessMessage(object sender, MessageEventArgs e)
        {
            // Note: e has already been logged by CommandLineTask class
            if (MakeTextOfDialogSelectable)
                ShowMessage(frm => new MemoForm(e.Message, "Please, be aware that...").InlineInit(mf => {
                    mf.Memo.ReadOnly = true;
                    mf.ShowIcon = false;
                    mf.ShowInTaskbar = false;
                }).Show(frm));
            else
                ShowMessage(frm => NonblockingXtraMessageBox.Show(frm, e.Message, "Please, be aware that..."));
        }

        void BusinessEvents_UserUnderstandableMessage(object sender, MessageEventArgs e)
        {
            // Note: e has already been logged by CommandLineTask class
            if (MakeTextOfDialogSelectable)
                ShowMessage(frm => new MemoForm(e.Message, "Please note that...").InlineInit(mf => {
                    mf.Memo.ReadOnly = true;
                    mf.ShowIcon = false;
                    mf.ShowInTaskbar = false;
                }).Show(frm));
            else
                ShowMessage(frm => NonblockingXtraMessageBox.Show(frm, e.Message, "Please note that..."));
        }

        void BusinessEvents_TechnicalError(object sender, TechnicalErrorEventArgs e)
        {
            // Note: e has already been logged by CommandLineTask class
            if (MakeTextOfDialogSelectable)
                ShowMessage(frm => new MemoForm(e.Problem, "Technical issue !").InlineInit(mf => {
                    mf.Memo.ReadOnly = true;
                    mf.ShowIcon = false;
                    mf.ShowInTaskbar = false;
                }).Show(frm));
            else
                ShowMessage(frm => NonblockingXtraMessageBox.Show(frm, e.Problem, "Technical issue !"));
        }

        /// <summary>
        /// Helper to display message above the form considered as mainform
        /// the message box used is a non blocking messagebox
        /// Because DevExpress' XtraMessageBox blocks UI thread until user answers which defeat the purpose of using BusEvents
        /// </summary>
        /// <param name="actionToShow">Action written in this style : frm => MessageBox.Show(frm, ...[whatever you want]...)</param>
        void ShowMessage(Action<Control> actionToShow)
        {
            var owner = GetDefaultOwner();
            owner.BeginInvoke((Action)(() =>
            {
                DebugTools.Assert(owner.IsHandleCreated, "We have to take care of HandleCreated/HandleDestroyed events too!");
                if (owner.Visible)
                    actionToShow(owner);
                else
                {
                    void onVisibleChanged(object _, EventArgs __)
                    {
                        owner.VisibleChanged -= onVisibleChanged;
                        if (owner.Visible && !owner.Disposing && !owner.IsDisposed)
                            actionToShow(owner);
                    }
                    owner.VisibleChanged += onVisibleChanged;
                }
            }));
        }

        #endregion

        #region Reconnection / Timeout messages

        void TimeoutHandling()
        {
            SqlRetry.Instance.OnDisconnect += ReconnectAfterLostConnectionMessage;
            SqlRetry.Instance.OnTimeOut += ReconnectAfterTimeOutMessage;
        }
        void TimeoutHandling_Dispose()
        {
            SqlRetry.Instance.OnDisconnect -= ReconnectAfterLostConnectionMessage;
            SqlRetry.Instance.OnTimeOut -= ReconnectAfterTimeOutMessage;
        }

        void ReconnectAfterLostConnectionMessage(object sender, SqlRetry.SqlQueryIssueEventArgs e)
        {
            //LogManager.logException("Disconnection while querying: " + e.Query, e.Exception);
            //if (!IsHandleCreated)
            //    try { CreateHandle(); } catch { return; }
            //Debug.Assert(IsHandleCreated);

            //if (InvokeRequired) // A faire apres le check isHandleCreated
            //{
            //    Invoke((Action)(() => ReconnectAfterLostConnectionMessage(sender, e)));
            //    return;
            //}
            //Debug.Assert(!InvokeRequired);

            //var resultTemp = AutomaticResponseForConcurrentDisconnexion;
            //if (resultTemp == DialogResult.None) // Il s'agit du premier message de deconnexion a valider (dans le cas ou plusieur thread etait en corus de travail)
            //{
            //    resultTemp = XtraMessageBox.Show(Authentication.CurrentUser != null ? _owner : null, // Si l'utilisateur n'est pas loggué, la MessageBox forcerait l'affichage de la MainForm, donc on met null,
            //                                     "Connection was lost :( !"  +Environment.NewLine +
            //                                     "Maybe you have some trouble with your network access..." + Environment.NewLine + 
            //                                     "Try to reconnect and continue current action ? (recommended: Retry)", Application.ProductName, MessageBoxButtons.AbortRetryIgnore);
            //    if (resultTemp == DialogResult.Abort)
            //        Application.Exit();

            //    // Si des connexions provenant d'autres thread ont échoué simultanement au cans en train d'etre géré,
            //    // l'utilisateur devrait potentiellement devoir repondre a beaucoup (peut etre des centaines ) de messagebox
            //    // Tous les autres threads sont en attente à cause du BeginInvoke ou bien le lock
            //    // Quand l'utilisateur repond a la premiere question il faut repondre automatiquement la meme reponse aux autres messagebox (validation par block)
            //    AutomaticResponseForConcurrentDisconnexion = resultTemp;
            //    // Ensuite on met a la suite de la file d'attente (ie : juste apres toutes les autres deconnexion) le reset de la reponse automatique
            //    // Cela permet de reposer la question si quelque temps plus tard une nouvelle déconnexion se produit.
            //    BeginInvoke((Action)(() => AutomaticResponseForConcurrentDisconnexion = DialogResult.None));
            //}
            //e.Cancel = resultTemp != DialogResult.Retry;
        }
        //volatile DialogResult AutomaticResponseForConcurrentDisconnexion = DialogResult.None;

        void ReconnectAfterTimeOutMessage(object sender, SqlRetry.SqlQueryIssueEventArgs e)
        {
            //LogManager.logException("Timeout while querying: " + e.Query, e.Exception);
            //if (!IsHandleCreated)
            //    try { CreateHandle(); } catch { return; }
            //Debug.Assert(IsHandleCreated);

            //if (InvokeRequired) // A faire apres le check isHandleCreated
            //{
            //    Invoke((Action)(() => ReconnectAfterTimeOutMessage(sender, e)));
            //    return;
            //}
            //Debug.Assert(!InvokeRequired);

            //var resultTemp = AutomaticResponseForConcurrentTimeout;
            //if (resultTemp == DialogResult.None) // Il s'agit du premier message de deconnexion a valider (dans le cas ou plusieur thread etait en corus de travail)
            //{
            //    resultTemp = XtraMessageBox.Show(Authentication.CurrentUser != null ? _owner : null, // Si l'utilisateur n'est pas loggué, la MessageBox forcerait l'affichage de la MainForm, donc on met null
            //                                     "Database took too long to answer <Application> request." + Environment.NewLine + 
            //                                     "Try to execute it again ? (recommenced : yes)", Application.ProductName, MessageBoxButtons.YesNo);

            //    // Si des connexions provenant d'autres thread ont échoué simultanement au cans en train d'etre géré,
            //    // l'utilisateur devrait potentiellement devoir repondre a beaucoup (peut etre des centaines ) de messagebox
            //    // Tous les autres threads sont en attente à cause du BeginInvoke ou bien le lock
            //    // Quand l'utilisateur repond a la premiere question il faut repondre automatiquement la meme reponse aux autres messagebox (validation par block)
            //    AutomaticResponseForConcurrentTimeout = resultTemp;
            //    // Ensuite on met a la suite de la file d'attente (ie : juste apres toutes les autres deconnexion) le reset de la reponse automatique
            //    // Cela permet de reposer la question si quelque temps plus tard une nouvelle déconnexion se produit.
            //    BeginInvoke((Action)(() => AutomaticResponseForConcurrentTimeout = DialogResult.None));
            //}
            //e.Cancel = resultTemp != DialogResult.Yes;
        }
        //private volatile static DialogResult AutomaticResponseForConcurrentTimeout = DialogResult.None;

        #endregion
    }
}
