﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Forms;

using DevExpress.XtraEditors;

using TechnicalTools;
using ApplicationBase.Deployment.Data;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Tools;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.BaseClasses;
using TechnicalTools.UI.DX.Tools;
using TechnicalTools.UI.DX.Forms.Diagnostics;

using ApplicationBase.DAL.Users;
using ApplicationBase.Common;
using ApplicationBase.Business;
using ApplicationBase.Business.Dialoguing;

using Config = ApplicationBase.Business.Config;


namespace ApplicationBase.UI
{
    public interface IDefaultMainFormBehavior
    {
        // Add other methods here if needed
        bool DeployVersion(ApplicationBase.Deployment.Deployer deployer, Control owner, EnvironmentConfig env, Version versionToInstall);
    }

    public abstract class DefaultMainFormBehavior<T> : IDefaultMainFormBehavior
        where T : XtraForm, IHasDockUserControl
    {
        public ApplicationMainFormStarter Starter { get; }

        public ISplashScreen SplashScreen { get; private set; }

        // ReSharper disable once NotAccessedField.Local
                  UserPreferenceErgonomyApplier _UserPreferenceErgonomyApplier; // Handle personnal user preference
                  Messenger                     _Messenger; // Make application "listen" for communication
                  DataChangeEventManager        _DataChangeEventManager; // handle notification of change on any DAL object (on base App only)
        protected DefaultExceptionHandler       _uiSyncOperationExceptionHandler;

        DesignTimeSkinApplicator designTimeSkinApplicator;

        protected DefaultMainFormBehavior(T owner, Action initializeComponent, Func<ISplashScreen> buildSplashScreen = null)
        { 
            Owner = owner;
            _initializeComponent = initializeComponent;
            _buildSplashScreen = buildSplashScreen;
            if (_buildSplashScreen == null)
                _buildSplashScreen = () => new FakeInvisibleSplashScreen();
            if (DesignTimeHelper.IsInDesignMode)
            {
                InitializeComponent(); // Le Designer de Visual Studio a absolument besoin qu'on appelle ca dans le contructeur
                _initializeComponent();
                return;
            }

            _uiSyncOperationExceptionHandler = new DefaultExceptionHandler(Config.Instance.Debug, Owner);
            Owner.Disposed += (_, __) => _uiSyncOperationExceptionHandler.Dispose();
            
            Owner.Icon = BootstrapConfig.Instance.ApplicationIcon;
            Owner.FormClosed += MainForm_FormClosed;

            // Demarrage de l'appli (gère le designer de Visual Studio)
            var mgr = BusinessSingletons.Instance.GetAuthenticationManager();
            if (mgr.CurrentUser == mgr.NoRightLogin)
            {
                Starter = new ApplicationMainFormStarter(Owner, () => SplashScreen = _buildSplashScreen(),
                                                                () => new SplashScreenInitializer(() => Owner.BeginInvoke((Action)OnUserAuthenticated)) { DefaultLogin = Config.Instance.Debug.AutoLoginWith ?? AuthenticationManager.LastLogin })
                { MaximiseMainForm = true, ShowAtStartup = true };
                Starter.Start();
            }
            else
            {
                // We could directly call OnUserAuthenticated 
                // but i prefer the behavior if app to be the same.
                // Especially for begininvoke later use that count on fact the current 
                // execution flow is completed before the call are done
                Starter = new ApplicationMainFormStarter(Owner, () => SplashScreen = new FakeInvisibleSplashScreen(),
                                                                () => new SplashScreenInitializer(() => Owner.BeginInvoke((Action)OnUserAuthenticated)) { DefaultLogin = Config.Instance.Debug.AutoLoginWith ?? AuthenticationManager.LastLogin })
                { MaximiseMainForm = true, ShowAtStartup = true };
                Starter.Start();
            }

            DevExpress.Utils.ToolTipController.DefaultController.AutoPopDelay = 15000;
        }
        static readonly ILogger _log = LogManager.Default.GetLogger(typeof(DefaultMainFormBehavior<T>));
        readonly Action _initializeComponent;
        readonly Func<ISplashScreen> _buildSplashScreen;
        DateTime? _LastAuthenticationDate;
        protected readonly T Owner;

        void InitializeComponent()
        {
            designTimeSkinApplicator = new DesignTimeSkinApplicator
            {
                ContainerControl = Owner
            };
        }
        void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _log.Info(Application.ProductName + " Closed!" + (_LastAuthenticationDate.HasValue ? "(live time: " + (DateTime.Now - _LastAuthenticationDate.Value).ToString() + ")" : ""));
        }

        // Est executé quand l'utilisateur est authentifié
        // Ce code contient également InitializeComponent (plutot que le constructeur) 
        // car le code des composants graphiques crée peut dépendre de l'utilisateur qui est connecté
        // Cela nous permet de gerer la mainform comme n'importe quel autre form ou vue
        protected virtual void OnUserAuthenticated()
        {
            EnhancedXtraForm.MainForm = Owner;
            _LastAuthenticationDate = DateTime.Now;
            _log.Debug("Thread's current culture is: " + System.Threading.Thread.CurrentThread.CurrentCulture.Name);
            _log.Debug("Thread's current UI culture is: " + System.Threading.Thread.CurrentThread.CurrentUICulture.Name);
            _log.Info($"Preparing {Application.ProductName}...");

            // Take no risk currently, if disabled
            if (Config.Instance.DialoguingConfig.EnableDataChangeNotification) 
            {
                _Messenger = BusinessSingletons.Instance.GetMessenger();
                _DataChangeEventManager = BusinessSingletons.Instance.GetDataChangeEventManager();
                _DataChangeEventManager.Enable = Config.Instance.DialoguingConfig.EnableDataChangeNotification;
            }

            // Creation des control graphiques
            InitializeComponent();
            _initializeComponent();

            BusinessSingletons.Instance.GetAuthenticationManager().CurrentUserChanged += RefreshAccess;
            Owner.Disposed += (_, __) => BusinessSingletons.Instance.GetAuthenticationManager().CurrentUserChanged -= RefreshAccess;
            RefreshAccess();

            BusinessSingletons.Instance.GetAuthenticationManager().CurrentUserChanged += _RefreshUserInfo;
            Owner.Disposed += (_, __) => BusinessSingletons.Instance.GetAuthenticationManager().CurrentUserChanged -= _RefreshUserInfo;
            _RefreshUserInfo();

            ToolTipOnDisableControl.EnableFeatureOn(Owner);

            _UserPreferenceErgonomyApplier = CreateUserPreferenceErgonomyApplier();

            if (Config.Instance.View is ViewConfig vConfig)
                ApplyConfig(vConfig);

            void onUserChangedSkin(string newSkinName)
            {
                BusinessSingletons.Instance.GetConfigOfUser().Application_DefaultSkin = newSkinName;
            }
            SkinApplicatorHelper.OnSkinChanged += onUserChangedSkin;
            Owner.Disposed += (_, __) => SkinApplicatorHelper.OnSkinChanged -= onUserChangedSkin;

            var notifier = BusinessSingletons.Instance.GetReleaseNotifier();
            Task.Run(() => notifier.GetNewReleaseNotesForUser(BusinessSingletons.Instance.GetConfigOfUser()))
                .ContinueWithUIWorkOnSuccess(releaseNotes =>
                {
                    if (releaseNotes.Count > 0)
                        UIControler.Instance.ShowObject(releaseNotes);
                });

            AutoClosingAfterLongIdleTime_Install();

            // Invoke to do after the potential overriden current method
            Owner.BeginInvoke((Action)(() =>
            {
                _log.Info(Application.ProductName + " Ready!");

                if (!DebugTools.IsForDevelopper)
                    NewVersionChecker_Init();
            }));
        }

        protected virtual UserPreferenceErgonomyApplier CreateUserPreferenceErgonomyApplier()
        {
            return new UserPreferenceErgonomyApplier(BusinessSingletons.Instance.GetConfigOfUser(), Owner);
        }

        public void RefreshAccess()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            if (Owner.InvokeRequired)
                Owner.BeginInvoke((Action)RefreshEnabilitiesAndVisibilities);
            else
                RefreshEnabilitiesAndVisibilities();
        }
        public abstract void RefreshEnabilitiesAndVisibilities();

        void ApplyConfig(ViewConfig config)
        {
            // a noel : Xmas 2008 Blue
            // a la saint valentin : Valentine
            var skinName = config.DefaultSkin ?? string.Empty;
            if (skinName.Contains("#"))
                skinName = skinName.Remove(skinName.IndexOf("#", StringComparison.Ordinal));

            if (!string.IsNullOrWhiteSpace(skinName))
                BusinessSingletons.Instance.GetConfigOfUser().WithoutSaving(() => SkinApplicatorHelper.SetCurrentSkin(skinName));

            BusyForm.AllowPreventBusyFormToBeShownAsDialog = config.PreventBusyFormToBeShownAsDialog && DebugTools.IsForDevelopper;
        }

        void _RefreshUserInfo()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            if (Owner.InvokeRequired)
            {
                Owner.BeginInvoke((Action)_RefreshUserInfo);
                return;
            }
            RefreshUserInfo(BusinessSingletons.Instance.GetAuthenticationManager().CurrentUser);
        }
        protected abstract void RefreshUserInfo(Login login);

        #region Main Menu

        public virtual void ShowDialoguer()
        {
            UIControler.Instance.ShowObject(BusinessSingletons.Instance.GetDialoguer());
        }

        public virtual void ShowReleaseNoteHistory()
        {
            UIControler.Instance.ShowObject(BusinessSingletons.Instance.GetReleaseNotifier());
        }

        #endregion Main Menu

        #region Developpers menu

        public virtual void ShowUsersProfilesRights()
        {
            UIControler.Instance.ShowObject(BusinessSingletons.Instance.GetAuthenticationManager());
        }
        public virtual void ShowEnvironmentSettings()
        {
            UIControler.Instance.ShowObject(EnvironmentSettingsView.Set.Instance);
        }
        public virtual void ShowUserLogs()
        {
            UIControler.Instance.ShowObject(BusinessSingletons.Instance.GetUserLogSource());
        }
        public virtual void ShowAdminLogs()
        {
            UIControler.Instance.ShowObject(BusinessSingletons.Instance.GetAdminLogSource());
        }
        public virtual void ShowTaskExecutionTimes()
        {
            var manager = BusinessSingletons.Instance.GetAuthenticationManager();
            if (manager.CurrentUser.IsAdmin)
                UIControler.Instance.ShowObject(new Business.Logs.TaskRunInfoSet(BusinessSingletons.Instance.GetAdminLogSource()));
            else
                UIControler.Instance.ShowObject(new Business.Logs.TaskRunInfoSet(BusinessSingletons.Instance.GetUserLogSource()));
        }
        public virtual void ShowDeploymentTool()
        {
            UIControler.Instance.ShowObject(BusinessSingletons.Instance.GetDeployer());
        }

        #endregion

        #region Status Bar

        public virtual void ShowDbTrace()
        {
            FrmDebugDatabaseTrace.Show();
        }

        public virtual void ShowInstancesInMemory()
        {
            FrmObjectTracking.Show();
        }

        #endregion

        #region Auto Update 

        Timer tmrCheckUpdate;

        public virtual void NewVersionChecker_Init()
        {
            if (Config.Instance.Environment.Domain == eEnvironment.LocalNoDB)
                return;
            if (!DebugTools.IsForDevelopper) // disable this because even when developper want to test, it is annoying
                CheckAndInstallNewVersion();

            tmrCheckUpdate_Tick(null, null);
            tmrCheckUpdate = new Timer
            {
                Interval = 1000 * int.Parse(((ViewConfig)Config.Instance.View).AutoUpdateCheckInterval ?? "60"),
                Enabled = true
            };
            tmrCheckUpdate.Tick += tmrCheckUpdate_Tick;
        }

        public virtual void ShowEnvironmentChooser()
        {
            var view = new EnvironmentChooserView(BusinessSingletons.Instance.GetDeployer(), this);
            var frm = EnhancedXtraForm.CreateWithInner(view, Owner);
            frm.Text = "Changing environment...";
            frm.ShowDialog(Owner);
        }

        void tmrCheckUpdate_Tick(object _, EventArgs __)
        {
            if (_taskCheckUpdate == null) // Prevent multiple check at same time
            {
                _taskCheckUpdate = new BackgroundWorker();
                _taskCheckUpdate.DoWork += (___, e) => e.Result = BusinessSingletons.Instance.GetDeployer().AReleaseMustBeInstalled(Config.Instance.Environment);
                _taskCheckUpdate.RunWorkerCompleted += (___, e) => 
                {
                    _taskCheckUpdate.Dispose();
                    _taskCheckUpdate = null; // whatever the result state we try again
                    if (e.Cancelled)
                        return;
                    if (e.Error != null)
                    {
                        e.Error.EnrichDiagnosticWith($"Cannot detect new {Application.ProductName} version", eExceptionEnrichmentType.UserUnderstandable);
                        ExceptionManager.Instance.NotifyException(e.Error, UnexpectedExceptionManager.eExceptionKind.Ignored);
                        return;
                    }
                    var version = (Version)e.Result;
                    if (version != null)
                    {
                        //barblbNewRelease.Visibility = BarItemVisibility.Always;
                        tmrCheckUpdate.Interval = 2000;
                        //barblbNewRelease.Caption = (string)barblbNewRelease.Tag;
                        tmrCheckUpdate.Tick -= tmrCheckUpdate_Tick;
                        tmrCheckUpdate.Tick += tmrCheckUpdate_Blink;
                        tmrCheckUpdate_Blink(null, null);
                    }
                    else
                    {
                        //barblbNewRelease.Visibility = BarItemVisibility.Never;
                    }
                };
                _taskCheckUpdate.RunWorkerAsync();
            }
        }
        volatile BackgroundWorker _taskCheckUpdate;

        void tmrCheckUpdate_Blink(object sender, EventArgs e)
        {
            _toggleColor = !_toggleColor;
            BlinkBecauseNewVersion(_toggleColor);
        }
        bool _toggleColor;
        public abstract void BlinkBecauseNewVersion(bool toggleColor);

        public virtual bool CheckAndInstallNewVersion(ApplicationBase.Deployment.Deployer deployer = null)
        {
            deployer = deployer ?? BusinessSingletons.Instance.GetDeployer();
            var releaseVersion = deployer.AReleaseMustBeInstalled(Config.Instance.Environment);
            if (releaseVersion != null &&
                DeployVersion(deployer, Owner, Config.Instance.Environment, releaseVersion))
            {
                var currentVersion = Deployer.GetCurrentVersion();
                if (currentVersion > releaseVersion)
                    // TODO : Impossible d'utiliser BeginInvoke, ca bloque le mecanisme de demarrage 
                    //        (La raison technique est le "_mainForm.BeginInvoke" qui n'est jamais honoré dans ApplicationMainFormStarter.OnSplashScreenFinishInitialization)
                    //  BeginInvoke((Action)(() =>
                    XtraMessageBox.Show(Owner, $"The current version of {BootstrapConfig.Instance.ApplicationName} is not yet allowed to connect to this environment!" +
                                                (DebugTools.IsForDevelopper ? Environment.NewLine + "(if you are a developper you can ignore this message)" : ""));
                //));

                Owner.BeginInvoke((Action)(() => Owner.Close())); // Postpone because we are maybe in a MainForm's Load event
                return true;
            }
            return false;
        }

        public virtual bool DeployVersion(ApplicationBase.Deployment.Deployer deployer, Control owner, EnvironmentConfig env, Version versionToInstall)
        {
            return DeployerView.DeployVersion(deployer, Owner, env, versionToInstall);
        }

        #endregion Auto Update 

        #region AutoClosing after long idle time

        void AutoClosingAfterLongIdleTime_Install()
        {
            // Autoclosing of Application for security measure / and data safety (It know which version of <Application> is running)
            IdleDetection.Instance.DurationConsideredAsIdle = Config.Instance.Environment.Domain == eEnvironment.LocalNoDB ? TimeSpan.MaxValue : new TimeSpan(4, 0, 0);
            if (!BusinessSingletons.Instance.GetAuthenticationManager().CurrentUser.IsAdmin)
                IdleDetection.Instance.OnUserIdle += idleDurationDetected =>
                {
                    if (!Owner.IsHandleCreated)
                        Environment.Exit(1);
                    Owner.BeginInvoke((Action)(() =>
                    {
                        IdleDetection.Instance.CheckInterval = TimeSpan.Zero; // Disable
                        _log.Info(Application.ProductName + $" is shutting down automatically because user have been idle for {idleDurationDetected}.");
                        // Warn the user about the fact <Application> is shutting down on itself at night.
                        // Later we could remove this messagebox;
                        // this is to force them to launch <Application> again and check that the current <Application>
                        // version is still allowed to run on the environment
                        XtraMessageBox.Show(Owner, $"You have been idle for a long time {Application.ProductName} will close now");
                        Owner.Close();
                    }));
                };
        }

        #endregion

    }
}