﻿using System;

using TechnicalTools.UI.DX;
using TechnicalTools.UI.Tools;


namespace ApplicationBase.UI
{
    public class ViewFinder : ControlEditorFinder<XtraUserControlBase, object>
    {
        public static readonly ViewFinder Instance = new ViewFinder();
        private ViewFinder() { }

        public override void RegisterEditor(XtraUserControlBase editing_editor, object edited_object)
        {
            base.RegisterEditor(editing_editor, edited_object);
            // Permet de ne pas avoir a appeler UnregisterEditor manuellement à chaque fois
            // L'evenement "Disposed" n'est a priori pas déclenché pour les Form MDI (CF http://stackoverflow.com/questions/3097364/c-sharp-form-close-vs-form-dispose),
            // on utilise donc l'event Closed.
            editing_editor.Disposed += (_, __) => UnregisterEditor(editing_editor, edited_object);
        }

        public override void RegisterEditor(XtraUserControlBase editing_editor)
        {
            base.RegisterEditor(editing_editor);
            // Idem
            editing_editor.Disposed += (_, __) => UnregisterEditor(editing_editor);
        }

        public TEditor FindEditorFor<TEditor>(object edited_object)
            where TEditor : XtraUserControlBase
        {
            return (TEditor)base.FindEditorFor(typeof(TEditor), edited_object);
        }

    }

}
