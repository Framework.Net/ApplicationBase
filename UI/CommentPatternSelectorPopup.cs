﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

using TechnicalTools;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;

using ApplicationBase.Business;


using ICommentPattern = ApplicationBase.DAL.HasPropertiesOf.Tools.CommentPattern;


namespace ApplicationBase.UI
{
    public partial class CommentPatternSelectorPopup : EnhancedXtraForm
    {
        readonly Dictionary<string, string> _entryValues = new Dictionary<string, string>();

        public event EventHandler UserSelect;

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string FinalComment { get; private set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ReadOnly { get; set; }

        public ICommentPattern SelectedPattern { get; private set; }

        readonly CommentPatternSubSet _set;

        [Obsolete("For designer only")]
        public CommentPatternSelectorPopup() : this(null) { }

        public CommentPatternSelectorPopup(CommentPatternSubSet set)
        {
            _set = set;

            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;

            BusinessSingletons.Instance.GetAuthenticationManager().CurrentUserChanged += DefineEnabilitiesAndVisibilities;
            Disposed += (__, ___) => BusinessSingletons.Instance.GetAuthenticationManager().CurrentUserChanged -= DefineEnabilitiesAndVisibilities;
            DefineEnabilitiesAndVisibilities();
        }

        public void ShowAsPopup(IWin32Window owner)
        {
            // This is way is better than using Deactivate event
            // because it allows "this" to open other modal windows (like column chooser for example)
            _frms = Application.OpenForms.OfType<Form>().ToList();
            foreach (var frm in _frms)
                frm.Activated += Frm_Activated;
            Show(owner);
        }
        List<Form> _frms;
        private void Frm_Activated(object sender, EventArgs e)
        {
            foreach (var frm in _frms)
                frm.Activated -= Frm_Activated;
            btnCancel.PerformClick();
            _frms.Clear();
        }

        void CommentPatternSelectorPopup_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            gcCommentPatterns.DataSource = _set.EditingSet;
            
            colKeyword.FieldName = nameof(ICommentPattern.KeyWord);
            colComment.FieldName = nameof(ICommentPattern.Pattern);
            colTreatedByAutoProcess.FieldName = nameof(ICommentPattern.PreventAutomaticTreatment);
            if (!_set.HasRelatedAutomaticAction)
                gvCommentPatterns.Columns.Remove(colTreatedByAutoProcess);
            RefreshEntries();
        }

        void DefineEnabilitiesAndVisibilities()
        {
            btnOk.Enabled = !ReadOnly && SelectedPattern != null;
            if (ReadOnly)
                btnOk.ToolTip = "You are not allowed to change comment!";
        }

        void CommentPatternSelectorPopup_Shown(object sender, EventArgs e)
        {
            gvCommentPatterns.FocusedRowHandle = GridControl.AutoFilterRowHandle;
        }

        void gvCommentPatterns_ColumnFilterChanged(object sender, EventArgs e)
        {
            if (gvCommentPatterns.RowCount == 1) // Count the number of _visible_ row  => Indique que l'utilisateur a bien filtrer
                BeginInvoke((Action)(() => gvCommentPatterns.FocusedRowHandle = 0));
        }

        void gvCommentPatterns_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (gvCommentPatterns.FocusedRowHandle == GridControl.AutoFilterRowHandle)
                return;
            // Fix the last selected pattern so that when the form is closed, SelectedPattern is still good.
            SelectedPattern = gvCommentPatterns.FocusedRowHandle >= 0
                ? (ICommentPattern)gvCommentPatterns.GetRow(gvCommentPatterns.FocusedRowHandle)
                : null;
            RefreshEntries();
            if (grpEntries.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Always)
            {
                //layoutControl1.Focus();
                Entry1LayoutItem.Control.Focus();
                //BeginInvoke((Action)(() => layoutControlItem1.Control.Focus()));
            }
            DefineEnabilitiesAndVisibilities();
        }

        void RefreshEntries()
        {
            if (gvCommentPatterns.FocusedRowHandle == GridControl.AutoFilterRowHandle)
            {
                grpEntries.Expanded = false;
            }
            else
            {
                var layoutItems = new[] {Entry1LayoutItem, Entry2LayoutItem, Entry3LayoutItem, Entry4LayoutItem, Entry5LayoutItem};
                foreach (var li in layoutItems)
                    li.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControl1.BeginUpdate();
                if (SelectedPattern != null)
                {
                    var entries = SelectedPattern.GetEntryNames().Take(5).ToList(); // ne prend que les 5 premier car l'interface ne permet pas dans gerer plus (il suffira d'en rajotuer si besoin)
                    if (entries.Count == 0)
                        grpEntries.Expanded = false;
                    else
                    {
                        for (int i = 0; i < entries.Count; ++i)
                        {
                            var entry = entries[i];
                            layoutItems[i].Tag = entry;
                            layoutItems[i].Text = entry.Name;
                            layoutItems[i].OptionsToolTip.ToolTip = string.IsNullOrWhiteSpace(entry.Comment) ? null : entry.Comment;
                            layoutItems[i].Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            ((TextEdit)layoutItems[i].Control).Text = _entryValues.TryGetValueClass(entry.Name) ?? "";
                        }

                        grpEntries.Expanded = true;
                    }
                }

                layoutControl1.EndUpdate();
            }
        }

        void anyTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            var te = (TextEdit)sender;
            var li = layoutControl1.GetItemByControl(te);
            var entry = (PatternEntry)li.Tag;
            _entryValues[entry.Name] = te.Text.Trim();
        }

        void btnOk_Click(object sender, EventArgs e)
        {
            if (SelectedPattern == null)
                return; // User must press escape
            FinalComment = SelectedPattern.Pattern;
            foreach (var entry in SelectedPattern.GetEntryNames().AsEnumerable().Reverse())
                FinalComment = FinalComment.Substring(0, entry.StartIndex)
                               + _entryValues.TryGetValueClass(entry.Name)
                               + FinalComment.Substring(entry.EndIndex + 1);
            Close();
            UserSelect?.Invoke(this, e);
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }



        private void gvCommentPatterns_MouseDown(object sender, MouseEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
                return;

            GridView view = (GridView)sender;
            GridHitInfo hi = view.CalcHitInfo(e.Location);
            if (!hi.InRowCell)
                return;

            if (hi.Column.RealColumnEdit.GetType() != typeof(RepositoryItemButtonEdit))
                return;

            view.FocusedRowHandle = hi.RowHandle;
            view.FocusedColumn = hi.Column;
            view.ShowEditor();
            //force button click 
            ButtonEdit edit = (ButtonEdit)view.ActiveEditor;
            Point p = view.GridControl.PointToScreen(e.Location);
            p = edit.PointToClient(p);
            EditHitInfo ehi = ((ButtonEditViewInfo)edit.GetViewInfo()).CalcHitInfo(p);
            if (ehi.HitTest == EditHitTest.Button)
            {
                edit.PerformClick(ehi.HitObject as EditorButton);
                ((DevExpress.Utils.DXMouseEventArgs)e).Handled = true;
            }
        }

        void btnEdit_Click(object sender, EventArgs e)
        {
            UIControler.Instance.ShowObject(_set);
        }
    }
}

