﻿namespace ApplicationBase.UI.Dialoguing
{
    partial class AppDialoguerView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lytCtlMain = new DevExpress.XtraLayout.LayoutControl();
            this.btnDiscoverConnectedClients = new DevExpress.XtraEditors.SimpleButton();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.gcDialogues = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvDialogues = new TechnicalTools.UI.DX.EnhancedGridView();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.lytCtlMain)).BeginInit();
            this.lytCtlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDialogues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDialogues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // lytCtlMain
            // 
            this.lytCtlMain.Controls.Add(this.btnDiscoverConnectedClients);
            this.lytCtlMain.Controls.Add(this.btnClear);
            this.lytCtlMain.Controls.Add(this.gcDialogues);
            this.lytCtlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lytCtlMain.Location = new System.Drawing.Point(0, 0);
            this.lytCtlMain.Name = "lytCtlMain";
            this.lytCtlMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(980, 262, 250, 350);
            this.lytCtlMain.Root = this.layoutControlGroup1;
            this.lytCtlMain.Size = new System.Drawing.Size(862, 491);
            this.lytCtlMain.TabIndex = 0;
            this.lytCtlMain.Text = "layoutControl1";
            // 
            // btnDiscoverConnectedClients
            // 
            this.btnDiscoverConnectedClients.Location = new System.Drawing.Point(381, 12);
            this.btnDiscoverConnectedClients.Name = "btnDiscoverConnectedClients";
            this.btnDiscoverConnectedClients.Size = new System.Drawing.Size(180, 22);
            this.btnDiscoverConnectedClients.StyleController = this.lytCtlMain;
            this.btnDiscoverConnectedClients.TabIndex = 6;
            this.btnDiscoverConnectedClients.Text = "Discover connected clients";
            this.btnDiscoverConnectedClients.Click += new System.EventHandler(this.btnDiscoverConnectedClients_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(751, 12);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(99, 22);
            this.btnClear.StyleController = this.lytCtlMain;
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // gcDialogues
            // 
            this.gcDialogues.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcDialogues.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcDialogues.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcDialogues.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcDialogues.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcDialogues.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcDialogues.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcDialogues.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcDialogues.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcDialogues.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcDialogues.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcDialogues.EmbeddedNavigator.TextStringFormat = "Dialogue {0} of {1} (visible) of {2} (total)";
            this.gcDialogues.Location = new System.Drawing.Point(12, 38);
            this.gcDialogues.MainView = this.gvDialogues;
            this.gcDialogues.Name = "gcDialogues";
            this.gcDialogues.Size = new System.Drawing.Size(838, 441);
            this.gcDialogues.TabIndex = 4;
            this.gcDialogues.UseEmbeddedNavigator = true;
            this.gcDialogues.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDialogues});
            // 
            // gvDialogues
            // 
            this.gvDialogues.GridControl = this.gcDialogues;
            this.gvDialogues.Name = "gvDialogues";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(862, 491);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gcDialogues;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(842, 445);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(369, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnClear;
            this.layoutControlItem2.Location = new System.Drawing.Point(739, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(103, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(103, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(103, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnDiscoverConnectedClients;
            this.layoutControlItem3.Location = new System.Drawing.Point(369, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(184, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(184, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(184, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(553, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(186, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // AppDialoguerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lytCtlMain);
            this.Name = "AppDialoguerView";
            this.Size = new System.Drawing.Size(862, 491);
            this.Load += new System.EventHandler(this.AppDialoguerView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lytCtlMain)).EndInit();
            this.lytCtlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDialogues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDialogues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraLayout.LayoutControl lytCtlMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private TechnicalTools.UI.DX.EnhancedGridControl gcDialogues;
        private TechnicalTools.UI.DX.EnhancedGridView gvDialogues;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton btnDiscoverConnectedClients;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    }
}
