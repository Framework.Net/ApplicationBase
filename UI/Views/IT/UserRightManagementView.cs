﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Data.Filtering;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Repository;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Forms;
using TechnicalTools.UI.DX.Helpers;

using ApplicationBase.DAL;
using ApplicationBase.DAL.Users;
using ApplicationBase.Common;
using ApplicationBase.Business;


namespace ApplicationBase.UI
{
    public partial class UserRightManagementView : ApplicationBaseView
    {
        AuthenticationManager Manager { get { return (AuthenticationManager)BusinessObject; } }

        readonly BindingList<UserRights> _UserRights = new BindingList<UserRights>();
        RepositoryItemRichTextEdit _repoHtmlEdit;

        // This class present data in two way : 
        // - Users : "direct right" means right associated to the user, and we can assign one or more Profile to user)
        // - Profiles : "direct right" means right associated to the Profile, and we can make the Profile inherit from zero or one Profile
        class UserRights
        {
                     AuthenticationManager                                     Manager { get;         set; }
            internal Dictionary<Feature, AuthenticationManager.DetailedAccess> Rights  { get; private set; }
                     Login                                                     Login   { get;         set; } // Login and person are not null
                     Person                                                    Person  { get;         set; }
                     Profile                                                   Profile { get;         set; } // or else Profile is not null

            #region When Login & _Person are filled (and not _Profile)
            internal IEnumerable<Profile> AssignedProfiles  { get { return Rights.SelectMany(kvp => kvp.Value.AssignedAccessByProfile.Keys).Distinct().Where(p => !ReferenceEquals(p, Profile.DirectRight)); } }
            // AssignedProfiles + Inherited Profiles
            internal IEnumerable<Profile> InheritedProfiles { get { return Rights.SelectMany(kvp => kvp.Value.InheritedAccessByProfile.Keys).Distinct().Where(p => !ReferenceEquals(p, Profile.DirectRight)); } }
            public DateTime  CreationDate  { get { return Login?.CreationDate ?? DateTime.MinValue; } }
            public string    LoginAsStr    { get { return Login?.Identifiant; } }
            public DateTime? LastLoginDate { get { return Login?.LastLoginDate; } }
            public string    Firstname     { get { return Person?.Firstname; } }
            public string    Lastname      { get { return Person?.Lastname; } }
            public bool      IsEnabled
            {
                get { return Login?.IsEnabled ?? false; }
                set
                {
                    if (Login.IsEnabled == value)
                        return;
                    Login.IsEnabled = value;
                    try
                    {
                        DB.Dao_Base.UpdateToDatabase(Login);
                    }
                    catch
                    {
                        Login.IsEnabled = !value;
                        throw;
                    }
                }
            }
            public bool      IsAdmin
            {
                get { return Login?.IsAdmin ?? false; }
                set
                {
                    if (Login.IsAdmin == value)
                        return;
                    Login.IsAdmin = value;
                    try
                    {
                        DB.Dao_Base.UpdateToDatabase(Login);
                    }
                    catch
                    {
                        Login.IsAdmin = !value;
                        throw;
                    }
                }
            }
            public string    AssignedProfilesStr { get { return AssignedProfiles.Select(p => p.Name).Join(); } }
            #endregion



            #region When _Profile is filled (and not Login and _Person)
            public string ProfileName { get { return Profile?.Name; } }
            public string InheritedProfilesStr { get { return InheritedProfiles.Select(p => p.Name).Join(); } }

            #endregion

            public static List<UserRights> LoadDataSource(AuthenticationManager manager, bool displayProfiles)
            {
                var rights = manager.GetAllRights(displayProfiles, true);
                var persons = DB.Dao_Base.LoadCollection<Person>().ToDictionary(p => p.Id);
                return rights.Keys.Select(owner => new UserRights()
                                            {
                                                Manager = manager,
                                                Login = owner as Login,
                                                Person = owner is Login ? persons[((Login)owner).Person_Id] : null,
                                                Profile = owner as Profile,
                                                Rights = rights[owner]
                                            })
                                  .OrderBy(ur => (ur.Person?.Firstname + ur.Person?.Lastname).IfBlankUse(ur.ProfileName))
                                  .ToList();
            }

            public void AssignProfile(Profile profile)
            {
                if (Login != null)
                {
                    var links = DB.Dao_Base.LoadCollection<Login_Profile>()
                                          .Where(lp => lp.Login_Id == Login.Person_Id
                                                    && lp.Profile_Id == profile.Id)
                                          .ToList();
                    if (links.Any())
                        throw new UserUnderstandableException("User has already this Profile!", null);
                    var link = new Login_Profile()
                    {
                        Login_Id = Login.Person_Id,
                        Profile_Id = profile.Id,
                        ChangedDate = DateTime.Now,
                        ChangedByLoginId = Manager.CurrentUser.Person_Id,
                    };
                    DB.Dao_Base.CreateInDatabase(link);
                }
                else
                {
                    if (profile != null)
                    {
                        var ids = new HashSet<long>();
                        long? id = profile.Id;

                        while (id.HasValue)
                        {
                            var p = DB.Dao_Base.GetObjectWithId<Profile>(id.Value);
                            if (ids.Contains(p.Id))
                                throw new UserUnderstandableException("Assigning this Profile as Parent Profile would create a cycle!", null);
                            id = p.InheritingProfile_Id;
                        }
                    }
                    Profile.InheritingProfile_Id = profile?.Id;
                    DB.Dao_Base.UpdateToDatabase(Profile);
                }
                Invalidate();
            }
            public void RemoveFromProfile(Profile profile)
            {
                if (Login != null)
                {
                    var links = DB.Dao_Base.LoadCollection<Login_Profile>()
                                      .Where(lp => lp.Login_Id == Login.Person_Id
                                                && lp.Profile_Id == profile.Id)
                                      .ToList();
                    foreach (var link in links)
                        DB.Dao_Base.DeleteInDatabase(link);
                }
                else
                {
                    profile.InheritingProfile_Id = null;
                    DB.Dao_Base.UpdateToDatabase(profile);
                }
                Invalidate();
            }
            public void AddDirectFeature(Feature feature, eFeatureAccess access)
            {
                if (Login != null)
                {
                    var links = DB.Dao_Base.LoadCollection<Login_Feature>()
                      .Where(lf => lf.Login_Id == Login.Person_Id
                                && lf.Feature_Id == feature.Id)
                      .ToList();
                    foreach (var link in links.Skip(1))
                        DB.Dao_Base.CreateInDatabase(link);
                    if (links.Any())
                    {
                        links.First().Access = access;
                        links.First().ChangedDate = DateTime.Now;
                        links.First().ChangedByLoginId = Manager.CurrentUser.Person_Id;
                        DB.Dao_Base.UpdateToDatabase(links.First());
                    }
                    else
                    {
                        DB.Dao_Base.CreateInDatabase(new Login_Feature()
                        {
                            Login_Id = Login.Person_Id,
                            Feature_Id = feature.Id,
                            ChangedDate = DateTime.Now,
                            ChangedByLoginId = Manager.CurrentUser.Person_Id,
                            Access = access
                        });
                    }
                }
                else
                {
                    var links = DB.Dao_Base.LoadCollection<Profile_Feature>()
                                          .Where(pf => pf.Profile_Id == Profile.Id
                                                    && pf.Feature_Id == feature.Id)
                                          .ToList();
                    foreach (var link in links.Skip(1))
                        DB.Dao_Base.CreateInDatabase(link);
                    if (links.Any())
                    {
                        links.First().Access = access;
                        links.First().ChangedDate = DateTime.Now;
                        links.First().ChangedByLoginId = Manager.CurrentUser.Person_Id;
                        DB.Dao_Base.UpdateToDatabase(links.First());
                    }
                    else
                    {
                        DB.Dao_Base.CreateInDatabase(new Profile_Feature()
                        {
                            Profile_Id = Profile.Id,
                            Feature_Id = feature.Id,
                            ChangedDate = DateTime.Now,
                            ChangedByLoginId = Manager.CurrentUser.Person_Id,
                            Access = access
                        });
                    }
                }
                Invalidate();
            }
            public void RemoveDirectFeature(Feature feature)
            {
                if (Login != null)
                {
                    var links = DB.Dao_Base.LoadCollection<Login_Feature>()
                                           .Where(lp => lp.Login_Id == Login.Person_Id
                                                     && lp.Feature_Id == feature.Id)
                                           .ToList();
                    foreach (var link in links)
                        DB.Dao_Base.DeleteInDatabase(link);
                }
                else
                {
                    var links = DB.Dao_Base.LoadCollection<Profile_Feature>()
                                           .Where(pf => pf.Profile_Id == Profile.Id
                                                     && pf.Feature_Id == feature.Id)
                                           .ToList();
                    foreach (var link in links)
                        DB.Dao_Base.DeleteInDatabase(link);
                }
                Invalidate();
            }
            
            public void Invalidate()
            {
                var allRight = Manager.GetAllRights(Profile != null, true);
                Rights = allRight[Profile as IFeatureOwner ?? Login];
            }
        }

        [Obsolete("For designer only", true)]
        protected UserRightManagementView()
            : this(null)
        {
        }

        public UserRightManagementView(AuthenticationManager manager)
            : base(manager, Feature.CanImpersonateLogins)
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            Text = "User Rights Management";
        }

        private void UserRightManagementView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            gcUserRights.DataSource = _UserRights;
            gvUserRights.Columns[nameof(UserRights.LoginAsStr)].Caption = "Login";
            gvUserRights.OptionsView.ShowAutoFilterRow = true;
            gvUserRights.OptionsView.ColumnAutoWidth = false;
            _repoHtmlEdit = GridViewHelper.CreateHtmlRepositoryEdit(gvUserRights);

            RefreshDataSource();
        }

        private void chkDisplayProfiles_CheckedChanged(object sender, EventArgs e)
        {
            RefreshDataSource();
            gvUserRights.ActiveFilterCriteria = chkDisplayProfiles.Checked 
                                              ? null
                                              : DisplayOnlyEnabledUser;
        }
        static readonly BinaryOperator DisplayOnlyEnabledUser = new BinaryOperator(nameof(UserRights.IsEnabled), true, BinaryOperatorType.Equal);

        void RefreshDataSource()
        {
            BeginInvoke((Action)(() => // So the busy form is center
            this.ShowBusyWhileDoing("Loading user rights...", pr => UserRights.LoadDataSource(Manager, chkDisplayProfiles.Checked))
                .ContinueWithUIWorkOnSuccess(DisplayDataSoure)));
        }

       

        void DisplayDataSoure(List<UserRights> lst)
        {
            _UserRights.Clear();
            _UserRights.AddRange(lst);

            gvUserRights.BeginUpdate();
            
            gvUserRights.Columns[nameof(UserRights.ProfileName)].Visible = chkDisplayProfiles.Checked;
            gvUserRights.Columns[nameof(UserRights.InheritedProfilesStr)].Visible = chkDisplayProfiles.Checked;
            gvUserRights.Columns[nameof(UserRights.InheritedProfilesStr)].Caption = "Inherited Profiles";

            gvUserRights.Columns[nameof(UserRights.CreationDate)].Visible = false; // !chkDisplayProfiles.Checked;
            gvUserRights.Columns[nameof(UserRights.LoginAsStr)].Visible = false; // !chkDisplayProfiles.Checked;
            gvUserRights.Columns[nameof(UserRights.LastLoginDate)].Visible = false; // !chkDisplayProfiles.Checked;

            gvUserRights.Columns[nameof(UserRights.Firstname)].Visible = !chkDisplayProfiles.Checked;
            gvUserRights.Columns[nameof(UserRights.Lastname)].Visible = !chkDisplayProfiles.Checked;
            gvUserRights.Columns[nameof(UserRights.IsEnabled)].Visible = !chkDisplayProfiles.Checked;
            gvUserRights.Columns[nameof(UserRights.IsAdmin)].Visible = !chkDisplayProfiles.Checked;
            gvUserRights.Columns[nameof(UserRights.AssignedProfilesStr)].Visible = !chkDisplayProfiles.Checked;
            gvUserRights.Columns[nameof(UserRights.AssignedProfilesStr)].Caption = "Assigned Profiles";


            if (_firsttime)
            {
                var features = Feature.All.ToDictionary(f => f.Id);

                foreach (var feature in features.OrderBy(kvp => kvp.Key > 0 ? 0 : 1) // negative id => technical feature
                                                .ThenBy(kvp => Math.Abs(kvp.Key))) // then by pseudo chronological order
                {
                    var col = gvUserRights.Columns.AddVisible(feature.Value.Name, feature.Value.Caption.Replace("Data Access on ", ""));
                    col.UnboundType = DevExpress.Data.UnboundColumnType.String;
                    col.Tag = feature.Value;
                    col.OptionsColumn.ReadOnly = true;
                    col.ColumnEdit = _repoHtmlEdit;
                }
                gvUserRights.ActiveFilterCriteria = DisplayOnlyEnabledUser;
            }
            else
                foreach (GridColumn col in gvUserRights.Columns)
                    if (col.Tag != null)
                        col.VisibleIndex = gvUserRights.Columns.Count;
            BeginInvoke((Action)(gvUserRights.BestFitColumns));
            gvUserRights.EndUpdate();
            _firsttime = false;
        }
        bool _firsttime = true;

        private void gvUserRights_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                var feature = (Feature)e.Column.Tag;
                var ur = _UserRights[e.ListSourceRowIndex];
                var userFeature = _accessesDetails.GetValueOrCreateDefault(ur.LoginAsStr ?? ur.ProfileName);
                var v = userFeature.TryGetValueClass(feature);
                if (v == null || v.Item2 + new TimeSpan(0, 0, 10) < DateTime.UtcNow) // older than 10s
                {
                    var accessesDetails = ur.Rights.TryGetValueClass(feature);
                    userFeature[feature] = Tuple.Create(accessesDetails?.ToString(), DateTime.UtcNow);
                }
                e.Value = userFeature[feature].Item1;
            }
        }
        Dictionary<string, Dictionary<Feature, Tuple<string, DateTime>>> _accessesDetails = new Dictionary<string, Dictionary<Feature, Tuple<string, DateTime>>>();

        private void gvUserRights_CalcRowHeight(object sender, DevExpress.XtraGrid.Views.Grid.RowHeightEventArgs e)
        {
            //e.RowHeight = 70;
        }

        private void btnCreateNewUser_Click(object sender, EventArgs e)
        {
            string firstname = "";
            if (DialogResult.OK != InputBox.ShowDialog(this, ref firstname, "Enter firstname of new person", validateOnEnter:true))
                return;
            string lastname = "";
            if (DialogResult.OK != InputBox.ShowDialog(this, ref lastname, "Enter lastname of new person", validateOnEnter: true))
                return;
            string loginName = "";
            if (DialogResult.OK != InputBox.ShowDialog(this, ref loginName, "Enter login of new user", validateOnEnter: true))
                return;
            string password = "";
            if (DialogResult.OK != InputBox.ShowDialog(this, ref password, "Enter a temporary password", validateOnEnter: true,
                                                       remarks: "The user will be able to change it later." + Environment.NewLine 
                                                              + "An empty password is possible if the system is able to handle password in another way"))
                return;
            this.ShowBusyWhileDoing("Creating new user", pr => Manager.CreateUser(firstname, lastname, loginName.Trim(), password.Trim()))
                .ContinueWithLongUIWorkOnSuccess(this, _ => RefreshDataSource());
        }

        private void btnCreateProfile_Click(object sender, EventArgs e)
        {
            string Profile_name = "";
            if (DialogResult.OK != InputBox.ShowDialog(this, ref Profile_name, "Enter name of new Profile"))
                return;
            this.ShowBusyWhileDoing("Creating Profile", pr => Manager.CreateProfile(Profile_name.Trim()))
                .ContinueWithLongUIWorkOnSuccess(this, _ => RefreshDataSource());
        }

        private void btnImpersonate_Click(object sender, EventArgs e)
        {
            eFeatureAccess access = Manager.GetAccessFor(Feature.CanImpersonateLogins);
            if (!access.HasFlag(eFeatureAccess.CanUse))
                return;
            Login choice = Manager.CurrentUser;
            if (DialogResult.OK != ItemPicker.ShowDialog(this, Manager.AllLogins().Keys, login => login.Identifiant, ref choice, "Simulate authentication with :",
                    remarks: "Remarks: If you used screen before with another login to load some data," + Environment.NewLine +
                                "the data can be still here even if the login you choose has no right to see this data."))
                return;
            Manager.Impersonate(choice);
        }

        private void btnGetFullTextReport_Click(object sender, EventArgs e)
        {
            var allRightsReport = Manager.GetAllRightsAsString(false);
            var frm = new MemoForm(allRightsReport);
            frm.Show(this);
        }

        private void gvUserRights_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (!e.HitInfo.InDataRow)
                return;
            if (e.HitInfo.Column == null) // Click on indicator
                return;
            var userRights = (UserRights)gvUserRights.GetRow(e.HitInfo.RowHandle);

            var feature = e.HitInfo.Column.Tag as Feature;
            var colIsParentProfile = e.HitInfo.Column.FieldName == nameof(UserRights.InheritedProfilesStr);
            var colIsProfile = colIsParentProfile || e.HitInfo.Column.FieldName == nameof(UserRights.AssignedProfilesStr);
            if (e.Menu.Items.Count > 0)
                e.Menu.Items[0].BeginGroup = true;

            if (feature != null)
            {
                e.Menu.Items.Insert(0, new DXMenuItem("Remove all direct rights for this feature ", (_, __) =>
                {
                    this.ShowBusyWhileDoing("Adding", pr => userRights.RemoveDirectFeature(feature))
                        .ContinueWithUIWorkOnSuccess(() => RefreshRow(userRights, e.HitInfo.RowHandle));
                })
                {
                    Enabled = chkDisplayProfiles.Checked
                            ? userRights.Rights.TryGetValueClass(feature)?.AssignedAccessByProfile.Any() ?? false
                            : userRights.Rights.TryGetValueClass(feature)?.AssignedAccessByProfile?.ContainsKey(Profile.DirectRight) ?? false
                });
                e.Menu.Items.Insert(1, new DXMenuItem("Set direct right to \"See Only\"", (_, __) =>
                {
                    this.ShowBusyWhileDoing("Setting", pr => userRights.AddDirectFeature(feature, eFeatureAccess.CanSee))
                        .ContinueWithUIWorkOnSuccess(() => RefreshRow(userRights, e.HitInfo.RowHandle));
                })
                { BeginGroup = true });
                e.Menu.Items.Insert(2, new DXMenuItem("Set direct right to \"See && Edit\"", (_, __) =>
                {
                    this.ShowBusyWhileDoing("Setting", pr => userRights.AddDirectFeature(feature, eFeatureAccess.CanSee | eFeatureAccess.CanEdit))
                        .ContinueWithUIWorkOnSuccess(() => RefreshRow(userRights, e.HitInfo.RowHandle));
                }));
                e.Menu.Items.Insert(3, new DXMenuItem("Set direct right to \"Can Use\"", (_, __) =>
                {
                    this.ShowBusyWhileDoing("Setting", pr => userRights.AddDirectFeature(feature, eFeatureAccess.CanUse))
                        .ContinueWithUIWorkOnSuccess(() => RefreshRow(userRights, e.HitInfo.RowHandle));
                }));
            }
            if (feature != null || colIsProfile || colIsParentProfile)
            {
                int i = 3;
                var Profiles = DB.Dao_Base.LoadCollection<Profile>().ToDictionary(p => p.Id);
                if (chkDisplayProfiles.Checked)
                {
                    if (colIsParentProfile)
                        foreach (var Profile in Profiles.Values.Except(userRights.AssignedProfiles))
                            e.Menu.Items.Insert(++i, new DXMenuItem($"Make inherit from \"{Profile.Name}\"", (_, __) =>
                            {
                                this.ShowBusyWhileDoing("Make inheriting", pr => userRights.AssignProfile(Profile))
                                    .ContinueWithUIWorkOnSuccess(() => RefreshRow(userRights, e.HitInfo.RowHandle));
                            }));
                }
                else
                    foreach (var Profile in Profiles.Values.Except(userRights.AssignedProfiles))
                        e.Menu.Items.Insert(++i, new DXMenuItem($"Add in Profile \"{Profile.Name}\"", (_, __) =>
                        {
                            this.ShowBusyWhileDoing("Adding in Profile", pr => userRights.AssignProfile(Profile))
                                .ContinueWithUIWorkOnSuccess(() => RefreshRow(userRights, e.HitInfo.RowHandle));
                        }));
                if (chkDisplayProfiles.Checked)
                    foreach (var Profile in userRights.InheritedProfiles)
                        e.Menu.Items.Insert(++i, new DXMenuItem($"Remove inheriting from \"{Profile.Name}\"", (_, __) =>
                        {
                            this.ShowBusyWhileDoing("Removing from Profile", pr => userRights.RemoveFromProfile(Profile))
                                .ContinueWithUIWorkOnSuccess(() => RefreshRow(userRights, e.HitInfo.RowHandle));
                        }));
                else
                    foreach (var Profile in userRights.AssignedProfiles)
                        e.Menu.Items.Insert(++i, new DXMenuItem($"Remove from Profile \"{Profile.Name}\"", (_, __) =>
                        {
                            this.ShowBusyWhileDoing("Removing from Profile", pr => userRights.RemoveFromProfile(Profile))
                                .ContinueWithUIWorkOnSuccess(() => RefreshRow(userRights, e.HitInfo.RowHandle));
                        }));
                if (e.Menu.Items.Count > 4)
                    e.Menu.Items[4].BeginGroup = true;
            }
        }
        void RefreshRow(UserRights ur, int rowHandle)
        {
            if (_accessesDetails.ContainsKey(ur.LoginAsStr ?? ur.ProfileName))
                _accessesDetails[ur.LoginAsStr ?? ur.ProfileName].Clear();
            gvUserRights.RefreshRow(rowHandle);
            gvUserRights.LayoutChanged(); // trigger CalcRowHeigh
        }


    }
}
