﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

using DevExpress.XtraGrid;
using DevExpress.Data;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Helpers;

using ApplicationBase.DAL.Users;
using ApplicationBase.DAL.Logging;
using ApplicationBase.Common;
using ApplicationBase.Business.Logs;
using DevExpress.XtraGrid.Views.Grid;

namespace ApplicationBase.UI
{
    public partial class LoggingView : ApplicationBaseView
    {
        LogSource LogSource { get { return (LogSource)BusinessObject; } }

        [Obsolete("For designer only", true)]
        protected LoggingView()
            : this((LogSource)null, null, null)
        {
        }
        public LoggingView(LogFilter f, string linkText, string link)
            : this(f.Source, linkText, link)
        {
            _lockedLogFilter = f;
        }
        LogFilter _lockedLogFilter;

        public LoggingView(LogSource source, string linkText, string link)
            : base(source, Feature.AllowEverybody) // Users rights are fully handled by source
        {
            InitializeComponent();
            Text = BootstrapConfig.Instance.ApplicationName + " Logs";

            if (DesignTimeHelper.IsInDesignMode)
                return;

            _linkText = linkText;
            _link = link;
        }
        readonly string _linkText;
        readonly string _link;

        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();

            var locked = _lockedLogFilter != null;
            lytMain.SuspendLayout();
            chkcmbLogins.EditableByUser = LogSource.SelectableLogins.Skip(1).Any() && !locked;
            chkcmbLogins_LayoutItem.Visibility = VisibleIf(!locked);
            dtFrom.Enabled = !locked;
            dtFrom_LayoutItem.Visibility = VisibleIf(!locked);
            dtTo.Enabled = !locked;
            dtTo_LayoutItem.Visibility = VisibleIf(!locked);
            lytMain.ResumeLayout();
        }

        void LoggingView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            _loginByIds = LogSource.SelectableLogins.ToDictionary(kvp => kvp.Key.Person_Id, kvp => kvp.Key);

            dtFrom.EditValue = DateTime.Now.Date;
            dtTo.EditValue = DateTime.Now;
            chkcmbLogins.Fill(true, true, LogSource.AuthenticationManager, LogSource.SelectableLogins);
            chkcmbLogins.SelectedLogins = LogSource.SelectableLogins.Keys.ToList();

            // Set lnkCompanyLogTool's parent to grid, so transparency works
            // Math is for not moving location of lnkCompanyLogTool relatively to screen
            var p = lnkCompanyLogTool.PointToScreen(Point.Empty);
            p = gcLogs.PointToClient(p);
            Controls.Remove(lnkCompanyLogTool);
            gcLogs.Controls.Add(lnkCompanyLogTool);
            lnkCompanyLogTool.Parent = gcLogs;
            lnkCompanyLogTool.BackColor = Color.Transparent;
            lnkCompanyLogTool.Location = p;
        
            // Change the text of link, and make sure distance from right is unchanged
            var width = lnkCompanyLogTool.Width;
            lnkCompanyLogTool.Text = _linkText.IfBlankUse(lnkCompanyLogTool.Text);
            lnkCompanyLogTool.Left += width - lnkCompanyLogTool.Width;
            lnkCompanyLogTool.Visible = !string.IsNullOrWhiteSpace(_link);

            if (_lockedLogFilter != null || Business.Config.Instance.Environment.Domain == Deployment.Data.eEnvironment.LocalNoDB)
                this.BeginInvokeSafely(btnGet.PerformClick);
        }
        Dictionary<long, Login> _loginByIds;

        public LogFilter CurrentFilter
        {
            get
            {
                if (_lockedLogFilter != null)
                    return _lockedLogFilter;
                var filter = new LogFilter(LogSource, _sessionId)
                {
                    DateUtcFrom = dtFrom.DateTime.ToUniversalTime(),
                    DateUtcToIncluded = dtTo.DateTime.ToUniversalTime()
                };
                filter.ForLogins.Clear();
                filter.ForLogins.AddRange(chkcmbLogins.SelectedLogins);
                return filter;
            }
            set
            {
                _sessionId = value.SessionId;
                dtFrom.EditValue = value.DateUtcFrom.ToLocalTime();
                dtTo.EditValue = value.DateUtcToIncluded.ToLocalTime();
                chkcmbLogins.SelectedLogins = value.ForLogins;
                RefreshEnabilitiesAndVisibilities();
            }
        }
        long? _sessionId;

        void btnGet_Click(object sender, EventArgs e)
        {
           this.ShowBusyWhileDoing("Getting logs", pr => LogSource.GetLogs(CurrentFilter))
               .ContinueWithLongUIWorkOnSuccess(this, DisplayDataSource);
        }

        void DisplayDataSource(Dictionary<LogSession, IReadOnlyCollection<Log>> logsBySessions)
        {
            gvLogs.BeginUpdate();
            gvLogs.BeginDataUpdate();
            try
            {
                _logSessions = logsBySessions.Keys.ToDictionary(session => session.Id);
                gcLogs.DataSource = logsBySessions.Values.Flatten().ToList();
                
                if (_firstLoad)
                {
                    _firstLoad = false;
                    InitializeLogView(gvLogs, LogSource, () => _logSessions, () => _loginByIds);
                    gvLogs.CustomSummaryCalculate += gv_CustomSummaryCalculate;
                    gvLogs.BestFitMaxRowCount = 50;
                    gvLogs.BestFitColumns();
                    gvLogs.ExpandAllGroups();
                }
            }
            finally
            {
                gvLogs.EndDataUpdate();
                gvLogs.EndUpdate();
            }
        }
        Dictionary<long, LogSession> _logSessions = new Dictionary<long, LogSession>();
        bool _firstLoad = true;

        protected internal static void InitializeLogView(EnhancedGridView gvLogs,
                                                         LogSource logSource,
                                                         Func<IReadOnlyDictionary<long, LogSession>> getLogSessions,
                                                         Func<IReadOnlyDictionary<long, Login>> getLoginByIds)
        {
            var colLogin = gvLogs.Columns.AddVisible("Login");
            colLogin.UnboundType = UnboundColumnType.String;
            (colLogin as IEnhancedGridColumn).UnboundValueGetter = (row, _) =>
            {
                var log = (Log)row;
                var session = getLogSessions()[log.Session_Id];
                // If user rights change in the middle of life of the current view (ex user become admin)
                // dictionary can have no login (technical) for a specific log 
                var login = getLoginByIds().TryGetValueClass(session.Login_Id);
                return login == null ? "New User " + session.Login_Id.ToStringInvariant()
                        : login.IsService ? login.Identifiant
                        : logSource.SelectableLogins[login].FullName + " (" + login.Identifiant + ")";
            };
            colLogin.GroupIndex = 0;

            var colId = gvLogs.Columns[nameof(Log.Id)];
            colId.Width = 255; // enough to grouping values

            var colSession = gvLogs.Columns[nameof(Log.Session_Id)]; colSession.GroupIndex = 1;

            // Not really handled in log yet (to uncomment later)
            //var colParentThreadId = gvLogs.Columns[nameof(Log.ParentThreadId)]; colParentThreadId.GroupIndex = 2;
            //var colThreadId = gvLogs.Columns[nameof(Log.ThreadId)]; colThreadId.GroupIndex = 3;

            var colTimeStampUTC = gvLogs.Columns[nameof(Log.TimestampUTC)];
            colTimeStampUTC.Visible = false;
            gvLogs.ColumnFormatChanger.DisplayDateAndTimeFor(colTimeStampUTC);

            var colTimeStamp = gvLogs.Columns[nameof(Log.Timestamp)];
            colTimeStamp.Width = 263; // enough to display summary
            gvLogs.GroupSummary.Add(new GridGroupSummaryItem(SummaryItemType.Custom, colTimeStamp.FieldName, colTimeStamp, "{0}")
            { ShowInGroupColumnFooter = null, Tag = MinmaxDate });

            gvLogs.ColumnFormatChanger.DisplayDateAndTimeFor(colTimeStamp);

            gvLogs.RefreshData(); // Absolument indispensable sinon le regroupement fait par Devexpress est erroné (il pense que tous les log appartienent a l'utilisateur des 60 premiere ligne de log...)

            var colMessage = gvLogs.Columns[nameof(Log.Message)];
            colMessage.ColumnEdit = gvLogs.CreateMultilineRepositoryEdit();
            colMessage.OptionsColumn.AllowEdit = false;

            // Weird : This line works but the sort icon is not displayed on column header...
            gvLogs.Columns[nameof(Log.TimestampUTC)].SortOrder = ColumnSortOrder.Ascending;

            gvLogs.RowItemDoubleClick += gv_RowItemDoubleClick;
        }
        const string MinmaxDate = "MinMaxDate";
        static void gv_RowItemDoubleClick(object sender, RowItemDoubleClickEventArgs e)
        {
            var gv = (GridView)sender;
            if (e.Column.FieldName != nameof(Log.Message))
                return;
            if (e.RowHandle >= 0)
            {
                var log = (Log)gv.GetRow(e.RowHandle);

                var parentForm = gv.GridControl.FindForm();
                var memoForm = new TechnicalTools.UI.DX.Forms.MemoForm(log.Message)
                {
                    Text = "Log's message",
                    Size = new Size(parentForm.Width * 80 / 100, parentForm.Height * 80 / 100)
                };
                memoForm.Show(parentForm);
            }
        }

        void gv_CustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        {
            var item = (GridSummaryItem)e.Item; // classe de base pour GridColumnSummaryItem (afficher dnas le footer) et GridGroupSummaryItem affiché dans les group row
            if (Equals(MinmaxDate, item.Tag))
            {
                if (e.SummaryProcess == CustomSummaryProcess.Start)
                    e.TotalValue = new KeyValuePair<DateTime, DateTime>(DateTime.MaxValue, DateTime.MinValue);
                if (e.SummaryProcess == CustomSummaryProcess.Calculate)
                {
                    var cur = (KeyValuePair<DateTime, DateTime>)e.TotalValue;
                    e.TotalValue = new KeyValuePair<DateTime, DateTime>(new DateTime(Math.Min(cur.Key.Ticks, ((DateTime)e.FieldValue).Ticks)),
                                                                        new DateTime(Math.Max(cur.Value.Ticks, ((DateTime)e.FieldValue).Ticks)));
                }
                if (e.SummaryProcess == CustomSummaryProcess.Finalize)
                {
                    var cur = (KeyValuePair<DateTime, DateTime>)e.TotalValue;
                    e.TotalValue = cur.Key + " - " + cur.Value;
                }
            }
        }

        void lnkCompanyLogTool_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_link))
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => Process.Start(_link));
        }
    }
}