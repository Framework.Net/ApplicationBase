﻿namespace ApplicationBase.UI
{
    partial class LoggingView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcLogs = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvLogs = new TechnicalTools.UI.DX.EnhancedGridView();
            this.lytMain = new DevExpress.XtraLayout.LayoutControl();
            this.chkcmbLogins = new ApplicationBase.UI.Controls.LoginSetSelector();
            this.btnGet = new DevExpress.XtraEditors.SimpleButton();
            this.dtTo = new DevExpress.XtraEditors.DateEdit();
            this.dtFrom = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gcLogs_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.dtFrom_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.dtTo_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnGet_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.chkcmbLogins_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lnkCompanyLogTool = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.gcLogs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLogs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytMain)).BeginInit();
            this.lytMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLogs_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnGet_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcmbLogins_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // gcLogs
            // 
            this.gcLogs.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcLogs.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcLogs.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcLogs.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcLogs.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcLogs.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcLogs.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcLogs.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcLogs.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcLogs.EmbeddedNavigator.Buttons.PrevPage.Enabled = false;
            this.gcLogs.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcLogs.EmbeddedNavigator.TextStringFormat = "Line {0} of {1} (visible) of {2} (available)";
            this.gcLogs.Location = new System.Drawing.Point(12, 38);
            this.gcLogs.MainView = this.gvLogs;
            this.gcLogs.Name = "gcLogs";
            this.gcLogs.Size = new System.Drawing.Size(767, 441);
            this.gcLogs.TabIndex = 0;
            this.gcLogs.UseEmbeddedNavigator = true;
            this.gcLogs.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLogs});
            // 
            // gvLogs
            // 
            this.gvLogs.GridControl = this.gcLogs;
            this.gvLogs.Name = "gvLogs";
            this.gvLogs.OptionsBehavior.ReadOnly = true;
            this.gvLogs.OptionsSelection.MultiSelect = true;
            this.gvLogs.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gvLogs.OptionsView.ColumnAutoWidth = false;
            this.gvLogs.OptionsView.ShowAutoFilterRow = true;
            this.gvLogs.OptionsView.ShowFooter = true;
            // 
            // lytMain
            // 
            this.lytMain.Controls.Add(this.chkcmbLogins);
            this.lytMain.Controls.Add(this.btnGet);
            this.lytMain.Controls.Add(this.dtTo);
            this.lytMain.Controls.Add(this.dtFrom);
            this.lytMain.Controls.Add(this.gcLogs);
            this.lytMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lytMain.Location = new System.Drawing.Point(0, 0);
            this.lytMain.Name = "lytMain";
            this.lytMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1137, 212, 650, 400);
            this.lytMain.Root = this.layoutControlGroup1;
            this.lytMain.Size = new System.Drawing.Size(791, 491);
            this.lytMain.TabIndex = 1;
            this.lytMain.Text = "layoutControl1";
            // 
            // chkcmbLogins
            // 
            this.chkcmbLogins.Location = new System.Drawing.Point(512, 12);
            this.chkcmbLogins.MaxSelectedItems = ((uint)(0u));
            this.chkcmbLogins.Name = "chkcmbLogins";
            this.chkcmbLogins.Size = new System.Drawing.Size(257, 20);
            this.chkcmbLogins.TabIndex = 12;
            // 
            // btnGet
            // 
            this.btnGet.Location = new System.Drawing.Point(12, 12);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(177, 22);
            this.btnGet.StyleController = this.lytMain;
            this.btnGet.TabIndex = 6;
            this.btnGet.Text = "Get Logs !";
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // dtTo
            // 
            this.dtTo.EditValue = null;
            this.dtTo.Location = new System.Drawing.Point(344, 12);
            this.dtTo.MinimumSize = new System.Drawing.Size(100, 0);
            this.dtTo.Name = "dtTo";
            this.dtTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dtTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtTo.Size = new System.Drawing.Size(100, 20);
            this.dtTo.StyleController = this.lytMain;
            this.dtTo.TabIndex = 5;
            // 
            // dtFrom
            // 
            this.dtFrom.EditValue = null;
            this.dtFrom.Location = new System.Drawing.Point(220, 12);
            this.dtFrom.MinimumSize = new System.Drawing.Size(100, 0);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dtFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtFrom.Size = new System.Drawing.Size(100, 20);
            this.dtFrom.StyleController = this.lytMain;
            this.dtFrom.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.gcLogs_LayoutItem,
            this.dtFrom_LayoutItem,
            this.dtTo_LayoutItem,
            this.btnGet_LayoutItem,
            this.chkcmbLogins_LayoutItem,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(791, 491);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // gcLogs_LayoutItem
            // 
            this.gcLogs_LayoutItem.Control = this.gcLogs;
            this.gcLogs_LayoutItem.Location = new System.Drawing.Point(0, 26);
            this.gcLogs_LayoutItem.Name = "gcLogs_LayoutItem";
            this.gcLogs_LayoutItem.Size = new System.Drawing.Size(771, 445);
            this.gcLogs_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.gcLogs_LayoutItem.TextVisible = false;
            // 
            // dtFrom_LayoutItem
            // 
            this.dtFrom_LayoutItem.Control = this.dtFrom;
            this.dtFrom_LayoutItem.Location = new System.Drawing.Point(181, 0);
            this.dtFrom_LayoutItem.MaxSize = new System.Drawing.Size(131, 24);
            this.dtFrom_LayoutItem.MinSize = new System.Drawing.Size(131, 24);
            this.dtFrom_LayoutItem.Name = "dtFrom_LayoutItem";
            this.dtFrom_LayoutItem.Size = new System.Drawing.Size(131, 26);
            this.dtFrom_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.dtFrom_LayoutItem.Text = "from";
            this.dtFrom_LayoutItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.dtFrom_LayoutItem.TextSize = new System.Drawing.Size(22, 13);
            this.dtFrom_LayoutItem.TextToControlDistance = 5;
            // 
            // dtTo_LayoutItem
            // 
            this.dtTo_LayoutItem.Control = this.dtTo;
            this.dtTo_LayoutItem.Location = new System.Drawing.Point(312, 0);
            this.dtTo_LayoutItem.MaxSize = new System.Drawing.Size(124, 24);
            this.dtTo_LayoutItem.MinSize = new System.Drawing.Size(124, 24);
            this.dtTo_LayoutItem.Name = "dtTo_LayoutItem";
            this.dtTo_LayoutItem.Size = new System.Drawing.Size(124, 26);
            this.dtTo_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.dtTo_LayoutItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 0, 0, 0);
            this.dtTo_LayoutItem.Text = "to";
            this.dtTo_LayoutItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.dtTo_LayoutItem.TextSize = new System.Drawing.Size(10, 13);
            this.dtTo_LayoutItem.TextToControlDistance = 5;
            // 
            // btnGet_LayoutItem
            // 
            this.btnGet_LayoutItem.Control = this.btnGet;
            this.btnGet_LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.btnGet_LayoutItem.MaxSize = new System.Drawing.Size(181, 26);
            this.btnGet_LayoutItem.MinSize = new System.Drawing.Size(181, 26);
            this.btnGet_LayoutItem.Name = "btnGet_LayoutItem";
            this.btnGet_LayoutItem.Size = new System.Drawing.Size(181, 26);
            this.btnGet_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.btnGet_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.btnGet_LayoutItem.TextVisible = false;
            // 
            // chkcmbLogins_LayoutItem
            // 
            this.chkcmbLogins_LayoutItem.Control = this.chkcmbLogins;
            this.chkcmbLogins_LayoutItem.Location = new System.Drawing.Point(436, 0);
            this.chkcmbLogins_LayoutItem.Name = "chkcmbLogins_LayoutItem";
            this.chkcmbLogins_LayoutItem.Size = new System.Drawing.Size(325, 26);
            this.chkcmbLogins_LayoutItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 0, 0, 0);
            this.chkcmbLogins_LayoutItem.Text = "for login(s):";
            this.chkcmbLogins_LayoutItem.TextSize = new System.Drawing.Size(56, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(761, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(10, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lnkCompanyLogTool
            // 
            this.lnkCompanyLogTool.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkCompanyLogTool.AutoSize = true;
            this.lnkCompanyLogTool.Location = new System.Drawing.Point(547, 48);
            this.lnkCompanyLogTool.Name = "lnkCompanyLogTool";
            this.lnkCompanyLogTool.Size = new System.Drawing.Size(110, 13);
            this.lnkCompanyLogTool.TabIndex = 11;
            this.lnkCompanyLogTool.TabStop = true;
            this.lnkCompanyLogTool.Text = "link to log custom tool";
            this.lnkCompanyLogTool.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkCompanyLogTool_LinkClicked);
            // 
            // LoggingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lnkCompanyLogTool);
            this.Controls.Add(this.lytMain);
            this.Name = "LoggingView";
            this.Size = new System.Drawing.Size(791, 491);
            this.Load += new System.EventHandler(this.LoggingView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcLogs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLogs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytMain)).EndInit();
            this.lytMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLogs_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnGet_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcmbLogins_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TechnicalTools.UI.DX.EnhancedGridControl gcLogs;
        private TechnicalTools.UI.DX.EnhancedGridView gvLogs;
        private DevExpress.XtraLayout.LayoutControl lytMain;
        private DevExpress.XtraEditors.SimpleButton btnGet;
        private DevExpress.XtraEditors.DateEdit dtTo;
        private DevExpress.XtraEditors.DateEdit dtFrom;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem gcLogs_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem dtFrom_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem dtTo_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem btnGet_LayoutItem;
        private System.Windows.Forms.LinkLabel lnkCompanyLogTool;
        private UI.Controls.LoginSetSelector chkcmbLogins;
        private DevExpress.XtraLayout.LayoutControlItem chkcmbLogins_LayoutItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}
