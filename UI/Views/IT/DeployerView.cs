﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Data.Filtering;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;

using TechnicalTools;
using TechnicalTools.Logs;
using TechnicalTools.UI;
using TechnicalTools.Diagnostics;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Forms;

using ApplicationBase.DAL.Users;
using ApplicationBase.Common;
using ApplicationBase.Deployment;
using ApplicationBase.Deployment.Data;

using Deployer = ApplicationBase.Deployment.Deployer;


namespace ApplicationBase.UI
{
    public partial class DeployerView : ApplicationBaseView
    {
        public Deployer Deployer { get { return (Deployer)BusinessObject; } }

        
        readonly BindingList<Deployer.FileToDeploy> _dsFiles = new BindingList<Deployer.FileToDeploy>();
        readonly BindingList<Deployer.EnvironmentWhereToDeploy> _dsEnvs = new BindingList<Deployer.EnvironmentWhereToDeploy>();

        [Obsolete("For designer only", true)]
        protected DeployerView()
        : this(null)
        {
        }
        public DeployerView(Deployer deployer)      
         : base(deployer, Feature.CanDeployApplication)
        {
            InitializeComponent();
            Text = "Deployment";

            if (DesignTimeHelper.IsInDesignMode)
                return;
        }

        void DeployView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            gcFilesToDeploy.DataSource = _dsFiles;
            gvFilesToDeploy.Columns[nameof(Deployer.FileToDeploy.ToDeploy)].VisibleIndex = 0;
            gvFilesToDeploy.Columns[nameof(Deployer.FileToDeploy.Hash)].VisibleIndex = 1;
            gvFilesToDeploy.Columns[nameof(Deployer.FileToDeploy.SourceLocalPath)].VisibleIndex = 2;
            gvFilesToDeploy.Columns[nameof(Deployer.FileToDeploy.SourceFileName)].VisibleIndex = 3;
            gvFilesToDeploy.Columns[nameof(Deployer.FileToDeploy.SourceFullPath)].VisibleIndex = 4;
            gvFilesToDeploy.Columns[nameof(Deployer.FileToDeploy.SourceFullPath)].Visible = false;
            gvFilesToDeploy.Columns[nameof(Deployer.FileToDeploy.MainFolder)].VisibleIndex = 5;
            gvFilesToDeploy.Columns[nameof(Deployer.FileToDeploy.MainFolder)].Visible = false;
            gvFilesToDeploy.OptionsView.ColumnAutoWidth = false;

            gvFilesToDeploy.ActiveFilterCriteria = new BinaryOperator(nameof(Deployer.FileToDeploy.ToDeploy), true, BinaryOperatorType.Equal);

            gcEnvironments.DataSource = _dsEnvs;
            gvEnvironments.Columns[nameof(Deployer.EnvironmentWhereToDeploy.Release)].VisibleIndex = 0;
            gvEnvironments.Columns[nameof(Deployer.EnvironmentWhereToDeploy.Name)].VisibleIndex = 1;
            gvEnvironments.Columns[nameof(Deployer.EnvironmentWhereToDeploy.Domain)].VisibleIndex = 2;
            gvEnvironments.Columns.Remove(gvEnvironments.Columns[nameof(Deployer.EnvironmentWhereToDeploy.Env)]);

            gvEnvironments.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDown; // Sinon il faut trois click pour editer une cellule !
            // Crée un repository item pour que le click de l'utilisateur soit immédiatement pris en compte 
            // Sinon il faut que l'utilisateur sorte de la cellule pour que la valeur saisie soit commité dans le modele (contre intuitif)
            var repo = new RepositoryItemCheckEdit();
            repo.EditValueChanged += (_, __) => gvEnvironments.PostEditor();
            gvEnvironments.Columns[nameof(Deployer.EnvironmentWhereToDeploy.Release)].ColumnEdit = repo;

            RefreshUIElement();

            if (DebugTools.IsForDevelopper)
                BeginInvoke((Action)(() =>
                {
                    btnRefreshFiles.PerformClick();
                    RefreshEnabilitiesAndVisibilities();
                }));
        }

        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();

            var releaseInProd = _dsEnvs?.Any(env => env.Release && env.Domain == Filter.Default.Domain)
                            ?? false;

            mmoPrivateTechnicalNote.Enabled = releaseInProd;
            mmoPublicReleaseNote.Enabled = releaseInProd;
            btnAddBulletForUIOnUserNoticeableFeature.Enabled = releaseInProd;
            btnAddBulletForUserNoticeableFeature.Enabled = releaseInProd;
            btnAddBulletForTechnicalBrick.Enabled = releaseInProd;
            btnAddBulletForListingThings.Enabled = releaseInProd;
        }

        void RefreshUIElement()
        {
            Version v = Deployer.GetNextAvailableVersionFromVersionNumberingSystem();
            if (grpPackageFiles.Tag == null)
                grpPackageFiles.Tag = grpPackageFiles.Text;
            grpPackageFiles.Text = (string)grpPackageFiles.Tag + v;
            if (btnDeploy.Tag == null)
                btnDeploy.Tag = btnDeploy.Text;
            btnDeploy.Text = (string)btnDeploy.Tag + " " + v;
        }
        void btnRefreshFiles_Click(object sender, EventArgs e)
        {
            this.ShowBusyWhileDoing("Processing file to deploy", pr =>
            {
                var lst = Deployer.GetFilesToDeploy();
                foreach (var item in lst)
                {
                    item.ToDeploy &= item.SourceLocalPath != "Logs"
                                 && item.SourceLocalPath != "de"
                                 && item.SourceLocalPath != "es"
                                 && item.SourceLocalPath != "ja"
                                 && item.SourceLocalPath != "ru"
                                 && !(item.SourceFileName.StartsWith("config.") && item.SourceFileName.EndsWith(".cfg"));
                }
                return lst;
            })
            .ContinueWithLongUIWorkOnSuccess(this, ds => 
            {
                _dsFiles.Clear();
                _dsFiles.AddRange(ds);
                RefreshUIElement();
                gvFilesToDeploy.BestFitColumns();
            });

            this.ShowBusyWhileDoing("Getting existing environments", pr =>
            {
                var envs = Deployer.GetExistingEnvironments();
                var prodEnv = envs.FirstOrDefault(env => env.Domain == Filter.Default.Domain && 
                                                         env.Name == Filter.Default.EnvName);
                if (prodEnv != null)
                    prodEnv.Release = true;
                return envs;
            })
            .ContinueWithLongUIWorkOnSuccess(this, envs => 
            {
                _dsEnvs.Clear();
                _dsEnvs.AddRange(envs);
                gvEnvironments.BestFitColumns();
            });
            if (_dsFiles.Count > 0)
                this.ShowBusyWhileDoing("Getting launcher files by build modes", pr =>
                {
                    var launcherBinPath = Deployer.Config.LauncherRelativePathToBinFromMainProjectOutputFolder;
                    return Deployer.GetLauncherFilesByBuildMode(launcherBinPath, _dsFiles.First().MainFolder);
                })
                .ContinueWithLongUIWorkOnSuccess(this, modes =>
                {
                    lueBuildModes.FillWithObjects(modes, m => m.Item1 + (m.Item2.Count == 0 ? "" : " (" + m.Item2.Count + " files)"));
                    var mode = modes.FirstOrDefault();
                    if (mode == null)
                        lueBuildModes.UnselectAnyObject();
                    else
                        lueBuildModes.SelectedValueAsObjectSet(modes.First());
                });
        }

        void btnAddLauncher_Click(object sender, EventArgs e)
        {
            var ftds = lueBuildModes.SelectedValueAsObject<Tuple<string, List<Deployer.FileToDeploy>>>();
            if (ftds.Item2.Count == 0)
                XtraMessageBox.Show("No files to add, please recompile them!");
            else
            {
                for (int i = ftds.Item2.Count - 1; i >= 0; --i)
                    _dsFiles.Insert(0, ftds.Item2[i]);
            }
        }

        void gvEnvironments_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            RefreshEnabilitiesAndVisibilities();
        }


        MemoEdit _lstMemoEntered;
        void mmoPublicReleaseNote_Enter(object sender, EventArgs e)
        {
            _lstMemoEntered = mmoPublicReleaseNote;
        }
        void mmoPrivateTechnicalNote_Enter(object sender, EventArgs e)
        {
            _lstMemoEntered = mmoPrivateTechnicalNote;
        }
        void btnAddBulletForUIOnUserNoticeableFeature_Click(object sender, EventArgs e)
        {
            // like word, other characters here https://www.alt-codes.net/diamond-symbols
            AddBullet(_lstMemoEntered ?? mmoPublicReleaseNote, Deployer.NoticeableUIChange);
        }
        void btnAddBulletForUserNoticeableFeature_Click(object sender, EventArgs e)
        {
            AddBullet(_lstMemoEntered ?? mmoPublicReleaseNote, Deployer.NoticeableBehavior);
        }
        void btnAddBulletForTechnicalBrick_Click(object sender, EventArgs e)
        {
            AddBullet(_lstMemoEntered ?? mmoPublicReleaseNote, Deployer.TechnicalBrick);
        }
        void btnAddBulletForListingThings_Click(object sender, EventArgs e)
        {
            if (_lstMemoEntered == null)
                return;
            AddBullet(_lstMemoEntered, Deployer.Bullet);
        }
        void AddBullet(MemoEdit mmo, char bullet)
        {
            var ss = mmo.SelectionStart;
            var sl = mmo.SelectionLength;
            var index = mmo.Text.LastIndexOf(Environment.NewLine, mmo.SelectionStart, StringComparison.Ordinal);
            if (index == -1)
                index = -Environment.NewLine.Length;
            var inserted = bullet + " ";
            mmo.Text = mmo.Text.Insert(index + Environment.NewLine.Length, inserted);
            mmo.Focus();
            mmo.SelectionStart = ss + inserted.Length;
            mmo.SelectionLength = sl;
        }

        void teAnimatedGIFPath_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            openFileDialog.Filter = "GIF files (*.gif)|*.gif|All files (*.*)|*.*";
            openFileDialog.RestoreDirectory = true;
            if (DialogResult.Cancel == openFileDialog.ShowDialog())
                return;
            teAnimatedGIFPath.Text = openFileDialog.FileName;
        }


        void btnDeploy_Click(object sender, EventArgs e)
        {
            byte[] gif = null;
            if (!teAnimatedGIFPath.Text.IsNullOrWhiteSpace())
            {
                if (!this.ShowBusyWhileDoingUIWorkInPlace("Reading GIF file", pr => { gif = File.ReadAllBytes(teAnimatedGIFPath.Text.Trim()); }))
                    return;
            }

            var files = _dsFiles.Where(f => f.ToDeploy).ToList();
            var envs = _dsEnvs.Where(f => f.Release).Select(env => env.Env).ToList();
            var releaseInProd = _dsEnvs.Any(env => env.Release && env.Domain == Filter.Default.Domain);
            this.ShowBusyWhileDoing("Deploying", 
                                    pr => Deployer.Deploy(files, envs, Deployer.Config.ManifestFileName, 
                                                          releaseInProd ? mmoPublicReleaseNote.Text.Trim() : "",
                                                          releaseInProd ? mmoPrivateTechnicalNote.Text.Trim() : "",
                                                          gif,
                                                          pr))
                .ContinueWithUIWorkOnSuccess(RefreshUIElement);
        }

       
        public static bool DeployVersion(Deployer deployer, Control owner, EnvironmentConfig env, Version versionToInstall)
        {
            var currentVersion = Deployer.GetCurrentVersion();
            if (versionToInstall == currentVersion)
                return false;
            // First test means we want to update current version for the same environment, 
            // so we let user authenticate at minima, because right after that the current version of Application is closed.
            // Other case are if we want to install a new version for another environment (when user switch environment) 
            // so if there is already an installed version we dont do anything and we can close current application.
            if ((BootstrapConfig.Instance.EnvName != env.Name || BootstrapConfig.Instance.Domain != env.Domain) && 
                deployer.IsThisReleaseConsistent(versionToInstall).Count == 0)
                return true;
            var answer = EnhancedXtraMessageBox.Show(new EnhancedXtraMessageArgs()
            {
                Owner = owner,
                AutoClose = true,
                Timeout = 35,
                ShowCountdown = true,
                AutoCloseResult = DialogResult.OK,
                DisableCancel = true,
                DelayToUnlockCancelButton = 30, 
                Buttons = MessageBoxButtons.OKCancel,
                Caption = "Update available",
                Text = currentVersion < versionToInstall
                        ? $"A new version ({versionToInstall}) of {BootstrapConfig.Instance.ApplicationName} has been released." + Environment.NewLine + "Install it now ?"
                        : $"Another version ({versionToInstall}) of {BootstrapConfig.Instance.ApplicationName} is required." + Environment.NewLine + "Install it now (if not already installed) ?"
            });
            if (answer == DialogResult.Cancel)
                return false;
            var frm = new FrmDeployer(deployer, new Filter { Domain = env.Domain, EnvName = env.Name }, Business.BusinessSingletons.Instance.GetAuthenticationManager().CurrentUser.Identifiant)
            {
                CreateShortCut = false
            };
            //frm.RunAfterInstall = runAfterInstall;
            frm.InstallDone += () =>
            {
                LogManager.Default.GetLogger(typeof(DeployerView)).Info($"User {Business.BusinessSingletons.Instance.GetAuthenticationManager().CurrentUser.Identifiant} has installed {BootstrapConfig.Instance.ApplicationName} version {versionToInstall} from version version {currentVersion}.");
                EnhancedXtraMessageBox.Show(new EnhancedXtraMessageArgs()
                {
                    Owner = frm,
                    AutoClose = currentVersion < versionToInstall,
                    Timeout = 10,
                    ShowCountdown = true,
                    AutoCloseResult = DialogResult.OK,
                    Buttons = MessageBoxButtons.OK,
                    Caption = "Install successful!",
                    Text = BootstrapConfig.Instance.ApplicationName + " will now restart..."
                });
                frm.DialogResult = DialogResult.OK;
            };
            //frm.Shown += (_, __) => // Wait handle is created
            //{
            //    frm.BeginInvoke((Action)(() => { frm.Install().ConfigureAwait(false); }));
            //};
            return frm.ShowDialog(owner) == DialogResult.OK;
        }
    }
}