﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Menu;

using TechnicalTools;
using ApplicationBase.Deployment.Data;
using TechnicalTools.Model;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Forms;

using ApplicationBase.DAL.Users;
using ApplicationBase.Business;


namespace ApplicationBase.UI
{
    public partial class EnvironmentSettingsView : ApplicationBaseView
    {
        readonly ApplicationBase.Deployment.Deployer _deployer;

        Dictionary<int, EnvironmentConfig> _envs;
        Dictionary<int, Dictionary<string, EnvironmentSetting>> _allSettings;

        BindingList<EnvironmentSetting> _ds;
        HashSet<EnvironmentSetting> _dsFake;

        public class Set : IUserInteractiveObject { public static Set  Instance = new Set(); }

        [Obsolete("For designer only", true)]
        protected EnvironmentSettingsView()
        : this(null)
        {
        }
        public EnvironmentSettingsView(Set deployer)      
         : base(deployer, Feature.AccessToEnvironmentsAndTheirSettings)
        {
            InitializeComponent();
            Text = "Environments & Settings";

            if (DesignTimeHelper.IsInDesignMode)
                return;
            _deployer = BusinessSingletons.Instance.GetDeployer();
        }

        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();
        }

        void EnvironmentSettingsView_Load(object sender, EventArgs _)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            tlSettings.Columns.AddVisible(nameof(EnvironmentSetting.Key));
            tlSettings.Columns.AddVisible(nameof(EnvironmentSetting.Value));
            tlSettings.Columns.AddVisible(nameof(EnvironmentSetting.Description));
            tlSettings.Columns.AddVisible(nameof(EnvironmentSetting.Environment_Id));
            var col = tlSettings.Columns.AddVisible(nameof(EnvironmentSetting.FullKey));
            col.Visible = false;
            tlSettings.OptionsView.AutoWidth = false;

            tlSettings.ParentFieldName = nameof(EnvironmentSetting.ParentKey);
            tlSettings.KeyFieldName = nameof(EnvironmentSetting.FullKey);
            tlSettings.RootValue = null;


            _envs = _deployer.Config.DataAccessor
                             .LoadCollection<EnvironmentConfig>()
                             .ToDictionary(e => e.Id);
            _allSettings = _deployer.Config.DataAccessor
                                    .LoadCollection<EnvironmentSetting>()
                                    .GroupByToDictionary2(s => s.Environment_Id, s => s.FullKey, grp => grp.Single());
            foreach (var envId in _envs.Keys)
                if (!_allSettings.ContainsKey(envId))
                    _allSettings.Add(envId, new Dictionary<string, EnvironmentSetting>());

            var prod = _envs.Values.SingleOrDefault(e => e.Domain == eEnvironment.Prod && e.Name == ApplicationBase.Deployment.Filter.Default.EnvName);
            // prod est null quand la base de donnes est vierge (vente de licence)
            var envSortedForUI = prod.Yield().Concat(_envs.Values.Except(prod).OrderBy(e => e.Domain).ThenBy(e => e.Name)).NotNull().ToList();
            lueEnvironments.FillWithObjects(envSortedForUI, e => e.FullName, true, false);
            lueEnvironmentComparedTo.FillWithObjects(envSortedForUI, e => e.FullName, true, false);
            lueEnvironments.UnselectAnyObject();
            lueEnvironmentComparedTo.UnselectAnyObject();

            lueEnvironments.EditValueChanged += lueAnyEnvironments_EditValueChanged;
            lueEnvironmentComparedTo.EditValueChanged += lueAnyEnvironments_EditValueChanged;

            tlSettings.ShowingEditor += tlSettings_ShowingEditor;
            tlSettings.CustomDrawNodeCell += tlSettings_CustomDrawNodeCell;

            tlSettings.PopupMenuShowing += TlSettings_PopupMenuShowing;
            if (prod != null)
                BeginInvoke((Action)(() =>
                {
                    lueEnvironmentComparedTo.SelectedValueAsObjectSet(prod);
                    lueEnvironments.SelectedValueAsObjectSet(prod);
                }));
        }

        // TODO : https://www.devexpress.com/Support/Center/Question/Details/CQ28492/bestfitcolumns-font-problem
        void lueAnyEnvironments_EditValueChanged(object sender, EventArgs _)
        {
            var env = lueEnvironments.SelectedValueAsObject<EnvironmentConfig>();
            var comparedEnv = lueEnvironmentComparedTo.SelectedValueAsObject<EnvironmentConfig>();
            var ds = BuildDataSourceFor(env, comparedEnv);
            _ds = ds.Item1;
            _dsFake = ds.Item2;
            tlSettings.DataSource = _ds;
            tlSettings.ExpandAll();
            tlSettings.BestFitColumns();
        }


        Tuple<BindingList<EnvironmentSetting>, HashSet<EnvironmentSetting>> BuildDataSourceFor(EnvironmentConfig env, EnvironmentConfig comparedTo)
        {
            var settingsToDisplay = GetEffectiveSettingFor(_deployer, env);

            // This add "folder" kind of node
            var fake = CompleteWithFolderNode(settingsToDisplay);

            if (comparedTo != null)
            {
                var comparedSettings = GetEffectiveSettingFor(_deployer, comparedTo);
                foreach (var kvp in comparedSettings)
                    if (!settingsToDisplay.ContainsKey(kvp.Key))
                        settingsToDisplay.Add(kvp.Key, new EnvironmentSetting() { Environment_Id = 0, FullKey = kvp.Key, Value = kvp.Value.Value });
            }

            CompleteWithFolderNode(settingsToDisplay, fake);

            return Tuple.Create(settingsToDisplay.Values.ToBindingList(), fake);
        }

        HashSet<EnvironmentSetting> CompleteWithFolderNode(Dictionary<string, EnvironmentSetting> settings, HashSet<EnvironmentSetting> fake = null)
        {
            fake = fake ?? new HashSet<EnvironmentSetting>();
            foreach (var kvp in settings.ToList())
            {
                var s = kvp.Value;
                while (s.ParentKey != null && !settings.ContainsKey(s.ParentKey))
                {
                    s = new EnvironmentSetting() { FullKey = s.ParentKey };
                    fake.Add(s);
                    settings.Add(s.FullKey, s);
                }
            }
            return fake;
        }

        static Dictionary<string, EnvironmentSetting> GetEffectiveSettingFor(ApplicationBase.Deployment.Deployer deployer, EnvironmentConfig env)
        {
            var envs = deployer.Config.DataAccessor
                               .LoadCollection<EnvironmentConfig>()
                               .ToDictionary(e => e.Id);
            var allSettings = deployer.Config.DataAccessor
                                      .LoadCollection<EnvironmentSetting>()
                                      .GroupByToDictionary2(s => s.Environment_Id, 
                                                            s => s.FullKey, 
                                                            grp => grp.Single());
            foreach (var envId in envs.Keys)
                if (!allSettings.ContainsKey(envId))
                    allSettings.Add(envId, new Dictionary<string, EnvironmentSetting>());

            var effectiveSettings = new Dictionary<string, EnvironmentSetting>();
            while (env != null)
            {
                var settings = new Dictionary<string, EnvironmentSetting>(allSettings[env.Id]);
                settings.OverwriteWith(effectiveSettings);
                effectiveSettings = settings;
                env = env.InheritedEnvironment_Id == null ? null
                    : envs[env.InheritedEnvironment_Id.Value];
            }
            return effectiveSettings;
        }

        void TlSettings_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (!(e.Menu is TreeListNodeMenu menu))
                return; // Could be TreeListColumnMenu

            // note setting is null if user right click on AutoFilterNode
            var setting = (EnvironmentSetting)tlSettings.GetDataRecordByNode(menu.Node);
            if (_dsFake.Contains(setting))
                return;
               
            var env = lueEnvironments.SelectedValueAsObject<EnvironmentConfig>();
            if (env == null)
                return;
            e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Edit in Form", (_, __) =>
            {
                var frm = new MemoForm(setting.Value)
                {
                    ShowCancel = true
                };
                var bounds = (FindForm() ?? (Control)this).Bounds;
                bounds.Inflate(-bounds.Width * 10/100, -bounds.Height * 10 / 100);
                frm.Bounds = bounds;
                if (DialogResult.OK != frm.ShowDialog(this))
                    return;
                setting.Value = frm.MemoText;
                tlSettings.RefreshNode(menu.Node);
            }));
            e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Create new key", (_, __) =>
            {
                string fullKey = setting == null ? null : setting.ParentKey + ".";
                if (DialogResult.OK != InputBox.ShowDialog(this, ref fullKey, "Please enter full key", "New Setting creation"))
                    return;
                if (_allSettings[env.Id].TryGetValueClass(fullKey)?.Environment_Id == env.Id)
                {
                    XtraMessageBox.Show(this, "A setting for this environment already exists with this key!");
                    return;
                }

                var newSetting = new EnvironmentSetting()
                {
                    Environment_Id = env.Id,
                    FullKey = fullKey,
                    Value = null
                };
                _deployer.Config.DataAccessor.CreateInDatabase(newSetting);
                _allSettings[newSetting.Environment_Id][newSetting.FullKey] = newSetting;
                _ds.Add(newSetting);
            }));

            e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Save all", (_, __) =>
                {
                    foreach (var s in _ds)
                        if (s.Environment_Id == env.Id &&
                            !_dsFake.Contains(s))
                        {
                            //Console.WriteLine("Value:" + s.Value);
                            _deployer.Config.DataAccessor.UpdateToDatabase(s);
                        }
                })
                {BeginGroup = true});

            e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem($"Delete the setting \"{setting.Key}\" (and use the inherited one, if any)", (_, __) =>
            {
                _deployer.Config.DataAccessor.DeleteInDatabase(setting);
                _allSettings[setting.Environment_Id].Remove(setting.FullKey);
                _ds.Remove(setting);
            })
            { BeginGroup = true });
        }

        void tlSettings_ShowingEditor(object sender, CancelEventArgs e)
        {
            var editedEnv = lueEnvironments.SelectedValueAsObject<EnvironmentConfig>();
            var s = (EnvironmentSetting)tlSettings.GetDataRecordByNode(tlSettings.FocusedNode);
            if (_dsFake.Contains(s)) // "Folder" node are not editable)
                e.Cancel = true;
            else if (editedEnv.Id != s.Environment_Id) // We dont allow to edit inherited setting)
                return;

            e.Cancel = tlSettings.FocusedColumn.FieldName != nameof(EnvironmentSetting.Value)
                    && tlSettings.FocusedColumn.FieldName != nameof(EnvironmentSetting.Description);
        }

        // TODO : passer en CustomColumnDisplayText quand Devexpress >= 17.1
        void tlSettings_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
        {
            if (e.Node.Id == TreeList.AutoFilterNodeId)
                return;
            var setting = (EnvironmentSetting)tlSettings.GetDataRecordByNode(e.Node);
            var editedEnv = lueEnvironments.SelectedValueAsObject<EnvironmentConfig>();
            bool isFake = _dsFake.Contains(setting);
            bool isComparedItem = !isFake && setting.Environment_Id == 0; // compared item that are not in current set of settings

            if (e.Column.FieldName == nameof(EnvironmentSetting.Environment_Id))
            {
                e.CellText = isFake || isComparedItem
                        ? ""
                        : _envs[setting.Environment_Id].FullName;
            }
            else if (e.Column.FieldName == nameof(EnvironmentSetting.Value))
            {
                Match m;
                if (chkHidePassword.Checked)
                    if (setting.FullKey.Contains("Pass"))
                        e.CellText = e.CellText == null ? null : new string('*', e.CellText.Length);
                    else if ((m = rePasswordInConnectionString.Match(setting.Value ?? string.Empty)).Success)
                    {
                        e.CellText = (setting.Value ?? "").Remove(m.Groups["pass"].Index)
                                   + new string('*', m.Groups["pass"].Length)
                                   + (setting.Value ?? string.Empty).Substring(m.Groups["pass"].Index + m.Groups["pass"].Length);
                    }
            }

            if (isComparedItem)
            {
                e.Appearance.BackColor = Color.Red.InterpolateTo(Color.White, 0.5);
            }
            else if (isFake)
            { 
                e.Appearance.BackColor = Color.DodgerBlue;
                _font = _font ?? new Font(e.Appearance.Font, FontStyle.Bold);
                e.Appearance.Font = _font;
            }
            else
            {
                if (setting.Environment_Id != 0 && editedEnv.Id != setting.Environment_Id)
                    e.Appearance.BackColor = Color.LightGray;
                else if (e.Column.FieldName == nameof(EnvironmentSetting.Value))
                {
                    var parentEnvId = _envs[setting.Environment_Id].InheritedEnvironment_Id;
                    if (parentEnvId != null &&
                        setting.Value == _allSettings[parentEnvId.Value].TryGetValueClass(setting.FullKey)?.Value)
                        e.Appearance.BackColor = Color.LightGray;
                }
            }
        }
        static Font _font;
        static readonly Regex rePasswordInConnectionString = new Regex("Password *[=:] *(?<pass>'[^']*'|\"[^\"]*\"|\\{([^}]|\\}\\})*\\}|[^;]*)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);

        void chkHidePassword_CheckedChanged(object sender, EventArgs e)
        {
            tlSettings.RefreshDataSource();
        }
    }
}