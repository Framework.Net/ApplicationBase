﻿namespace ApplicationBase.UI
{
    partial class DeployerView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeployerView));
            this.gcFilesToDeploy = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvFilesToDeploy = new TechnicalTools.UI.DX.EnhancedGridView();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.teAnimatedGIFPath = new DevExpress.XtraEditors.ButtonEdit();
            this.btnAddBulletForListingThings = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddBulletForTechnicalBrick = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.btnAddBulletForUserNoticeableFeature = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.btnAddBulletForUIOnUserNoticeableFeature = new DevExpress.XtraEditors.SimpleButton();
            this.mmoPrivateTechnicalNote = new TechnicalTools.UI.DX.Controls.MemoEdit();
            this.mmoPublicReleaseNote = new TechnicalTools.UI.DX.Controls.MemoEdit();
            this.lueBuildModes = new DevExpress.XtraEditors.LookUpEdit();
            this.btnAddLauncher = new DevExpress.XtraEditors.SimpleButton();
            this.gcEnvironments = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvEnvironments = new TechnicalTools.UI.DX.EnhancedGridView();
            this.btnRefreshFiles = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeploy = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem3 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.grpPackageFiles = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gcFilesToDeploy_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lueBuildModes_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.openFileDialog = new DevExpress.XtraEditors.XtraOpenFileDialog(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcFilesToDeploy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFilesToDeploy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teAnimatedGIFPath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mmoPrivateTechnicalNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mmoPublicReleaseNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBuildModes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcEnvironments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvEnvironments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpPackageFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFilesToDeploy_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBuildModes_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // gcFilesToDeploy
            // 
            this.gcFilesToDeploy.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcFilesToDeploy.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcFilesToDeploy.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcFilesToDeploy.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcFilesToDeploy.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcFilesToDeploy.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcFilesToDeploy.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcFilesToDeploy.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcFilesToDeploy.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcFilesToDeploy.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcFilesToDeploy.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcFilesToDeploy.EmbeddedNavigator.TextStringFormat = "File {0} of {1} (visible) of {2} (total)";
            this.gcFilesToDeploy.Location = new System.Drawing.Point(24, 71);
            this.gcFilesToDeploy.MainView = this.gvFilesToDeploy;
            this.gcFilesToDeploy.Name = "gcFilesToDeploy";
            this.gcFilesToDeploy.Size = new System.Drawing.Size(400, 469);
            this.gcFilesToDeploy.TabIndex = 0;
            this.gcFilesToDeploy.UseEmbeddedNavigator = true;
            this.gcFilesToDeploy.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvFilesToDeploy});
            // 
            // gvFilesToDeploy
            // 
            this.gvFilesToDeploy.GridControl = this.gcFilesToDeploy;
            this.gvFilesToDeploy.Name = "gvFilesToDeploy";
            this.gvFilesToDeploy.OptionsView.ShowAutoFilterRow = true;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.teAnimatedGIFPath);
            this.layoutControl1.Controls.Add(this.btnAddBulletForListingThings);
            this.layoutControl1.Controls.Add(this.btnAddBulletForTechnicalBrick);
            this.layoutControl1.Controls.Add(this.pictureEdit4);
            this.layoutControl1.Controls.Add(this.pictureEdit3);
            this.layoutControl1.Controls.Add(this.pictureEdit2);
            this.layoutControl1.Controls.Add(this.btnAddBulletForUserNoticeableFeature);
            this.layoutControl1.Controls.Add(this.pictureEdit1);
            this.layoutControl1.Controls.Add(this.btnAddBulletForUIOnUserNoticeableFeature);
            this.layoutControl1.Controls.Add(this.mmoPrivateTechnicalNote);
            this.layoutControl1.Controls.Add(this.mmoPublicReleaseNote);
            this.layoutControl1.Controls.Add(this.lueBuildModes);
            this.layoutControl1.Controls.Add(this.btnAddLauncher);
            this.layoutControl1.Controls.Add(this.gcEnvironments);
            this.layoutControl1.Controls.Add(this.btnRefreshFiles);
            this.layoutControl1.Controls.Add(this.btnDeploy);
            this.layoutControl1.Controls.Add(this.gcFilesToDeploy);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(836, 468, 250, 682);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(991, 642);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // teAnimatedGIFPath
            // 
            this.teAnimatedGIFPath.Location = new System.Drawing.Point(532, 421);
            this.teAnimatedGIFPath.Name = "teAnimatedGIFPath";
            this.teAnimatedGIFPath.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.teAnimatedGIFPath.Size = new System.Drawing.Size(435, 20);
            this.teAnimatedGIFPath.StyleController = this.layoutControl1;
            this.teAnimatedGIFPath.TabIndex = 24;
            this.teAnimatedGIFPath.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.teAnimatedGIFPath_ButtonPressed);
            // 
            // btnAddBulletForListingThings
            // 
            this.btnAddBulletForListingThings.Appearance.Font = new System.Drawing.Font("Lucida Sans Unicode", 16F);
            this.btnAddBulletForListingThings.Appearance.Options.UseFont = true;
            this.btnAddBulletForListingThings.Location = new System.Drawing.Point(796, 226);
            this.btnAddBulletForListingThings.MaximumSize = new System.Drawing.Size(24, 0);
            this.btnAddBulletForListingThings.MinimumSize = new System.Drawing.Size(24, 0);
            this.btnAddBulletForListingThings.Name = "btnAddBulletForListingThings";
            this.btnAddBulletForListingThings.Size = new System.Drawing.Size(24, 32);
            this.btnAddBulletForListingThings.StyleController = this.layoutControl1;
            this.btnAddBulletForListingThings.TabIndex = 22;
            this.btnAddBulletForListingThings.Text = "•";
            this.btnAddBulletForListingThings.ToolTip = "A normal bullet just listing things :)";
            this.btnAddBulletForListingThings.Click += new System.EventHandler(this.btnAddBulletForListingThings_Click);
            // 
            // btnAddBulletForTechnicalBrick
            // 
            this.btnAddBulletForTechnicalBrick.Appearance.Font = new System.Drawing.Font("Lucida Sans Unicode", 16F);
            this.btnAddBulletForTechnicalBrick.Appearance.Options.UseFont = true;
            this.btnAddBulletForTechnicalBrick.Location = new System.Drawing.Point(712, 226);
            this.btnAddBulletForTechnicalBrick.MaximumSize = new System.Drawing.Size(24, 0);
            this.btnAddBulletForTechnicalBrick.MinimumSize = new System.Drawing.Size(24, 0);
            this.btnAddBulletForTechnicalBrick.Name = "btnAddBulletForTechnicalBrick";
            this.btnAddBulletForTechnicalBrick.Size = new System.Drawing.Size(24, 32);
            this.btnAddBulletForTechnicalBrick.StyleController = this.layoutControl1;
            this.btnAddBulletForTechnicalBrick.TabIndex = 21;
            this.btnAddBulletForTechnicalBrick.Text = "⟡";
            this.btnAddBulletForTechnicalBrick.ToolTip = "Adding technical brick (code) AND user won\\\'t really notice the difference.";
            this.btnAddBulletForTechnicalBrick.Click += new System.EventHandler(this.btnAddBulletForTechnicalBrick_Click);
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit4.EditValue = ((object)(resources.GetObject("pictureEdit4.EditValue")));
            this.pictureEdit4.Location = new System.Drawing.Point(824, 226);
            this.pictureEdit4.MaximumSize = new System.Drawing.Size(20, 0);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit4.Size = new System.Drawing.Size(20, 32);
            this.pictureEdit4.StyleController = this.layoutControl1;
            this.pictureEdit4.TabIndex = 20;
            this.pictureEdit4.ToolTip = "A normal bullet just listing things :)";
            this.pictureEdit4.ToolTipTitle = "Just Listing anything";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit3.EditValue = ((object)(resources.GetObject("pictureEdit3.EditValue")));
            this.pictureEdit3.Location = new System.Drawing.Point(740, 226);
            this.pictureEdit3.MaximumSize = new System.Drawing.Size(20, 0);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit3.Size = new System.Drawing.Size(20, 32);
            this.pictureEdit3.StyleController = this.layoutControl1;
            this.pictureEdit3.TabIndex = 19;
            this.pictureEdit3.ToolTip = "Adding technical brick (code) AND user won\\\'t really notice the difference.";
            this.pictureEdit3.ToolTipTitle = "Technical Brick";
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = ((object)(resources.GetObject("pictureEdit2.EditValue")));
            this.pictureEdit2.Location = new System.Drawing.Point(656, 226);
            this.pictureEdit2.MaximumSize = new System.Drawing.Size(20, 0);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit2.Size = new System.Drawing.Size(20, 32);
            this.pictureEdit2.StyleController = this.layoutControl1;
            this.pictureEdit2.TabIndex = 18;
            this.pictureEdit2.ToolTip = resources.GetString("pictureEdit2.ToolTip");
            this.pictureEdit2.ToolTipTitle = "User Noticeable Feature";
            // 
            // btnAddBulletForUserNoticeableFeature
            // 
            this.btnAddBulletForUserNoticeableFeature.Appearance.Font = new System.Drawing.Font("Lucida Sans Unicode", 16F);
            this.btnAddBulletForUserNoticeableFeature.Appearance.Options.UseFont = true;
            this.btnAddBulletForUserNoticeableFeature.Location = new System.Drawing.Point(628, 226);
            this.btnAddBulletForUserNoticeableFeature.MaximumSize = new System.Drawing.Size(24, 0);
            this.btnAddBulletForUserNoticeableFeature.MinimumSize = new System.Drawing.Size(24, 0);
            this.btnAddBulletForUserNoticeableFeature.Name = "btnAddBulletForUserNoticeableFeature";
            this.btnAddBulletForUserNoticeableFeature.Size = new System.Drawing.Size(24, 32);
            this.btnAddBulletForUserNoticeableFeature.StyleController = this.layoutControl1;
            this.btnAddBulletForUserNoticeableFeature.TabIndex = 17;
            this.btnAddBulletForUserNoticeableFeature.Text = "⯁";
            this.btnAddBulletForUserNoticeableFeature.ToolTip = "When adding a module (code only) that does things that user will notice";
            this.btnAddBulletForUserNoticeableFeature.Click += new System.EventHandler(this.btnAddBulletForUserNoticeableFeature_Click);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(572, 226);
            this.pictureEdit1.MaximumSize = new System.Drawing.Size(20, 0);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit1.Size = new System.Drawing.Size(20, 32);
            this.pictureEdit1.StyleController = this.layoutControl1;
            this.pictureEdit1.TabIndex = 16;
            this.pictureEdit1.ToolTip = "Like User Noticeable Feature but only if in addition the major UI part (screen / " +
    "form / view) has been done.";
            this.pictureEdit1.ToolTipTitle = "Interface [+? user feature]";
            // 
            // btnAddBulletForUIOnUserNoticeableFeature
            // 
            this.btnAddBulletForUIOnUserNoticeableFeature.Appearance.Font = new System.Drawing.Font("Lucida Sans Unicode", 16F);
            this.btnAddBulletForUIOnUserNoticeableFeature.Appearance.Options.UseFont = true;
            this.btnAddBulletForUIOnUserNoticeableFeature.Location = new System.Drawing.Point(544, 226);
            this.btnAddBulletForUIOnUserNoticeableFeature.MaximumSize = new System.Drawing.Size(24, 0);
            this.btnAddBulletForUIOnUserNoticeableFeature.MinimumSize = new System.Drawing.Size(24, 0);
            this.btnAddBulletForUIOnUserNoticeableFeature.Name = "btnAddBulletForUIOnUserNoticeableFeature";
            this.btnAddBulletForUIOnUserNoticeableFeature.Size = new System.Drawing.Size(24, 32);
            this.btnAddBulletForUIOnUserNoticeableFeature.StyleController = this.layoutControl1;
            this.btnAddBulletForUIOnUserNoticeableFeature.TabIndex = 15;
            this.btnAddBulletForUIOnUserNoticeableFeature.Text = " ❖";
            this.btnAddBulletForUIOnUserNoticeableFeature.ToolTip = resources.GetString("btnAddBulletForUIOnUserNoticeableFeature.ToolTip");
            this.btnAddBulletForUIOnUserNoticeableFeature.Click += new System.EventHandler(this.btnAddBulletForUIOnUserNoticeableFeature_Click);
            // 
            // mmoPrivateTechnicalNote
            // 
            this.mmoPrivateTechnicalNote.Location = new System.Drawing.Point(462, 461);
            this.mmoPrivateTechnicalNote.Name = "mmoPrivateTechnicalNote";
            this.mmoPrivateTechnicalNote.Size = new System.Drawing.Size(505, 116);
            this.mmoPrivateTechnicalNote.StyleController = this.layoutControl1;
            this.mmoPrivateTechnicalNote.TabIndex = 14;
            this.mmoPrivateTechnicalNote.Enter += new System.EventHandler(this.mmoPrivateTechnicalNote_Enter);
            // 
            // mmoPublicReleaseNote
            // 
            this.mmoPublicReleaseNote.Location = new System.Drawing.Point(462, 278);
            this.mmoPublicReleaseNote.Name = "mmoPublicReleaseNote";
            this.mmoPublicReleaseNote.Size = new System.Drawing.Size(505, 129);
            this.mmoPublicReleaseNote.StyleController = this.layoutControl1;
            this.mmoPublicReleaseNote.TabIndex = 9;
            this.mmoPublicReleaseNote.Enter += new System.EventHandler(this.mmoPublicReleaseNote_Enter);
            // 
            // lueBuildModes
            // 
            this.lueBuildModes.Location = new System.Drawing.Point(244, 549);
            this.lueBuildModes.Name = "lueBuildModes";
            this.lueBuildModes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueBuildModes.Properties.NullText = "<Recompile launcher project>";
            this.lueBuildModes.Size = new System.Drawing.Size(180, 20);
            this.lueBuildModes.StyleController = this.layoutControl1;
            this.lueBuildModes.TabIndex = 8;
            // 
            // btnAddLauncher
            // 
            this.btnAddLauncher.Location = new System.Drawing.Point(24, 544);
            this.btnAddLauncher.Name = "btnAddLauncher";
            this.btnAddLauncher.Size = new System.Drawing.Size(109, 33);
            this.btnAddLauncher.StyleController = this.layoutControl1;
            this.btnAddLauncher.TabIndex = 7;
            this.btnAddLauncher.Text = "Add Launcher files\r\nto release";
            this.btnAddLauncher.Click += new System.EventHandler(this.btnAddLauncher_Click);
            // 
            // gcEnvironments
            // 
            this.gcEnvironments.Location = new System.Drawing.Point(462, 45);
            this.gcEnvironments.MainView = this.gvEnvironments;
            this.gcEnvironments.Name = "gcEnvironments";
            this.gcEnvironments.Size = new System.Drawing.Size(505, 122);
            this.gcEnvironments.TabIndex = 6;
            this.gcEnvironments.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvEnvironments});
            // 
            // gvEnvironments
            // 
            this.gvEnvironments.GridControl = this.gcEnvironments;
            this.gvEnvironments.Name = "gvEnvironments";
            this.gvEnvironments.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvEnvironments_CellValueChanged);
            // 
            // btnRefreshFiles
            // 
            this.btnRefreshFiles.Location = new System.Drawing.Point(24, 45);
            this.btnRefreshFiles.Name = "btnRefreshFiles";
            this.btnRefreshFiles.Size = new System.Drawing.Size(400, 22);
            this.btnRefreshFiles.StyleController = this.layoutControl1;
            this.btnRefreshFiles.TabIndex = 5;
            this.btnRefreshFiles.Text = "Refresh";
            this.btnRefreshFiles.Click += new System.EventHandler(this.btnRefreshFiles_Click);
            // 
            // btnDeploy
            // 
            this.btnDeploy.Location = new System.Drawing.Point(840, 608);
            this.btnDeploy.Name = "btnDeploy";
            this.btnDeploy.Size = new System.Drawing.Size(139, 22);
            this.btnDeploy.StyleController = this.layoutControl1;
            this.btnDeploy.TabIndex = 4;
            this.btnDeploy.Text = "Deploy";
            this.btnDeploy.Click += new System.EventHandler(this.btnDeploy_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.splitterItem2,
            this.grpPackageFiles,
            this.splitterItem1,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(991, 642);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup4.Location = new System.Drawing.Point(438, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(533, 171);
            this.layoutControlGroup4.Text = "Environments where this release will be allowed to connect";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gcEnvironments;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(509, 126);
            this.layoutControlItem3.Text = "Environments where this release is allowed to connect";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.simpleLabelItem1,
            this.layoutControlItem15,
            this.splitterItem3,
            this.emptySpaceItem3,
            this.layoutControlItem11,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem12,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.layoutControlItem20,
            this.layoutControlItem13,
            this.emptySpaceItem7,
            this.layoutControlItem21});
            this.layoutControlGroup5.Location = new System.Drawing.Point(438, 181);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(533, 400);
            this.layoutControlGroup5.Text = "Release notes";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.mmoPublicReleaseNote;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 36);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(509, 149);
            this.layoutControlItem10.Text = "For user : What feature(s) contains this release ?";
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(370, 13);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem1.MaxSize = new System.Drawing.Size(64, 24);
            this.simpleLabelItem1.MinSize = new System.Drawing.Size(64, 24);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Size = new System.Drawing.Size(64, 36);
            this.simpleLabelItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem1.Text = "Add Bullet :";
            this.simpleLabelItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.mmoPrivateTechnicalNote;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 219);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(509, 136);
            this.layoutControlItem15.Text = "For developpers : Versionning label, side note etc (wil not be visible by users)";
            this.layoutControlItem15.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(370, 13);
            // 
            // splitterItem3
            // 
            this.splitterItem3.AllowHotTrack = true;
            this.splitterItem3.Location = new System.Drawing.Point(0, 185);
            this.splitterItem3.Name = "splitterItem3";
            this.splitterItem3.Size = new System.Drawing.Size(509, 10);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(386, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(123, 36);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.btnAddBulletForUIOnUserNoticeableFeature;
            this.layoutControlItem11.Location = new System.Drawing.Point(82, 0);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(28, 0);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(28, 36);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(28, 36);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.pictureEdit1;
            this.layoutControlItem16.Location = new System.Drawing.Point(110, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(24, 36);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.btnAddBulletForUserNoticeableFeature;
            this.layoutControlItem17.Location = new System.Drawing.Point(166, 0);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(28, 36);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(28, 36);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(28, 36);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.pictureEdit2;
            this.layoutControlItem12.Location = new System.Drawing.Point(194, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(24, 36);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.pictureEdit3;
            this.layoutControlItem18.Location = new System.Drawing.Point(278, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(24, 36);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.pictureEdit4;
            this.layoutControlItem19.Location = new System.Drawing.Point(362, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(24, 36);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(134, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(32, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(32, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(32, 36);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(218, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(32, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(32, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(32, 36);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(302, 0);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(32, 0);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(32, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(32, 36);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.btnAddBulletForTechnicalBrick;
            this.layoutControlItem20.Location = new System.Drawing.Point(250, 0);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(28, 36);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(28, 36);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(28, 36);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.btnAddBulletForListingThings;
            this.layoutControlItem13.Location = new System.Drawing.Point(334, 0);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(28, 36);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(28, 36);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(28, 36);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(64, 0);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(18, 0);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(18, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(18, 36);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.teAnimatedGIFPath;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 195);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(509, 24);
            this.layoutControlItem21.Text = "Animated GIF";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(65, 13);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.FixedStyle = DevExpress.XtraLayout.SplitterItemFixedStyles.LeftTop;
            this.splitterItem2.Location = new System.Drawing.Point(438, 171);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(533, 10);
            // 
            // grpPackageFiles
            // 
            this.grpPackageFiles.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.gcFilesToDeploy_LayoutItem,
            this.layoutControlItem4,
            this.lueBuildModes_LayoutItem});
            this.grpPackageFiles.Location = new System.Drawing.Point(0, 0);
            this.grpPackageFiles.Name = "grpPackageFiles";
            this.grpPackageFiles.Size = new System.Drawing.Size(428, 581);
            this.grpPackageFiles.Text = "Files to package in version";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnRefreshFiles;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(51, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(404, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // gcFilesToDeploy_LayoutItem
            // 
            this.gcFilesToDeploy_LayoutItem.Control = this.gcFilesToDeploy;
            this.gcFilesToDeploy_LayoutItem.Location = new System.Drawing.Point(0, 26);
            this.gcFilesToDeploy_LayoutItem.Name = "gcFilesToDeploy_LayoutItem";
            this.gcFilesToDeploy_LayoutItem.Size = new System.Drawing.Size(404, 473);
            this.gcFilesToDeploy_LayoutItem.Text = "Files in the release to deploy";
            this.gcFilesToDeploy_LayoutItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.gcFilesToDeploy_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.gcFilesToDeploy_LayoutItem.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnAddLauncher;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 499);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(113, 37);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(113, 37);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(113, 37);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // lueBuildModes_LayoutItem
            // 
            this.lueBuildModes_LayoutItem.Control = this.lueBuildModes;
            this.lueBuildModes_LayoutItem.Location = new System.Drawing.Point(113, 499);
            this.lueBuildModes_LayoutItem.Name = "lueBuildModes_LayoutItem";
            this.lueBuildModes_LayoutItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 7, 2);
            this.lueBuildModes_LayoutItem.Size = new System.Drawing.Size(291, 37);
            this.lueBuildModes_LayoutItem.Text = "From this build Mode:";
            this.lueBuildModes_LayoutItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lueBuildModes_LayoutItem.TextSize = new System.Drawing.Size(102, 13);
            this.lueBuildModes_LayoutItem.TextToControlDistance = 5;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.FixedStyle = DevExpress.XtraLayout.SplitterItemFixedStyles.RightBottom;
            this.splitterItem1.Location = new System.Drawing.Point(428, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(10, 581);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 596);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(828, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnDeploy;
            this.layoutControlItem1.Location = new System.Drawing.Point(828, 596);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(143, 26);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(143, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(143, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 581);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 15);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(1, 15);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(971, 15);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // DeployerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "DeployerView";
            this.Size = new System.Drawing.Size(991, 642);
            this.Load += new System.EventHandler(this.DeployView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcFilesToDeploy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFilesToDeploy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teAnimatedGIFPath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mmoPrivateTechnicalNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mmoPublicReleaseNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBuildModes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcEnvironments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvEnvironments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpPackageFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFilesToDeploy_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBuildModes_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.DX.EnhancedGridControl gcFilesToDeploy;
        private TechnicalTools.UI.DX.EnhancedGridView gvFilesToDeploy;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnDeploy;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem gcFilesToDeploy_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton btnRefreshFiles;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private TechnicalTools.UI.DX.EnhancedGridControl gcEnvironments;
        private TechnicalTools.UI.DX.EnhancedGridView gvEnvironments;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraEditors.SimpleButton btnAddLauncher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.LookUpEdit lueBuildModes;
        private DevExpress.XtraLayout.LayoutControlItem lueBuildModes_LayoutItem;
        private TechnicalTools.UI.DX.Controls.MemoEdit mmoPublicReleaseNote;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.LayoutControlGroup grpPackageFiles;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private TechnicalTools.UI.DX.Controls.MemoEdit mmoPrivateTechnicalNote;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.SplitterItem splitterItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.SimpleButton btnAddBulletForUIOnUserNoticeableFeature;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.SimpleButton btnAddBulletForUserNoticeableFeature;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.SimpleButton btnAddBulletForTechnicalBrick;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.SimpleButton btnAddBulletForListingThings;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.ButtonEdit teAnimatedGIFPath;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.XtraOpenFileDialog openFileDialog;
    }
}
