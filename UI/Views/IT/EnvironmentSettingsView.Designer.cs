﻿namespace ApplicationBase.UI
{
    partial class EnvironmentSettingsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.chkHidePassword = new DevExpress.XtraEditors.CheckEdit();
            this.lueEnvironmentComparedTo = new DevExpress.XtraEditors.LookUpEdit();
            this.lueEnvironments = new DevExpress.XtraEditors.LookUpEdit();
            this.tlSettings = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkHidePassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEnvironmentComparedTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEnvironments.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.chkHidePassword);
            this.layoutControl2.Controls.Add(this.lueEnvironmentComparedTo);
            this.layoutControl2.Controls.Add(this.lueEnvironments);
            this.layoutControl2.Controls.Add(this.tlSettings);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(991, 642);
            this.layoutControl2.TabIndex = 2;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // chkHidePassword
            // 
            this.chkHidePassword.EditValue = true;
            this.chkHidePassword.Location = new System.Drawing.Point(793, 12);
            this.chkHidePassword.Name = "chkHidePassword";
            this.chkHidePassword.Properties.Caption = "Hide password values";
            this.chkHidePassword.Size = new System.Drawing.Size(186, 19);
            this.chkHidePassword.StyleController = this.layoutControl2;
            this.chkHidePassword.TabIndex = 5;
            this.chkHidePassword.CheckedChanged += new System.EventHandler(this.chkHidePassword_CheckedChanged);
            // 
            // lueEnvironmentComparedTo
            // 
            this.lueEnvironmentComparedTo.Location = new System.Drawing.Point(614, 12);
            this.lueEnvironmentComparedTo.Name = "lueEnvironmentComparedTo";
            this.lueEnvironmentComparedTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueEnvironmentComparedTo.Properties.NullText = "";
            this.lueEnvironmentComparedTo.Size = new System.Drawing.Size(175, 20);
            this.lueEnvironmentComparedTo.StyleController = this.layoutControl2;
            this.lueEnvironmentComparedTo.TabIndex = 4;
            // 
            // lueEnvironments
            // 
            this.lueEnvironments.Location = new System.Drawing.Point(87, 12);
            this.lueEnvironments.Name = "lueEnvironments";
            this.lueEnvironments.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueEnvironments.Properties.NullText = "";
            this.lueEnvironments.Size = new System.Drawing.Size(455, 20);
            this.lueEnvironments.StyleController = this.layoutControl2;
            this.lueEnvironments.TabIndex = 1;
            // 
            // tlSettings
            // 
            this.tlSettings.Location = new System.Drawing.Point(12, 36);
            this.tlSettings.Name = "tlSettings";
            this.tlSettings.OptionsView.ShowAutoFilterRow = true;
            this.tlSettings.OptionsView.ShowHorzLines = false;
            this.tlSettings.Size = new System.Drawing.Size(967, 594);
            this.tlSettings.TabIndex = 0;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(991, 642);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.tlSettings;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(971, 598);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lueEnvironments;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(534, 24);
            this.layoutControlItem7.Text = "Environments :";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(72, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.lueEnvironmentComparedTo;
            this.layoutControlItem8.Location = new System.Drawing.Point(534, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(247, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(247, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(247, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "Compare to :";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(63, 13);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.chkHidePassword;
            this.layoutControlItem9.Location = new System.Drawing.Point(781, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(190, 24);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // EnvironmentSettingsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl2);
            this.Name = "EnvironmentSettingsView";
            this.Size = new System.Drawing.Size(991, 642);
            this.Load += new System.EventHandler(this.EnvironmentSettingsView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkHidePassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEnvironmentComparedTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEnvironments.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraTreeList.TreeList tlSettings;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.LookUpEdit lueEnvironments;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.LookUpEdit lueEnvironmentComparedTo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.CheckEdit chkHidePassword;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
    }
}
