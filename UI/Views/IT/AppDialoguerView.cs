﻿using System;
using System.ComponentModel;

using DevExpress.XtraEditors;

using TechnicalTools.UI;
using TechnicalTools.UI.DX;

using ApplicationBase.DAL.Users;
using ApplicationBase.DAL.Dialoguing;

using ApplicationBase.Business;


namespace ApplicationBase.UI.Dialoguing
{
    public partial class AppDialoguerView : ApplicationBaseView
    {
        Dialoguer Dialoguer { get { return (Dialoguer)BusinessObject; } }

        readonly BindingList<Dialogue> _dialogues = new BindingList<Dialogue>();

        [Obsolete("For designer only", true)]
        protected AppDialoguerView()
            : this(null)
        {
        }
        public AppDialoguerView(Dialoguer dialoguer)      
         : base(dialoguer, Feature.CanAccessExperimentalParts)
        {
            InitializeComponent();
            Text = "Dialoguer";

            if (DesignTimeHelper.IsInDesignMode)
                return;
        }

        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();
        }

        void AppDialoguerView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            Dialoguer.ReceiveEvent += _dialoguer_ReceiveEvent;
            Dialoguer.BroadcastEvent += _dialoguer_BroadcastEvent;
            TopFormOrPanelClosed += (_, __) =>
            {
                Dialoguer.ReceiveEvent -= _dialoguer_ReceiveEvent;
                Dialoguer.BroadcastEvent -= _dialoguer_BroadcastEvent;
            };
            if (!Dialoguer.Start())
                XtraMessageBox.Show(this, $"Fail to start dialoguer on {System.Windows.Forms.Application.ProductName} database :'( !");

            gcDialogues.DataSource = _dialogues;
            gvDialogues.OptionsBehavior.Editable = false;

            gvDialogues.Columns[nameof(Dialogue.OriginActorType)].ConfigureForObjectsAsInt(ActorType.All, a => a.Name, a => a.Id, true);
            gvDialogues.Columns[nameof(Dialogue.EventType)].ConfigureForObjectsAsInt(EventType.All, a => a.Name, a => a.Id, true);
            gvDialogues.Columns[nameof(Dialogue.Status)].ConfigureForObjectsAsInt(Status.All, a => a.Name, a => a.Id, true);
            gvDialogues.Columns[nameof(Dialogue.EntityType)].ConfigureForObjectsAsInt(EntityType.All, a => a.Name, a => a.Id, true);
            
            TopFormOrPanelClosed += (_, __) => Dialoguer.Stop();
        }

        void _dialoguer_BroadcastEvent(Dialogue e)
        {
            BeginInvoke((Action)(() =>
                {
                    _dialogues.Add(e);
                }));
        }

        void _dialoguer_ReceiveEvent(Dialogue e)
        {
            BeginInvoke((Action)(() =>
            {
                _dialogues.Add(e);
            }));
        }

        void btnClear_Click(object sender, EventArgs e)
        {
            _dialogues.Clear();
        }

        void btnDiscoverConnectedClients_Click(object sender, EventArgs e)
        {
            var responses = new BindingList<Dialogue>();
            var frm = new GenericGridViewerForm(responses);
            frm.Show(this);
            frm.Shown += (_, __) =>
            {
                var messenger = BusinessSingletons.Instance.GetMessenger();
                var request = messenger.ListAllConnectedActors(response => BeginInvoke((Action)(() => responses.Add(response))));
                frm.Tag = request;
            };
        }



    }
}