﻿namespace ApplicationBase.UI
{
    partial class UserRightManagementView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcUserRights = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvUserRights = new TechnicalTools.UI.DX.EnhancedGridView();
            this.repoRichFeatureEdit = new DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnCreateProfile = new DevExpress.XtraEditors.SimpleButton();
            this.btnImpersonate = new DevExpress.XtraEditors.SimpleButton();
            this.chkDisplayProfiles = new DevExpress.XtraEditors.CheckEdit();
            this.btnCreateNewUser = new DevExpress.XtraEditors.SimpleButton();
            this.btnGetFullTextReport = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gcUserRights_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.gcUserRights)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUserRights)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoRichFeatureEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisplayProfiles.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcUserRights_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // gcUserRights
            // 
            this.gcUserRights.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcUserRights.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcUserRights.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcUserRights.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcUserRights.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcUserRights.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcUserRights.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcUserRights.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcUserRights.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcUserRights.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcUserRights.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcUserRights.EmbeddedNavigator.TextStringFormat = "Right Owner {0} of {1} (visible) of {2} (total)";
            this.gcUserRights.Location = new System.Drawing.Point(12, 64);
            this.gcUserRights.MainView = this.gvUserRights;
            this.gcUserRights.Name = "gcUserRights";
            this.gcUserRights.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repoRichFeatureEdit});
            this.gcUserRights.Size = new System.Drawing.Size(958, 380);
            this.gcUserRights.TabIndex = 0;
            this.gcUserRights.UseEmbeddedNavigator = true;
            this.gcUserRights.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvUserRights});
            // 
            // gvUserRights
            // 
            this.gvUserRights.GridControl = this.gcUserRights;
            this.gvUserRights.Name = "gvUserRights";
            this.gvUserRights.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gvUserRights_PopupMenuShowing);
            this.gvUserRights.CalcRowHeight += new DevExpress.XtraGrid.Views.Grid.RowHeightEventHandler(this.gvUserRights_CalcRowHeight);
            this.gvUserRights.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gvUserRights_CustomUnboundColumnData);
            // 
            // repoRichFeatureEdit
            // 
            this.repoRichFeatureEdit.Name = "repoRichFeatureEdit";
            this.repoRichFeatureEdit.ShowCaretInReadOnly = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(226, 13);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Note : All changes are immediately done in DB !";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnCreateProfile);
            this.layoutControl1.Controls.Add(this.btnImpersonate);
            this.layoutControl1.Controls.Add(this.chkDisplayProfiles);
            this.layoutControl1.Controls.Add(this.btnCreateNewUser);
            this.layoutControl1.Controls.Add(this.btnGetFullTextReport);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.gcUserRights);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(814, 240, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(982, 456);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnCreateProfile
            // 
            this.btnCreateProfile.Location = new System.Drawing.Point(264, 38);
            this.btnCreateProfile.Name = "btnCreateProfile";
            this.btnCreateProfile.Size = new System.Drawing.Size(137, 22);
            this.btnCreateProfile.StyleController = this.layoutControl1;
            this.btnCreateProfile.TabIndex = 7;
            this.btnCreateProfile.Text = "Create profile";
            this.btnCreateProfile.Click += new System.EventHandler(this.btnCreateProfile_Click);
            // 
            // btnImpersonate
            // 
            this.btnImpersonate.Location = new System.Drawing.Point(726, 12);
            this.btnImpersonate.Name = "btnImpersonate";
            this.btnImpersonate.Size = new System.Drawing.Size(88, 22);
            this.btnImpersonate.StyleController = this.layoutControl1;
            this.btnImpersonate.TabIndex = 6;
            this.btnImpersonate.Text = "Impersonate";
            this.btnImpersonate.Click += new System.EventHandler(this.btnImpersonate_Click);
            // 
            // chkDisplayProfiles
            // 
            this.chkDisplayProfiles.Location = new System.Drawing.Point(12, 29);
            this.chkDisplayProfiles.Name = "chkDisplayProfiles";
            this.chkDisplayProfiles.Properties.Caption = "Display profiles";
            this.chkDisplayProfiles.Size = new System.Drawing.Size(226, 19);
            this.chkDisplayProfiles.StyleController = this.layoutControl1;
            this.chkDisplayProfiles.TabIndex = 5;
            this.chkDisplayProfiles.CheckedChanged += new System.EventHandler(this.chkDisplayProfiles_CheckedChanged);
            // 
            // btnCreateNewUser
            // 
            this.btnCreateNewUser.Location = new System.Drawing.Point(264, 12);
            this.btnCreateNewUser.Name = "btnCreateNewUser";
            this.btnCreateNewUser.Size = new System.Drawing.Size(137, 22);
            this.btnCreateNewUser.StyleController = this.layoutControl1;
            this.btnCreateNewUser.TabIndex = 4;
            this.btnCreateNewUser.Text = "Create new user";
            this.btnCreateNewUser.Click += new System.EventHandler(this.btnCreateNewUser_Click);
            // 
            // btnGetFullTextReport
            // 
            this.btnGetFullTextReport.Location = new System.Drawing.Point(828, 12);
            this.btnGetFullTextReport.Name = "btnGetFullTextReport";
            this.btnGetFullTextReport.Size = new System.Drawing.Size(142, 22);
            this.btnGetFullTextReport.StyleController = this.layoutControl1;
            this.btnGetFullTextReport.TabIndex = 3;
            this.btnGetFullTextReport.Text = "Get Full Text Report";
            this.btnGetFullTextReport.Click += new System.EventHandler(this.btnGetFullTextReport_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.gcUserRights_LayoutItem,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem3,
            this.emptySpaceItem4,
            this.layoutControlItem6});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(982, 456);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // gcUserRights_LayoutItem
            // 
            this.gcUserRights_LayoutItem.Control = this.gcUserRights;
            this.gcUserRights_LayoutItem.Location = new System.Drawing.Point(0, 52);
            this.gcUserRights_LayoutItem.Name = "gcUserRights_LayoutItem";
            this.gcUserRights_LayoutItem.Size = new System.Drawing.Size(962, 384);
            this.gcUserRights_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.gcUserRights_LayoutItem.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.labelControl1;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(230, 17);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.chkDisplayProfiles;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(230, 35);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnImpersonate;
            this.layoutControlItem5.Location = new System.Drawing.Point(714, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(92, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(92, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(92, 52);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnGetFullTextReport;
            this.layoutControlItem1.Location = new System.Drawing.Point(816, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(146, 26);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(146, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(146, 52);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(806, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(10, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(10, 52);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(393, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(321, 52);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnCreateNewUser;
            this.layoutControlItem3.Location = new System.Drawing.Point(252, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(141, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(141, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(141, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(230, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(22, 52);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnCreateProfile;
            this.layoutControlItem6.Location = new System.Drawing.Point(252, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(141, 26);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // UserRightManagementView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "UserRightManagementView";
            this.Size = new System.Drawing.Size(982, 456);
            this.Load += new System.EventHandler(this.UserRightManagementView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcUserRights)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUserRights)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoRichFeatureEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkDisplayProfiles.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcUserRights_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.DX.EnhancedGridControl gcUserRights;
        private TechnicalTools.UI.DX.EnhancedGridView gvUserRights;
        private DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit repoRichFeatureEdit;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnGetFullTextReport;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem gcUserRights_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton btnCreateNewUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.CheckEdit chkDisplayProfiles;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton btnImpersonate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.SimpleButton btnCreateProfile;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    }
}