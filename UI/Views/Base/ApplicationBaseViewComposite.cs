﻿using System;
using System.Windows.Forms;
using System.Diagnostics;

using TechnicalTools.UI;
using TechnicalTools.UI.DX;

using ApplicationBase.DAL;
using ApplicationBase.DAL.Users;

using ApplicationBase.Business;


namespace ApplicationBase.UI
{
    // Classe de base pour afficher une vue d'un objet (et un seul !)
    // Avoid to put code in Load event if the ApplicationBaseView is displayed in a dockpanel
    // Because the load event occurs right after the load panel is visible
    // So the dock panel is display but not initialized (the title for example will be = this.Text)
    // Then the displayed dock is empty with a bad caption until the load event is done ...
    public partial class ApplicationBaseViewComposite : XtraUserControlComposite, IApplicationBaseView
    {
        public       Control Owner          { get { return _impl.Owner; } protected internal set { _impl.Owner = value; } }
        public        object BusinessObject { get { return _impl.BusinessObject; } }
        public       Feature Feature        { get { return _impl.Feature; } }
        public override bool UserCanSee     { get { return _impl.UserCanSee; } }
        public override bool UserCanEdit    { get { return _impl.UserCanEdit; } }

        [Obsolete("For designer ONLY", true)]
        protected ApplicationBaseViewComposite() : this(null) { }
        protected ApplicationBaseViewComposite(object businessObject, Feature featureAccess = null)
        {
            _impl = new ApplicationBaseViewImpl(this, businessObject, featureAccess);
            InitializeComponent();
        }
        readonly ApplicationBaseViewImpl _impl;
    }
}
