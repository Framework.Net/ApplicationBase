﻿using System;
using System.Windows.Forms;
using System.Diagnostics;

using TechnicalTools.UI;
using TechnicalTools.UI.DX;

using ApplicationBase.DAL;
using ApplicationBase.DAL.Users;

using ApplicationBase.Business;


namespace ApplicationBase.UI
{
    public interface IApplicationBaseView : IBaseControl
    {
        /// <summary>
        /// Owner is not always the parent 
        /// (example : the current parent can be a dockable floating form, but the owner is another form)
        /// </summary>
        Control Owner          { get; }
        object  BusinessObject { get; }
        Feature Feature        { get; }
    }

    class ApplicationBaseViewImpl : IApplicationBaseView
    {
        public Control             Parent         { get { return _base.Parent; } }
        public Control             Owner          { get; set; }
        public object              BusinessObject { get; } // If you want to change this, create a new view, dont remove read-only !

        public Feature Feature { get; }

        // User is allowed to see data (data are not sensible)
        public bool UserCanSee
        {
            get
            {
                return Feature != null
                    || (_manager?.GetAccessFor(Feature).HasFlag(eFeatureAccess.CanSee) ?? true);
            }
        }

        // User is allowed to insert / update / delete of data
        public bool UserCanEdit
        {
            get
            {
                return Feature != null
                    && (_manager?.GetAccessFor(Feature).HasFlag(eFeatureAccess.CanEdit) ?? true);
            }
        }

        // User can do some action (not editing data directly)
        public bool UserCanUse
        {
            get
            {
                return Feature != null
                    && (_manager?.GetAccessFor(Feature).HasFlag(eFeatureAccess.CanUse) ?? true);
            }
        }

        internal ApplicationBaseViewImpl(XtraUserControlBase ctlToImpl, object businessObject, Feature featureAccess = null)
        {
            _base = ctlToImpl;
            BusinessObject = businessObject;
            _manager = DesignTimeHelper.IsInDesignMode ? null : BusinessSingletons.Instance.GetAuthenticationManager();
            Feature = featureAccess ?? Feature.AllowEverybody;
            _base.Load += ApplicationBaseView_Load;
            _base.Disposed += OnDisposed;
        }
        readonly AuthenticationManager _manager;
        XtraUserControlBase _base { get; }

        void ApplicationBaseView_Load(object sender, EventArgs e)
        {
            if (!DesignTimeHelper.IsInDesignMode)
                _manager.CurrentUserChanged += _base.RefreshAccesses;
            _base.RefreshAccesses();
        }

        void OnDisposed(object sender, EventArgs e)
        {
            if (!DesignTimeHelper.IsInDesignMode)
                _manager.CurrentUserChanged -= _base.RefreshAccesses;

            var st = new StackTrace(1, true);
            _disposedAt = st.ToString();
        }
        static string _disposedAt; // To debug a rare case where the business object are disposed but still in use (should be impossible but with thread it is...)
    }
}
