﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Forms;
using TechnicalTools.UI.DX.Helpers;

using ApplicationBase.DAL;
using ApplicationBase.DAL.Users;
using ApplicationBase.Business;
using ApplicationBase.Business.FileImporting;


namespace ApplicationBase.UI
{
    public partial class AnyDataFileAnalyserView : ApplicationBaseView
    {
        AnyDataFileReader OriginalReader { get { return (AnyDataFileReader)BusinessObject; } }
        AnyDataFileReader _reader;

        GridViewDataSourceFlattenizer _flattenizer;

        [Obsolete("For designer only", true)]
        protected AnyDataFileAnalyserView()
        : this(null)
        {
        }
        public AnyDataFileAnalyserView(AnyDataFileReader reader, Feature feature = null)
         : base(reader, feature ?? Feature.AllowEverybody)
        {
            _reader = reader;
            InitializeComponent();

            if (DesignTimeHelper.IsInDesignMode)
                return;

            Text = "Any Data File Analyser";
            lblUserStatement.Parent = gcData;

            InitGenerationCodeUI();
        }

        protected virtual void InitGenerationCodeUI()
        {
            gvData.PopupMenuShowing += gvData_PopupMenuShowing;
            gvData.MasterRowExpanded += gvData_MasterRowExpanded;
        }

        void gvData_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            var mnu = new DXMenuItem("Generate code", (_, __) =>
            {
                var files = _reader.GenerateSourceCode();
                foreach (var file in files)
                {
                    var frm = new MemoForm(file.Value, file.Key);
                    frm.Memo.Font = new Font("Courier New", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
                    frm.Size = new Size(Width * 80 / 100, Height * 80 / 100);
                    frm.Show(this);
                }
            })
            {
                Visible = BusinessSingletons.Instance.GetAuthenticationManager().GetAccessFor(Feature.CanAccessExperimentalParts).HasFlag(eFeatureAccess.CanSee),
                Enabled = _reader?.GenerateSourceCode != null && gcData.DataSource != null
            };
            e.Menu.Items.Insert(0, mnu);
            if (e.Menu.Items.Count >= 2)
                e.Menu.Items[1].BeginGroup = true;
        }

        void AnyDataFileAnalyserView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            gvData.OptionsView.ColumnAutoWidth = false;
            gvData.OptionsView.ShowAutoFilterRow = true;
            gvData.OptionsView.AllowCellMerge = true;

            gvData.LayoutManager.ShowLayoutMenu = false;

            InitDragImport();
        }

        private void gvData_MasterRowExpanded(object sender, CustomMasterRowEventArgs e)
        {
            var subView = (GridView)gvData.GetDetailView(e.RowHandle, e.RelationIndex);
            CustomizeGridView(subView);
        }
        static void CustomizeGridView(GridView view)
        {
            view.OptionsBehavior.Editable = false;
            view.OptionsView.ColumnAutoWidth = false;
            view.OptionsView.AllowCellMerge = true;
            view.OptionsCustomization.AllowRowSizing = true;
            GridViewHelper.SetDefaultColumnFormatting(view);
            GridViewHelper.HideTechnicalProperties(view);
            view.BestFitMaxRowCount = 100;
            view.BestFitColumns();
        }

        #region Drag And Drop

        void InitDragImport()
        {
            gcData.AllowDrop = true;
            gcData.DragEnter += gcImports_DragEnter;
            gcData.DragDrop += gcImports_DragDrop;
        }

        void gcImports_DragEnter(object sender, DragEventArgs e)
        {
            ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                    e.Effect = DragDropEffects.Copy;
            });
        }

        async void gcImports_DragDrop(object sender, DragEventArgs e)
        {
            await ImportAllDroppedFiles(e);
        }
        async Task ImportAllDroppedFiles(DragEventArgs e)
        {
            string[] filepaths = null;

            if (!this.ShowBusyWhileDoingUIWorkInPlace(pr =>
            {
                filepaths = (string[])e.Data.GetData(DataFormats.FileDrop);
            }))
                return;

            var notFoundFiles = new List<string>();

            Debug.Assert((_reader.DisplayedInterpretation?.DataSource == null) == (gcData.DataSource == null));
            AnyDataFileReader reader = _reader.DisplayedInterpretation?.DataSource == null ? _reader : null;
            AnyDataFileAnalyserView view = gcData.DataSource == null ? this : null;
            foreach (string filepath in filepaths)
            {
                reader = reader ?? OriginalReader.CreateNew();

                // ReSharper disable once AccessToModifiedClosure
                var interpretations = await BusyForm.ShowBusyWhileDoing(this, $"Trying to read file \"{Path.GetFileName(filepath)}\"", pr => reader.GetDataSourceFrom(filepath, pr));

                if (interpretations.Count == 0)
                    notFoundFiles.Add(filepath);
                else
                {
                    var interpretation = interpretations.Last();
                    if (interpretations.Count > 1)
                    {
                        var title = "Multiple interpretation possible for this file";
                        var message = "Select the interpretation you want to explore...";
                        if (DialogResult.Cancel == ItemPicker.ShowDialog(this, interpretations, i => i.Name, ref interpretation, message, title))
                            return;
                    }

                    if (interpretation.Importer.WarningsOnRead.Count > 0)
                    {
                        var frm = new MemoForm(interpretation.Importer.WarningsOnRead.Join(Environment.NewLine),
                                               "Parsing successful, but please note that...");
                        frm.Memo.ReadOnly = true;
                        frm.ShowIcon = false;
                        frm.BestFit();
                        frm.Show(FindForm());
                    }
                    reader.Accept(interpretation);
                    view = view ?? (AnyDataFileAnalyserView)UIControler.Instance.ShowObject(reader);
                    Initialize(reader, view, filepath);

                    view = null;
                    reader = null;
                }
            }

            if (notFoundFiles.Any())
                XtraMessageBox.Show(this, "Files not recognised:" + Environment.NewLine + notFoundFiles.Select(f => "  - " + f).Join(Environment.NewLine) + Environment.NewLine +
                                          Environment.NewLine +
                                          "Ask IT team to handle these files",
                                          "File analysis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        static void Initialize(AnyDataFileReader reader, AnyDataFileAnalyserView view, string filepath)
        {
            if (view._flattenizer != null)
                view._flattenizer.Uninstall();
            view.gcData.DataSource = reader.DisplayedInterpretation.DataSource;
            foreach (GridColumn col in view.gvData.Columns)
                if (reader.DisplayedInterpretation.Captions != null &&
                    reader.DisplayedInterpretation.Captions.TryGetValue(col.FieldName, out string caption))
                    col.Caption = caption;
            // TODO : display reader.DisplayedInterpretation.SuperCaptions
            var gv = view.gvData;
            CustomizeGridView(gv);
            view.Text = "Analysing: " + Path.GetFileName(filepath);
            view.lblUserStatement.Visible = false;
            view._flattenizer = new GridViewDataSourceFlattenizer(gv, reader.DisplayedInterpretation.ChildrenDescriptions);
            view._flattenizer.Install(reader.DisplayedInterpretation.DataSource);

            GridViewHelper.SetDefaultColumnFormatting(gv);
            view.BeginInvoke((Action)(() =>
            {
                var value = gv.BestFitMaxRowCount;
                gv.BestFitMaxRowCount = 100;
                gv.BestFitColumns();
                gv.BestFitMaxRowCount = value;
            }));
        }

        #endregion
    }
}
