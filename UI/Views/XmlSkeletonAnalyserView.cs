﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.Xml;
using System.Xml.Linq;

using DevExpress.Utils.Menu;

using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Folding;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Forms;

using ApplicationBase.DAL.Users;
using ApplicationBase.Business.FileImporting;


namespace ApplicationBase.UI
{
    public partial class XmlSkeletonAnalyserView : ApplicationBaseView
    {
        XmlSkeletonAnalyser Analyser { get { return (XmlSkeletonAnalyser)BusinessObject; } }

        XDocument _docSkeleton;

        [Obsolete("For designer only", true)]
        protected XmlSkeletonAnalyserView()
        : this(null)
        {
        }
        public XmlSkeletonAnalyserView(XmlSkeletonAnalyser analyser, Feature feature = null)
         : base(analyser, feature ?? Feature.AllowEverybody)
        {
            InitializeComponent();

            if (DesignTimeHelper.IsInDesignMode)
                return;

            Text = "Xml Skeleton Analyser";
            InitEditor();
            _txtCode.MouseRightButtonDown += _txtCode_MouseRightButtonDown;
        }

        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();

            lblUserStatement.Visible = _txtCode.Text.Length == 0;
        }
        #region XML Editor

        private void InitEditor()
        {
            _txtCode = new ICSharpCode.AvalonEdit.TextEditor
            {
                IsReadOnly = true
            };
            wpfHoster.Child = _txtCode;
            ConfigureAvalonEditor();
        }
        ICSharpCode.AvalonEdit.TextEditor _txtCode;

        void ConfigureAvalonEditor()
        {
            _txtCode.ShowLineNumbers = true;
            _txtCode.TextArea.Caret.PositionChanged += Caret_PositionChanged;
            _txtCode.TextArea.SelectionChanged += Caret_PositionChanged;

            // CONFIGURE HIGHLIGHT
            // Liste des resource existante (ensemble de regles pour la coloration) https://github.com/icsharpcode/AvalonEdit/tree/master/ICSharpCode.AvalonEdit/Highlighting/Resources
            // Pour programmer ca par le code : http://www.kellen.tech/questions/983772/adding-syntax-highlighting-rules-to-avalonedit-programmatically
            using (Stream s = _txtCode.GetType().Assembly.GetManifestResourceStream("ICSharpCode.AvalonEdit.Highlighting.Resources.XML-Mode.xshd"))
            {
                Debug.Assert(s != null, nameof(s) + " != null");
                using (XmlTextReader reader = new XmlTextReader(s))
                {
                    _txtCode.SyntaxHighlighting = HighlightingLoader.Load(reader, HighlightingManager.Instance);
                }
            }

            // CONFIGURE CODE FOLDING
            // officla doc: http://avalonedit.net/documentation/html/440df648-413e-4f42-a28b-6b2b0e9b1084.htm
            // to do folding manually : https://stackoverflow.com/questions/5246467/bracefolding-in-avalonedit
            _foldingManager = FoldingManager.Install(_txtCode.TextArea);
            _foldingStrategy = new XmlFoldingStrategy();
            UpdateCodeFolding();
        }
        FoldingManager _foldingManager;
        XmlFoldingStrategy _foldingStrategy;

        // To call regularly to update folding
        void UpdateCodeFolding()
        {
            _foldingStrategy.UpdateFoldings(_foldingManager, _txtCode.Document);
        }

        private void Caret_PositionChanged(object sender, EventArgs e)
        {
            const string space = "    ";
            tslPosition.Text = "Ln : " + _txtCode.TextArea.Caret.Line
                             + space
                             + "Col : " + _txtCode.TextArea.Caret.Column
                             + space
                             + "Sel : " + _txtCode.TextArea.Selection.Length + " | " + Math.Abs(_txtCode.TextArea.Selection.EndPosition.Line - _txtCode.TextArea.Selection.StartPosition.Line);
        }
        #endregion XML Editor


        void XmlSkeletonAnalyserView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            InitDragImport();
        }

        #region Drag And Drop

        void InitDragImport()
        {
            wpfHoster.AllowDrop = true;
            wpfHoster.DragEnter += wpfHoster_DragEnter;
            wpfHoster.DragDrop += wpfHoster_DragDrop;
            lblUserStatement.AllowDrop = true;
            lblUserStatement.DragEnter += wpfHoster_DragEnter;
            lblUserStatement.DragDrop += wpfHoster_DragDrop;
        }

        void wpfHoster_DragEnter(object sender, DragEventArgs e)
        {
            ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                    e.Effect = DragDropEffects.Copy;
            });
        }

        // Note drop does not work for ElementHoster (ie: xpfHoster)
        // Follow indication with CaptureMouse here : https://stackoverflow.com/a/5304976/294998
        void wpfHoster_DragDrop(object sender, DragEventArgs e)
        {
            this.ShowBusyWhileDoing("Analysing...", pr =>
                {
                    string[] filePaths = null;
                    try
                    {
                        filePaths = (string[])e.Data.GetData(DataFormats.FileDrop);
                    }
                    catch (Exception ex)
                    {
                        ex.EnrichDiagnosticWith("Dropped items are not files", eExceptionEnrichmentType.UserUnderstandable, true);
                    }
                        
                    return Analyser.BuildSkeletonFromFiles(filePaths, pr);
                })
                .ContinueWithUIWorkOnSuccess(docSkeleton =>
                {
                    _docSkeleton = docSkeleton;
                    // Because we rely on the fact that all parsing metadata (line number & position of tags) are sync on texteditor and _docSkeleton
                    // we have to convert XDocument to string by preserving xmlprolog 
                    // This way we preserve shifting of 1 for line number
                    _txtCode.Text = _docSkeleton.ToStringWithProlog();
                    UpdateCodeFolding();
                    RefreshEnabilitiesAndVisibilities();
                });
        }


        string GetWordAtPosition(ICSharpCode.AvalonEdit.TextViewPosition caretPos)
        {
            var line = caretPos.Line;
            var column = caretPos.Column;
            string wordHoverAfter = string.Empty;
            var offset = _txtCode.Document.GetOffset(line, column);
            while (offset < _txtCode.Document.TextLength)
            {
                var c = _txtCode.Document.GetText(offset, 1)[0];
                if (!char.IsLetterOrDigit(c))
                    break;
                wordHoverAfter = wordHoverAfter + c;
                offset++;
            }

            string wordHoverBefore = string.Empty;
            offset = _txtCode.Document.GetOffset(line, column);
            offset--;
            while (offset >= 0)
            {
                var c = _txtCode.Document.GetText(offset, 1)[0];
                if (!char.IsLetterOrDigit(c))
                    break;
                wordHoverBefore = c + wordHoverBefore;
                offset--;
            }

            string wordHover = wordHoverBefore + wordHoverAfter;
            return wordHover;
        }

        #endregion

        private void _txtCode_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(_txtCode);
            var caretPos = _txtCode.GetPositionFromPoint(pos);
            if (caretPos.HasValue)
            {
                _txtCode.TextArea.Caret.Position = caretPos.Value;
                var wordHovered = GetWordAtPosition(caretPos.Value);
                if (!string.IsNullOrWhiteSpace(wordHovered))
                {
                    // Find the related xml node
                    Debug.Assert(_txtCode.IsReadOnly); // so _docSkeleton is the good reflect of what user see
                    var xmlNode = _docSkeleton.Root.Concat(_docSkeleton.Root.Descendants())
                                              .Where(node => (node as IXmlLineInfo).LineNumber == caretPos.Value.Line
                                                          && node.Name.LocalName == wordHovered)
                                              .SingleOrDefault();
                    if (xmlNode != null)
                    {
                        var menu = new DXPopupMenu();
                        menu.Items.Add(new DXMenuItem("Generate SQL table script for this node",
                                        (_, __) =>
                                        {
                                            this.ShowBusyWhileDoing("Generating...", pr => Analyser.GenerateSqlTableScriptForXmlNode(xmlNode))
                                                .ContinueWithUIWorkOnSuccess(tableHierarchyDef =>
                                                {
                                                    var frm = new MemoForm(tableHierarchyDef.ToSqlScript(true))
                                                    {
                                                        Text = $"Sql table script for Node \"{xmlNode.Name.LocalName};\""
                                                    };
                                                    frm.Show(this);
                                                });
                                        }));
                        menu.Items.Add(new DXMenuItem("Generate XML Parser in C# for this node/file",
                                        (_, __) =>
                                        {
                                            this.ShowBusyWhileDoing("Generating...", pr => Analyser.GenerateParsingCodeCSharp(Analyser.GenerateSqlTableScriptForXmlNode(xmlNode), false))
                                                .ContinueWithUIWorkOnSuccess(parserCode =>
                                                {
                                                    var frm = new MemoForm(parserCode)
                                                    {
                                                        Text = $"Sql table script for Node \"{xmlNode.Name.LocalName};\""
                                                    };
                                                    frm.Show(this);
                                                });
                                        }));
                        var spos = _txtCode.PointToScreen(pos);
                        var thisPos = PointToClient(new Point((int)spos.X, (int)spos.Y));
                        menu.ShowPopup(this, thisPos);
                    }
                }
            }
        }
    }
}
