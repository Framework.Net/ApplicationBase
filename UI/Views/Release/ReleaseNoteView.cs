﻿using System;
using System.Drawing;
using System.IO;
using System.ComponentModel;

using TechnicalTools.Diagnostics;

using ApplicationBase.DAL;
using ApplicationBase.DAL.Users;
using ApplicationBase.Common;
using ApplicationBase.Deployment.Data;
using ApplicationBase.Business;


namespace ApplicationBase.UI
{
    [ToolboxItem(true)]
    public partial class ReleaseNoteView : ApplicationBaseView
    {
        [Obsolete("For designer only")]
        protected ReleaseNoteView() : this(new ReleaseNoteWithContent() { Version = new Version(2,1), HasGIF = true, ExplainingGIF = GifSampleBytes }) { }
        public static readonly string GifSampleAsBase64 = "R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="; // from https://stackoverflow.com/questions/6018611
        public static readonly byte[] GifSampleBytes = Convert.FromBase64String(GifSampleAsBase64);


        public ReleaseNoteView(ReleaseNoteWithContent note)
            : base(note, Feature.AllowEverybody)
        {
            InitializeComponent();
            if (note == null)
                return;
            Text = note.Version.ToString();
            meReleaseNote.Text = note.Note;
            

            pnlPlayerContainer.Visible = false;
            if (note.ExplainingGIF != null)
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
                {
                    gifPlayer.GIF = Image.FromStream(new MemoryStream(note.ExplainingGIF));
                    pnlPlayerContainer.MinimumSize = new Size(gifPlayer.MinimumSize.Width + pnlPlayerContainer.Margin.Left + pnlPlayerContainer.Margin.Right, 
                                                              gifPlayer.MinimumSize.Height + pnlPlayerContainer.Margin.Top + pnlPlayerContainer.Margin.Bottom);
                    pnlPlayerContainer.Height = gifPlayer.Height;
                    pnlPlayerContainer.AutoScroll = true;
                    // if no problem with image we show player
                    pnlPlayerContainer.Visible = note.ExplainingGIF != null;
                });
        }

        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();
            var mgr = BusinessSingletons.Instance.GetAuthenticationManager();
            var userCanEdit = mgr.GetAccessFor(Feature.CanDeployApplication).HasFlag(eFeatureAccess.CanEdit);
            meReleaseNote.Properties.ReadOnly = !userCanEdit;
            btnSave.Visible = btnSave.Enabled = userCanEdit;
        }

        void pnlPlayerContainer_Resize(object sender, EventArgs e)
        {
            // Center the video inside black background
            gifPlayer.Left = Math.Max(0, (Width - gifPlayer.Width) / 2);
        }

        void gifPlayer_MouseEnter(object sender, EventArgs e)
        {
            if (gifPlayer.Visible && !_playFirstTime)
            {
                gifPlayer.Play();
                _playFirstTime = true;
            }
        }
        bool _playFirstTime;

        private void btnSave_Click(object sender, EventArgs e)
        {
            var note = (ReleaseNoteWithContent)BusinessObject;
            note.Note = meReleaseNote.Text.Trim();
            DB.Dao_Base.UpdateToDatabase(note, n => n.Note);
        }
    }
}
