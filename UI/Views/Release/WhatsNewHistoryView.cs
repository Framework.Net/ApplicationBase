﻿using System;
using System.Windows.Forms;

using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Helpers;
using ApplicationBase.Deployment.Data;

using ApplicationBase.DAL.Users;

using ApplicationBase.Business;


namespace ApplicationBase.UI
{
    public partial class WhatsNewHistoryView : ApplicationBaseView
    {
        ReleaseNotifier Notifier { get { return (ReleaseNotifier)BusinessObject; } }

        public WhatsNewHistoryView(ReleaseNotifier notifier)
            : base(notifier, Feature.AllowEverybody)
        {
            InitializeComponent();
            Text = $"Release notes of {Application.ProductName}";

            if (DesignTimeHelper.IsInDesignMode)
                return;
        }
        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();
        }

        void DeployView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            gvReleaseNotes.Columns.AddVisible(nameof(ReleaseNote.Version));
            var colNote = gvReleaseNotes.Columns.AddVisible(nameof(ReleaseNote.Note));
            colNote.ColumnEdit = gvReleaseNotes.CreateMultilineRepositoryEdit();
            gvReleaseNotes.Columns.AddVisible(nameof(ReleaseNote.HasGIF));

            gvReleaseNotes.RowItemDoubleClick += GvReleaseNotes_RowItemDoubleClick;

            if (BusinessSingletons.Instance.GetAuthenticationManager().CurrentUser.IsAdmin)
            {
                var colTechnicalNote = gvReleaseNotes.Columns.AddVisible(nameof(ReleaseNote.NoteTechnical));
                colTechnicalNote.ColumnEdit = gvReleaseNotes.CreateMultilineRepositoryEdit();
                gvReleaseNotes.Columns.AddVisible(nameof(ReleaseNote.IsPublic));
            }
            BeginInvoke((Action)RefreshData);
        }

        void RefreshData()
        {
            FindForm().ShowBusyWhileDoing("Getting release notes", pr => Notifier.Load())
                .ContinueWithUIWorkOnSuccess(() =>
                {
                    gcReleaseNotes.DataSource = Notifier.Notes;
                    gvReleaseNotes.BestFitMaxRowCount = 50;
                    gvReleaseNotes.BestFitColumns();
                });
        }

        void GvReleaseNotes_RowItemDoubleClick(object sender, RowItemDoubleClickEventArgs e)
        {
            var note = e.Item as ReleaseNote;
            UIControler.Instance.ShowObject(note);
        }
    }
}