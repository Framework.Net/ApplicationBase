﻿namespace ApplicationBase.UI
{
    partial class WhatsNewHistoryView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcReleaseNotes = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvReleaseNotes = new TechnicalTools.UI.DX.EnhancedGridView();
            this.lytMain = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gcLogs_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.gcReleaseNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReleaseNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytMain)).BeginInit();
            this.lytMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLogs_LayoutItem)).BeginInit();
            this.SuspendLayout();
            // 
            // gcReleaseNotes
            // 
            this.gcReleaseNotes.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcReleaseNotes.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcReleaseNotes.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcReleaseNotes.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcReleaseNotes.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcReleaseNotes.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcReleaseNotes.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcReleaseNotes.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcReleaseNotes.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcReleaseNotes.EmbeddedNavigator.Buttons.PrevPage.Enabled = false;
            this.gcReleaseNotes.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcReleaseNotes.EmbeddedNavigator.TextStringFormat = "Line {0} of {1} (visible) of {2} (available)";
            this.gcReleaseNotes.Location = new System.Drawing.Point(12, 12);
            this.gcReleaseNotes.MainView = this.gvReleaseNotes;
            this.gcReleaseNotes.Name = "gcReleaseNotes";
            this.gcReleaseNotes.Size = new System.Drawing.Size(838, 467);
            this.gcReleaseNotes.TabIndex = 0;
            this.gcReleaseNotes.UseEmbeddedNavigator = true;
            this.gcReleaseNotes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReleaseNotes});
            // 
            // gvReleaseNotes
            // 
            this.gvReleaseNotes.GridControl = this.gcReleaseNotes;
            this.gvReleaseNotes.Name = "gvReleaseNotes";
            this.gvReleaseNotes.OptionsBehavior.ReadOnly = true;
            this.gvReleaseNotes.OptionsSelection.MultiSelect = true;
            this.gvReleaseNotes.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gvReleaseNotes.OptionsView.ColumnAutoWidth = false;
            this.gvReleaseNotes.OptionsView.ShowAutoFilterRow = true;
            this.gvReleaseNotes.OptionsView.ShowFooter = true;
            // 
            // lytMain
            // 
            this.lytMain.Controls.Add(this.gcReleaseNotes);
            this.lytMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lytMain.Location = new System.Drawing.Point(0, 0);
            this.lytMain.Name = "lytMain";
            this.lytMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1016, 262, 250, 350);
            this.lytMain.Root = this.layoutControlGroup1;
            this.lytMain.Size = new System.Drawing.Size(862, 491);
            this.lytMain.TabIndex = 1;
            this.lytMain.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.gcLogs_LayoutItem});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(862, 491);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // gcLogs_LayoutItem
            // 
            this.gcLogs_LayoutItem.Control = this.gcReleaseNotes;
            this.gcLogs_LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.gcLogs_LayoutItem.Name = "gcLogs_LayoutItem";
            this.gcLogs_LayoutItem.Size = new System.Drawing.Size(842, 471);
            this.gcLogs_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.gcLogs_LayoutItem.TextVisible = false;
            // 
            // WhatsNewHistoryView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lytMain);
            this.Name = "WhatsNewHistoryView";
            this.Size = new System.Drawing.Size(862, 491);
            this.Load += new System.EventHandler(this.DeployView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcReleaseNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReleaseNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytMain)).EndInit();
            this.lytMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLogs_LayoutItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.DX.EnhancedGridControl gcReleaseNotes;
        private TechnicalTools.UI.DX.EnhancedGridView gvReleaseNotes;
        private DevExpress.XtraLayout.LayoutControl lytMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem gcLogs_LayoutItem;
    }
}
