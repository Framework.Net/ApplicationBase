﻿namespace ApplicationBase.UI
{
    partial class ReleaseNoteView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.meReleaseNote = new TechnicalTools.UI.DX.Controls.MemoEdit();
            this.gifPlayer = new TechnicalTools.UI.Controls.GIFPlayer();
            this.pnlPlayerContainer = new DevExpress.XtraEditors.PanelControl();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.meReleaseNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPlayerContainer)).BeginInit();
            this.pnlPlayerContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // meReleaseNote
            // 
            this.meReleaseNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meReleaseNote.Location = new System.Drawing.Point(0, 0);
            this.meReleaseNote.Name = "meReleaseNote";
            this.meReleaseNote.Size = new System.Drawing.Size(664, 348);
            this.meReleaseNote.TabIndex = 0;
            // 
            // gifPlayer
            // 
            this.gifPlayer.Location = new System.Drawing.Point(97, 5);
            this.gifPlayer.Name = "gifPlayer";
            this.gifPlayer.Size = new System.Drawing.Size(445, 179);
            this.gifPlayer.TabIndex = 1;
            this.gifPlayer.MouseEnter += new System.EventHandler(this.gifPlayer_MouseEnter);
            // 
            // pnlPlayerContainer
            // 
            this.pnlPlayerContainer.Appearance.BackColor = System.Drawing.Color.Black;
            this.pnlPlayerContainer.Appearance.Options.UseBackColor = true;
            this.pnlPlayerContainer.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.pnlPlayerContainer.Controls.Add(this.gifPlayer);
            this.pnlPlayerContainer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlPlayerContainer.Location = new System.Drawing.Point(0, 348);
            this.pnlPlayerContainer.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.pnlPlayerContainer.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlPlayerContainer.Name = "pnlPlayerContainer";
            this.pnlPlayerContainer.Size = new System.Drawing.Size(664, 184);
            this.pnlPlayerContainer.TabIndex = 2;
            this.pnlPlayerContainer.Resize += new System.EventHandler(this.pnlPlayerContainer_Resize);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(570, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ReleaseNoteView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.meReleaseNote);
            this.Controls.Add(this.pnlPlayerContainer);
            this.Name = "ReleaseNoteView";
            this.Size = new System.Drawing.Size(664, 532);
            ((System.ComponentModel.ISupportInitialize)(this.meReleaseNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlPlayerContainer)).EndInit();
            this.pnlPlayerContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.DX.Controls.MemoEdit meReleaseNote;
        private TechnicalTools.UI.Controls.GIFPlayer gifPlayer;
        private DevExpress.XtraEditors.PanelControl pnlPlayerContainer;
        private DevExpress.XtraEditors.SimpleButton btnSave;
    }
}