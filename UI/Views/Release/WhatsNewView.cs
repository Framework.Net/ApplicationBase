﻿using System;
using System.Linq;
using System.Windows.Forms;

using TechnicalTools.Diagnostics;
using TechnicalTools.UI.DX;
using TechnicalTools.UI;

using ApplicationBase.DAL.Users;

using ApplicationBase.Business;


namespace ApplicationBase.UI
{
    public partial class WhatsNewView : ApplicationBaseView
    {
        ReleaseNotifier.UnseenReleaNoteByUser ReleaseNotes { get { return (ReleaseNotifier.UnseenReleaNoteByUser)BusinessObject; } }

        [Obsolete("For designer only")]
        protected WhatsNewView() : this(null, null) { }

        public WhatsNewView(ReleaseNotifier.UnseenReleaNoteByUser notes, ConfigOfUser config)
            : base(notes, Feature.AllowEverybody)
        {
            _config = config;
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            Text = $"What's new in {Application.ProductName} ?";
            btnOk.Enabled = _config != null;
        }
        readonly ConfigOfUser _config;

        void WhatsNewView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            foreach (var note in ReleaseNotes.AsEnumerable().Reverse())
                tabContainer.DockAsDefault(new ReleaseNoteView(note));
        }

        void btnRemindMeLater_Click(object sender, EventArgs e)
        {
            CancelAndClose();
        }

        void btnOk_Click(object sender, EventArgs e)
        {
            ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => ReleaseNotes.SetLastReleaseNoteSeen(ReleaseNotes.OrderBy(rn => rn.Id).Last()));
            AcceptAndClose();
        }
    }
}
