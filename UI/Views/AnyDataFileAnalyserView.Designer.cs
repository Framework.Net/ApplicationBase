﻿namespace ApplicationBase.UI
{
    partial class AnyDataFileAnalyserView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcData = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvData = new TechnicalTools.UI.DX.EnhancedBandedGridView();
            this.bLevel0 = new TechnicalTools.UI.DX.EnhancedGridBand();
            this.lblUserStatement = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gcData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).BeginInit();
            this.SuspendLayout();
            // 
            // gcData
            // 
            this.gcData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcData.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcData.EmbeddedNavigator.TextStringFormat = "Record n°{0} of {1} (visible) of {2} (total)";
            this.gcData.Location = new System.Drawing.Point(0, 0);
            this.gcData.MainView = this.gvData;
            this.gcData.Name = "gcData";
            this.gcData.Size = new System.Drawing.Size(860, 518);
            this.gcData.TabIndex = 0;
            this.gcData.UseEmbeddedNavigator = true;
            this.gcData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvData});
            // 
            // gvData
            // 
            this.gvData.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.bLevel0});
            this.gvData.GridControl = this.gcData;
            this.gvData.Name = "gvData";
            // 
            // bLevel0
            // 
            this.bLevel0.Name = "bLevel0";
            this.bLevel0.VisibleIndex = 0;
            // 
            // lblUserStatement
            // 
            this.lblUserStatement.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblUserStatement.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblUserStatement.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.lblUserStatement.Appearance.Options.UseBackColor = true;
            this.lblUserStatement.Appearance.Options.UseFont = true;
            this.lblUserStatement.Location = new System.Drawing.Point(247, 266);
            this.lblUserStatement.Name = "lblUserStatement";
            this.lblUserStatement.Size = new System.Drawing.Size(340, 23);
            this.lblUserStatement.TabIndex = 1;
            this.lblUserStatement.Text = "Please, drag and drop any file(s) on grid";
            // 
            // AnyDataFileAnalyserView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblUserStatement);
            this.Controls.Add(this.gcData);
            this.MinimumSize = new System.Drawing.Size(500, 100);
            this.Name = "AnyDataFileAnalyserView";
            this.Size = new System.Drawing.Size(860, 518);
            this.Load += new System.EventHandler(this.AnyDataFileAnalyserView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TechnicalTools.UI.DX.EnhancedGridControl gcData;
        private DevExpress.XtraEditors.LabelControl lblUserStatement;
        private TechnicalTools.UI.DX.EnhancedBandedGridView gvData;
        private TechnicalTools.UI.DX.EnhancedGridBand bLevel0;
    }
}
