﻿namespace ApplicationBase.UI.Views.DataRelation
{
    partial class DataExplorerView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions buttonImageOptions1 = new DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataExplorerView));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.mappedFilterControl = new TechnicalTools.UI.DX.Controls.MappedFilterControl();
            this.gcData = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvData = new TechnicalTools.UI.DX.EnhancedBandedGridView();
            this.enhancedGridBand1 = new TechnicalTools.UI.DX.EnhancedGridBand();
            this.lueDatabaseType = new DevExpress.XtraEditors.LookUpEdit();
            this.lueTable = new DevExpress.XtraEditors.LookUpEdit();
            this.lueDbMappers = new ApplicationBase.UI.Controls.MapperSetSelector();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lytgrpSourceAndRequest = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lueTable_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.lueDatabase_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.lueDatabaseType_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterMain = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDatabaseType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytgrpSourceAndRequest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTable_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDatabase_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDatabaseType_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnLoad);
            this.layoutControl1.Controls.Add(this.mappedFilterControl);
            this.layoutControl1.Controls.Add(this.gcData);
            this.layoutControl1.Controls.Add(this.lueDatabaseType);
            this.layoutControl1.Controls.Add(this.lueTable);
            this.layoutControl1.Controls.Add(this.lueDbMappers);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(586, 402, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(852, 490);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(706, 51);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(122, 121);
            this.btnLoad.StyleController = this.layoutControl1;
            this.btnLoad.TabIndex = 6;
            this.btnLoad.Text = "Load !";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // mappedFilterControl
            // 
            this.mappedFilterControl.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.mappedFilterControl.Location = new System.Drawing.Point(36, 121);
            this.mappedFilterControl.Name = "mappedFilterControl";
            this.mappedFilterControl.PreventUserToChangeOperand = false;
            this.mappedFilterControl.Size = new System.Drawing.Size(654, 39);
            this.mappedFilterControl.TabIndex = 13;
            this.mappedFilterControl.Text = "mappedFilterControl1";
            // 
            // gcData
            // 
            this.gcData.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcData.EmbeddedNavigator.Buttons.PrevPage.Enabled = false;
            this.gcData.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcData.EmbeddedNavigator.TextStringFormat = "Item {0} of {1} (visible) of {2} (available)";
            this.gcData.Location = new System.Drawing.Point(12, 193);
            this.gcData.MainView = this.gvData;
            this.gcData.Name = "gcData";
            this.gcData.Size = new System.Drawing.Size(828, 285);
            this.gcData.TabIndex = 5;
            this.gcData.UseEmbeddedNavigator = true;
            this.gcData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvData});
            // 
            // gvData
            // 
            this.gvData.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.enhancedGridBand1});
            this.gvData.GridControl = this.gcData;
            this.gvData.Name = "gvData";
            this.gvData.OptionsView.ColumnAutoWidth = false;
            this.gvData.OptionsView.ShowAutoFilterRow = true;
            // 
            // enhancedGridBand1
            // 
            this.enhancedGridBand1.Name = "enhancedGridBand1";
            this.enhancedGridBand1.VisibleIndex = 0;
            // 
            // lueDatabaseType
            // 
            this.lueDatabaseType.Location = new System.Drawing.Point(118, 81);
            this.lueDatabaseType.Name = "lueDatabaseType";
            this.lueDatabaseType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lueDatabaseType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDatabaseType.Properties.NullText = "";
            this.lueDatabaseType.Size = new System.Drawing.Size(50, 20);
            this.lueDatabaseType.StyleController = this.layoutControl1;
            this.lueDatabaseType.TabIndex = 4;
            // 
            // lueTable
            // 
            this.lueTable.Location = new System.Drawing.Point(446, 81);
            this.lueTable.Name = "lueTable";
            this.lueTable.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lueTable.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTable.Properties.NullText = "";
            this.lueTable.Size = new System.Drawing.Size(244, 20);
            this.lueTable.StyleController = this.layoutControl1;
            this.lueTable.TabIndex = 4;
            // 
            // lueDbMappers
            // 
            this.lueDbMappers.Location = new System.Drawing.Point(260, 81);
            this.lueDbMappers.MaxSelectedItems = ((uint)(0u));
            this.lueDbMappers.Name = "lueDbMappers";
            this.lueDbMappers.Size = new System.Drawing.Size(143, 20);
            this.lueDbMappers.TabIndex = 12;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.lytgrpSourceAndRequest,
            this.splitterMain});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(852, 490);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gcData;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 181);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(832, 289);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // lytgrpSourceAndRequest
            // 
            this.lytgrpSourceAndRequest.BestFitWeight = 200;
            buttonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("buttonImageOptions1.Image")));
            this.lytgrpSourceAndRequest.CustomHeaderButtons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("See data network", true, buttonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", 0, true, null, true, false, true, null, -1)});
            this.lytgrpSourceAndRequest.ExpandButtonVisible = true;
            this.lytgrpSourceAndRequest.ExpandOnDoubleClick = true;
            this.lytgrpSourceAndRequest.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.lytgrpSourceAndRequest.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1,
            this.layoutControlItem4});
            this.lytgrpSourceAndRequest.Location = new System.Drawing.Point(0, 0);
            this.lytgrpSourceAndRequest.Name = "lytgrpSourceAndRequest";
            this.lytgrpSourceAndRequest.Size = new System.Drawing.Size(832, 176);
            this.lytgrpSourceAndRequest.Text = "Source && Request";
            this.lytgrpSourceAndRequest.CustomButtonClick += new DevExpress.XtraBars.Docking2010.BaseButtonEventHandler(this.lytgrpSourceAndRequest_CustomButtonClick);
            // 
            // lueTable_LayoutItem
            // 
            this.lueTable_LayoutItem.Control = this.lueTable;
            this.lueTable_LayoutItem.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.lueTable_LayoutItem.CustomizationFormText = "layoutControlItem1";
            this.lueTable_LayoutItem.Location = new System.Drawing.Point(376, 0);
            this.lueTable_LayoutItem.Name = "lueTable_LayoutItem";
            this.lueTable_LayoutItem.Size = new System.Drawing.Size(282, 24);
            this.lueTable_LayoutItem.Text = "Table:";
            this.lueTable_LayoutItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lueTable_LayoutItem.TextSize = new System.Drawing.Size(30, 13);
            this.lueTable_LayoutItem.TextToControlDistance = 4;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(371, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 24);
            // 
            // lueDatabase_LayoutItem
            // 
            this.lueDatabase_LayoutItem.Control = this.lueDbMappers;
            this.lueDatabase_LayoutItem.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.lueDatabase_LayoutItem.CustomizationFormText = "Partner Environments";
            this.lueDatabase_LayoutItem.Location = new System.Drawing.Point(141, 0);
            this.lueDatabase_LayoutItem.Name = "lueDatabase_LayoutItem";
            this.lueDatabase_LayoutItem.Size = new System.Drawing.Size(230, 24);
            this.lueDatabase_LayoutItem.Text = "Database Name:";
            this.lueDatabase_LayoutItem.TextSize = new System.Drawing.Size(80, 13);
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.FixedStyle = DevExpress.XtraLayout.SplitterItemFixedStyles.LeftTop;
            this.splitterItem2.Location = new System.Drawing.Point(136, 0);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(5, 24);
            // 
            // lueDatabaseType_LayoutItem
            // 
            this.lueDatabaseType_LayoutItem.Control = this.lueDatabaseType;
            this.lueDatabaseType_LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.lueDatabaseType_LayoutItem.Name = "lueDatabaseType_LayoutItem";
            this.lueDatabaseType_LayoutItem.Size = new System.Drawing.Size(136, 24);
            this.lueDatabaseType_LayoutItem.Text = "Database Type:";
            this.lueDatabaseType_LayoutItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lueDatabaseType_LayoutItem.TextSize = new System.Drawing.Size(77, 13);
            this.lueDatabaseType_LayoutItem.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.mappedFilterControl;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(658, 59);
            this.layoutControlItem3.Text = "Loading filter:";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(80, 13);
            // 
            // splitterMain
            // 
            this.splitterMain.AllowHotTrack = true;
            this.splitterMain.FixedStyle = DevExpress.XtraLayout.SplitterItemFixedStyles.LeftTop;
            this.splitterMain.Location = new System.Drawing.Point(0, 176);
            this.splitterMain.Name = "splitterMain";
            this.splitterMain.Size = new System.Drawing.Size(832, 5);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lueDatabaseType_LayoutItem,
            this.lueDatabase_LayoutItem,
            this.lueTable_LayoutItem,
            this.splitterItem1,
            this.splitterItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(682, 125);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnLoad;
            this.layoutControlItem4.Location = new System.Drawing.Point(682, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(126, 1000);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(126, 125);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(126, 125);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // DataExplorerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "DataExplorerView";
            this.Size = new System.Drawing.Size(852, 490);
            this.Load += new System.EventHandler(this.DataExplorerView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDatabaseType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytgrpSourceAndRequest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTable_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDatabase_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDatabaseType_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private TechnicalTools.UI.DX.EnhancedGridControl gcData;
        private DevExpress.XtraEditors.LookUpEdit lueDatabaseType;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem lueDatabaseType_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraEditors.LookUpEdit lueTable;
        private DevExpress.XtraLayout.LayoutControlItem lueTable_LayoutItem;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private TechnicalTools.UI.DX.EnhancedBandedGridView gvData;
        private TechnicalTools.UI.DX.EnhancedGridBand enhancedGridBand1;
        private ApplicationBase.UI.Controls.MapperSetSelector lueDbMappers;
        private DevExpress.XtraLayout.LayoutControlItem lueDatabase_LayoutItem;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private TechnicalTools.UI.DX.Controls.MappedFilterControl mappedFilterControl;
        private DevExpress.XtraLayout.LayoutControlGroup lytgrpSourceAndRequest;
        private DevExpress.XtraLayout.SplitterItem splitterMain;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
    }
}
