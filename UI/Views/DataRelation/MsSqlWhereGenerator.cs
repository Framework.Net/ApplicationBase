﻿using System;

using DevExpress.Data.Db;
using DevExpress.Data.Filtering;

using DataMapper;


namespace ApplicationBase.UI.Views.DataRelation
{
    // Code largely inspired from DevExpress.Data.Filtering.Helpers.MsSqlWhereGenerator
    public class MsSqlWhereGenerator : BaseWhereGenerator, ICriteriaVisitor<string>
    {
        public MsSqlWhereGenerator(DbMappedTable mtable, MsSqlFormatterHelper.MSSqlServerVersion sqlServerVersion, Func<OperandProperty, string> propertyFormatter = null)
        {
            _mtable = mtable;
            _sqlServerVersion = sqlServerVersion ?? new MsSqlFormatterHelper.MSSqlServerVersion(false, false, true, false);
            _propertyFormatter = propertyFormatter;
        }
        readonly DbMappedTable _mtable;
        readonly MsSqlFormatterHelper.MSSqlServerVersion _sqlServerVersion;
        readonly Func<OperandProperty, string> _propertyFormatter;


        string ICriteriaVisitor<string>.Visit(OperandValue theOperand)
        {
            object value = theOperand.Value;
            if (value == null)
                return "null";
            return MsSqlFormatterHelper.FormatConstant(value, _sqlServerVersion);
        }

        string ICriteriaVisitor<string>.Visit(BinaryOperator theOperator)
        {
            string leftOperand = Process(theOperator.LeftOperand);
            string rightOperand = Process(theOperator.RightOperand);
            return MsSqlFormatterHelper.FormatBinary(theOperator.OperatorType, leftOperand, rightOperand);
        }

        protected override string VisitInternal(FunctionOperator theOperator)
        {
            string text = MsSqlFormatterHelper.FormatFunction((object obj) => Process((CriteriaOperator)obj), theOperator.OperatorType, _sqlServerVersion, theOperator.Operands.ToArray());
            if (text != null)
                return text;
            string[] array = new string[theOperator.Operands.Count];
            for (int i = 0; i < theOperator.Operands.Count; i++)
                array[i] = Process(theOperator.Operands[i]);
            text = MsSqlFormatterHelper.FormatFunction(theOperator.OperatorType, _sqlServerVersion, array);
            if (text != null)
                return text;
            return base.VisitInternal(theOperator);
        }

        protected override string FormatOperandProperty(OperandProperty operand)
        {
            if (_propertyFormatter != null)
            {
                string text = _propertyFormatter(operand);
                if (text != null)
                    return text;
            }
            return DefaultFormatOperandProperty(operand, squareBrackets: true);
        }

        public string DefaultFormatOperandProperty(OperandProperty operand, bool squareBrackets)
        {
            var columnName = _mtable.AllFieldsByPropertyName[operand.PropertyName].ColumnName;
            string text = columnName ?? string.Empty;
            if (squareBrackets)
                return "[" + text.Replace("]", "]]") + "]";
            return "\"" + text.Replace("\"", "\"\"") + "\"";
        }
    }
}