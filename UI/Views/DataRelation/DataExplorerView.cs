﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Linq;
using System.Threading.Tasks;

using DevExpress.Data.Filtering;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.BandedGrid;

using TechnicalTools;
using TechnicalTools.Annotations;
using TechnicalTools.Logs;
using TechnicalTools.Tools;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Controls;

using DataMapper;

using ApplicationBase.DAL.Users;
using ApplicationBase.DAL.Dialoguing;
using ApplicationBase.Business;
using ApplicationBase.Business.DataRelation;
using ApplicationBase.Business.GraphVisualization.Graphviz;
using ApplicationBase.UI.Helpers;


namespace ApplicationBase.UI.Views.DataRelation
{
    public partial class DataExplorerView : ApplicationBaseView
    {
        DataExplorer Explorer { get { return (DataExplorer)BusinessObject; } }

        readonly BindingList<Dialogue> _dialogues = new BindingList<Dialogue>();

        [Obsolete("For designer only", true)]
        protected DataExplorerView()
            : this(null)
        {
        }
        public DataExplorerView(DataExplorer explorer)      
            : base (explorer, Feature.AllowEverybody)
        {
            InitializeComponent();

            if (DesignTimeHelper.IsInDesignMode)
                return;
            Text = "Data Explorer";

            _dbInfos = explorer.GetDatabaseInfo();
        }
        static readonly ILogger _log = LogManager.Default.GetLogger(typeof(DataExplorerView));
        readonly IReadOnlyDictionary<string, DataExplorer.MappersTypes> _dbInfos;

        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();

            btnLoad.Enabled = UserCanSee 
                           && CurrentRequest?.ItemType != null // for starting
                           && (CurrentRequest?.Mappers.Any() ?? false);
        }

        void DataExplorerView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            InitSourceSelection();
            gvData.MasterRowGetLevelDefaultView += TechnicalTools.UI.DX.Helpers.GridViewHelper.CreateDefaultEnhancedGridView_On_MasterRowGetLevelDefaultView;
        }

        #region Source Selection

        public DataExplorer.Request CurrentRequest
        {
            get
            {
                if (lueTable.Properties.DataSource == null)
                    return null;
                return new DataExplorer.Request()
                {
                    DbType = lueDatabaseType.SelectedValueAsObject<string>(null),
                    Mappers = lueDbMappers.SelectedMappers,
                    ItemType = lueTable.SelectedValueAsObject<Type>(null)
                };
            }
        }

        void InitSourceSelection()
        {
            lueDatabaseType.FillWithObjects(_dbInfos.Keys);

            if (_dbInfos.Keys.Any())
                lueDatabaseType.SelectedValueAsObjectIdSet(_dbInfos.Keys.First());
            else
                lueDatabaseType.UnselectAnyObject();
            lueDatabaseType.EditValueChanged += lueDatabaseType_EditValueChanged;
            lueDatabaseType_LayoutItem.Visibility = VisibleIf(_dbInfos.Keys.Count() > 1);
            lueDatabaseType_EditValueChanged(null, null);
        }
        void lueDatabaseType_EditValueChanged(object sender, EventArgs e)
        {
            var dbType = lueDatabaseType.SelectedValueAsObject<string>(null);

            lueDbMappers.SelectionChanged -= lueDatabase_SelectionChanged;
            var mappers = dbType == null ? new List<IDbMapperReadAccess>() : _dbInfos[dbType].Mappers;
            lueDbMappers.Fill(mappers);
            lueDbMappers.SelectionChanged += lueDatabase_SelectionChanged;
            lueDatabase_SelectionChanged(null, null);
        }

        void lueDatabase_SelectionChanged(object sender, EventArgs e)
        {
            var dbType = lueDatabaseType.SelectedValueAsObject<string>(null);
            var mappers = lueDbMappers.SelectedMappers;
            var tableType = lueTable.IsHandledByExtensions() ? lueTable.SelectedValueAsObject<Type>(null) : null;

            lueTable.EditValueChanged -= lueTable_EditValueChanged;
            var tableTypes = dbType == null ? new List<Type>() : _dbInfos[dbType].ProvidedTypes;
            var anyMapper = mappers.FirstOrDefault() ?? _dbInfos[dbType].Mappers.First();
            lueTable.FillWithObjects(tableTypes, t => anyMapper?.GetTableMapping(t).SchemaName + " - " + anyMapper.GetTableMapping(t).TableName);
            if (!tableTypes.Any())
                lueTable.UnselectAnyObject();
            else if (tableType != null && tableTypes.Contains(tableType))
                lueTable.SelectedValueAsObjectIdSet(tableType);
            else
                lueTable.SelectedValueAsObjectIdSet(tableTypes.First());

            lueTable.EditValueChanged += lueTable_EditValueChanged;
            lueTable_EditValueChanged(null, null);

            RefreshEnabilitiesAndVisibilities();
        }

        private void lueTable_EditValueChanged(object sender, EventArgs e)
        {
            gcData.DataSource = null;

            SetLayout(CurrentRequest);
        }

        #endregion Source Selection

        

        private void SetLayout(DataExplorer.Request request)
        {
            if (_lastType != null)
                _criteriaOperatorsByType[_lastType] = mappedFilterControl.FilterCriteria;

            gvData.Bands.Clear();
            gvData.Columns.Clear();
            mappedFilterControl.Init(request.ItemType, null);
            if (request.ItemType != null)
            {
                mappedFilterControl.FilterCriteria = _criteriaOperatorsByType.TryGetValueClass(request.ItemType);

                TechnicalTools.UI.DX.Helpers.GridViewHelper.PopulateColumnsFromDataAnnotations(gvData, request.ItemType);

                var virtualColumns = Explorer.GetVirtualColumns(request).ToList();
                virtualColumns = virtualColumns.Where(vc => vc.IsImportant).Reverse()
                         .Concat(virtualColumns.Where(vc => !vc.IsImportant)).ToList();
                foreach (var vProp in virtualColumns)
                {
                    var vCol = gvData.Columns.AddVisible("*virtual*" + vProp.Caption, vProp.Caption);
                    vCol.UnboundType = TechnicalTools.UI.DX.Helpers.GridViewHelper.ToDevExpressGridViewUnboundType(vProp.Type);
                    (vCol as IEnhancedGridColumn).UnboundValueGetter = (row, _) => vProp.GetValue(row);
                    gvData.Bands.First().Columns.Add((BandedGridColumn)vCol);
                    if (vProp.IsImportant)
                        vCol.InsertAfter(null);
                }
            }
            _lastType = request.ItemType;
        }
        Type _lastType;
        readonly Dictionary<Type, CriteriaOperator> _criteriaOperatorsByType = new Dictionary<Type, CriteriaOperator>();

        protected virtual long ResultLimit { get { return 1000000; } }

        void btnLoad_Click(object sender, EventArgs e)
        {
            var request = CurrentRequest;
            request.SqlWhereCondition = GetSqlWhereClause(request);

            var dataTypeName = BusinessNameAttribute.GetNameFor(request.ItemType) ?? request.ItemType.Name.Uncamelify();
            this.ShowBusyWhileDoing("Counting data flow items for " + dataTypeName + " to retrieve...",
                                    pr => Explorer.GetMatchingItemsCount(request, pr))
            .ContinueWithUIWorkOnSuccess(countsToLoad =>
            {
                var mgr = BusinessSingletons.Instance.GetAuthenticationManager();
                var count = countsToLoad.Values.DefaultIfEmpty(0).Sum();
                if (count > ResultLimit)
                    if (!mgr.CurrentUser.IsAdmin)
                    {
                        XtraMessageBox.Show($"There is exactly {count:n0} operations to retrieve, please narrow down your request!");
                        return;
                    }
                    else if (XtraMessageBox.Show($"There is more than {ResultLimit:n0} rows to retrieve ({count:n0}), are you sure to continue ?" + Environment.NewLine +
                                                  "It could be slow...", "Too much operations to retrieve?", MessageBoxButtons.YesNo) == DialogResult.No)
                        return;
                request.Mappers = countsToLoad.Keys;

                this.ShowBusyWhileDoing("Getting items...", pr => Explorer.Load(request, pr))
                    .ContinueWithLongUIWork(this, t =>
                    {
                        var results = t.Status == TaskStatus.RanToCompletion
                                   ? t.Result
                                   : new List<object>();
                        DisplayDataSource(request, results);
                        RefreshEnabilitiesAndVisibilities();
                    });
            });
        }

        string GetSqlWhereClause(DataExplorer.Request request)
        {
            var anyMapper = request.Mappers.First();
            var mtable = anyMapper.GetTableMapping(request.ItemType);
            var sqlGenerator = new MsSqlWhereGenerator(mtable, null, null);
            var sql = sqlGenerator.Process(mappedFilterControl.FilterCriteria);
            return sql;
        }

        void DisplayDataSource(DataExplorer.Request request, System.Collections.IList ds)
        {
            gvData.BeginUpdate();

            _revertGridViewConfig?.DynamicInvoke();

            // Set datasource to null to reset and avoid useless event to be raised when touching columns and bands  
            gcData.DataSource = null;

            // Recreate related band / columns, related to request selected by user  
            var itemType = request.ItemType;

            _revertGridViewConfig = GridView_ConfiguratorProxy.Instance.ConfigureFor(request.ItemType, gvData, Explorer.HierarchyProvider);

            
            gcData.DataSource = ds;

            CustomizeGridView?.Invoke();
            gvData.EndUpdate();
            RefreshEnabilitiesAndVisibilities();
        }

        protected event Action CustomizeGridView;
        Delegate _revertGridViewConfig;

        private void lytgrpSourceAndRequest_CustomButtonClick(object sender, DevExpress.XtraBars.Docking2010.BaseButtonEventArgs e)
        {
            Graph.CheckCanRender();
            var svgGraph = new SVGViewer();

            EnhancedXtraForm frm;
            if (Explorer.HierarchyProvider.HaveInterestingLegend)
            {
                var splitter = new SplitContainerControl
                {
                    CollapsePanel = SplitCollapsePanel.Panel1,
                    SplitterPosition = 200
                };
                var svgLegend = new SVGViewer
                {
                    ShowControls = false,
                    Dock = DockStyle.Fill
                };
                splitter.Panel1.Controls.Add(svgLegend);
                svgGraph.Dock = DockStyle.Fill;
                splitter.Panel2.Controls.Add(svgGraph);

                frm = EnhancedXtraForm.CreateWithInner(splitter, this);

                Graph legend = null;
                frm.Shown += (_, __) => 
                {
                    WaitControlGeneric.ShowOnControl(svgLegend, "Refreshing...", 
                                                    () =>
                                                    {
                                                        legend = Explorer.HierarchyProvider.GraphUniverseToGraphvizDigraphLegend();
                                                        return legend.ToSvg(preserveXmlDeclaration: false);
                                                    },
                                                    svg => svgLegend.Svg = svg);
                };
                svgLegend.PopupMenuShowing += (_, ee) =>
                {
                    ee.PopupMenu.AddToGroup(ePopupMenuHeaderType.Standard, new DXMenuItem("See GraphViz description",
                    (__, ___) => svgLegend.ShowInMemoForm(() => legend.ToDot(), "Legend GraphViz description")));
                };
            }
            else
                frm = EnhancedXtraForm.CreateWithInner(svgGraph, this);

            frm.Size = new System.Drawing.Size(Width * 95 / 100, Height * 95 / 100);
            frm.Text = "Data network (Technical view)";

            Graph network = null;
            Action<eLayoutEngine> refreshGraph = (eLayoutEngine layoutEngine) =>
            {
                WaitControlGeneric.ShowOnControl(svgGraph, "Generating...",
                    () =>
                    {
                        network = Explorer.HierarchyProvider.GraphUniverseToGraphvizDigraph(true, true, layoutEngine: layoutEngine);
                        return network.ToSvg();
                    },
                    svg => svgGraph.Svg = svg);
            };
            frm.Shown += (_, __) => refreshGraph(eLayoutEngine.Dot);
            frm.Show(this);

            svgGraph.PopupMenuShowing += (_, ee) =>
            {
                ee.PopupMenu.Items[0].BeginGroup = true;
                ee.PopupMenu.Items[1].BeginGroup = false;
                ee.PopupMenu.AddToGroup(ePopupMenuHeaderType.Standard, new DXMenuItem("See GraphViz description",
                (__, ___) => svgGraph.ShowInMemoForm(() => network.ToDot(), "GraphViz description")));
                ee.PopupMenu.AddToGroup(ePopupMenuHeaderType.Custom, new DXMenuItem("Display hierarchically", (__, ___) => refreshGraph(eLayoutEngine.Dot)));
                ee.PopupMenu.AddToGroup(ePopupMenuHeaderType.Custom, new DXMenuItem("Display organically", (__, ___) => refreshGraph(eLayoutEngine.Circo)));
            };
        }
    }
}