﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;

using DevExpress.Data.Db;
using DevExpress.Data.Filtering;
using DevExpress.Data.Filtering.Helpers;
using DevExpress.Xpo.DB.Helpers;


namespace ApplicationBase.UI.Views.DataRelation
{
    // Code largely inspired from DevExpress.Data.Filtering.Helpers.BaseWhereGenerator
    public abstract class BaseWhereGenerator : IClientCriteriaVisitor<string>, ICriteriaVisitor<string>
    {
        public string Process(CriteriaOperator operand)
        {
            if (operand.ReferenceEqualsNull())
                return string.Empty;
            return operand.Accept(this);
        }

        string IClientCriteriaVisitor<string>.Visit(OperandProperty theOperand)
        {
            return FormatOperandProperty(theOperand);
        }

        protected abstract string FormatOperandProperty(OperandProperty operand);

        string ICriteriaVisitor<string>.Visit(OperandValue theOperand)
        {
            return theOperand.ToString();
        }

        string ICriteriaVisitor<string>.Visit(BetweenOperator theOperator)
        {
            return Process(CriteriaOperator.And(new BinaryOperator(theOperator.TestExpression, theOperator.BeginExpression, BinaryOperatorType.GreaterOrEqual), new BinaryOperator(theOperator.TestExpression, theOperator.EndExpression, BinaryOperatorType.LessOrEqual)));
        }

        string ICriteriaVisitor<string>.Visit(InOperator theOperator)
        {
            string text = Process(theOperator.LeftOperand);
            StringBuilder stringBuilder = new StringBuilder();
            foreach (CriteriaOperator operand in theOperator.Operands)
            {
                if (stringBuilder.Length > 0)
                    stringBuilder.Append(',');
                stringBuilder.Append(Process(operand));
            }
            return string.Format(CultureInfo.InvariantCulture, "{0} in ({1})", new object[2]
            {
                text,
                stringBuilder.ToString()
            });
        }

        string ICriteriaVisitor<string>.Visit(GroupOperator theOperator)
        {
            StringCollection stringCollection = new StringCollection();
            foreach (CriteriaOperator operand in theOperator.Operands)
            {
                string text = Process(operand);
                if (text != null)
                    stringCollection.Add(text);
            }
            return "(" + StringListHelper.DelimitedText(stringCollection, " " + theOperator.OperatorType.ToString() + " ") + ")";
        }

        string ICriteriaVisitor<string>.Visit(UnaryOperator theOperator)
        {
            return BaseFormatterHelper.DefaultFormatUnary(theOperator.OperatorType, Process(theOperator.Operand));
        }

        string ICriteriaVisitor<string>.Visit(BinaryOperator theOperator)
        {
            throw new NotImplementedException();
        }

        string ICriteriaVisitor<string>.Visit(FunctionOperator theOperator)
        {
            if (EvalHelpers.IsLocalDateTime(theOperator.OperatorType))
            {
                if (theOperator.Operands.Count != 0)
                    throw new ArgumentException("theOperator.Operands.Count != 0");
                return Process(new ConstantValue(EvalHelpers.EvaluateLocalDateTime(theOperator.OperatorType)));
            }
            if (EvalHelpers.IsOutlookInterval(theOperator.OperatorType))
                return Process(EvalHelpers.ExpandIsOutlookInterval(theOperator));
            return VisitInternal(theOperator);
        }

        protected virtual string VisitInternal(FunctionOperator theOperator)
        {
            if (theOperator.OperatorType == FunctionOperatorType.Custom || theOperator.OperatorType == FunctionOperatorType.CustomNonDeterministic)
                return FormatCustomFunction(theOperator);
            if (IsLocalDateTimeOrOutlookInterval(theOperator, out CriteriaOperator res))
                return Process(res);
            string[] array = new string[theOperator.Operands.Count];
            for (int i = 0; i < theOperator.Operands.Count; i++)
                array[i] = Process(theOperator.Operands[i]);
            string text = BaseFormatterHelper.DefaultFormatFunction(theOperator.OperatorType, array);
            if (text != null)
                return text;
            throw new NotImplementedException();
        }

        private string FormatCustomFunction(FunctionOperator customOperator)
        {
            if (customOperator.Operands.Count < 1)
                throw new ArgumentException("customOperator.Operands.Count < 1");
            OperandValue operandValue = customOperator.Operands[0] as OperandValue;
            if (operandValue.ReferenceEqualsNull())
                throw new ArgumentNullException("customOperator.Operands[0] as OperandValue");
            string text = operandValue.Value as string;
            if (text == null)
                throw new ArgumentException("functionName (customOperator.Operands[0].Value is not string)");
            ICustomFunctionOperator customFunction = CriteriaOperator.GetCustomFunction(text);
            if (customFunction == null)
                throw new InvalidOperationException($"Undefined custom function '{text}'");
            ICustomFunctionOperatorFormattable customFunctionOperatorFormattable = customFunction as ICustomFunctionOperatorFormattable;
            if (customFunctionOperatorFormattable == null)
                throw new InvalidOperationException($"Custom function '{text}' does not implement ICustomFunctionOperatorFormattable");
            return customFunctionOperatorFormattable.Format(GetType(), (from x in customOperator.Operands.Skip(1)
                                                                        select Process(x)).ToArray());
        }

        string IClientCriteriaVisitor<string>.Visit(AggregateOperand theOperand)
        {
            throw new NotSupportedException("AggregateOperand");
        }

        string IClientCriteriaVisitor<string>.Visit(JoinOperand theOperand)
        {
            throw new NotSupportedException("JoinOperand");
        }

        private static bool IsLocalDateTimeOrOutlookInterval(FunctionOperator op, out CriteriaOperator res)
        {
            res = null;
            if (EvalHelpers.IsLocalDateTime(op.OperatorType))
            {
                if (op.Operands.Count != 0)
                    throw new ArgumentException("theOperator.Operands.Count != 0");
                res = new OperandValue(EvalHelpers.EvaluateLocalDateTime(op.OperatorType));
                return true;
            }
            if (EvalHelpers.IsOutlookInterval(op.OperatorType))
            {
                res = EvalHelpers.ExpandIsOutlookInterval(op);
                return true;
            }
            return false;
        }
    }
}