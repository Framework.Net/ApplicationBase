﻿namespace ApplicationBase.UI
{
    partial class TaskRunInfosView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions buttonImageOptions1 = new DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaskRunInfosView));
            DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions buttonImageOptions2 = new DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions();
            this.ctlChart = new TechnicalTools.UI.DX.Controls.EnhancedChartControl();
            this.lytctlMain = new DevExpress.XtraLayout.LayoutControl();
            this.lueColorOn = new DevExpress.XtraEditors.LookUpEdit();
            this.chkDaysOfWeeks = new TechnicalTools.UI.DX.Controls.CheckedTreeListLookUpEdit();
            this.chkTaskNames = new TechnicalTools.UI.DX.Controls.CheckedTreeListLookUpEdit();
            this.chkcmbDisplayLogins = new ApplicationBase.UI.Controls.LoginSetSelector();
            this.lueDisplayType = new DevExpress.XtraEditors.LookUpEdit();
            this.chkcmbDataSetLogins = new ApplicationBase.UI.Controls.LoginSetSelector();
            this.dtTo = new DevExpress.XtraEditors.DateEdit();
            this.dtFrom = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lytgrpSourceDataSet = new DevExpress.XtraLayout.LayoutControlGroup();
            this.dtFrom_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.dtTo_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lytgrpDisplayFiltering = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.ctlChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlMain)).BeginInit();
            this.lytctlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueColorOn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDisplayType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytgrpSourceDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytgrpDisplayFiltering)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // ctlChart
            // 
            this.ctlChart.Legend.Name = "Default Legend";
            this.ctlChart.Location = new System.Drawing.Point(2, 152);
            this.ctlChart.Name = "ctlChart";
            this.ctlChart.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.ctlChart.Size = new System.Drawing.Size(1128, 374);
            this.ctlChart.TabIndex = 1;
            this.ctlChart.CustomDrawCrosshair += new DevExpress.XtraCharts.CustomDrawCrosshairEventHandler(this.ctlChart_CustomDrawCrosshair);
            this.ctlChart.CustomDrawSeriesPoint += new DevExpress.XtraCharts.CustomDrawSeriesPointEventHandler(this.ctlChart_CustomDrawSeriesPoint);
            this.ctlChart.PopupMenuShowing += new System.EventHandler<DevExpress.Utils.Menu.DXPopupMenu>(this.ctlChart_PopupMenuShowing);
            // 
            // lytctlMain
            // 
            this.lytctlMain.Controls.Add(this.lueColorOn);
            this.lytctlMain.Controls.Add(this.chkDaysOfWeeks);
            this.lytctlMain.Controls.Add(this.chkTaskNames);
            this.lytctlMain.Controls.Add(this.chkcmbDisplayLogins);
            this.lytctlMain.Controls.Add(this.lueDisplayType);
            this.lytctlMain.Controls.Add(this.chkcmbDataSetLogins);
            this.lytctlMain.Controls.Add(this.dtTo);
            this.lytctlMain.Controls.Add(this.dtFrom);
            this.lytctlMain.Controls.Add(this.ctlChart);
            this.lytctlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lytctlMain.Location = new System.Drawing.Point(0, 0);
            this.lytctlMain.Name = "lytctlMain";
            this.lytctlMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(907, 480, 650, 400);
            this.lytctlMain.Root = this.layoutControlGroup1;
            this.lytctlMain.Size = new System.Drawing.Size(1132, 528);
            this.lytctlMain.TabIndex = 4;
            this.lytctlMain.Text = "layoutControl2";
            // 
            // lueColorOn
            // 
            this.lueColorOn.Location = new System.Drawing.Point(1031, 116);
            this.lueColorOn.Name = "lueColorOn";
            this.lueColorOn.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueColorOn.Size = new System.Drawing.Size(87, 20);
            this.lueColorOn.StyleController = this.lytctlMain;
            this.lueColorOn.TabIndex = 20;
            // 
            // chkDaysOfWeeks
            // 
            this.chkDaysOfWeeks.Location = new System.Drawing.Point(658, 116);
            this.chkDaysOfWeeks.MaxSelectedItems = ((uint)(0u));
            this.chkDaysOfWeeks.Name = "chkDaysOfWeeks";
            this.chkDaysOfWeeks.Size = new System.Drawing.Size(98, 20);
            this.chkDaysOfWeeks.TabIndex = 19;
            // 
            // chkTaskNames
            // 
            this.chkTaskNames.Location = new System.Drawing.Point(363, 116);
            this.chkTaskNames.MaxSelectedItems = ((uint)(0u));
            this.chkTaskNames.Name = "chkTaskNames";
            this.chkTaskNames.Size = new System.Drawing.Size(212, 20);
            this.chkTaskNames.TabIndex = 18;
            // 
            // chkcmbDisplayLogins
            // 
            this.chkcmbDisplayLogins.Location = new System.Drawing.Point(49, 116);
            this.chkcmbDisplayLogins.MaxSelectedItems = ((uint)(0u));
            this.chkcmbDisplayLogins.Name = "chkcmbDisplayLogins";
            this.chkcmbDisplayLogins.Size = new System.Drawing.Size(276, 20);
            this.chkcmbDisplayLogins.TabIndex = 13;
            // 
            // lueDisplayType
            // 
            this.lueDisplayType.Location = new System.Drawing.Point(835, 116);
            this.lueDisplayType.Name = "lueDisplayType";
            this.lueDisplayType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lueDisplayType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDisplayType.Size = new System.Drawing.Size(138, 20);
            this.lueDisplayType.StyleController = this.lytctlMain;
            this.lueDisplayType.TabIndex = 11;
            // 
            // chkcmbDataSetLogins
            // 
            this.chkcmbDataSetLogins.Location = new System.Drawing.Point(374, 41);
            this.chkcmbDataSetLogins.MaxSelectedItems = ((uint)(0u));
            this.chkcmbDataSetLogins.Name = "chkcmbDataSetLogins";
            this.chkcmbDataSetLogins.Size = new System.Drawing.Size(744, 20);
            this.chkcmbDataSetLogins.TabIndex = 10;
            // 
            // dtTo
            // 
            this.dtTo.EditValue = null;
            this.dtTo.Location = new System.Drawing.Point(216, 41);
            this.dtTo.Name = "dtTo";
            this.dtTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dtTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtTo.Size = new System.Drawing.Size(100, 20);
            this.dtTo.StyleController = this.lytctlMain;
            this.dtTo.TabIndex = 5;
            // 
            // dtFrom
            // 
            this.dtFrom.EditValue = null;
            this.dtFrom.Location = new System.Drawing.Point(92, 41);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dtFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtFrom.Size = new System.Drawing.Size(100, 20);
            this.dtFrom.StyleController = this.lytctlMain;
            this.dtFrom.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.lytgrpSourceDataSet,
            this.lytgrpDisplayFiltering});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1132, 528);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ctlChart;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1132, 378);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // lytgrpSourceDataSet
            // 
            buttonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("buttonImageOptions1.Image")));
            this.lytgrpSourceDataSet.CustomHeaderButtons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("Button", false, buttonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1)});
            this.lytgrpSourceDataSet.ExpandButtonVisible = true;
            this.lytgrpSourceDataSet.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.dtFrom_LayoutItem,
            this.dtTo_LayoutItem,
            this.layoutControlItem1});
            this.lytgrpSourceDataSet.Location = new System.Drawing.Point(0, 0);
            this.lytgrpSourceDataSet.Name = "lytgrpSourceDataSet";
            this.lytgrpSourceDataSet.Size = new System.Drawing.Size(1132, 75);
            this.lytgrpSourceDataSet.Text = "Source Data Set";
            this.lytgrpSourceDataSet.CustomButtonClick += new DevExpress.XtraBars.Docking2010.BaseButtonEventHandler(this.lytgrpSourceDataSet_CustomButtonClick);
            // 
            // dtFrom_LayoutItem
            // 
            this.dtFrom_LayoutItem.Control = this.dtFrom;
            this.dtFrom_LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.dtFrom_LayoutItem.MaxSize = new System.Drawing.Size(182, 24);
            this.dtFrom_LayoutItem.MinSize = new System.Drawing.Size(182, 24);
            this.dtFrom_LayoutItem.Name = "dtFrom_LayoutItem";
            this.dtFrom_LayoutItem.Size = new System.Drawing.Size(182, 24);
            this.dtFrom_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.dtFrom_LayoutItem.Text = "Load data from";
            this.dtFrom_LayoutItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.dtFrom_LayoutItem.TextSize = new System.Drawing.Size(73, 13);
            this.dtFrom_LayoutItem.TextToControlDistance = 5;
            // 
            // dtTo_LayoutItem
            // 
            this.dtTo_LayoutItem.Control = this.dtTo;
            this.dtTo_LayoutItem.Location = new System.Drawing.Point(182, 0);
            this.dtTo_LayoutItem.MaxSize = new System.Drawing.Size(124, 24);
            this.dtTo_LayoutItem.MinSize = new System.Drawing.Size(124, 24);
            this.dtTo_LayoutItem.Name = "dtTo_LayoutItem";
            this.dtTo_LayoutItem.Size = new System.Drawing.Size(124, 24);
            this.dtTo_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.dtTo_LayoutItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 0, 0, 0);
            this.dtTo_LayoutItem.Text = "to";
            this.dtTo_LayoutItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.dtTo_LayoutItem.TextSize = new System.Drawing.Size(10, 13);
            this.dtTo_LayoutItem.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chkcmbDataSetLogins;
            this.layoutControlItem1.Location = new System.Drawing.Point(306, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(802, 24);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 0, 0, 0);
            this.layoutControlItem1.Text = "for logins";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(44, 13);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // lytgrpDisplayFiltering
            // 
            buttonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("buttonImageOptions2.Image")));
            this.lytgrpDisplayFiltering.CustomHeaderButtons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("Button", false, buttonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1)});
            this.lytgrpDisplayFiltering.ExpandButtonVisible = true;
            this.lytgrpDisplayFiltering.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem3,
            this.layoutControlItem5});
            this.lytgrpDisplayFiltering.Location = new System.Drawing.Point(0, 75);
            this.lytgrpDisplayFiltering.Name = "lytgrpDisplayFiltering";
            this.lytgrpDisplayFiltering.Size = new System.Drawing.Size(1132, 75);
            this.lytgrpDisplayFiltering.Text = "Display Filtering";
            this.lytgrpDisplayFiltering.CustomButtonClick += new DevExpress.XtraBars.Docking2010.BaseButtonEventHandler(this.lytgrpDisplayFiltering_CustomButtonClick);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.lueDisplayType;
            this.layoutControlItem4.Location = new System.Drawing.Point(746, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(217, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(217, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(217, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 0, 0, 0);
            this.layoutControlItem4.Text = "Display Type:";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(65, 13);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.chkcmbDisplayLogins;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(315, 24);
            this.layoutControlItem6.Text = "Logins";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(30, 13);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.chkTaskNames;
            this.layoutControlItem8.Location = new System.Drawing.Point(315, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem8.Text = "Task: ";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(29, 13);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.chkDaysOfWeeks;
            this.layoutControlItem3.Location = new System.Drawing.Point(565, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(181, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(181, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(181, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 0, 0, 0);
            this.layoutControlItem3.Text = "Days of week:";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(69, 13);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lueColorOn;
            this.layoutControlItem5.Location = new System.Drawing.Point(963, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(145, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(145, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(145, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 0, 0, 0);
            this.layoutControlItem5.Text = "Color on:";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(44, 13);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // TaskRunInfosView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lytctlMain);
            this.Name = "TaskRunInfosView";
            this.Size = new System.Drawing.Size(1132, 528);
            this.Load += new System.EventHandler(this.TaskRunInfosView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ctlChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlMain)).EndInit();
            this.lytctlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueColorOn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDisplayType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytgrpSourceDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFrom_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTo_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytgrpDisplayFiltering)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.DX.Controls.EnhancedChartControl ctlChart;
        private DevExpress.XtraLayout.LayoutControl lytctlMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.DateEdit dtTo;
        private DevExpress.XtraEditors.DateEdit dtFrom;
        private DevExpress.XtraLayout.LayoutControlItem dtFrom_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem dtTo_LayoutItem;
        private ApplicationBase.UI.Controls.LoginSetSelector chkcmbDataSetLogins;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LookUpEdit lueDisplayType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup lytgrpSourceDataSet;
        private DevExpress.XtraLayout.LayoutControlGroup lytgrpDisplayFiltering;
        private ApplicationBase.UI.Controls.LoginSetSelector chkcmbDisplayLogins;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private TechnicalTools.UI.DX.Controls.CheckedTreeListLookUpEdit chkTaskNames;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private TechnicalTools.UI.DX.Controls.CheckedTreeListLookUpEdit chkDaysOfWeeks;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LookUpEdit lueColorOn;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
    }
}