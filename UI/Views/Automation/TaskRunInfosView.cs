﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Utils.Menu;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Designer;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;

using ApplicationBase.DAL.Users;
using ApplicationBase.DAL.Logging;
using ApplicationBase.Business.Logs;


namespace ApplicationBase.UI
{
    [ToolboxItem(true)]
    public partial class TaskRunInfosView : ApplicationBaseView
    {
        TaskRunInfoSet TaskRunInfoSet { get { return (TaskRunInfoSet)BusinessObject; } }

        [Obsolete("For designer only")]
        protected TaskRunInfosView() : this(null) { }

        public TaskRunInfosView(TaskRunInfoSet taskRunInfos)
            : base(taskRunInfos ?? new TaskRunInfoSet(null) { }, Feature.AllowEverybody)
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            Text = "Automated tasks' run infos";
        }


        private void TaskRunInfosView_Load(object sender, EventArgs e)
        {
            var dt = DateTime.Now.Date;
            dtTo.EditValue = dt;
            dt = dt.AddDays(-15);
            dtFrom.EditValue = dt.AddDays(1 - dt.Day); // beginning of the month or previous month if less than 15 days

            lueDisplayType.FillWithEnumValues<eDisplayType>();
            lueDisplayType.SelectedValueAsEnumSet(eDisplayType.Duration);

            lueColorOn.FillWithEnumValues<eColorOn>();
            lueColorOn.SelectedValueAsEnumSet(eColorOn.Task);

            _loginsById = TaskRunInfoSet.Source.SelectableLogins.Keys.ToDictionary(login => login.Person_Id);
            chkcmbDataSetLogins.Fill(true, true, TaskRunInfoSet.Source.AuthenticationManager, TaskRunInfoSet.Source.SelectableLogins);
            chkcmbDataSetLogins.SelectedLogins = TaskRunInfoSet.Source.SelectableLogins.Keys.ToList();

            chkDaysOfWeeks.ItemsAvailablesSet(DaysOfWeek.AllDays);
            chkDaysOfWeeks.ItemsSelected = SelectedDaysOfWeek;
        }
        IReadOnlyDictionary<long, Login> _loginsById;
        readonly FixedBindingList<DaysOfWeek> SelectedDaysOfWeek = DaysOfWeek.AllDays.ToFixedBindingList();

        
        enum eDisplayType
        {
            [Description("Duration")]
            Duration,
            [Description("Time range of execution")]
            TimeRangeOfExecution,
        }
        enum eColorOn
        {
            None,
            Task,
            Login
        }
        class DaysOfWeek : ITreeNodeWithParentReadable, IIsNamed
        {
            public DayOfWeek DayOfWeek { get; private set; }
            public string Name { get { return (DayOfWeek == DayOfWeek.Sunday ? 7 : (int)DayOfWeek) + " " +  DayOfWeek.GetDescription(); } }
            ITreeNodeWithParentReadable ITreeNodeWithParentReadable.ParentNode { get { return null; } }
            public static readonly DaysOfWeek[] AllDays = new DaysOfWeek[]
            {
                new DaysOfWeek() { DayOfWeek = DayOfWeek.Monday },
                new DaysOfWeek() { DayOfWeek = DayOfWeek.Tuesday },
                new DaysOfWeek() { DayOfWeek = DayOfWeek.Wednesday },
                new DaysOfWeek() { DayOfWeek = DayOfWeek.Thursday },
                new DaysOfWeek() { DayOfWeek = DayOfWeek.Friday },
                new DaysOfWeek() { DayOfWeek = DayOfWeek.Saturday },
                new DaysOfWeek() { DayOfWeek = DayOfWeek.Sunday },
            };
        }

        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();
            lytgrpDisplayFiltering.Enabled = _runInfosByTaskNames != null;
        }

        private void lytgrpSourceDataSet_CustomButtonClick(object sender, DevExpress.XtraBars.Docking2010.BaseButtonEventArgs e)
        {
            var filter = new LogFilter(TaskRunInfoSet.Source, null)
            {
                DateUtcFrom = dtFrom.DateTime.ToUniversalTime(),
                DateUtcToIncluded = dtTo.DateTime.ToUniversalTime(),
            };
            filter.ForLogins.AddRange(chkcmbDataSetLogins.SelectedLogins);
            this.ShowBusyWhileDoing("Loading", _ => TaskRunInfoSet.Load(filter))
                .ContinueWithLongUIWorkOnSuccess(this, InitializeDataSetFiltering);
        }
        void InitializeDataSetFiltering(IReadOnlyDictionary<string, List<TaskRunInfo>> runInfosByTaskNames)
        {
            _runInfosByTaskNames = runInfosByTaskNames;
            
            var loginsWithLogs = runInfosByTaskNames.Values.SelectMany(tris => tris)
                                                           .Select(tri => tri.LoginId)
                                                           .Distinct()
                                                           .Select(login_id => _loginsById[login_id])
                                                           .ToList();
            chkcmbDisplayLogins.Fill(true, true, TaskRunInfoSet.Source.AuthenticationManager, loginsWithLogs.ToDictionary(login => login, login => TaskRunInfoSet.Source.SelectableLogins[login]));
            chkcmbDisplayLogins.UnavailableLogins = TaskRunInfoSet.Source.SelectableLogins.Keys.Except(loginsWithLogs).ToList();
            chkcmbDisplayLogins.SelectedLogins = loginsWithLogs;

            _availableTaskNames = TaskNameView.Generate(_runInfosByTaskNames.Keys, _availableTaskNames);
            var selected = chkTaskNames.ItemsSelected.Count == 0
                         ? _availableTaskNames.Values
                                              .Where(tnv => !tnv.HasChildren && tnv.FullName.Contains(nameof(Business)))
                                              .OfType<ITreeNodeWithParentReadable>()
                                              .ToFixedBindingList()
                         : _availableTaskNames.Values
                                              .OfType<ITreeNodeWithParentReadable>()
                                              .Intersect(chkTaskNames.ItemsSelected)
                                              .ToFixedBindingList();
            chkTaskNames.ItemsSelected.Clear();
            chkTaskNames.ItemsAvailablesSet(_availableTaskNames.Values);
            chkTaskNames.ItemsUnselectable = _availableTaskNames.Values.Where(tnv => tnv.HasChildren);
            chkTaskNames.ItemsSelected = selected;

            lytgrpSourceDataSet.Expanded = false;
            lytgrpDisplayFiltering.Expanded = true;
            RefreshEnabilitiesAndVisibilities();
        }
        IReadOnlyDictionary<string, List<TaskRunInfo>> _runInfosByTaskNames;
        Dictionary<string, TaskNameView> _availableTaskNames = new Dictionary<string, TaskNameView>();
        class TaskNameView : ITreeNodeWithParentReadable, IIsNamed
        {
            public string Name { get; set; }
            public ITreeNodeWithParentReadable ParentNode { get { return Parent; } }
            TaskNameView Parent;
            public bool HasChildren { get; private set; }
            public string FullName
            {
                get
                {
                    return (ParentNode == null
                           ? ""
                           : Parent.FullName + TaskRunInfoSet.TaskNameSeparator)
                           + Name;
                }
            }

            public string ShortName { get; private set; }

            public static Dictionary<string, TaskNameView> Generate(IEnumerable<string> taskNames, IReadOnlyDictionary<string, TaskNameView> existingTaskNames)
            {
                var res = new Dictionary<string, TaskNameView>();
                foreach (var name in taskNames)
                {
                    TaskNameView parent = null;
                    foreach (var part in name.Split(TaskRunInfoSet.TaskNameSeparator.WrapInArray(), StringSplitOptions.None))
                    {
                        parent = new TaskNameView() { Name = part, Parent = parent, HasChildren = true };
                        if (res.ContainsKey(parent.FullName))
                            parent = res[parent.FullName];
                        else
                        {
                            if (existingTaskNames.ContainsKey(parent.FullName))
                                parent = existingTaskNames[parent.FullName];
                            res.Add(parent.FullName, parent);
                        }
                    }
                    parent.HasChildren = false;
                }

                // Simple algorithm to get shorter name
                foreach (var tnvs in res.Values.GroupBy(tnv => tnv.Name))
                {
                    if (tnvs.Count() == 1)
                        tnvs.First().ShortName = tnvs.Key;
                    else
                        foreach (var tnv in tnvs)
                            tnv.ShortName = tnv.FullName;
                }
                return res;
            }

            public override int GetHashCode()
            {
                return FullName.GetHashCode();
            }
            public override bool Equals(object obj)
            {
                return obj is TaskNameView tnv
                    && FullName == tnv?.FullName;
            }
        }
        
        private void lytgrpDisplayFiltering_CustomButtonClick(object sender, DevExpress.XtraBars.Docking2010.BaseButtonEventArgs e)
        {
            var taskNamesSelected = chkTaskNames.ItemsSelected.Cast<TaskNameView>().ToList();
            if (taskNamesSelected.Count == 0)
            {
                XtraMessageBox.Show("Select at least one task name!");
                return;
            }
            var loginIds = chkcmbDisplayLogins.SelectedLogins.Select(login => login.Person_Id).ToHashSet();
            if (chkcmbDisplayLogins.SelectedLogins.Count == 0)
            {
                XtraMessageBox.Show("Select at least one login!");
                return;
            }
            var colorOn = lueColorOn.SelectedValueAsEnum<eColorOn>();
            var days = SelectedDaysOfWeek.ToDictionary(d => d.DayOfWeek);
            var runInfos = taskNamesSelected.SelectMany(taskNameSelected => _runInfosByTaskNames[taskNameSelected.FullName])
                                            .Where(tri => days.ContainsKey(tri.StartDateUtc.ToLocalTime().DayOfWeek))
                                            .Where(tri => loginIds.Contains(tri.LoginId))
                                            .ToArray();
            var firstTime = ctlChart.SeriesSerializable.IsEmpty();
            if (firstTime)
                InitializeChart();

            ctlChart.Titles.Clear();
            var xyDiagram = (XYDiagram)ctlChart.Diagram;
            if (xyDiagram != null)
                xyDiagram.AxisX.DateTimeScaleOptions.ScaleMode = ScaleMode.Manual;
            string chartTitle = "";
            var displayType = lueDisplayType.SelectedValueAsEnum<eDisplayType>();
            switch (displayType)
            {
                case eDisplayType.Duration:
                    chartTitle = "Execution times";
                    CreateDurationSeries(runInfos, colorOn);
                    break;
                case eDisplayType.TimeRangeOfExecution:
                    chartTitle = "Execution ranges";
                    CreateTimeRangeSeries(runInfos, colorOn);
                    break;
            }
            if (taskNamesSelected.Count == 1)
                chartTitle += " of " + taskNamesSelected[0].ShortName;
            ctlChart.Titles.Add(new ChartTitle() { Text = chartTitle });
            FinalizeChart(displayType);
        }

        void InitializeChart()
        {
            ctlChart.ObjectHotTracked += ctlChart_ObjectHotTracked;
            ctlChart.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;

            // https://documentation.devexpress.com/WindowsForms/14710/Controls-and-Libraries/Chart-Control/End-User-Features/Tooltip-and-Crosshair-Cursor/Crosshair-Cursor
            // https://docs.devexpress.com/WindowsForms/2141/common-features/formatting-values/format-specifiers
            ctlChart.CrosshairOptions.GroupHeaderPattern = "Duration: {VDTM:F2} min";
            ctlChart.CrosshairOptions.HighlightPoints = true;
            ctlChart.CrosshairOptions.CrosshairLabelMode = CrosshairLabelMode.ShowForNearestSeries;
        }
        void FinalizeChart(eDisplayType displayType)
        {
            // ctlChart.Diagram exist only after series are assigned
            var xyDiagram = (XYDiagram)ctlChart.Diagram;
            // https://docs.devexpress.com/WPF/11234/controls-and-libraries/charts-suite/chart-control/end-user-features/end-user-capabilities/zooming-and-scrolling-2d-xy-charts
            xyDiagram.EnableAxisXZooming = true;
            xyDiagram.EnableAxisYZooming = true;
            xyDiagram.EnableAxisXScrolling = true;
            xyDiagram.EnableAxisYScrolling = true;
            xyDiagram.ZoomingOptions.AxisXMaxZoomPercent = 1000000;
            xyDiagram.ZoomingOptions.AxisYMaxZoomPercent = 1000000;

            // Disable aggregation by date https://docs.devexpress.com/WindowsForms/6464/Controls-and-Libraries/Chart-Control/Examples/Creating-Charts/Data-Representation/How-to-Use-Automatic-Date-Time-Scale-Modes-of-an-Axis
            //                             https://docs.devexpress.com/CoreLibraries/DevExpress.XtraCharts.ScaleOptionsBase.ScaleMode
            if (displayType == eDisplayType.Duration)
            {
                xyDiagram.AxisX.DateTimeScaleOptions.ScaleMode = ScaleMode.Continuous;
                Debug.Assert(xyDiagram.AxisY.TimeSpanScaleOptions.MeasureUnit == TimeSpanMeasureUnit.Second);
            }
            else
            {
                // Here ScaleMode.Interval make chartcontrol to crashes on second refresh with overflow exception, like this https://supportcenter.devexpress.com/ticket/details/T726942
                xyDiagram.AxisX.DateTimeScaleOptions.ScaleMode = ScaleMode.Manual;  // So we keep Manual
                xyDiagram.AxisX.DateTimeScaleOptions.MeasureUnit = DateTimeMeasureUnit.Second; // forbidden for ScaleMode.Automatic & Continous
            }
            xyDiagram.AxisX.DateTimeScaleOptions.WorkdaysOptions.Workdays = (Weekday)chkDaysOfWeeks.ItemsSelected.Cast<DaysOfWeek>().Select(d => Math.Pow(2, (int)d.DayOfWeek)).DefaultIfEmpty(0).Sum();
            // Allow user to zoom exactly on the area he selected (and potentially ignore some point that would be above & outside the chart now)
            // Otherwise devexpress adapt the zommed Y range so all point are visible >:(
            xyDiagram.DependentAxesYRange = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram.AxisX.DateTimeScaleOptions.AggregateFunction = AggregateFunction.None;

            // Reinitialize ranges (even if values are already true, it seems to reset)
            // Useful when series' data have changed
            xyDiagram.AxisX.WholeRange.Auto = true;
            xyDiagram.AxisX.WholeRange.AlwaysShowZeroLevel = false;
            xyDiagram.AxisX.VisualRange.Auto = true;
            xyDiagram.AxisY.WholeRange.Auto = true;
            xyDiagram.AxisY.WholeRange.AlwaysShowZeroLevel = false;
            xyDiagram.AxisY.VisualRange.Auto = true;

            xyDiagram.AxisX.Strips.Clear();
            if (displayType == eDisplayType.TimeRangeOfExecution)
            {
                DateTime dt = (DateTime)xyDiagram.AxisX.WholeRange.MinValue;
                Debug.Assert(xyDiagram.AxisX.DateTimeScaleOptions.MeasureUnit == DateTimeMeasureUnit.Second);
                dt = dt.AddSeconds(-xyDiagram.AxisX.WholeRange.SideMarginsValue);
                dt = dt.Date;
                DateTime dtTo = (DateTime)xyDiagram.AxisX.WholeRange.MaxValue;
                dtTo = dtTo.AddSeconds(xyDiagram.AxisX.WholeRange.SideMarginsValue);
                dtTo = dtTo.AddDays(1).AddMilliseconds(-1).Date;
                while (dt < dtTo)
                {
                    var from = dt;
                    dt = dt.AddDays(1);

                    if (dt.DayOfWeek == DayOfWeek.Monday || dt.DayOfWeek == DayOfWeek.Wednesday || dt.DayOfWeek == DayOfWeek.Friday)
                        continue;

                    var strip = new Strip("", from, dt);
                    strip.Color = dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday
                                ? Color.WhiteSmoke
                                : Color.MintCream;
                    xyDiagram.AxisX.Strips.Add(strip);
                }
            }
        }

        void CreateDurationSeries(TaskRunInfo[]  runInfos, eColorOn colorOn)
        {
            var series = new List<Series>();
            // In some rare cases (but they exists) there is some execution that begin to the same exact ms)
            // So we need to create a second serie. We set color for each point and hide series titles so it is transparent for user
            var pointsByDates = runInfos.OrderBy(tri => tri.StartDateUtc).ToList();
            var remainingPointsByDates = new List<TaskRunInfo>();
            while (pointsByDates.Count > 0)
            {
                var seriePoints = new List<SeriesPoint>();

                TaskRunInfo last = null;
                foreach (var tri in pointsByDates)
                {
                    // Must be a comparison on Date (even if chart is in mode Continuous)
                    // otherwise we got a zero divison exception in Devexpress code :/
                    if (last == null || last.StartDateUtc.Date != tri.StartDateUtc.Date)
                    {
                        var sp = new SeriesPoint(tri.StartDateUtc.ToLocalTime(), tri.Duration) { Tag = tri };
                        sp.Color = ColorFor(tri, colorOn);
                        seriePoints.Add(sp);
                    }
                    else
                        remainingPointsByDates.Add(tri);
                    last = tri;
                }
                var s = new Series();
                s.Name = series.Count.ToString();
                s.ArgumentScaleType = ScaleType.DateTime;
                s.ValueScaleType = ScaleType.TimeSpan;
                var psv = new PointSeriesView();
                // According to https://supportcenter.devexpress.com/ticket/details/T325794 it should work :'(
                psv.PointMarkerOptions.FillStyle.FillMode = FillMode.Solid;
                psv.PointMarkerOptions.BorderVisible = false;
                psv.PointMarkerOptions.Size = 10;
                psv.PointMarkerOptions.Kind = MarkerKind.Circle;
                // We do not set "psv.ColorEach = true" 
                // because it causes the legend to have all points instead of all series in it :/
                // So we apply coloration by using event CustomDrawSeriesPoint
                s.View = psv;
                s.Points.AddRange(seriePoints.ToArray());
                series.Add(s);

                pointsByDates.Clear();

                var tmp = pointsByDates;
                pointsByDates = remainingPointsByDates;
                remainingPointsByDates = tmp;
            }
            
            ctlChart.SeriesSerializable = series.ToArray();
        }

        void CreateTimeRangeSeries(TaskRunInfo[] runInfos, eColorOn colorOn)
        {
            var series = new List<Series>();
            // In some rare cases (but they exists) there is some execution that begin to the same exact ms)
            // So we need to create a second serie. We set color for each point and hide series titles so it is transparent for user
            var pointsByDates = runInfos.OrderBy(tri => tri.StartDateUtc).ToList();
            var remainingPointsByDates = new List<TaskRunInfo>();
            while (pointsByDates.Count > 0)
            {
                var seriePoints = new List<SeriesPoint>();

                TaskRunInfo last = null;
                foreach (var tri in pointsByDates)
                {
                    // No need to check on date here, the full date + time is ok
                    if (last == null || last.StartDateUtc != tri.StartDateUtc)
                    {
                        var sp = new SeriesPoint(tri.StartDateUtc.ToLocalTime(), tri.StartDateUtc.ToLocalTime().TimeOfDay, tri.EndDateUtc.ToLocalTime().TimeOfDay) { Tag = tri };
                        seriePoints.Add(sp);
                        sp.Color = ColorFor(tri, colorOn);
                    }
                    else
                        remainingPointsByDates.Add(tri);
                    last = tri;
                }
                var s = new Series();
                s.Name = series.Count.ToString();
                s.ArgumentScaleType = ScaleType.DateTime;
                s.ValueScaleType = ScaleType.TimeSpan;
                var orbs = new OverlappedRangeBarSeriesView();
                // We do not set "orbs.ColorEach = true" 
                // because it causes the legend to have all points instead of all series in it :/
                // So we apply coloration by using event CustomDrawSeriesPoint
                s.View = orbs;
                s.Points.AddRange(seriePoints.ToArray());
                series.Add(s);

                pointsByDates.Clear();

                var tmp = pointsByDates;
                pointsByDates = remainingPointsByDates;
                remainingPointsByDates = tmp;
            }

            ctlChart.SeriesSerializable = series.ToArray();
        }

        Color ColorFor(TaskRunInfo tri, eColorOn colorOn)
        {
            if (colorOn == eColorOn.Login)
            {
                return Color_Extensions.DistinctColors1024[(1024 + tri.LoginId) % 1022 + 2];
            }
            else if (colorOn == eColorOn.Task)
            {
                Debug.Assert(_availableTaskNames.Count < 1022);
                var mod = tri.TaskName.GetHashCode();
                mod %= _availableTaskNames.Count;
                if (mod < 0)
                    mod += _availableTaskNames.Count;
                mod += 2; // as advised in Color_Extensions.DistinctColors1024
                return Color_Extensions.DistinctColors1024[mod];
            }
            else
                return Color.Black;
        }

        private void ctlChart_CustomDrawSeriesPoint(object sender, CustomDrawSeriesPointEventArgs e)
        {
            var tri = e.SeriesPoint.Tag as TaskRunInfo;
            if (tri == null)
                return;
            
            var pdo = e.SeriesDrawOptions as PointDrawOptions;
            if (pdo != null)
            {
                pdo.Marker.BorderColor = pdo.Color;
                pdo.Marker.Size = 3 + (int)(24 * (1 - Math.Exp(-tri.Duration.TotalMinutes / 60)));
            }
        }
        
        private void ctlChart_ObjectHotTracked(object sender, HotTrackEventArgs e)
        {
            _hoveredPoint = e.AdditionalObject as SeriesPoint;
        }
        SeriesPoint _hoveredPoint;


        // https://docs.devexpress.com/WindowsForms/14710/controls-and-libraries/chart-control/end-user-features/tooltip-and-crosshair-cursor/crosshair-cursor#use-the-crosshair-cursors-runtime-api
        private void ctlChart_CustomDrawCrosshair(object sender, CustomDrawCrosshairEventArgs e)
        {
            if (e.CrosshairElementGroups.Count <= 0)
                return;
            CrosshairElementGroup group = e.CrosshairElementGroups[0];
            if (group.CrosshairElements.Count <= 0)
                return;
            var element = group.CrosshairElements[0];
            var labelElement = element.LabelElement;
            var tag = element.SeriesPoint.Tag;
            if (labelElement == null)
                return;
            group.HeaderElement.Font = new Font(group.HeaderElement.Font.FontFamily, 14, FontStyle.Bold);

            labelElement.Font = new Font("Courier New", 12F);
            var tri = tag as TaskRunInfo;
            if (tri != null)
                labelElement.Text = TaskRunInfoToLabel(tri);
            else if (tag is List<object> tris)
                labelElement.Text = tris.OfType<TaskRunInfo>().Select(tri2 => TaskRunInfoToLabel(tri2)).Join(Environment.NewLine + Environment.NewLine);
        }
        string TaskRunInfoToLabel(TaskRunInfo tri)
        {
            var login = _loginsById[tri.LoginId];
            var person = TaskRunInfoSet.Source.SelectableLogins[login];
            var tnv = _availableTaskNames[tri.TaskName.Replace(".", TaskRunInfoSet.TaskNameSeparator)];
            var dtFrom = tri.StartDateUtc.ToLocalTime();
            var dtTo = tri.EndDateUtc.ToLocalTime();
            return "Task:      " + tnv.ShortName + Environment.NewLine
                 + (login.IsService
                 ? "Service:   "
                 : "User:      ") + person.FullName + Environment.NewLine
                 + "Duration:  " + tri.Duration.ToHumanReadableShortNotation()
                 + " ( " + dtFrom.ToString("dddd") + " " + dtFrom.ToString("d") + " " + tri.StartDateUtc.ToLocalTime().TimeOfDay.ToString("hh':'mm':'ss")
                 + " ==> " + (dtTo.Date == dtFrom.Date ? "" : dtTo.ToString("d") + " ") + dtTo.TimeOfDay.ToString("hh':'mm':'ss") + " )" + Environment.NewLine
                 + "SessionId: " + tri.SessionId;
        }
        static readonly Font _courierNew = new Font("Courier New", 8F);

        void ctlChart_PopupMenuShowing(object sender, DXPopupMenu menu)
        {
            if (menu == null)
                return;
            // Tag may also be a List<object> where object are TaskRunInfo
            var tri = _hoveredPoint?.Tag as TaskRunInfo;

            var mnu = new DXMenuItem() { Caption = "See full log of this task" };
            mnu.Click += (_, __) =>
            {
                var login = _loginsById[tri.LoginId];
                var person = TaskRunInfoSet.Source.SelectableLogins[login];
                var lockedFilter = new LogFilter(TaskRunInfoSet.Source, tri.SessionId);
                var ctl = UIControler.Instance.ShowObject(lockedFilter);
                ctl.Text += " of Session " + tri.SessionId + " for " + person.FullName;
            };
            mnu.Enabled = tri != null;
            menu.Items.Add(mnu);
        }
    }
}
