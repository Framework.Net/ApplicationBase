﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;

using TechnicalTools.UI;

using ApplicationBase.DAL.Users;
using ApplicationBase.Business;

using DevExpress.XtraVerticalGrid.Rows;


namespace ApplicationBase.UI
{
    public partial class ConfigOfUserView : ApplicationBaseView
    {
        ConfigOfUser Config { get { return (ConfigOfUser)BusinessObject; } }

        [Obsolete("For designer only", true)]
        protected ConfigOfUserView()
            : this(null)
        {
        }
        public ConfigOfUserView(ConfigOfUser cfg)
            : base(cfg, Feature.AllowEverybody)
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            Text = "Your settings";
        }

        void ChangeEnvironmentView_Load(object sender, EventArgs _)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            var frm = FindForm();
            Debug.Assert(frm != null);

            pUserSettings.SelectedObject = Config;
            if (Config != null)
            {
                var editableProperties = Config.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)
                                                         .Where(p => p.CanRead && p.CanWrite)
                                                         .Where(p => p.GetCustomAttribute<DisplayNameAttribute>() != null)
                                                         .ToDictionary(p => p.Name);
                foreach (var crow in pUserSettings.Rows.OfType<CategoryRow>().ToList())
                {
                    foreach (var row in crow.ChildRows.OfType<EditorRow>().ToList())
                        if (!editableProperties.ContainsKey(row.Properties.FieldName))
                            crow.ChildRows.Remove(row);
                    if (crow.ChildRows.Count == 0)
                        pUserSettings.Rows.Remove(crow);
                }

                foreach (var row in pUserSettings.Rows.OfType<EditorRow>().ToList())
                    if (!editableProperties.ContainsKey(row.Properties.FieldName))
                        pUserSettings.Rows.Remove(row);
            }

            // process the required width
            pUserSettings.LayoutChanged();
            var vi = pUserSettings.ViewInfo;
            var lastRowInfo = vi.RowsViewInfo[vi.RowsViewInfo.Count - 1];
            var firstRowInfo = vi.RowsViewInfo[0];
            var requiredSize = new Size(lastRowInfo.RowRect.Right  + firstRowInfo.RowRect.X,
                                        lastRowInfo.RowRect.Bottom + firstRowInfo.RowRect.Y);

            var dw = panelControl1.Width - pUserSettings.Width;
            panelControl1.MinimumSize = new Size(requiredSize.Width + dw, 0);
            BeginInvoke((Action)(() => panelControl1.MinimumSize = Size.Empty));
        }

        private void pUserSettings_ShowingEditor(object sender, CancelEventArgs e)
        {
            e.Cancel = !UserCanEdit;
        }
    }
}
