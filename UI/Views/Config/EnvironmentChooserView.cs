﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using TechnicalTools.Diagnostics;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;

using ApplicationBase.Deployment;
using ApplicationBase.Deployment.Data;
using ApplicationBase.DAL.Users;

using Config = ApplicationBase.Business.Config;


namespace ApplicationBase.UI
{
    public partial class EnvironmentChooserView : ApplicationBaseView
    {
        Deployer Deployer { get { return (Deployer)BusinessObject; } }
        readonly IDefaultMainFormBehavior _mainFormBehavior;

        [Obsolete("For designer only", true)]
        protected EnvironmentChooserView()
            : this(null, null)
        {
        }
        public EnvironmentChooserView(Deployer deployer, IDefaultMainFormBehavior mainFormBehavior)
            : base(deployer, Feature.AllowEverybody)
        {
            _mainFormBehavior = mainFormBehavior;
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            Text = "Connect to another environment";
        }

        void ChangeEnvironmentView_Load(object sender, EventArgs _)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            var frm = FindForm();
            Debug.Assert(frm != null);
            frm.Shown += ChangeEnvironmentView_Shown;
        }

        void ChangeEnvironmentView_Shown(object sender, EventArgs _)
        {
            this.ShowBusyWhileDoing("Gathering environment informations",
                pr => Deployer.GetAvailableEnvironmentsForUser(pr))
                .ContinueWithUIWorkOnSuccess(DisplaySource);
        }

        private void DisplaySource(List<EnvironmentConfig> envs)
        {
            // Note: current could be null if user changed his profile after he started Application as admin
            // We refilter by domain because not sure the instance are the same (different mapper at login time...)
            var curEnv = Config.Instance.Environment;
            var current = envs.SingleOrDefault(env => env.Domain == curEnv.Domain
                                                   && env.Name == curEnv.Name);
            if (current == null)
            {
                current = Config.Instance.Environment.Clone();
                current.Name += " <Not recognised on current ecosystem!>";
                current.Id = -1; // flag "current" as invalid (used on button ok)
                envs.Add(current);
            }

            lueEnvs.FillWithObjects(envs.ToList(), env => env.Domain.ToString());
            if (current == null)
                lueEnvs.UnselectAnyObject();
            else
                lueEnvs.SelectedValueAsObjectSet(current);


            lueEnvs.CustomDisplayText += (__, e) =>
            {
                if (e.Value == null)
                    return;
                var env = (EnvironmentConfig)((LookUpEdit_ExtensionsForObjects.IItem)e.Value).GetObject();
                e.DisplayText = env.Domain.ToString() + " - " + env.Name;
            };
            lueEnvs.Properties.Columns[0].Caption = "Domain";
            lueEnvs.Properties.Columns[0].SortOrder = DevExpress.Data.ColumnSortOrder.None; // must be sync with the order by above
            lueEnvs.Properties.ShowHeader = true;
            lueEnvs.GetNotInListValue += (__, e) =>
            {
                if (e.FieldName == nameof(EnvironmentConfig.Name))
                    //if (e.RecordIndex == 0) // handle empty item
                    //    e.Value = string.Empty;
                    //else
                    e.Value = envs[e.RecordIndex/* - 1*/].Name;
            };
            lueEnvs.Properties.Columns.Add(new LookUpColumnInfo(nameof(EnvironmentConfig.Name), "Name") { AllowSort = DefaultBoolean.True });
        }

        private void btnGenerateShortcut_Click(object sender, EventArgs e)
        {
            var env    = lueEnvs.SelectedValueAsObject<EnvironmentConfig>();
            if (env == null || env.Id < 0)
            {
                XtraMessageBox.Show(FindForm(), InvalidEnvironmentMessage);
                return;
            }

            Deployer.CreateDesktopShortcut(env.Domain, env.Name, "..");
        }
        const string InvalidEnvironmentMessage = "This environment is invalid! Please select another one.";
        private void btnConnect_Click(object sender, EventArgs e)
        {
            var env = lueEnvs.SelectedValueAsObject<EnvironmentConfig>();
            if (env == null || env.Id < 0)
            {
                XtraMessageBox.Show(FindForm(), InvalidEnvironmentMessage);
                return;
            }

            // Check user has a good version of Application to do so
            if (DebugTools.IsForDevelopper)
                Deployer.StartVersionForDevelopper(env.Domain, env.Name);
            else
            {
                var forCurrentVersion = env.Domain == Config.Instance.Environment.Domain && env.Name == Config.Instance.Environment.Name;
                var lastVersion = Deployer.AReleaseMustBeInstalled(env, forCurrentVersion);
                if (lastVersion == null || _mainFormBehavior.DeployVersion(Deployer, this, env, lastVersion))
                    Deployer.StartVersion(env.Domain, env.Name);
                else
                    return;
            }
            var frm = EnhancedXtraForm.MainForm;
            if (frm != null)
                frm.Close();
            else
                Environment.Exit(0);
        }
    }
}
