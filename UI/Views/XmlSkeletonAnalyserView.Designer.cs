﻿namespace ApplicationBase.UI
{
    partial class XmlSkeletonAnalyserView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUserStatement = new DevExpress.XtraEditors.LabelControl();
            this.wpfHoster = new System.Windows.Forms.Integration.ElementHost();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.tslSpaceFiller = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslPosition = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblUserStatement
            // 
            this.lblUserStatement.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblUserStatement.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblUserStatement.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.lblUserStatement.Appearance.Options.UseBackColor = true;
            this.lblUserStatement.Appearance.Options.UseFont = true;
            this.lblUserStatement.Location = new System.Drawing.Point(158, 265);
            this.lblUserStatement.Name = "lblUserStatement";
            this.lblUserStatement.Size = new System.Drawing.Size(497, 23);
            this.lblUserStatement.TabIndex = 1;
            this.lblUserStatement.Text = "Please, drag and drop multiple files to run skeleton analysis";
            // 
            // wpfHoster
            // 
            this.wpfHoster.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wpfHoster.Location = new System.Drawing.Point(0, 0);
            this.wpfHoster.Name = "wpfHoster";
            this.wpfHoster.Size = new System.Drawing.Size(860, 518);
            this.wpfHoster.TabIndex = 2;
            this.wpfHoster.Text = "elementHost1";
            this.wpfHoster.Child = null;
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslSpaceFiller,
            this.tslPosition});
            this.statusBar.Location = new System.Drawing.Point(0, 496);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(860, 22);
            this.statusBar.TabIndex = 7;
            this.statusBar.Text = "statusStrip1";
            // 
            // tslSpaceFiller
            // 
            this.tslSpaceFiller.Name = "tslSpaceFiller";
            this.tslSpaceFiller.Size = new System.Drawing.Size(708, 17);
            this.tslSpaceFiller.Spring = true;
            // 
            // tslPosition
            // 
            this.tslPosition.Name = "tslPosition";
            this.tslPosition.Size = new System.Drawing.Size(137, 17);
            this.tslPosition.Text = "Ln : 0    Col : 0    Sel : 0 | 0";
            // 
            // XmlSkeletonAnalyserView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.lblUserStatement);
            this.Controls.Add(this.wpfHoster);
            this.MinimumSize = new System.Drawing.Size(500, 100);
            this.Name = "XmlSkeletonAnalyserView";
            this.Size = new System.Drawing.Size(860, 518);
            this.Load += new System.EventHandler(this.XmlSkeletonAnalyserView_Load);
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl lblUserStatement;
        private System.Windows.Forms.Integration.ElementHost wpfHoster;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel tslSpaceFiller;
        private System.Windows.Forms.ToolStripStatusLabel tslPosition;
    }
}
