﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Input;

using TechnicalTools;
using TechnicalTools.UI.DX.Tools;
using TechnicalTools.Diagnostics;
using TechnicalTools.UI;

using ApplicationBase.Business;


namespace ApplicationBase.UI
{
    public class SplashScreenInitializer : ApplicationMainFormStarter.IApplicationInitializerBase
    {
        public string DefaultLogin { get; set; }

        ISplashScreen _splashScreen;
        readonly Action _afterInit;

        public SplashScreenInitializer(Action afterInit = null)
        {
            _afterInit = afterInit;
        }

        public void Initialize(ApplicationMainFormStarter.ISplashScreenBase splashScreen, Action<Exception, bool> onDone)
        {
            _splashScreen = (ISplashScreen)splashScreen;
            _splashScreen.Post(() => _splashScreen.EnvVisible = false);

            var bw = new BackgroundWorker();
            bw.DoWork += (_, e) =>
                {
                    //KillableBackgroundWorker.ProvisionPoolThread(32 - 1); // 16 : Nombre de vue sur position keeping (*2 si l'utilisateur reclique rapidement), -1 thread en cours !
                    e.Cancel = !ConnectToDatabase();
                    if (!e.Cancel)
                    {
                        _splashScreen.Post(DisplayApplicationInfo);
                        //Thread.Sleep(5000);
                    }
                };

            bw.RunWorkerCompleted += (o, e) =>
                {
                    if (!e.Cancelled && e.Error == null && _afterInit != null)
                        _afterInit();
                    onDone(e.Error, e.Cancelled);
                };
            bw.RunWorkerAsync();      
        }

        bool ConnectToDatabase()
        { // from a separate thread
            while (true)
            {
                string user_login = "";
                string user_password = "";
                try
                {
                    _splashScreen.Post(() => _splashScreen.Info = "Please  authenticate...");
                    if (!GetUserLoginCredential(out user_login, out user_password))
                        return false;
                    _splashScreen.Post(() => _splashScreen.Info = "Connecting to database...");
                    
                    BusinessSingletons.Instance.GetAuthenticationManager().Authenticate(user_login, user_password.IfBlankUse(Config.Instance.Debug.AutoPassword), !string.IsNullOrWhiteSpace(Config.Instance.Debug.AutoLoginWith));

                    _splashScreen.Post(() => _splashScreen.Info = "Loading data...");

                    return true;
                }
                catch (Exception exception)
                {
                    Config.Instance.Debug.AutoLoginWith = null; // did not work, make user authenticate as usual
                    _splashScreen.Post(() => _splashScreen.ShowMessageBox(exception.Message, "Connection"));
                    if (Config.Instance.Environment.Domain == ApplicationBase.Deployment.Data.eEnvironment.LocalNoDB)
                        throw; // Prevent stackoverflow 
                }
            }
        }

        bool GetUserLoginCredential(out string login, out string password)
        { // from a separate thread
            string user_login = null;
            string user_password = null;

            if (!string.IsNullOrWhiteSpace(DefaultLogin))
                _splashScreen.Post(() => _splashScreen.LoginValue = DefaultLogin);
            _splashScreen.Post(() =>
            {
                if (!string.IsNullOrWhiteSpace(_splashScreen.LoginValue))
                    _splashScreen.FocusOnPassword();
            });

            AutoResetEvent waitUserToEnterLoginInfo = new AutoResetEvent(false);
            Action onUserClickCancel = null;
            bool unexpectedCloseHappened = false;
            void unexpectedClose(object _, EventArgs __)
            {
                unexpectedCloseHappened = true;
                onUserClickCancel();
            }
            void onUserClickConnect()
            {
                user_login = _splashScreen.LoginValue; // Ne peut pas etre null
                user_password = _splashScreen.PasswordValue;
                _splashScreen.Closed -= unexpectedClose;
                waitUserToEnterLoginInfo.Set(); // debloque MakeUserEditConnectionString
            }
            onUserClickCancel = () =>
            {
                user_login = null;
                user_password = null;
                _splashScreen.Closed -= unexpectedClose;
                waitUserToEnterLoginInfo.Set(); // debloque MakeUserEditConnectionString
            };

            bool progressBarVisible = false;
            _splashScreen.Post(() =>
                {
                    _splashScreen.LoginVisible = true;
                    _splashScreen.PasswordVisible = true;
                    _splashScreen.BtnConnectionVisible = true;
                    _splashScreen.BtnQuitVisible = true;
                    progressBarVisible = _splashScreen.AnimatedProgressBarVisible;
                    _splashScreen.AnimatedProgressBarVisible = false;
                    _splashScreen.UserConnect += onUserClickConnect;
                    _splashScreen.UserQuit += onUserClickCancel;
                    _splashScreen.Closed += unexpectedClose;
                    // Auto login pour les developpeurs :-)
                    if (     DebugTools.IsForDevelopper
                            // Developper set autologin
                            && !string.IsNullOrWhiteSpace(Config.Instance.Debug.AutoLoginWith)
                            // and he did not cancel manually the autologin
                            && !Keyboard.IsKeyDown(Key.LeftCtrl) && !Keyboard.IsKeyDown(Key.RightCtrl)
                        ||
                            // or a login exists and we are not connecting to a real DB anyway... 
                            // so password is probably useless
                            Config.Instance.Environment.Domain == ApplicationBase.Deployment.Data.eEnvironment.LocalNoDB)
                    {
                        if (!string.IsNullOrWhiteSpace(_splashScreen.LoginValue))
                            onUserClickConnect();
                    }
                });
#if DEBUG
             //_splashScreen.Post(() =>
             //   {
             //       onUserClickConnect();
             //   });
#endif
            waitUserToEnterLoginInfo.WaitOne();
            waitUserToEnterLoginInfo.Dispose();

            // ingore exception if form is already close when we try to Post
            //ExceptionManager.IgnoreException<System.Threading.Tasks.TaskCanceledException>(() => 
            //{
            if (!unexpectedCloseHappened)
                _splashScreen.Post(() =>
                {
                    //_splashScreen.LoginVisible = false;
                    _splashScreen.PasswordVisible = false;
                    _splashScreen.BtnConnectionVisible = false;
                    _splashScreen.BtnQuitVisible = false;
                    _splashScreen.AnimatedProgressBarVisible = progressBarVisible;
                    _splashScreen.UserConnect -= onUserClickConnect;
                    _splashScreen.UserQuit -= onUserClickCancel;
                });
            //});

            login = user_login;
            password = null;
            if (user_login == null)
                return false;
            password = user_password;
            return true;
        }
       

        void DisplayApplicationInfo()
        {
            //var currentVersion = Application.Factory.GetConfig().Version;

            //_splashScreen.Version = String.Format("{0}.{1}.{2}", currentVersion.Major, currentVersion.Minor, currentVersion.Revision);
            //_splashScreen.BuildInfo = String.Format("build {0}", currentVersion.Build);
            //_splashScreen.VersionVisible = true;
            //_splashScreen.BuildInfoVisible = true;
            //_splashScreen.Info = "Starting...";
            //var customer = modUtilities.SystemSettings["Customer"];
            //_splashScreen.LicensingInfo = "Licensed to " + customer;
        }


    }
}
