﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

using TechnicalTools;
using TechnicalTools.Automation;
using TechnicalTools.Diagnostics;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Forms.Errors;

using DefaultLayoutRepository = TechnicalTools.UI.DX.EnhancedGridView_LayoutManager.DefaultLayoutRepository;


namespace ApplicationBase.UI
{
    public abstract class ShowMainGuiBase : Business.Automation.CommandLineTask
    {
        [Argument()]
        public bool UseDirectX { get; set; }

        public ShowMainGuiBase(Business.Config cfg, DateTime atDate)
            : base(cfg, atDate)
        {
      
        }
        protected abstract Form CreateMainForm();

        public virtual void Initialize()
        {
            ExceptionManager.Instance = new TechnicalToolsClassic.Diagnostics.DefaultExceptionManager();

            if (!DebugTools.IsForDevelopper)
                Debug.Listeners.Clear(); // Quand on livre une version debug aux utilisateurs on ne veut pas qu'il ait les assertions d'affichées

            // Ebable Directx  for all DevExpress controls except for CalendarControl and PictureEdit controls.
            // Enable DirectX make some code (about GDI+) not working anymore (API Limitations) this code will be inactive but does not throw exception
            // (read more here: https://docs.devexpress.com/WindowsForms/119441/common-features/graphics-performance-and-high-dpi/directx-hardware-acceleration#hardware)
            // There is a tool on this link to see the work to do (tracing developped feature that are now useless for example until we migrate code for using directx)
            // To check, in code, later, if directx is enabled, cast GridControl or TreeList to DevExpress.Utils.DirectXPaint.IDirectXClient interface and examine the DirectXProvider.Enabled property
            if (UseDirectX)
            {
                DevExpress.XtraEditors.WindowsFormsSettings.ForceDirectXPaint();
                DevExpress.XtraEditors.WindowsFormsSettings.EnableFormSkins();
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //DevExpress.UserSkins.BonusSkins.Register(); // No need, done by SkinApplicatorhelper

            EnhancedGridView_LayoutManager.AllEnabled = true;
            DefaultLayoutRepository.DefaultRepositoryFileName = Path.GetFullPath(Path.GetDirectoryName(Application.ExecutablePath) + @"\..") + "\\" + Path.GetFileName(DefaultLayoutRepository.DefaultRepositoryFileName);

            var defaultShowInstanceOfType = EnhancedGridView_BrowsableNestedProperties.ShowInstanceOfType;
            EnhancedGridView_BrowsableNestedProperties.ShowInstanceOfType = t =>
            {
                var res = defaultShowInstanceOfType(t);
                if (res.HasValue)
                    return res;
                if (t.RemoveNullability().Assembly == typeof(Business.Config).Assembly ||
                    t.RemoveNullability().Assembly.FullName.StartsWith(nameof(ApplicationBase) + "."))
                    return true;
                return null;
            };

            TechnicalErrorPopup.GetVersioningInfo = () => Deployment.Deployer.GetCurrentVersion();
        }

        public override int DoWork()
        {
            if (!DisplayFormOnError("Initializing UI...", Initialize))
                return ERROR;

            Form mainForm = null;
            if (!DisplayFormOnError($"Error while starting {Assembly.GetEntryAssembly().GetName().Name}!", () => { mainForm = CreateMainForm(); }))
                return ERROR;

            // If an exception occurs here at runtime (with green background)
            // and the callstack of exception contains TechnicalTools.UI.DX.ToolTipOnDisableControl.GlobalMouseHandler_MouseMovedEvent
            // You can ignore this exception, it is "by design". There is no way to completely prevent them. 
            // But they are rare and depend on your mouse position. 
            DisplayFormOnError("Critical error !", () => Application.Run(mainForm));
            return SUCCESS;
        }


        protected virtual bool DisplayFormOnError(string title, Action action)
        {
            try
            {
                action();
                return true;
            }
            catch (Exception ex)
            {
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
                {
                    var str = ExceptionManager.Instance.Format(ex);
                    MessageBox.Show(str, title);
                });
                Debugger.Launch();
                return false;
            }
        }

    }
}
        