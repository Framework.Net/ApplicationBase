﻿using System;

using TechnicalTools.UI.DX;
using TechnicalTools.UI;
using TechnicalTools.UI.DX.Helpers;

using ApplicationBase.DAL.Tools;
using ApplicationBase.DAL.Users;

using ApplicationBase.Business;

using ICommentPattern = ApplicationBase.DAL.HasPropertiesOf.Tools.CommentPattern;


namespace ApplicationBase.UI
{
    public partial class CommentPatternEditor : ApplicationBaseView
    {
        readonly GridViewForEditableSet<CommentPattern, ICommentPattern> _viewHelper;

        public CommentPatternSubSet Set { get { return (CommentPatternSubSet)BusinessObject; } }

        [Obsolete("For designer only")]
        public CommentPatternEditor() : this(null) { }

        public CommentPatternEditor(CommentPatternSubSet set)
            : base(set, Feature.AllowEverybody)
        {
            InitializeComponent();
            Text = "Comment Patterns Editor";

            _viewHelper = new GridViewForEditableSet<CommentPattern, ICommentPattern>();
        }

        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();
            // Application des droits utilisateurs
            gvCommentPatterns.OptionsBehavior.ReadOnly = !UserCanEdit;
        }

        void CommentPatternEditor_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            Configure_CommentPattern_Edition();
        }

        void Configure_CommentPattern_Edition()
        {
            _viewHelper.Install(gvCommentPatterns, Set);

            var colId = gvCommentPatterns.Columns[nameof(ICommentPattern.Id)];
            colId.Visible = false;

            var col = gvCommentPatterns.Columns[nameof(ICommentPattern.SetId)];
            gvCommentPatterns.Columns.Remove(col);

            col = gvCommentPatterns.Columns[nameof(ICommentPattern.KeyWord)];
            col.InsertAfter(colId);

            col = gvCommentPatterns.Columns[nameof(ICommentPattern.PreventAutomaticTreatment)];
            col.ToolTip = "Beware, if you change a comment pattern currently used by some line in crediting screen (not audit)," + Environment.NewLine +
                          "This setting won't work anymore for these lines!";
            if (!Set.HasRelatedAutomaticAction)
                gvCommentPatterns.Columns.Remove(col);

            gcCommentPatterns.DataSource = Set.EditingSet;
        }
    }
}

