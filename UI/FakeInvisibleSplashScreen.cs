﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace ApplicationBase.UI
{
    /// <summary>
    /// Basic implementation of ISplashScreen that connect or cancel as soon as it is shown.
    /// This splashscreen is invisible for user.
    /// </summary>
    public class FakeInvisibleSplashScreen : Form, ISplashScreen
    {
        readonly bool _simulateConnect;

        public event Action UserConnect;
        public event Action UserQuit;

        public FakeInvisibleSplashScreen(bool simulateConnect = true)
        {
            _simulateConnect = simulateConnect;
            Opacity = 0;
            StartPosition = FormStartPosition.Manual;
            SetBounds(-500, 0, 200, 200);
            ShowInTaskbar = false;
            Location = new System.Drawing.Point();
            CreateControl();
            Shown += FakeInvisibleSplasShcreen_Shown;
        }

        void FakeInvisibleSplasShcreen_Shown(object sender, EventArgs e)
        {
            if (_simulateConnect)
                UserConnect?.Invoke();
            else
                UserQuit?.Invoke();
        }

        public void Post(Action action)
        {
            BeginInvoke(action);
        }

        public void RunInNewGuiThread()
        {
            Application.Run(this);
        }

        public void ShowMessageBox(string message, string title)
        {
            MessageBox.Show(this, message, title);
        }

        public Screen CurrentScreen { get { return Screen.FromPoint(Cursor.Position); } }



        public bool VersionVisible { get; set; }
        public bool BuildInfoVisible { get; set; }
        public bool BtnQuitVisible { get; set; }
        public bool BtnConnectionVisible { get; set; }
        public bool AnimatedProgressBarVisible { get; set; }
        public bool LoginVisible { get; set; }
        public bool PasswordVisible { get; set; }
        public bool EnvVisible { get; set; }
        public string LoginValue { get; set; }

        public string PasswordValue { get; set; }

        public List<object> EnvAvailableValues { get; set; }
        public object EnvSelectedValue { get; set; }
        public string Version { get; set; }
        public string BuildInfo { get; set; }
        public string LicensingInfo { get; set; }
        public string Info { get; set; }

        public void FocusOnLogin()    { /* Nothing because we fake */ }
        public void FocusOnPassword() { /* Nothing because we fake */ }

    }

}
