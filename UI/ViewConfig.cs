﻿using System;


namespace ApplicationBase.UI
{
    public class ViewConfig : Business.ViewConfig
    {
        public bool   PreventBusyFormToBeShownAsDialog { get; set; } // Only work for developper
        public string DefaultSkin                      { get; set; }
        public string AutoUpdateCheckInterval          { get; set; } // in seconds

        protected override void CopyFrom(Business.ViewConfig source)
        {
            base.CopyFrom(source);
            if (!(source is ViewConfig from))
                return;
            PreventBusyFormToBeShownAsDialog = from.PreventBusyFormToBeShownAsDialog;
            DefaultSkin = from.DefaultSkin;
            AutoUpdateCheckInterval = from.AutoUpdateCheckInterval;
        }
    }
}
