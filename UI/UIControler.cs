﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using TechnicalTools.Tools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.UI.DX;
using ApplicationBase.Deployment;
using ApplicationBase.Deployment.Data;

using ApplicationBase.Business;
using ApplicationBase.Business.FileImporting;
using ApplicationBase.Business.DataRelation;


namespace ApplicationBase.UI
{
    public class UIControler
    {
        // Please See "Note 01" in file "Code Design Notes.txt"
        public static UIControler Instance { get; set; } = new UIControler();

        public XtraUserControlBase ShowObject(object businessObject,
                                              Control owner = null,
                                              Action onDisposed = null,
                                              Action<XtraUserControlBase> configureView = null)
        {
            var mapping = AllMappings.ToList().Find(m => m.ObjectType == businessObject.GetType())
                       ?? AllMappings.ToList().Find(m => m.ObjectType.IsAssignableFrom(businessObject.GetType()));
            if (mapping == null)
            {
                DebugTools.Break(); // You have to add a mapping for object "businessObject" like below in this file
                // Probably in a class witch has the same name "UIControler", which inherits from this class, which is declared in your assembly (if not, do it!)
                throw new NotImplementedException($"Mapping à ajouter dans {GetType().FullName}.{nameof(AllMappings)}!");
            }
            // Tente de récuperer la fenetre deja existante pour l'objet
            XtraUserControlBase view = ViewFinder.Instance.FindEditorFor(mapping.ViewType, businessObject);
            if (view == null)
            {
                // Au cas ou le service broker a un peu de mal, il faut s'assurer que editableObject est à jour si il est dans le cache
                // TODO : update businessObject

                view = mapping.CreateViewFor(businessObject);
                Debug.Assert(view != null);
                if (view is ApplicationBaseView)
                    (view as ApplicationBaseView).Owner = owner;
                else if (view is ApplicationBaseViewComposite)
                    (view as ApplicationBaseViewComposite).Owner = owner;

                // Si l'objet gère l'accès concurrentiel...
                //frm.TryKeepEditable = true;

                if (businessObject != null)
                    ViewFinder.Instance.RegisterEditor(view, businessObject);
                else
                    ViewFinder.Instance.RegisterEditor(view);

                configureView?.Invoke(view);
                view.Disposed += (_, __) => { CleanOnViewClosing.SchedulePostponedExecution();};
            }
            
            return ShowView(view, onDisposed);
        }
        PostponingExecuter CleanOnViewClosing
        {
            get
            {
                return _CleanOnViewClosing 
                    ?? (_CleanOnViewClosing = new PostponingExecuter(GC.Collect));  // NOSONAR: Because we do want the view to be collected. So in case the vie is screwed, user can reload only the view, not the data.
            }
        }
        PostponingExecuter _CleanOnViewClosing;
    

        public XtraUserControlBase ShowView(XtraUserControlBase view, Action onDisposed = null)
        {
            if (view.FindForm() == null)
            {
                if (!(EnhancedXtraForm.MainForm is IHasDockUserControl docker))
                    throw new TechnicalException(nameof(EnhancedXtraForm) + "." + nameof(EnhancedXtraForm.MainForm) + " must be assigned so we can display view " + view.Name + "!", null);
                var dock = docker.DockAsDefault(view); // affiche la nouvelle fenêtre
                void onViewTextChanged(object _, EventArgs __)
                {
                    dock.Text = view.Text;
                }
                view.TextChanged += onViewTextChanged;
                view.Disposed += (_, __) => view.TextChanged -= onViewTextChanged;
            }
            view.Focus(); // Met le focus sur la view deja existante
            
            if (onDisposed != null)
                view.Disposed += (_, __) => onDisposed();
            return view;
        }

        // Order has no importance, but to be clean we order using complexity of object IUserInteractiveObject
        // From the simplest to the more complex ones
        protected virtual IReadOnlyCollection<Mapping> AllMappings { get; } = new[]
        {
            // Base technical view
            Mapping.Create((Deployer deployer) => new DeployerView(deployer)),
            Mapping.Create((EnvironmentSettingsView.Set _) => new EnvironmentSettingsView(_)),
            Mapping.Create((Business.Logs.LogSource s) => new LoggingView(s, null, null)),
            Mapping.Create((Business.Logs.LogFilter lockedFilter) => new LoggingView(lockedFilter, null, null)),
            Mapping.Create((Business.Logs.TaskRunInfoSet ds) => new TaskRunInfosView(ds)),

            Mapping.Create((AuthenticationManager manager) => new UserRightManagementView(manager)),
            Mapping.Create((ReleaseNotifier notifier) => new WhatsNewHistoryView(notifier)),
            Mapping.Create((ReleaseNotifier.UnseenReleaNoteByUser notes) => new WhatsNewView(notes, BusinessSingletons.Instance.GetConfigOfUser())),
            Mapping.Create((ReleaseNoteWithContent note) => new ReleaseNoteView(note)),
            Mapping.Create((Business.Dialoguing.Dialoguer dial) => new Dialoguing.AppDialoguerView(dial)),

            // Base user config & tools
            Mapping.Create((ConfigOfUser cfg) => new ConfigOfUserView(cfg)),
            Mapping.Create((CommentPatternSubSet set) => new CommentPatternEditor(set)),

            // Base business readonly view
            Mapping.Create((DataExplorer explorer) => new Views.DataRelation.DataExplorerView(explorer)),
            Mapping.Create((AnyDataFileReader reader) => new AnyDataFileAnalyserView(reader)),
            Mapping.Create((XmlSkeletonAnalyser analyser) => new XmlSkeletonAnalyserView(analyser)),

        }.ToList();

        // Ce dictionaire associe le type d'un objet, au type de la view permettant de l'editer, ainsi qu'au moyen de creer une view de ce type a partir d'un objet
        protected class Mapping
        {
            public Type                              ObjectType    { get; private set; }
            public Type                              ViewType      { get; private set; }
            public Func<object, XtraUserControlBase> CreateViewFor { get; private set; }

            public static Mapping Create<TBusinessObject, TView>(Func<TBusinessObject, TView> createView)
                where TBusinessObject : IUserInteractiveObject
                where TView : XtraUserControlBase
            {
                return new Mapping()
                {
                    ObjectType = typeof(TBusinessObject),
                    ViewType = typeof(TView),
                    CreateViewFor = obj => createView((TBusinessObject)obj)
                };
            }
        }

    }
}
