﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Logs;
using TechnicalTools.UI.DX;
using TechnicalTools.Tools;

using DataMapper;

using ApplicationBase.DAL.UsersPreferences;
using ApplicationBase.Common;
using ApplicationBase.Business;


namespace ApplicationBase.UI
{
    /// <summary>
    /// Class  that handle storage of gridview layouts in DB
    /// Use:
    /// <code>EnhancedGridView_LayoutManager.CreateLayoutRepository = () => new ApplicationBase.UI.GridViewLayoutDbRepository();</code>
    /// </summary>
    public class GridViewLayoutDbRepository : EnhancedGridView_LayoutManager.ILayoutRepository
    {
        public bool   HandleSharingLayout { get { return true; } }
        public string UIKeyPath           { get; set; }

        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(GridViewLayoutDbRepository));

        public Tuple<string, bool> DefaultLayoutInfo
        {
            get { return Tuple.Create(DefaultLayout?.Item1?.LayoutName, DefaultLayout?.Item2 ?? false); }
            set
            {
                var layout = (value.Item2 ? LayoutsShared : Layouts).Single(ugl => ugl.LayoutName == value.Item1);
                DefaultLayout = Tuple.Create(layout, value.Item2);
            }
        }
        Tuple<UserGridLayout, bool> DefaultLayout
        {
            get
            {
                var personalDefaultLayout = Layouts.FirstOrDefault(lyt => lyt.DefaultSince != null);
                var sharedDefaultLayout = LayoutsShared.FirstOrDefault(lyt => lyt.DefaultSince != null);

                if (personalDefaultLayout?.DefaultSince < sharedDefaultLayout?.DefaultSince)
                {
                    BusEvents.Instance.RaiseUserUnderstandableMessage("Someone recently set a default shared layout for this grid." + Environment.NewLine + 
                                                                      "Set again your personnal layout as the default one to prevent this message to appear again.", _log);
                }
                return Tuple.Create(personalDefaultLayout ?? sharedDefaultLayout, personalDefaultLayout == null && sharedDefaultLayout != null);
            }
            set
            {
                ShowBusy(pr =>
                {
                    if (value.Item2 == false)
                    {
                        var personalDefaultLayout = Layouts.FirstOrDefault(lyt => lyt.DefaultSince != null);
                        if (personalDefaultLayout != null)
                        {
                            personalDefaultLayout.DefaultSince = null;
                            DB.Dao_Base.UpdateToDatabase(personalDefaultLayout, lyt => lyt.DefaultSince);
                        }
                    }
                    else
                    {
                        var sharedDefaultLayout = Layouts.FirstOrDefault(lyt => lyt.DefaultSince != null);
                        if (sharedDefaultLayout != null)
                        {
                            sharedDefaultLayout.DefaultSince = null;
                            DB.Dao_Base.UpdateToDatabase(sharedDefaultLayout, lyt => lyt.DefaultSince);
                        }
                    }
                    if (value.Item1 != null)
                    {
                        value.Item1.DefaultSince = DateTime.Now.TruncateUnderSeconds();
                        DB.Dao_Base.UpdateToDatabase(value.Item1, lyt => lyt.DefaultSince);
                    }
                });
            }
        }

        public List<string> GetLayoutNames(bool sharedOrNotShared)
        {
            return (sharedOrNotShared ? LayoutsShared : Layouts).Select(ugl => ugl.LayoutName).ToList();
        }

        public MemoryStream RetrieveLayout(string layoutName, bool shared)
        {
            var layout = (shared ? LayoutsShared : Layouts).Single(ugl => ugl.LayoutName == layoutName);
            return new MemoryStream(layout.LayoutContent);
        }

        public void SaveLayout(string layoutName, bool shared, MemoryStream stream)
        {
            ShowBusy(pr =>
            { 
                var layouts = shared ? LayoutsShared : Layouts;
                var layout = layouts.SingleOrDefault(ugl => ugl.LayoutName == layoutName);
                bool isNew = layout == null;
                var mgr = BusinessSingletons.Instance.GetAuthenticationManager();
                layout = layout ?? new UserGridLayout()
                             {
                                 Person_Id = (shared ? mgr.NoRightLogin : mgr.CurrentUser).Person_Id,
                                 GridKey = UIKeyPath,
                                 LayoutName = layoutName
                             };
                Debug.Assert(stream.Position == 0);
                layout.LayoutContent = stream.ToArray();
                if (isNew)
                {
                    DB.Dao_Base.CreateInDatabase(layout);
                    layouts.Add(layout);
                }
                else
                    DB.Dao_Base.UpdateToDatabase(layout);
            });
        }
        public void DeleteLayout(string layoutName, bool shared)
        {
            ShowBusy(pr =>
            {
                var layouts = shared ? LayoutsShared : Layouts;
                var layout = layouts.Single(ugl => ugl.LayoutName == layoutName);
                DB.Dao_Base.DeleteInDatabase(layout);
                layouts.Remove(layout);
            });
        }

        List<UserGridLayout> Layouts
        {
            get
            {
                if (_Layouts == null)
                {
                    BusinessSingletons.Instance.GetAuthenticationManager().CurrentUserChanged += () => _Layouts = null;
                    ShowBusy((IProgress<string> pr) =>
                    {
                        string sqlFilter = DB.Dao_Base.GetColumnNameFor((UserGridLayout ugl) => ugl.Person_Id) + " = " + DbMappedField.ToSql((object)BusinessSingletons.Instance.GetAuthenticationManager().CurrentUser.Person_Id)
                                         + " and " 
                                         + DB.Dao_Base.GetColumnNameFor((UserGridLayout ugl) => ugl.GridKey) + " = " + DbMappedField.ToSql((object)UIKeyPath);
                        _Layouts = DB.Dao_Base.LoadCollection<UserGridLayout>(sqlFilter);
                    });
                }
                return _Layouts;
            }
        }
        List<UserGridLayout> _Layouts;
        List<UserGridLayout> LayoutsShared
        {
            get
            {
                if (_LayoutsShared == null)
                {
                    var mgr = BusinessSingletons.Instance.GetAuthenticationManager();
                    mgr.CurrentUserChanged += () => _LayoutsShared = null;
                    ShowBusy((IProgress<string> pr) =>
                    {
                        string sqlFilter = DB.Dao_Base.GetColumnNameFor((UserGridLayout ugl) => ugl.Person_Id) + " = " + DbMappedField.ToSql(mgr.NoRightLogin.Person_Id)
                                         + " and "
                                         + DB.Dao_Base.GetColumnNameFor((UserGridLayout ugl) => ugl.GridKey) + " = " + DbMappedField.ToSql(UIKeyPath);
                        _LayoutsShared = DB.Dao_Base.LoadCollection<UserGridLayout>(sqlFilter);
                    });
                }
                return _LayoutsShared;
            }
        }
        List<UserGridLayout> _LayoutsShared;

        
        void ShowBusy(Action<IProgress<string>> action) { ShowBusy(null, action); }
        void ShowBusy(string actionName, Action<IProgress<string>> action)
        {
            var frm = EnhancedXtraForm.MainForm;
            if (frm != null && !_showInProgress)
            {
                _showInProgress = true;
                frm.ShowBusyWhileDoingUIWorkInPlace(actionName ?? "Applying changes", action);
                _showInProgress = false;
            }
            else
                action(new Progress<string>());
        }
        bool _showInProgress;
    }
}
