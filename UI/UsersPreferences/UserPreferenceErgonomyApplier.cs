using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Diagnostics;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.BaseClasses;

using ApplicationBase.Business;


namespace ApplicationBase.UI
{
    public class UserPreferenceErgonomyApplier
    {
        protected readonly ConfigOfUser _config;

        protected readonly XtraForm _frm;

        public UserPreferenceErgonomyApplier(ConfigOfUser config, XtraForm frm)
        {
            _frm = frm;
            _config = config;
            _config.PropertyChanged += OnConfigPropertyChanged;

            if (frm.Visible)
                Setup();
            else
            {
                void onFirstVisibleChanged(object _, EventArgs __)
                {
                    // This condition may be true if there was some issue at the startup with visibility
                    // (see ApplicationMainFormStarter, at bottom of method OnSplashScreenFinishInitialization)
                    if (!frm.Visible)
                        return;
                    frm.VisibleChanged -= onFirstVisibleChanged;
                    if (frm.Visible)
                        Setup();
                }
                frm.Disposed += (_, __) => frm.VisibleChanged -= onFirstVisibleChanged;
                frm.VisibleChanged += onFirstVisibleChanged;
            }
        }

        public virtual void Setup()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            BusinessSingletons.Instance.GetAuthenticationManager().CurrentUserChanged -= ApplyUIUserPreferences;
            BusinessSingletons.Instance.GetAuthenticationManager().CurrentUserChanged += ApplyUIUserPreferences;
            _frm.Disposed += (_, __) => BusinessSingletons.Instance.GetAuthenticationManager().CurrentUserChanged -= ApplyUIUserPreferences;
            
            ApplyUIUserPreferences();
        }
        
        protected virtual void ApplyUIUserPreferences()
        {
            var tolerantToEasterEgg = _tolerantToEasterEgg;
            _tolerantToEasterEgg = false;
            if (!tolerantToEasterEgg || // If it is not the first time (when we are tolerant to easter egg)
                SkinApplicatorHelper.GetTheoricalCurrentSkin() == null) // or no easter eggs happened
                OnConfigPropertyChanged(_config, new PropertyChangedEventArgs(nameof(ConfigOfUser.Application_DefaultSkin)));
            OnConfigPropertyChanged(_config, new PropertyChangedEventArgs(nameof(ConfigOfUser.Application_MaxConcurrentParallelBusinessWorkers)));
            OnConfigPropertyChanged(_config, new PropertyChangedEventArgs(nameof(ConfigOfUser.GridView_OptionsBehavior_AutoExpandAllGroups)));
        }
        bool _tolerantToEasterEgg = true;

        protected virtual void OnConfigPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ConfigOfUser.Application_DefaultSkin))
                SkinApplicatorHelper.SetCurrentSkin(_config.Application_DefaultSkin ?? SkinApplicatorHelper.DefaultSkin);

            if (e.PropertyName == nameof(ConfigOfUser.Application_MaxConcurrentParallelBusinessWorkers))
                TechnicalTools.IList_Extensions.MaxConcurrentParallelBusinessWorkers = _config.Application_MaxConcurrentParallelBusinessWorkers ?? TechnicalTools.IList_Extensions.DefaultMaxConcurrentParallelBusinessWorkers;

            if (e.PropertyName == nameof(ConfigOfUser.GridView_OptionsBehavior_AutoExpandAllGroups))
                RefreshOnAllControls((Control ctl) =>
                {
                    if (ctl is GridControl)
                        foreach (var gv in (ctl as GridControl).Views.OfType<GridView>())
                            if (_config.GridView_OptionsBehavior_AutoExpandAllGroups != null)
                                gv.OptionsBehavior.AutoExpandAllGroups = _config.GridView_OptionsBehavior_AutoExpandAllGroups.Value;
                });
        }
        /// <summary>
        /// An helper method
        /// </summary>
        protected void RefreshOnAllControls(Action<Control> refresh)
        {
            foreach (Form frm in Application.OpenForms)
            {
                Action a = () =>
                {
                    foreach (var ctl in frm.AllSubControls())
                        refresh(ctl);
                };
                // it is possible if user run a form in another thread with Application.Run
                if (frm.InvokeRequired)
                    frm.BeginInvoke(a);
                else
                    a();
            }
        }

    }
}
