﻿namespace ApplicationBase.UI
{
    partial class CommentPatternSelectorPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gcCommentPatterns = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvCommentPatterns = new TechnicalTools.UI.DX.EnhancedGridView();
            this.colComment = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colKeyword = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colTreatedByAutoProcess = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.repoGridButtonRemove = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.grpEntries = new DevExpress.XtraLayout.LayoutControlGroup();
            this.Entry1LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.Entry2LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.Entry3LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.Entry4LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.Entry5LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.gcCommentPatterns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCommentPatterns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoGridButtonRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpEntries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Entry1LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Entry2LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Entry3LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Entry4LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Entry5LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // gcCommentPatterns
            // 
            this.gcCommentPatterns.Location = new System.Drawing.Point(12, 29);
            this.gcCommentPatterns.MainView = this.gvCommentPatterns;
            this.gcCommentPatterns.Name = "gcCommentPatterns";
            this.gcCommentPatterns.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repoGridButtonRemove});
            this.gcCommentPatterns.Size = new System.Drawing.Size(886, 254);
            this.gcCommentPatterns.TabIndex = 0;
            this.gcCommentPatterns.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvCommentPatterns});
            // 
            // gvCommentPatterns
            // 
            this.gvCommentPatterns.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colComment,
            this.colKeyword,
            this.colTreatedByAutoProcess});
            this.gvCommentPatterns.GridControl = this.gcCommentPatterns;
            this.gvCommentPatterns.Name = "gvCommentPatterns";
            this.gvCommentPatterns.OptionsBehavior.Editable = false;
            this.gvCommentPatterns.OptionsBehavior.ReadOnly = true;
            this.gvCommentPatterns.OptionsView.ShowAutoFilterRow = true;
            this.gvCommentPatterns.OptionsView.ShowGroupPanel = false;
            this.gvCommentPatterns.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvCommentPatterns_FocusedRowChanged);
            this.gvCommentPatterns.ColumnFilterChanged += new System.EventHandler(this.gvCommentPatterns_ColumnFilterChanged);
            this.gvCommentPatterns.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gvCommentPatterns_MouseDown);
            // 
            // colComment
            // 
            this.colComment.Caption = "   Comment";
            this.colComment.Name = "colComment";
            this.colComment.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colComment.OptionsFilter.ImmediateUpdateAutoFilter = false;
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 1;
            this.colComment.Width = 755;
            // 
            // colKeyword
            // 
            this.colKeyword.AppearanceHeader.Options.UseTextOptions = true;
            this.colKeyword.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKeyword.Caption = "Keyword";
            this.colKeyword.Name = "colKeyword";
            this.colKeyword.Visible = true;
            this.colKeyword.VisibleIndex = 0;
            this.colKeyword.Width = 113;
            // 
            // colTreatedByAutoProcess
            // 
            this.colTreatedByAutoProcess.AppearanceHeader.Options.UseTextOptions = true;
            this.colTreatedByAutoProcess.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreatedByAutoProcess.Caption = "Treated by AutoProcess";
            this.colTreatedByAutoProcess.Name = "colTreatedByAutoProcess";
            this.colTreatedByAutoProcess.Visible = true;
            this.colTreatedByAutoProcess.VisibleIndex = 2;
            // 
            // repoGridButtonRemove
            // 
            this.repoGridButtonRemove.AutoHeight = false;
            this.repoGridButtonRemove.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Remove", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repoGridButtonRemove.Name = "repoGridButtonRemove";
            this.repoGridButtonRemove.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnEdit);
            this.layoutControl1.Controls.Add(this.textEdit5);
            this.layoutControl1.Controls.Add(this.textEdit4);
            this.layoutControl1.Controls.Add(this.textEdit3);
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.textEdit1);
            this.layoutControl1.Controls.Add(this.btnOk);
            this.layoutControl1.Controls.Add(this.btnCancel);
            this.layoutControl1.Controls.Add(this.gcCommentPatterns);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1010, 180, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(910, 483);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(400, 449);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(144, 22);
            this.btnEdit.StyleController = this.layoutControl1;
            this.btnEdit.TabIndex = 8;
            this.btnEdit.Text = "Edit patterns";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(82, 413);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(804, 20);
            this.textEdit5.StyleController = this.layoutControl1;
            this.textEdit5.TabIndex = 5;
            this.textEdit5.EditValueChanged += new System.EventHandler(this.anyTextEdit_EditValueChanged);
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(82, 389);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(804, 20);
            this.textEdit4.StyleController = this.layoutControl1;
            this.textEdit4.TabIndex = 4;
            this.textEdit4.EditValueChanged += new System.EventHandler(this.anyTextEdit_EditValueChanged);
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(82, 365);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(804, 20);
            this.textEdit3.StyleController = this.layoutControl1;
            this.textEdit3.TabIndex = 3;
            this.textEdit3.EditValueChanged += new System.EventHandler(this.anyTextEdit_EditValueChanged);
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(82, 341);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(804, 20);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 2;
            this.textEdit2.EditValueChanged += new System.EventHandler(this.anyTextEdit_EditValueChanged);
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(82, 317);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(804, 20);
            this.textEdit1.StyleController = this.layoutControl1;
            this.textEdit1.TabIndex = 1;
            this.textEdit1.EditValueChanged += new System.EventHandler(this.anyTextEdit_EditValueChanged);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(750, 449);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(148, 22);
            this.btnOk.StyleController = this.layoutControl1;
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(12, 449);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(143, 22);
            this.btnCancel.StyleController = this.layoutControl1;
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.simpleLabelItem1,
            this.grpEntries,
            this.layoutControlItem4,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(910, 483);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gcCommentPatterns;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(890, 258);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnCancel;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 437);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(147, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(147, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(147, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnOk;
            this.layoutControlItem3.Location = new System.Drawing.Point(738, 437);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(152, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(152, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(152, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(536, 437);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(202, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Size = new System.Drawing.Size(890, 17);
            this.simpleLabelItem1.Text = "Instant comment chooser :";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(129, 13);
            // 
            // grpEntries
            // 
            this.grpEntries.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.grpEntries.ExpandButtonVisible = true;
            this.grpEntries.ExpandOnDoubleClick = true;
            this.grpEntries.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Entry1LayoutItem,
            this.Entry2LayoutItem,
            this.Entry3LayoutItem,
            this.Entry4LayoutItem,
            this.Entry5LayoutItem});
            this.grpEntries.Location = new System.Drawing.Point(0, 275);
            this.grpEntries.Name = "grpEntries";
            this.grpEntries.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.grpEntries.Size = new System.Drawing.Size(890, 162);
            this.grpEntries.Text = "Entries";
            // 
            // Entry1LayoutItem
            // 
            this.Entry1LayoutItem.Control = this.textEdit1;
            this.Entry1LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.Entry1LayoutItem.Name = "Entry1LayoutItem";
            this.Entry1LayoutItem.Size = new System.Drawing.Size(866, 24);
            this.Entry1LayoutItem.Text = "ValueTitle 1";
            this.Entry1LayoutItem.TextSize = new System.Drawing.Size(55, 13);
            // 
            // Entry2LayoutItem
            // 
            this.Entry2LayoutItem.Control = this.textEdit2;
            this.Entry2LayoutItem.Location = new System.Drawing.Point(0, 24);
            this.Entry2LayoutItem.Name = "Entry2LayoutItem";
            this.Entry2LayoutItem.Size = new System.Drawing.Size(866, 24);
            this.Entry2LayoutItem.Text = "ValueTitle 2";
            this.Entry2LayoutItem.TextSize = new System.Drawing.Size(55, 13);
            // 
            // Entry3LayoutItem
            // 
            this.Entry3LayoutItem.Control = this.textEdit3;
            this.Entry3LayoutItem.Location = new System.Drawing.Point(0, 48);
            this.Entry3LayoutItem.Name = "Entry3LayoutItem";
            this.Entry3LayoutItem.Size = new System.Drawing.Size(866, 24);
            this.Entry3LayoutItem.Text = "ValueTitle 3";
            this.Entry3LayoutItem.TextSize = new System.Drawing.Size(55, 13);
            // 
            // Entry4LayoutItem
            // 
            this.Entry4LayoutItem.Control = this.textEdit4;
            this.Entry4LayoutItem.Location = new System.Drawing.Point(0, 72);
            this.Entry4LayoutItem.Name = "Entry4LayoutItem";
            this.Entry4LayoutItem.Size = new System.Drawing.Size(866, 24);
            this.Entry4LayoutItem.Text = "ValueTitle 4";
            this.Entry4LayoutItem.TextSize = new System.Drawing.Size(55, 13);
            // 
            // Entry5LayoutItem
            // 
            this.Entry5LayoutItem.Control = this.textEdit5;
            this.Entry5LayoutItem.Location = new System.Drawing.Point(0, 96);
            this.Entry5LayoutItem.Name = "Entry5LayoutItem";
            this.Entry5LayoutItem.Size = new System.Drawing.Size(866, 24);
            this.Entry5LayoutItem.Text = "ValueTitle 5";
            this.Entry5LayoutItem.TextSize = new System.Drawing.Size(55, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnEdit;
            this.layoutControlItem4.Location = new System.Drawing.Point(388, 437);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(148, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(148, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(148, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(147, 437);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(241, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // CommentPatternSelectorPopup
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(910, 483);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.None;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CommentPatternSelectorPopup";
            this.Text = "Please select a comment pattern or type its keyword to autoselect it";
            this.Load += new System.EventHandler(this.CommentPatternSelectorPopup_Load);
            this.Shown += new System.EventHandler(this.CommentPatternSelectorPopup_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gcCommentPatterns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCommentPatterns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoGridButtonRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpEntries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Entry1LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Entry2LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Entry3LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Entry4LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Entry5LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.DX.EnhancedGridControl gcCommentPatterns;
        private TechnicalTools.UI.DX.EnhancedGridView gvCommentPatterns;
        private TechnicalTools.UI.DX.EnhancedGridColumn colComment;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup grpEntries;
        private DevExpress.XtraLayout.LayoutControlItem Entry1LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem Entry2LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem Entry3LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem Entry4LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem Entry5LayoutItem;
        private TechnicalTools.UI.DX.EnhancedGridColumn colKeyword;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repoGridButtonRemove;
        private TechnicalTools.UI.DX.EnhancedGridColumn colTreatedByAutoProcess;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
    }
}