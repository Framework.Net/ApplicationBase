﻿using System;
using System.Drawing;

using ApplicationBase.Deployment.Data;


namespace ApplicationBase.UI
{
    public interface IGraphicalChart
    {
        Color GetColor(eEnvironment env);

        Color ColorFulBlinkingForUpdateAvailable { get; }
    }
}
