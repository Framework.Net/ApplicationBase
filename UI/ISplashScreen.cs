﻿using System;
using System.Collections.Generic;

using TechnicalTools.UI.DX.Tools;


namespace ApplicationBase.UI
{
    /// <summary>
    /// Define what a splashscreen must implement to be handled by SplashScreenInitializer
    /// See <see cref="FakeInvisibleSplashScreen">FakeInvisibleSplashScreen</see> for the most basic example.
    /// </summary>
    public interface ISplashScreen : ApplicationMainFormStarter.ISplashScreenBase
    {
        bool VersionVisible             { get; set; }
        bool BuildInfoVisible           { get; set; }
        bool BtnQuitVisible             { get; set; }
        bool BtnConnectionVisible       { get; set; }
        bool AnimatedProgressBarVisible { get; set; }
        bool LoginVisible               { get; set; }
        bool PasswordVisible            { get; set; }
        bool EnvVisible                 { get; set; }

        string       LoginValue         { get; set; }
        string       PasswordValue      { get; }
        List<object> EnvAvailableValues { get; set; }
        object       EnvSelectedValue   { get; set; }

        string Version       { get; set; }
        string BuildInfo     { get; set; }
        string LicensingInfo { get; set; }
        string Info          { get; set; }

        event Action UserConnect;
        event Action UserQuit;

        void FocusOnLogin();
        void FocusOnPassword();
        void ShowMessageBox(string message, string title);
        //bool IsClosed { get; }
    }
}
