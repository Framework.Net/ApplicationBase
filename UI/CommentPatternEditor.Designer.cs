﻿namespace ApplicationBase.UI
{
    partial class CommentPatternEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcCommentPatterns = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvCommentPatterns = new TechnicalTools.UI.DX.EnhancedGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcCommentPatterns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCommentPatterns)).BeginInit();
            this.SuspendLayout();
            // 
            // gcCommentPatterns
            // 
            this.gcCommentPatterns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcCommentPatterns.Location = new System.Drawing.Point(0, 0);
            this.gcCommentPatterns.MainView = this.gvCommentPatterns;
            this.gcCommentPatterns.Name = "gcCommentPatterns";
            this.gcCommentPatterns.Size = new System.Drawing.Size(910, 483);
            this.gcCommentPatterns.TabIndex = 0;
            this.gcCommentPatterns.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvCommentPatterns});
            // 
            // gvCommentPatterns
            // 
            this.gvCommentPatterns.GridControl = this.gcCommentPatterns;
            this.gvCommentPatterns.Name = "gvCommentPatterns";
            // 
            // CommentPatternEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcCommentPatterns);
            this.Name = "CommentPatternEditor";
            this.Size = new System.Drawing.Size(910, 483);
            this.Load += new System.EventHandler(this.CommentPatternEditor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcCommentPatterns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCommentPatterns)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.DX.EnhancedGridControl gcCommentPatterns;
        private TechnicalTools.UI.DX.EnhancedGridView gvCommentPatterns;
    }
}