﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;

using TechnicalTools.Diagnostics;

using DataMapper.Tools;


namespace DataMapper
{
    public class ServerNotifier : IDisposable
    {
//        public static ServerNotifier Instance;
        readonly string _connectionStringListener;
        readonly string _connectionStringStarter;


        // Le service SqlDependency doit utiliser une connection personalisée (et pas celle de l'application)
        // car cette derniere provoque énormément d'évènement de type Isolation
        public ServerNotifier(string connectionStringListener, string connectionStringStarter, bool forTest = false)
        {
            //if (!forTest && Instance != null)
            //    throw new Exception("Only one instance allowed");
            _connectionStringListener = connectionStringListener;
            _connectionStringStarter = connectionStringStarter;
            //Instance = this;
        }

        public bool IsReady
        {
            get
            {
                return _isReady;
            }
        }

        volatile bool _isReady;

        public bool Start()
        {
            if (!_isReady)
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
                {
                    SqlRetry.Instance.HandleRetry("Starting listening of event in DB", true, () =>
                        _isReady = SqlDependency.Start(_connectionStringStarter));
                });
            return _isReady;
        }
        public void Stop()
        {
            ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
            {
                SqlDependency.Stop(_connectionStringStarter);
                _isReady = false;
            });
        }

        public void Dispose()
        {
            Stop();
        }

        /// <summary>
        /// S'abonne au modification de résultat d'une requete SQL
        /// La requete ne doit contenir ni étoile et les noms de table doivent être au format schema.nomdeTable
        /// Garder la requete le plus simple possible. qui retourne le minimum d'info possible pour detecter tous les cas de modification qui vous interesse.
        /// Il n'est pas possible d'utiliser TOP. (TODO : pour ça essayer http://stackoverflow.com/questions/29781639/sql-dependency-and-data-refering-in-sql)
        /// </summary>
        public void Listen(string sqlQuery, SynchronizationContext context, OnChangeEventHandler onChangeAction, int waitBeforeNextSubscribe = 0)
        {
            Listen(() => { return sqlQuery; }, context, onChangeAction, waitBeforeNextSubscribe);
        }

        public void Listen(Func<string> sqlQueryDelegate, SynchronizationContext context, OnChangeEventHandler onChangeAction, int waitBeforeNextSubscribe = 0)
        {
            void subscribe()
            {
                var sqlQuery = sqlQueryDelegate();
                if (string.IsNullOrEmpty(sqlQuery))
                    return;

                using (var connection = SqlConnectionTracer.Create(_connectionStringListener))
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.OpenOrWaitUntilOpen();

                    using (var command = new SqlCommand(sqlQuery, connection))
                    {
                        var dependency = new SqlDependency(command, null, 24*60*60);
                        // This should not last more than 2 minutes or Sql Server will bug
                        // (cf http://rusanu.com/2008/01/04/sqldependencyonchange-callback-timing/)
                        // So using BeginInvoke or Post, both asynchronous action) is good
                        dependency.OnChange += (_, e) =>
                        {
                            if (!_isReady)
                                return;
                            if (waitBeforeNextSubscribe > 0)
                                Thread.Sleep(waitBeforeNextSubscribe);
                            if (context == null)
                                subscribe();
                            else
                                context.Post(__ => subscribe(), null); // raison pourquoi on invoke (sur le context de la gui en general):
                            ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => onChangeAction(this, e));
                        };

                        command.ExecuteNonQuery();
                    }
                }
            }
            if (context == null)
                subscribe();
            else
                context.Post(__ => subscribe(), null); // raison pourquoi on invoke (sur le context de la gui en general): http://rusanu.com/2008/01/04/sqldependencyonchange-callback-timing/
        }
    }
}
