﻿using System;
using System.ComponentModel.DataAnnotations;


namespace DataMapper
{
    // Note : we can use this if length is not variable
    // DbMaxLength(3, MinimumLength = 3)
    [AttributeUsage(AttributeTargets.Property)]
    public class DbMaxLength : StringLengthAttribute
    {
        public DbMaxLength(int maxLength)
            : base(maxLength)
        {
        }
        public DbMaxLength(int minLength, int maxLength)
            : base(maxLength)
        {
            MinimumLength = minLength;
        }
    }
}
