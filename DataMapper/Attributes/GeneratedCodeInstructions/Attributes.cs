﻿using System;


namespace DataMapper
{
    public interface IGeneratedCodeInstructionAttribute
    {
    }

    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
    public class GeneratedAssemblyAttribute : AssemblyFacets, IGeneratedCodeInstructionAttribute
    {
        /// <summary> Indicate the assembly concerned by this attribute </summary>
        public string For { get { return ForAssembly; } set { ForAssembly = value; } }
    }


    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class GeneratedClassAttribute : ClassFacets, IGeneratedCodeInstructionAttribute
    {
        /// <summary> Indicate the class concerned by this attribute </summary>
        public string For       { get { return ForClass; } set { ForClass = value; } }

        public GeneratedClassAttribute() : base(null) { }
    }

    public class GeneratedClassIsBaseDTOAttribute : GeneratedClassAttribute
    {
        public GeneratedClassIsBaseDTOAttribute() : this(null) { }
        public GeneratedClassIsBaseDTOAttribute(params string[] identityKeys)
        {
            BaseClassKind = eClassKind.BaseDTO;
            IdentityKeys = identityKeys ?? new string[] { }; // identityKeys can be empty, it will be found automaticlaly later
        }
    }

    public class GeneratedClassIsSmallTableAttribute : GeneratedClassIsBaseDTOAttribute
    {
        public GeneratedClassIsSmallTableAttribute() : this(null) { }
        public GeneratedClassIsSmallTableAttribute(params string[] identityKeys)
        {
            IdentityKeys = identityKeys ?? new string[] { };
            BaseClassKind = eClassKind.SmallTableInMemory;
        }
    }

    public class GeneratedClassIsDynamicEnumAttribute : GeneratedClassIsSmallTableAttribute
    {
        public GeneratedClassIsDynamicEnumAttribute(string businessNamePropertyName, string idPropertyName = null)
             : base(idPropertyName == null ? null : new[] { idPropertyName })
        {
            BaseClassKind = eClassKind.DynamicEnum;
            BusinessNamePropertyName = businessNamePropertyName;
            if (BusinessNamePropertyName == null && !TechnicalTools.UI.DesignTimeHelper.IsInDesignMode)
                throw new Exception($"Argument \"{nameof(businessNamePropertyName)}\" is mandatory!");
        }
    }


    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property, AllowMultiple = true)]
    public class GeneratedPropertyAttribute : PropertyFacets
    {
        /// <summary> Indicate the property concerned by this attribute </summary>
        public string For { get { return ForProperty; } set { ForProperty = value; } }

        // A more convenient name for ProjectedType
        public Type EnumType { get => ProjectedType; set => ProjectedType = value; }

        public GeneratedPropertyAttribute() : base(null) { }
    }
}
