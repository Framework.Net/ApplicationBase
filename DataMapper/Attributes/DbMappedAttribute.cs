﻿using System;
using System.Reflection;


namespace DataMapper
{
    public abstract partial class DbMappedAttribute : Attribute, IHasMapperOwner
    {
        public IDbMapper     Mapper       { get; internal set; }
        public DbMappedTable Table        { get; internal set; }
    }
    public abstract partial class DbMappedPropertyAttribute : DbMappedAttribute
    {
        public PropertyInfo PropertyInfo { get; internal set; }
    }
}
