﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using TechnicalTools;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;


namespace DataMapper
{
    /// <summary>
    /// Attribute à mettre au dessus d'une classe qui indique le nom de la table de la base de donnée:
    /// <example>
    /// [DbMappedTable("Assets")] public class clsAsset { ... }
    /// </example>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    [DebuggerDisplay("{"+ nameof(DebugString) + ",nq}")]
    public partial class DbMappedTable : Attribute, IHasMapperOwner
    {
        public IDbMapper Mapper              { get; internal set; }
        public string    SchemaName          { get; internal set; }
        public string    SchemaNameProtected { get { return "[".AsPrefixForIfNotBlank(SchemaName).IfNotBlankAddSuffix("]"); } }
        public string    TableName           { get; internal set; }
        public string    TableNameProtected  { get { return "[".AsPrefixForIfNotBlank(TableName).IfNotBlankAddSuffix("]"); } }
        public string    FullName            { get { return SchemaName.IfNotBlankAddSuffix(".") + TableName; } }
        public string    FullNameProtected   { get { return SchemaNameProtected.IfNotBlankAddSuffix(".") + TableNameProtected; } }
        public Type      MappedType          { get; set; }


        public DbMappedField PkField
        {
            get { Debug.Assert(PkFields.Count == 1); return PkFields[0]; }
        }
        public DbMappedField AutoIncrementedField
        {
            get { return AllFields.SingleOrDefault(f => f.IsAutoIncremented); }
        }


        public readonly List<DbMappedField> PkFields = new List<DbMappedField>(); // Toujours ordonné par KeyOrderIndex
        public readonly List<DbForeignKey>  ForeignKeys = new List<DbForeignKey>();
        public IEnumerable<DbMappedField>   FkFields { get { return ForeignKeys?.SelectMany(fk => fk.Keys) ?? Enumerable.Empty<DbMappedField>(); } }

        public readonly List<DbMappedField> NonPKFields = new List<DbMappedField>();

        public readonly List<DbMappedField> AllFields = new List<DbMappedField>();
        public readonly Dictionary<PropertyInfo, DbMappedField> AllFieldsByProperty = new Dictionary<PropertyInfo, DbMappedField>();
        public readonly Dictionary<string, DbMappedField> AllFieldsByPropertyName = new Dictionary<string, DbMappedField>();
        public readonly Dictionary<string, DbMappedField> AllFieldsByColumnName = new Dictionary<string, DbMappedField>();

        public readonly List<DbAggregation>        Aggregations = new List<DbAggregation>();
        public readonly List<DbAggregationReverse> ReverseAggregations = new List<DbAggregationReverse>();
        public readonly List<DbComposition>        Compositions = new List<DbComposition>();
        public          DbCompositionReverse       CompositionOwner { get; internal set; }

        internal string DebugString
        {
            get
            {
                return nameof(FullNameProtected) + "=" + FullNameProtected + ", " +
                       nameof(MappedType) + "=" + (MappedType?.Name ?? "NULL")+ ", " +
                       nameof(PkFields) + "=" + ((PkFields?.Count ?? 0) == 0 ? "No PK" : PkFields.Select(f => f.ColumnName).Join());
            }
        }

        public DbMappedTable(string tableName, string schemaName = "dbo")
        {
            SchemaName = schemaName;
            TableName = tableName;
        }

        public readonly List<DbMappedField> FieldsWithDefaultValues = new List<DbMappedField>();

        public DbMappedField this[string field_name]
        {
            get { return AllFieldsByColumnName[field_name]; }
        }
        public DbMappedField GetMappedColumnFromProperty<TObject, TProperty>(Expression<Func<TObject, TProperty>> get)
            where TObject : IAutoMappedDbObject
        {
            AllFieldsByPropertyName.TryGetValue(GetMemberName.For(get), out DbMappedField field);
            return field;
        }
        public string GetSqlKeyFilter(IIdTuple id)
        {
            return string.Join(" AND ", PkFields.Zip(id.Keys, (f, key) => f.ColumnNameProtected + " = " + DbMappedField.ToSql(key)));
        }
        public string GetSqlKeyFilter<TObject>(TObject obj)
            where TObject : IAutoMappedDbObject
        {
            return string.Join(" AND ", PkFields.Select(f => f.ColumnNameProtected + " = " + f.GetDbStringValue(obj)));
        }

        public string GetSqlFieldNames(IEnumerable<DbMappedField> fields)
        {
            return string.Join(", ", fields.Select(field => field.ColumnNameProtected));
        }

        public string GetSqlFieldInits<TObject>(IEnumerable<DbMappedField> fields, TObject obj)
            where TObject : IAutoMappedDbObject
        {
            return string.Join(", ", fields.Select(f => f.ColumnNameProtected + " = " + f.GetDbStringValue(obj)));
        }

        public string GetSqlFieldValues<TObject>(IEnumerable<DbMappedField> fields, TObject obj)
            where TObject : IAutoMappedDbObject
        {
            return string.Join(", ", fields.Select(f => f.GetDbStringValue(obj)));
        }
    }

}
