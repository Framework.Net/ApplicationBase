﻿using System;


namespace DataMapper
{
    [AttributeUsage(AttributeTargets.Class)]
    public partial class DbMappedView : DbMappedTable
    {
        public DbMappedView(string tableName, string schemaName = "dbo")
            : base(tableName, schemaName)
        {
        }
    }

}
