﻿using System;
using System.Diagnostics;
using System.Linq;


namespace DataMapper
{
    /// <summary>
    /// Une aggregation est un lien ou chaque instance de classe vit sa vie séparement
    /// Dans l'exemple ci dessous, le livre peut etre supprimé et l'autheur rester en base de donnés.
    /// DbAggregation est la pour dire que la classe possedant la propriete sur laquelle vous avez utilisé DbAggregation
    /// fait référence a une autre class mais sans la controler.
    /// A noter que la propriété sur laquelle est utilisé DbAggregation _peut_ ne pas avoir de setter.
    /// Actuellement le DbMapper n'ecrit pas dans ces propriétés. Ces propriété ne sont utilisé que permettre d'ecrire des requetes en Linq.
    /// Exemple :
    /// <example>
    /// <code>
    /// public class Book : IAutoMappedDbObject
    /// {
    ///     [DbMappedField([...])]
    ///     public  int AuthorId { get; set;}
    ///
    ///     // Here we are saying that it is the table Book that contains the relation
    ///     // Sometimes Database administrator _could_ prefer to store the relation in Author
    ///     // if, for example, your application currently handle only one book by author and you have performance issue
    ///     [DbAggregation(FkPropertyName = nameof(AuthorId)]
    ///     public  Author Author { get; set;}
    ///
    ///     // If the relation is many-to-many
    ///     // Here is how to write the relation, the DbAggregation means it is ana aggregation, still from the perspective of Book
    ///     // But the refencing table is je join table, and the property type is now a List
    ///     [DbAggregation(ReferencingClass = typeof(BookAuthors), FkPropertyName = nameof(AuthorId)]
    ///     public  List&lt;Author&gt; Authors { get; set;}
    ///
    ///     [...]
    /// }
    /// // this class contains the data fro aggregation relation but it does not own the aggregation so they are "reverse"
    /// public class BookAuthor : IAutoMappedDbObject
    /// {
    ///     [DbMappedField([...])]
    ///     public  int AuthorId { get; set;}
    ///     [DbMappedField([...])]
    ///     public  int BookId { get; set;}
    ///
    ///     [DbAggregationReverse(OwningClass = typeof(Auhor), AggregationPropertyName = nameof(Auhor.AuthorId)]
    ///     public  Author Author { get; set;}
    ///     [DbAggregationReverse(OwningClass = typeof(Book), AggregationPropertyName = nameof(Book.BookId)]
    ///     public  Book Book { get; set;}
    ///
    ///     [...]
    /// }
    /// </code>
    /// </example>
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    [DebuggerDisplay("{" + nameof(Name) + ",nq}")]
    //[DebuggerDisplay(nameof(Table) + "={" + nameof(Table) + "?." + nameof(DbMappedTable.FullName) + ",nq}, " +
    //                 nameof(FkPropertyName) + "={" + nameof(FkPropertyName) + "." + nameof(System.Reflection.PropertyInfo.Name) + ",nq}")]
    public class DbAggregation : DbMappedPropertyAttribute
    {
        // Class that _technically_ holds the relation but this class not necessarly owns the relation
        public Type          ReferencingClass { get; set; }
        // Property name of this class on which a DbMappedField (with FkTo) has been declared
        public string        FkPropertyName   { get; set; }
        // Will be filled by the engine
        public DbMappedTable ReferencingTable { get; internal set; }
        // KeyField belongs to the table related to ReferencingClass
        public DbMappedField KeyField { get; internal set; }

        public string Name
        {
            get
            {
                return "Aggregation settled on " + Table.MappedType.Name + TechnicalRelation;
            }
        }
        internal string TechnicalRelation
        {
            get
            {
                bool ownerIsReferencing = ReferencingClass == Table.MappedType;
                return " (FK: " + (ownerIsReferencing ? "" : ReferencingClass.Name + ".") + KeyField.PropertyInfo.Name
                     + " => " + (ownerIsReferencing ? KeyField.ForeignKeys.Single().To.Name + "." + Table.Mapper.GetTableMapping(KeyField.ForeignKeys.Single().To, false)?.PkField.PropertyInfo.Name
                                                    : Table.PkField.PropertyInfo.Name) + ")";

            }
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    [DebuggerDisplay("{" + nameof(Name) + ",nq}")]
    //[DebuggerDisplay(nameof(Table) + "={" + nameof(Table) + "?." + nameof(DbMappedTable.FullName) + "}, " +
    //                 nameof(OwningClass) + "={" + nameof(OwningClass) + "." + nameof(Type.Name) + "}" +
    //                 nameof(AggregationRelation) + "={" + nameof(AggregationRelation) + "}")]
    public class DbAggregationReverse : DbMappedPropertyAttribute
    {
        // Base class where an aggregation relation has been declared. It is not necessarly the class that technically holds the relation
        public Type          OwningClass             { get; set; }
        // Property name of this class on which a DbAggregation has been declared
        public string        AggregationPropertyName { get; set; }
        // Will be filled by the engine
        public DbAggregation AggregationRelation     { get; internal set; }

        public string Name { get { return "Settled on " + Table.MappedType.Name + ", Reverse of " + AggregationRelation.Name; } }
    }
}
