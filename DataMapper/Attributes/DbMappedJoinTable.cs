﻿using System;


namespace DataMapper
{
    [AttributeUsage(AttributeTargets.Class)]
    public partial class DbMappedJoinTable : DbMappedTable
    {
        public DbMappedJoinTable(string tableName, string schemaName = "dbo")
            : base(tableName, schemaName)
        {
        }
    }

}
