﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using TechnicalTools;


namespace DataMapper
{
    /// <summary>
    /// Allow to indicate a column/property is the foreign key to multiple another table.
    /// This is useful for example when one column indicate the table name while the other indicate the foreign key
    /// For example, given this class:
    /// <code>
    /// class ReferencedClass1
    /// {
    ///     [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)] public int Id { get; set; }
    ///     // ...
    /// }
    /// class ReferencedClass2
    /// {
    ///     [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)] public int Id { get; set; }
    ///     // ...
    /// }
    /// </code>
    /// <para>
    /// Here is a way to use this attribute
    /// </para>
    /// <code>
    ///
    /// class Foo : IAutoMappedDbObject
    /// {
    ///     [<see cref="DbMappedField"/>("Id", IsAutoPK = true)]
    ///     public int Id { get; set; }
    ///     [<see cref="DbMappedField"/>("ReferencedObjectType")]
    ///     public string ReferencedObjectType { get; set; }
    ///     [<see cref="DbMappedField"/>("ReferencedObjectId")]
    ///     public int    ReferencedObjectId   { get; set; }
    /// }
    /// </code>
    /// <para>
    /// So
    /// </para>
    /// [<see cref="DbMappedField.FkTo"/>(<see cref="DbMappedField.FkTo"/> = typeof(T))]
    /// <para>
    /// is equals to :
    /// </para>
    /// [<see cref="DbCompositeLink"/>(<see cref="FkTo"/> = typeof(T))]
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    [DebuggerDisplay("{" + nameof(DebugString) + ",nq}")]
    [DbForeignKey(typeof(void), nameof(To))]
    public partial class DbForeignKey : DbMappedAttribute
    {
        /// <summary>
        /// Tell the class mapped with table the foreign key reference
        /// </summary>
        public Type                To    { get; set; }

        /// <summary>
        /// Mandatory if there are two or more FK to the same table
        /// Otherwise it is optional (but recommended to be set anyway)
        /// Example : Table "Body" has two properties "LeftlegId" and "RightLegId" poiting to Table "Leg",
        /// Roles can be set to "Left Leg" and "Right Leg".
        /// This is mandatory to distinguish the case we have two fks to same table
        /// from one FK (defined with two columns) to one table.
        /// </summary>
        public string              RelationName  { get; set; }

        // Filled with all keys composing the foreign key
        public List<DbMappedField> Keys  { get; } = new List<DbMappedField>();
        // Temporary storage
        internal List<string> KeyPropertiesNames { get; set; }


        /// <summary>
        /// Useful if some Foreign key depend of another column to know the real table to link
        /// <example>
        /// <code>
        /// [DbForeignKey(To = typeof(Foo), ConditionalPropertyName = nameof(LinkedTableName), ConditionalPropertyValueInSql = "'Foo'")]
        /// [DbForeignKey(To = typeof(Bar), ConditionalPropertyName = nameof(LinkedTableName), ConditionalPropertyValueInSql = "'Bar'"))]
        /// [DbMappedField("FooOrBarId")]
        /// public int FooOrBarId { get; set; }
        /// [DbMappedField("LinkedTableName")]
        /// public string LinkedTableName { get; set; }
        /// </code>
        /// </example>
        /// </summary>
        public string       ConditionalPropertyName       { get; set; }
        /// <summary> See <see cref="ConditionalPropertyName"/> for explanation </summary>
        public object       ConditionalPropertyValueInSql { get; set; }
        public DbForeignKey Previous                      { get; internal set; }
        public DbForeignKey Next                          { get; internal set; }

        /// <summary>
        /// For case where Foreign key point to another database (one among a multiple db of same type)
        /// </summary>
        public string       DbMapperNamePropertyName      { get; set; }


        public DbForeignKey(Type to, string key1, params string[] otherKeys)
        {
            To = to;
            if (string.IsNullOrWhiteSpace(key1))
                throw new ArgumentException(null, nameof(key1));
            KeyPropertiesNames = new List<string>();
            KeyPropertiesNames.Add(key1);
            if (otherKeys != null)
            {
                if (otherKeys.Any(k => string.IsNullOrWhiteSpace(k)))
                    throw new ArgumentException(null, nameof(otherKeys));
                KeyPropertiesNames.AddRange(otherKeys);
            }
        }

        internal string DebugString
        {
            get
            {
                return "{".AsPrefixForIfNotBlank(RelationName).IfNotBlankAddSuffix("} ") +
                       "(" + (KeyPropertiesNames ?? Keys.Select(kp => kp.PropertyInfo.Name)).Join(" | ") + ") => " +
                       To.Name +
                       " [only if ".AsPrefixForIfNotBlank(ConditionalPropertyName).IfNotBlankAddSuffix(" = " + ConditionalPropertyValueInSql + "]");
            }
        }
    }
}
