﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;

using DataMapper.Diagnostics;


namespace DataMapper
{
    internal partial class DbMapperConcrete : IDbMapper
    {
        internal void Trace(IConnectionProvider cn, string request, Action action)
        {
            Trace(cn, request, () => { action(); return true; });
        }
        internal T Trace<T>(IConnectionProvider cn, string sqlRequest, Func<T> func)
        {
            if (!IDbMapperSharedSettings.Instance.RequestEventHasSubscribers)
                return func();

            var trace = new RequestTrace(this, cn.ConnectionString, sqlRequest);
            try
            {
                trace.StartTimeUTC = DateTime.UtcNow; // using UtcNow is a lot faster than DateTime.Now
                var res = func();
                trace.EndTimeUTC = DateTime.UtcNow;
                return res;
            }
            catch (Exception ex)
            {
                trace.EndTimeUTC = DateTime.UtcNow;
                trace.Exception = ex;
                throw;
            }
            finally
            {
                IDbMapperSharedSettings.Instance.RaiseRequest(trace);
            }
        }
        internal IEnumerable<T> TraceEnumerable<T>(IConnectionProvider cn, string sqlRequest, IEnumerable<T> enumerable)
        {
            return _TraceEnumerable(cn, sqlRequest, enumerable, IDbMapperSharedSettings.Instance.RequestEventHasSubscribers);
        }
        internal IEnumerable<T> _TraceEnumerable<T>(IConnectionProvider cn, string sqlRequest, IEnumerable<T> enumerable, bool withEvents)
        {
            if (!withEvents)
            {
                foreach (var t in enumerable)
                    yield return t;
            }
            else
            {
                var trace = new RequestTrace(this, cn.ConnectionString, sqlRequest)
                {
                    StartTimeUTC = DateTime.UtcNow // using UtcNow is a lot faster than DateTime.Now
                };
                try
                {
                    // Note : try...catch cannot enclose "yield return" statement,
                    //        this is why there is a two try...catch statements here
                    //        Without this limitation code could be factorized
                    IEnumerator<T> enumerator;
                    try
                    {
                        enumerator = enumerable.GetEnumerator();
                    }
                    catch (Exception ex)
                    {
                        trace.EndTimeUTC = DateTime.UtcNow;
                        trace.Exception = ex;
                        throw;
                    }
                    do
                    {
                        T t;
                        try
                        {
                            if (!enumerator.MoveNext())
                            {
                                enumerator.Dispose();
                                break;
                            }
                            t = enumerator.Current;
                        }
                        catch (Exception ex)
                        {
                            trace.EndTimeUTC = DateTime.UtcNow;
                            trace.Exception = ex;
                            throw;
                        }
                        yield return t;
                    } while (true);
                    trace.EndTimeUTC = DateTime.UtcNow;
                }
                finally
                {
                    IDbMapperSharedSettings.Instance.RaiseRequest(trace);
                }
            }
        }
        internal IEnumerable<DbDataReader> TraceEnumerable(IConnectionProvider cn, string sqlRequest, IEnumerable<DbDataReader> enumerable)
        {
            var withEvent = IDbMapperSharedSettings.Instance.RequestEventHasSubscribers;

            if (!withEvent || !IDbMapperSharedSettings.Instance.DisableStreaming)
            {
                foreach (var rdr in _TraceEnumerable(cn, sqlRequest, enumerable, withEvent))
                    yield return rdr;
                yield break;
            }

            var trace = new RequestTrace(this, cn.ConnectionString, sqlRequest);
            DataTable dt = null;
            var enumerator = enumerable.GetEnumerator();
            trace.StartTimeUTC = DateTime.UtcNow; // using UtcNow is a lot faster than DateTime.Now
            var isNotEmpty = enumerator.MoveNext();
            var dataReader = isNotEmpty ? enumerator.Current : null;
            if (!(dataReader is SqlDataReader))
            { // we dont know how to measure.. so we get back to default _trace enumerable and cancel current measure
                foreach (var rdr in _TraceEnumerable(cn, sqlRequest, enumerable, withEvent))
                    yield return rdr;
                yield break;
            }

            try
            {
                dt = ConvertDataReaderToDataTable((SqlDataReader)dataReader, false);
                Debug.Assert(!enumerator.MoveNext()); // all stream should be read
            }
            catch (Exception ex)
            {
                trace.EndTimeUTC = DateTime.UtcNow;
                trace.Exception = ex;
                throw;
            }
            finally
            {
                if (trace.Exception == null)
                    trace.EndTimeUTC = DateTime.UtcNow;
                IDbMapperSharedSettings.Instance.RaiseRequest(trace);
            }
            var drd = dt.CreateDataReader();
            while (drd.Read())
                yield return drd;
        }
        DataTable ConvertDataReaderToDataTable(SqlDataReader dataReader, bool readFirst)
        {
            var datatable = new DataTable();
            DataTable schemaTable = dataReader.GetSchemaTable();

            foreach (DataRow myRow in schemaTable.Rows)
            {
                var myDataColumn = new DataColumn
                {
                    DataType = (Type)myRow[12],
                    ColumnName = myRow[0].ToString()
                };
                datatable.Columns.Add(myDataColumn);
            }
            if (!readFirst || dataReader.Read())
                do
                {
                    DataRow myDataRow = datatable.NewRow();
                    for (int i = 0; i < schemaTable.Rows.Count; i++)
                        myDataRow[i] = dataReader[i];
                    datatable.Rows.Add(myDataRow);
                    myDataRow = null;
                } while (dataReader.Read());
            schemaTable = null;
            return datatable;
        }
    }
}
