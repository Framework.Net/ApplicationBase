﻿using DataMapper.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;


namespace DataMapper
{
    // Fonctionalités offertes par les classes respectant l'interface IAutoMappedDbObject
    internal partial class DbMapperConcrete
    {
        public DbMappedTable GetTableMapping(IAutoMappedDbObject obj)
        {
            return GetTableMapping(obj.GetType());
        }

        public TObject TryGetObject<TObject>(long id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
        {
            return (TObject)_cache.TryGet(new IdTuple<long>(id).ToTypedId(typeof(TObject)));
        }
        public TObject TryGetObject<TObject>(int id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
        {
            return (TObject)_cache.TryGet(new IdTuple<int>(id).ToTypedId(typeof(TObject)));
        }
        public TObject TryGetObject<TObject>(IIdTuple id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
        {
            return (TObject)_cache.TryGet(id.ToTypedId(typeof(TObject)));
        }
        public TObject TryGetObject<TObject>(ITypedId id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
        {
            return (TObject)_cache.TryGet(id);
        }
        public List<TObject> GetObjectsWithIds<TObject>(IEnumerable<long> distinctIds)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
        {
            DbMappedTable mtable = GetTableMapping(typeof(TObject));
            if (mtable.PkFields.Count != 1 || mtable.PkField.PropertyInfo.PropertyType != typeof(long))
                throw new TechnicalException(string.Format("Type {0} does not have a non composite PrimaryKey of type long!", mtable.MappedType.Name), null);
            var idsToLoad = new List<RemoteTuple<long>>();
            var result = new List<TObject>();
            foreach (var value in distinctIds)
            {
                var obj = (TObject)_cache.TryGet(new IdTuple<long>(value).ToTypedId(typeof(TObject)));
                if (obj == null)
                    idsToLoad.Add(new RemoteTuple<long>(value));
                else
                    result.Add(obj);
            }
            if (idsToLoad.Count > 0)
            {
                var context = CreateRemoteContext().With(idsToLoad, out string tableIds);
                var loadedObjects = EnumerateCollectionFromInnerJoinSql<TObject>(
                                        context: context,
                                        innerJoin: tableIds + " as I",
                                        joinCondition: "A." + mtable.PkField.ColumnNameProtected + " = I.Item1",
                                        whereCondition: null)
                                    .ToList();
                result.AddRange(loadedObjects);
            }
            return result;
        }

        public TObject GetObjectWithId<TObject>(long id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
        {
            DbMappedTable mtable = GetTableMapping(typeof(TObject));
            if (mtable.PkFields.Count != 1 || mtable.PkField.PropertyInfo.PropertyType != typeof(long))
                throw new TechnicalException(string.Format("Type {0} does not have a non composite PrimaryKey of type long!", mtable.MappedType.Name), null);

            var obj = (TObject)_cache.TryGet(new IdTuple<long>(id).ToTypedId(typeof(TObject)));
            if (obj != null)
                return obj;
            return LoadCollection<TObject>(mtable.PkField.ColumnNameProtected + " = " + id)
                    .SingleOrDefault();
        }
        public TObject GetObjectWithId<TObject>(int id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
        {
            DbMappedTable mtable = GetTableMapping(typeof(TObject));
            if (mtable.PkFields.Count != 1 || mtable.PkField.PropertyInfo.PropertyType != typeof(int))
                throw new TechnicalException(string.Format("Type {0} does not have a non composite PrimaryKey of type int!", mtable.MappedType.Name), null);

            var obj = (TObject)_cache.TryGet(new IdTuple<int>(id).ToTypedId(typeof(TObject)));
            if (obj != null)
                return obj;
            return LoadCollection<TObject>(mtable.PkField.ColumnNameProtected + " = " + id)
                    .SingleOrDefault();
        }

        public TObject GetObjectWithId<TObject>(byte id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
        {
            DbMappedTable mtable = GetTableMapping(typeof(TObject));
            if (mtable.PkFields.Count != 1 || mtable.PkField.PropertyInfo.PropertyType != typeof(byte))
                throw new TechnicalException(string.Format("Type {0} does not have a non composite PrimaryKey of type int!", mtable.MappedType.Name), null);

            var obj = (TObject)_cache.TryGet(new IdTuple<byte>(id).ToTypedId(typeof(TObject)));
            if (obj != null)
                return obj;
            return LoadCollection<TObject>(mtable.PkField.ColumnNameProtected + " = " + id)
                    .SingleOrDefault();
        }

        public TObject GetObjectWithId<TObject>(DateTime dt)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
        {
            DbMappedTable mtable = GetTableMapping(typeof(TObject));
            if (mtable.PkFields.Count != 1 || mtable.PkField.PropertyInfo.PropertyType != typeof(DateTime))
                throw new TechnicalException(string.Format("Type {0} does not have a non composite PrimaryKey of type DateTime!", mtable.MappedType.Name), null);

            var obj = (TObject)_cache.TryGet(new IdTuple<DateTime>(dt).ToTypedId(typeof(TObject)));
            if (obj != null)
                return obj;
            return LoadCollection<TObject>(mtable.PkField.ColumnNameProtected + " = " + DbMappedField.ToSql(dt))
                .SingleOrDefault();
        }


        void LoadProperties(IAutoMappedDbObject item, DataRow row, DbMappedTable mtable)
        {
            foreach (DbMappedField mfield in mtable.AllFields)
                mfield.SetDbValue(item, row[mfield.ColumnName]);
        }

        public void CreateInDatabase<TObject>(TObject item, bool with_transaction = true)
            where TObject : IAutoMappedDbObject
        {
            DbMappedTable mtable = GetTableMapping(item.GetType());
            string sql_request = GetSqlFor_CreateInDatabase(mtable, new[] { item });

            using (var cn = GetConnectionProvider())
            {
                // ReSharper disable AccessToDisposedClosure
                Debug.Assert(cn != null, nameof(cn) + " != null");

                using (IDbTransaction tran = with_transaction ? cn.StartTransaction() : null)
                {
                    if (mtable is DbMappedJoinTable)
                        Trace(cn, sql_request, () => cn.ExecuteNonQuery(sql_request));
                    else
                    {
                        var newId = Trace(cn, sql_request, () => cn.ExecuteScalar(sql_request, true));

                        // Met à jour l'id des objets créés
                        // si l'objet est nouveau et a ete enregistre dans une table qui a priori contient un, et un seul, id
                        if (mtable.AutoIncrementedField != null)
                        {
                            Debug.Assert(!object.Equals(mtable.AutoIncrementedField.GetValue(item), newId));
                            mtable.AutoIncrementedField.SetValue(item, newId);
                        }
                    }
                    tran?.Commit();
                }
                // ReSharper restore AccessToDisposedClosure
            }
            var asBaseDTO = item as BaseDTO;
            if (asBaseDTO != null && asBaseDTO.OwnerMapper == null)
                asBaseDTO.OwnerMapper = this;

            if (typeof(IHasTypedIdReadable).IsAssignableFrom(typeof(TObject)))
                _cache.Add((IHasTypedIdReadable)item);

            if (item is DynamicEnum)
            {
                var t = item.GetType();
                while (t != null)
                {
                    t = t.BaseType;
                    if (t.IsGenericType)
                    {
                        var gt = t.GetGenericTypeDefinition();
                        if (gt == typeof(DynamicEnum<,>))
                        {
                            var m = t.GetMethod(nameof(DynamicEnum.DummyEnum.AddNewItem), BindingFlags.Static | BindingFlags.NonPublic);
                            m.Invoke(null, new object[] { item, this });
                            break;
                        }
                    }
                }
            }
        }
        public string GetSqlFor_CreateInDatabase<TObject>(DbMappedTable mtable, IReadOnlyCollection<TObject> items)
            where TObject : IAutoMappedDbObject
        {
            Debug.Assert(items != null);
            Debug.Assert(items.Count > 0);

            // Si une propriete est null est que la base spécifie une valeur par default pour la colonne associé,
            // alors on écarte ce champs de la sauvegarde (même si la colonne est nullable à la base!) lors de la creation (insertion en base)
            // Cela reproduit le comportement des ADODB.Recordsets (la methode Resync() faisait apparaitre les valeurs par defaut)
            foreach (var f in mtable.FieldsWithDefaultValues)
                foreach (var item in items)
                    if (f.IsNullable || f.GetValue(item) == null)
                        f.SetValue(item, f.DefaultValue);

            var fieldsToSave = mtable.AllFields.Where(c => !c.IsAutoIncremented && !c.IsComputed).ToArray();
            Debug.Assert(fieldsToSave.Any());

            string sql = "INSERT INTO " + mtable.FullName + " (" + mtable.GetSqlFieldNames(fieldsToSave) + ")" + Environment.NewLine;
            // On utilise OUTPUT au lieu de SCOPE_IDENTITY http://www.ubitsoft.com/products/sqlenlight/help_19/html/fad6ca48-cfe1-4a40-80db-6583153c7d23.htm
            sql += mtable.AutoIncrementedField == null
                    ? " OUTPUT -1.0" + Environment.NewLine // On choisit -1 car 0 peut etre utilisé comme id (de meme pour les tables representant des enums, donc on choisit -1)
                    : " OUTPUT INSERTED." + mtable.AutoIncrementedField.ColumnName + Environment.NewLine;
            if (!items.Skip(1).Any())
            {
                sql += " VALUES " + Environment.NewLine;
                foreach (var item in items)
                    sql += "( " + string.Join(", ", fieldsToSave.Select(f => f.UseDefaultOnInsert ? "default" : f.GetDbStringValue(item))) + " )," + Environment.NewLine;
                sql = sql.Remove(sql.Length - 1 - Environment.NewLine.Length);
            }
            else
            {
                // Sql Server state that INSERT queries that "use SELECT with ORDER BY to populate rows guarantees how identity values are computed but not the order in which the rows are inserted"
                // from  http://dba.stackexchange.com/questions/155735/is-it-safe-to-rely-on-the-order-of-an-inserts-output-clause/155737
                // We should sort the list of generated id though
                sql += " SELECT " + string.Join(", ", fieldsToSave.Select(f => "t." + f.ColumnNameProtected)) + Environment.NewLine;
                sql += "   FROM " + Environment.NewLine;
                sql += "(VALUES " + Environment.NewLine;
                long index = 0;
                foreach (var item in items)
                    sql += "   (" + (++index) + ", " + fieldsToSave.Select(f => f.UseDefaultOnInsert ? "default" : f.GetDbStringValue(item)).Join() + " )," + Environment.NewLine;
                sql = sql.Remove(sql.Length - 1 - Environment.NewLine.Length);
                sql += ") t (__SourceId, " + mtable.GetSqlFieldNames(fieldsToSave) + ")" + Environment.NewLine;
                sql += "ORDER BY t.__SourceId" + Environment.NewLine; // using select + order by is the only way (according to documentation ) to guarantee order of generated id


                // Note we should use this request in order to really guaranteed all order
                // from : http://stackoverflow.com/questions/35687950/combine-output-inserted-id-with-value-from-selected-row
                //MERGE INTO [dbo].[Test]
                //USING
                //(
                //    SELECT [Data]
                //    FROM @Old AS O
                //) AS Src
                //ON 1 = 0
                //WHEN NOT MATCHED BY TARGET THEN
                //INSERT ([Data])
                //VALUES (Src.[Data])
                //OUTPUT Src.ID AS OldID, inserted.ID AS NewID
                //INTO @New(ID, [OtherID])
                //;
            }

            return sql;
        }

        public int UpdateToDatabase<TObject>(TObject item, bool with_transaction = true, string sqlCondition = null, IReadOnlyCollection<DbMappedField> fieldsToUpdate = null)
            where TObject : IAutoMappedDbObject
        {
            //var o = item as IPropertyNotifierObject;
            //if (o != null)
            //    o.CheckIsNotReadOnlyForAction("UpdateToDatabase");
            DbMappedTable mtable = GetTableMapping(item.GetType());
            string sql_request = GetSqlFor_UpdateToDatabase(item, mtable, sqlCondition, fieldsToUpdate);

            int affectedRecordCount = 0;
            using (var cn = GetConnectionProvider())
            {
                // ReSharper disable AccessToDisposedClosure
                Debug.Assert(cn != null, nameof(cn) + " != null");
                using (IDbTransaction tran = with_transaction ? cn.StartTransaction() : null)
                {
                    //Console.WriteLine("Update " + typeof(TObject).Name + ": " + item.GetSqlIds());
                    affectedRecordCount = Trace(cn, sql_request, () => cn.ExecuteNonQuery(sql_request));
                    tran?.Commit();
                }
                // ReSharper restore AccessToDisposedClosure
            }
            var asBaseDTO = item as BaseDTO;
            if (asBaseDTO != null && asBaseDTO.OwnerMapper == null)
                asBaseDTO.OwnerMapper = this;
            return affectedRecordCount;
        }
        public static string GetSqlFor_UpdateToDatabase<TObject>(TObject item, DbMappedTable mtable, string sqlCondition = null, IReadOnlyCollection<DbMappedField> specificFieldsToUpdate = null)
            where TObject : IAutoMappedDbObject
        {
            if (mtable.PkFields.Count == 0)
                throw new Exception(string.Format("Do not know how to identify object of table {0} to update in database", mtable.FullName));
            if (specificFieldsToUpdate != null)
            {
                if (mtable.AutoIncrementedField != null && specificFieldsToUpdate.Contains(mtable.AutoIncrementedField))
                    throw new Exception(string.Format("Auto incremented field of table {0} cannot be updated in database", mtable.FullName));
                if (specificFieldsToUpdate.Any(f => f.Table != mtable))
                    throw new Exception(string.Format("A DbMappedField does not belong to mapping table {0} of current object to update!", mtable.FullName));
            }

            var fields = specificFieldsToUpdate != null && specificFieldsToUpdate.Any()
                       ? specificFieldsToUpdate
                       : mtable.AllFields.Where(c => !c.IsAutoIncremented && !c.IsComputed);
            // Pas de crochet autours du nom de table car les tables peuvent contenir  leur schema
            return "UPDATE " + mtable.FullName + " " +
                   " SET "   + mtable.GetSqlFieldInits(fields, item) +
                   " FROM "  + mtable.FullName +
                   " WHERE " + mtable.GetSqlKeyFilter(item) +
                   " AND (".AsPrefixForIfNotBlank(sqlCondition.IfNotBlankAddSuffix(")")) + ";";
        }
        public int UpdateToDatabase<TObject>(TObject item, string sqlCondition, params Expression<Func<TObject, object>>[] propertiesToUpdate)
            where TObject : IAutoMappedDbObject
        {
            var mtable = GetTableMapping(item);
            return UpdateToDatabase(item, sqlCondition, propertiesToUpdate.Select(f => mtable.MappedType.GetProperty(GetMemberName.GetMemberNameFor(item, f))).ToArray());
        }
        public int UpdateToDatabase<TObject>(TObject item, params Expression<Func<TObject, object>>[] propertiesToUpdate)
            where TObject : IAutoMappedDbObject
        {
            var mtable = GetTableMapping(item);
            return UpdateToDatabase(item, null, propertiesToUpdate.Select(f => mtable.MappedType.GetProperty(GetMemberName.GetMemberNameFor(item, f))).ToArray());
        }
        public int UpdateToDatabase<TObject>(TObject item, string sqlCondition, IReadOnlyCollection<PropertyInfo> propertiesToUpdate)
            where TObject : IAutoMappedDbObject
        {
            var mtable = GetTableMapping(item);
            var dbMappedFields = propertiesToUpdate.Select(p => mtable.AllFieldsByProperty[p]).ToList();
            if (dbMappedFields.Any(f => f == null))
                throw new TechnicalException("Mapped fields not found!", null);
            return UpdateToDatabase(item, sqlCondition, dbMappedFields);
        }
        public int UpdateToDatabase<TObject>(TObject item, string sqlCondition, IReadOnlyCollection<DbMappedField> fieldsToUpdate)
            where TObject : IAutoMappedDbObject
        {
            Debug.Assert(fieldsToUpdate.All(f => f.Table != null), "Use GetTableMapping(...).AllFieldsByProperty(...) to get real attribute instance in cache !");
            return UpdateToDatabase(item, true, sqlCondition, fieldsToUpdate);
        }


        public void DeleteInDatabase<TObject>(TObject item, bool with_transaction = true)
            where TObject : IAutoMappedDbObject
        {
            //var o = item as IPropertyNotifierObject;
            //if (o != null)
            //    o.CheckIsNotReadOnlyForAction("DeleteInDatabase");
            DbMappedTable mtable = GetTableMapping(item.GetType());
            string sql_request = GetSqlFor_DeleteInDatabase(item, mtable);

            using (var cn = GetConnectionProvider())
            {
                // ReSharper disable AccessToDisposedClosure
                Debug.Assert(cn != null, nameof(cn) + " != null");
                using (IDbTransaction tran = with_transaction ? cn.StartTransaction() : null)
                {
                    //Console.WriteLine("Delete " + typeof(TObject).Name + ": " + item.GetSqlIds());
                    Trace(cn, sql_request, () => cn.ExecuteNonQuery(sql_request));
                    tran?.Commit();
                }
                // ReSharper restore AccessToDisposedClosure
            }
            var asBaseDTO = item as BaseDTO;
            if (asBaseDTO != null && asBaseDTO.OwnerMapper == null)
                asBaseDTO.OwnerMapper = this;

            if (typeof(IHasTypedIdReadable).IsAssignableFrom(typeof(TObject)))
                _cache.Remove((IHasTypedIdReadable)item);
        }
        public static string GetSqlFor_DeleteInDatabase<TObject>(TObject item, DbMappedTable mtable)
            where TObject : IAutoMappedDbObject
        {
            if (mtable.PkFields.Count == 0)
                throw new Exception(string.Format("Do not know how to identify object of table {0} to delete in database", mtable.FullName));
            // On englobe pas FullName entre crochet car quand on écrit dbo.NomDeTable on ne peut pas mettre de crochet. Les crochets doivent être indiqué quand on utilise DbMappedTable
            return "DELETE FROM " + mtable.FullName + " WHERE " + mtable.GetSqlKeyFilter(item) + ";";
        }


        // Helper pour copier tous les champs mappés d'un objet vers un autre de même type
        public void CopyAllFieldsFrom<TObject>(TObject target, TObject source, IReadOnlyCollection<DbMappedField> sqlFields = null)
            where TObject : IAutoMappedDbObject
        {
            var mtable = GetTableMapping(target);
            sqlFields = sqlFields ?? mtable.NonPKFields;
            foreach (var field in sqlFields)
                field.SetValue(target, field.GetValue(source));
        }


        public List<PropertyInfo> GetPropertiesComposingKey(Type type)
        {
            var mtable = GetTableMapping(type);
            if (mtable.PkFields.Count == 0)
                return new List<PropertyInfo>();
            return mtable.PkFields.Select(f => f.PropertyInfo).ToList();
        }
    }
}
