﻿using System;
using System.Text;

using TechnicalTools;
using TechnicalTools.Model.Cache;


namespace DataMapper
{
    internal partial class DbMapperConcrete : IDbMapper
    {
        public string DumpObject<T>(T obj, string indent = null, StringBuilder sb = null)
            where T : class, IAutoMappedDbObject
        {
            sb = sb ?? new StringBuilder();
            try
            {
                indent = indent ?? string.Empty;
                var fields = GetTableMapping(obj).AllFields;
                foreach (var field in fields)
                {
                    sb.Append(indent);
                    sb.Append(field.ColumnName);
                    sb.Append(" = ");
                    var value = field.GetValue(obj);
                    if (value == null)
                        sb.AppendLine("<NULL>");
                    else if (value is DynamicEnum)
                        sb.AppendLine((value as DynamicEnum).EnumId + " (meaning " + value.GetType().Name + "." + (value as DynamicEnum).EnumName + ")");
                    else if (value is IHasTypedIdReadable)
                        sb.AppendLine((value as IHasTypedIdReadable).TypedId.AsDebugString);
                    else if (value is IConvertible)
                        sb.AppendLine((value as IConvertible).ToStringInvariant());
                    else if (value is TimeSpan) // only type that not intuitively implements IConvertible
                        sb.AppendLine(((TimeSpan)value).ToString("hh':'mm':'ss'.'fff"));
                    else
                        sb.AppendLine("DO NOT KNOW HOW TO SERIALIZE");
                }
            }
            catch (Exception ex)
            {
                sb.AppendLine("<Error while serializing more>" + Environment.NewLine + ex.ToString());
            }
            return sb.ToString();
        }
    }
}
