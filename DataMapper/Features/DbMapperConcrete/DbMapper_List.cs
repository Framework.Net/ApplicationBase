﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;

using DataMapper.Tools;
using DataMapper.Helpers;

namespace DataMapper
{
    internal partial class DbMapperConcrete
    {
        public RemoteContext CreateRemoteContext()
        {
            return new RemoteContext();
        }

        /// <summary>
        /// Helper method to load a collection of child object containing the link (fk) to their parent object
        /// </summary>
        /// <typeparam name="TParentObject"></typeparam>
        /// <typeparam name="TChildObject"></typeparam>
        /// <typeparam name="TFkProperty"></typeparam>
        /// <param name="distinctParentObjects">List of parent objects</param>
        /// <param name="uniqueMatchingKey">Expression to get the key from a child object</param>
        /// <returns></returns>
        public List<TChildObject> LoadChildCollection<TParentObject, TChildObject, TFkProperty>(IEnumerable<TParentObject> distinctParentObjects, Expression<Func<TChildObject, TFkProperty>> uniqueMatchingKey)
            where TParentObject : class, IAutoMappedDbObject, IHasTypedIdReadable
            where TChildObject : class, IAutoMappedDbObject, IHasTypedIdReadable
            where TFkProperty : IEquatable<TFkProperty>
        {
            DbMappedTable mParentTable = GetTableMapping(typeof(TParentObject));
            DbMappedTable mChildTable = GetTableMapping(typeof(TChildObject));
            var propRelation = mChildTable.GetMappedColumnFromProperty(uniqueMatchingKey);
            if (propRelation == null)
                throw new TechnicalException($"Property {mChildTable.MappedType.Name}.{GetMemberName.For(uniqueMatchingKey)} has no mapping on database!", null);
            DbForeignKey fk = null;
            if (propRelation.ForeignKeys.ConsumeThenEnumerate(1, firstFk => fk = firstFk).Any())
                throw new TechnicalException($"There is more than one Foreign key using property {mChildTable.MappedType.Name}.{propRelation.PropertyInfo.Name}." +
                                             $"Use method {nameof(EnumerateCollectionMatching)} instead!", null);
            if (fk == null)
                throw new TechnicalException($"Property {mChildTable.MappedType.Name}.{propRelation.PropertyInfo.Name} is not used " +
                                             $"in an attribute of type {nameof(DbForeignKey)} as a foreign key!", null);
            if (mParentTable.PkFields.Count != 1)
                throw new TechnicalException($"Parent/Child relation between {mParentTable.MappedType.Name}" +
                                             $" and {mChildTable.MappedType.Name}," +
                                             $" use more than one value!", null);
            if (mParentTable.PkField.PropertyInfo.PropertyType != typeof(TFkProperty))
                throw new TechnicalException($"Parent/Child relation is badly defined, Child' FK property and Parent primary key have types that mismatch!", null);
            if (!typeof(TParentObject).IsAssignableFrom(fk.To) &&
                !fk.To.IsAssignableFrom(typeof(TParentObject)))
                throw new TechnicalException($"Property {mChildTable.MappedType.Name}.{propRelation.PropertyInfo.Name} is referencing type {fk.To}" + Environment.NewLine +
                                             $"which is not related to type {typeof(TParentObject)}!", null);
            return EnumerateCollectionMatching(distinctParentObjects.Select(obj => (TFkProperty)obj.Id.Keys[0]), uniqueMatchingKey)
                    .ToList();
        }

        /// <summary>
        /// Allow to load a collection of object (of type <typeparamref name="TObject"/>)
        /// where values for one of their a specific property are in set (<paramref name="distinctValues"/>).
        /// This allow developper to do some kind of a "join" betwen objects (in C#), and a sql table.
        /// </summary>
        public IEnumerable<TObject> EnumerateCollectionMatching<TObject, TProperty>(IEnumerable<TProperty> distinctValues, Expression<Func<TObject, TProperty>> uniqueMatchingKey, string whereCondition = null)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
            where TProperty : IEquatable<TProperty>
        {
            var mappedField = GetTableMapping(typeof(TObject)).AllFieldsByPropertyName[GetMemberName.For(uniqueMatchingKey)];
            return EnumerateCollectionMatching<TObject, TProperty>(distinctValues, mappedField, whereCondition);
        }
        /// <summary>
        /// Same than <see cref="EnumerateCollectionMatching{TObject, TProperty}(IEnumerable{TProperty}, Expression{Func{TObject, TProperty}}, string)"/>
        /// But specifying directly a mapped field instead of expression
        /// </summary>
        public IEnumerable<TObject> EnumerateCollectionMatching<TObject, TProperty>(IEnumerable<TProperty> distinctValues, DbMappedField uniqueMatchingKey, string whereCondition = null)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
            where TProperty : IEquatable<TProperty>
        {
            var valuesToLoad = distinctValues.Select(v => new RemoteTuple<TProperty>(v));
            var context = CreateRemoteContext().With(valuesToLoad, out string tableIds);
            var result = EnumerateCollectionFromInnerJoinSql<TObject>(context: context,
                                                                      innerJoin: tableIds + " as I",
                                                                      joinCondition: "A.[" + uniqueMatchingKey.ColumnName + "] = I.Item1",
                                                                      whereCondition: whereCondition);
            return result;
        }



        public List<TObject> LoadCollection<TObject>(string whereCondition = null, string join = null, string joinCondition = null)
            where TObject : class, IAutoMappedDbObject
        {
            List<TObject> result =
            SqlRetry.Instance.HandleRetry("LoadingCollection of " + typeof(TObject).Name, true, () =>
            {
                var res = new List<TObject>();
                var enumerable = EnumerateCollectionFromInnerJoinSql<TObject>(whereCondition, join, joinCondition);
                foreach (var item in enumerable)
                    res.Add(item);
                return res;
            });
            return result;
        }
        public List<TObject> LoadCollection<TObject>(Func<TObject, bool> filter, string whereCondition)
            where TObject : class, IAutoMappedDbObject
        {
            // No way to calculate whereCondition automatically... yet
            return LoadCollection<TObject>(whereCondition);
        }
        [Obsolete("Feature not available yet! You have to specify equivalent sql condition manually", true)]
        public List<TObject> LoadCollection<TObject>(Func<TObject, bool> filter)
            where TObject : class, IAutoMappedDbObject
        {
            return null;
        }


        public int Count<TObject>(string whereCondition, string join = null)
            where TObject : class, IAutoMappedDbObject
        {
            DbMappedTable mtable = GetTableMapping(typeof(TObject));
            string query = string.Format("SELECT count(1) FROM {0} {1} {2}",
                                         mtable.FullName,
                                         " as A WITH (NOLOCK)" + (string.IsNullOrWhiteSpace(join) ? "" : " INNER JOIN " + join),
                                         string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition);

            using (var cn = GetConnectionProvider())
                // ReSharper disable once AccessToDisposedClosure
                return Trace(cn, query, () => (int)cn.ExecuteScalar(query, true));
        }
        public IEnumerable<TObject> EnumerateCollectionFromInnerJoinSql<TObject>(string whereCondition, string innerJoin, string joinCondition, RemoteContext context = null)
            where TObject : class, IAutoMappedDbObject
        {
            Debug.Assert(string.IsNullOrWhiteSpace(innerJoin) == string.IsNullOrWhiteSpace(joinCondition));
            DbMappedTable mtable = GetTableMapping(typeof(TObject));
            string sqlQuery = string.Format("SELECT {0} FROM {1} {2} {3}",
                                             string.Join(", ", mtable.AllFields.Select(f => "A." + f.ColumnNameProtected)),
                                             mtable.FullName,
                                             " as A" + (string.IsNullOrWhiteSpace(innerJoin) ? "" : " INNER JOIN " + innerJoin + " ON " + joinCondition),
                                             string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition);
            return EnumerateCollectionFromSqlQuery<TObject>(sqlQuery, context);
        }
        /// <param name="whereCondition">in form 'A.Foo = 42 and B.Bar = 51 </param>
        /// <param name="outerJoin">in form "TableToJoin as B"</param>
        /// <param name="joinCondition">in form "A.Id = B.AnotherId</param>
        public IEnumerable<TObject> EnumerateCollectionFromOuterJoinSql<TObject>(string whereCondition, string outerJoin, string joinCondition, RemoteContext context = null)
            where TObject : class, IAutoMappedDbObject
        {
            Debug.Assert(string.IsNullOrWhiteSpace(outerJoin) == string.IsNullOrWhiteSpace(joinCondition));
            DbMappedTable mtable = GetTableMapping(typeof(TObject));
            string sqlQuery = string.Format("SELECT {0} FROM {1} as A {2} {3}",
                                             string.Join(", ", mtable.AllFields.Select(f => "A." + f.ColumnNameProtected)),
                                             mtable.FullName,
                                             (string.IsNullOrWhiteSpace(outerJoin) ? "" : " LEFT OUTER JOIN " + outerJoin + " ON " + joinCondition),
                                             string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition);
            return EnumerateCollectionFromSqlQuery<TObject>(sqlQuery, context);
        }

        public IEnumerable<TObject> EnumerateCollectionFromSqlQuery<TObject>(string sqlQuery, RemoteContext context = null)
            where TObject : class, IAutoMappedDbObject
        {
            DbMappedTable mtable = GetTableMapping(typeof(TObject));
            if (typeof(IHasOptimizedLoading).IsAssignableFrom(mtable.MappedType))
                return EnumerateCollectionFromSqlOptimized<TObject>(sqlQuery, context);
            using (var cn = GetConnectionProvider())
                return EnumerateConvertedDataRow<TObject>(Trace(cn, sqlQuery, () => cn.GetDataTable(sqlQuery, context).AsEnumerable()), mtable);
        }
        private IEnumerable<TObject> EnumerateCollectionFromSqlOptimized<TObject>(string query, RemoteContext context = null)
             where TObject : class, IAutoMappedDbObject // and IHasOptimizedLoading
        {
            using (var cn = GetConnectionProvider())
                foreach (var item in EnumerateConvertedDbSource<TObject>(TraceEnumerable(cn, query, cn.GetEnumerableReader(query, context))))
                    yield return item;
        }

        // This method actually fetches the answer of a request into instances of class. Name to change later
        private IEnumerable<TObject> EnumerateCollection<TObject>(DbDataReader reader)
            where TObject : class, IAutoMappedDbObject // and IHasOptimizedLoading
        {
            Debug.Assert(typeof(IHasOptimizedLoading).IsAssignableFrom(typeof(TObject)));
            foreach (var item in EnumerateConvertedDbSource<TObject>(AsEnumerable(reader)))
                yield return item;
        }
        IEnumerable<DbDataReader> AsEnumerable(DbDataReader reader)
        {
            while (reader.Read())
                yield return reader;
        }

        // This method actually fetches the answer of a request into instances of class. Name to change later
        private IEnumerable<TObject> EnumerateConvertedDbSource<TObject>(IEnumerable<DbDataReader> readerEnumerable)
            where TObject : class, IAutoMappedDbObject // and IHasOptimizedLoading
        {
            Debug.Assert(typeof(IHasOptimizedLoading).IsAssignableFrom(typeof(TObject)));
            void fillItem(TObject itemToFill, DbDataReader rdr) => (itemToFill as IHasOptimizedLoading).FillFromReader(rdr);
            foreach (var itemLoaded in EnumerateConvertedResult<TObject, DbDataReader>(readerEnumerable, fillItem))
                yield return itemLoaded;
        }
        // This method actually fetches the answer of a request into instances of class. Name to change later
        public IEnumerable<TObject> EnumerateConvertedDataRow<TObject>(IEnumerable<DataRow> dataRows, DbMappedTable mtable = null)
            where TObject : class, IAutoMappedDbObject
        {
            mtable = mtable ?? GetTableMapping(typeof(TObject));
            void fillItem(TObject obj, DataRow row) => LoadProperties(obj, row, mtable);
            foreach (var item in EnumerateConvertedResult<TObject, DataRow>(dataRows, fillItem))
                yield return item;
        }
        // This method actually fetches the answer of a request into instances of class. Name to change later
        private IEnumerable<TObject> EnumerateConvertedResult<TObject, TItemDataSource>(IEnumerable<TItemDataSource> sources, Action<TObject, TItemDataSource> fill)
            where TObject : class, IAutoMappedDbObject // and IHasOptimizedLoading
        {
            bool hasOwnerMapper = typeof(BaseDTO).IsAssignableFrom(typeof(TObject));
            var mtable = GetTableMapping(typeof(TObject));
            Func<object> cons = DefaultObjectFactory.ConstructorFor(mtable.MappedType);
            TObject item = null;
            foreach (var source in sources)
            {
                if (item == null) // If we dont recycle previous item
                {
                    item = (TObject)cons();
                    if (hasOwnerMapper)
                        // ReSharper disable once PossibleNullReferenceException
                        (item as BaseDTO).OwnerMapper = this;
                }
                fill(item, source);

                if (!(item is IHasTypedIdReadable asHavingTypedId))
                {
                    yield return item;
                    item = null; // cannot recycle item
                }
                else
                {
                    var cachedItem = (TObject)_cache.GetExistingOrAdd(asHavingTypedId);
                    if (ReferenceEquals(cachedItem, asHavingTypedId))
                        item = null; // cannot recycle item
                    else
                        cachedItem.CopyFrom(item); // update cahed item that already exists
                    yield return cachedItem; // we can recycle item here
                }
            }
        }

        public List<TObject> LoadCollectionExclusive<TObject, TPropertyValue>(Expression<Func<TObject, TPropertyValue>> getAtomicProperty,
                                                                              TPropertyValue valueToTest,
                                                                              TPropertyValue valueToSet,
                                                                              string otherFilter = null)
            where TObject : class, IAutoMappedDbObject
        {
            DbMappedTable mtable = GetTableMapping(typeof(TObject));
            var colNameToTest = GetColumnNameFor(getAtomicProperty);
            var checkCond = colNameToTest + (valueToSet == null ? " is " : " = ") + DbMappedField.ToSql(valueToTest);

            string sqlQuery = "UPDATE " + mtable.FullName
                            + " SET " + colNameToTest + " = " + DbMappedField.ToSql(valueToSet)
                            + " OUTPUT " + string.Join(", ", mtable.AllFields.Select(f => "inserted." + f.ColumnNameProtected))
                            + " WHERE " + checkCond + otherFilter.IfNotBlankAddPrefix(" and (").IfNotBlankAddSuffix(")");
            var enumerable = EnumerateCollectionFromSqlQuery<TObject>(sqlQuery);
            var res = new List<TObject>();
            foreach (var item in enumerable)
                res.Add(item);
            return res;
        }




        public void CreateInDatabaseCollectionSlow<TObject>(IReadOnlyCollection<TObject> items, bool with_transaction = true)
            where TObject : IAutoMappedDbObject
        {
            if (!items.Any())
                return;

            DbMappedTable mtable = GetTableMapping(items.First().GetType());
            // Pour envoyer plus de 1000 ligne d'un coup, voir hack ici http://dba.stackexchange.com/questions/82921/the-number-of-row-value-expressions-in-the-insert-statement-exceeds-the-maximum
            // Il parait que si nombre de colonne * nombre de row > 1000, le serveur change de methode de construction du plan d'execution
            // Et peut donner l'impression de perdre du temps si la connection est rapide (reseau interne)
            // Interessant aussi : http://stackoverflow.com/questions/8635818/multiple-insert-statements-vs-single-insert-with-multiple-values/8640583#8640583
            if (items.Count > 1000)
            {
                int n = Math.DivRem(items.Count, 1000, out int remain);
                var all_items = items.ToList();
                for (int s = 0; s < n; ++s)
                    CreateInDatabaseCollection(all_items.GetRange(s * 1000, 1000), with_transaction);
                if (remain > 0)
                    CreateInDatabaseCollection(all_items.GetRange(n * 1000, remain), with_transaction);
            }
            else
            {
                string sql_request = GetSqlFor_CreateInDatabase(mtable, items);
                using (var cn = GetConnectionProvider())
                {
                    // ReSharper disable AccessToDisposedClosure
                    Debug.Assert(cn != null);
                    using (IDbTransaction tran = with_transaction ? cn.StartTransaction() : null)
                    {
                        if (mtable is DbMappedJoinTable)
                            Trace(cn, sql_request, () => cn.ExecuteNonQuery(sql_request));
                        else
                        {
                            // On choisit -1 car 0 peut etre utilisé comme id (de meme pour les tables representant des enums, donc on choisit -1)
                            //int id = Convert.ToInt32(DbMapper.cn.ExecuteScalarNet<decimal>(sql_request, -1));
                            //int id = Convert.ToInt32(DbMapper.cn.ExecuteScalar(sql_request));

                            var dtIds = Trace(cn, sql_request, () => cn.GetDataTable(sql_request));

                            // Met à jour l'id des objets créés
                            // si l'objet est nouveau et a ete enregistre dans une table qui a priori contient un, et un seul, id
                            if (mtable.AutoIncrementedField != null)
                            {
                                int r = 0;
                                foreach (var item in items)
                                {
                                    Debug.Assert(!object.Equals(mtable.AutoIncrementedField.GetValue(item), dtIds.Rows[r][0]));
                                    mtable.AutoIncrementedField.SetValue(item, dtIds.Rows[r][0]);
                                    ++r;
                                }
                            }
                            if (typeof(IHasTypedIdReadable).IsAssignableFrom(typeof(TObject)))
                                foreach (TObject item in items)
                                    _cache.Add((IHasTypedIdReadable)item);
                        }
                        tran?.Commit();
                    }
                    // ReSharper restore AccessToDisposedClosure
                }
                bool hasOwnerMapper = typeof(BaseDTO).IsAssignableFrom(typeof(TObject));
                if (hasOwnerMapper)
                    foreach (var item in items)
                        if (((BaseDTO)(object)item).OwnerMapper == null)
                            ((BaseDTO)(object)item).OwnerMapper = this;

            }
        }
        public void CreateInDatabaseCollection(IReadOnlyCollection<IAutoMappedDbObject> items, bool with_transaction = true)
        {
            if (!items.Any())
                return;
            if (items.GroupBy(item => item.GetType()).Count() != 1)
                throw new TechnicalException("Only one concrete type is allowed!", null);
            var t = items.First().GetType();
            var info = (ICreateInDatabaseCollectionReflection)Activator.CreateInstance(typeof(CreateInDatabaseCollectionReflection<>).MakeGenericType(t));
            var enumerableStronglyTyped = info.Cast.Invoke(null, new object[] { items });
            var listStronglyTyped = info.ToList.Invoke(null, new[] { enumerableStronglyTyped });
            info.CreateInDatabaseCollection.Invoke(this, new[] { listStronglyTyped, with_transaction });
        }

        public int UpdateToDatabaseCollection<TObject>(IReadOnlyCollection<TObject> items, params Expression<Func<TObject, object>>[] propertiesToUpdate)
            where TObject : IAutoMappedDbObject
        {
            if (items is null || items.Count == 0)
                return 0;

            var firstItem = items.First();
            var mtable = GetTableMapping(firstItem.GetType());

            var dbMappedFieldsToUpdate = propertiesToUpdate?.Select(p =>
            {
                var property = mtable.MappedType.GetProperty(GetMemberName.GetMemberNameFor(firstItem, p));
                var dbMappedField = mtable.AllFieldsByProperty[property];
                return dbMappedField;

            }).ToArray();

            var sqlParts = items.Select(i => GetSqlFor_UpdateToDatabase(i, mtable, null, dbMappedFieldsToUpdate));
            var fullSql = sqlParts.Join(Environment.NewLine);

            int affectedRows = 0;

            using(var cn = GetConnectionProvider())
            {
                using(IDbTransaction tran = cn.StartTransaction())
                {
                    affectedRows = Trace(cn, fullSql, () => cn.ExecuteNonQuery(fullSql));
                }
            }

            return affectedRows;
        }

        interface ICreateInDatabaseCollectionReflection
        {
            MethodInfo CreateInDatabaseCollection { get; }
            MethodInfo Cast { get; }
            MethodInfo ToList { get; }
        }
        class CreateInDatabaseCollectionReflection<T> : ICreateInDatabaseCollectionReflection
        {
            // ReSharper disable PossibleNullReferenceException
            static readonly MethodInfo CreateInDatabaseCollection = typeof(IDbMapperWriteAccess).GetMethods()
                                                                                                .Single(m => m.Name == nameof(IDbMapperWriteAccess.CreateInDatabaseCollection) && m.IsGenericMethod)
                                                                                                .MakeGenericMethod(typeof(T));
            static readonly MethodInfo Cast = typeof(Enumerable).GetMethod(nameof(Enumerable.Cast)).MakeGenericMethod(typeof(T));
            static readonly MethodInfo ToList = typeof(Enumerable).GetMethod(nameof(Enumerable.ToList)).MakeGenericMethod(typeof(T));
            // ReSharper restore PossibleNullReferenceException

            MethodInfo ICreateInDatabaseCollectionReflection.CreateInDatabaseCollection { get { return CreateInDatabaseCollection; } }
            MethodInfo ICreateInDatabaseCollectionReflection.Cast { get { return Cast; } }
            MethodInfo ICreateInDatabaseCollectionReflection.ToList { get { return ToList; } }
        }

        public void CreateInDatabaseCollection<TObject>(IReadOnlyCollection<TObject> items, bool with_transaction = true)
            where TObject : IAutoMappedDbObject
        {
            var itemCount = items.Count();
            if (itemCount == 0)
                return;

            DbMappedTable mtable = GetTableMapping(items.First().GetType());
            if (mtable.AutoIncrementedField != null && mtable.AutoIncrementedField.PropertyInfo.PropertyType != typeof(int)
                                                    && mtable.AutoIncrementedField.PropertyInfo.PropertyType != typeof(long))
                throw new TechnicalException("Auto incremented column \"" + mtable.AutoIncrementedField.ColumnName + "\" of table \"" + mtable.FullName + "\" must be of type int or bigint in order to be retrieved automatically in bulk insert!", null);
            object lastInsertedIdAsObj = null; // will store the last inserted id. We dont use the current max + 1 because we cannot know if the seed has been changed or the last added row has been deleted

            // Basically we want to do this in SQL :
            //BEGIN TRAN
            //BEGIN TRY
            //select top 1 * from  [Reconciliation].[AtosLogsDegraded]  WITH (TABLOCKX, HOLDLOCK) order by Id desc  // Ensure the second select will see only data insert by us

            // // Bulk Insert is like
            //INSERT INTO YouTable ([Field1], [Field2], ...)
            // // OUTPUT INSERTED.Id // Just to test, the bulk import does not output the id
            // VALUES
            //( 'Field1', 'Field2');
            //WAITFOR DELAY '00:00:05'; // To run a concurrent request to test
            //select top 1 * from  [Reconciliation].[AtosLogsDegraded]  WITH (TABLOCKX, HOLDLOCK) order by Id desc
            // the select above should always return what the OUTPUT INSERTED returned
            // Even if there is a concurrent request in another thread
            //COMMIT TRAN
            //END TRY
            //BEGIN CATCH
            //ROLLBACK TRANSACTION
            //-- any other error handling goes here
            //END CATCH

            using (var cn = GetConnectionProvider())
                // ReSharper disable once AccessToDisposedClosure
                cn.WithConnectionDo((sqlConnection, itran) => Trace(cn, "Bulk insert of " + items.Count() + " items of type " + typeof(TObject).Name, () =>
                {
                    sqlConnection.OpenOrWaitUntilOpen();
                    using (var sqlTran = sqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted))
                    {
                        //object currentMax;
                        if (mtable.AutoIncrementedField != null) // This allow to lock the whole table
                            using (var cmd = sqlConnection.CreateCommand())
                            {
                                cmd.CommandText = "SELECT TOP 1 " + mtable.AutoIncrementedField.ColumnNameProtected + " FROM " + mtable.FullName + " WITH (TABLOCKX, HOLDLOCK) ORDER BY " + mtable.AutoIncrementedField.ColumnNameProtected + " DESC;";
                                cmd.Transaction = sqlTran;
                                /*currentMax = */cmd.ExecuteScalar();
                            }
                        //    cn.ExecuteScalar();

                        using (var bulkCopy = new SqlBulkCopy(sqlConnection,
                                                         SqlBulkCopyOptions.KeepNulls | // Objects should always match what's in database and default value for property should be in business layer anyway
                                                         SqlBulkCopyOptions.CheckConstraints | // if Constraints exist and the CHECK_CONSTRAINTS hint is not specified and connection does not have ALTER TABLE rights, we need this
                                                         SqlBulkCopyOptions.FireTriggers | // if Triggers exist and the FIRE_TRIGGER hint is not specified and connection does not have ALTER TABLE rights, we need this
                                                         SqlBulkCopyOptions.TableLock, // Increase performance and make
                                                         externalTransaction: sqlTran))
                        {
                            bulkCopy.DestinationTableName = mtable.FullName;
                            var to = IDbMapperSharedSettings.Instance.BulkInsertTimeout;
                            if (to.HasValue)
                                bulkCopy.BulkCopyTimeout = to == 0 || to >= int.MaxValue ? 0 : (int)to.Value;
                            bulkCopy.EnableStreaming = true; // Make bulkCopy avoiding to read all source before sending
                            foreach (var f in mtable.AllFields)
                                if (mtable.AutoIncrementedField != f)
                                    bulkCopy.ColumnMappings.Add(f.ColumnName, f.ColumnName);
                            using (var dataReader = new MappedDbObjectDataReader<TObject>(GetTableMapping(typeof(TObject)), items))
                            {
                                bulkCopy.WriteToServer(dataReader);
                            }
                        }
                        if (mtable.AutoIncrementedField != null)
                            using (var cmd = sqlConnection.CreateCommand())
                            {
                                cmd.CommandText = "SELECT TOP 1 " + mtable.AutoIncrementedField.ColumnNameProtected + " FROM " + mtable.FullName + " WITH (TABLOCKX, HOLDLOCK) ORDER BY " + mtable.AutoIncrementedField.ColumnNameProtected + " DESC;";
                                cmd.Transaction = sqlTran;
                                lastInsertedIdAsObj = cmd.ExecuteScalar();
                            }
                        sqlTran.Commit();
                    }
                    // Critical section : Synchronzing DB & software data
                    if (mtable.AutoIncrementedField != null)
                    {
                        if (lastInsertedIdAsObj is long)
                        {
                            var id = (long)lastInsertedIdAsObj - itemCount;
                            foreach (var item in items)
                                mtable.AutoIncrementedField.SetValue(item, ++id);
                        }
                        else if (lastInsertedIdAsObj is int)
                        {
                            var id = (int)lastInsertedIdAsObj - itemCount;
                            foreach (var item in items)
                                mtable.AutoIncrementedField.SetValue(item, ++id);
                        }
                    }

                    if (typeof(IHasTypedIdReadable).IsAssignableFrom(typeof(TObject)))
                        foreach (TObject item in items)
                            _cache.Add((IHasTypedIdReadable)item);
                }));

            bool hasOwnerMapper = typeof(BaseDTO).IsAssignableFrom(typeof(TObject));
            if (hasOwnerMapper)
                foreach (var item in items)
                    // ReSharper disable once PossibleNullReferenceException
                    (item as BaseDTO).OwnerMapper = this;
        }

        //public void DeleteInDatabaseCollection<TObject, TKey>(IReadOnlyCollection<TObject> items, bool with_transaction)
        //    where TObject : BaseDTO<IdTuple<TKey>>, IAutoMappedDbObject
        //    where TKey : IEquatable<TKey>
        public void DeleteInDatabaseCollection<TObject, TKey>(IReadOnlyCollection<TObject> items, bool with_transaction = true)
            where TObject : BaseDTO<TKey>, IAutoMappedDbObject
            where TKey : struct, IEquatable<TKey>, IIdTupleWithOnlyOneKey
        {
            if (items.Count == 0)
                return;
            LongIdSet setOfIds;
            if (typeof(TKey) == typeof(IdTuple<long>))
                setOfIds = items?.Select(item => (long)item.ClosedId.Id1).ToMappedIntegerSet();
            else if (typeof(TKey) == typeof(IdTuple<int>))
                setOfIds = items?.Select(item => (long)(int)item.ClosedId.Id1).ToMappedIntegerSet();
            else if (typeof(TKey) == typeof(IdTuple<ulong>))
                setOfIds = items?.Select(item => (long)(ulong)item.ClosedId.Id1).ToMappedIntegerSet();
            else if (typeof(TKey) == typeof(IdTuple<uint>))
                setOfIds = items?.Select(item => (long)(uint)item.ClosedId.Id1).ToMappedIntegerSet();
            else if (typeof(TKey) == typeof(IdTuple<short>))
                setOfIds = items?.Select(item => (long)(short)item.ClosedId.Id1).ToMappedIntegerSet();
            else if (typeof(TKey) == typeof(IdTuple<ushort>))
                setOfIds = items?.Select(item => (long)(ushort)item.ClosedId.Id1).ToMappedIntegerSet();
            else if (typeof(TKey) == typeof(IdTuple<byte>))
                setOfIds = items?.Select(item => (long)(byte)item.ClosedId.Id1).ToMappedIntegerSet();
            else if (typeof(TKey) == typeof(IdTuple<sbyte>))
                setOfIds = items?.Select(item => (long)(sbyte)item.ClosedId.Id1).ToMappedIntegerSet();
            else
                throw new TechnicalException($"{typeof(TKey).Name} is not a key type handled in {nameof(DeleteInDatabaseCollection)}!", null);

            DbMappedTable mtable = GetTableMapping(items.First());
            string sql_request = "DELETE FROM " + mtable.FullName
                               + " WHERE "
                               + setOfIds.AsSqlCondition(mtable.PkField.ColumnNameProtected);

            using (var cn = GetConnectionProvider())
            {
                // ReSharper disable AccessToDisposedClosure
                Debug.Assert(cn != null, nameof(cn) + " != null");
                using (IDbTransaction tran = with_transaction ? cn.StartTransaction() : null)
                {
                    //Console.WriteLine("Delete " + typeof(TObject).Name + ": " + item.GetSqlIds());
                    Trace(cn, sql_request, () => cn.ExecuteNonQuery(sql_request));
                    tran?.Commit();
                }
                // ReSharper restore AccessToDisposedClosure
            }
            foreach (var item in items)
                if (item != null && item.OwnerMapper == null)
                    item.OwnerMapper = this;
        }

        // Pour l'update : http://stackoverflow.com/questions/20255138/sql-update-multiple-records-in-one-query
        // Mais attention : https://connect.microsoft.com/SQLServer/feedback/details/773895/merge-incorrectly-reports-unique-key-violations
        // une rreur parmis plusieurs : https://www.mssqltips.com/sqlservertip/3074/use-caution-with-sql-servers-merge-statement/
        // Engloiber dans une transaction car MERGE n'est pas atomique
        //  Exemple :
        // INSERT INTO mytable (id, a, b, c)
        // VALUES(1, 'a1', 'b1', 'c1'),
        // (2, 'a2', 'b2', 'c2'),
        // (3, 'a3', 'b3', 'c3'),
        // (4, 'a4', 'b4', 'c4'),
        // (5, 'a5', 'b5', 'c5'),
        // (6, 'a6', 'b6', 'c6')
        // ON DUPLICATE KEY UPDATE id=VALUES(id),
        // a=VALUES(a),
        // b=VALUES(b),
        // c=VALUES(c);

        #region Cache


        //void HandleCache<TObject, TRowData>(ref TObject item, TRowData row, Action<TObject, TRowData> fill, Action<TObject> @add, MethodInfo mTryGetFromCacheByTypedId)
        //    where TObject : class, IAutoMappedDbObject
        //{
        //    bool hasOwnerMapper = typeof(BaseDTO).IsAssignableFrom(typeof(TObject));

        //    if (item == null)
        //    {
        //        item = new TObject();
        //        if (hasOwnerMapper)
        //            (item as BaseDTO).OwnerMapper = this;
        //    }

        //    fill(item, row);
        //    var asHavingTypedId = item as IHasTypedIdReadable;
        //    if (asHavingTypedId == null)
        //    {
        //        @add(item);
        //        item = null;
        //    }
        //    else
        //    {
        //        var cachedItem = (TObject)mTryGetFromCacheByTypedId.Invoke(_cache, new[] { (object)asHavingTypedId.TypedId });
        //        if (cachedItem == null)
        //        {
        //            _cache.Add(asHavingTypedId);
        //            @add(item);
        //            item = null;
        //        }
        //        else
        //        {
        //            cachedItem.CopyFrom(item); // update
        //            @add(cachedItem);
        //        }
        //    }
        //}
        IEnumerable<TObject> SubstituteByCachedItem<TObject>(IEnumerable<TObject> items)
        {
            if (typeof(IHasTypedIdReadable).IsAssignableFrom(typeof(TObject)))
                foreach (var item in SubstituteByCachedItem(items.Cast<IHasTypedIdReadable>()))
                    yield return (TObject)item;
            else
                foreach (var item in items)
                    yield return item;
        }
        IEnumerable<IHasTypedIdReadable> SubstituteByCachedItem(IEnumerable<IHasTypedIdReadable> items)
        {
            foreach (var item in items)
            {
                var cachedItem = _cache.GetExistingOrAdd(item);
                if (!ReferenceEquals(cachedItem, item))
                {
                    if (cachedItem is ICopyable asCopyable)
                        asCopyable.CopyFrom((ICopyable)item); // update cahed item that already exists
                }
                yield return cachedItem;
            }
        }
        //MethodInfo GetMethodTryGetFromCache<TObject>()
        //{
        //    MethodInfo gmi = null;
        //    if (typeof(IHasTypedIdReadable).IsAssignableFrom(typeof(TObject)) &&
        //        !_genericTryGets.TryGetValue(typeof(TObject), out gmi))
        //    {
        //        gmi = _genericTryGetMethod.MakeGenericMethod(typeof(TObject));
        //        _genericTryGets.TryAdd(typeof(TObject), gmi);
        //    }
        //    return gmi;
        //}
        //static readonly MethodInfo _genericTryGetMethod = typeof(CacheManager).GetMethod(nameof(CacheManager.TryGet), BindingFlags.Instance | BindingFlags.Public, null, new[] { typeof(ITypedId) }, null);
        //static readonly ConcurrentDictionary<Type, MethodInfo> _genericTryGets = new ConcurrentDictionary<Type, MethodInfo>();

        #endregion

        public void Truncate<TObject>()
            where TObject : IAutoMappedDbObject
        {
            DbMappedTable mtable = GetTableMapping(typeof(TObject));
            using (var cn = GetConnectionProvider())
                cn.ExecuteNonQuery("delete from [" + mtable.FullName + "]");
        }
    }

}
