﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace DataMapper
{
    public abstract class DbGeneratedMapper : DbMapperWrapper
    {
        public DbGeneratedMapper(IDbMapper mapper)
            : base(mapper)
        {
        }

        public abstract Dictionary<Type, Type> AllTypes { get; }
    }

    //public partial class DbGeneratedMapper2 : DbGeneratedMapper
    //{
    //    public DbGeneratedMapper2(IDbMapper mapper)
    //        : base(mapper)
    //    {
    //    }

    //    public override Dictionary<Type, Type> AllTypes { get; } = new List<Type>()
    //    {
    //        typeof(int),
    //        typeof(int),
    //    }.ToDictionary(t => t);
    //}
}