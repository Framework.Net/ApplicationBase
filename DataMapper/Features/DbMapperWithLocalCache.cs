﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.Tools;

namespace DataMapper
{
    class DbMapperWithLocalCache : DbMapperWrapper
    {
        readonly string _cachePath;
        readonly Version _version;

        public static bool Enabled = false;

        public DbMapperWithLocalCache(IDbMapper mapper, string cachePath, Version version)
            : base(mapper)
        {
            _cachePath = cachePath;
            _version = version;
            Directory.CreateDirectory(cachePath);
        }

        public override List<TObject> LoadCollection<TObject>(string whereCondition = null, string join = null, string joinCondition = null)
        {
            List<TObject> res;
            var mTable = GetTableMapping(typeof(TObject));
            var fileName = Path.Combine(_cachePath, mTable.FullName) + _version.ToString() + ".dat";
            bool noFilter = string.IsNullOrWhiteSpace(whereCondition)
                         && string.IsNullOrWhiteSpace(join)
                         && string.IsNullOrWhiteSpace(joinCondition);

            if (Enabled && noFilter)
            {
                if (File.Exists(fileName))
                {
                    res = null;
                    if (null == ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
                    {
                        string data;
                        res = DeserializeFromFile<TObject>(fileName, str => data = str).ToList();
                    }))
                        return res;
                }
            }
            res = base.LoadCollection<TObject>(whereCondition, join, joinCondition);
            if (Enabled && noFilter)
            {
                var th = _ths.GetOrAdd(fileName, _ => new Thread((ThreadStart)(() =>
                {
                    ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
                    {
                        SerializeToFile(res, fileName + ".tmp", _version.ToString());
                        File.Move(fileName + ".tmp", fileName + ".tmp");
                        _ths.TryRemove(fileName, out Thread _);
                    });
                })));
                if (th.ThreadState == System.Threading.ThreadState.Unstarted)
                    th.Start();
            }
            return res;
        }
        readonly ConcurrentDictionary<string, Thread> _ths = new ConcurrentDictionary<string, Thread>();
        #region Cache Feature


        public void SerializeToFile<TItem>(IEnumerable<TItem> lst, string filename, string otherData = null)
             where TItem : IAutoMappedDbObject
        {
            using (var fOut = System.IO.File.Open(filename, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                foreach (var bs in SerializeValue(otherData, typeof(string), new byte[1]))
                    fOut.Write(bs, 0, bs.Length);
                fOut.WriteByte((byte)'\n');
                foreach (var bs in Serialize(lst))
                    fOut.Write(bs, 0, bs.Length);
                fOut.Flush();
                fOut.Close();
            }
        }

        public IEnumerable<byte[]> Serialize<TItem>(IEnumerable<TItem> lst)
             where TItem : IAutoMappedDbObject
        {
            byte[] oneByte = new byte[1];
            var mTable = GetTableMapping(typeof(TItem));

            foreach (var bs in SerializeValue(GetMetadataToSerialize(mTable).ToString(), typeof(string), oneByte))
                yield return bs;

            foreach (var item in lst)
            {
                oneByte[0] = (byte)'\n'; yield return oneByte;
                foreach (var bs in SerializeItem(item, mTable.AllFields))
                    yield return bs;
            }
        }
        public IEnumerable<byte[]> SerializeItem<TItem>(TItem item)
            where TItem : IAutoMappedDbObject
        {
            var mTable = GetTableMapping(typeof(TItem));
            return SerializeItem(item, mTable.AllFields);
        }
        private StringBuilder GetMetadataToSerialize(DbMappedTable mTable)
        {
            var sb = new StringBuilder();
            foreach (var col in mTable.AllFields)
            {
                sb.Append(col.PropertyInfo.PropertyType.ToSmartString(true));
                var t = TryGetNullableType(col.PropertyInfo.PropertyType) ?? col.PropertyInfo.PropertyType;
                if (t.IsEnum)
                    sb.Append(" as " + Enum.GetUnderlyingType(t).ToSmartString(true));
                sb.Append(";");
            }
            return sb;
        }

        private IEnumerable<byte[]> SerializeItem(IAutoMappedDbObject item, IReadOnlyList<DbMappedField> cols)
        {
            byte[] oneByte = new byte[1];
            foreach (var col in cols)
            {
                var t = col.PropertyInfo.PropertyType;
                var obj = col.GetValue(item);
                var nt = TryGetNullableType(t);
                if (nt != null)
                {
                    oneByte[0] = obj == null ? (byte)1 : (byte)0;
                    yield return oneByte;
                    if (obj == null)
                        continue;
                    t = nt;
                }
                foreach (var bs in SerializeValue(obj, t, oneByte))
                    yield return bs;
            }

        }
        IEnumerable<byte[]> SerializeValue(object obj, Type t, byte[] oneByte)
        {
            byte[] bs;
            byte[] bs2;
            if (t == typeof(byte))
            {
                oneByte[0] = (byte)obj;
                yield return oneByte;
            }
            else if (t == typeof(bool))
            {
                oneByte[0] = (bool)obj ? (byte)255 : (byte)0;
                yield return oneByte;
            }
            else if (t == typeof(sbyte))
            {
                oneByte[0] = (byte)(sbyte)obj;
                yield return oneByte;
            }
            else if (t == typeof(decimal))
            {
                var ints = decimal.GetBits((decimal)obj);
                Debug.Assert(ints.Length == 4);
                bs = BitConverter.GetBytes(ints[0]);
                yield return bs;
                bs = BitConverter.GetBytes(ints[1]);
                yield return bs;
                bs = BitConverter.GetBytes(ints[2]);
                yield return bs;
                bs = BitConverter.GetBytes(ints[3]);
                yield return bs;
            }
            else if (t == typeof(short))
            {
                bs = BitConverter.GetBytes((short)obj);
                yield return bs;
            }
            else if (t == typeof(ushort))
            {
                bs = BitConverter.GetBytes((ushort)obj);
                yield return bs;
            }
            else if (t == typeof(int))
            {
                bs = BitConverter.GetBytes((int)obj);
                yield return bs;
            }
            else if (t == typeof(long))
            {
                bs = BitConverter.GetBytes((long)obj);
                yield return bs;
            }
            else if (t == typeof(uint))
            {
                bs = BitConverter.GetBytes((uint)obj);
                yield return bs;
            }
            else if (t == typeof(double))
            {
                bs = BitConverter.GetBytes((double)obj);
                yield return bs;
            }
            else if (t == typeof(float))
            {
                bs = BitConverter.GetBytes((float)obj);
                yield return bs;
            }
            else if (t == typeof(TimeSpan))
            {
                bs = BitConverter.GetBytes(((TimeSpan)obj).Ticks);
                Debug.Assert(((TimeSpan)obj).Ticks == BitConverter.ToInt64(bs, 0));
                yield return bs;
            }
            else if (t == typeof(DateTime))
            {
                bs = BitConverter.GetBytes(((DateTime)obj).Ticks);
                Debug.Assert(((DateTime)obj).Ticks == BitConverter.ToInt64(bs, 0));
                yield return bs;
            }
            else if (t == typeof(string))
            {
                // The first byte is 255 if string is null
                // otherwise four bytes are used to store length
                if (obj == null)
                {
                    oneByte[0] = (byte)255; yield return oneByte;
                }
                else
                {
                    var str = (string)obj;
                    bs = Encoding.UTF8.GetBytes(str);
                    Debug.Assert(bs.Length <= unchecked((1 << 31) - 1 - (1 << 24)));
                    bs2 = BitConverter.GetBytes(bs.Length);
                    yield return bs2;
                    yield return bs;
                }
            }
            else if (t.IsEnum)
            {
                Type underlyingType = Enum.GetUnderlyingType(t);
                object value = Convert.ChangeType(obj, underlyingType);
                foreach (var bs3 in SerializeValue(value, underlyingType, oneByte))
                    yield return bs3;
            }
            else
            {
                throw new TechnicalException($"Type \"{t.FullName}\" + not handled (TODO)!", null);
            }
        }




        public IEnumerable<TItem> DeserializeFromFile<TItem>(string filename, Action<string> setOtherData)
            where TItem : IAutoMappedDbObject
        {
            using (FileStream fIn = File.OpenRead(filename))
            {
                var otherData = (string)DeserializeValue(fIn, typeof(string));
                setOtherData(otherData);
                fIn.ReadByte(); // Skip '\n'
                foreach (var item in DeserializeList<TItem>(fIn))
                    yield return item;
            }
        }

        public IEnumerable<TItem> DeserializeList<TItem>(FileStream fIn)
             where TItem : IAutoMappedDbObject
        {
            var mTable = GetTableMapping(typeof(TItem));
            var supposedMetadata = GetMetadataToSerialize(mTable).ToString();
            var actualMetadata = (string)DeserializeValue(fIn, typeof(string));
            if (supposedMetadata != actualMetadata)
                throw new FormatOutOfDateException(typeof(TItem), actualMetadata, supposedMetadata);
            while (fIn.ReadByte() != -1) // Skip '\n'
            {
                TItem item = DefaultObjectFactory<TItem>.Constructor();
                DeserializeItem(fIn, item, mTable.AllFields);
                yield return item;
            }
        }
        public TItem DeserializeItem<TItem>(FileStream fIn)
            where TItem : IAutoMappedDbObject, new()
        {
            var mTable = GetTableMapping(typeof(TItem));
            var item = new TItem();
            DeserializeItem(fIn, item, mTable.AllFields);
            return item;
        }
        private void DeserializeItem(FileStream fIn, IAutoMappedDbObject item, List<DbMappedField> cols)
        {
            int b;
            foreach (var col in cols)
            {
                var t = col.PropertyInfo.PropertyType;
                var nt = TryGetNullableType(t);
                if (nt != null)
                {
                    b = fIn.ReadByte();
                    Debug.Assert(b >= 0, "End of File not expected !");
                    if (b == 1)
                    {
                        col.SetValue(item, (object)null);
                        continue;
                    }
                    t = nt;
                }
                var obj = DeserializeValue(fIn, t);
                col.SetValue(item, obj);
            }
        }
        // TODO : (to improve perf, use unsafe code : https://stackoverflow.com/a/44626921)
        object DeserializeValue(FileStream fIn, Type t)
        {
            int b;
            if (t == typeof(byte))
            {
                b = fIn.ReadByte();
                Debug.Assert(b >= 0, "End of File not expected !");
                return (byte)b;
            }
            else if (t == typeof(bool))
            {
                b = fIn.ReadByte();
                return b == 255;
            }
            else if (t == typeof(sbyte))
            {
                b = fIn.ReadByte();
                return (sbyte)b;
            }
            else if (t == typeof(decimal))
            {
                var bs = new byte[sizeof(int) * 4];
                fIn.Read(bs, 0, bs.Length);
                var i1 = BitConverter.ToInt32(bs, 0);
                var i2 = BitConverter.ToInt32(bs, 4);
                var i3 = BitConverter.ToInt32(bs, 8);
                var i4 = BitConverter.ToInt32(bs, 12);
                return new decimal(new int[] { i1, i2, i3, i4 });
            }
            else if (t == typeof(short))
            {
                var bs = new byte[2];
                fIn.Read(bs, 0, 2);
                return BitConverter.ToInt16(bs, 0);
            }
            else if (t == typeof(ushort))
            {
                var bs = new byte[2];
                fIn.Read(bs, 0, 2);
                return BitConverter.ToUInt16(bs, 0);
            }
            else if (t == typeof(int))
            {
                var bs = new byte[4];
                fIn.Read(bs, 0, 4);
                return BitConverter.ToInt32(bs, 0);
            }
            else if (t == typeof(long))
            {
                var bs = new byte[8];
                fIn.Read(bs, 0, 8);
                return BitConverter.ToInt64(bs, 0);
            }
            else if (t == typeof(uint))
            {
                var bs = new byte[4];
                fIn.Read(bs, 0, 4);
                return BitConverter.ToUInt32(bs, 0);
            }
            else if (t == typeof(double))
            {
                var bs = new byte[8];
                fIn.Read(bs, 0, 8);
                return BitConverter.ToDouble(bs, 0);
            }
            else if (t == typeof(float))
            {
                var bs = new byte[4];
                fIn.Read(bs, 0, 4);
                return BitConverter.ToSingle(bs, 0);
            }
            else if (t == typeof(TimeSpan))
            {
                var bs = new byte[8];
                fIn.Read(bs, 0, 8);
                return TimeSpan.FromTicks(BitConverter.ToInt64(bs, 0));
            }
            else if (t == typeof(DateTime))
            {
                var bs = new byte[8];
                fIn.Read(bs, 0, 8);
                return new DateTime(BitConverter.ToInt64(bs, 0));
            }
            else if (t == typeof(string))
            {
                b = fIn.ReadByte();
                if (b == 255)
                    return null;
                var bs = new byte[4];
                bs[0] = (byte)b;
                fIn.Read(bs, 1, 3);
                var length = BitConverter.ToInt32(bs, 0);
                bs = new byte[length];
                fIn.Read(bs, 0, length);
                return Encoding.UTF8.GetString(bs);
            }
            else if (t.IsEnum)
            {
                Type underlyingType = Enum.GetUnderlyingType(t);
                var value = DeserializeValue(fIn, underlyingType);
                return value; // because current method return object, we don't have to do anything special
            }
            else
            {
                throw new TechnicalException($"Type \"{t.FullName}\" + not handled (TODO)!", null);
            }
        }


        public class FormatOutOfDateException : TechnicalException
        {
            public FormatOutOfDateException(Type type, string actualMetadata, string supposedMetadata)
                : base(null, null)
            {
                ItemType = type;
                ActualMetadata = actualMetadata;
                SupposedMetadata = supposedMetadata;
            }

            public string ActualMetadata { get; private set; }
            public string SupposedMetadata { get; private set; }
            public Type ItemType { get; private set; }

            public override string Message
            {
                get
                {
                    return ItemType.FullName + " instances saved in the stream are in a old format:" + Environment.NewLine
                          + ActualMetadata + Environment.NewLine
                          + "instead of " + Environment.NewLine
                          + SupposedMetadata;
                }
            }

        }

        /// <summary> Retourne le type sous jacent d'un type nullable, ou null si le type n'est pas nullable </summary>
        static Type TryGetNullableType(Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                return type.GetGenericArguments()[0];
            return null;
        }


        #endregion
    }
}
