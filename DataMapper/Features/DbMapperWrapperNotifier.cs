﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using TechnicalTools.Model.Cache;


namespace DataMapper
{
    // See below this class to see how it is integrated with other prexisting classes
    public class DbMapperWrapperNotifier : DbMapperWrapper
    {
        public DbMapperWrapperNotifier(IDbMapper mapper)
            : base(mapper)
        {
        }

        public override void CreateInDatabase<TObject>(TObject item, bool with_transaction = true)
        {
            WrappedMapper.CreateInDatabase(item, with_transaction);
            if (item is IHasTypedIdReadable hastypedId)
            {
                OnDataChanged(this, new DataChangedEventArgs(eDataChangedType.Created, hastypedId.TypedId.Type, new[] { hastypedId.Id }));
            }
        }

        public override int UpdateToDatabase<TObject>(TObject item, bool with_transaction = true, string sqlCondition = null, IReadOnlyCollection<DbMappedField> fieldsToUpdate = null)
        {
            var res = WrappedMapper.UpdateToDatabase(item, with_transaction, sqlCondition, fieldsToUpdate);
            if (item is IHasTypedIdReadable hastypedId)
            {
                OnDataChanged(this, new DataChangedEventArgs(eDataChangedType.Updated, hastypedId.TypedId.Type, new[] { hastypedId.Id }));
            }

            return res;
        }

        public override void DeleteInDatabase<TObject>(TObject item, bool with_transaction = true)
        {
            WrappedMapper.DeleteInDatabase(item, with_transaction);
            if (item is IHasTypedIdReadable hastypedId)
            {
                OnDataChanged(this, new DataChangedEventArgs(eDataChangedType.Deleted, hastypedId.TypedId.Type, new[] { hastypedId.Id }));
            }
        }

        public override List<TObject> LoadCollectionExclusive<TObject, TPropertyValue>(Expression<Func<TObject, TPropertyValue>> getAtomicProperty, TPropertyValue valueToTest, TPropertyValue valueToSet, string otherFilter = null)
        {
            var res = WrappedMapper.LoadCollectionExclusive(getAtomicProperty, valueToTest, valueToSet, otherFilter);
            if (res.FirstOrDefault() is IHasTypedIdReadable hastypedId)
            {
                OnDataChanged(this, new DataChangedEventArgs(eDataChangedType.Created, hastypedId.TypedId.Type, res.Cast<IHasTypedIdReadable>().Select(item => item.Id)));
            }

            return res;
        }

        public override void CreateInDatabaseCollectionSlow<TObject>(IReadOnlyCollection<TObject> items, bool with_transaction = true)
        {
            WrappedMapper.CreateInDatabaseCollectionSlow(items, with_transaction);
            if (items.FirstOrDefault() is IHasTypedIdReadable hastypedId)
            {
                OnDataChanged(this, new DataChangedEventArgs(eDataChangedType.Created, hastypedId.TypedId.Type, items.Cast<IHasTypedIdReadable>().Select(item => item.Id)));
            }
        }

        public override void CreateInDatabaseCollection<TObject>(IReadOnlyCollection<TObject> items, bool with_transaction = true)
        {
            WrappedMapper.CreateInDatabaseCollection(items, with_transaction);
            if (items.FirstOrDefault() is IHasTypedIdReadable hastypedId)
            {
                OnDataChanged(this, new DataChangedEventArgs(eDataChangedType.Created, hastypedId.TypedId.Type, items.Cast<IHasTypedIdReadable>().Select(item => item.Id)));
            }
        }

        public override void Truncate<TObject>()
        {
            WrappedMapper.Truncate<TObject>();
            if (typeof(IHasTypedIdReadable).IsAssignableFrom(typeof(TObject)))
            {
                OnDataChanged(this, new DataChangedEventArgs(eDataChangedType.Truncate, typeof(TObject), null));
            }
        }
    }

    #region Integration in other class

    public partial interface IDbMapperReadAccess
    {
        event DataChangedEventHandler DataChanged;
    }
    public delegate void DataChangedEventHandler(IDbMapper mapper, DataChangedEventArgs e);

    internal partial class DbMapperConcrete
    {
#pragma warning disable CS0414
        event DataChangedEventHandler IDbMapperReadAccess.DataChanged { add { } remove { } }
#pragma warning restore CS0414
    }
    public partial class DbMapperWrapperAbstract
    {
        public abstract event DataChangedEventHandler DataChanged;
    }

    public partial class DbMapperWrapper
    {
        void DataChanged_Init()
        {
            WrappedMapper.DataChanged += _OnDataChanged;
        }
        public override event DataChangedEventHandler DataChanged;
        void _OnDataChanged(IDbMapper mapper, DataChangedEventArgs e)
        {
            OnDataChanged(this, e);
        }
        protected virtual void OnDataChanged(IDbMapper mapper, DataChangedEventArgs e)
        {
            DataChanged?.Invoke(mapper, e);
        }
    }

    public enum eDataChangedType
    {
        Created,
        Updated,
        Deleted,
        Truncate,
    }
    public class DataChangedEventArgs : EventArgs
    {
        public eDataChangedType ChangeType { get; }
        public Type EntityType { get; }
        public IIdTuple[] EntityIds { get; }

        public DataChangedEventArgs(eDataChangedType changeType, Type entityType, IEnumerable<IIdTuple> entityIds)
        {
            ChangeType = changeType;
            EntityType = entityType;
            EntityIds = entityIds == null || (entityIds as IReadOnlyList<IIdTuple>)?.Count == 0 ? _emptyArray : entityIds.ToArray();
        }
        static readonly IIdTuple[] _emptyArray = new IIdTuple[0];
    }
    #endregion
}
