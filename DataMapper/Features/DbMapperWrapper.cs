﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using DataMapper.Linq2Sql;


namespace DataMapper
{
    public partial class DbMapperWrapper : DbMapperWrapperAbstract
    {
        public override bool             AsDisconnected { get { return WrappedMapper.AsDisconnected; } }
        public override SqlQueryProvider Provider       { get { return WrappedMapper.Provider; } }

        public DbMapperWrapper(IDbMapper mapper) : base(mapper)
        {
            DataChanged_Init();
        }
        public virtual void CheckEnumsAreLoaded()
        {

        }

        #region DbMapper

        public override IIsNamed Owner { get { return WrappedMapper.Owner; } }

        public override string GetConnectionString() { return WrappedMapper.GetConnectionString(); }

        public override TObject CreateInstanceOf<TObject>()
        { return WrappedMapper.CreateInstanceOf<TObject>(); }

        public override DbMappedTable GetTableMapping(Type type, bool buildIfNeeded = true) { return WrappedMapper.GetTableMapping(type, buildIfNeeded); }
        public override List<DbMappedTable> GetTableMappings() { return WrappedMapper.GetTableMappings(); }

        public override string GetTableNameFor<TObject>()
        { return WrappedMapper.GetTableNameFor<TObject>(); }
        [DebuggerHidden][DebuggerStepThrough]
        public override string GetColumnNameFor<TObject>(string propertyName)
        { return WrappedMapper.GetColumnNameFor<TObject>(propertyName); }
        [DebuggerHidden][DebuggerStepThrough]
        public override string GetColumnNameFor<TObject, TProperty>(Expression<Func<TObject, TProperty>> getProperty)
        { return WrappedMapper.GetColumnNameFor(getProperty); }

        [Obsolete("Plus bon du tout!")]
        public override IIdTuple GetSqlIds<TObject>(TObject obj)
        { return WrappedMapper.GetSqlIds<TObject>(obj); }
        public override void SetSqlIds<TObject>(TObject obj, IIdTuple ids)
        { WrappedMapper.SetSqlIds(obj, ids); }

        public override void ChangeTableMapping<TObject>(string newTableName, string newSchemaName = "dbo")
        { WrappedMapper.ChangeTableMapping<TObject>(newTableName, newSchemaName); }
        public override void ChangeTableMapping(Type type, string newTableName, string newSchemaName = "dbo")
        { WrappedMapper.ChangeTableMapping(type, newTableName, newSchemaName); }

        #endregion DbMapper

        #region DbMapper_Item

        public override DbMappedTable GetTableMapping(IAutoMappedDbObject obj)
        { return WrappedMapper.GetTableMapping(obj); }
        public override TObject TryGetObject<TObject>(long id)
        { CheckEnumsAreLoaded(); return WrappedMapper.TryGetObject<TObject>(id); }

        public override TObject TryGetObject<TObject>(int id)
        { CheckEnumsAreLoaded(); return WrappedMapper.TryGetObject<TObject>(id); }

        public override TObject TryGetObject<TObject>(IIdTuple id)
        { CheckEnumsAreLoaded(); return WrappedMapper.TryGetObject<TObject>(id); }

        public override TObject TryGetObject<TObject>(ITypedId id)
        { CheckEnumsAreLoaded(); return WrappedMapper.TryGetObject<TObject>(id); }

        public override List<TObject> GetObjectsWithIds<TObject>(IEnumerable<long> distinctIds)
        { CheckEnumsAreLoaded(); return WrappedMapper.GetObjectsWithIds<TObject>(distinctIds); }

        public override TObject GetObjectWithId<TObject>(long id)
        { CheckEnumsAreLoaded(); return WrappedMapper.GetObjectWithId<TObject>(id); }

        public override TObject GetObjectWithId<TObject>(int id)
        { CheckEnumsAreLoaded(); return WrappedMapper.GetObjectWithId<TObject>(id); }

        public override TObject GetObjectWithId<TObject>(byte id)
        { CheckEnumsAreLoaded(); return WrappedMapper.GetObjectWithId<TObject>(id); }

        public override TObject GetObjectWithId<TObject>(DateTime dt)
        { CheckEnumsAreLoaded(); return WrappedMapper.GetObjectWithId<TObject>(dt); }

        public override void CreateInDatabase<TObject>(TObject item, bool with_transaction = true)
        { WrappedMapper.CreateInDatabase(item, with_transaction); }

        public override string GetSqlFor_CreateInDatabase<TObject>(DbMappedTable mtable, IReadOnlyCollection<TObject> items)
        { return WrappedMapper.GetSqlFor_CreateInDatabase(mtable, items); }


        public override int UpdateToDatabase<TObject>(TObject item, bool with_transaction = true, string sqlCondition = null, IReadOnlyCollection<DbMappedField> fieldsToUpdate = null)
        { CheckEnumsAreLoaded(); return WrappedMapper.UpdateToDatabase(item, with_transaction, sqlCondition, fieldsToUpdate); }
        public override int UpdateToDatabase<TObject>(TObject item, params Expression<Func<TObject, object>>[] propertiesToUpdate)
        { CheckEnumsAreLoaded(); return WrappedMapper.UpdateToDatabase(item, propertiesToUpdate); }
        public override int UpdateToDatabase<TObject>(TObject item, string sqlCondition, params Expression<Func<TObject, object>>[] propertiesToUpdate)
        { CheckEnumsAreLoaded(); return WrappedMapper.UpdateToDatabase(item, sqlCondition, propertiesToUpdate); }
        public override int UpdateToDatabase<TObject>(TObject item, string sqlCondition, IReadOnlyCollection<PropertyInfo> propertiesToUpdate)
        { CheckEnumsAreLoaded(); return WrappedMapper.UpdateToDatabase(item, sqlCondition, propertiesToUpdate); }
        public override int UpdateToDatabase<TObject>(TObject item, string sqlCondition, IReadOnlyCollection<DbMappedField> fieldsToUpdate)
        { CheckEnumsAreLoaded(); return WrappedMapper.UpdateToDatabase(item, sqlCondition, fieldsToUpdate); }

        public override void DeleteInDatabase<TObject>(TObject item, bool with_transaction = true)
        { CheckEnumsAreLoaded(); WrappedMapper.DeleteInDatabase(item, with_transaction); }

        // Helper pour copier tous les champs mappés d'un objet vers un autre de même type
        public override void CopyAllFieldsFrom<TObject>(TObject target, TObject source, IReadOnlyCollection<DbMappedField> sqlFields = null)
        { WrappedMapper.CopyAllFieldsFrom(target, source, sqlFields); }

        public override List<PropertyInfo> GetPropertiesComposingKey(Type type)
        { return WrappedMapper.GetPropertiesComposingKey(type); }

        #endregion DbMapper_Item

        #region DbMapper_List

        public override RemoteContext CreateRemoteContext()
        { return WrappedMapper.CreateRemoteContext(); }

        public override List<TChildObject> LoadChildCollection<TParentObject, TChildObject, TFkProperty>(IEnumerable<TParentObject> distinctParentObjects, Expression<Func<TChildObject, TFkProperty>> uniqueMatchingKey)
        { CheckEnumsAreLoaded(); return WrappedMapper.LoadChildCollection(distinctParentObjects, uniqueMatchingKey); }
        public override IEnumerable<TObject> EnumerateCollectionMatching<TObject, TProperty>(IEnumerable<TProperty> distinctValues, Expression<Func<TObject, TProperty>> uniqueMatchingKey, string whereCondition = null)
        { CheckEnumsAreLoaded(); return WrappedMapper.EnumerateCollectionMatching(distinctValues, uniqueMatchingKey, whereCondition); }
        public override IEnumerable<TObject> EnumerateCollectionMatching<TObject, TProperty>(IEnumerable<TProperty> distinctValues, DbMappedField uniqueMatchingKey, string whereCondition = null)
        { CheckEnumsAreLoaded(); return WrappedMapper.EnumerateCollectionMatching<TObject, TProperty>(distinctValues, uniqueMatchingKey, whereCondition); }

        public override List<TObject> LoadCollection<TObject>(string whereCondition = null, string join = null, string joinCondition = null)
        { CheckEnumsAreLoaded(); return WrappedMapper.LoadCollection<TObject>(whereCondition, join, joinCondition); }

        public override List<TObject> LoadCollection<TObject>(Func<TObject, bool> filter, string whereCondition)
        { CheckEnumsAreLoaded(); return WrappedMapper.LoadCollection(filter, whereCondition); }

        public override int Count<TObject>(string whereCondition, string join = null)
        { CheckEnumsAreLoaded(); return WrappedMapper.Count<TObject>(whereCondition, join); }

        public override IEnumerable<TObject> EnumerateCollectionFromInnerJoinSql<TObject>(string whereCondition, string innerJoin, string joinCondition, RemoteContext context = null)
        { CheckEnumsAreLoaded(); return WrappedMapper.EnumerateCollectionFromInnerJoinSql<TObject>(whereCondition, innerJoin, joinCondition, context); }

        public override IEnumerable<TObject> EnumerateCollectionFromOuterJoinSql<TObject>(string whereCondition, string outerJoin, string joinCondition, RemoteContext context = null)
        { CheckEnumsAreLoaded(); return WrappedMapper.EnumerateCollectionFromOuterJoinSql<TObject>(whereCondition, outerJoin, joinCondition, context); }


        public override IEnumerable<TObject> EnumerateCollectionFromSqlQuery<TObject>(string sqlQuery, RemoteContext context = null)
        { CheckEnumsAreLoaded(); return WrappedMapper.EnumerateCollectionFromSqlQuery<TObject>(sqlQuery, context); }

        public override IEnumerable<TObject> EnumerateConvertedDataRow<TObject>(IEnumerable<DataRow> dataRows, DbMappedTable mtable = null)
        { CheckEnumsAreLoaded(); return WrappedMapper.EnumerateConvertedDataRow<TObject>(dataRows, mtable); }



        public override List<TObject> LoadCollectionExclusive<TObject, TPropertyValue>(Expression<Func<TObject, TPropertyValue>> getAtomicProperty,
                                                                              TPropertyValue valueToTest,
                                                                              TPropertyValue valueToSet,
                                                                              string otherFilter = null)
        { CheckEnumsAreLoaded(); return WrappedMapper.LoadCollectionExclusive(getAtomicProperty, valueToTest, valueToSet, otherFilter); }





        public override void CreateInDatabaseCollectionSlow<TObject>(IReadOnlyCollection<TObject> items, bool with_transaction)
        { CheckEnumsAreLoaded(); WrappedMapper.CreateInDatabaseCollectionSlow(items, with_transaction); }


        public override void CreateInDatabaseCollection<TObject>(IReadOnlyCollection<TObject> items, bool with_transaction)
        { CheckEnumsAreLoaded(); WrappedMapper.CreateInDatabaseCollection(items, with_transaction); }
        public override void CreateInDatabaseCollection(IReadOnlyCollection<IAutoMappedDbObject> items, bool with_transaction = true)
        { CheckEnumsAreLoaded(); WrappedMapper.CreateInDatabaseCollection(items, with_transaction); }

        public override int UpdateToDatabaseCollection<TObject>(IReadOnlyCollection<TObject> items, params Expression<Func<TObject, object>>[] propertiesToUpdate)
        { CheckEnumsAreLoaded(); return WrappedMapper.UpdateToDatabaseCollection(items, propertiesToUpdate); }

        public override void DeleteInDatabaseCollection<TObject, TKey>(IReadOnlyCollection<TObject> items, bool with_transaction)
        { CheckEnumsAreLoaded(); WrappedMapper.DeleteInDatabaseCollection<TObject, TKey>(items, true); }

        public override void Truncate<TObject>()
        { WrappedMapper.Truncate<TObject>(); }

        #endregion DbMapper_List

        #region DbMapper_StdFeatures

        public override DataTable GetDataTable(string query, RemoteContext context = null)
        { return WrappedMapper.GetDataTable(query, context); }

        public override decimal? GetDecimalScalar(string query)
        { return WrappedMapper.GetDecimalScalar(query); }

        public override IEnumerable<TObject> ReadAndEnumerate<TObject>(string query, Func<DbDataReader, TObject> treat)
        { return WrappedMapper.ReadAndEnumerate(query, treat); }

        public override IEnumerable<DbMappedField> EnumerateEditedFields<TObject>(TObject x, TObject y)
        { return WrappedMapper.EnumerateEditedFields(x, y); }

        public override void Attach<TObject>(TObject item)
        { WrappedMapper.Attach(item); }

        public override string DumpObject<T>(T obj, string indent = null, StringBuilder sb = null)
        { return WrappedMapper.DumpObject(obj, indent, sb); }

        #endregion

        #region Old (Legacy code that use "state")


        #region DbMapper_Item

        public override void AutoLoadItem<T>(T item, IIdTuple id)
        { WrappedMapper.AutoLoadItem(item, id); }
        public override void CancelChanges<T>(T item)
        { WrappedMapper.CancelChanges(item); }
        public override void SynchronizeToDatabase<TObject>(TObject item)
        { WrappedMapper.SynchronizeToDatabase(item); }

        #region Sucre syntaxique

        public override void Save<T>(T item)
        { WrappedMapper.Save(item); }
        public override void Delete<T>(T item)
        { WrappedMapper.Delete(item); }

        #endregion

        #endregion DbMapper_Item


        #region DbMapper_List

        public override List<TObject> AutoLoadCollection<TObject>(string whereCondition = null)
        { return WrappedMapper.AutoLoadCollection<TObject>(whereCondition); }
        [Obsolete("")]
        public override void AutoLoadCollection<TObject>(CompositeList<TObject> collection, string whereCondition = null)
        { WrappedMapper.AutoLoadCollection(collection, whereCondition); }

        public override void AutoLoadCollectionFromRows<TObject>(CompositeList<TObject> collection, IEnumerable<DataRow> rows)
        { WrappedMapper.AutoLoadCollectionFromRows(collection, rows); }

        public override void AutoSaveCollection<TObject>(CompositeList<TObject> collection)
        { WrappedMapper.AutoSaveCollection(collection); }

        public override void AutoSaveItems<TObject>(IEnumerable<TObject> collection)
        { WrappedMapper.AutoSaveItems(collection); }


        #endregion  DbMapper_List

        #endregion
    }
}