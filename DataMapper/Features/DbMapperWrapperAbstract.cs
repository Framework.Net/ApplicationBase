﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using DataMapper.Linq2Sql;


namespace DataMapper
{
    public abstract partial class DbMapperWrapperAbstract : IDbMapper
    {
        protected IDbMapper WrappedMapper { get; set; }

        public abstract SqlQueryProvider Provider { get; }

        protected DbMapperWrapperAbstract(IDbMapper mapper)
        {
            WrappedMapper = mapper;
        }

        #region DbMapper

        public abstract IIsNamed Owner          { get; }
        public abstract bool     AsDisconnected { get; }

        public abstract string GetConnectionString();

        public abstract TObject CreateInstanceOf<TObject>()
            where TObject : class, IAutoMappedDbObject;

        public abstract DbMappedTable GetTableMapping(Type type, bool buildIfNeeded = true);
        public abstract List<DbMappedTable> GetTableMappings();

        public abstract string GetTableNameFor<TObject>()
            where TObject : IAutoMappedDbObject;

        public abstract string GetColumnNameFor<TObject>(string propertyName)
            where TObject : IAutoMappedDbObject;

        public abstract string GetColumnNameFor<TObject, TProperty>(Expression<Func<TObject, TProperty>> getProperty)
            where TObject : IAutoMappedDbObject;

        [Obsolete("Plus bon du tout!")]
        public abstract IIdTuple GetSqlIds<TObject>(TObject obj)
                where TObject : IAutoMappedDbObject;

        public abstract void SetSqlIds<TObject>(TObject obj, IIdTuple ids)
                where TObject : IAutoMappedDbObject;

        public abstract void ChangeTableMapping<TObject>(string newTableName, string newSchemaName = "dbo")
            where TObject : IAutoMappedDbObject;
        public abstract void ChangeTableMapping(Type type, string newTableName, string newSchemaName = "dbo");

        #endregion DbMapper

        #region DbMapper_Item

        public abstract DbMappedTable GetTableMapping(IAutoMappedDbObject obj);
        public abstract TObject TryGetObject<TObject>(long id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        public abstract TObject TryGetObject<TObject>(int id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;


        public abstract TObject TryGetObject<TObject>(IIdTuple id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        public abstract TObject TryGetObject<TObject>(ITypedId id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        public abstract List<TObject> GetObjectsWithIds<TObject>(IEnumerable<long> distinctIds)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        public abstract TObject GetObjectWithId<TObject>(long id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        public abstract TObject GetObjectWithId<TObject>(int id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        public abstract TObject GetObjectWithId<TObject>(byte id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        public abstract TObject GetObjectWithId<TObject>(DateTime dt)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        public abstract void CreateInDatabase<TObject>(TObject item, bool with_transaction = true)
            where TObject : IAutoMappedDbObject;

        public abstract string GetSqlFor_CreateInDatabase<TObject>(DbMappedTable mtable, IReadOnlyCollection<TObject> items)
            where TObject : IAutoMappedDbObject;

        public abstract int UpdateToDatabase<TObject>(TObject item, bool with_transaction = true, string sqlCondition = null, IReadOnlyCollection<DbMappedField> fieldsToUpdate = null)
            where TObject : IAutoMappedDbObject;
        public abstract int UpdateToDatabase<TObject>(TObject item, string sqlCondition, params Expression<Func<TObject, object>>[] propertiesToUpdate)
            where TObject : IAutoMappedDbObject;
        public abstract int UpdateToDatabase<TObject>(TObject item, params Expression<Func<TObject, object>>[] propertiesToUpdate)
            where TObject : IAutoMappedDbObject;
        public abstract int UpdateToDatabase<TObject>(TObject item, string sqlCondition, IReadOnlyCollection<PropertyInfo> propertiesToUpdate)
            where TObject : IAutoMappedDbObject;
        public abstract int UpdateToDatabase<TObject>(TObject item, string sqlCondition, IReadOnlyCollection<DbMappedField> fieldsToUpdate)
            where TObject : IAutoMappedDbObject;

        public abstract void DeleteInDatabase<TObject>(TObject item, bool with_transaction = true)
            where TObject : IAutoMappedDbObject;

        // Helper pour copier tous les champs mappés d'un objet vers un autre de même type
        public abstract void CopyAllFieldsFrom<TObject>(TObject target, TObject source, IReadOnlyCollection<DbMappedField> sqlFields = null)
            where TObject : IAutoMappedDbObject;

        public abstract List<PropertyInfo> GetPropertiesComposingKey(Type type);

        #endregion DbMapper_Item

        #region DbMapper_List

        public abstract RemoteContext CreateRemoteContext();

        public abstract List<TChildObject> LoadChildCollection<TParentObject, TChildObject, TFkProperty>(IEnumerable<TParentObject> distinctParentObjects, Expression<Func<TChildObject, TFkProperty>> uniqueMatchingKey)
            where TParentObject : class, IAutoMappedDbObject, IHasTypedIdReadable
            where TChildObject : class, IAutoMappedDbObject, IHasTypedIdReadable
            where TFkProperty : IEquatable<TFkProperty>;
        public abstract IEnumerable<TObject> EnumerateCollectionMatching<TObject, TProperty>(IEnumerable<TProperty> distinctValues, Expression<Func<TObject, TProperty>> uniqueMatchingKey, string whereCondition = null)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
            where TProperty : IEquatable<TProperty>;
        public abstract IEnumerable<TObject> EnumerateCollectionMatching<TObject, TProperty>(IEnumerable<TProperty> distinctValues, DbMappedField uniqueMatchingKey, string whereCondition = null)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
            where TProperty : IEquatable<TProperty>;

        public abstract List<TObject> LoadCollection<TObject>(string whereCondition = null, string join = null, string joinCondition = null)
            where TObject : class, IAutoMappedDbObject;

        public abstract List<TObject> LoadCollection<TObject>(Func<TObject, bool> filter, string whereCondition)
            where TObject : class, IAutoMappedDbObject;

        public abstract int Count<TObject>(string whereCondition, string join = null)
            where TObject : class, IAutoMappedDbObject;

        public abstract IEnumerable<TObject> EnumerateCollectionFromInnerJoinSql<TObject>(string whereCondition, string innerJoin, string joinCondition, RemoteContext context = null)
            where TObject : class, IAutoMappedDbObject;

        public abstract IEnumerable<TObject> EnumerateCollectionFromOuterJoinSql<TObject>(string whereCondition, string outerJoin, string joinCondition, RemoteContext context = null)
            where TObject : class, IAutoMappedDbObject;

        public abstract IEnumerable<TObject> EnumerateCollectionFromSqlQuery<TObject>(string sqlQuery, RemoteContext context = null)
            where TObject : class, IAutoMappedDbObject;

        public abstract IEnumerable<TObject> EnumerateConvertedDataRow<TObject>(IEnumerable<DataRow> dataRows, DbMappedTable mtable = null)
            where TObject : class, IAutoMappedDbObject;

        public abstract List<TObject> LoadCollectionExclusive<TObject, TPropertyValue>(Expression<Func<TObject, TPropertyValue>> getAtomicProperty,
                                                                              TPropertyValue valueToTest,
                                                                              TPropertyValue valueToSet,
                                                                              string otherFilter = null)
            where TObject : class, IAutoMappedDbObject;

        public abstract void CreateInDatabaseCollectionSlow<TObject>(IReadOnlyCollection<TObject> items, bool with_transaction = true)
            where TObject : IAutoMappedDbObject;
        public abstract void CreateInDatabaseCollection<TObject>(IReadOnlyCollection<TObject> items, bool with_transaction = true)
            where TObject : IAutoMappedDbObject;
        public abstract void CreateInDatabaseCollection(IReadOnlyCollection<IAutoMappedDbObject> items, bool with_transaction = true);


        public abstract int UpdateToDatabaseCollection<TObject>(IReadOnlyCollection<TObject> items, params Expression<Func<TObject, object>>[] propertiesToUpdate)
            where TObject : IAutoMappedDbObject;

        public abstract void DeleteInDatabaseCollection<TObject, TKey>(IReadOnlyCollection<TObject> items, bool with_transaction = true)
            where TObject : BaseDTO<TKey>, IAutoMappedDbObject
            where TKey : struct, IEquatable<TKey>, IIdTupleWithOnlyOneKey;

        public abstract void Truncate<TObject>()
            where TObject : IAutoMappedDbObject;

        #endregion DbMapper_List

        #region DbMapper_StdFeatures

        public abstract DataTable GetDataTable(string query, RemoteContext context = null);

        public abstract decimal? GetDecimalScalar(string query);

        public abstract IEnumerable<TObject> ReadAndEnumerate<TObject>(string query, Func<DbDataReader, TObject> treat);

        public abstract IEnumerable<DbMappedField> EnumerateEditedFields<TObject>(TObject x, TObject y)
            where TObject : IAutoMappedDbObject;

        public abstract void Attach<TObject>(TObject item)
            where TObject : BaseDTO, IAutoMappedDbObject, IHasTypedIdReadable;

        public abstract string DumpObject<T>(T obj, string indent = null, StringBuilder sb = null)
            where T : class, IAutoMappedDbObject;

        #endregion

        #region Old (Legacy code that use "state")


        #region DbMapper_Item

        public abstract void AutoLoadItem<T>(T item, IIdTuple id)
            where T : IPropertyNotifierObject, IAutoMappedDbObject;
        public abstract void CancelChanges<T>(T item)
              where T : IPropertyNotifierObject, IAutoMappedDbObject, IHasClosedIdReadable;
        public abstract void SynchronizeToDatabase<TObject>(TObject item)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject;

        #region Sucre syntaxique

        public abstract void Save<T>(T item)
              where T : IPropertyNotifierObject, IAutoMappedDbObject;
        public abstract void Delete<T>(T item)
              where T : IPropertyNotifierObject, IAutoMappedDbObject;
        #endregion

        #endregion DbMapper_Item


        #region DbMapper_List

        public abstract List<TObject> AutoLoadCollection<TObject>(string whereCondition = null)
            where TObject : class, IAutoMappedDbObject, IPropertyNotifierObject, new();
        [Obsolete("")]
        public abstract void AutoLoadCollection<TObject>(CompositeList<TObject> collection, string whereCondition = null)
            where TObject : class, IAutoMappedDbObject, IPropertyNotifierObject, new();

        public abstract void AutoLoadCollectionFromRows<TObject>(CompositeList<TObject> collection, IEnumerable<DataRow> rows)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject, new();

        public abstract void AutoSaveCollection<TObject>(CompositeList<TObject> collection)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject, new();

        public abstract void AutoSaveItems<TObject>(IEnumerable<TObject> collection)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject, new();

        #endregion  DbMapper_List

        #endregion
    }
}
