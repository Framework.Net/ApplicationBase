﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using TechnicalTools.Analysis;


namespace DataMapper.Diagnostics
{
    public class RequestTrace
    {
        public IDbMapper Mapper            { get; private set; }
        public   string  MapperName        { get { return Mapper.Owner.Name; } }
        public   string  ConnectionString  { get { lock (_stringById) return _stringById[ConnectionId]; } }
        public int       ConnectionId      { get; set; }
        public int       ThreadId          { get; private set; }
        public   string  Request           { get; private set; }

        public DateTime  StartTimeUTC      { get; internal set; }
        public DateTime  EndTimeUTC        { get; internal set; }
        public Exception Exception         { get; internal set; }

        public TimeSpan  Duration          { get { return EndTimeUTC - StartTimeUTC; } }
        public double    DurationInSeconds { get { return Duration.TotalSeconds; } } // TODO : A Supprimer (ici en attente de migration vers Devexpress >= 16.1.6), present pour que les summary fonctionne sur timespan
        public bool      Success           { get { return Exception == null; } }

        public RequestTrace(IDbMapper dbMapper, string connectionString, string request)
        {
            Mapper = dbMapper;
            ThreadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
            Request = request;
            int connectionId;
            lock (_stringById)
                if (!_idByString.TryGetValue(connectionString, out connectionId))
                {
                    connectionId = _idByString.Count + 1;
                    _idByString.Add(connectionString, connectionId);
                    _stringById.Add(connectionId, connectionString);
                }
            ConnectionId = connectionId;
        }
        static readonly Dictionary<int, string> _stringById = new Dictionary<int, string>();
        static readonly Dictionary<string, int> _idByString = new Dictionary<string, int>();

        #region Recherche de pattern


        static readonly StringPatternFinder<RequestTrace> _patternFinder = new StringPatternFinder<RequestTrace>(rt => rt.Request);

        // Permet de regrouper les requetes sans prendre de valeur trop longue
        public int RequestPatternId { get { return _patternFinder.GetPattern(this).Id; } }
        //public int RequestPatternId { get { if (_RequestPatternId == 0) GetOrCreatePatternId(); return _RequestPatternId; } }
        //int _RequestPatternId;
        public string RequestPattern { get { return _patternFinder.GetPattern(this).Pattern; } }
        //public string RequestPattern
        //{
        //    get
        //    {
        //        if (_RequestPatternId == 0)
        //            GetOrCreatePatternId();
        //        return _stringPatternById[_RequestPatternId];
        //    }
        //}
        //void GetOrCreatePatternId()
        //{
        //    var pattern = ProcessPattern(Request);
        //    int patternId;
        //    lock (_stringPatternById)
        //        if (!_idByStringPattern.TryGetValue(pattern, out patternId))
        //        {
        //            patternId = _idByStringPattern.Count + 1;
        //            _idByStringPattern.Add(pattern, patternId);
        //            _stringPatternById.Add(patternId, pattern);
        //        }
        //    _RequestPatternId = patternId;
        //}
        //static string ProcessPattern(string request)
        //{
        //    string str = request;
        //    foreach (var pattern in patterns)
        //    {
        //        Match m = pattern.Match(str);
        //        if (m.Success)
        //        //while ((m = pattern.re.Match(str)).Success)
        //        {
        //            int g = m.Groups.Count - 1;
        //            foreach (var grp in m.Groups.Cast<Group>().Skip(1).Reverse())
        //            {
        //                int c = grp.Captures.Count;
        //                foreach (Capture capture in grp.Captures.Cast<Capture>().Reverse())
        //                {
        //                    string id = "¤";
        //                    id += string.Join("_", new[] { (m.Groups.Count == 2 ? null : g.ToString()),
        //                                                       grp.Captures.Count == 1 ? null : c.ToString() }.Where(s => s != null));
        //                    str = str.Remove(capture.Index) + id + str.Substring(capture.Index + capture.Length);
        //                    --c;
        //                }
        //                --g;
        //            }
        //        }
        //    }
        //    return str;
        //}

        //public static readonly Dictionary<int, string> _stringPatternById = new Dictionary<int, string>();
        //public static readonly Dictionary<string, int> _idByStringPattern = new Dictionary<string, int>();

        //static readonly RegexOptions reOpts = RegexOptions.Compiled | RegexOptions.ExplicitCapture;
        //static readonly List<Regex> patterns = new List<Regex>()
        //    {
        //        new Regex("((?<number>[0-9]+)[^0-9]*)+", reOpts)
        //    };
        #endregion Recherche

    }

}
