﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using DataMapper.Linq2Sql;


namespace DataMapper
{
    /// <summary>
    /// This is the read access of interface IDbMapped.
    /// Contains all methods to read data from database but not change it...
    /// Creation of DTO objects and altering their mapping is still possible though.
    /// </summary>
    public partial interface IDbMapperReadAccess
    {
        #region DbMapper

        IIsNamed Owner          { get; }
        bool     AsDisconnected { get; }

        string GetConnectionString();

        DbMappedTable GetTableMapping(Type type, bool buildIfNeeded = true);
        List<DbMappedTable> GetTableMappings();

        string GetTableNameFor<TObject>()
            where TObject : IAutoMappedDbObject;
        string GetColumnNameFor<TObject>(string propertyName)
            where TObject : IAutoMappedDbObject;
        string GetColumnNameFor<TObject, TProperty>(Expression<Func<TObject, TProperty>> getProperty)
            where TObject : IAutoMappedDbObject;

        [Obsolete("Plus bon du tout!")]
        IIdTuple GetSqlIds<TObject>(TObject obj)
            where TObject : IAutoMappedDbObject;

        void SetSqlIds<TObject>(TObject obj, IIdTuple ids)
            where TObject : IAutoMappedDbObject;

        void ChangeTableMapping<TObject>(string newTableName, string newSchemaName = "dbo")
            where TObject : IAutoMappedDbObject;
        void ChangeTableMapping(Type type, string newTableName, string newSchemaName = "dbo");

        #endregion DbMapper

        #region DbMapper_Item

        DbMappedTable GetTableMapping(IAutoMappedDbObject obj);
        TObject TryGetObject<TObject>(long id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        TObject CreateInstanceOf<TObject>()
            where TObject : class, IAutoMappedDbObject;

        TObject TryGetObject<TObject>(int id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        TObject TryGetObject<TObject>(IIdTuple id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        TObject TryGetObject<TObject>(ITypedId id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        List<TObject> GetObjectsWithIds<TObject>(IEnumerable<long> distinctIds)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        TObject GetObjectWithId<TObject>(long id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        TObject GetObjectWithId<TObject>(int id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        TObject GetObjectWithId<TObject>(byte id)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        TObject GetObjectWithId<TObject>(DateTime dt)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable;

        string GetSqlFor_CreateInDatabase<TObject>(DbMappedTable mtable, IReadOnlyCollection<TObject> items)
            where TObject : IAutoMappedDbObject;

        // Helper pour copier tous les champs mappés d'un objet vers un autre de même type
        void CopyAllFieldsFrom<TObject>(TObject target, TObject source, IReadOnlyCollection<DbMappedField> sqlFields = null)
            where TObject : IAutoMappedDbObject;

        List<PropertyInfo> GetPropertiesComposingKey(Type type);

        #endregion DbMapper_Item

        #region DbMapper_List

        RemoteContext CreateRemoteContext();

        /// <summary>
        /// Get all childs objects having object in distinctParentObjects as parent, using foreign key relationship expressed by uniqueMatchingKey.
        /// This method is especially useful when parents objects are numerous and their ids are not contiguous.
        /// <example>
        /// var authors = new List&lt;Author&gt; { ... } // works with thousands of random objects ...
        ///                 .Distinct(); // important otherwise you will have the same child object multiple times in list (same reference though)
        /// var allBooksForAllAuthors = LoadChildCollection&lt;Author, Book&gt;(authors, (Book b) => b.AuthorId);
        /// </example>
        /// </summary>
        /// <typeparam name="TParentObject">Type of parent object the object to load refer to</typeparam>
        /// <typeparam name="TChildObject">Type of object to load</typeparam>
        /// <typeparam name="TFkProperty">Type of primary key of parent object and foreign key property on child object</typeparam>
        /// <param name="distinctParentObjects">List of parent objects, roles of all object (ie: ids) must be unique.</param>
        /// <param name="uniqueMatchingKey">Expression that express the property holding the foregn key value</param>
        List<TChildObject> LoadChildCollection<TParentObject, TChildObject, TFkProperty>(IEnumerable<TParentObject> distinctParentObjects, Expression<Func<TChildObject, TFkProperty>> uniqueMatchingKey)
            where TParentObject : class, IAutoMappedDbObject, IHasTypedIdReadable
            where TChildObject : class, IAutoMappedDbObject, IHasTypedIdReadable
            where TFkProperty : IEquatable<TFkProperty>;
        IEnumerable<TObject> EnumerateCollectionMatching<TObject, TProperty>(IEnumerable<TProperty> distinctValues, Expression<Func<TObject, TProperty>> uniqueMatchingKey, string whereCondition = null)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
            where TProperty : IEquatable<TProperty>;
        IEnumerable<TObject> EnumerateCollectionMatching<TObject, TProperty>(IEnumerable<TProperty> distinctValues, DbMappedField uniqueMatchingKey, string whereCondition = null)
            where TObject : class, IAutoMappedDbObject, IHasTypedIdReadable
            where TProperty : IEquatable<TProperty>;

        List<TObject> LoadCollection<TObject>(string whereCondition = null, string join = null, string joinCondition = null)
            where TObject : class, IAutoMappedDbObject;

        List<TObject> LoadCollection<TObject>(Func<TObject, bool> filter, string whereCondition)
            where TObject : class, IAutoMappedDbObject;


        int Count<TObject>(string whereCondition, string join = null)
            where TObject : class, IAutoMappedDbObject;

        IEnumerable<TObject> EnumerateCollectionFromInnerJoinSql<TObject>(string whereCondition, string innerJoin, string joinCondition, RemoteContext context = null)
            where TObject : class, IAutoMappedDbObject;

        IEnumerable<TObject> EnumerateCollectionFromOuterJoinSql<TObject>(string whereCondition, string outerJoin, string joinCondition, RemoteContext context = null)
            where TObject : class, IAutoMappedDbObject;


        IEnumerable<TObject> EnumerateCollectionFromSqlQuery<TObject>(string sqlQuery, RemoteContext context = null)
            where TObject : class, IAutoMappedDbObject;

        IEnumerable<TObject> EnumerateConvertedDataRow<TObject>(IEnumerable<DataRow> dataRows, DbMappedTable mtable = null)
            where TObject : class, IAutoMappedDbObject;



        List<TObject> LoadCollectionExclusive<TObject, TPropertyValue>(Expression<Func<TObject, TPropertyValue>> getAtomicProperty,
                                                                              TPropertyValue valueToTest,
                                                                              TPropertyValue valueToSet,
                                                                              string otherFilter = null)
            where TObject : class, IAutoMappedDbObject;

        #endregion DbMapper_List

        #region DbMapper_StdFeatures

        DataTable GetDataTable(string query, RemoteContext context = null);

        decimal? GetDecimalScalar(string query);

        IEnumerable<TObject> ReadAndEnumerate<TObject>(string query, Func<DbDataReader, TObject> treat);

        IEnumerable<DbMappedField> EnumerateEditedFields<TObject>(TObject x, TObject y)
            where TObject : IAutoMappedDbObject;

        void Attach<TObject>(TObject item)
            where TObject : BaseDTO, IAutoMappedDbObject, IHasTypedIdReadable;

        string DumpObject<T>(T obj, string indent = null, StringBuilder sb = null)
            where T : class, IAutoMappedDbObject;

        #endregion

        #region Old (Legacy code that use "state")


        #region DbMapper_Item

        void AutoLoadItem<T>(T item, IIdTuple id)
            where T : IPropertyNotifierObject, IAutoMappedDbObject;
        void CancelChanges<T>(T item)
            where T : IPropertyNotifierObject, IAutoMappedDbObject, IHasClosedIdReadable;

        #endregion DbMapper_Item

        #region DbMapper_List

        List<TObject> AutoLoadCollection<TObject>(string whereCondition = null)
            where TObject : class, IAutoMappedDbObject, IPropertyNotifierObject, new();
        [Obsolete("")]
        void AutoLoadCollection<TObject>(CompositeList<TObject> collection, string whereCondition = null)
            where TObject : class, IAutoMappedDbObject, IPropertyNotifierObject, new();

        void AutoLoadCollectionFromRows<TObject>(CompositeList<TObject> collection, IEnumerable<DataRow> rows)
            where TObject : IAutoMappedDbObject, IPropertyNotifierObject, new();

        #endregion  DbMapper_List

        #endregion

        #region R&D

        SqlQueryProvider Provider { get; }

        #endregion R&D
    }
}