﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace DataMapper.Extensions.Data.SqlClient
{
    public static class SqlParameter_Extensions
    {
        // From http://stackoverflow.com/a/4146573 (adapted)
        public static String ParameterValueForSQL(this SqlParameter sp)
        {
            if (sp.Value == DBNull.Value || sp.Value == null)
                return "NULL";

            switch (sp.SqlDbType)
            {
                case SqlDbType.Char:
                case SqlDbType.NChar:
                case SqlDbType.NText:
                case SqlDbType.NVarChar:
                case SqlDbType.Text:
                case SqlDbType.Time:
                case SqlDbType.VarChar:
                case SqlDbType.Xml:
                case SqlDbType.Date:
                case SqlDbType.DateTime:
                case SqlDbType.DateTime2:
                case SqlDbType.DateTimeOffset:
                    return "'" + sp.Value.ToString().Replace("'", "''") + "'";

                case SqlDbType.Bit:
                    return ToBooleanOrDefault(sp.Value) ? "1" : "0";

                default:
                    return sp.Value.ToString().Replace("'", "''");
            }
        }

        static bool ToBooleanOrDefault(object value)
        {
            switch (value.ToString().ToLowerInvariant())
            {
                case "true":
                case "yes":
                case "ok":
                case "y":
                    return true;
                case "false":
                case "no":
                case "n":
                    return false;
                default:
                    return bool.Parse(value.ToString());
            }
        }
    }
}
