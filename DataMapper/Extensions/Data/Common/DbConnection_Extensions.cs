﻿using System;
using System.Data.Common;

using DataMapper.Tools;


namespace DataMapper
{
    public static class DbConnection_Extensions
    {
        public static void OpenOrWaitUntilOpen(this DbConnection con)
        {
            SqlRetry.Instance.OpenOrWaitUntilOpen(con);
        }
    }
}
