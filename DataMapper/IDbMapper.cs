﻿using System;


namespace DataMapper
{
    public partial interface IDbMapper : IDbMapperReadAccess, IDbMapperWriteAccess
    {
    }
}
