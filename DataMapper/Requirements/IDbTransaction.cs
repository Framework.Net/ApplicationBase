﻿using System;

namespace DataMapper
{
    public interface IDbTransaction : IDisposable
    {
        void Commit();
        void RollBack();
        event Action OnCommit;
        event Action OnRollback;
    }
}
