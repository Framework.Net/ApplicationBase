﻿using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

namespace DataMapper
{
	/// <summary> A mettre au dessus des classes dont vous voulez une gestion automatique du chargement </summary>
	public interface IAutoMappedDbObject : ICopyable<IAutoMappedDbObject>
    {
        // Rien à implémenter si ce n'est qu'il faut utiliser les attributs DbMapped* pour beneficier des fonctionalités automatiques
        // Aller voir IAutoMappedDbObject_Extensions
    }
}
