﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;

namespace DataMapper
{
    // TODO : Voir : https://github.com/ironyx/sharpmemorycache/blob/master/SharpMemoryCache/TrimmingMemoryCache.cs
    //         via : http://stackoverflow.com/a/22900208/1942901
    // Inspiré de ObjectTracker
    public partial class MemoryCacheManager //: IMessageFilter necessite une reference vers System.Windows.Forms
    {
        public MemoryCacheManager()
        {
        }

        /// <summary>
        /// Retourne la liste de tous les objets encore vivants.
        /// Note @Dev : GetAllAliveObjects retourne un tableau car cela force la copie de tous les éléments.
        ///             On evite de fournir un moyen a l'appelant de generer des effet de bord sur le lock (blocage) en fonction de la vitesse à laquelle il itere
        /// En fait ce probleme est résolue de la même maniere que quand le framework déclenche un event :
        /// les subscribers (handler) sont copiés dans une liste temporaire puis on itère dessus pour les executer.
        /// </summary>
        public List<object> GetObjectsInMemory()
        {
            return AllAliveObjectsEnumerator().ToList();
        }


        public List<TObject> GetObjectsInMemory<TObject>()
            where TObject : class, IHasTypedIdReadable
        {
            CacheListUI<TObject> lst = GetCounterByTypeOrCreate<TObject>();
            lock (lst)
                return lst.GetInstances();
        }

        /// <summary>
        /// ATTENTION ! Enumerateur privée seulement. Ne pas passer en public.
        /// La valeur enumerable renvoyée doit _TOUJOURS_ être completement parcouru avant qu'une methode public ne retourne.
        /// On peut donc appeler ToList ou ToArray avant de retourner.
        /// </summary>
        /// <returns>Un énumerateur privé qui ne doit jamais être retourné en dehors de la classe</returns>
        IEnumerable<object> AllAliveObjectsEnumerator()
        {
            lock (_typesMet)
                foreach (var lst in _typesMet.Values)
                    foreach (var obj in lst.GetInstances())
                        yield return obj;
        }

        #region Get an object by its ID

        //public TObject TryGet<TObject>(ITypedId key)
        //    where TObject : class, IHasTypedIdReadable
        //{
        //    var lst = GetCounterByTypeOrCreate(key.Type);

        //    TObject obj;
        //    lock (lst)
        //        obj = (TObject)lst.TryGetValue(key);

        //    //if (obj == null)
        //    //    Log(string.Format("Asking for {0} with id {1} ==> Found", typeof(TObject).Name, id));
        //    //else
        //    //    Log(string.Format("Asking for {0} with id {1} ==> Not Found, return null.", typeof(TObject).Name, id));
        //    return obj;
        //}
        public IHasTypedIdReadable TryGet<TObject>(IIdTuple closedId)
            where TObject : class, IHasTypedIdReadable
        {
            return (TObject)TryGet(closedId.ToTypedId(typeof(TObject)));
        }
        public IHasTypedIdReadable TryGet(ITypedId key)
        {
            var lst = GetCounterByTypeOrCreate(key.Type);

            IHasTypedIdReadable obj;
            lock (lst)
                obj = (IHasTypedIdReadable)lst.TryGetValue(key);

            //if (obj == null)
            //    Log(string.Format("Asking for {0} with id {1} ==> Found", typeof(TObject).Name, id));
            //else
            //    Log(string.Format("Asking for {0} with id {1} ==> Not Found, return null.", typeof(TObject).Name, id));
            return obj;
        }
        #endregion

        [DebuggerHidden, DebuggerStepThrough]
        public List<TObject> TryGetAll<TObject>(IEnumerable<IIdTuple> ids)
            where TObject : class, IHasTypedIdReadable
        {
            CacheListUI<TObject> lst = GetCounterByTypeOrCreate<TObject>();
            lock (lst)
                return ids.Select(id => (TObject)TryGet(id.ToTypedId(typeof(TObject))))
                          .Where(obj => obj != null)
                          .ToList();
        }

        // Renvoie obj ou bien l'objet qui existe deja avec la meme clef
        [DebuggerHidden, DebuggerStepThrough]
        public void Add(IHasTypedIdReadable obj)
        {
            var id = obj.TypedId;
            Debug.Assert(obj != null, "obj is not null");
            Debug.Assert(id.Type.IsInstanceOfType(obj), "obj must be the type of the key");
            Type type = id.Type;

            var lst = GetCounterByTypeOrCreate(type);
            lock (lst)
                lst.AddIfNotAlreadyInside(id, obj, false);
        }

        // Renvoie obj ou bien l'objet qui existe deja avec la meme clef
        [DebuggerHidden, DebuggerStepThrough]
        public IHasTypedIdReadable GetExistingOrAdd(IHasTypedIdReadable obj)
        {
            var id = obj.TypedId;
            Debug.Assert(obj != null, "obj is not null");
            Debug.Assert(id.Type.IsInstanceOfType(obj), "obj must be the type of the key");
            Type type = id.Type;

            var lst = GetCounterByTypeOrCreate(type);
            lock (lst)
                return (IHasTypedIdReadable)lst.AddIfNotAlreadyInside(id, obj, true) ?? obj;
        }

        [DebuggerHidden, DebuggerStepThrough]
        public void Remove(IHasTypedIdReadable obj)
        {
            var id = obj.TypedId;
            Debug.Assert(obj != null, "obj is not null");
            Debug.Assert(id.Type.IsInstanceOfType(obj), "obj must be the type of the key");
            Type type = id.Type;

            var lst = GetCounterByTypeOrCreate(type);
            lock (lst)
                lst.Remove(id);
        }


        public IEnumerable<ICacheListReadOnly> Counters
        {
            [DebuggerHidden, DebuggerStepThrough]
            get
            {
                lock (_typesMet)
                    return _typesMet.Values.ToList();
            }
        }

        #region Private

        ICacheList GetCounterByTypeOrCreate(Type type)
        {
            Debug.Assert(type.IsClass, "type of key must be a class");

            ICacheList cnt;
            lock (_typesMet)
            {
                if (!_typesMet.TryGetValue(type, out cnt))
                {
                    var gType = typeof(CacheListUI<>).MakeGenericType(new[] { type });
                    var cons = DefaultObjectFactory.ConstructorFor(gType);
                    cnt = (ICacheList)cons();
                    _typesMet.Add(type, cnt);
                }
            }
            return cnt;
        }

        CacheListUI<T> GetCounterByTypeOrCreate<T>()
            where T : class, IHasTypedIdReadable
        {
            Debug.Assert(!typeof(T).IsGenericType && !typeof(T).IsAbstract);
            var type = typeof(T);
            ICacheList cnt;
            lock (_typesMet)
            {
                if (!_typesMet.TryGetValue(type, out cnt))
                {
                    cnt = new CacheListUI<T>();
                    _typesMet.Add(type, cnt);
                }
            }
            return (CacheListUI<T>)cnt;
        }
        readonly Dictionary<Type, ICacheList> _typesMet = new Dictionary<Type, ICacheList>();

        public partial interface ICacheListReadOnly
        {
            Type Type  { get; }
            uint Count { get; }
            IList GetInstances();
        }

        public partial interface ICacheList : ICacheListReadOnly
        {
            object AddIfNotAlreadyInside(ITypedId key, object obj, bool canExist);
            object TryGetValue(ITypedId key);
            object Remove(ITypedId key);
        }
        #endregion Private

        partial class CacheListUI<T> : ICacheList
            where T : class, IHasTypedIdReadable
        {
            public Type Type
            {
                get { return typeof(T); }
            }

            public uint Count
            {
                get { return (uint)GetInstances().Count; }
            }

            public object TryGetValue(ITypedId key) { T obj; TryGetValue(key, out obj); return obj; }
            public bool TryGetValue(ITypedId key, out T obj)
            {
                Debug.Assert(key != null, "id != null");
                lock (this)
                {
                    WeakReference wr;
                    if (_instances.TryGetValue(key, out wr))
                    {
                        obj = (T)wr.Target;
                        if (obj == null)
                        {
                            //Log("Previous object with uid " + id.AsDebugString + " has been garbage collected (sooner) !");
                            _instances.Remove(key);
                            //DebugOnRemove(id);
                        }
                        return obj != null;
                    }
                }
                obj = null;
                return false;
            }

            [DebuggerHidden, DebuggerStepThrough]
            public object AddIfNotAlreadyInside(ITypedId key, object obj, bool canExist)
            {
                return Add(key, (T)obj, canExist);
            }
            /// <summary>
            /// return the object with same id stored in this class. The object return can be obj
            /// If no object exist with same id null is return and obj is now stored in the class.
            /// </summary>
            /// <param name="canExist">false to indicate we don't expect an object (different from obj) to already exist with same ID</param>
            [DebuggerHidden, DebuggerStepThrough]
            object Add(ITypedId key, T obj, bool canExist)
            {
                Debug.Assert(obj != null, "obj != null");
                lock (this)
                {
                    if (_instances.TryGetValue(key, out WeakReference wr))
                    {
                        object present = wr.Target;
                        if (present != null)
                        {
                            Debug.Assert(canExist || ReferenceEquals(present, obj),
                                         string.Format("Du code essaye de charger un objet {0} or un objet similaire existe déjà dans le cache !" + Environment.NewLine +
                                                       "Remontez la callstack et modifiez le code pour demander au cache cet objet plutôt que d'en charger un nouveau", key.AsDebugString));
                            return present;
                        }
                        _instances.Remove(key);
                    }
                    _instances.Add(key, new WeakReference(obj));
                }
                return null;
            }

            [DebuggerHidden, DebuggerStepThrough]
            public object Remove(ITypedId key)
            {
                lock (this)
                    if (_instances.TryGetValue(key, out WeakReference wr))
                    {
                        object present = wr.Target;
                        _instances.Remove(key);
                        return present;
                    }
                    else
                        return null;
            }

            //[DebuggerHidden, DebuggerStepThrough]
            //void IdOfManagedObjectHasChanged(object sender, PropertyHasChangedEventArgs e)
            //{
            //    var obj = (T)sender;
            //    lock (this)
            //    {
            //        var oldTypedId = e.PreviousValue is ITypedId ? (ITypedId)e.PreviousValue : obj.MakeTypedId(e.PreviousValue);
            //        Debug.Assert(_instances.ContainsKey(oldTypedId), "_instances.ContainsKey(oldTypedId)");
            //        Debug.Assert(!e.PreviousValue.Equals(obj.Id), "L'Id n'as pas reelement changé : Soit il est inutile que l'event soit declenché, soit l'id retourné n'est pas le nouvel ID :(");
            //        //Log(string.Format("Replacing {0} by {1}", oldTypedId.AsDebugString, obj.TypedId.AsDebugString));
            //        _instances.Remove(oldTypedId);
            //        //if (_cacheHolders.Count > 0)
            //        //    _instancesHeld.Remove(oldTypedId);
            //        //DebugOnRemove(oldTypedId);
            //        //obj.IdHasChanged -= IdOfManagedObjectHasChanged;
            //        if (obj.Id != null)
            //            Add(obj.TypedId, obj);
            //    }
            //}

            /// <summary>
            /// Retourne la liste de tous les objets encore vivants.
            /// Note @Dev : GetInstances retourne un tableau car cela force la copie de tous les éléments.
            ///             On evite de fournir un moyen a l'appelant de generer des effet de bord sur le lock (blocage) en fonction de la vitesse a laquelle il itere
            /// En fait ce probleme est résolue de la même maniere que quand le framework déclenche un event :
            /// les subscribers (handler) sont copiés dans une liste temporaire puis on itère dessus pour les executer.
            /// </summary>
            /// <returns></returns>
            public List<T> GetInstances()
            {
                lock (this)
                    return GetEnumerator().ToList();
            }
            IList ICacheListReadOnly.GetInstances() { return GetInstances(); }

            #region Private

            readonly Dictionary<ITypedId, WeakReference> _instances = new Dictionary<ITypedId, WeakReference>();


            public void Clean()
            {
                lock (this)
                {
                    foreach (var pair in _instances.Where(pair => !pair.Value.IsAlive).ToList())
                        _instances.Remove(pair.Key);
                }
                //DebugOnClean();
            }

            /// <summary>
            /// ATTENTION ! Enumerateur privée seulement. Ne pas passer en public.
            /// La valeur enumerable renvoyée doit _TOUJOURS_ être completement parcouru avant qu'une methode public ne retourne.
            /// On peut donc appeler ToList ou ToArray avant de retourner.
            /// </summary>
            /// <returns>Un énumerateur privé qui ne doit jamais être retourné en dehors de la classe</returns>
            IEnumerable<T> GetEnumerator()
            {
                // Debug.Assert(_lock must be locked !)
                return _instances.Values.Select(value => (T)value.Target).Where(obj => obj != null);
            }

            #endregion Private


        }

        #region InitAutoClean

        void InitAutoClean()
        {
            // TODO : http://www.codeproject.com/Articles/30345/Application-Idle
            //System.Windows.Forms.Application.Idle += ApplicationOnIdle;
            //System.Windows.Forms.Application.AddMessageFilter(this);
            //System.Windows.Forms.Application.Idle -= ApplicationOnIdle;
            //System.Windows.Forms.Application.RemoveMessageFilter(this);
        }
        //bool IMessageFilter.PreFilterMessage(ref Message m)
        //{
        //    //if (m.HWnd == ownerWindow && interestedMessages.ContainsKey(m.Msg))
        //    //{
        //    //    MessageReceived(m.Msg);
        //    //}
        //    return true;
        //}
        //void ApplicationOnIdle(object sender, EventArgs eventArgs)
        //{

        //}

        #endregion

    }
}
