using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;

using TechnicalTools;
using TechnicalTools.Diagnostics;

using DataMapper.Tools;


namespace DataMapper
{
    public class SqlFeaturesProvider : IConnectionProvider
    {
        public string ConnectionString { get; private set; }

        protected internal SqlFeaturesProvider(string connectionString)
        {
            ConnectionString = connectionString;
        }

        readonly RemoteContext NoContext = new RemoteContext();

        public DataTable GetDataTable(string sqlQuery, RemoteContext context = null)
        {
            DataTable dt = new DataTable();
            using (var con = SqlConnectionTracer.Create(ConnectionString))
            {
                con.OpenOrWaitUntilOpen();
                (context ?? NoContext).ExecuteInRemoteContext(con, () =>
                {
                    using (var cmd = new SqlCommand(sqlQuery, con))
                    using (var da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandTimeout = 180;

                        while (true)
                            try
                            {
                                var numberOfRowsAffected = SqlRetry.Instance.HandleRetry(cmd, true, () => { dt.Clear(); return da.Fill(dt); });
                                break;
                            }
                            catch (Exception ex) when (LastChanceCatch(ex)) // Cherche a catcher (pour ressayer) "Named Pipes Provider, error: 40 - Impossible d'ouvrir une connexion à SQL Server"
                            { // TODO : Revoir car quand il y a une errreur de SQL, on tombe ici
                                System.Threading.Thread.Sleep(DebugTools.IsForDevelopper ? 1000 : 5000);
                                continue;
                            }
                    }
                });
            }
            return dt;
        }
        bool LastChanceCatch(Exception ex)
        {
            var sqlex = ex as SqlException;
            Debug.WriteLine(DateTime.Now + ": Unexpected error on GetDataTable (HResult:" + ex.HResult + (sqlex == null ? "" : ", Number:" + sqlex.Number) + ", Message: " + ex.Message);
            if (ex.HResult == -2146232060 && (sqlex?.Number ?? 0).In(102,  // Erreur de syntaxe dans la requête
                                                                     208,  // Invalid object Name
                                                                     156,  // Incorrect syntax near the keyword 'Table'
                                                                     164,  // Each GROUP BY expression must contain at least one column that is not an outer reference.
                                                                     174,  // The dateadd function requires 3 argument(s)
                                                                     207,  // Invalid Column Name
                                                                     229,  // The SELECT permission was denied on the object
                                                                     242,  // The conversion of a varchar data type to a datetime data type resulted in an out-of-range value.
                                                                     334, // The target table 'dbo.te_cash_exchange_new' of the DML statement cannot have any enabled triggers if the statement contains an OUTPUT clause without INTO clause.
                                                                     4104,  // The multi-part identifier \"dbo.table_name\" could not be bound.
                                                                     4121, // Cannot find either column \"*\" or the user-defined function or aggregate \"***\", or the name is ambiguous.
                                                                     4145)) // An expression of non-boolean type specified in a context where a condition is expected, near ','
                return false;
            return true;
        }
        //public void GetReaderAndTreatIt(string sqlQuery, Action<DbDataReader> treat)
        //{
        //    SqlDataReader reader = null;
        //    using (var con = SqlConnectionTracer.Create(ConnectionString))
        //    using (var cmd = new SqlCommand(sqlQuery, con))
        //    {
        //        cmd.CommandTimeout = 60;
        //        con.OpenOrWaitUntilOpen();
        //        SqlRetry.HandleRetry(cmd, () => reader = cmd.ExecuteReader());
        //        try
        //        {
        //            treat(reader);
        //        }
        //        finally
        //        {
        //            reader.Dispose();
        //        }
        //    }
        //}
        public IEnumerable<DbDataReader> GetEnumerableReader(string sqlQuery, RemoteContext context = null)
        {
            context = context ?? NoContext;

            using (var con = SqlConnectionTracer.Create(ConnectionString))
            {
                con.OpenOrWaitUntilOpen();
                var enumerable = context.ExecuteInRemoteContext<DbDataReader>(con, () => ReadEnumerable(con, sqlQuery));
                foreach (var item in enumerable)
                    yield return item;
            }
        }
        IEnumerable<SqlDataReader> ReadEnumerable(SqlConnection con, string sqlQuery)
        {
            using (var cmd = new SqlCommand(sqlQuery, con))
            {
                cmd.CommandTimeout = 180;
                SqlDataReader reader = null;
                SqlRetry.Instance.HandleRetry(cmd, true, () => reader = cmd.ExecuteReader());
                try
                {
                    while (reader.Read())
                        yield return reader;
                }
                finally
                {
                    reader.Dispose();
                }
            }
        }

        public int ExecuteNonQuery(string sql)
        {
            using (var con = SqlConnectionTracer.Create(ConnectionString))
            using (var cmd = new SqlCommand(sql, con))
            {
                con.OpenOrWaitUntilOpen();
                var numberOfRowsAffected = SqlRetry.Instance.HandleRetry(cmd, false, () => cmd.ExecuteNonQuery());
                return numberOfRowsAffected;
            }
        }

        public T ExecuteScalar<T>(string sql, bool canRetry, T default_value = default(T))
            where T : struct
        {
            return (T)(ExecuteScalar(sql, canRetry, null) ?? default_value);
        }
        public object ExecuteScalar(string sql, bool canRetry, object default_value = null)
        {
            using (var con = SqlConnectionTracer.Create(ConnectionString))
            using (var cmd = new SqlCommand(sql, con))
            {
                con.OpenOrWaitUntilOpen();
                return SqlRetry.Instance.HandleRetry(cmd, canRetry, () => { return cmd.ExecuteScalar(); })
                    ?? default_value;
            }
        }

        public void WithConnectionDo(Action<SqlConnection, IDbTransaction> doWithConnection)
        {
            using (var con = SqlConnectionTracer.Create(ConnectionString))
            using (var tran = StartTransaction())
                doWithConnection(con, tran);
        }

        public IDbTransaction StartTransaction()
        {
            return null; // TODO
        }

        public void Dispose()
        {
            // nothing to do
        }
    }
}
