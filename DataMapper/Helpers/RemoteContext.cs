﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DataMapper.Tools;
using TechnicalTools;
using TechnicalTools.Model;

namespace DataMapper
{
    public class RemoteContext
    {
        abstract class TempTable
        {
            public DbMappedTable Table;
            public bool          Replayable;
            public abstract void BulkInsert(SqlConnection sqlConnection);

            /// <summary> Make Replayable = true (but it disable late binding) </summary>
            protected internal abstract void EvaluateSource();

            protected internal abstract void AsSqlCode(StringBuilder sb);
        }
        class TempTable<TObject> : TempTable
             where TObject : IAutoMappedDbObject
        {
            public IEnumerable<TObject> Source;

            public TempTable(RemoteContext owner)
            {
                _owner = owner;
            }
            readonly RemoteContext _owner;

            public override void BulkInsert(SqlConnection sqlConnection)
            {
                using (var bulkCopy = new SqlBulkCopy(sqlConnection,
                                                      SqlBulkCopyOptions.KeepNulls | // Objects should always match what's in database and default value for property should be in business layer anyway
                                                      SqlBulkCopyOptions.CheckConstraints | // if Constraints exist and the CHECK_CONSTRAINTS hint is not specified and connection does not have ALTER TABLE rights, we need this
                                                      SqlBulkCopyOptions.FireTriggers | // if Triggers exist and the FIRE_TRIGGER hint is not specified and connection does not have ALTER TABLE rights, we need this
                                                      SqlBulkCopyOptions.TableLock, // Increase performance and make
                                                      null))
                {
                    bulkCopy.DestinationTableName = Table.FullName;
                    bulkCopy.BulkCopyTimeout = 0; // This big timeout is ok because we upload in a temporary table that exists only for the connection/current user
                    bulkCopy.EnableStreaming = true; // Make bulkCopy avoiding to read all source before sending
                    foreach (var f in Table.AllFields)
                        if (Table.AutoIncrementedField != f)
                            bulkCopy.ColumnMappings.Add(f.ColumnName, f.ColumnName);
                    using (var dataReader = new MappedDbObjectDataReader<TObject>(Table, Source))
                    {
                        bulkCopy.WriteToServer(dataReader);
                    }
                }
            }

            /// <summary> Make Replayable = true (but it disable late binding) </summary>
            protected internal override void EvaluateSource()
            {
                var lst = Source.ToList();
                Source = lst;
                Replayable = true;
            }

            /// <summary> Return sql code that has same effect than <see cref="BulkInsert"/> </summary>
            protected internal override void AsSqlCode(StringBuilder sb)
            {
                if (!Replayable)
                    throw new TechnicalException($"Call {nameof(EvaluateSource)} before calling {nameof(AsSqlCode)}!", null);

                sb.AppendLine(ToCreateSqlTempTable(this).ToString());
                sb.AppendLine(_owner.TempMapper.GetSqlFor_CreateInDatabase(Table, (IReadOnlyCollection<TObject>)Source));
            }
        }
        readonly List<TempTable>        _tempTables = new List<TempTable>();
        bool _canBeReplayed = true;

        public RemoteContext()
        {
        }
        // Lazy Property to prevent infinite recursion,
        // Because IDbMapper is a top-hierarchy class that already depend on "this" on init :
        // IdbMapper build one its composite in constructor:  Linq2Sql.SqlQueryProvider
        // Linq2Sql.SqlQueryProvider need a connectionstring through SqlFeatureProvider
        // SqlFeatureProvider need a fake RemoteContext
        // So RemoteContext must not instanciate a IdbMapper
        IDbMapper TempMapper { get { return __tempMapper ?? (__tempMapper = DbMapperFactory.CreateSqlMapper(new NamedObject("TempMapper"), "", false)); } }
        IDbMapper __tempMapper;

        public RemoteContext With<T>(IEnumerable<T> dataSource, out string tableName)
            where T : IAutoMappedDbObject
        {
            var baseName = "#" + typeof(T).Name;
            int i = baseName.IndexOf('`');
            if (i >= 0)
                baseName = baseName.Remove(i);
            var name = baseName;
            i = 1;
            while (_tempTables.Any(tt => tt.Table.TableName == name))
            {
                ++i;
                name = baseName + i;
            }
            tableName = name;
            return With(dataSource, tableName);
        }
        public RemoteContext With<T>(IEnumerable<T> dataSource, string tableName)
            where T : IAutoMappedDbObject
        {
            if (!tableName.StartsWith("#"))
                throw new TechnicalException(nameof(tableName) + " must begin by '#'!", null);
            if (tableName.StartsWith("##"))
                throw new TechnicalException(nameof(tableName) + " must begin by only one '#'!", null);
            TempMapper.ChangeTableMapping(typeof(T), tableName, null);
            _tempTables.Add(new TempTable<T>(this)
            {
                Source = dataSource,
                Table = TempMapper.GetTableMapping(typeof(T)),
                Replayable = dataSource is IReadOnlyCollection<T> // false if IEnumerable only
            });
            return this;
        }

        public void ExecuteInRemoteContext(SqlConnection con, Action action)
        {
            try
            {
                RemotelyDeclareAndFillTempTables(con);
                action();
            }
            finally
            {
                RemotelyCleanTempTables(con);
            }
        }
        public IEnumerable<T> ExecuteInRemoteContext<T>(SqlConnection con, Func<IEnumerable<T>> func)
        {
            try
            {
                RemotelyDeclareAndFillTempTables(con);
                var enumerator = func();
                foreach (var item in enumerator)
                    yield return item;
            }
            finally
            {
                RemotelyCleanTempTables(con);
            }
        }

        void RemotelyDeclareAndFillTempTables(SqlConnection con)
        {
            if (!_canBeReplayed)
                throw new Exception("Cannot be executed again! Some datasource cannot be enumerated twice!");
            foreach (var tt in _tempTables)
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = ToCreateSqlTempTable(tt).ToString();
                    cmd.ExecuteNonQuery();
                }
            foreach (var tt in _tempTables)
                tt.BulkInsert(con);
        }
        void RemotelyCleanTempTables(SqlConnection con)
        {
            _canBeReplayed = _tempTables.All(tn => tn.Replayable);
            foreach (var tt in _tempTables)
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = ToSqlDropTable(tt, true).ToString();
                    cmd.ExecuteNonQuery();
                }
        }


        static StringBuilder ToCreateSqlTempTable(TempTable table)
        {
            var sb = new StringBuilder(ToSqlDropTable(table, true));
            sb.AppendLine($@"
CREATE TABLE {table.Table.TableName}
(");
            foreach (var p in table.Table.AllFields)
                sb.AppendLine(p.ColumnName + " " + ToSqlType(p.PropertyInfo.PropertyType, p.IsPK || p.ForeignKeys.Any()) + ",");
            sb.Length = sb.Length - Environment.NewLine.Length - ",".Length;
            sb.AppendLine(")");
            var indexedColumns = table.Table.AllFields.Where(c => c.IsPK);
            var indexedColumn = indexedColumns.Skip(1).Any() ? null : indexedColumns.FirstOrDefault();
            if (indexedColumn != null)
                sb.AppendLine($"CREATE CLUSTERED INDEX {table.Table.TableName}_{indexedColumn.ColumnName} ON {table.Table.TableNameProtected}({indexedColumn.ColumnNameProtected})");
            return sb;
        }
        static string ToSqlDropTable(TempTable table, bool safe = false)
        {
            return (safe ? $@"IF OBJECT_ID('tempdb..{table.Table.TableName}') IS NOT NULL" + Environment.NewLine + "    " : string.Empty)
                 + $@"DROP TABLE {table.Table.TableName};";
        }

        static string ToSqlType(Type t, bool isIndex, bool withNullDeclaration = true)
        {
            var nonNullableType = t.RemoveNullability();
            var res = ToSqlType(nonNullableType, isIndex);
            if (!withNullDeclaration)
                return res;
            if (nonNullableType != t || !t.IsValueType)
                return res + " NULL";
            return res + " NOT NULL";
        }

        static string ToSqlType(Type t, bool isIndex)
        {
            if (t == typeof(byte) || t == typeof(sbyte))
                return "tinyint";
            if (t == typeof(short) || t == typeof(ushort))
                return "smallint";
            if (t == typeof(int) || t == typeof(uint))
                return "int";
            if (t == typeof(long) || t == typeof(ulong))
                return "bigint";
            if (t == typeof(string))
                // 4000 = max size for index
                // COLLATE DATABASE_DEFAULT = By default Temp tables use server collation, which can be different from database
                return "nvarchar(" + (isIndex ? "4000" : "max") + ") COLLATE DATABASE_DEFAULT";
            if (t == typeof(char))
                return "nchar(1)";
            if (t == typeof(bool))
                return "bit";
            if (t == typeof(decimal))
                return "numeric(38,10)";
            if (t == typeof(double))
                return "float";
            if (t == typeof(float))
                return "real";
            if (t == typeof(byte[]))
                return "varbinary(max)";
            if (t == typeof(Guid))
                return "uniqueidentifier";
            if (t == typeof(DateTimeOffset))
                return "DATETIMEOFFSET";
            if (t == typeof(DateTime))
                return "datetime2";
            if (t == typeof(TimeSpan))
                return "time";
            throw new Exception("Not handled yet!");
        }

        /// <summary>
        /// This method could have side effect,
        /// datasource of remote tables may be evaluated in this method instead of later in usual execution path...
        /// </summary>
        public StringBuilder ToDebugSql()
        {
            var sb = new StringBuilder();
            if (!_canBeReplayed)
            {
                sb.Append("-- Cannot be executed again! Some datasource cannot be enumerated twice!");
                return sb;
            }
            foreach (var tt in _tempTables)
            {
                if (!tt.Replayable)
                    tt.EvaluateSource();
                tt.AsSqlCode(sb);
            }
            sb.AppendLine("-- your code here");
            foreach (var tt in _tempTables)
                sb.AppendLine(ToSqlDropTable(tt, true).ToString());
            return sb;
        }
    }
}
