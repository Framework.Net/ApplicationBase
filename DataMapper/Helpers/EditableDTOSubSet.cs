﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.UI;
using TechnicalTools.UI.EditableLayer;


namespace DataMapper.Helpers
{
    /// <summary>
    /// TODO : this code is almost a Copy / Paste of EditableSubSetDependent.... Make an "_Impl" class in TechnicalTool ?
    /// EditableDTOSubSetDependent is not an EditableDTOSet where the GetFreshItem is just "more filtered"
    /// EditableDTOSubSetDependent is dependant on another EditableDTOSet so that items are shared and not loaded twice in memory
    /// EditableDTOSubSetDependent is to EditableDTOSet as DataView Type is to DataTable
    /// </summary>
    public abstract class EditableDTOSubSetDependent<TItem, IItem> : EditableDTOSet<TItem, IItem>
        where TItem : BaseDTO, IItem, IHasTypedIdReadable, ICloneable, new()
        where IItem : class, ICopyable<IItem>, IHasTypedIdReadable
    {
        readonly EditableDTOSet<TItem, IItem> _source; // dependency

        protected abstract Func<TItem, bool> GetFilter();

        protected EditableDTOSubSetDependent(EditableDTOSet<TItem, IItem> source, bool doNotLoadItems = false)
            : base(source._getMapper, source.BuildClosedId, source.ProvidedTypeBuilder, source.AllowAccessOnlyToMainInterface, true)
        {
            _source = source;
            AllowNew = _source.AllowNew;
            AllowEdit = _source.AllowEdit;
            AllowRemove = _source.AllowRemove;

            if (!doNotLoadItems && !DesignTimeHelper.IsInDesignMode)
                Refresh();
        }


        protected sealed override List<TItem> GetFreshItems(IProgress<string> pr)
        {
            lock (_lock)
            {
                if (!_avoidSourceRefresh)
                    _source.Refresh(pr);
                return GetFreshItemsFromLocalData();
            }
        }
        readonly object _lock = new object();
        bool _avoidSourceRefresh;

        List<TItem> GetFreshItemsFromLocalData()
        {
            var filter = GetFilter();
            return _source.OriginalSet.Where(filter).ToList();
        }

        protected override Action ApplyToDatabaseAndModel(IProgress<string> pr = null)
        {
            // because EditableSet class is supposed to works using diff,
            // We can use ApplyToDatabaseAndModel the items not inside "this" won't be touched
            Action baseApply = base.ApplyToDatabaseAndModel(pr);
            return () =>
            {
                baseApply();
                // Notify base source that some object have been modified
                // TODO : Generate a back event instead of a method call because if _source is also a EditableSubSetDependent...
                _source.NotifyDependentSetHaveChanged(this);
                // Note : if we would call NotifyDependentSetHaveChanged(this) here,
                // the content of "this" should not change because this is already updated
            };
        }

        // Called by another dependent set
        public override void NotifyDependentSetHaveChanged(EditableSet<TItem, IItem> dependentSet = null)
        {
            // Probably call _source.Refresh() but we let _source to decide what it want to do
            _source.NotifyDependentSetHaveChanged(dependentSet);
            // Here we want to update / sync locally the result without doing a call to real source
            var b = _avoidSourceRefresh;
            _avoidSourceRefresh = true;
            try
            {
                Refresh();
            }
            finally
            {
                _avoidSourceRefresh = b;
            }
        }

        // TODO : Write other redirection ?
        protected override string ValidateProperty(IItem item, Type interfaceType, PropertyInfo property)
        {
            return _source._ValidateProperty(item, interfaceType, property);
        }

        protected override string ValidateSet(IEnumerable<IItem> set, IProgress<string> pr = null)
        {
            return _source._ValidateSet(set);
        }
    }

    public abstract class EditableDTOSubSetDependentOnSimpleFilter<TItem, IItem> : EditableDTOSubSetDependent<TItem, IItem>
        where TItem : BaseDTO, IItem, IHasTypedIdReadable, ICloneable, new()
        where IItem : class, ICopyable<IItem>, IHasTypedIdReadable
    {
        protected Func<TItem, bool> Filter { get; set; }

        protected override Func<TItem, bool> GetFilter() { return Filter; }

        protected EditableDTOSubSetDependentOnSimpleFilter(EditableDTOSet<TItem, IItem> source, Func<TItem, bool> filter, bool doNotLoadItems = false)
            : base(source, true)
        {
            Filter = filter;
            if (!doNotLoadItems && !DesignTimeHelper.IsInDesignMode)
                Refresh();
        }
    }

}
