﻿using System;
using System.Collections.Generic;
using TechnicalTools.Diagnostics;
using TechnicalTools.Extensions.Data;
using TechnicalTools.Model.Cache;


namespace DataMapper
{
    public abstract class DynamicEnumWithId<TKey, TEnumClass> : DynamicEnum<TKey, TEnumClass>, IHasTypedIdReadable<IdTuple<TKey>>
        where TKey : struct, IConvertible, IEquatable<TKey>, IComparable<TKey>, IComparable
        where TEnumClass : DynamicEnumWithId<TKey, TEnumClass>, IAutoMappedDbObject, new()
    {
        #region Same implementation than BaseDTO (in the same folder)

        /// <summary>
        /// Représente l'id de l'objet. L'id est un IIdTuple (composition de plusieurs données de type simple : int string date etc)
        /// Cette proprieté est nommé ClosedId pour eviter un conflit de nom avec la propriete courrament nommée Id dans les classes filles
        /// </summary>
        protected abstract IdTuple<TKey> ClosedId { get; set; } IIdTuple IHasClosedIdReadable.Id { get { return ClosedId; } }

        [IsTechnicalProperty]
        public virtual TypedId<IdTuple<TKey>>                                      TypedId                       { get { return ClosedId.ToTypedId(GetType()); } }
                       ITypedId                                IHasTypedIdReadable.TypedId                       { get { return ClosedId.ToTypedId(GetType()); } }
                       ITypedId                                IHasTypedIdReadable.MakeTypedId(IIdTuple id)      { return id.ToTypedId(GetType()); }
                       TypedId<IdTuple<TKey>>   IHasTypedIdReadable<IdTuple<TKey>>.MakeTypedId(IdTuple<TKey> id) { return id.ToTypedId(GetType()); }


        public override int GetHashCode()
        {
            unchecked
            {
                return TypedId.GetHashCode();
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TEnumClass asDynamicEnum))
                return false;
            if (!TypedId.Type.IsAssignableFrom(asDynamicEnum.TypedId.Type) &&
                !asDynamicEnum.TypedId.Type.IsAssignableFrom(TypedId.Type))  // not the same type and no inheritancerelation (enum extending)
                return false;
            if (TypedId.Key.GetType() != asDynamicEnum.TypedId.Key.GetType())
                return false;
            if (!TypedId.Key.Equals(asDynamicEnum.TypedId.Key))
                return false;
            // ... do they belongs to the same owner (ie: in general it means same database) ?
            if (ReferenceEquals(OwnerMapper?.Owner, asDynamicEnum?.OwnerMapper?.Owner) ||
                OwnerMapper?.Owner == null || asDynamicEnum?.OwnerMapper?.Owner == null) // or maybe some of them are clone (ui layer do that sometimes when selecting a distinct value in column filter)
                return true;

            DebugTools.Break("Because i want to delete this behavior! if different database supposed to be identical do not have the same id, it is not the responsability of this class");
            return false;
        }

        string DebugString
        {
            get
            {
                return "("
                    + (OwnerMapper?.Owner?.Name
                        ?? "<NO SPECIFIC OWNER>")
                    + ", "
                    + ObjectDebugString
                    + ")";
            }
        }
        protected virtual string ObjectDebugString
        {
            get
            {
                return TypedId.ToSmartString() + Caption;
            }
        }

        #endregion

    }
}
