﻿using System;
using TechnicalTools.Annotations;
using TechnicalTools.Tools;


namespace DataMapper.Helpers
{
    [ToDo(About = IsAbout.Improvement | IsAbout.Performance, Priority = eWorkPriority.Low,
          Comment = "Override " + nameof(GetEstimatedSizeOfContent) + " to handle " + nameof(DynamicEnum) + " classes")]
    public class DtoTypeSizeEstimator : TypeSizeEstimator
    {
        public DtoTypeSizeEstimator(IDbMapper mapper = null)
        {
            _mapper = mapper;
            AddRuleForEmptySize((t, rec) =>
            {
                if (!typeof(DynamicEnum).IsAssignableFrom(t))
                    return null;
                // Nobodies can inherit DynamicENum except DynamicEnum<,>
                // so this code is safe
                while (!t.IsGenericType || t.GetGenericTypeDefinition() != typeof(DynamicEnum<,>))
                    t = t.BaseType;
                t = t.GetGenericArguments()[0];
                return rec(t);
            });

            AddRuleForEmptySize((t, rec) =>
            {
                if (!typeof(DynamicEnum).IsAssignableFrom(t))
                    return null;
                return 0;
            });

            AddRuleForEmptySize((t, _) => typeof(IDbMapper).IsAssignableFrom(t)
                                        ? 0
                                        : (int?)null);
        }
        protected readonly IDbMapper _mapper;
    }
}
