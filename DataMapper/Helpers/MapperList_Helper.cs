﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TechnicalTools;
using TechnicalTools.Logs;
using TechnicalTools.Tools;


namespace DataMapper
{
    public static class MapperList_Helper
    {
        /// <summary>
        /// See doc for <see cref="ForEachMapper{TMapper, TResult}(IReadOnlyCollection{TMapper}, IProgress{string}, Func{TMapper, Action{string}, TResult})"/>.
        /// The difference is that errors are automatically routed to UI Layer as a message and no exception is thrown.
        /// Use this when you are able to process incomplete set of partner.
        /// <code>
        public static async Task<Dictionary<TMapper, TResult>>
        ForEachMapper_WarnAndDiscardException<TMapper, TResult>(this IReadOnlyCollection<TMapper> partners, IProgress<string> pr, ILogger log,
                                              Func<TMapper, Action<string>, TResult> doWork)
            where TMapper : IDbMapperReadAccess
        {
            var results = await ForEachMapper(partners, pr, doWork).ConfigureAwait(false);
            if (results.Item2.Count > 0)
            {
                BusEvents.Instance.RaiseBusinessWarning("Please note the result of current task may be incomplete due to issue for some partner environments' DB!",
                                                        log, results.Item2.Select(error => error.Value));
            }
            return results.Item1;
        }

        /// <summary>
        /// Run a treatment on a set of partner, and display nicely the progression to user.
        /// Exception are kept until the end.
        /// See also <seealso cref="ForEachMapper_WarnAndDiscardException{TMapper, TResult}(IReadOnlyCollection{TMapper}, IProgress{string}, Func{TMapper, Action{string}, TResult})"/>
        /// <example><code>
        /// Dictionary&lt;Mapper, DataWorkLoadType&gt; partners = ...
        /// var pr = existingPr ?? new Progress&lt;string&gt;();
        /// pr = pr.WrapReportWith("Retrieving stats...");
        /// var result = partners.Keys.ToList().ForEachMapper(pr, (pe, reportProgress) =>
        /// {
        ///    reportProgress("Doing awsome step 1/3");
        ///    ...
        ///    reportProgress("Doing more awsome step 2/3");
        ///    ...
        ///    reportProgress("Doing final step 3/3");
        ///    ...
        ///    reportProgress("Done"); // Will be not, or very briefly, displayed
        ///    return 42;
        /// }).Results; // wait for the loop to finish
        /// // Here you have to check result.Item2 to know if error occured for some partner
        /// // and you can get the good results in result.Item1.
        /// </code></example>
        public static Task<Tuple<Dictionary<TMapper, TResult>, Dictionary<TMapper, Exception>>>
        ForEachMapper<TMapper, TResult>(this IReadOnlyCollection<TMapper> partners, IProgress<string> pr,
                                             Func<TMapper, Action<string>, TResult> doWork, int maxConcurrent = 0)
            where TMapper : IDbMapperReadAccess
        {
            var progressions = partners.ToConcurrentDictionary(pe => pe, kvp => (string)null);

            //long opeDoneBeforeCount = progress.Values.DefaultIfEmpty(0).Sum();
            var workingOn = new ConcurrentDictionary<TMapper, TMapper>();
            var done = new ConcurrentDictionary<TMapper, TMapper>();
            var start = DateTime.UtcNow;
            Func<string> generateReport = () =>
            {
                string res = done.Count.ToString() + " partners done of " + partners.Count.ToString() + Environment.NewLine;
                res += "Working on: " + Environment.NewLine;
                res += workingOn.Take(6)
                                .Select((kvp, i) => i + 1 < 6 ? "  - " + kvp.Key.Owner.Name + ": " + progressions[kvp.Key]
                                                              : "  - And others...")
                                .Join(Environment.NewLine);
                return res;
            };

            var wrap = pr.WrapReportWith(s => generateReport() + Environment.NewLine.AsPrefixForIfNotBlank(s));

            return partners.ForEachParallelSafelyAsDictionary("Evaluating all", new Progress<string>(), maxConcurrent: maxConcurrent, doWork: pe =>
            {
                workingOn.TryAdd(pe, pe);
                try
                {
                    Action<string> reportProgress = (progress) =>
                    {
                        progressions[pe] = progress;
                        wrap.Report(string.Empty);
                    };
                    wrap.Report(string.Empty);
                    var res = doWork(pe, reportProgress);
                    return res;
                }
                catch (Exception ex) when (ex.EnrichDiagnosticWithAndReturnFalse("For partner " + pe.Owner.Name, eExceptionEnrichmentType.UserUnderstandable, true, true))
                {
                    throw;
                }
                finally
                {
                    done.TryAdd(pe, pe);
                    (workingOn as IDictionary<IDbMapperReadAccess, IDbMapperReadAccess>).Remove(pe);
                    wrap.Report(string.Empty);
                }
            });
        }

    }
}
