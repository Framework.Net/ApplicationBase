﻿using System;


namespace DataMapper
{
    public partial class DbMapperWrapper
    {
        protected class EnumMappingCheckingHelper<TDAO>
            where TDAO : DbMapperWrapper
        {

            public EnumMappingCheckingHelper(Action<TDAO> check)
            {
                _check = check;
            }
            readonly Action<TDAO> _check;
            readonly object _lock = new object(); // double if pattern
            volatile bool _isDone; // double if pattern
            volatile bool _isChecking; // prevent recursion

            public void Check(TDAO dao)
            {
                if (!_isDone)
                    lock (_lock)
                        if (!_isDone && !_isChecking)
                        {
                            _isChecking = true;
                            try
                            {
                                _check(dao);
                            }
                            finally
                            {
                                _isChecking = false;
                            }
                            _isDone = true;
                        }
            }
        }
    }
}
