﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;

using TechnicalTools;
using TechnicalTools.Algorithm.Graph;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.UI.EditableLayer;


namespace DataMapper.Helpers
{
    public interface IEditableDTOSet : IEditableSet
    {
        Type GetTItemType();
        Type GetIItemType();
        bool MapperIsConstant { get; }
        Func<object, IDbMapper> GetMapper { get; }

        IEnumerable<EditableSubstitute> EditingSet { get; }
        IEnumerable<EditableSubstitute> Removed    { get; }
        IEnumerable<EditableSubstitute> Added      { get; }
    }
    public abstract class EditableDTOSet<TItem, IItem> : EditableSet<TItem, IItem>, IEditableDTOSet
        where TItem : BaseDTO, IItem, IHasTypedIdReadable, ICloneable, new()
        where IItem : class, ICopyable<IItem>, IHasTypedIdReadable
    {
        protected internal virtual bool MapperIsConstant  { get; set; } = true; bool IEditableDTOSet.MapperIsConstant { get { return MapperIsConstant; } }
        protected internal readonly Func<IItem, IDbMapper> _getMapper; Func<object, IDbMapper> IEditableDTOSet.GetMapper { get { return o => _getMapper((IItem)o); } }

        Type IEditableDTOSet.GetTItemType() { return typeof(TItem); }
        Type IEditableDTOSet.GetIItemType() { return typeof(IItem); }
        IEnumerable<EditableSubstitute> IEditableDTOSet.EditingSet { get { return EditingSet.Cast<EditableSubstitute>(); } }
        IEnumerable<EditableSubstitute> IEditableDTOSet.Removed    { get { return Removed.Cast<EditableSubstitute>(); } }
        IEnumerable<EditableSubstitute> IEditableDTOSet.Added      { get { return Added.Cast<EditableSubstitute>(); } }

        protected internal new EditableSubstituteBuilder ProvidedTypeBuilder            { get { return (EditableSubstituteBuilder)base.ProvidedTypeBuilder; } }
        protected internal new Func<IItem, IIdTuple>     BuildClosedId                  { get { return base.BuildClosedId; } }
        protected internal new bool                      AllowAccessOnlyToMainInterface { get { return base.AllowAccessOnlyToMainInterface; } }
        protected internal new FixedBindingList<TItem>   OriginalSet                    { get { return base.OriginalSet; } }

        protected EditableDTOSet(IDbMapper mapper, bool itemPKIsEditableOrPartiallyEditable, bool allowAccesOnlyToMainInterface = false, bool doNotLoadItems = false)
             : base(GetBuildClosedId(mapper, itemPKIsEditableOrPartiallyEditable), allowAccesOnlyToMainInterface, new EditableSubstituteBuilder(), doNotLoadItems)
        {
            _getMapper = _ => mapper;
        }
        protected EditableDTOSet(Func<IItem, IDbMapper> getMapper, bool itemPKIsEditableOrPartiallyEditable, bool allowAccesOnlyToMainInterface = false, bool doNotLoadItems = false, EditableSubstituteBuilder editableSubstituteBuilder = null)
             : base(GetBuildClosedId(getMapper, itemPKIsEditableOrPartiallyEditable), allowAccesOnlyToMainInterface, editableSubstituteBuilder ?? new EditableSubstituteBuilder(), doNotLoadItems)
        {
            _getMapper = getMapper;
        }
        protected EditableDTOSet(IDbMapper mapper, Func<IItem, IIdTuple> buildClosedId, bool allowAccesOnlyToMainInterface = false)
            : base(buildClosedId, allowAccesOnlyToMainInterface, new EditableSubstituteBuilder())
        {
            _getMapper = _ => mapper;
        }
        protected EditableDTOSet(Func<IItem, IDbMapper> getMapper, Func<IItem, IIdTuple> buildClosedId, bool allowAccesOnlyToMainInterface = false, bool doNotLoadItems = false)
            : base(buildClosedId, allowAccesOnlyToMainInterface, new EditableSubstituteBuilder(), doNotLoadItems)
        {
            _getMapper = getMapper;
        }
        protected internal EditableDTOSet(Func<IItem, IDbMapper> getMapper, Func<IItem, IIdTuple> buildClosedId, EditableSubstituteBuilder typebuilder = null, bool allowAccesOnlyToMainInterface = false, bool doNotLoadItems = false)
            : base(buildClosedId, allowAccesOnlyToMainInterface, typebuilder, doNotLoadItems)
        {
            _getMapper = getMapper;
        }
        static Func<IItem, IIdTuple> GetBuildClosedId(Func<IItem, IDbMapper> getMapper, bool itemPKIsEditableOrPartiallyEditable)
        {
            var dico = itemPKIsEditableOrPartiallyEditable ? CloseIdBuildersByStaticMapperGetter1 : CloseIdBuildersByStaticMapperGetter0;
            if (!dico.TryGetValue(getMapper, out Func<IItem, IIdTuple> res))
                res = dico.GetValue(getMapper, gm => item =>
                {
                    var mapper = getMapper(item);
                    var key2 = Tuple.Create(mapper, itemPKIsEditableOrPartiallyEditable);
                    if (!CloseIdBuildersByItem.TryGetValue(key2, out Func<IItem, IIdTuple>  res2))
                        res = CloseIdBuildersByItem.GetValue(key2, k => GetBuildClosedId(mapper, itemPKIsEditableOrPartiallyEditable));
                    return res(item);
                });
            return res;
        }
        static readonly ConditionalWeakTable<Tuple<IDbMapper, bool>, Func<IItem, IIdTuple>> CloseIdBuildersByItem = new ConditionalWeakTable<Tuple<IDbMapper, bool>, Func<IItem, IIdTuple>>();
        // These two are not Dictionary because developper-user won't think to use a static lambda to optimize... i guess, so we can't retain reference
        static readonly ConditionalWeakTable<Func<IItem, IDbMapper>, Func<IItem, IIdTuple>> CloseIdBuildersByStaticMapperGetter0 = new ConditionalWeakTable<Func<IItem, IDbMapper>, Func<IItem, IIdTuple>>();
        static readonly ConditionalWeakTable<Func<IItem, IDbMapper>, Func<IItem, IIdTuple>> CloseIdBuildersByStaticMapperGetter1 = new ConditionalWeakTable<Func<IItem, IDbMapper>, Func<IItem, IIdTuple>>();

        static Func<IItem, IIdTuple> GetBuildClosedId(IDbMapper mapper, bool itemPKIsEditableOrPartiallyEditable)
        {
            var mtable = mapper.GetTableMapping(typeof(TItem));
            var item = new TItem();
            var idTupleType = item.TypedId.Key.GetType();
            var cons = idTupleType.GetConstructors().Single();
            var iitemProperties = (itemPKIsEditableOrPartiallyEditable ? typeof(IItem) : typeof(TItem)).GetProperties();
            var keyProperties = mtable.PkFields.Select(f => Tuple.Create(f, iitemProperties.First(p => p.Name == f.PropertyInfo.Name))).ToArray();
            return iitem => (IIdTuple)cons.Invoke(keyProperties.Select(fp =>
            {
                var id = fp.Item2.GetValue(itemPKIsEditableOrPartiallyEditable ? iitem : GetEditedObject(iitem));
                id = fp.Item1.ConvertCsToDbValue(id);
                return id;
            }).ToArray());
        }

        protected internal class EditableSubstituteBuilder : EditableSubstituteBuilder<TItem, IItem>
        {
            protected override HashSet<Type> GetTypeDependencies(Type objType)
            {
                var res = base.GetTypeDependencies(objType);
                res.Add(typeof(IDbMapper));
                return res;
            }
        }

        protected override string ValidateProperty(IItem item, Type interfaceType, PropertyInfo property)
        {
            if (Removed.Contains(item))
                return null;
            // Redirect to another class for now, to make code editable at runtime
            return PropertyValidator.ValidateProperty(typeof(TItem), typeof(IItem), item, interfaceType, property);
        }
        [EditorBrowsable(EditorBrowsableState.Never)]
        internal string _ValidateProperty(IItem item, Type interfaceType, PropertyInfo property)
        {
            return ValidateProperty(item, interfaceType, property);
        }
        [EditorBrowsable(EditorBrowsableState.Never)]
        internal string _ValidateSet(IEnumerable<IItem> set)
        {
            return ValidateSet(set);
        }

        // This method just save all the list without looking for dependent data
        public override void Commit(SynchronizationContext context = null, IProgress<string> pr = null)
        {
            base.Commit(context, pr);
        }
        // This method save all the list and other data that are dependent in some way (foreign key)
        public virtual void CommitWith(params IEditableDTOSet[] otherLists)
        {
            using (var softTran = OpenSoftwareTransaction())
            {
                var actions = _CommitWith(otherLists.Concat(this).ToList());
                softTran.OnCommit += () => actions.ForEach(a => a());
                softTran.Commit();
            }
        }
        static List<Action> _CommitWith(List<IEditableDTOSet> allLists)
        {
            if (allLists.Any(lst => !lst.MapperIsConstant))
                throw new TechnicalException("CommitWith not implemented for sets with datamapper that depends on item!", null);
            if (allLists.GroupBy(lst => lst.GetMapper(null)).Count() > 1)
                throw new TechnicalException("CommitWith not implemented for set using different mapper!", null);

            var commonMapper = allLists.First().GetMapper(null);

            // Ca ne marche que si il n'y a pas de cycle dans les references !
            var mtables = allLists.ToLookup(lst => commonMapper.GetTableMapping(lst.GetTItemType()));
            var nodes = allLists.GroupBy(lst => commonMapper.GetTableMapping(lst.GetTItemType()))
                                .ToDictionary(grp => grp.Key,
                                              grp => new
                                              {
                                                  TableReferenced = grp.Key.ForeignKeys
                                                                           .Select(fk => commonMapper.GetTableMapping(fk.To))
                                                                           .Where(mtable => mtables.Contains(mtable)),
                                                  Lists = grp.ToList()
                                              });
            var tri = TopologicalOrderSimple.DoTopologicalSort(nodes.Select(node => node.Key).ToList(), mtable => nodes[mtable].TableReferenced, true)
                                            .SelectMany(node => nodes[node].Lists)
                                            .Reverse()
                                            .ToList();

            // La suite de l'algo peut planter si il y a des contraintes d'unicité autre que la PK !

            // Important : on simplifie (exemple un instance de type Referencé a son PK passé de "foo" a "bar" et une autre instance de Referencé a son id a foo
            //             il faut absolument simplifier car si une instance de type Referencant, non modifié,  a une FK sur "foo",
            //             l'algo suivant ne fonctionnera pas si on ne considere pas que l'instance de "foo" a juste été modifié (et conserve sa PK) et bar est une nouvelle instance avec une nouvelle PK
            //
            foreach (var lst in tri)
            {
                ((EditableSet)lst).ValidateBeforeCommit(); // A faire avant d'appeler SimplifyChangesOnId, Ce test est au moins censé tester l'unicite des PK
                ((EditableSet)lst).SimplifyChangesOnId();
            }


            var modelCommits = new List<Action>();

            // On fait les delete de la fin (les objets référencants) vers le début (objets réferencés)
            foreach (var lst in tri.AsEnumerable().Reverse())
            {
                var modelCommit = ((EditableSet)lst).ApplyRemovedItemToDatabase();
                modelCommits.Add(modelCommit);
            }

            foreach (var lst in tri)
            {
                var modelCommit = ((EditableSet)lst).ApplyCreatedItemToDatabase();
                modelCommits.Add(modelCommit);
            }

            foreach (var lst in tri)
            {
                var modelCommit = ((EditableSet)lst).ApplyChangedItemToDatabase();
                modelCommits.Add(modelCommit);
            }

            return modelCommits;
        }
        protected override SoftwareTransaction OpenSoftwareTransaction()
        {
            return new SoftwareTransaction();
        }
        public /*protected*/  override Action ApplyRemovedItemToDatabase(IProgress<string> pr = null)
        {
            var actions = new List<Action>();
            foreach (var r in Removed)
            {
                var item = GetEditedObject(r);
                _getMapper(item).DeleteInDatabase(item, false);
                actions.Add(() =>
                {
                    _originalSet.Remove(item);
                    Removed.Remove(r);
                    EditingSet.Remove(r);
                });
            }
            return () => actions.ForEach(a => a());
        }
        public /*protected*/  override Action ApplyCreatedItemToDatabase(IProgress<string> pr = null)
        {
            var actions = new List<Action>();
            foreach (var a in Added)
            {
                var item = GetEditedObject(a);
                if (item == null)
                {
                    DebugTools.Break("Should never happen, rest of code is just a secure way to avoid bug");
                    item = new TItem();
                }
                item.CopyFrom(a);
                _getMapper(item).CreateInDatabase(item, true);
                actions.Add(() =>
                { // TODO : ajouter au cache du mapper ? Il ne faut pas que le mapper ajoute le clone...
                    SetEditedObject(a, item); //we set object so if there is technical id (auto incremented, we have it !)
                    _originalSet.Add(item);
                    Added.Remove(a);
                });
            }

            // Instance with a PK that has changed
            var newFromChanged = EditingSet.Except(Added).Except(Removed).Where(c => !Equals(GetEditedObject(c).TypedId, c.TypedId)).ToList();
            foreach (var changed in newFromChanged)
            {
                var item = GetEditedObject(changed);
                var newItem = (TItem)item.Clone();
                newItem.CopyFrom(changed);
                _getMapper(item).CreateInDatabase(newItem, true);
                actions.Add(() =>
                {
                    item.CopyFrom(newItem);
                });
            }

            return () => actions.ForEach(a => a());
        }
        public /*protected*/  override Action ApplyChangedItemToDatabase(IProgress<string> pr = null)
        {
            var actions = new List<Action>();
            foreach (var c in EditingSet.Except(Added).Except(Removed).Where(c => Equals(GetEditedObject(c).TypedId, c.TypedId)).ToList())
            {
                if (IsDirty(c))
                {
                    var item = GetEditedObject(c);
                    var clone = (TItem)item.Clone();
                    clone.CopyFrom(c);
                    _getMapper(item).UpdateToDatabase(clone, false);
                    actions.Add(() =>
                    {
                        item.CopyFrom(clone);
                    });
                }
            }
            return () => actions.ForEach(a => a());
        }

    }


    static class PropertyValidator
    {
        public static string ValidateProperty(Type titem, Type iitem, object item, Type interfaceType, PropertyInfo property)
        {
            var value = property.GetValue(item);

            var propWithAtt = titem.GetProperty(property.Name);
            Debug.Assert(propWithAtt != null, nameof(propWithAtt) + " != null");

            if (value == null && (!(propWithAtt.GetCustomAttribute<DbMappedField>()    ?.IsNullable     ?? true) ||
                                  !(propWithAtt.GetCustomAttribute<RequiredAttribute>()?.IsValid(value) ?? true)))
                return "The " + property.Name + " field is required!";

            var att = propWithAtt.GetCustomAttribute<StringLengthAttribute>();
            if (att != null)
            {
                if (value == null)
                    if (att.MinimumLength > 0)
                        return "The " + property.Name + " field is required!";
                    else
                        return null;

                if (value is string)
                {
                    if ((value as string).Length < att.MinimumLength)
                        return "Length must be at least " + att.MinimumLength + " characters!";
                    else if ((value as string).Length > att.MaximumLength)
                        return "Length must be " + att.MaximumLength + " characters top!";
                }
                else if (value is Array)
                {
                    if ((value as Array).Length < att.MinimumLength)
                        return "Length must be at least " + att.MinimumLength + " characters!";
                    else if ((value as Array).Length > att.MaximumLength)
                        return "Length must be " + att.MaximumLength + " characters top!";
                }
                else
                {
                    DebugTools.Should(false, "Does not know how to handle this !");
                }
            }

            // Autre attribut a gérer un jour
            // (from https://documentation.devexpress.com/#WPF/CustomDocument9770)
            //System.ComponentModel.DataAnnotations.CustomValidationAttribute Uses a custom method for validation.
            //System.ComponentModel.DataAnnotations.DataTypeAttribute Specifies a particular type of data, such as an e - mail address or phone number.
            //System.ComponentModel.DataAnnotations.EnumDataTypeAttribute Ensures that the value exists in an enumeration.
            //System.ComponentModel.DataAnnotations.RangeAttribute    Designates minimum and maximum constraints.
            //System.ComponentModel.DataAnnotations.RegularExpressionAttribute    Uses a regular expression to determine valid values.

            // Old code
            //try
            //{
            //    // Make setter validate technical constraint (length, nullability etc)
            //    property.SetValue(new TItem(), property.GetValue(item));
            //    return null;
            //}
            //catch (Exception ex)
            //{
            //    while (ex is TargetInvocationException)
            //        ex = (ex as TargetInvocationException).InnerException;
            //    return ex.Message;
            //}

            return null;
        }
    }
}
