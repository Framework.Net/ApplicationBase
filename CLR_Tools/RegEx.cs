using System;
using System.Data.SqlTypes;
using System.Text.RegularExpressions;

using Microsoft.SqlServer.Server;


public partial class clrTools // NOSONAR because putting this in a namespace "MyNamespace" would force sql user to write clrTools.MyNamespace.MethodName which is counter intuitive
{
    [SqlFunction(DataAccess = DataAccessKind.None,
                 IsDeterministic = true, IsPrecise = false,
                 SystemDataAccess = SystemDataAccessKind.None)]
    public static SqlString RegEx_Replace(SqlString pattern, SqlString substitute, SqlString input)
    {
        if (pattern.IsNull)
            return input;
        if (input.IsNull)
            return input;
        if (substitute.IsNull)
            substitute = new SqlString("");
        return Regex.Replace(input.Value, pattern.Value, substitute.Value);
    }

    [SqlFunction(DataAccess = DataAccessKind.None,
                 IsDeterministic = true, IsPrecise = false,
                 SystemDataAccess = SystemDataAccessKind.None)]
    public static SqlBoolean RegEx_IsMatch(SqlString patternAsDotNetRegex, SqlString text)
    {
        if (patternAsDotNetRegex.IsNull)
            throw new Exception("Pattern is not expected to be null!");
        if (text.IsNull)
            return SqlBoolean.False;

        // Important note : Framework .Net uses an internal cache string => Regex
        // TODO : Check for memory leak (but cache seems to be handled by WeakReference so it should be ok)
        return Regex.IsMatch(text.Value, patternAsDotNetRegex.Value, MostOptimizedAndRawOptions);
    }
    static readonly RegexOptions MostOptimizedAndRawOptions = RegexOptions.Compiled // Optimized if we need to use IsMatch on millions of row
                                                            | RegexOptions.ExplicitCapture // we rarely need to capture so it will optimize RegexOptions.Compiled
                                                            | RegexOptions.CultureInvariant // Make things simple and let software guys to deal with culture specific issues
                                                            | RegexOptions.Multiline; // Provide the maximum of feature. Developper is still able to use [^\r\n] to inhibit this option
}
