using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlTypes;

using Microsoft.SqlServer.Server;


public partial class clrTools // NOSONAR because putting this in a namespace "MyNamespace" would force sql user to write clrTools.MyNamespace.MethodName which is counter intuitive
{
    [SqlFunction(DataAccess = DataAccessKind.None,
        IsDeterministic = true, IsPrecise = false,
        SystemDataAccess = SystemDataAccessKind.None,
        FillRowMethodName = "StringToRow", TableDefinition = "value nvarchar(max)")]
    public static IEnumerable String_Split(SqlChars @string, SqlChars separator)
    {
        var result = splitToString(@string, separator).ToList();
        return result;
    }
    [SqlFunction(DataAccess = DataAccessKind.Read,
        IsDeterministic = true, IsPrecise = false,
        SystemDataAccess = SystemDataAccessKind.None,
        FillRowMethodName = "StringToRow", TableDefinition = "value nvarchar(max)")]
    public static IEnumerable String_Split_Streamed(SqlChars @string, SqlChars separator)
    {
        return splitToString(@string, separator);
    }
    public static void StringToRow(object source, out string str)
    {
        str = (string)source;
    }
    static IEnumerable<string> splitToString(SqlChars @string, [SqlFacet(MaxSize = 1, IsNullable = false)] SqlChars separator)
    {
        if (@string.IsNull)
            yield break;
        var str = new string(@string.Value);
        if (new string(separator.Value).Length == 0)
            throw new Exception("Separator cannot be empty");
        var sep = new string(separator.Value)[0];
        int i = 0;
        int next;
        while ( i  < str.Length &&
               -1 != (next = str.IndexOf(sep, i)))
        {
            yield return str.Substring(i, next - i);
            i = next + 1;
        }
        yield return str.Substring(i, str.Length - i);
    }



    [SqlFunction(DataAccess = DataAccessKind.None,
        IsDeterministic = true, IsPrecise = false,
        SystemDataAccess = SystemDataAccessKind.None,
        FillRowMethodName = "IntIntervalToRow", TableDefinition = "lower int, upper int")]
    public static IEnumerable String_SplitToIntIntervals(SqlChars chars)
    {
        var result = splitToInt(chars).ToList();
        return result;
    }
    [SqlFunction(DataAccess = DataAccessKind.Read,
        IsDeterministic = true, IsPrecise = false,
        SystemDataAccess = SystemDataAccessKind.None,
        FillRowMethodName = "IntIntervalToRow", TableDefinition = "lower int, upper int")]
    public static IEnumerable String_SplitToIntIntervals_Streamed(SqlChars chars)
    {
        return splitToInt(chars);
    }
    public static void IntIntervalToRow(object source, out int lower, out int upper)
    {
        var t = (Tuple<int, int>)source;
        lower = t.Item1;
        upper = t.Item2;
    }
    static IEnumerable<Tuple<int, int>> splitToInt(SqlChars chars)
    {
        var str = new string(chars.Value);
        int i = 0;
        while (i < str.Length)
        {
            if (str[i] < '0' || str[i] > '9')
                throw new Exception("Char at index " + i + " is not expected!");

            int lower = str[i] - '0';
            while (++i < str.Length && str[i] >= '0' && str[i] <= '9')
            {
                lower *= 10;
                lower += str[i] - '0';
            }

            int upper;
            if (i < str.Length && str[i] == '-')
            {
                ++i;
                upper = str[i] - '0';
                while (++i < str.Length && str[i] >= '0' && str[i] <= '9')
                {
                    upper *= 10;
                    upper += str[i] - '0';
                }
            }
            else
                upper = lower;
            yield return Tuple.Create(lower, upper);

            if (i >= str.Length)
                yield break;
            if (str[i] != ',')
                throw new Exception("Char at index " + i + " is expected to be a comma!");
            ++i;
        }
    }



    [SqlFunction(DataAccess = DataAccessKind.None,
        IsDeterministic = true, IsPrecise = false,
        SystemDataAccess = SystemDataAccessKind.None,
        FillRowMethodName = "BigIntIntervalToRow", TableDefinition = "lower bigint, upper bigint")]
    public static IEnumerable String_SplitToBigIntIntervals(SqlChars chars)
    {
        var result = splitToLong(chars).ToList();
        return result;
    }
    [SqlFunction(DataAccess = DataAccessKind.Read,
        IsDeterministic = true, IsPrecise = false,
        SystemDataAccess = SystemDataAccessKind.None,
        FillRowMethodName = "BigIntIntervalToRow", TableDefinition = "lower bigint, upper bigint")]
    public static IEnumerable String_SplitToBigIntIntervals_Streamed(SqlChars chars)
    {
        return splitToLong(chars);
    }
    public static void BigIntIntervalToRow(object source, out long lower, out long upper)
    {
        var t = (Tuple<long, long>)source;
        lower = t.Item1;
        upper = t.Item2;
    }
    static IEnumerable<Tuple<long, long>> splitToLong(SqlChars chars)
    {
        var str = new string(chars.Value);
        int i = 0;
        while (i < str.Length)
        {
            if (str[i] < '0' || str[i] > '9')
                throw new Exception("Char at index " + i + " is not expected!");

            long lower = str[i] - '0';
            while (++i < str.Length && str[i] >= '0' && str[i] <= '9')
            {
                lower *= 10;
                lower += str[i] - '0';
            }

            long upper;
            if (i < str.Length && str[i] == '-')
            {
                ++i;
                upper = str[i] - '0';
                while (++i < str.Length && str[i] >= '0' && str[i] <= '9')
                {
                    upper *= 10;
                    upper += str[i] - '0';
                }
            }
            else
                upper = lower;
            yield return Tuple.Create(lower, upper);

            if (i >= str.Length)
                yield break;
            if (str[i] != ',')
                throw new Exception("Char at index " + i + " is expected to be a comma!");
            ++i;
        }
    }

    class Tuple<T1, T2>
    {
        public T1 Item1 { get; private set; }
        public T2 Item2 { get; private set; }

        public Tuple(T1 item1, T2 item2)
        {
            Item1 = item1;
            Item2 = item2;
        }
    }
    class Tuple
    {
        public static Tuple<T1, T2> Create<T1, T2>(T1 item1, T2 item2)
        {
            return new Tuple<T1, T2>(item1, item2);
        }
    }
}
