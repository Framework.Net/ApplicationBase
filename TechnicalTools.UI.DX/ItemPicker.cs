﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using DevExpress.XtraEditors;


namespace TechnicalTools.UI.DX
{
    public partial class ItemPicker : XtraForm
    {
        public static DialogResult ShowDialog<T>(IWin32Window owner, IEnumerable<T> values, Func<T, string> getDisplayText, ref T default_choice, string msg, string title = null, bool? allow_null = null, string remarks = null)
            where T : class
        {
            var frm = new ItemPicker
            {
                Text = title ?? Application.ProductName
            };
            frm.lblRemark.Text = remarks ?? " "; // we use " " otherwise DevExpress displays the name of the control

            if (allow_null.HasValue && !allow_null.Value && default_choice == null)
                throw new Exception("If user is not allowed to choose null, you have to define a default choice (not null)!");
            if (!values.Contains(default_choice))
                throw new Exception("Default choice must be in selectable list of values!");
            frm.lueSelector.FillWithObjects(values, getDisplayText, allow_null ?? default_choice == null);
            if (default_choice == null)
                frm.lueSelector.UnselectAnyObject();
            else
                frm.lueSelector.SelectedValueAsObjectSet(default_choice);

            frm.lueSelector_LayoutItem.Text = msg;


            // Resize the form to fit the content
            var size = frm.layoutControl1.GetPreferredSize(Minimum);
            frm.ClientSize = size;
            var status = frm.ShowDialog(owner);

            default_choice = frm.lueSelector.SelectedValueAsObject<T>();
            return status;
        }
        static readonly System.Drawing.Size Minimum = new System.Drawing.Size(1, 1);

        public static DialogResult ShowDialog(IWin32Window owner, IEnumerable<string> values, ref string default_choice, string msg, string title = null, bool? allow_null = null)
        {
            var frm = new ItemPicker();
            return ShowDialog(owner, values, s => s, ref default_choice, msg, title, allow_null);
        }

        public static DialogResult ShowDialog<T>(IWin32Window owner, IEnumerable<T> values, ref T? default_choice, string msg, string title = null, bool? allow_null = null)
            where T : struct
        {
            var frm = new ItemPicker();
            var tuple_values = values.Select(item => new Tuple<T>(item)).ToList();
            T? dv = default_choice;
            var default_tuple = dv == null
                              ? null
                              : (tuple_values.FirstOrDefault(t => Equals(t.Item1, dv.Value)) ?? Tuple.Create(dv.Value)); // On crée un objet par defaut pour que ce soit l'autre methode qui gere les cas foireux
            var status = ShowDialog(owner, tuple_values, t => t.Item1.ToString(), ref default_tuple, msg, title, allow_null);
            if (status == DialogResult.OK)
                default_choice = default_tuple == null ? null : (T?)default_tuple.Item1;
            return status;
        }


        protected ItemPicker() // protected for VS designer
        {
            InitializeComponent();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
