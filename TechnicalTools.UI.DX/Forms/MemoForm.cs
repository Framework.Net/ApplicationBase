﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.XtraLayout.Utils;


namespace TechnicalTools.UI.DX.Forms
{
    public partial class MemoForm : EnhancedXtraForm
    {
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DevExpress.XtraEditors.MemoEdit Memo
        {
            get { return memo; }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string MemoText
        {
            get { return memo.Text; }
            set { memo.Text = value; }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ShowCancel
        {
            get { return btnCancel_LayoutItem.Visibility == LayoutVisibility.Always; }
            set { btnCancel_LayoutItem.Visibility = value ? LayoutVisibility.Always : LayoutVisibility.Never; }
        }

        public MemoForm(string text = null, string title = null)
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;

            Text += title?.Length == 0 ? "" : (title ?? "Memo") + " " + DateTime.Now.ToString(CultureInfo.CurrentCulture);
            memo.Text = text;
            btnCancel_LayoutItem.Visibility = LayoutVisibility.Never;
            BestFit();
        }

        void btnOk_Click(object sender, EventArgs e)
        {
            if (Modal) DialogResult = DialogResult.OK;
            else       Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (Modal) DialogResult = DialogResult.Cancel;
            else Close();
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            void Default_StyleChanged(object sender, EventArgs _) => BestFit();

            Disposed += (_, __) => UserLookAndFeel.Default.StyleChanged -= Default_StyleChanged;
            UserLookAndFeel.Default.StyleChanged += Default_StyleChanged;
        }

        public void BestFit()
        {
            // More magic number, weee
            const int AbsoluteMinWidth = 467;
            const int MaxSizeMargin = 20; // Margin to the screen for max size, avoir control to stick to screen borders
            const int AdjustWidth = 30; // Memo.CalcBestSize doesn't seem to take the scroll bar in the calculation...
            const int FormTextMargin = 30; // For this.controlbox (how to get the size in runtime?)


            var screenSize = Screen.FromControl(this).WorkingArea;
            var memoBestSize = Memo.CalcBestSize();
            var fontSize = layoutControl1.Font.Size; // How to get the font size from UserLookAndFeel?
            var formTextWidth = (int)(Text.Length * fontSize);

            var heightMargin = Height - Memo.Height;
            var widthMargin = Width - Memo.Width;

            var newHeight = memoBestSize.Height + heightMargin;
            var newWidth = memoBestSize.Width + widthMargin + AdjustWidth;

            var maxHeight = screenSize.Height - MaxSizeMargin;
            var maxWidth = screenSize.Width - MaxSizeMargin;

            var minWidth = Math.Max(formTextWidth + FormTextMargin, AbsoluteMinWidth);

            Height = Math.Min(newHeight, maxHeight);
            Width = Math.Min(Math.Max(newWidth, minWidth), maxWidth);
            CenterToParent();
        }
    }
}
