﻿using System;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;


namespace TechnicalTools.UI.DX.Forms
{
    public partial class FilterForm : EnhancedXtraForm
    {
        //ExpressionEvaluator ee = new ExpressionEvaluator(TypeDescriptor.GetProperties(type),
        //                                                  mappedFilterControl1.FilterCriteria);
        // foreach (var line in ee.Filter(-data))
        public string FilterString { get { return DialogResult == DialogResult.OK ? mappedFilterControl1.FilterString : _originalFilterCriteria; } }

        string _originalFilterCriteria;

        public FilterControl FilterControl { get { return mappedFilterControl1; } }

        public FilterForm(Type type, string filterString, bool allowInterface = false)
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            SetMessage(string.Empty);
            _originalFilterCriteria = filterString;
            mappedFilterControl1.Init(type, filterString, allowInterface:allowInterface);
        }

        public void SetMessage(string message)
        {
            // not empty because LayoutItem display their name by default
            lblMessage.Text = message.IfBlankUse(" ");
            lblMessage.Visibility = string.IsNullOrWhiteSpace(message) ? LayoutVisibility.Never : LayoutVisibility.Always;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (Modal) DialogResult = DialogResult.OK;
            else       Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (Modal) DialogResult = DialogResult.Cancel;
            else Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            mappedFilterControl1.ApplyFilter();
        }
    }
}
