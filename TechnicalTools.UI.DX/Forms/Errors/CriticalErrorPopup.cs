﻿using System;
using System.Drawing;


namespace TechnicalTools.UI.DX.Forms.Errors
{
    public partial class CriticalErrorPopup : TechnicalErrorPopup
    {
        public static Image CriticalErrorImage
        {
            get { return _CriticalErrorImage ?? DefaultCriticalErrorImage; }
            set { _CriticalErrorImage = value; }
        }
        static Image _CriticalErrorImage;

        public static readonly Image DefaultCriticalErrorImage = Image.FromStream(typeof(EnhancedGridView_DisplayColumnHint).Assembly.GetManifestResourceStream(typeof(EnhancedGridView_DisplayColumnHint).Namespace + ".Resources.Images.Errors.CriticalError.png"));


        CriticalErrorPopup()
            : this (null, null)
        {
        }

        public CriticalErrorPopup(Exception ex, string additional_info = null)
            : base(ex, additional_info, true)
        {
            InitializeComponent();
            // Prevent memory leak because constructor of timer does not take (sometimes) this.component as argument when created in designer
            Disposed += (_, __) => timerLockButton.Dispose();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            Image = CriticalErrorImage;
            ButtonOK.Enabled = false;
            timerLockButton.Enabled = true;


            Picture.Left += 15;
            Memo.Left += 30;
            Memo.Width -= 30;

            //Picture.BringToFront();
        }

        void CriticalErrorPopup_Load(object sender, EventArgs e)
        {
            Picture.LookAndFeel.UseDefaultLookAndFeel = false;
            Picture.LookAndFeel.SkinName = "DevExpress Style";
            Memo.LookAndFeel.UseDefaultLookAndFeel = false;
            Memo.LookAndFeel.SkinName = "DevExpress Style";
            ButtonOK.LookAndFeel.UseDefaultLookAndFeel = false;
            ButtonOK.LookAndFeel.SkinName = "DevExpress Style";
            ButtonCopy.LookAndFeel.UseDefaultLookAndFeel = false;
            ButtonCopy.LookAndFeel.SkinName = "DevExpress Style";
            // Setter ce skin n'impacte que la form
            // La form ne changera pas de skin tant qu'on ne l'enleve pas.
            // Le but de ce skin est de faire comprendre a l'utilisateur que quelquechose ne vas pas
            // Le but est qu'il previennent le plus vite possible les developpeurs afin que ces derniers
            // puissent retablir la cohérence des données.
            // Si c'est le reseau qui dconne les développeurs ne peuvent pas mettre en place de detection automatique
            LookAndFeel.SetSkinStyle("Metropolis Dark");
        }


        void timerLockButton_Tick(object sender, EventArgs e)
        {
            if (Disposing || IsDisposed)
                return;
            if (--_secondRemaining <= 0)
            {
                timerLockButton.Enabled = false;
                ButtonOK.Enabled = true;
                ButtonOK.Text = _ButtonOKText ?? ButtonOK.Text;
            }
            else
            {
                if (_ButtonOKText == null)
                    _ButtonOKText = ButtonOK.Text;
                ButtonOK.Text = _ButtonOKText + " (" + new TimeSpan(0, 0, _secondRemaining).ToHumanReadableShortNotation() + ")";
            }
        }
        string _ButtonOKText;
        int _secondRemaining = 5 * 60;
    }
}
