﻿using System;
using System.Diagnostics;
using System.Drawing;


namespace TechnicalTools.UI.DX.Forms.Errors
{
    public partial class UserUnderstandableErrorPopup : SoftErrorPopup
    {
        public static Image UserUnderstandableErrorImage { get; set; }

        UserUnderstandableErrorPopup()
            : this (null, false)
        {
        }

        public UserUnderstandableErrorPopup(Exception ex, bool displayTechnicalDetails)
            : base(ex, displayTechnicalDetails)
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            Image = UserUnderstandableErrorImage;
            // Setter ce skin n'impacte que la form
            // La form ne changera pas de skin tant qu'on ne l'enleve pas.
            // Le but etant de rassurer (un peu) l'utilisateur en donnant une teinte bleuatre à l'erreur car il s'agit
            // d'une erreur facilement compréhensible par l'utilisateur et non lié à une erreur de code
            // Voir la definition de UserUnderstandableException
            LookAndFeel.SetSkinStyle("Liquid Sky"); // y'a Lilian aussi et Blue mais ce dernier est un peu vieillot
        }

        void UserUnderstandableErrorPopup_Load(object sender, EventArgs e)
        {
            Debug.Assert(Name == "UserUnderstandableErrorPopup", "Nécessaire pour que des outils de test puissent reconnaitre la fenêtre");
        }
    }
}
