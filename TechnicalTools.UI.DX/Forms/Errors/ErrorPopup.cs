﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using TechnicalTools.UI.DX.BaseClasses;

namespace TechnicalTools.UI.DX.Forms.Errors
{
    /// <summary>
    /// Base class for all error understandable by user
    /// </summary>
    public partial class ErrorPopup : EnhancedXtraForm
    {
        protected SimpleButton ButtonOK { get { return btnOk; } }

        protected ErrorPopup()
        {
            InitializeComponent();

            // Using right/bottom anchors does not work in inherited forms
            meIssueReferenceIdCornerDist = new Size(ClientSize.Width - meIssueReferenceId.Right, meIssueReferenceId.Top);
            btnOkCornerDist = new Size(ClientSize.Width - btnOk.Right, ClientSize.Height - btnOk.Bottom);
            meIssueReferenceIdOriginalColor = meIssueReferenceId.ForeColor;
            Resize += ErrorPopup_Resize;
            if (DesignTimeHelper.IsInDesignMode)
                return;

            // Needed so testing automation can close the modal
            Tag = GetType().Name;
            CancelButton = btnOk;

            // Ergonomy preparation
            AcceptButton = btnOk; // overridable by child class
        }

        private void ErrorPopup_Load(object sender, EventArgs e)
        {
            if (Owner != null)
                StartPosition = FormStartPosition.CenterParent;
        }

        // triggered also when skin chanegd
        private void ErrorPopup_Resize(object sender, EventArgs e)
        {
            RefreshLayout();
        }

        void RefreshLayout()
        {
            var curSkin = SkinApplicatorHelper.GetTheoricalCurrentSkin()
                       ?? SkinApplicatorHelper.DefaultSkin
                       ?? string.Empty;
            meIssueReferenceId.ForeColor = curSkin.Contains("Dark")
                                          ? Color.Black
                                          : meIssueReferenceIdOriginalColor;
            meIssueReferenceId.Visible = !string.IsNullOrWhiteSpace(BugOrTicketId);
            meIssueReferenceId.Left = ClientSize.Width - meIssueReferenceId.Width - meIssueReferenceIdCornerDist.Width;

            btnOk.Visible = !string.IsNullOrWhiteSpace(BugOrTicketId);
            btnOk.Left = ClientSize.Width - btnOk.Width - btnOkCornerDist.Width;
            btnOk.Top = ClientSize.Height - btnOk.Height - btnOkCornerDist.Height;
        }
        Color meIssueReferenceIdOriginalColor;
        Size meIssueReferenceIdCornerDist;
        Size btnOkCornerDist;

        [DefaultValue("")]
        protected string BugOrTicketId
        {
            get { return _BugOrTicketId; }
            set
            {
                _BugOrTicketId = value;
                if (meIssueReferenceId.Tag == null)
                    meIssueReferenceId.Tag = meIssueReferenceId.Text;
                meIssueReferenceId.Text = (string)meIssueReferenceId.Tag + value;
                meIssueReferenceId.Width = CalcMemoHeighRequiredToDisplayError(this, meIssueReferenceId).Width;
                RefreshLayout();
            }
        }
        string _BugOrTicketId;

        protected virtual void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        public static Size CalcMemoHeighRequiredToDisplayError(Form frm, Controls.MemoEdit me)
        {
            var screen = Screen.FromRectangle(MainForm.Bounds);
            // Calcule la taille du texte
            var size = TextRenderer.MeasureText(me.Text, me.Font);
            // To handle size of scrollbars & skin
            size.Width += 15;
            size.Height += 15;
            // Fait en sorte que la taille ne depasse pas la zone travaillable de l'ecran (ie : l'ecran moins la taskbar par exemple)
            size = new Size(Math.Min(size.Width, screen.WorkingArea.Width), Math.Min(size.Height, screen.WorkingArea.Height));
            return size;
        }

        protected static Image GetImage(Image image, Color color)
        {
            Bitmap b = new Bitmap(image.Width, image.Height);
            Graphics g = Graphics.FromImage(b);
            g.FillRectangle(new SolidBrush(color), new Rectangle(new Point(0, 0), image.Size));
            g.Dispose();
            return b;
        }
    }
}
