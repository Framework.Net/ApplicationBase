﻿namespace TechnicalTools.UI.DX.Forms.Errors
{
    partial class TechnicalErrorPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCopy = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit = new TechnicalTools.UI.DX.Controls.MemoEdit();
            this.errorImage = new DevExpress.XtraEditors.PictureEdit();
            this.chkStopPopup = new DevExpress.XtraEditors.CheckEdit();
            this.txtForDevOnly = new DevExpress.XtraEditors.TextEdit();
            this.lblCollapseExpand2 = new System.Windows.Forms.Label();
            this.lblCollapseExpand1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorImage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStopPopup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtForDevOnly.Properties)).BeginInit();
            this.SuspendLayout();
            //
            // btnCopy
            //
            this.btnCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCopy.Location = new System.Drawing.Point(12, 530);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(151, 23);
            this.btnCopy.TabIndex = 3;
            this.btnCopy.Text = "Copy error informations";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            //
            // memoEdit
            //
            this.memoEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEdit.Location = new System.Drawing.Point(65, 33);
            this.memoEdit.Name = "memoEdit";
            this.memoEdit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.memoEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.memoEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.memoEdit.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit.Properties.Appearance.Options.UseFont = true;
            this.memoEdit.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit.Properties.ReadOnly = true;
            this.memoEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit.ShowScrollbarsOnlyWhenNeeded = true;
            this.memoEdit.Size = new System.Drawing.Size(547, 482);
            this.memoEdit.TabIndex = 4;
            //
            // errorImage
            //
            this.errorImage.Cursor = System.Windows.Forms.Cursors.Default;
            this.errorImage.Location = new System.Drawing.Point(0, 0);
            this.errorImage.MinimumSize = new System.Drawing.Size(40, 40);
            this.errorImage.Name = "errorImage";
            this.errorImage.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.errorImage.Properties.NullText = " ";
            this.errorImage.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.errorImage.Size = new System.Drawing.Size(65, 77);
            this.errorImage.TabIndex = 5;
            //
            // chkStopPopup
            //
            this.chkStopPopup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkStopPopup.Location = new System.Drawing.Point(325, 528);
            this.chkStopPopup.Name = "chkStopPopup";
            this.chkStopPopup.Properties.Appearance.Options.UseTextOptions = true;
            this.chkStopPopup.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.chkStopPopup.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chkStopPopup.Properties.Caption = "Always hide this exact issue until restart (not recommended)";
            this.chkStopPopup.Size = new System.Drawing.Size(216, 30);
            this.chkStopPopup.TabIndex = 1;
            this.chkStopPopup.Visible = false;
            this.chkStopPopup.CheckedChanged += new System.EventHandler(this.chkStopPopup_CheckedChanged);
            //
            // txtForDevOnly
            //
            this.txtForDevOnly.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtForDevOnly.EditValue = "[ DEVELOPERS ONLY ]";
            this.txtForDevOnly.Location = new System.Drawing.Point(60, 7);
            this.txtForDevOnly.Name = "txtForDevOnly";
            this.txtForDevOnly.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtForDevOnly.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtForDevOnly.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtForDevOnly.Properties.Appearance.Options.UseBackColor = true;
            this.txtForDevOnly.Properties.Appearance.Options.UseFont = true;
            this.txtForDevOnly.Properties.Appearance.Options.UseForeColor = true;
            this.txtForDevOnly.Properties.Appearance.Options.UseTextOptions = true;
            this.txtForDevOnly.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.txtForDevOnly.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtForDevOnly.Properties.ReadOnly = true;
            this.txtForDevOnly.Size = new System.Drawing.Size(193, 24);
            this.txtForDevOnly.TabIndex = 7;
            this.txtForDevOnly.Visible = false;
            //
            // lblCollapseExpand2
            //
            this.lblCollapseExpand2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCollapseExpand2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblCollapseExpand2.Location = new System.Drawing.Point(618, 500);
            this.lblCollapseExpand2.Name = "lblCollapseExpand2";
            this.lblCollapseExpand2.Size = new System.Drawing.Size(14, 15);
            this.lblCollapseExpand2.TabIndex = 8;
            this.lblCollapseExpand2.Text = "▼";
            this.lblCollapseExpand2.Click += new System.EventHandler(this.lblCollapseExpand_Click);
            //
            // lblCollapseExpand1
            //
            this.lblCollapseExpand1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCollapseExpand1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblCollapseExpand1.Location = new System.Drawing.Point(618, 491);
            this.lblCollapseExpand1.Name = "lblCollapseExpand1";
            this.lblCollapseExpand1.Size = new System.Drawing.Size(14, 15);
            this.lblCollapseExpand1.TabIndex = 9;
            this.lblCollapseExpand1.Text = "▼";
            this.lblCollapseExpand1.Click += new System.EventHandler(this.lblCollapseExpand_Click);
            //
            // TechnicalErrorPopup
            //
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 561);
            this.Controls.Add(this.txtForDevOnly);
            this.Controls.Add(this.chkStopPopup);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.memoEdit);
            this.Controls.Add(this.errorImage);
            this.Controls.Add(this.lblCollapseExpand1);
            this.Controls.Add(this.lblCollapseExpand2);
            this.Name = "TechnicalErrorPopup";
            this.Text = "Error";
            this.Load += new System.EventHandler(this.TechnicalErrorPopup_Load);
            this.Controls.SetChildIndex(this.lblCollapseExpand2, 0);
            this.Controls.SetChildIndex(this.lblCollapseExpand1, 0);
            this.Controls.SetChildIndex(this.errorImage, 0);
            this.Controls.SetChildIndex(this.memoEdit, 0);
            this.Controls.SetChildIndex(this.btnCopy, 0);
            this.Controls.SetChildIndex(this.chkStopPopup, 0);
            this.Controls.SetChildIndex(this.txtForDevOnly, 0);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorImage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStopPopup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtForDevOnly.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCopy;
        private TechnicalTools.UI.DX.Controls.MemoEdit memoEdit;
        private DevExpress.XtraEditors.PictureEdit errorImage;
        private DevExpress.XtraEditors.CheckEdit chkStopPopup;
        private DevExpress.XtraEditors.TextEdit txtForDevOnly;
        private System.Windows.Forms.Label lblCollapseExpand2;
        private System.Windows.Forms.Label lblCollapseExpand1;
    }
}