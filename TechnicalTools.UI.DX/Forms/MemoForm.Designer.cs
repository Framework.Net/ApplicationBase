﻿namespace TechnicalTools.UI.DX.Forms
{
    partial class MemoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.memo = new TechnicalTools.UI.DX.Controls.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.memo_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnOk_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.btnCancel_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memo_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel_LayoutItem)).BeginInit();
            this.SuspendLayout();
            //
            // layoutControl1
            //
            this.layoutControl1.Controls.Add(this.btnCancel);
            this.layoutControl1.Controls.Add(this.btnOk);
            this.layoutControl1.Controls.Add(this.memo);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(857, 118, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(467, 316);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            //
            // btnCancel
            //
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(12, 282);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(104, 22);
            this.btnCancel.StyleController = this.layoutControl1;
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            //
            // btnOk
            //
            this.btnOk.Location = new System.Drawing.Point(351, 282);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(104, 22);
            this.btnOk.StyleController = this.layoutControl1;
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            //
            // memo
            //
            this.memo.Location = new System.Drawing.Point(12, 12);
            this.memo.Name = "memo";
            this.memo.Properties.WordWrap = false;
            this.memo.Size = new System.Drawing.Size(443, 266);
            this.memo.StyleController = this.layoutControl1;
            this.memo.TabIndex = 1;
            //
            // layoutControlGroup1
            //
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.memo_LayoutItem,
            this.btnOk_LayoutItem,
            this.emptySpaceItem1,
            this.btnCancel_LayoutItem});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(467, 316);
            this.layoutControlGroup1.TextVisible = false;
            //
            // memo_LayoutItem
            //
            this.memo_LayoutItem.Control = this.memo;
            this.memo_LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.memo_LayoutItem.Name = "memo_LayoutItem";
            this.memo_LayoutItem.Size = new System.Drawing.Size(447, 270);
            this.memo_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.memo_LayoutItem.TextVisible = false;
            //
            // btnOk_LayoutItem
            //
            this.btnOk_LayoutItem.Control = this.btnOk;
            this.btnOk_LayoutItem.Location = new System.Drawing.Point(339, 270);
            this.btnOk_LayoutItem.MaxSize = new System.Drawing.Size(108, 26);
            this.btnOk_LayoutItem.MinSize = new System.Drawing.Size(108, 26);
            this.btnOk_LayoutItem.Name = "btnOk_LayoutItem";
            this.btnOk_LayoutItem.Size = new System.Drawing.Size(108, 26);
            this.btnOk_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.btnOk_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.btnOk_LayoutItem.TextVisible = false;
            //
            // emptySpaceItem1
            //
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(108, 270);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(231, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            // btnCancel_LayoutItem
            //
            this.btnCancel_LayoutItem.Control = this.btnCancel;
            this.btnCancel_LayoutItem.Location = new System.Drawing.Point(0, 270);
            this.btnCancel_LayoutItem.MaxSize = new System.Drawing.Size(108, 26);
            this.btnCancel_LayoutItem.MinSize = new System.Drawing.Size(108, 26);
            this.btnCancel_LayoutItem.Name = "btnCancel_LayoutItem";
            this.btnCancel_LayoutItem.Size = new System.Drawing.Size(108, 26);
            this.btnCancel_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.btnCancel_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.btnCancel_LayoutItem.TextVisible = false;
            //
            // MemoForm
            //
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(467, 316);
            this.Controls.Add(this.layoutControl1);
            this.Name = "MemoForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memo_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel_LayoutItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private TechnicalTools.UI.DX.Controls.MemoEdit memo;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem memo_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem btnOk_LayoutItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraLayout.LayoutControlItem btnCancel_LayoutItem;
    }
}