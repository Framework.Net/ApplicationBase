﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;


namespace TechnicalTools.UI.DX.Forms.Diagnostics
{
    // TODO : Ajouter un bouton dans la bar de titre de la fenetre permettant de switcher "Always on Top"
    // Voir http://stackoverflow.com/questions/2391382/custom-control-box-button
    // et http://stackoverflow.com/questions/106912/how-to-draw-custom-button-in-window-titlebar-with-windows-forms
    public partial class DebugFormBase : EnhancedXtraForm
    {
        public DebugFormBase()
        {
            InitializeComponent();

            Load += OnLoad;
        }

        void OnLoad(object sender, EventArgs eventArgs)
        {
            StartPosition = FormStartPosition.Manual;
            if (!DesignTimeHelper.IsInDesignMode)
            {
                // FromControl peut thrower si la MainForm est dans un autre thread
                // Donc on utilise FromRectangle
                CenterOnScreen(GetSideScreenOf(Screen.FromRectangle(MainForm.Bounds), true, Size));
            }
        }

        Screen GetSideScreenOf(Screen screen, bool return_current_screen_if_not_found, Size sizeToContains)
        {
            var screensOrdered = Screen.AllScreens // Les screens sont par default dans un ordre inconnu ...
                                       .OrderBy(s => s.Bounds.Left) // ... On les range donc en fonction de la zone de travail qu'ils représentent
                                       .ToList();

            int index = screensOrdered.IndexOf(screen);

            // Recupere un écran juste à coté de celui en cours d'utilisation.
            var screenOnSide = screensOrdered
                                     .Where(s => !s.Equals(screen)) // Les objets Screen sont recréés à chaque fois qu'on les demande, donc Equals
                                     .Select((s, i) => new
                                     {
                                         Screen = s,
                                         Index = i,
                                         Proximite = (s.Bounds.Left + screen.Bounds.Width / 2) - (screen.Bounds.Left + screen.Bounds.Width / 2)
                                     })
                // Tri par Voisnage des écran (gauche et droite en premier)
                                     .OrderBy(s => Math.Abs(s.Index - index)) // ecart des index = proximité par rapport a l'ecran utilisé
                // Puis capacité a afficher completement la form (attention ! false precede true !)
                                     .ThenByDescending(s => s.Screen.Bounds.Size.Width > sizeToContains.Width && s.Screen.Bounds.Size.Height > sizeToContains.Height)
                // Puis proximité de la form par rapprt a la form parente (evite de tourner la tete)
                                     .ThenBy(s => Math.Abs(s.Proximite))
                                     .Select(s => s.Screen)
                                     .FirstOrDefault();

            screenOnSide = screenOnSide ?? (return_current_screen_if_not_found ? screen : null);
            return screenOnSide;
        }

        protected void CenterOnScreen(Screen screen)
        {
            int left = screen.WorkingArea.Left + (screen.WorkingArea.Width - Width) / 2;
            int top = screen.WorkingArea.Top + (screen.WorkingArea.Height - Height) / 2;
            left = Math.Max(left, screen.WorkingArea.Left); // pas plus a gauche que la gauche de l'ecran
            top = Math.Max(top, screen.WorkingArea.Top); // pas plus en haut que le haut de l'ecran
            Location = new Point(left, top);
        }


        /// <summary>
        /// Affiche une fenêtre dans un autre thread.
        /// Cela permet que la fenetre reagisse mieux quand l'appli fait une grosse action qui bloque les évènement graphiques (souris, claviers, paint etc).
        /// </summary>
        /// <returns>La fenêtre créé. Attention !!! Elle est dans un autre thread graphique !</returns>
        protected static Form ShowInOtherGuiThread(Func<Form> createForm, string formName)
        {
            Debug.Assert(!string.IsNullOrEmpty(formName));

            while (_currentFormInCreation != null)
                Thread.Sleep(50);

            var th = new Thread(() =>
            {
                var frm = createForm();
                _currentFormInCreation = frm;
                //((Form)frm).Show();
                Application.Run(frm);
            });
            th.SetApartmentState(ApartmentState.STA); // Sinon certain controle graphique plante (exemple http://stackoverflow.com/questions/135803/dragdrop-registration-did-not-succeed)
            th.Start();
            while (_currentFormInCreation == null)
                Thread.Sleep(50);
            var res = _currentFormInCreation;
            _currentFormInCreation = null; // Delock si jamais il y avait une autre creation en même temps
            return res;
        }
        static volatile Form _currentFormInCreation;
    }
}
