﻿using System;
using System.ComponentModel;
using System.Linq;

using DevExpress.XtraGrid.Columns;

using DataMapper;
using DataMapper.Diagnostics;
using DataMapper.Tools;


namespace TechnicalTools.UI.DX.Forms.Diagnostics
{
    public partial class FrmDebugDatabaseTrace : DebugFormBase
    {
        readonly BindingList<RequestTrace> _requests = new BindingList<RequestTrace>();
        SqlPerformanceReader _sqlPerformanceReader;

        /// <summary>
        /// Pour la raison de cette méthode, voir <see cref="DebugFormBase.ShowInOtherGuiThread">base.Show</see>
        /// La fenetre se raffraichit régulièrement en cas de grosse action
        /// </summary>
        /// <returns>La fenêtre créée. Attention !!! Elle est dans un autre thread graphique !</returns>
        public static new FrmDebugDatabaseTrace Show()
        {
            return (FrmDebugDatabaseTrace)ShowInOtherGuiThread(() => new FrmDebugDatabaseTrace(), typeof(FrmDebugDatabaseTrace).Name);
        }

        public FrmDebugDatabaseTrace()
        {
            InitializeComponent();
        }

        void btnClear_Click(object sender, EventArgs e)
        {
            _requests.Clear();
            Text = "Capture start at " + DateTime.UtcNow + " (UTC)";
        }
        void btnStartCapture_Click(object sender, EventArgs e)
        {
            btnStartCapture.Enabled = false;
            btnStopCapture.Enabled = true;
            Enable();
        }
        void Enable()
        {
            IDbMapperSharedSettings.Instance.Request += DbMapper_Request;
            IDbMapperSharedSettings.Instance.DisableStreaming = true;
        }
        void btnStopCapture_Click(object sender, EventArgs e)
        {
            Disable();
            btnStartCapture.Enabled = true;
            btnStopCapture.Enabled = false;
        }
        void Disable()
        {
            IDbMapperSharedSettings.Instance.DisableStreaming = false;
            IDbMapperSharedSettings.Instance.Request -= DbMapper_Request;
        }
        void FormDebugClassHierarchy_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            gvRequestInfo.OptionsBehavior.Editable = false;
            gvRequestInfo.OptionsBehavior.ReadOnly = true;
            gvRequestInfo.OptionsView.ColumnAutoWidth = false;
            gcRequestInfo.DataSource = _requests;
            gvRequestInfo.Columns[nameof(RequestTrace.Mapper)].Visible = false;
            gvRequestInfo.Columns[nameof(RequestTrace.ConnectionString)].Visible = false;
            gvRequestInfo.Columns[nameof(RequestTrace.StartTimeUTC)].Visible = false;
            gvRequestInfo.Columns[nameof(RequestTrace.EndTimeUTC)].Visible = false;
            gvRequestInfo.Columns[nameof(RequestTrace.RequestPattern)].Visible = false;
            gvRequestInfo.Columns[nameof(RequestTrace.Success)].VisibleIndex = 98;
            gvRequestInfo.Columns[nameof(RequestTrace.Success)].Visible = false;
            gvRequestInfo.Columns[nameof(RequestTrace.Exception)].VisibleIndex = 99;
            gvRequestInfo.Columns[nameof(RequestTrace.Exception)].Visible = false;

            gvRequestInfo.Columns[nameof(RequestTrace.Request)].VisibleIndex = 97;

            foreach (GridColumn col in gvRequestInfo.Columns)
                col.OptionsColumn.FixedWidth = true;
            gvRequestInfo.Columns[nameof(RequestTrace.Request)].OptionsColumn.FixedWidth = false;

            gcRequestInfo.ForceInitialize();

            btnStartCapture_Click(null, null);
            btnClear_Click(null, null);
            Disposed += (_, __) => Disable();

            _sqlPerformanceReader = new SqlPerformanceReader();
        }

        void DbMapper_Request(RequestTrace trace)
        {
            if (InvokeRequired)
            {
                this.BeginInvokeSafely(() => DbMapper_Request(trace));
                if (!_ExceptionColumnHasBeenDisplayedOnce && trace.Exception != null)
                {
                    _ExceptionColumnHasBeenDisplayedOnce = true;
                    this.BeginInvokeSafely(() =>
                    {
                        gvRequestInfo.Columns[nameof(RequestTrace.Exception)].Visible = true;
                        gvRequestInfo.Columns[nameof(RequestTrace.Success)].Visible = true;
                    });
                }
                return;
            }
            _requests.Add(trace);
            if (_requests.Count == 1)
                gvRequestInfo.BestFitColumns(true);

            // Warning : this code is generally executed on another thread
        }
        bool _ExceptionColumnHasBeenDisplayedOnce;

        void btnGetSqlPerformanceReport_Click(object sender, EventArgs e)
        {
            string report = _sqlPerformanceReader.GetReport()
                                .Select(kvp => kvp.Key + ": " + kvp.Value)
                                .Join(Environment.NewLine);
            var frm = new MemoForm(report);
            frm.Show(this);
        }
    }
}
