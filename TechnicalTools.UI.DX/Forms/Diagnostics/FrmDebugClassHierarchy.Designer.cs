﻿namespace TechnicalTools.UI.DX.Forms.Diagnostics
{
    partial class FrmDebugClassHierarchy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeList = new DevExpress.XtraTreeList.TreeList();
            this.colTypeName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colGenericTypeInstanciations = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colNamespace = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colHierarchySize = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chkDisplayAsHierarchy = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisplayAsHierarchy.Properties)).BeginInit();
            this.SuspendLayout();
            //
            // treeList
            //
            this.treeList.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.treeList.Appearance.Row.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.treeList.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.treeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colTypeName,
            this.colGenericTypeInstanciations,
            this.colNamespace,
            this.colHierarchySize});
            this.treeList.CustomizationFormBounds = new System.Drawing.Rectangle(931, 452, 250, 209);
            this.treeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList.FixedLineWidth = 1;
            this.treeList.Location = new System.Drawing.Point(0, 45);
            this.treeList.Name = "treeList";
            this.treeList.OptionsFilter.ShowAllValuesInFilterPopup = true;
            this.treeList.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.treeList.OptionsView.FocusRectStyle = DevExpress.XtraTreeList.DrawFocusRectStyle.None;
            this.treeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList.OptionsView.ShowIndicator = false;
            this.treeList.OptionsView.ShowVertLines = false;
            this.treeList.OptionsView.TreeLineStyle = DevExpress.XtraTreeList.LineStyle.Dark;
            this.treeList.Size = new System.Drawing.Size(691, 399);
            this.treeList.TabIndex = 0;
            this.treeList.TreeLevelWidth = 12;
            //
            // colTypeName
            //
            this.colTypeName.Caption = "Type";
            this.colTypeName.FieldName = " ";
            this.colTypeName.Name = "colTypeName";
            this.colTypeName.OptionsColumn.FixedWidth = true;
            this.colTypeName.Visible = true;
            this.colTypeName.VisibleIndex = 0;
            this.colTypeName.Width = 338;
            //
            // colGenericTypeInstanciations
            //
            this.colGenericTypeInstanciations.Caption = "Generic Type Instanciation(s)";
            this.colGenericTypeInstanciations.FieldName = " ";
            this.colGenericTypeInstanciations.Name = "colGenericTypeInstanciations";
            this.colGenericTypeInstanciations.Visible = true;
            this.colGenericTypeInstanciations.VisibleIndex = 1;
            //
            // colNamespace
            //
            this.colNamespace.Caption = "Namespace";
            this.colNamespace.FieldName = " ";
            this.colNamespace.Name = "colNamespace";
            this.colNamespace.Visible = true;
            this.colNamespace.VisibleIndex = 2;
            this.colNamespace.Width = 317;
            //
            // colHierarchySize
            //
            this.colHierarchySize.Caption = "Hierarchy Size";
            this.colHierarchySize.FieldName = " ";
            this.colHierarchySize.Name = "colHierarchySize";
            this.colHierarchySize.OptionsColumn.FixedWidth = true;
            this.colHierarchySize.Visible = true;
            this.colHierarchySize.VisibleIndex = 3;
            this.colHierarchySize.Width = 20;
            //
            // panelControl1
            //
            this.panelControl1.Controls.Add(this.chkDisplayAsHierarchy);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(691, 45);
            this.panelControl1.TabIndex = 1;
            //
            // chkDisplayAsHierarchy
            //
            this.chkDisplayAsHierarchy.Location = new System.Drawing.Point(12, 12);
            this.chkDisplayAsHierarchy.Name = "chkDisplayAsHierarchy";
            this.chkDisplayAsHierarchy.Properties.Caption = "Display as hierarchy";
            this.chkDisplayAsHierarchy.Size = new System.Drawing.Size(126, 19);
            this.chkDisplayAsHierarchy.TabIndex = 0;
            this.chkDisplayAsHierarchy.CheckedChanged += new System.EventHandler(this.chkDisplayAsHierarchy_CheckedChanged);
            //
            // FrmDebugClassHierarchy
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 444);
            this.Controls.Add(this.treeList);
            this.Controls.Add(this.panelControl1);
            this.Name = "FrmDebugClassHierarchy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.FormDebugClassHierarchy_Load);
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkDisplayAsHierarchy.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList treeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTypeName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colNamespace;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colHierarchySize;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit chkDisplayAsHierarchy;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colGenericTypeInstanciations;
    }
}