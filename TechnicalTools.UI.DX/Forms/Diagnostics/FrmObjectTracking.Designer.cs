namespace TechnicalTools.UI.DX.Forms.Diagnostics
{
    partial class FrmObjectTracking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gc = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gv = new TechnicalTools.UI.DX.EnhancedGridView();
            this.colNamespace = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colTypeName = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colMinCount = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colMaxCount = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colDeltaMaxMin = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colNowCount = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colValues = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colDeltaNowMin = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.chkTrackObjectInstances = new DevExpress.XtraEditors.CheckEdit();
            this.btnPause = new DevExpress.XtraEditors.SimpleButton();
            this.lblLastRefreshTime = new DevExpress.XtraEditors.LabelControl();
            this.lblLastRefreshTimeTitle = new DevExpress.XtraEditors.LabelControl();
            this.lblHistory = new System.Windows.Forms.Label();
            this.seHistoryLength = new DevExpress.XtraEditors.SpinEdit();
            this.btnClearStatistics = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefreshNow = new DevExpress.XtraEditors.SimpleButton();
            this.btnClean = new DevExpress.XtraEditors.SimpleButton();
            this.lblUpdateSpeed = new System.Windows.Forms.Label();
            this.lueUpdateSpeed = new DevExpress.XtraEditors.LookUpEdit();
            this.tmrRefresh = new System.Windows.Forms.Timer(this.components);
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lblGCTotalMemory = new DevExpress.XtraEditors.LabelControl();
            this.lblPrivateMemorySize = new DevExpress.XtraEditors.LabelControl();
            this.lblVirtualMemorySizePeak = new DevExpress.XtraEditors.LabelControl();
            this.lblVirtualMemorySize = new DevExpress.XtraEditors.LabelControl();
            this.lblWorkingSetPeak = new DevExpress.XtraEditors.LabelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.lblWorkingSet = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gc_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblWorkingSet_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblPrivateMemorySize_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblVirtualMemorySize_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblWorkingSetPeak_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblVirtualMemorySizePeak_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblGCTotalMemory_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.gc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTrackObjectInstances.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seHistoryLength.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueUpdateSpeed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWorkingSet_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrivateMemorySize_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVirtualMemorySize_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWorkingSetPeak_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVirtualMemorySizePeak_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGCTotalMemory_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            this.SuspendLayout();
            //
            // gc
            //
            this.gc.Cursor = System.Windows.Forms.Cursors.Default;
            this.gc.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gc.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gc.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gc.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gc.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gc.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gc.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gc.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gc.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gc.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gc.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gc.EmbeddedNavigator.TextStringFormat = "Line {0}/{1} (total : {2})";
            this.gc.Location = new System.Drawing.Point(12, 12);
            this.gc.MainView = this.gv;
            this.gc.Name = "gc";
            this.gc.Size = new System.Drawing.Size(901, 385);
            this.gc.TabIndex = 0;
            this.gc.UseEmbeddedNavigator = true;
            this.gc.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv});
            //
            // gv
            //
            this.gv.Columns.AddRange(new TechnicalTools.UI.DX.EnhancedGridColumn[] {
            this.colNamespace,
            this.colTypeName,
            this.colMinCount,
            this.colMaxCount,
            this.colDeltaMaxMin,
            this.colNowCount,
            this.colValues,
            this.colDeltaNowMin});
            this.gv.GridControl = this.gc;
            this.gv.GroupCount = 1;
            this.gv.Name = "gv";
            this.gv.OptionsBehavior.Editable = false;
            this.gv.OptionsBehavior.ReadOnly = true;
            this.gv.OptionsDetail.EnableMasterViewMode = false;
            this.gv.OptionsView.ShowIndicator = false;
            this.gv.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNamespace, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDeltaNowMin, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNowCount, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gv.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv_CustomDrawCell);
            this.gv.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gv_RowCellStyle);
            //
            // colNamespace
            //
            this.colNamespace.Caption = "Namespace";
            this.colNamespace.Name = "colNamespace";
            this.colNamespace.OptionsColumn.FixedWidth = true;
            this.colNamespace.Visible = true;
            this.colNamespace.VisibleIndex = 0;
            this.colNamespace.Width = 113;
            //
            // colTypeName
            //
            this.colTypeName.AppearanceHeader.Options.UseTextOptions = true;
            this.colTypeName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTypeName.Caption = "Type";
            this.colTypeName.Name = "colTypeName";
            this.colTypeName.OptionsColumn.FixedWidth = true;
            this.colTypeName.Visible = true;
            this.colTypeName.VisibleIndex = 0;
            this.colTypeName.Width = 144;
            //
            // colMinCount
            //
            this.colMinCount.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colMinCount.AppearanceCell.Options.UseBackColor = true;
            this.colMinCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colMinCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMinCount.Caption = "# Min";
            this.colMinCount.Name = "colMinCount";
            this.colMinCount.OptionsColumn.FixedWidth = true;
            this.colMinCount.Visible = true;
            this.colMinCount.VisibleIndex = 3;
            //
            // colMaxCount
            //
            this.colMaxCount.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colMaxCount.AppearanceCell.Options.UseBackColor = true;
            this.colMaxCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxCount.Caption = "# Max";
            this.colMaxCount.Name = "colMaxCount";
            this.colMaxCount.OptionsColumn.FixedWidth = true;
            this.colMaxCount.Visible = true;
            this.colMaxCount.VisibleIndex = 1;
            this.colMaxCount.Width = 74;
            //
            // colDeltaMaxMin
            //
            this.colDeltaMaxMin.AppearanceHeader.Options.UseTextOptions = true;
            this.colDeltaMaxMin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDeltaMaxMin.Caption = "Δ Max-Min";
            this.colDeltaMaxMin.Name = "colDeltaMaxMin";
            this.colDeltaMaxMin.OptionsColumn.FixedWidth = true;
            this.colDeltaMaxMin.Visible = true;
            this.colDeltaMaxMin.VisibleIndex = 2;
            this.colDeltaMaxMin.Width = 79;
            //
            // colNowCount
            //
            this.colNowCount.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colNowCount.AppearanceCell.Options.UseBackColor = true;
            this.colNowCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colNowCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNowCount.Caption = "# Now";
            this.colNowCount.Name = "colNowCount";
            this.colNowCount.OptionsColumn.FixedWidth = true;
            this.colNowCount.Visible = true;
            this.colNowCount.VisibleIndex = 5;
            this.colNowCount.Width = 81;
            //
            // colValues
            //
            this.colValues.AppearanceHeader.Options.UseTextOptions = true;
            this.colValues.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colValues.Caption = "History";
            this.colValues.Name = "colValues";
            this.colValues.Visible = true;
            this.colValues.VisibleIndex = 6;
            this.colValues.Width = 403;
            //
            // colDeltaNowMin
            //
            this.colDeltaNowMin.AppearanceHeader.Options.UseTextOptions = true;
            this.colDeltaNowMin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDeltaNowMin.Caption = "Δ Now-Min";
            this.colDeltaNowMin.Name = "colDeltaNowMin";
            this.colDeltaNowMin.Visible = true;
            this.colDeltaNowMin.VisibleIndex = 4;
            this.colDeltaNowMin.Width = 67;
            //
            // panelControl2
            //
            this.panelControl2.Controls.Add(this.chkTrackObjectInstances);
            this.panelControl2.Controls.Add(this.btnPause);
            this.panelControl2.Controls.Add(this.lblLastRefreshTime);
            this.panelControl2.Controls.Add(this.lblLastRefreshTimeTitle);
            this.panelControl2.Controls.Add(this.lblHistory);
            this.panelControl2.Controls.Add(this.seHistoryLength);
            this.panelControl2.Controls.Add(this.btnClearStatistics);
            this.panelControl2.Controls.Add(this.btnRefreshNow);
            this.panelControl2.Controls.Add(this.btnClean);
            this.panelControl2.Controls.Add(this.lblUpdateSpeed);
            this.panelControl2.Controls.Add(this.lueUpdateSpeed);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(925, 58);
            this.panelControl2.TabIndex = 2;
            //
            // chkTrackObjectInstances
            //
            this.chkTrackObjectInstances.Location = new System.Drawing.Point(482, 39);
            this.chkTrackObjectInstances.Name = "chkTrackObjectInstances";
            this.chkTrackObjectInstances.Properties.Caption = "Track object at creation";
            this.chkTrackObjectInstances.Size = new System.Drawing.Size(159, 19);
            this.chkTrackObjectInstances.TabIndex = 13;
            this.chkTrackObjectInstances.ToolTip = "When disable, new instances of object won\'t be ever tracked, even after this opti" +
    "ons is enabled again";
            this.chkTrackObjectInstances.ToolTipTitle = "Warning";
            this.chkTrackObjectInstances.CheckedChanged += new System.EventHandler(this.chkTrackObjectInstances_CheckedChanged);
            //
            // btnPause
            //
            this.btnPause.Location = new System.Drawing.Point(358, 11);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(59, 23);
            this.btnPause.TabIndex = 12;
            this.btnPause.Text = "Pause";
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            //
            // lblLastRefreshTime
            //
            this.lblLastRefreshTime.Location = new System.Drawing.Point(284, 39);
            this.lblLastRefreshTime.Name = "lblLastRefreshTime";
            this.lblLastRefreshTime.Size = new System.Drawing.Size(0, 13);
            this.lblLastRefreshTime.TabIndex = 11;
            //
            // lblLastRefreshTimeTitle
            //
            this.lblLastRefreshTimeTitle.Location = new System.Drawing.Point(213, 39);
            this.lblLastRefreshTimeTitle.Name = "lblLastRefreshTimeTitle";
            this.lblLastRefreshTimeTitle.Size = new System.Drawing.Size(62, 13);
            this.lblLastRefreshTimeTitle.TabIndex = 10;
            this.lblLastRefreshTimeTitle.Text = "Last refresh:";
            //
            // lblHistory
            //
            this.lblHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHistory.AutoSize = true;
            this.lblHistory.Location = new System.Drawing.Point(479, 17);
            this.lblHistory.Name = "lblHistory";
            this.lblHistory.Size = new System.Drawing.Size(78, 13);
            this.lblHistory.TabIndex = 9;
            this.lblHistory.Text = "History length:";
            //
            // seHistoryLength
            //
            this.seHistoryLength.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.seHistoryLength.EditValue = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.seHistoryLength.Location = new System.Drawing.Point(563, 14);
            this.seHistoryLength.Name = "seHistoryLength";
            this.seHistoryLength.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.seHistoryLength.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.seHistoryLength.Properties.MinValue = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.seHistoryLength.Size = new System.Drawing.Size(60, 20);
            this.seHistoryLength.TabIndex = 8;
            this.seHistoryLength.EditValueChanged += new System.EventHandler(this.seHistoryLength_EditValueChanged);
            //
            // btnClearStatistics
            //
            this.btnClearStatistics.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearStatistics.Location = new System.Drawing.Point(629, 12);
            this.btnClearStatistics.Name = "btnClearStatistics";
            this.btnClearStatistics.Size = new System.Drawing.Size(139, 23);
            this.btnClearStatistics.TabIndex = 7;
            this.btnClearStatistics.Text = "Clear statistics";
            this.btnClearStatistics.Click += new System.EventHandler(this.btnClearStatistics_Click);
            //
            // btnRefreshNow
            //
            this.btnRefreshNow.Location = new System.Drawing.Point(213, 12);
            this.btnRefreshNow.Name = "btnRefreshNow";
            this.btnRefreshNow.Size = new System.Drawing.Size(139, 23);
            this.btnRefreshNow.TabIndex = 6;
            this.btnRefreshNow.Text = "Refresh now (F5)";
            this.btnRefreshNow.Click += new System.EventHandler(this.btnRefreshNow_Click);
            //
            // btnClean
            //
            this.btnClean.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClean.Location = new System.Drawing.Point(774, 12);
            this.btnClean.Name = "btnClean";
            this.btnClean.Size = new System.Drawing.Size(139, 23);
            this.btnClean.TabIndex = 5;
            this.btnClean.Text = "Collect unused objects";
            this.btnClean.Click += new System.EventHandler(this.btnClean_Click);
            //
            // lblUpdateSpeed
            //
            this.lblUpdateSpeed.AutoSize = true;
            this.lblUpdateSpeed.Location = new System.Drawing.Point(13, 17);
            this.lblUpdateSpeed.Name = "lblUpdateSpeed";
            this.lblUpdateSpeed.Size = new System.Drawing.Size(78, 13);
            this.lblUpdateSpeed.TabIndex = 4;
            this.lblUpdateSpeed.Text = "Update speed:";
            //
            // lueUpdateSpeed
            //
            this.lueUpdateSpeed.Location = new System.Drawing.Point(97, 14);
            this.lueUpdateSpeed.Name = "lueUpdateSpeed";
            this.lueUpdateSpeed.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueUpdateSpeed.Size = new System.Drawing.Size(110, 20);
            this.lueUpdateSpeed.TabIndex = 3;
            this.lueUpdateSpeed.EditValueChanged += new System.EventHandler(this.lueUpdateSpeed_EditValueChanged);
            this.lueUpdateSpeed.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.lueUpdateSpeed_EditValueChanging);
            //
            // tmrRefresh
            //
            this.tmrRefresh.Interval = 2000;
            this.tmrRefresh.Tick += new System.EventHandler(this.tmrRefresh_Tick);
            //
            // layoutControl1
            //
            this.layoutControl1.Controls.Add(this.lblGCTotalMemory);
            this.layoutControl1.Controls.Add(this.lblPrivateMemorySize);
            this.layoutControl1.Controls.Add(this.lblVirtualMemorySizePeak);
            this.layoutControl1.Controls.Add(this.lblVirtualMemorySize);
            this.layoutControl1.Controls.Add(this.lblWorkingSetPeak);
            this.layoutControl1.Controls.Add(this.btnClose);
            this.layoutControl1.Controls.Add(this.lblWorkingSet);
            this.layoutControl1.Controls.Add(this.gc);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 58);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(995, 393, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(925, 443);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            //
            // lblGCTotalMemory
            //
            this.lblGCTotalMemory.Location = new System.Drawing.Point(528, 418);
            this.lblGCTotalMemory.MinimumSize = new System.Drawing.Size(45, 0);
            this.lblGCTotalMemory.Name = "lblGCTotalMemory";
            this.lblGCTotalMemory.Size = new System.Drawing.Size(45, 13);
            this.lblGCTotalMemory.StyleController = this.layoutControl1;
            this.lblGCTotalMemory.TabIndex = 10;
            this.lblGCTotalMemory.Text = "N.A.";
            //
            // lblPrivateMemorySize
            //
            this.lblPrivateMemorySize.Location = new System.Drawing.Point(528, 401);
            this.lblPrivateMemorySize.MinimumSize = new System.Drawing.Size(45, 0);
            this.lblPrivateMemorySize.Name = "lblPrivateMemorySize";
            this.lblPrivateMemorySize.Size = new System.Drawing.Size(45, 13);
            this.lblPrivateMemorySize.StyleController = this.layoutControl1;
            this.lblPrivateMemorySize.TabIndex = 9;
            this.lblPrivateMemorySize.Text = "N.A.";
            //
            // lblVirtualMemorySizePeak
            //
            this.lblVirtualMemorySizePeak.Location = new System.Drawing.Point(333, 418);
            this.lblVirtualMemorySizePeak.MinimumSize = new System.Drawing.Size(45, 0);
            this.lblVirtualMemorySizePeak.Name = "lblVirtualMemorySizePeak";
            this.lblVirtualMemorySizePeak.Size = new System.Drawing.Size(45, 13);
            this.lblVirtualMemorySizePeak.StyleController = this.layoutControl1;
            this.lblVirtualMemorySizePeak.TabIndex = 8;
            this.lblVirtualMemorySizePeak.Text = "N.A.";
            //
            // lblVirtualMemorySize
            //
            this.lblVirtualMemorySize.Location = new System.Drawing.Point(333, 401);
            this.lblVirtualMemorySize.MinimumSize = new System.Drawing.Size(45, 0);
            this.lblVirtualMemorySize.Name = "lblVirtualMemorySize";
            this.lblVirtualMemorySize.Size = new System.Drawing.Size(45, 13);
            this.lblVirtualMemorySize.StyleController = this.layoutControl1;
            this.lblVirtualMemorySize.TabIndex = 7;
            this.lblVirtualMemorySize.Text = "N.A.";
            //
            // lblWorkingSetPeak
            //
            this.lblWorkingSetPeak.Location = new System.Drawing.Point(138, 418);
            this.lblWorkingSetPeak.MinimumSize = new System.Drawing.Size(45, 0);
            this.lblWorkingSetPeak.Name = "lblWorkingSetPeak";
            this.lblWorkingSetPeak.Size = new System.Drawing.Size(45, 13);
            this.lblWorkingSetPeak.StyleController = this.layoutControl1;
            this.lblWorkingSetPeak.TabIndex = 6;
            this.lblWorkingSetPeak.Text = "N.A.";
            //
            // btnClose
            //
            this.btnClose.Location = new System.Drawing.Point(756, 403);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(157, 22);
            this.btnClose.StyleController = this.layoutControl1;
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            //
            // lblWorkingSet
            //
            this.lblWorkingSet.Location = new System.Drawing.Point(138, 401);
            this.lblWorkingSet.Name = "lblWorkingSet";
            this.lblWorkingSet.Size = new System.Drawing.Size(22, 13);
            this.lblWorkingSet.StyleController = this.layoutControl1;
            this.lblWorkingSet.TabIndex = 4;
            this.lblWorkingSet.Text = "N.A.";
            //
            // layoutControlGroup1
            //
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.gc_LayoutItem,
            this.lblWorkingSet_LayoutItem,
            this.layoutControlItem2,
            this.lblPrivateMemorySize_LayoutItem,
            this.lblVirtualMemorySize_LayoutItem,
            this.lblWorkingSetPeak_LayoutItem,
            this.lblVirtualMemorySizePeak_LayoutItem,
            this.lblGCTotalMemory_LayoutItem,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem5});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(925, 443);
            this.layoutControlGroup1.TextVisible = false;
            //
            // gc_LayoutItem
            //
            this.gc_LayoutItem.Control = this.gc;
            this.gc_LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.gc_LayoutItem.Name = "gc_LayoutItem";
            this.gc_LayoutItem.Size = new System.Drawing.Size(905, 389);
            this.gc_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.gc_LayoutItem.TextVisible = false;
            //
            // lblWorkingSet_LayoutItem
            //
            this.lblWorkingSet_LayoutItem.Control = this.lblWorkingSet;
            this.lblWorkingSet_LayoutItem.Location = new System.Drawing.Point(0, 389);
            this.lblWorkingSet_LayoutItem.Name = "lblWorkingSet_LayoutItem";
            this.lblWorkingSet_LayoutItem.Size = new System.Drawing.Size(175, 17);
            this.lblWorkingSet_LayoutItem.Text = "Working Set:";
            this.lblWorkingSet_LayoutItem.TextSize = new System.Drawing.Size(123, 13);
            //
            // layoutControlItem2
            //
            this.layoutControlItem2.Control = this.btnClose;
            this.layoutControlItem2.Location = new System.Drawing.Point(744, 391);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(161, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(161, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(161, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            //
            // lblPrivateMemorySize_LayoutItem
            //
            this.lblPrivateMemorySize_LayoutItem.Control = this.lblPrivateMemorySize;
            this.lblPrivateMemorySize_LayoutItem.Location = new System.Drawing.Point(390, 389);
            this.lblPrivateMemorySize_LayoutItem.Name = "lblPrivateMemorySize_LayoutItem";
            this.lblPrivateMemorySize_LayoutItem.Size = new System.Drawing.Size(175, 17);
            this.lblPrivateMemorySize_LayoutItem.Text = "Private Memory Size:";
            this.lblPrivateMemorySize_LayoutItem.TextSize = new System.Drawing.Size(123, 13);
            //
            // lblVirtualMemorySize_LayoutItem
            //
            this.lblVirtualMemorySize_LayoutItem.Control = this.lblVirtualMemorySize;
            this.lblVirtualMemorySize_LayoutItem.Location = new System.Drawing.Point(195, 389);
            this.lblVirtualMemorySize_LayoutItem.Name = "lblVirtualMemorySize_LayoutItem";
            this.lblVirtualMemorySize_LayoutItem.Size = new System.Drawing.Size(175, 17);
            this.lblVirtualMemorySize_LayoutItem.Text = "Virtual Memory Size:";
            this.lblVirtualMemorySize_LayoutItem.TextSize = new System.Drawing.Size(123, 13);
            //
            // lblWorkingSetPeak_LayoutItem
            //
            this.lblWorkingSetPeak_LayoutItem.Control = this.lblWorkingSetPeak;
            this.lblWorkingSetPeak_LayoutItem.Location = new System.Drawing.Point(0, 406);
            this.lblWorkingSetPeak_LayoutItem.Name = "lblWorkingSetPeak_LayoutItem";
            this.lblWorkingSetPeak_LayoutItem.Size = new System.Drawing.Size(175, 17);
            this.lblWorkingSetPeak_LayoutItem.Text = "Working Set Peak:";
            this.lblWorkingSetPeak_LayoutItem.TextSize = new System.Drawing.Size(123, 13);
            //
            // lblVirtualMemorySizePeak_LayoutItem
            //
            this.lblVirtualMemorySizePeak_LayoutItem.Control = this.lblVirtualMemorySizePeak;
            this.lblVirtualMemorySizePeak_LayoutItem.Location = new System.Drawing.Point(195, 406);
            this.lblVirtualMemorySizePeak_LayoutItem.Name = "lblVirtualMemorySizePeak_LayoutItem";
            this.lblVirtualMemorySizePeak_LayoutItem.Size = new System.Drawing.Size(175, 17);
            this.lblVirtualMemorySizePeak_LayoutItem.Text = "Virtual Memory Size Peak:";
            this.lblVirtualMemorySizePeak_LayoutItem.TextSize = new System.Drawing.Size(123, 13);
            //
            // lblGCTotalMemory_LayoutItem
            //
            this.lblGCTotalMemory_LayoutItem.Control = this.lblGCTotalMemory;
            this.lblGCTotalMemory_LayoutItem.Location = new System.Drawing.Point(390, 406);
            this.lblGCTotalMemory_LayoutItem.Name = "lblGCTotalMemory_LayoutItem";
            this.lblGCTotalMemory_LayoutItem.Size = new System.Drawing.Size(175, 17);
            this.lblGCTotalMemory_LayoutItem.Text = "GC\'s Total Memory:";
            this.lblGCTotalMemory_LayoutItem.TextSize = new System.Drawing.Size(123, 13);
            //
            // emptySpaceItem1
            //
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(565, 389);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(179, 34);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            // emptySpaceItem2
            //
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(744, 389);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 1);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(161, 2);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            //
            // emptySpaceItem3
            //
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(744, 417);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 1);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(161, 6);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            //
            // emptySpaceItem4
            //
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(175, 389);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(20, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(20, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(20, 34);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            //
            // emptySpaceItem5
            //
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(370, 389);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(20, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(20, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(20, 34);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            //
            // FrmObjectTracking
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 501);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl2);
            this.Name = "FrmObjectTracking";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Object tracking manager";
            this.Load += new System.EventHandler(this.FrmObjectTracking_Load);
            this.Shown += new System.EventHandler(this.FrmObjectTracking_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTrackObjectInstances.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seHistoryLength.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueUpdateSpeed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWorkingSet_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrivateMemorySize_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVirtualMemorySize_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWorkingSetPeak_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVirtualMemorySizePeak_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGCTotalMemory_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected EnhancedGridControl gc;
        protected EnhancedGridView gv;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.Label lblUpdateSpeed;
        private DevExpress.XtraEditors.LookUpEdit lueUpdateSpeed;
        private System.Windows.Forms.Timer tmrRefresh;
        private DevExpress.XtraEditors.SimpleButton btnClean;
        private DevExpress.XtraEditors.SimpleButton btnRefreshNow;
        private TechnicalTools.UI.DX.EnhancedGridColumn colNamespace;
        private TechnicalTools.UI.DX.EnhancedGridColumn colTypeName;
        private TechnicalTools.UI.DX.EnhancedGridColumn colNowCount;
        private TechnicalTools.UI.DX.EnhancedGridColumn colMaxCount;
        private TechnicalTools.UI.DX.EnhancedGridColumn colValues;
        private DevExpress.XtraEditors.SimpleButton btnClearStatistics;
        private TechnicalTools.UI.DX.EnhancedGridColumn colMinCount;
        private System.Windows.Forms.Label lblHistory;
        private DevExpress.XtraEditors.SpinEdit seHistoryLength;
        private TechnicalTools.UI.DX.EnhancedGridColumn colDeltaMaxMin;
        private DevExpress.XtraEditors.LabelControl lblLastRefreshTime;
        private DevExpress.XtraEditors.LabelControl lblLastRefreshTimeTitle;
        private DevExpress.XtraEditors.SimpleButton btnPause;
        private TechnicalTools.UI.DX.EnhancedGridColumn colDeltaNowMin;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.LabelControl lblGCTotalMemory;
        private DevExpress.XtraEditors.LabelControl lblPrivateMemorySize;
        private DevExpress.XtraEditors.LabelControl lblVirtualMemorySizePeak;
        private DevExpress.XtraEditors.LabelControl lblVirtualMemorySize;
        private DevExpress.XtraEditors.LabelControl lblWorkingSetPeak;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.LabelControl lblWorkingSet;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem gc_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem lblWorkingSet_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem lblPrivateMemorySize_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem lblVirtualMemorySize_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem lblWorkingSetPeak_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem lblVirtualMemorySizePeak_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem lblGCTotalMemory_LayoutItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.CheckEdit chkTrackObjectInstances;
    }
}