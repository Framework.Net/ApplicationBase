﻿namespace TechnicalTools.UI.DX.Forms.Diagnostics
{
    partial class FrmDebugDatabaseTrace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcRequestInfo = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvRequestInfo = new TechnicalTools.UI.DX.EnhancedGridView();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnGetSqlPerformanceReport = new DevExpress.XtraEditors.SimpleButton();
            this.btnStopCapture = new DevExpress.XtraEditors.SimpleButton();
            this.btnStartCapture = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.gcRequestInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRequestInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            //
            // gcRequestInfo
            //
            this.gcRequestInfo.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcRequestInfo.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcRequestInfo.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcRequestInfo.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcRequestInfo.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcRequestInfo.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcRequestInfo.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcRequestInfo.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcRequestInfo.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcRequestInfo.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcRequestInfo.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcRequestInfo.EmbeddedNavigator.TextStringFormat = "Request {0} of {1} (visible) of {2} (total)";
            this.gcRequestInfo.Location = new System.Drawing.Point(12, 38);
            this.gcRequestInfo.MainView = this.gvRequestInfo;
            this.gcRequestInfo.Name = "gcRequestInfo";
            this.gcRequestInfo.Size = new System.Drawing.Size(803, 394);
            this.gcRequestInfo.TabIndex = 0;
            this.gcRequestInfo.UseEmbeddedNavigator = true;
            this.gcRequestInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRequestInfo});
            //
            // gvRequestInfo
            //
            this.gvRequestInfo.GridControl = this.gcRequestInfo;
            this.gvRequestInfo.Name = "gvRequestInfo";
            //
            // btnClear
            //
            this.btnClear.Location = new System.Drawing.Point(12, 12);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(128, 22);
            this.btnClear.StyleController = this.layoutControl1;
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            //
            // layoutControl1
            //
            this.layoutControl1.Controls.Add(this.btnGetSqlPerformanceReport);
            this.layoutControl1.Controls.Add(this.btnStopCapture);
            this.layoutControl1.Controls.Add(this.btnStartCapture);
            this.layoutControl1.Controls.Add(this.gcRequestInfo);
            this.layoutControl1.Controls.Add(this.btnClear);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(815, 246, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(827, 444);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            //
            // btnGetSqlPerformanceReport
            //
            this.btnGetSqlPerformanceReport.Location = new System.Drawing.Point(674, 12);
            this.btnGetSqlPerformanceReport.Name = "btnGetSqlPerformanceReport";
            this.btnGetSqlPerformanceReport.Size = new System.Drawing.Size(141, 22);
            this.btnGetSqlPerformanceReport.StyleController = this.layoutControl1;
            this.btnGetSqlPerformanceReport.TabIndex = 5;
            this.btnGetSqlPerformanceReport.Text = "Get Sql performance report";
            this.btnGetSqlPerformanceReport.Click += new System.EventHandler(this.btnGetSqlPerformanceReport_Click);
            //
            // btnStopCapture
            //
            this.btnStopCapture.Location = new System.Drawing.Point(398, 12);
            this.btnStopCapture.Name = "btnStopCapture";
            this.btnStopCapture.Size = new System.Drawing.Size(128, 22);
            this.btnStopCapture.StyleController = this.layoutControl1;
            this.btnStopCapture.TabIndex = 4;
            this.btnStopCapture.Text = "Pause Capture";
            this.btnStopCapture.Click += new System.EventHandler(this.btnStopCapture_Click);
            //
            // btnStartCapture
            //
            this.btnStartCapture.Location = new System.Drawing.Point(222, 12);
            this.btnStartCapture.Name = "btnStartCapture";
            this.btnStartCapture.Size = new System.Drawing.Size(128, 22);
            this.btnStartCapture.StyleController = this.layoutControl1;
            this.btnStartCapture.TabIndex = 3;
            this.btnStartCapture.Text = "Start Capture";
            this.btnStartCapture.Click += new System.EventHandler(this.btnStartCapture_Click);
            //
            // layoutControlGroup1
            //
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem1,
            this.layoutControlItem3,
            this.emptySpaceItem2,
            this.layoutControlItem4,
            this.emptySpaceItem3,
            this.layoutControlItem5});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(827, 444);
            this.layoutControlGroup1.TextVisible = false;
            //
            // layoutControlItem1
            //
            this.layoutControlItem1.Control = this.btnClear;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(132, 26);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(132, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(132, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            //
            // layoutControlItem2
            //
            this.layoutControlItem2.Control = this.gcRequestInfo;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(807, 398);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            //
            // emptySpaceItem1
            //
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(518, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(144, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            // layoutControlItem3
            //
            this.layoutControlItem3.Control = this.btnStartCapture;
            this.layoutControlItem3.Location = new System.Drawing.Point(210, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(132, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(132, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(132, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            //
            // emptySpaceItem2
            //
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(132, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(78, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            //
            // layoutControlItem4
            //
            this.layoutControlItem4.Control = this.btnStopCapture;
            this.layoutControlItem4.Location = new System.Drawing.Point(386, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(132, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(132, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(132, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            //
            // emptySpaceItem3
            //
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(342, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(44, 26);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            //
            // layoutControlItem5
            //
            this.layoutControlItem5.Control = this.btnGetSqlPerformanceReport;
            this.layoutControlItem5.Location = new System.Drawing.Point(662, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(145, 26);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            //
            // FrmDebugDatabaseTrace
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 444);
            this.Controls.Add(this.layoutControl1);
            this.Name = "FrmDebugDatabaseTrace";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.FormDebugClassHierarchy_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcRequestInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRequestInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private EnhancedGridControl gcRequestInfo;
        private EnhancedGridView gvRequestInfo;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton btnStopCapture;
        private DevExpress.XtraEditors.SimpleButton btnStartCapture;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.SimpleButton btnGetSqlPerformanceReport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
    }
}