﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Diagnostics;
using TechnicalTools.UI.DX.Helpers;


namespace TechnicalTools.UI.DX
{
    public partial class GenericShowAndSendData : XtraForm
    {
        readonly IList _datasource;
        readonly Action _importAction;


        public GenericShowAndSendData(IList datasource = null, Action importAction = null)
        {
            _datasource = datasource;
            _importAction = importAction;
            InitializeComponent();

            btnImport.Enabled = _importAction != null;

            if (DesignTimeHelper.IsInDesignMode)
                return;
        }

        public GenericShowAndSendData WithDetails(IEnumerable<GridView_DetailViewManager.DetailView> detailViews)
        {
            foreach (var dv in detailViews)
                gvData.DetailViewManager.Add(dv);
            return this;
        }
        public GenericShowAndSendData WithDetails<T>(Dictionary<string, Func<T, IReadOnlyCollection<object>>> getDetails)
        {
            foreach (var desc in getDetails)
                gvData.DetailViewManager.Add(BuildDetailView(desc));
            return this;
        }


        protected virtual DetailView<T> BuildDetailView<T>(KeyValuePair<string, Func<T, IReadOnlyCollection<object>>> desc)
        {
            return new DetailView<T>(desc.Key, desc.Value);
        }

        protected class DetailView<T> : GridView_DetailViewManager.DetailView
        {
            public DetailView(string name, Func<T, IReadOnlyCollection<object>> getChildren)
            {
                _name = name;
                _getChildren = getChildren;
            }
            readonly string _name;
            readonly Func<T, IReadOnlyCollection<object>> _getChildren;

            public override bool HasMeaningAndIsEnabledFor(GridView sender, int rowHandle)
            {
                return true;
            }

            public override string GetName(GridView sender, int rowHandle)
            {
                return _name;
            }

            public override bool? IsEmpty(GridView view, int rowHandle)
            {
                var obj = view.GetRow(rowHandle);
                return _getChildren((T)obj).Count == 0;
            }


            public override Task<IReadOnlyCollection<object>> GetChildList(GridView view, int rowHandle)
            {
                var obj = view.GetRow(rowHandle);
                return Task.FromResult(_getChildren((T)obj));
            }

            public override void Expanded(GridView sender, int rowHandle, BaseView clonedDetailView)
            {
                base.Expanded(sender, rowHandle, clonedDetailView);
                var gvClonedDetailView = clonedDetailView as GridView;
                if (gvClonedDetailView != null)
                {
                    SetDefaultFormatting(gvClonedDetailView);
                    GridViewHelper.HideTechnicalProperties(gvClonedDetailView);
                    gvClonedDetailView.BestFitMaxRowCount = 100;
                    gvClonedDetailView.BestFitColumns();
                }
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            gcData.DataSource = _datasource;
            SetDefaultFormatting(gvData);
            GridViewHelper.HideTechnicalProperties(gvData);
            gvData.BestFitMaxRowCount = 100;
            gvData.BestFitColumns();

            if (_datasource.Count == 1)
                gvData.ExpandMasterRow(0);
        }

        static void SetDefaultFormatting(GridView view)
        {
            view.OptionsBehavior.Editable = false;
            view.OptionsView.ColumnAutoWidth = false;
            view.OptionsView.AllowCellMerge = true;
            view.OptionsCustomization.AllowRowSizing = true;
            GridViewHelper.SetDefaultColumnFormatting(view);
        }

        private void btnDiscard_Click(object sender, EventArgs e)
        {
            if (btnImport.Enabled)
            {
                BeginInvoke((Action)(() => SetFocus(IntPtr.Zero))); // Remove the focus on the following messagebox
                DialogResult answer;
                using (new UI.Tools.CenterWinDialog(this))
                    answer = XtraMessageBox.Show(this, "Are you sure to discard these data?", "Please confirm", MessageBoxButtons.YesNo);
                if (answer != DialogResult.Yes)
                    return;
            }
            Close();
        }
        void DoClose()
        {
            if (Modal) DialogResult = DialogResult.Cancel;
            else Close();
        }
        [DllImport("user32.dll")]
        static extern IntPtr SetFocus(IntPtr hWnd);

        private void btnImport_Click(object sender, EventArgs e)
        {
            Debug.Assert(_importAction != null);

            BeginInvoke((Action)(() => SetFocus(IntPtr.Zero))); // Remove the focus on the following messagebox
            DialogResult answer;
            using (new UI.Tools.CenterWinDialog(this))
                answer = XtraMessageBox.Show(this, "Are you sure to send these data?", "Please confirm", MessageBoxButtons.YesNo);
            if (answer != DialogResult.Yes)
                return;
            if (null == ExceptionManager.Instance.IgnoreException(_importAction))
                DoClose();
        }


    }
}
