﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    /// <summary>
    /// <para>Permet la configuration d'une colonne d'une gridview pour un enum</para>
    /// <para>Utilisation :
    /// myGridColumn.ConfigureForEnum&lt;yourEnumType&gt;();</para>
    ///
    /// <para>Gère plusieurs choses :
    /// - Traduction d'un id en nom metier (fait appel à la methode d'extension GetDescription sur les valeurs des enums <see cref="Enum_Extensions.GetDescription">voir ici</see>)
    /// - Traduction egalement dans la row filter
    /// - Traduction dans la popup filter (Si OptionsView.ShowAutoFilterRow = true)
    /// - Le tri se fait dans l'ordre des chaines de caractères et non pas des id</para>
    /// </summary>
    public static class GridColumn_ExtensionsForEnums
    {
        interface IHelperBase { void Uninstall(); }
        class Helper<TEnum> : IHelperBase
            where TEnum : struct
        {
            readonly GridColumn _column;
            RepositoryItemLookUpEdit _repoEditor;
            readonly bool _as_int;
            readonly IEnumerable<TEnum> _allowedEnums; // We have to keep this as ienumerable because source can be reevaluated for each row
            readonly Func<object, IEnumerable<TEnum>> _getAllowedEnums;
            readonly bool _sorted_by_caption;

            public Helper(GridColumn column, bool as_int, IEnumerable<TEnum> allowedEnums, Func<object, IEnumerable<TEnum>> getAllowedEnums, bool sorted_by_caption = true)
            {
                _column = column;
                _as_int = as_int;
                _allowedEnums = allowedEnums;
                _getAllowedEnums = getAllowedEnums;
                _sorted_by_caption = sorted_by_caption;
            }

            public void Install()
            {
                _column.View.CustomColumnDisplayText += View_CustomColumnDisplayText;

                _column.SortMode   = ColumnSortMode.DisplayText;
                _column.FilterMode = ColumnFilterMode.DisplayText;

                _repoEditor = new RepositoryItemLookUpEdit();
                _column.View.GridControl.RepositoryItems.AddRange(new RepositoryItem[] { _repoEditor });
                _repoEditor.AutoHeight = false;
                _repoEditor.Name = _column.View.Name + "_autoRepoForEnum_" + typeof(TEnum).Name;

                if (_as_int)
                    _repoEditor.FillWithEnumValuesAsInt<TEnum>();
                else if (_allowedEnums != null)
                        _repoEditor.FillWithEnumValues((IReadOnlyCollection<TEnum>)_allowedEnums.ToList());
                else if (_getAllowedEnums != null)
                {
                    // If list is not constant and is a IEnumerable that depend of some business rule, we have to reevaluate the container !
                    ((GridView)_column.View).CustomRowCellEdit += View_CustomRowCellEdit;
                    ((GridView)_column.View).CustomRowCellEditForEditing += View_CustomRowCellEditForEditing;
                }
                else
                    _repoEditor.FillWithEnumValues<TEnum>();

                _repoEditor.EditValueChanged += RepositoryEditor_EditValueChanged;

                _column.ColumnEdit = _repoEditor;
                // How to make the width of the GridLookUpEdit dropdown grid be the same as that of the editor
                // http://www.devexpress.com/Support/Center/Example/Details/E1574
                //static void gridLookUpEdit1_QueryPopUp(object sender, CancelEventArgs e)
                //{
                //    GridLookUpEdit editor = (GridLookUpEdit) sender;
                //    RepositoryItemGridLookUpEdit properties = editor.Properties;
                //    properties.PopupFormSize = new Size(editor.Width - 4, properties.PopupFormSize.Height);
                //}
            }
            #region Idée de RepositoryItem efficace dansu ne gridview pour les ligne qui n'ont pas le focus (ie : non editable)
            // TODO : Mettre un tag pour dire que l'editeur associé est ConstTextEdit
            //class ConstRepositoryItemTextEdit : RepositoryItemTextEdit
            //{

            //}
            //class ConstTextEdit : DevExpress.XtraEditors.TextEdit
            //{
            //    public ConstTextEdit(Func<object, string> getValue)
            //    {
            //        _getValue = getValue;
            //    }
            //    object _getValue;

            //    public override string Text
            //    {
            //        get
            //        {
            //            if (EditValue == null)
            //                return null;
            //            return _getValue(EditValue);
            //        }
            //        set
            //        {
            //            throw new InvalidOperationException("ConstTextEdit is not intended to be editable!");
            //        }
            //    }
            //}
            #endregion

            void View_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
            {
                if (e.Column == _column)
                {
                    e.RepositoryItem = new RepositoryItemTextEdit() { NullText = null };
                    Debug.Assert(e.RepositoryItem.BestFitWidth == -1); // Could prevent BestFitColumns to work
                }
            }
            void View_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
            {
                if (e.RowHandle == GridControl.InvalidRowHandle)
                    return;
                if (e.RowHandle == GridControl.AutoFilterRowHandle)
                    return;
                if (e.Column == _column)
                {
                    var view = (GridView)sender;
                    var obj = view.GetRow(e.RowHandle);
                    _repoEditor.FillWithEnumValues(_getAllowedEnums(obj).ToList(), _sorted_by_caption);
                    _repoEditor.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
                    e.RepositoryItem = _repoEditor;
                    Debug.Assert(e.RepositoryItem.BestFitWidth == -1); // Could prevent BestFitColumns to work
                }
            }

            public void Uninstall()
            {
                if (_column.View != null)
                {
                    _column.View.CustomColumnDisplayText -= View_CustomColumnDisplayText;
                    _column.View.GridControl.RepositoryItems.Remove(_repoEditor);
                    ((GridView)_column.View).CustomRowCellEdit -= View_CustomRowCellEdit;
                    ((GridView)_column.View).CustomRowCellEditForEditing -= View_CustomRowCellEditForEditing;

                }
                _column.ColumnEdit = null;
                _repoEditor = null;
            }

            // Cet évènement a l'avantage, contrairement à CustomDrawCell, de convertir également les valeurs dans le PopupFilter
            // (la popup apparait quand on clique sur le petit signe en forme de filtre qui apparait quand la souris passe sur le titre de la colone)
            void View_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
            {
                if (e.Column == _column &&
                    e.Value != null && // ce cas se presente quand la row filter est visible
                    (e.Value.GetType().IsEnum || e.Value.GetType() == typeof(Enum) || e.Value is int)) // Idem si l'objet = chaine entrée par l'utilisateur
                    e.DisplayText = ((TEnum)e.Value as Enum).GetDescription();
            }

            void RepositoryEditor_EditValueChanged(object sender, EventArgs e)
            {
                // Dès que l'utilisateur change la valeur dans la combobox, la valeur doit petre setté et validé dans le modèle
                // C'est le comportement le plus intuitif pour l'utilisateur
                // Pour ce faire on doit appeler PostEditor()

                // Mais au cas ou la validation echoue, on doit laisser le message d'erreur apparaitre
                // Donc on intercepte toutes les erreurs
                bool isValid = true;
                void onInvalidValueException(object _, InvalidValueExceptionEventArgs __) => isValid = false;
                _column.View.InvalidValueException += onInvalidValueException;
                try
                {
                    _column.View.PostEditor();
                    if (_getAllowedEnums != null) // Si il s'agissait d'un ensemble dynamique
                    {
                        if (isValid) // et que l'assignation a ete validée
                            _column.View.CloseEditor(); // Alors on force la fermeture du control, ainsi View_CustomRowCellEditForEditing sera rexecuté et l'affichage (datasource) raffraichit
                    }
                }
                finally
                {
                    _column.View.InvalidValueException -= onInvalidValueException;
                }
            }
        }

        static readonly Dictionary<GridColumn, IHelperBase> Helpers = new Dictionary<GridColumn, IHelperBase>();
        public static void Install<TEnum>(GridColumn col, bool as_int, IEnumerable<TEnum> allowedEnums, Func<object, IEnumerable<TEnum>> getAllowedEnums, bool sorted_by_caption = true)
            where TEnum : struct
        {
            Debug.Assert(typeof(TEnum).IsEnum, "FillWithEnumValues works only for enum type !");
            if (Helpers.ContainsKey(col))
                return; // déjà installé, donc on quitte

            var helper = new Helper<TEnum>(col, as_int, allowedEnums, getAllowedEnums, sorted_by_caption);
            Helpers.Add(col, helper);
            col.Disposed += col_Disposed;
            helper.Install();
        }

        static void col_Disposed(object sender, EventArgs e)
        {
            ((GridColumn)sender).Disposed -= col_Disposed;
            Uninstall((GridColumn)sender);
        }
        // Au cas ou quelqu'un chercherait une aide dans la colonne
        public static GridColumn ConfigureForEnum<TEnum>(this GridColumn col)
            where TEnum : struct
        {
            Install<TEnum>(col, false, null, null);
            return col;
        }
        public static GridColumn ConfigureForEnum<TEnum>(this GridColumn col, IEnumerable<TEnum> onlyTheseValues)
            where TEnum : struct
        {
            Install(col, false, onlyTheseValues, null);
            return col;
        }
        public static GridColumn ConfigureForEnum<TItem, TEnum>(this GridColumn col, Func<TItem, IEnumerable<TEnum>> getAllowedEnums)
            where TEnum : struct
        {
            Install(col, false, null, o => getAllowedEnums((TItem)o), false);
            return col;
        }
        public static BandedGridColumn ConfigureForEnum<TEnum>(this BandedGridColumn col, IEnumerable<TEnum> onlyTheseValues)
            where TEnum : struct
        {
            Install(col, false, onlyTheseValues, null);
            return col;
        }
        public static BandedGridColumn ConfigureForEnum<TItem, TEnum>(this BandedGridColumn col, Func<TItem, IEnumerable<TEnum>> getAllowedEnums)
            where TEnum : struct
        {
            Install(col, false, null, o => getAllowedEnums((TItem)o), false);
            return col;
        }
        public static BandedGridColumn ConfigureForEnum<TEnum>(this BandedGridColumn col)
            where TEnum : struct
        {
            Install<TEnum>(col, false, null, null);
            return col;
        }

        // Au cas ou quelqu'un chercherait une aide dans la colonne
        public static GridColumn ConfigureForEnumAsInt<TEnum>(this GridColumn col)
            where TEnum : struct
        {
            Install<TEnum>(col, true, null, null);
            return col;
        }
        public static BandedGridColumn ConfigureForEnumAsInt<TEnum>(this BandedGridColumn col)
            where TEnum : struct
        {
            Install<TEnum>(col, true, null, null);
            return col;
        }

        public static void Uninstall(GridColumn col)
        {
            if (!Helpers.ContainsKey(col))
                return; // Rien à faire car déjà désinstallé

            Helpers[col].Uninstall();
            Helpers.Remove(col);
            col.Disposed -= col_Disposed;
        }


    }
}
