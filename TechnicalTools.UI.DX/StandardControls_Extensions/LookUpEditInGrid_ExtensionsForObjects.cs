﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;

using TechnicalTools.Tools;

using Internal = TechnicalTools.UI.DX.LookUpEdit_ExtensionsForObjects;


namespace TechnicalTools.UI.DX
{
    /// <summary>
    /// Ce Helpeur expose simplifie le remplissage d'un RepositoryItemLookUpEdit dans un GridControl.
    /// </summary>
    public static class LookUpEditInGrid_ExtensionsForObjects
    {
        #region Generic for Objects

        #region RepositoryItemLookUpEdit

        public static void FillWithObjects<TObject, TId>(this RepositoryItemLookUpEdit lue, IEnumerable<TObject> objects, Func<TObject, string> get_text, Func<TObject, TId> get_id, bool with_empty = false)
            where TObject : class
        {
            Debug.Assert(get_text != null);
            Debug.Assert(get_id != null);

            var choices = new Internal.ItemList<TObject, TId>(objects.Select(obj => new Item<TObject, TId>() { Object = obj, Id = get_id(obj), Caption = get_text(obj) })
                                                                     .OrderBy(kvp => kvp.Caption));
            if (with_empty)
                choices.Insert(0, new Item<TObject, TId>() { Object = null, Id = default(TId), Caption = "" });
            ConfigureRepositoryItemLookUpEditAsDropDownList(lue, choices);
        }

        #endregion RepositoryItemLookUpEdit

        internal static void ConfigureRepositoryItemLookUpEditAsDropDownList<TObject, TId>(RepositoryItemLookUpEdit lue, Internal.ItemList<TObject, TId> source, bool sort_by_caption = true)
            where TObject : class
        {
            lue.DisplayMember = GetMemberName.For((Item<TObject, TId> item) => item.Caption);
            lue.ValueMember = GetMemberName.For((Item<TObject, TId> item) => item.Id);
            //lue.AllowNullInput = DefaultBoolean.False; // Empeche l'utilisateur de faire "Ctrl - Suppr", on gère le cas de la valeur vide dans la methode FillWith
            lue.NullText = "";
            lue.DataSource = source;
            var col = new LookUpColumnInfo(lue.DisplayMember);
            if (sort_by_caption)
                col.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            lue.Columns.Clear();
            lue.Columns.Add(col);
            lue.ShowHeader = false;
            lue.ShowFooter = false;
            lue.DropDownRows = Math.Min(Math.Max(source.Count, 1), 50);
        }

        // Cette classe est inutile, elle sert juste a faire un alias
        internal class Item<TObject, TId> : LookUpEdit_ExtensionsForObjects.Item<TObject, TId>
            where TObject : class
        {
        }

        #endregion Generic for Objects
    }

}
