﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;

using DevExpress.XtraEditors;

using Internal = TechnicalTools.UI.DX.LookUpEdit_ExtensionsForObjects;


namespace TechnicalTools.UI.DX
{
    /// <summary>
    /// Ce Helpeur permet de binder des enums a un lookupedit
    /// Une fois que vous optez pour l'utilisationc de ce helpeur, vous ne DEVEZ plus gérer manuellement
    /// les propriétés EditValue, SelectedIndex etc de la LookUpEdit.
    /// Ce helpeur encapsule tout ce qui est nécessaire (sinon ajouter une nouvelle méthode !).
    /// </summary>
    public static class LookUpEdit_ExtensionsForEnums
    {
        #region Generic about Static Enums

        /// <summary>
        /// Recupère une liste de correspondance entre les valeurs d'un type Enum et leur description.
        /// Cette liste est particulièrement utile pour les ComboBox (avec DisplayMember assigné à "Value").
        /// <see>FillWithEnumValues&lt;TEnum&gt;<cref>FillWithEnumValues{TEnum}</cref></see>
        /// </summary>
        internal static ReadOnlyCollection<Internal.Item<Enum, long>> GetEnumItemList<TEnum>(IReadOnlyCollection<TEnum> allowed_values, bool sort_by_caption = true)
        {
            Type t = typeof (TEnum);
            Debug.Assert(t.IsEnum, "GetEnumItemList works only for enum type !");

            if (allowed_values == null && EnumListCaches.ContainsKey(t))
                return ((List<Internal.Item<Enum, long>>)EnumListCaches[t]).AsReadOnly();

            var source = allowed_values ?? Enum.GetValues(t).Cast<TEnum>();
            var tmp = source.Cast<Enum>()
                            .Distinct() // Required because if enum type is flags, GetValues can returns duplicates on System.Reflection.MethodAttributes
                            .Select(e => new Internal.Item<Enum, long>
                            {
                                Id = t.GetEnumUnderlyingType() == typeof(ulong) ? (long)Convert.ToUInt64(e) : Convert.ToInt64(e),
                                Object = e,
                                Caption = e.GetDescription()
                            });
            var lst = (sort_by_caption ? tmp.OrderBy(kvp => kvp.Caption) : tmp).ToList();

            // Important : On DOIT retourner une nouvelle instance autours de la liste en cache.
            //             Si on retournait directement la liste (ou la meme instance)
            //             et si plusieur composant de selection dans un ecran utilisent cette liste, alors
            //             les selections seraient synchronizées !
            // TODO : Il faudra a la place creer une BindingSOurce qui encapsule la list original => et utiliser la BindingSource comme datasource,
            // c'est ce que semble faire les composants par defaut quand on leur donne une list (list normal enumerable ou bindinglist)
            // LA synchronisation de la selection est stocke dans BindingSource (cf http://www.devexpress.com/Support/Center/Question/Details/Q391047) et http://blogs.msdn.com/b/bethmassi/archive/2007/09/19/binding-multiple-comboboxes-to-the-same-datasource.aspx)
            // Il faudrait a priori juste retourner new BindingSource(lst, null);
            //
            //             Plus d'info : http://stackoverflow.com/questions/4861520/share-combobox-datasource
            if (allowed_values == null)
                EnumListCaches.Add(t, lst);
            return lst.AsReadOnly();
        }

        internal static readonly Dictionary<Type, IList> EnumListCaches = new Dictionary<Type, IList>();

        internal static Internal.ItemList<Enum, long> GetSpecificSourceFor<TEnum>(IReadOnlyCollection<TEnum> allowed_values, bool sort_by_caption = true)
            where TEnum : struct
        {
            Debug.Assert(allowed_values?.Count > 0);
            ReadOnlyCollection<Internal.Item<Enum, long>> source = GetEnumItemList(allowed_values, sort_by_caption);

            var lst = new Internal.ItemList<Enum, long>() { IdType = typeof(int), ObjectType = typeof(TEnum) };
            lst.AddRange(source);
            return lst;
        }
        internal static Internal.ItemList<Enum, long> GetSpecificSourceExcept<TEnum>(params TEnum[] removed_values)
            where TEnum : struct
        {
            ReadOnlyCollection<Internal.Item<Enum, long>> source = GetEnumItemList<TEnum>(null);

            var lst = new Internal.ItemList<Enum, long>() { IdType = typeof(int), ObjectType = typeof(TEnum) };
            if (removed_values?.Length > 0)
                foreach (Internal.Item<Enum, long> pair in source)
                {
                    bool remain = true;
                    foreach (TEnum removed in removed_values)
                        if (pair.Id == Convert.ToInt64(removed))
                        {
                            remain = false;
                            break;
                        }
                    if (remain)
                        lst.Add(pair);
                }
            else
                lst.AddRange(source);
            return lst;
        }

        #region LookUpEdit (DevExpress)

        // Exemple d'utilisation des méthodes ci dessous :
        //   LookUpEdit lue = new LookUpEdit();
        //   lue.FillWithEnumValuesExceptFor<MyEnumType>(MyEnumType.Undefined) // la valeur indefini ne sera pas utilisé
        //   lue.UnselectValue()
        // Event OnChange :
        //  // Recupere la valeur d'enum slectionné ou bien MyEnumType.Undefined si le choix est vide
        //   MessageBox.Show("You have selected : " + cmb.SelectedValueAsEnum<MyEnumType>(MyEnumType.Undefined).GetDescription());

        /// <summary>
        /// Remplit un LookUpEdit avec les valeurs d'un type Enum
        /// </summary>
        /// <param name="lue"></param>
        /// <param name="removed_values">Valeurs à ignorer</param>
        public static void FillWithEnumValuesExceptFor<TEnum>(this LookUpEdit lue, params TEnum[] removed_values)
            where TEnum : struct
        {
            FillWithEnumValuesExceptFor(lue, false, removed_values);
        }
        public static void FillWithEnumValuesExceptFor<TEnum>(this LookUpEdit lue, bool with_empty, params TEnum[] removed_values)
            where TEnum : struct
        {
            Debug.Assert(typeof(TEnum).IsEnum, "FillWithEnumValuesExceptFor works only for enum type !");
            // Ces deux lignes sont a virer si ca fait un ptit moment qu'ele sont commenté (normalement elles sont inutiles car c'est ce que fait le fill en dessous !)
            //lue.ConfigureLookUpEditAsDropDownList(GetSpecificSource(removed_values));
            //lue.Install_InitializationChecks();

            if (removed_values != null && removed_values.Length != 0)
            {
                var remaining_values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>()
                                           .Distinct() // Required because if enum type is flags, GetValues can returns duplicates on System.Reflection.MethodAttributes
                                           .Except(removed_values)
                                           .ToArray();
                FillWithEnumValues(lue, with_empty, remaining_values);
            }
            else
                FillWithEnumValues<TEnum>(lue, with_empty);
        }

        public static void FillWithEnumValues<TEnum>(this LookUpEdit lue, IEnumerable<TEnum> values, bool with_empty = false)
            where TEnum : struct
        {
            FillWithEnumValues(lue, with_empty, values.ToArray());
        }
        public static void FillWithEnumValues<TEnum>(this LookUpEdit lue, params TEnum[] values)
            where TEnum : struct
        {
            FillWithEnumValues(lue, false, values);
        }

        public static void FillWithEnumValues<TEnum>(this LookUpEdit lue, bool with_empty, IEnumerable<TEnum> values)
            where TEnum : struct
        {
            FillWithEnumValues(lue, with_empty, values.ToArray());
        }
        public static void FillWithEnumValues<TEnum>(this LookUpEdit lue, bool with_empty, params TEnum[] values)
            where TEnum : struct
        {
            Debug.Assert(typeof(TEnum).IsEnum, "FillWithEnumValues works only for enum type !");

            CheckEnum?.Invoke(typeof (TEnum));

            TEnum? value_to_reselect = null;

            if (lue.IsHandledByExtensions() && lue.EditValue != null && lue.EditValue != DBNull.Value)
            {
                var lst = lue.Properties.DataSource as Internal.IItemList;
                Debug.Assert(lst != null);
                if (lue.EditValue != lst.EmptyItem)
                {
                    Enum previous_value = ((Internal.Item)lue.EditValue).GetObject() as Enum;
                    Debug.Assert(previous_value != null);
                    if (previous_value.GetType() == typeof(TEnum))
// ReSharper disable PossibleInvalidCastException
                        value_to_reselect = (TEnum)(object)previous_value;
// ReSharper restore PossibleInvalidCastException
                }
            }


            if (values != null && values.Length > 0)
            {
                var all = Enum.GetValues(typeof(TEnum)).Cast<TEnum>()
                              .Distinct() // Required because if enum type is flags, GetValues can returns duplicates on System.Reflection.MethodAttributes
                              .Except(values)
                              .ToArray();
                values = all;
            }
            Internal.ItemList<Enum, long> choices = GetSpecificSourceExcept(values);

            if (with_empty)
            {
                long nullValue = 0;
                while (choices.Any(i => i.Id == nullValue)) // thus avoids value duplication in LookUpEdit items
                {
                    nullValue--;
                }
                var item = new Internal.Item<Enum, long>() { Id = nullValue, Object = null, Caption = "" };
                choices.EmptyItem = item;
                choices.Insert(0, item);
            }

            lue.ConfigureLookUpEditAsDropDownList(choices);
            lue.Install_InitializationChecks();

            if (value_to_reselect.HasValue)
                SelectedValueAsEnumSet(lue, value_to_reselect.Value, true);
        }


        public static Action<Type> CheckEnum;

        static void LookUpEditChecksForUse(this LookUpEdit lue, string action_name, Type enum_type)
        {
            if (enum_type != null)
                Debug.Assert(enum_type.IsEnum,
                             string.Format("Method \"" + action_name + " works only for Enum type ! Cannot use {0} on lookupedit {1} with type {2}", action_name, lue.Name, enum_type.Name));
            var lst = lue.Properties.DataSource as Internal.ItemList<Enum, long>;
            Debug.Assert(lst != null,
                $"Cannot use method \"{action_name}\" on lookupedit {lue.Name} because no extensions method named FillWithEnumsValues* has been used on it!");
            Debug.Assert(enum_type == null || lst.ObjectType == enum_type,
                $"Cannot use {(enum_type != null ? enum_type.Name : "")} as enum type in lookupedit, because lookupedit {lue.Name} has been filled with enum of type {lst.ObjectType.Name}!");
        }

        public static TEnum? NullValue<TEnum>(this LookUpEdit lue)
            where TEnum : struct
        {
            lue.LookUpEditChecksForUse("NullValue", typeof(TEnum));
            var lst = (lue.Properties.DataSource as Internal.IItemList);
            Debug.Assert(lst != null);
            Debug.Assert(lst.AllowEmpty, "Impossible pour la lookupedit " + lue.Name + " de récupérer la valeur de substitution lorsqu'aucune valeur n'est selectionnée " +
                                          "car vous avez indiqué ne pas autoriser de selection vide lors de son remplissage (argument with_empty : false) !");
            Debug.Assert(lst.EmptyItem != null);
            return (TEnum?)lst.EmptyItem.GetObject();
        }
        public static void NullValueSet<TEnum>(this LookUpEdit lue, TEnum null_value)
            where TEnum : struct
        {
            NullValueSet(lue, (TEnum?)null_value);
        }
        public static void NullValueSet<TEnum>(this LookUpEdit lue, TEnum? null_value)
            where TEnum : struct
        {
            lue.LookUpEditChecksForUse("NullValueSet", typeof(TEnum));
            var lst = lue.Properties.DataSource as Internal.ItemList<Enum, long>;
            Debug.Assert(lst != null);
            //Debug.Assert(lst.AllowEmpty, "Impossible pour la lookupedit " + lue.Name + " de définir la valeur de substitution lorsqu'aucune valeur n'est selectionnée " +
            //                              "car vous avez indiqué ne pas autoriser de selection vide lors de son remplissage (argument with_empty : false) !");
            if (lst.EmptyItem == null)
                lst.EmptyItem = new Internal.Item<Enum, long>() { Caption = lue.Properties.NullText };
            lst.EmptyItem.SetObject(null);
            lst.EmptyItem.SetId(null);
            if (null_value.HasValue)
            {
                lst.EmptyItem.SetObject(null_value);
                lst.EmptyItem.SetId(null_value);
            }
        }

        /// <summary>
        /// Recupere une valeur typé de l'enum selectionné.
        /// Si la valeur est vide, une exception est levée.
        /// Si vous ne souhaitez pas avoir d'exception, utiliseé la urcharge de cette methode indiquant la valeur par defaut a retourner lorsqu'aucune valeur n'est selectionnée.
        ///
        /// Cette fonction ne fonctionne que si la LookUpEdit a été remplie avec la méthode FillWithEnumValues.
        /// </summary>
        public static TEnum? GetValueFromChangingEvent<TEnum>(this LookUpEdit lue, object item, TEnum? defaultValue = null)
            where TEnum : struct
        {
            lue.LookUpEditChecksForUse("SelectedValueInChangingEventArgs", typeof(TEnum));
            var lst = lue.Properties.DataSource as Internal.IItemList;
            Debug.Assert(lst != null);
            if (item == null || item == lst.EmptyItem)
                return defaultValue;
            return (TEnum)((Internal.Item)item).GetObject();
        }


        /// <summary>
        /// Recupere une valeur typé de l'enum selectionné.
        /// Si la valeur est vide, une exception est levée.
        /// Si vous ne souhaitez pas avoir d'exception, utiliseé la urcharge de cette methode indiquant la valeur par defaut a retourner lorsqu'aucune valeur n'est selectionnée.
        ///
        /// Cette fonction ne fonctionne que si la LookUpEdit a été remplie avec la méthode FillWithEnumValues.
        /// </summary>
        public static TEnum SelectedValueAsEnum<TEnum>(this LookUpEdit lue)
            where TEnum : struct
        {
            lue.LookUpEditChecksForUse("SelectedValueAsEnum", typeof(TEnum));
            if (lue.EditValue == null)
            {
                var lst = lue.Properties.DataSource as Internal.IItemList;
                Debug.Assert(lst != null);
                if (!lst.AllowEmpty)
                    throw new Exception("SelectedValueAsEnum: Cannot return a typed value because no value is selected, Please provide a default enum value with optional argument \"defaultValueForNoSelection\" !");
                return (TEnum)lst.EmptyItem.GetObject();
            }
            return (TEnum)((Internal.Item)lue.EditValue).GetObject();
        }

        public static TEnum? SelectedValueAsEnum<TEnum>(this LookUpEdit lue, TEnum? defaultValueForNoSelection)
            where TEnum : struct
        {
            lue.LookUpEditChecksForUse("SelectedValueAsEnum", typeof(TEnum));
            var lst = lue.Properties.DataSource as Internal.IItemList;
            Debug.Assert(lst != null);
            if (lue.EditValue == null || lue.EditValue == lst.EmptyItem)
                return defaultValueForNoSelection;
            return (TEnum)((Internal.Item)lue.EditValue).GetObject();
        }

        /// <summary>
        /// Assigne une valeur d'enumeratio na cette combobox
        ///
        /// Cette fonction ne fonctionne que si la LookUpEdit a été remplie avec la méthode FillWithEnumValues.
        /// </summary>
        public static void SelectedValueAsEnumSet<TEnum>(this LookUpEdit lue, TEnum value, bool unselect_if_not_selectable = false)
            where TEnum : struct
        {
            lue.LookUpEditChecksForUse("SelectedValueAsEnumSet", typeof (TEnum));
            SelectedValueAsEnumSet_NoCheck(lue, value, unselect_if_not_selectable);
        }
        internal static void SelectedValueAsEnumSet_NoCheck(this LookUpEdit lue, object value, bool unselect_if_not_selectable)
        {
            var lst = (Internal.ItemList<Enum, long>)lue.Properties.DataSource;
            foreach (var pair in lst)
// ReSharper disable RedundantCast
                if (pair.Id == (value.GetType().GetEnumUnderlyingType() == typeof(ulong) ? (long)Convert.ToUInt64(value) : Convert.ToInt64(value)))
// ReSharper restore RedundantCast
                {
                    lue.EditValue = pair;
                    ((Internal.IItemList)lst).Initialized = true;
                    return;
                }
            if (!unselect_if_not_selectable)
// ReSharper disable RedundantCast
                throw new Exception($"Enum value \"{((Enum)value).GetDescription()}\" is not selectable in LookUpEdit \"{lue.Name}\"!");
// ReSharper restore RedundantCast

            ((Internal.IItemList)lst).Initialized = true;
            lue.UnselectAnyEnum();
        }

        /// <summary>
        /// Deselectionne la valeur selectionnée dans une lookupedit, la chaine vide est donc affiché et EditValue est à null
        ///
        /// Cette fonction ne fonctionne que si la LookUpEdit a été remplie avec la méthode FillWithEnumValues.
        /// </summary>
        public static void UnselectAnyEnum(this LookUpEdit lue)
        {
            UnselectAnyEnum(lue, true);
        }
        public static void UnselectAnyEnum<TEnum>(this LookUpEdit lue, TEnum default_value)
            where TEnum : struct
        {
            NullValueSet(lue, default_value);
            UnselectAnyEnum(lue, true);
        }
        internal static void UnselectAnyEnum(this LookUpEdit lue, bool developper_explicitly_call)
        {
            var lst = (Internal.ItemList<Enum, long>)lue.Properties.DataSource;
            //Form frm = lue.FindForm();
            //Debug.Assert(lst.AllowEmpty,
            //    "Impossible de déselectionner tous les choix de la combobox " + lue.Name + (frm == null ? "" : " de la form " + frm.Name + " (\"" + frm.Text + "\")") + " car : " + System.Environment.NewLine +
            //             " - la combobox a ete rempli ne indiquant qu'il ne pouvait y avoir de valeur empty (argument with_empty = false" + System.Environment.NewLine +
            //             " ET " + System.Environment.NewLine +
            //             " - aucune valeur par defaut n'a ete configuré via un appel à NullValueSet" + System.Environment.NewLine +
            //             "Corriger l'une de ces deux propositions");

            lue.LookUpEditChecksForUse("UnselectAnyEnum", null);
            lue.EditValue = null;
            lst.Initialized |= lst.AllowEmpty || developper_explicitly_call;
        }

        #endregion LookUpEdit (DevExpress)

        #endregion Generic about Static Enums
    }
}
