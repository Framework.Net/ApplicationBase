﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;

using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;

using TechnicalTools.Diagnostics;
using TechnicalTools.Tools;


namespace TechnicalTools.UI.DX
{
    public partial class BusyForm : XtraForm
	{
        public string   MainAction      { get; }

	    readonly bool _preventBusyFormToBeShownAsDialog;

        [DefaultValue(true)]
        public bool ShowElapsedTime
        {
            get { return _ShowElapsedTime; }
            set
            {
                _ShowElapsedTime = value;
                if (!DesignTimeHelper.IsInDesignMode)
                    barElapsedTimeStatus.Visibility = _ShowElapsedTime ? BarItemVisibility.Always : BarItemVisibility.Never;
            }
        }
        bool _ShowElapsedTime = true;

        public static event Action<string, Exception> DefaultExceptionHandling;
        internal static void RaiseDefaultExceptionHandling(string actionName, Exception ex)
        {
            DefaultExceptionHandling?.Invoke(actionName, ex);
        }

        public BusyForm()
            : this(null, null)
        {
		}
        readonly CancellationTokenSource            _cancellation;
        //readonly Progress<string>                   _progressReporter;
        static readonly ConcurrentDictionary<BusyForm, bool> _areUITasksDetached = new ConcurrentDictionary<BusyForm, bool>(); // BusyForm instance => true if the instance of busyform is running in thread different than main default Gui thread

        public BusyForm(string actionName, Progress<string> progressReporter, CancellationTokenSource cancellation = null, bool preventBusyFormToBeShownAsDialog = false)
		{
            MainAction = actionName;
            _cancellation = cancellation;
            _preventBusyFormToBeShownAsDialog = preventBusyFormToBeShownAsDialog;
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            lblProgress.AppearanceItemCaption.Font = new Font(FontFamily.GenericMonospace, lblProgress.AppearanceItemCaption.Font.Size, FontStyle.Regular);

            // Prevent memory leak because constructor of timer does not take (sometimes) this.component as argument when created in designer
            Disposed += (_, __) => timerElapsedTime.Dispose();
            _areUITasksDetached.TryAdd(this, preventBusyFormToBeShownAsDialog);
            Disposed += (_, __) => _areUITasksDetached.TryRemove(this, out preventBusyFormToBeShownAsDialog);

            // We do not use CenterForm because it places the form in the middle of the screen where is the mouse, not where is the owning form
            // So we handle placement manually
            //this.HandleCreated += (_, __) => CenterToParent();

            if (_preventBusyFormToBeShownAsDialog)
            {
                barlblNonBlockingDevelopper.Visibility = BarItemVisibility.Always;
                barlblNonBlockingDevelopper.Hint = "You're seeing this because the busy form is not blocking, this behavior is because you'are an admin / dev or something like that";
            }
            //var t = barlblNonBlockingDevelopper.ItemAppearance;
            //barlblNonBlockingDevelopper.ItemAppearance. SetFont(barlblNonBlockingDevelopper.ItemAppearance.)
            ToolTipOnDisableControl.EnableFeatureOn(this);

            Text = string.IsNullOrEmpty(actionName) ? " " : actionName; // If empty the window border does not appear

            btnStop.Enabled = _cancellation != null;
            if (!btnStop.Enabled)
                btnStop.ToolTip = "(Sorry... Stopping in the middle of this task is not available)";

            Shown += (__, ___) =>
            {
                ForceRefresh(); // Make sure the form is displayed at least once
                if (progressReporter != null)
                    progressReporter.ProgressChanged += ProcessMessage;
            };
            Debug.Assert(!IsHandleCreated, "Obligatoire car la gestion des taches utilise le fait que le handle n'est pas crée encore");
            _refreshProgressMessage = new PostponingExecuter(EnsureProgressMessageIsDisplayed);
        }

        void ProcessMessage(object _, string progressMsg)
        {
            _progressMessage = progressMsg;
            _refreshProgressMessage.SchedulePostponedExecution(_progressMessage != null);
        }
        readonly PostponingExecuter _refreshProgressMessage;
        string _progressMessage;

        void EnsureProgressMessageIsDisplayed()
        {
            lblProgress.Text = _progressMessage;
            if (BusyFormlayoutControl1ConvertedLayout.Root != null)
            {
                var layoutMinSize = BusyFormlayoutControl1ConvertedLayout.Root.MinSize;
                var newSize = new Size(25 + Math.Max(ClientSize.Width - 25, layoutMinSize.Width),
                    25 + Math.Max(ClientSize.Height - 25, layoutMinSize.Height));
                // ReSharper disable once RedundantCheckBeforeAssignment
                if (newSize != ClientSize)
                {
                    var dw = newSize.Width - ClientSize.Width;
                    var dh = newSize.Height - ClientSize.Height;
                    //ClientSize = newSize;
                    SetBounds(Left - dw / 2, Top - dh / 2, Width + dw, Height + dh);
                    //Debug.Assert(ClientSize == newSize);
                    ForceRefresh();
                    // CenterToParent();
                }
            }
        }



        private void btnStop_Click(object sender, EventArgs e)
        {
            Debug.Assert(_cancellation != null);

            if (!_cancellation.IsCancellationRequested)
            {
                lblProgress.Text = "Cancelling...";
                try { _cancellation.Cancel(); }
                catch (Exception ex) // Unlikely case but happens sometimes
                {
                    DebugTools.Break();
                    XtraMessageBox.Show(this, "Error during stopping !" + Environment.NewLine +
                                              "Tell about this error to someone in charge of " + Application.ProductName + Environment.NewLine +
                                              Environment.NewLine +
                                              ex.Message,
                                              "Cannot stop / cancel !");
                    lblProgress.Text = "Cancelling failed ! (Please wait)...";
                }
            }
		}

        public static Task<T> ShowBusyWhileDoing<T>(Control ownerOfTask, string actionName,
                                                    Func<IProgress<string>, Task<T>> doWork,
                                                    Action uiCallBackOnSuccess = null,
                                                    Action<Exception> onException = null)
        {
            var progressReporter = new Progress<string>();
            // Sadly we can't start the tasks until the busyform is displayed
            // Because Default Task scheduler often uses the main gui thread
            // So the busy form would not appear immediately and user would think application has frozen
            // So we wait until the shown event is raised and the form is displayed at least once
            Task<T> task = null;
            var curContext = TaskScheduler.FromCurrentSynchronizationContext();
            using (var busyForm = new BusyForm(actionName, progressReporter, null, AllowPreventBusyFormToBeShownAsDialog && Keyboard.IsKeyDown(Key.LeftShift)) { Opacity = 0 })
            {
                busyForm.Shown += async (_, __) =>
                {
                    // ReSharper disable AccessToDisposedClosure
                    busyForm.Opacity = 100;
                    try
                    {
                        busyForm._startTime = DateTime.UtcNow;
                        busyForm.timerElapsedTime.Enabled = true;
                        task = doWork(progressReporter);
                        if (task.Status == TaskStatus.Created)
                            task.Start();

                        if (uiCallBackOnSuccess != null)
#pragma warning disable 4014
                            task.ContinueWith(t =>
                            {
                                if (t.Status == TaskStatus.RanToCompletion)
                                    uiCallBackOnSuccess();
                            }, curContext);
#pragma warning restore 4014
                        await task; // NOSONAR This library is for UI WinForm
                        Debug.WriteLine($"Action {actionName} done in {(DateTime.UtcNow - busyForm._startTime).ToString()}");
                    }
                    catch (Exception ex)
                    {
                        if (onException != null)
                            onException(ex);
                        else
                            RaiseDefaultExceptionHandling(actionName, ex);
                    }
                    finally
                    {
                        busyForm.timerElapsedTime.Enabled = false;
                        busyForm.Close();
                    }
                    // ReSharper restore AccessToDisposedClosure
                };

                busyForm.StartPosition = FormStartPosition.Manual;
                var p = ownerOfTask.PointToScreen(Point.Empty);
                p.X -= Math.Min(0, (ownerOfTask.Parent as DocumentContainer)?.Left ?? 0); // Fix documentmanager offset which is negative until the document manager is truly (desptie the fact that property Visible and IsHandleCreated are true)
                p.Y -= Math.Min(0, (ownerOfTask.Parent as DocumentContainer)?.Top ?? 0);
                var bounds = Screen.FromControl(ownerOfTask).Bounds;
                bounds = Screen.GetWorkingArea(bounds.Location); // Ignore task bar space etc
                busyForm.Left = Math.Min(bounds.Right - busyForm.Width,
                                         Math.Max(bounds.Left, p.X + (ownerOfTask.Width - busyForm.Width) / 2));
                busyForm.Top  = Math.Min(bounds.Bottom - busyForm.Height,
                                         Math.Max(bounds.Top, p.Y + (ownerOfTask.Height - busyForm.Height) / 2));

                if (busyForm._preventBusyFormToBeShownAsDialog)
                {
                    // Show is non blocking so we have to simulate the block
                    var evt = new ManualResetEvent(false);
                    busyForm.Closed += (_, __) =>
                    {
                        // ReSharper disable once AccessToDisposedClosure
                        evt.Set();
                    };
                    if (ownerOfTask.InvokeRequired)
                        busyForm.Show();
                    else
                        busyForm.Show(ownerOfTask as Form ?? ownerOfTask.FindForm());
                    busyForm.BringToFront();
                    while (!evt.WaitOne(250))
                        Application.DoEvents();
                    evt.Dispose();
                }
                else
                {
                    if (ownerOfTask.InvokeRequired)
                        busyForm.ShowDialog();
                    else
                        busyForm.ShowDialog(ownerOfTask as Form ?? ownerOfTask.FindForm());
                }
            }


            return task;
        }
        public static bool AllowPreventBusyFormToBeShownAsDialog { get; set; }

        public static Task<T> ShowBusyWhileDoing<T>(Control ownerOfTask, string actionName,
                                                    Func<IProgress<string>, T> doWork,
                                                    Action<T> uiCallBackOnSuccess = null,
                                                    Action<Exception> onException = null)
        {
            // We use Task.Factory to specify TaskScheduler.Default that understand "TaskCreationOptions.LongRunning" as run in other thread
            // TaskScheduler.Current (which is used by default) does not provide a way to force thread execution in other thread
            T result = default(T);
            return ShowBusyWhileDoing(ownerOfTask, actionName, progressReporter => Task.Factory.StartNew(() => { result = doWork(progressReporter); return result; }, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default),
                                                               () => uiCallBackOnSuccess?.Invoke(result), onException);
        }
        public static Task<T> ShowBusyWhileDoing<T>(Control ownerOfTask, string actionName,
                                                    Func<IProgress<string>, T> doWork,
                                                    Action uiCallBackOnSuccess = null,
                                                    Action<Exception> onException = null)
        {
            // We use Task.Factory to specify TaskScheduler.Default that understand "TaskCreationOptions.LongRunning" as run in other thread
            // TaskScheduler.Current (which is used by default) does not provide a way to force thread execution in other thread
            // TODO : Si une exception a lieu dans doWork, elle n'est pas gérée, et donc remontée par le finalizeur qui l'affiche dans une aggregate exception.. revoir ca :(
            return ShowBusyWhileDoing(ownerOfTask, actionName, progressReporter => Task.Factory.StartNew(() => doWork(progressReporter), CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default),
                                                               uiCallBackOnSuccess, onException);
        }
        private void timerElapsedTime_Tick(object sender, EventArgs e)
        {
            if (Disposing || IsDisposed)
                return;
            barElapsedTimeStatus.Caption = "Elapsed time: " + (DateTime.UtcNow - _startTime).ToString("hh':'mm':'ss");
        }
        DateTime _startTime;

        static GlobalKeyboardHookSample _hookToDetachBusyFormAndSetToBackground = new GlobalKeyboardHookSample();

        class GlobalKeyboardHookSample
        {
            private GlobalKeyboardHook _globalKeyboardHook;

            public GlobalKeyboardHookSample()
            {
                Install();
            }
            void Install()
            {
                _globalKeyboardHook = new GlobalKeyboardHook();
                _globalKeyboardHook.KeyboardPressed += OnKeyPressed;

                Application.ApplicationExit += ApplicationExit_Or_DomainUnload;
                // DomainUnload is not raised for default AppDomain, but in case currentDomain would be a
                AppDomain.CurrentDomain.DomainUnload += ApplicationExit_Or_DomainUnload;

                _enabled = true;
            }
            bool _enabled;
            void Uninstall()
            {
                _enabled = false;
                _globalKeyboardHook?.Dispose();
                Application.ApplicationExit -= ApplicationExit_Or_DomainUnload;
                AppDomain.CurrentDomain.DomainUnload -= ApplicationExit_Or_DomainUnload;
            }

            private void ApplicationExit_Or_DomainUnload(object sender, EventArgs e)
            {
                Uninstall();
            }

            [DebuggerStepThrough]
            void OnKeyPressed(object sender, GlobalKeyboardHookEventArgs e)
            {
                if (!_enabled || Environment.HasShutdownStarted)
                    return;
                //Debug.WriteLine("key pressed: " + e.KeyboardData.VirtualCode + " state: " + e.KeyboardState);
                if (e.KeyboardData.VirtualCode == GlobalKeyboardHook.VkLeftControl)
                    _ctrlLeftPressed = e.KeyboardState == GlobalKeyboardHook.KeyboardState.KeyDown;
                else if (e.KeyboardData.VirtualCode == GlobalKeyboardHook.B)
                    _BKeyPressed = e.KeyboardState == GlobalKeyboardHook.KeyboardState.KeyDown;

                if (_ctrlLeftPressed && _BKeyPressed && ApplicationFocusDetector.IsApplicationActivated())
                {
                    // L'evenement OnKeyPressed est recu en continue si on continue de presser une touche
                    // On désactive donc une variable de la condition pour ne pas farie deux fois le code ci-dessous
                    _BKeyPressed = false;

                    var busyForm = _areUITasksDetached.Where(kvp => !kvp.Value).Select(kvp => kvp.Key).FirstOrDefault();
                    if (busyForm != null)
                    {
                        Debug.Assert(!busyForm.InvokeRequired);
                        // Debug.WriteLine("TODO : Pass current Task in background");
                        //e.Handled = true;
                    }
                }
            }
            bool _ctrlLeftPressed;
            bool _BKeyPressed;

        }
    }

    public static class BusyForm_Extensions
    {
        public static Task ShowBusyWhileDoing(this Control ownerOfTask, string actionName, Action<IProgress<string>> doWork, Action<Exception> onException = null)
        {
            return BusyForm.ShowBusyWhileDoing(ownerOfTask, actionName, progress => { doWork(progress); return true; }, (Action)null, onException);
        }
        public static Task ShowBusyWhileDoing(this Control ownerOfTask, string actionName, Action<IProgress<string>> doWork, Action uiCallBackOnSuccess, Action<Exception> onException = null)
        {
            return BusyForm.ShowBusyWhileDoing(ownerOfTask, actionName, progress => { doWork(progress); return true; }, uiCallBackOnSuccess, onException);
        }
        public static Task<T> ShowBusyWhileDoing<T>(this Control ownerOfTask, string actionName, Func<IProgress<string>, T> doWork, Action<Exception> onException = null)
        {
            return BusyForm.ShowBusyWhileDoing(ownerOfTask, actionName, doWork, (Action)null, onException);
        }
        public static Task<T> ShowBusyWhileDoing<T>(this Control ownerOfTask, string actionName, Func<IProgress<string>, T> doWork, Action<T> uiCallBackOnSuccess, Action<Exception> onException = null)
        {
            return BusyForm.ShowBusyWhileDoing(ownerOfTask, actionName, doWork, uiCallBackOnSuccess, onException);
        }
        public static Task<T> ShowBusyWhileDoing<T>(this Control ownerOfTask, string actionName, Func<IProgress<string>, T> doWork, Action uiCallBackOnSuccess, Action<Exception> onException = null)
        {
            return BusyForm.ShowBusyWhileDoing(ownerOfTask, actionName, doWork, uiCallBackOnSuccess, onException);
        }

        public  static bool ShowBusyWhileDoingUIWorkInPlace(this Control ownerOfTask,                    Action<IProgress<string>> doUIWork) { return ShowBusyWhileDoingUIWork(ownerOfTask, "Updating user interface...", doUIWork, true);  }
        public  static bool ShowBusyWhileDoingUIWorkInPlace(this Control ownerOfTask, string actionName, Action<IProgress<string>> doUIWork) { return ShowBusyWhileDoingUIWork(ownerOfTask, actionName,                   doUIWork, true); }
        public  static bool ShowBusyWhileDoingUIWork       (this Control ownerOfTask,                    Action<IProgress<string>> doUIWork) { return ShowBusyWhileDoingUIWork(ownerOfTask, "Updating user interface...", doUIWork, false); }
        public  static bool ShowBusyWhileDoingUIWork       (this Control ownerOfTask, string actionName, Action<IProgress<string>> doUIWork) { return ShowBusyWhileDoingUIWork(ownerOfTask, actionName,                   doUIWork, false); }
        private static bool ShowBusyWhileDoingUIWork       (this Control ownerOfTask, string actionName, Action<IProgress<string>> doUIWork, bool doNotPostponeWork)
        {
            Debug.Assert(SynchronizationContext.Current != null, "This method should be executed in UI Thread!" + Environment.NewLine +
                                                                 "If you run it via ContinueWith (called by ui context), add the following argument to ContinueWith call : TaskScheduler.FromCurrentSynchronizationContext()");

            // Execute these two lines because they need t obe execute in ownerOfTask' thread
            var p = ownerOfTask.PointToScreen(Point.Empty);
            var bounds = Screen.FromControl(ownerOfTask).Bounds;

            var evtWaitForTaskDone = new AutoResetEvent(false);
            var evtWaitForProgressCreated = new AutoResetEvent(false);
            Progress<string> progressReporter = null;
            Form_Extensions.ShowInOtherGuiThread(() =>
            {
                progressReporter = new Progress<string>();
                evtWaitForProgressCreated.Set();
                var busyForm = new BusyForm(actionName, progressReporter) { Opacity = 0, ShowElapsedTime = false };
                // busyForm.Owner = ownerOfTask as Form; // probleme de thread // voir  http://stackoverflow.com/questions/29264017/c-sharp-winforms-form-showdialog-with-iwin32window-owner-parameter-in-a-diffe
                busyForm.Shown += (_, __) =>
                {
                    busyForm.Opacity = 100;
                    //busyForm._startTime = DateTime.UtcNow;
                    //busyForm.timerElapsedTime.Enabled = true;
                    busyForm.TopMost = true;
                    var current = SynchronizationContext.Current; // Fix value to debug later
                    //DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true;
                    //Control.CheckForIllegalCrossThreadCalls = false;
                    Task.Factory.StartNew(() =>
                    {
                        evtWaitForTaskDone.WaitOne();
                        evtWaitForProgressCreated.Dispose();
                        // A la place de cette ligne, si on utilise sur la tache renvoyé :
                        // .ContinueWith(t => busyForm.Close, TaskScheduler.FromCurrentSynchronizationContext())
                        // ca ne marche pas de temps en temps... Pourquoi (cf le debugAssert en dessous) ?
                        busyForm.BeginInvoke((Action)busyForm.Close);
                    }, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default)
                                // if bug here, check that current == SynchronizationContext.Current && current == TaskScheduler.FromCurrentSynchronizationContext()
                                // and ContinueWithUIWork with ContinueWith(..., current)
                                //.ContinueWithUIWork(t => busyForm.Close());
                                .ContinueWith(t => DebugTools.Should(current == SynchronizationContext.Current),
                                              TaskScheduler.FromCurrentSynchronizationContext());
                };
                busyForm.StartPosition = FormStartPosition.Manual;
                bounds = Screen.GetWorkingArea(bounds.Location); // enleve la barre des taches etc
                p.X -= Math.Min(0, (ownerOfTask.Parent as DocumentContainer)?.Left ?? 0);  // fix documentmanager offset which is negative until the document manager is truly (desptie the fact that property Visible and IsHandleCreated are true)
                p.Y -= Math.Min(0, (ownerOfTask.Parent as DocumentContainer)?.Top ?? 0);
                busyForm.Left = Math.Min(bounds.Right - busyForm.Width,
                                        Math.Max(bounds.Left, p.X + (ownerOfTask.Width - busyForm.Width) / 2));
                busyForm.Top = Math.Min(bounds.Bottom - busyForm.Height,
                                        Math.Max(bounds.Top, p.Y + (ownerOfTask.Height - busyForm.Height) / 2));
                return busyForm;
            });
            evtWaitForProgressCreated.WaitOne(); // Wait for busyForm to initialize progressReporter

            Func<bool> protectedDoUIWork = () =>
            {
                try
                {
                    try
                    {
                        doUIWork(progressReporter);
                        return true;
                    }
                    finally
                    {
                        evtWaitForTaskDone.Set();
                    }
                }
                catch (Exception ex)
                {
                    //if (onException != null)
                    //    onException(ex);
                    //else
                    BusyForm.RaiseDefaultExceptionHandling(actionName, ex);
                }
                return false;
            };
            if (doNotPostponeWork)
            {
                Application.DoEvents();
                return protectedDoUIWork();
            }
            ownerOfTask.BeginInvoke(protectedDoUIWork);
            return true;
        }

        //static void Do(string msg, Action action)
        //{
        //    //lock (logs)
        //    //{
        //    //    logs.Add(DateTime.UtcNow.ToString() + " : " + msg);
        //    //}
        //        action();

        //}
        //public static readonly List<string> logs = new List<string>();
    }

}
