﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using TechnicalTools.UI.DX.Forms.Errors;


namespace TechnicalTools.UI.DX.Tools
{
    /// <summary>
    /// Classe qui permet de démarrer une splashscreen avant d'afficher la fenetre principale
    /// Voici comment il faut agencer le code si une fenetre (FrmMain) hérite de (FrmMainBase)
    /// <code>
    /// public FrmMain()
    /// {
    ///     if (DesignTimeHelper.IsInDesignMode)
    ///          InitializeComponent();
    ///     else
    ///         _starter = new ApplicationMainFormStarter(this, () => new SplashScreen(),
    ///                                                         () => new SplashScreenInitializer(() => Invoke((Action)OnSplashScreenDone))) { MaximiseMainForm = true }
    ///                                                  .Start();
    ///     // with class SplashScreenInitializer implementing ApplicationMainFormStarter.IApplicationInitializerBase
    /// }
    /// readonly ApplicationMainFormStarter _starter;
    /// void OnSplashScreenDone()
    /// {
    ///     InitializeComponent();
    ///     // Others things to init when connected to db
    /// }
    /// </code>
    /// </summary>
    public class ApplicationMainFormStarter
    {
        public ISplashScreenBase SplashScreen { get; protected set; }

        public bool ShowAtStartup { get; set; } // Fait en sorte que la form ne soit pas visible au demarrage
        public bool MaximiseMainForm { get; set; } // Fait en sorte de maximiser la main form avant de l'afficher

        public interface ISplashScreenBase : IWin32Window
        {
            Screen CurrentScreen { get; }
            event EventHandler Shown;
            event EventHandler Closed;
            double Opacity { get; set; }

            void RunInNewGuiThread(); // implementé en winforms en faisant : Application.Run(this)
            void Close();

            void Post(Action action);
        }

        public interface IApplicationInitializerBase
        {
            void Initialize(ISplashScreenBase splashScreen, Action<Exception/* ex*/, bool /*start_canceled*/> onDone);
        }

        //new TSplashScreen SplashScreen { get { return (TSplashScreen)base.SplashScreen; } set { base.SplashScreen = value; } }

        readonly Form _mainForm;
        readonly Func<ISplashScreenBase> _createSplashScreen;
        readonly Func<IApplicationInitializerBase> _createInitializer;

        public ApplicationMainFormStarter(Form frm, Func<ISplashScreenBase> createSplashScreen, Func<IApplicationInitializerBase> createInitializer)
        {
            ShowAtStartup = true;
            _mainForm = frm;
            _createSplashScreen = createSplashScreen;
            _createInitializer = createInitializer;
        }

        public ApplicationMainFormStarter Start()
        {
            _mainForm.Opacity = 0;
            _mainForm.ShowInTaskbar = false;
            _mainForm.Shown += FrmMain_Shown;
            return this;
        }

        private void FrmMain_Shown(object sender, EventArgs e)
        {
            Debug.Assert(_mainForm.IsHandleCreated);
            _mainForm.Visible = false;
            _mainForm.Opacity = 100;

            var th = new Thread(() =>
            {
                SplashScreen = _createSplashScreen();
                //if (!ShowAtStartup)
                //{
                //    SplashScreen.Opacity = 0;
                //    SplashScreen.Shown += (...); // mettre visible a false et l'opacité à 100
                //}
                SplashScreen.Shown += OnSplashShown;
                SplashScreen.Closed += OnSplashFailed;
                _mainForm.AllowCrossThreadCallsTemporary(() => // Dirty mais super efficace !
                {
                    SplashScreen.RunInNewGuiThread();
                });
                SplashScreen = null;
            });
            th.SetApartmentState(ApartmentState.STA); // Fait que l'edition de la connexion ouvre la bonne boite de dialog (plantage transparent en interne sinon)
            //th.IsBackground = true; // <-- The workaround
            th.Start();
        }

        // Pour faire apparaitre la splashscreen dans la taskbar, mais ca ne marche pas
        // D'après DevExpres il faut utiliser une WaitForm.
        // Sinon il faut utiliser un hack, voir ici http://stackoverflow.com/questions/659506/what-causes-a-window-to-not-appear-in-the-taskbar-until-its-alt-tabbed-to-in-vi
        // et dans le dossier Docs
        DialogResult OnShowDialogFromAnywhereInCode(Func<DialogResult> func)
        {
            // Améliore l'experience utilisateur, mais pas obligatoire
            // TODO
            //try { (SplashScreen as Form).EnsureFormIsShownInTaskBar(); }
            //catch { }
            return func();
        }

        private void OnSplashShown(object sender, EventArgs e)
        { // executé dans le thread de la splashscreen
            if (!ShowAtStartup)
                SplashScreen.Opacity = 0;
            var initializer = _createInitializer();
            initializer.Initialize(SplashScreen, OnSplashScreenFinishInitialization);
        }

        void OnSplashScreenFinishInitialization(Exception ex, bool start_canceled)
        { // executé dans le thread de la splashscreen
            SplashScreen.Closed -= OnSplashFailed;
            if (start_canceled)
            {
                SplashScreen.Close();
                _mainForm.BeginInvoke((Action)_mainForm.Close);
                return;
            }
            if (ex != null)
            {
                var frm = new TechnicalErrorPopup(ex);
                frm.ShowDialog(SplashScreen);
                SplashScreen.Close();
                return;
            }
            _mainForm.BeginInvoke((Action)(() =>
            {
                if (_mainForm.IsDisposed)
                    return;
                _mainForm.ShowInTaskbar = true;
                if (MaximiseMainForm)
                {
                    Screen s = SplashScreen.CurrentScreen;
                    var ws = _mainForm.WindowState;
                    _mainForm.WindowState = FormWindowState.Normal; // Obligatoire sinon impossible de changer la position
                    _mainForm.SetBounds(s.WorkingArea.Left, s.WorkingArea.Top, s.WorkingArea.Width, s.WorkingArea.Height, BoundsSpecified.All);
                    _mainForm.WindowState = ws;
                }
                SplashScreen.Post(SplashScreen.Close);
                while (!_mainForm.Visible)
                    try
                    {
                        // Sometimes exception occurs here (see commit's comment) => Main is just a blank rectangle (no title..)
                        // It happens whit initialization of RibbonBar
                        // So statusBar at bottom is not shown when this happens
                        _mainForm.Visible = true;
                    }
                    catch
                    {
                        // Reset and retry to be sure all element are initialized
                        // (ex: status bar when ribbon failed to be initialized)
                        _mainForm.Visible = false;
                        Thread.Sleep(10);
                    }
            }));
        }

        void OnSplashFailed(object sender, EventArgs e)
        {
            _mainForm.BeginInvoke((Action) (() =>
                {
                    string product = ((AssemblyProductAttribute)Attribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof(AssemblyProductAttribute), false)).Product;

                    var frm = new TechnicalErrorPopup(product + " has encountered an unexpected error and will now shut down.")
                    {
                        Text = "Unexpected error!"
                    };
                    frm.ShowDialog(_mainForm);
                    _mainForm.Close();
                }));
        }

    }
}
