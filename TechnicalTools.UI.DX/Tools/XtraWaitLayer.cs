﻿using System;
using System.Drawing;
using System.Windows.Forms;

using DevExpress.XtraEditors;


namespace TechnicalTools.UI.DX
{
    public partial class XtraWaitLayer : XtraUserControl, BackgroundUITask.IWaitLayer
    {
        // Pour le designer Visual Studio
        internal XtraWaitLayer()
        {
            InitializeComponent();
        }

        public void SetMaskedControl(Control masked_control)
        {
            /* Seems useless currently */
        }

        public string Caption { get { return progressPanel.Caption; }
                                set { progressPanel.Caption = value; CenterProgressPanel(); } }

        public string Description { get { return progressPanel.Description; }
                                    set { progressPanel.Description = value; CenterProgressPanel(); } }

        public double ProgressionInPercent
        {
            get
            {
                return _ProgressionInPercent;
            }
            set
            {
                _ProgressionInPercent = value;
            }
        }
        double _ProgressionInPercent;

        public Bitmap Background { get; set; }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (Background != null)
                e.Graphics.DrawImage(Background, Point.Empty);
        }

        private void WaitControlGeneric_Resize(object sender, EventArgs e)
        {
            CenterProgressPanel();
        }

        void CenterProgressPanel()
        {
            SizeF size = TextRenderer.MeasureText(progressPanel.Caption, progressPanel.AppearanceCaption.Font);

            progressPanel.Size = new Size(60 + (int)size.Width, Math.Max(40, (int)size.Height)); // (60,40 = Taille minimal sans texte du progress panel)

            progressPanel.Location = new Point((Width - progressPanel.Width) / 2,
                                               (Height - progressPanel.Height) / 2);
            // Pour debugger les positions / taille :
            // progressPanel.BackColor = Color.Red;
            // BackColor = Color.Black;
        }

        #region Ce code fourni par DevExpress ne fonctionne pas (a virer si besoin)
        // voir http://www.devexpress.com/Support/Center/Example/Details/E3892 si ca peut aider
        // (mais pas reussi a le faire marcher quand j'ai essayé)
        /*
        static Dictionary<Control, SplashScreenManager> Managers = new Dictionary<Control, SplashScreenManager>();

        class UserControlWaitForm : DevExpress.XtraWaitForm.WaitForm
        {
            public enum UserControlWaitFormCommand { SetSize }

            // Necessaire pour devexpress
            public UserControlWaitForm()
            {
                //InitializeComponent();
            }

            public static void ShowWaitForm(Control owner)
            {
                if (Managers.ContainsKey(owner))
                    return;
                SplashScreenManager manager = new SplashScreenManager(typeof(UserControlWaitForm),
                    SplashFormStartPosition.Manual, owner.Parent.PointToScreen(owner.Location),
                    new SplashFormProperties());
                Managers.Add(owner, manager);
                manager.ShowWaitForm();
                manager.SendCommand(UserControlWaitFormCommand.SetSize, owner.Size);
            }

            public static void HideWaitForm(Control owner)
            {
                if (!Managers.ContainsKey(owner))
                    return;
                SplashScreenManager manager = Managers[owner];
                Managers.Remove(owner);
                try
                {
                    manager.CloseWaitForm();
                }
                finally
                {
                    manager.Dispose();
                }
            }

            public override void ProcessCommand(Enum cmd, object arg)
            {
                if (((UserControlWaitFormCommand)cmd) == UserControlWaitFormCommand.SetSize)
                    Size = (System.Drawing.Size)arg;
            }
        }
        */
        #endregion Ce code fourni par DevExpress ne fonctionne pas



    }

    // To replace with IOverlaySplashScreenHandle (DX >= 18.1)
    public class WaitControlGeneric
    {
        /// <summary>
        /// Bloque l'acces a un control en le recouvrant (temporairement) d'un voile transparent gris
        /// comprenant en son milieu un petit texte, tout en executant, dans un autre thread, une action prenant du temps.
        /// Finalement, le voile disparait et les autres callbacks sont appelées dans le thread UI courant
        /// Le resultat de l'action prenant du temps est passe en argument au callback quand tout s'est bien terminé.
        /// </summary>
        /// <typeparam name="TOutData">Type de la valeur retourné par l'action prenant du temps</typeparam>
        /// <param name="ctl_to_block">Controle a bloquer</param>
        /// <param name="caption">Petit texte a afficher au milieu du voile gris qui bloque le control</param>
        /// <param name="description">Petite description sous le petit texte</param>
        /// <param name="actionTakingTime">Action (methode ou lambda) qui prend du temps.</param>
        /// <param name="async_callback">Action a appeler une fois que l'action s'est terminée (sans planter)</param>
        /// <param name="async_finally">Action appelé en tout dernier, dans tous les cas avec un argument Exception non null si il y a eu une exeption</param>
        public static void ShowOnControl<TOutData>(Control ctl_to_block, string caption, string description, Func<TOutData> actionTakingTime, Action<TOutData> async_callback, Action<Exception> async_finally = null)
        {
            BackgroundUITask.CreateWaitLayer = DevExpressCreator;
            BackgroundUITask.LoadDataOnControl<TOutData>(ctl_to_block, caption, description, actionTakingTime, async_callback, async_finally);
            BackgroundUITask.CreateWaitLayer = BackgroundUITask.CreateDefaultWaitLayer;
        }
        /// <summary>
        /// Bloque l'acces a un control en le recouvrant (temporairement) d'un voile transparent gris
        /// comprenant en son milieu un petit texte, tout en executant, dans un autre thread, une action prenant du temps.
        /// Finalement, le voile disparait et les autres callbacks sont appelées dans le thread UI courant
        /// Le resultat de l'action prenant du temps est passe en argument au callback quand tout s'est bien terminé.
        /// </summary>
        /// <typeparam name="TOutData">Type de la valeur retourné par l'action prenant du temps</typeparam>
        /// <param name="ctl_to_block">Controle a bloquer</param>
        /// <param name="caption">Petit texte a afficher au milieu du voile gris qui bloque le control</param>
        /// <param name="actionTakingTime">Action (methode ou lambda) qui prend du temps.</param>
        /// <param name="async_callback">Action a appeler une fois que l'action s'est terminée (sans planter)</param>
        /// <param name="async_finally">Action appelé en tout dernier, dans tous les cas avec un argument Exception non null si il y a eu une exeption</param>
        public static void ShowOnControl<TOutData>(Control ctl_to_block, string caption, Func<TOutData> actionTakingTime, Action<TOutData> async_callback, Action<Exception> async_finally = null)
        {
            BackgroundUITask.CreateWaitLayer = DevExpressCreator;
            BackgroundUITask.LoadDataOnControl<TOutData>(ctl_to_block, caption, actionTakingTime, async_callback, async_finally);
            BackgroundUITask.CreateWaitLayer = BackgroundUITask.CreateDefaultWaitLayer;
        }
        /// <summary>
        /// Bloque l'acces a un control en le recouvrant (temporairement) d'un voile transparent gris
        /// comprenant en son milieu un petit texte, tout en executant, dans un autre thread, une action prenant du temps.
        /// Finalement, le voile disparait et les autres callbacks sont appelées dans le thread UI courant
        /// </summary>
        /// <param name="ctl_to_block">Controle a bloquer</param>
        /// <param name="caption">Petit texte a afficher au milieu du voile gris qui bloque le control</param>
        /// <param name="actionTakingTime">Action (methode ou lambda) qui prend du temps.</param>
        /// <param name="async_callback">Action a appeler une fois que l'action s'est terminée (sans planter)</param>
        /// <param name="async_finally">Action appelé en tout dernier, dans tous les cas avec un argument Exception non null si il y a eu une exeption</param>
        public static void ShowOnControl(Control ctl_to_block, string caption, Action actionTakingTime, Action async_callback, Action<Exception> async_finally = null)
        {
            BackgroundUITask.CreateWaitLayer = DevExpressCreator;
            BackgroundUITask.LoadDataOnControl(ctl_to_block, caption, actionTakingTime, async_callback, async_finally);
            BackgroundUITask.CreateWaitLayer = BackgroundUITask.CreateDefaultWaitLayer;
        }
        /// <summary>
        /// Bloque l'acces a un control en le recouvrant (temporairement) d'un voile transparent gris
        /// comprenant en son milieu un petit texte, tout en executant, dans un autre thread, une action prenant du temps.
        /// Finalement, le voile disparait et les autres callbacks sont appelées dans le thread UI courant
        /// </summary>
        /// <param name="ctl_to_block">Controle a bloquer</param>
        /// <param name="caption">Petit texte a afficher au milieu du voile gris qui bloque le control</param>
        /// <param name="description">Petite description sous le petit texte</param>
        /// <param name="actionTakingTime">Action (methode ou lambda) qui prend du temps.</param>
        /// <param name="async_callback">Action a appeler une fois que l'action s'est terminée (sans planter)</param>
        /// <param name="async_finally">Action appelé en tout dernier, dans tous les cas avec un argument Exception non null si il y a eu une exeption</param>
        public static void ShowOnControl(Control ctl_to_block, string caption, string description, Action actionTakingTime, Action async_callback, Action<Exception> async_finally = null)
        {
            BackgroundUITask.CreateWaitLayer = DevExpressCreator;
            BackgroundUITask.LoadDataOnControl(ctl_to_block, caption, description, actionTakingTime, async_callback, async_finally);
            BackgroundUITask.CreateWaitLayer = BackgroundUITask.CreateDefaultWaitLayer;
        }

        static BackgroundUITask.IWaitLayer DevExpressCreator()
        {
            return new XtraWaitLayer();
        }
    }
}
