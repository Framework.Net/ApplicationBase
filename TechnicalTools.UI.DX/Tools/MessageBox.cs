﻿using System;

using System.Windows.Forms;

using DevExpress.XtraEditors;


namespace TechnicalTools.UI.DX.Tools
{
    // Destiné à intercepter toutes les messagebox par la suite (automatisation des tests)
    public class MessageBoxExt
    {
        public static DialogResult Show(string msg, string title, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return XtraMessageBox.Show(msg, title, buttons, icon);
        }
        public static DialogResult Show(string msg)
        {
            return XtraMessageBox.Show(msg);
        }
    }
}
