﻿using System;
using System.Linq;
using System.Windows.Forms;

using DevExpress.XtraTab;


namespace TechnicalTools.UI.DX.Tools
{
    public static class TabControl_Extensions
    {
        public static void ForceControlCreations(this XtraTabControl tabs, params XtraTabPage[] exceptThesePages)
        {
            UI.Tools.TabControl_Extensions.ForceControlCreations(tabs);
            foreach (Control subcontrol in tabs.TabPages.Except(exceptThesePages).Reverse())
                UI.Tools.TabControl_Extensions.ForceControlCreations(subcontrol);
        }
    }
}
