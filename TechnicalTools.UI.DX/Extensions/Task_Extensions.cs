﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TechnicalTools.UI.DX
{
    public static class Task_Extensions
    {
        public static Task ContinueWithUIWork(this Task task, Action action)
        {
            Debug.Assert(SynchronizationContext.Current != null);
            return task.ContinueWith(t =>
            {
                action();
                // ces options font en sorte d'execute la ligne ci dessus dans le contexte UI
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
        public static Task ContinueWithUIWork<T>(this Task<T> task, Action<Task<T>> action)
        {
            Debug.Assert(SynchronizationContext.Current != null);
            return task.ContinueWith(t =>
            {
                action(t);
                // ces options font en sorte d'execute la ligne ci dessus dans le contexte UI
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
        public static Task ContinueWithUIWorkOnSuccess<T>(this Task<T> task, Action<T> action)
        {
            Debug.Assert(SynchronizationContext.Current != null);
            return task.ContinueWith(t =>
            {
                if (t.Status == TaskStatus.RanToCompletion)
                    action(t.Result);
                // ces options font en sorte d'execute la ligne ci dessus dans le contexte UI
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
        public static Task ContinueWithUIWorkOnSuccess(this Task task, Action action)
        {
            Debug.Assert(SynchronizationContext.Current != null);
            return task.ContinueWith(t =>
            {
                if (t.Status == TaskStatus.RanToCompletion)
                    action();
                // ces options font en sorte d'execute la ligne ci dessus dans le contexte UI
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public static Task ContinueWithLongUIWorkOnSuccess<T>(this Task<T> task, Control ownerTask, Action<T> action)
        {
            return ContinueWithLongUIWork(task, ownerTask, t =>
            {
                if (t.Status == TaskStatus.RanToCompletion)
                    action(t.Result);
            });
        }
        public static Task ContinueWithLongUIWork<T>(this Task<T> task, Control ownerTask, Action<Task<T>> action)
        {
            return ContinueWithLongUIWork(task, ownerTask, () => action(task));
        }
        public static Task ContinueWithLongUIWork(this Task task, Control ownerTask, Action action)
        {
            var currentIsUI = SynchronizationContext.Current != null;

            return task.ContinueWith(t =>
            {
                if (currentIsUI)
                {
                    var result = new TaskCompletionSource<bool>();
                    // Comme ShowBusyWhileDoingUIWork est bloquant
                    ownerTask.ShowBusyWhileDoingUIWorkInPlace(pr => { try { action(); } finally { result.SetResult(true); } });
                    //ownerTask.ShowBusyWhileDoingUIWork(action);
                }
                else
                    ownerTask.Invoke((Action)(() => ContinueWithLongUIWork(task, ownerTask, action).Wait()));
            }, CancellationToken.None, TaskContinuationOptions.None,
               TaskScheduler.FromCurrentSynchronizationContext()); // Cette option fait en sorte d'execute la ligne ci dessus dans le contexte UI
        }


        public static Task<T> AsCompletedTask<T>(this T obj)
        {
            var taskSource = new TaskCompletionSource<T>();
            taskSource.SetResult(obj);
            return taskSource.Task;
        }

        // Same method than stand but with ContinuationOption argument first
        public static Task<TNewResult> ContinueWith<TResult, TNewResult>(this Task<TResult> @this, TaskContinuationOptions continuationOptions, Func<Task<TResult>, TNewResult> continuationFunction)
        { return @this.ContinueWith(continuationFunction, continuationOptions); }
        public static Task<TNewResult> ContinueWith<TResult, TNewResult>(this Task<TResult> @this, TaskContinuationOptions continuationOptions, Func<Task<TResult>, object, TNewResult> continuationFunction, object state)
        { return @this.ContinueWith(continuationFunction, state, continuationOptions); }
        public static Task<TNewResult> ContinueWith<TResult, TNewResult>(this Task<TResult> @this, TaskContinuationOptions continuationOptions, Func<Task<TResult>, TNewResult> continuationFunction, CancellationToken cancellationToken, TaskScheduler scheduler)
        { return @this.ContinueWith(continuationFunction, cancellationToken, continuationOptions, scheduler); }
        public static Task<TNewResult> ContinueWith<TResult, TNewResult>(this Task<TResult> @this, TaskContinuationOptions continuationOptions, Func<Task<TResult>, object, TNewResult> continuationFunction, object state, CancellationToken cancellationToken, TaskScheduler scheduler)
        { return @this.ContinueWith(continuationFunction, state, cancellationToken, continuationOptions, scheduler); }


    }
}
