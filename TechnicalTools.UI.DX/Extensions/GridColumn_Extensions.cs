﻿using System;
using System.Diagnostics;
using System.Linq;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;

namespace TechnicalTools.UI.DX
{
    public static class GridColumn_Extensions
    {
        public static TGridColumn CenterDataText<TGridColumn>(this TGridColumn bcol)
            where TGridColumn : GridColumn
        {
            bcol.AppearanceCell.Options.UseTextOptions = true;
            bcol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            return bcol;
        }

        public static GridColumn CenterHeaderText(this GridColumn bcol)
        {
            bcol.AppearanceHeader.Options.UseTextOptions = true;
            bcol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            return bcol;
        }

        public static void InsertAfter(this GridColumn col, GridColumn parentCol)
        {
            if (col is BandedGridColumn bCol)
            {
                int index = ((BandedGridColumn)parentCol)?.ColIndex + 1 ?? 0;
                bCol.OwnerBand = ((BandedGridColumn)parentCol)?.OwnerBand ?? ((BandedGridView)col.View).Bands.FirstOrDefault();
                bCol.Visible = true;
                bCol.OwnerBand?.Columns.MoveTo(index, bCol);
            }
            else
            {
                col.View.Columns.Add(col); // if col already inside, this line dont add it again
                col.VisibleIndex = parentCol?.VisibleIndex + 1 ?? 0;
                col.Visible = true;
            }
        }

        public static void AddColumnAtRight(this GridColumn col, GridView view)
        {
            Debug.Assert(view != null);
            AddColumnAtRight(col, null, view);
        }
        public static void AddColumnAtRight(this GridColumn col, GridBand band)
        {
            Debug.Assert(band != null);
            Debug.Assert(band.View != null);
            AddColumnAtRight(col, band, band.View);
        }
        static void AddColumnAtRight(GridColumn col, GridBand band, GridView view)
        {
            Debug.Assert(col != null);
            if (col is BandedGridColumn bCol)
            {
                int index = band.Columns.Cast<BandedGridColumn>().Select(c => c.ColIndex).DefaultIfEmpty(-1).Max() + 1;
                bCol.OwnerBand = band;
                bCol.Visible = true;
                bCol.OwnerBand.Columns.MoveTo(index, bCol);
            }
            else
            {
                int index = view.Columns.Select(c => c.VisibleIndex).DefaultIfEmpty(-1).Max() + 1;
                view.Columns.Add(col);
                col.VisibleIndex = index;
                col.Visible = true;
            }
        }

    }
}
