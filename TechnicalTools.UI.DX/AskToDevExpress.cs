﻿using System;

using TechnicalTools.Annotations;


namespace TechnicalTools.UI.DX
{
    public class AskToDevExpressAttribute : ToDoAttribute
    {
        public AskToDevExpressAttribute(string comment = null)
            : base (comment)
        {
        }
    }
}
