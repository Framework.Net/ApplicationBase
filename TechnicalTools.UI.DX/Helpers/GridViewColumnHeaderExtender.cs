﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using DevExpress.Skins;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Drawing;
using DevExpress.Utils.Drawing;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;


namespace TechnicalTools.UI.DX.Helpers
{
    // Inspired from https://github.com/DevExpress-Examples/gridcontrol-how-to-add-a-check-box-to-a-column-header-t325446
    public class GridViewColumnHeaderExtender : Component
    {
        public GridView View
        {
            get
            {
                return _view;
            }
            set
            {
                if (_view == value)
                    return;
                ViewEvents(false);
                _view = value;
                ViewEvents(true);
            }
        }
        private GridView _view = null;

        public readonly HashSet<GridColumn> ColumnsToExtend = new HashSet<GridColumn>();


        public class ColumnStateRepository
        {
            public bool        Checked { get; set; }
            public ObjectState State   { get; set; }
        }
        public class ColumnCheckedChangedEventArgs : EventArgs
        {
            public bool       Checked { get; set; }
            public GridColumn Column  { get; private set; }

            public ColumnCheckedChangedEventArgs(GridColumn col, bool @checked)
            {
                Column = col;
                Checked = @checked;
            }
        }

        public GridViewColumnHeaderExtender()
        {
        }

        void OnMouseUp(object sender, MouseEventArgs e)
        {
            var col = GetHoveredColumnHeader(e.Location);
            if (col == null)
                return;
            if (CheckBoxContainsCursor(e.Location, col))
            {
                var state = GetState(col);
                state.Checked = !state.Checked;
                RaiseColumnCheckedChanged(new ColumnCheckedChangedEventArgs(col, state.Checked));
                SetCheckBoxState(col, ObjectState.Normal);
                DXMouseEventArgs.GetMouseArgs(e).Handled = true;
            }
        }

        void OnMouseMove(object sender, MouseEventArgs e)
        {
            var col = GetHoveredColumnHeader(e.Location);
            if (col == null)
                return;
            ObjectState state = ObjectState.Normal;
            if (CheckBoxContainsCursor(e.Location, col))
                state = ObjectState.Hot;
            SetCheckBoxState(col, state);
        }

        void OnMouseDown(object sender, MouseEventArgs e)
        {
            var col = GetHoveredColumnHeader(e.Location);
            if (col == null)
                return;
            if (CheckBoxContainsCursor(e.Location, col))
                SetCheckBoxState(col, ObjectState.Pressed);
        }

        void view_MouseLeave(object sender, EventArgs e)
        {
            foreach(var col in ColumnsToExtend)
               View.InvalidateColumnHeader(col);
        }

        private void SetCheckBoxState(GridColumn column, ObjectState state)
        {
            GetState(column).State = state;
            View.InvalidateColumnHeader(column);
        }

        private GridColumn GetHoveredColumnHeader(Point pt)
        {
            GridHitInfo hitInfo = View.CalcHitInfo(pt);
            var col = ColumnsToExtend.Contains(hitInfo.Column)
                    ? hitInfo.Column
                    : null;
            var inHeader = hitInfo.HitTest == GridHitTest.Column;
            return inHeader ? col : null;
        }

        private bool CheckBoxContainsCursor(Point point, GridColumn col)
        {
            Rectangle rect = CalcCheckBoxRectangle(col);
            return rect.Contains(point);
        }

        private Rectangle CalcCheckBoxRectangle(GridColumn col)
        {
            GraphicsInfo.Default.AddGraphics(null);
            GridViewInfo viewInfo = View.GetViewInfo() as GridViewInfo;
            GridColumnInfoArgs columnArgs = viewInfo.ColumnsInfo[col];
            Rectangle rect;
            try {
                rect = GetCheckBoxRectangle(columnArgs, GraphicsInfo.Default.Graphics);
            }
            finally {
                GraphicsInfo.Default.ReleaseGraphics();
            }

            return rect;
        }

        private Rectangle GetCheckBoxRectangle(GridColumnInfoArgs columnArgs, Graphics gr)
        {
            Rectangle columnRect = columnArgs.Bounds;
            int innerElementsWidth = CalcInnerElementsMinWidth(columnArgs, gr);
            Rectangle Rect = new Rectangle(columnRect.Right - innerElementsWidth - checkBoxSize.Width - 5,
                columnRect.Y + columnRect.Height / 2 - checkBoxSize.Height / 2, checkBoxSize.Width, checkBoxSize.Height);
            return Rect;
        }
        readonly Size checkBoxSize = new Size(14, 14);

        private int CalcInnerElementsMinWidth(GridColumnInfoArgs columnArgs, Graphics gr)
        {
            bool canDrawMode = true;
            return columnArgs.InnerElements.CalcMinSize(gr, ref canDrawMode).Width;
        }

        void OnCustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            if (e.Column == null)
                return;
            DefaultDrawColumnHeader(e);

            if (CanDrawCheckBox(e.Column))
                DrawCheckBox(e);

            e.Handled = true;
        }

        public void SetStatus(GridColumn col, bool @checked)
        {
            var state = GetState(col);
            if (state.Checked == @checked)
                return;
            state.Checked = @checked;
            RaiseColumnCheckedChanged(new ColumnCheckedChangedEventArgs(col, state.Checked));
        }
        private ColumnStateRepository GetState(GridColumn col)
        {
            if (!__checkStatus.TryGetValue(col, out ColumnStateRepository res))
                __checkStatus[col] = res = new ColumnStateRepository { State = ObjectState.Normal, Checked = false };
            return res;
        }
        readonly Dictionary<GridColumn, ColumnStateRepository> __checkStatus = new Dictionary<GridColumn, ColumnStateRepository>();

        private bool CanDrawCheckBox(GridColumn col)
        {
            return ColumnsToExtend.Contains(col);
        }

        private void DrawCheckBox(ColumnHeaderCustomDrawEventArgs e)
        {
            int index = 0;
            GetState(e.Column);
            ColumnStateRepository temp = GetState(e.Column);
            int offset = temp.Checked == true ? 4 : 0;
            switch (temp.State)
            {
                case ObjectState.Normal:
                    index = offset;
                    break;
                case ObjectState.Hot:
                    index = offset + 1;
                    break;
                case ObjectState.Hot | ObjectState.Pressed:
                    index = offset + 2;
                    break;
            }
            Rectangle rect = CalcCheckBoxRectangle(e.Column);
            e.Graphics.DrawImage(CheckBoxCollection.Images[index], rect);
        }

        private ImageCollection CheckBoxCollection
        {
            get
            {
                if (_checkBoxCollection == null)
                    _checkBoxCollection = GetCheckBoxImages();
                return _checkBoxCollection;
            }
        }
        ImageCollection _checkBoxCollection = null;

        protected virtual ImageCollection GetCheckBoxImages()
        {
            Skin skin = EditorsSkins.GetSkin(DevExpress.LookAndFeel.UserLookAndFeel.Default);
            SkinElement skinElement = skin["CheckBox"];
            if (skinElement == null)
                return null;
            return skinElement.Image.GetImages();
        }

        private void DefaultDrawColumnHeader(ColumnHeaderCustomDrawEventArgs e)
        {
            e.Painter.DrawObject(e.Info);
        }

        private void ViewEvents(bool subscribe)
        {
            if (View == null)
                return;
            if (!subscribe)
            {
                View.CustomDrawColumnHeader -= OnCustomDrawColumnHeader;
                View.MouseDown -= OnMouseDown;
                View.MouseUp -= OnMouseUp;
                View.MouseMove -= OnMouseMove;
                View.MouseLeave -= view_MouseLeave;
                return;
            }

            View.CustomDrawColumnHeader += OnCustomDrawColumnHeader;
            View.MouseDown += OnMouseDown;
            View.MouseUp += OnMouseUp;
            View.MouseMove += OnMouseMove;
            View.MouseLeave += view_MouseLeave;
        }

        protected override void Dispose(bool disposing)
        {
            ViewEvents(false);
            base.Dispose(disposing);
        }

        public event EventHandler<ColumnCheckedChangedEventArgs> ColumnCheckedChanged;
        public virtual void RaiseColumnCheckedChanged(ColumnCheckedChangedEventArgs ea)
        {
            ColumnCheckedChanged?.Invoke(View, ea);
        }

    }
}
