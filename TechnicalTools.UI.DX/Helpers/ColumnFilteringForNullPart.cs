﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;

using DevExpress.Data.Filtering;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


namespace TechnicalTools.UI.DX.Helpers
{
    /// <summary>
    /// Add ability for user to filter nested property null / not null
    /// For example if a column is bound to Foo.Bar
    /// if bar is "int" (ie: not nullable), actually cell can be blank if Foo is null
    /// The filter added work on Foo, not bar.
    /// Example of use:
    /// <code>
    /// var manager = new ColumnFilteringForNullPart(gridView1);
    /// manager.Install(new[] { col1, col2 }, nameof(BusinessObject.Foo));
    /// </code>
    /// </summary>
    public class ColumnFilteringForNullPart
    {
        readonly GridView _view;
        readonly RepositoryItemButtonEdit _edit;
        public ColumnFilteringForNullPart(GridView view)
        {
            _view = view;
            _edit = new RepositoryItemButtonEdit();
            _edit.Buttons.Clear();
            _edit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            _edit.ContextImageOptions.Image = NotNullFilter16X16;
            // When user click on editor in auto filter row
            _edit.Enter += (sender, e) =>
            {
                var btn = (ButtonEdit)sender;
                btn.Properties.ContextImageOptions.Image = NotNullFilter16X16;
            };
        }
        const string NotNullFilter16X16AsBase64 = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAALGPC/xhBQAAABp0RVh0VGl0bGUASWdub3JlIEZpbHRlcjtGaWx0ZXKz7HjLAAADVUlEQVQ4T2XTe0xSURwH8GsP/yr7o39bW1u1VZYlQszMjTSbq6zYmiWSGqJGqJGBpFYKlBlWZmCQOTB7uHxsaa+tspdbafYQRUITLVEJEkXFJGvf7mXpepzte8/dOff3+f1xziXIMVtwKG9Jlkx9/JhC05Cj0BjJGdnyS5CeUBnFOUVPDkmVJ+L5kmXUt2R8ABDTIbJyVRmq0hrP69YPGHQMYdTthnty0hvXuBt9g3Y0NhtQcOGq56Do5BEK+QsQZuSP27468bSxBZqyKsTyjiImIdMb6p1ao/asNgf4qbJhEvD9C+AL8+p0N+/C3GPFxKRnpjuVsYkJWPoG0dhixFn1DSQkZdf/B+zjSwVZeSrU3nsOm8MJz/cpmLp68aLJgNKrt1GorUH+xRtIlygRzRUJSWBODTOEmA6xlha6kC/MtRVfrsbLN0bYh0bQ9M6E2rtPcbbkpvPMpaqGbIVGHcvLTF20eKkfCcwlM2sGoMTdnLR9AtEpFGkq0dJqxv2G11DrartlhTqthpuiLw8NM1cxQ1zVzJCvlYzgR7qg9XuoOjI+FEA9fNnRAh5PkDuVLS9B2bV6Jzlrr2zZ8ayeFYYOCR/9hRmwKg/DeISHOhYLFXRmkReJT8qZQSK2ciIThXK8NXQ2VKSI9HciNsOmlWGs6TEmv/TDbTGhX56M3qNc1IWHQ7uOwSbi+FlENDdjGvFLTlOg3WRRX2dtsbSLeRh99RDfp37i28gwrAUi9IjYaI8NwxvuVpTTmC8JLk/qBX4jfgdE+Wjr6JJW0Df86C8UwT3QB/fQMD6fSkN36k4Yk6IwbGqDOWUb9DSmi+AkZP4JLBCKlei2fJLqg4J/WAvSMWI2olcuRJdgOwz7t8FlH4Kzx4KOxEiUrmOOE3vjJURMvJiIiRNTwLxde4QPozmH1cX0EMvbVC66JbHoTI7Eu7hIjNgdcLk9sFaX4wWbhZIAeiNV9OegjmY+mQWKlQHcaxtD0XFgB5p3BsNuaIXj40f03dKjTbAb5Yz1OL1yLXvmSk5HHUAnVGvoXuycf+CZK4xgPCO7tXI24T2ZJ1GhuBzIgHJVYJ634b9A8eog4oJ/EAVQpzJHtiIggoQenPenfTnnTxsgC+/lLl8TRu6Rvzbh8wtgmS1O4c78xgAAAABJRU5ErkJggg==";
        public static readonly Image NotNullFilter16X16 = Image.FromStream(new MemoryStream(Convert.FromBase64String(NotNullFilter16X16AsBase64)));

        Dictionary<GridColumn, bool> _colNullFilterEnabled = new Dictionary<GridColumn, bool>();
        string _nestedProperty;

        public void Install(IEnumerable<GridColumn> cols, string nestedProperty)
        {
            _nestedProperty = nestedProperty;
            _view.PopupMenuShowing += view_PopupMenuShowing;

            foreach (GridColumn column in cols)
                _colNullFilterEnabled.Add(column, false);

            _view.CustomRowCellEdit += view_CustomRowCellEdit;
            _view.CustomDrawCell += view_CustomDrawCell;
        }

        public void Uninstall()
        {
            _colNullFilterEnabled.Clear();
            _view.PopupMenuShowing -= view_PopupMenuShowing;
            _view.CustomRowCellEdit -= view_CustomRowCellEdit;
            _view.CustomDrawCell -= view_CustomDrawCell;
        }

        private void view_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle == GridControl.AutoFilterRowHandle &&
                _colNullFilterEnabled.ContainsKey(e.Column) &&
                _colNullFilterEnabled[e.Column])
            {
                var editorViewInfo = (e.Cell as GridCellInfo).ViewInfo as TextEditViewInfo;
                editorViewInfo.ContextImage = NotNullFilter16X16;
            }
        }

        private void view_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle == GridControl.AutoFilterRowHandle &&
                _colNullFilterEnabled.ContainsKey(e.Column) &&
                _colNullFilterEnabled[e.Column])
            {
                e.RepositoryItem = _edit;
            }
        }

        private void view_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.MenuType == GridMenuType.AutoFilter &&
                _colNullFilterEnabled.ContainsKey(_view.FocusedColumn))
            {
                DXMenuCheckItem item = new DXMenuCheckItem();
                item.ImageOptions.Image = NotNullFilter16X16;
                item.Caption = "Has Data";
                item.Tag = this;
                var column = _view.FocusedColumn;
                if (_colNullFilterEnabled[column])
                    foreach (DXMenuCheckItem it in e.Menu.Items)
                        it.Checked = false;
                item.Checked = _colNullFilterEnabled[column];
                e.Menu.Items.Add(item);
                e.Menu.ItemClick += Menu_ItemClick;
            }
        }

        private void Menu_ItemClick(object sender, DXMenuItemEventArgs e)
        {
            var column = _view.FocusedColumn;
            if (e.Item.Tag == this)
            {
                //gridView1.CloseEditor();
                _colNullFilterEnabled[column] = !_colNullFilterEnabled[column];
                Debug.Assert(column.FieldName.Contains(_nestedProperty + "."));
                if (_colNullFilterEnabled[column])
                    _view.ActiveFilterCriteria &= CriteriaOperator.Parse("Not IsNullOrEmpty([" + _nestedProperty + "])");
                else
                    _view.ActiveFilterCriteria = CriteriaPatcherSkipProperties.Patch(_view.ActiveFilterCriteria, _nestedProperty);
            }
            else
            {
                _view.CloseEditor();
                _colNullFilterEnabled[column] = false;
            }
        }
    }
}
