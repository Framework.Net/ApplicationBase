﻿using System;
using System.Collections.Generic;

using DevExpress.Data.Filtering;
using DevExpress.Data.Filtering.Helpers;


namespace TechnicalTools.UI.DX.Helpers
{
    // From https://devexpress.com/support/center/Question/Details/E3396
    // from https://github.com/DevExpress-Examples/XDL_how-to-delete-all-criteria-corresponding-to-a-particular-field-from-criteriaoperator-e3396/blob/18.1.4%2B/CS/PatchCriteriaExample/Filtering/CriteriaPatcherSkipProperties.cs
    public class CriteriaPatcherSkipProperties : ClientCriteriaLazyPatcherBase.AggregatesCommonProcessingBase
    {
        readonly IList<string> _propertiesToremove;

        private CriteriaPatcherSkipProperties(string propertiesToRemove)
        {
            _propertiesToremove = new List<string>(propertiesToRemove.Split(','));
        }

        public static CriteriaOperator Patch(CriteriaOperator source, string propertiesToRemove)
        {
            return new CriteriaPatcherSkipProperties(propertiesToRemove).Process(source);
        }

        private static bool IsNull(CriteriaOperator theOperator)
        {
            return ReferenceEquals(theOperator, null);
        }

        public override CriteriaOperator Visit(OperandProperty theOperand)
        {
            if (_propertiesToremove.Contains(theOperand.PropertyName))
                return null;
            return theOperand;
        }

        public override CriteriaOperator Visit(AggregateOperand theOperand)
        {
            CriteriaOperator collectionProperty = Visit(theOperand.CollectionProperty);
            if (IsNull(collectionProperty))
                return null;
            CriteriaOperator patched = base.Visit(theOperand);
            if (ReferenceEquals(theOperand, patched))
                return theOperand;
            return null;
        }

        public override CriteriaOperator Visit(BetweenOperator theOperator)
        {
            theOperator = (BetweenOperator)base.Visit(theOperator);
            if (IsNull(theOperator.BeginExpression) ||
                IsNull(theOperator.EndExpression) ||
                IsNull(theOperator.TestExpression))
                return null;
            return theOperator;
        }

        public override CriteriaOperator Visit(BinaryOperator theOperator)
        {
            theOperator = (BinaryOperator)base.Visit(theOperator);
            if (IsNull(theOperator.LeftOperand) ||
                IsNull(theOperator.RightOperand))
                return null;
            return theOperator;
        }

        public override CriteriaOperator Visit(FunctionOperator theOperator)
        {
            var result = (FunctionOperator)base.Visit(theOperator);
            if (!ReferenceEquals(theOperator, result))
                return null;
            return result;
        }

        public override CriteriaOperator Visit(InOperator theOperator)
        {
            var result = (InOperator)base.Visit(theOperator);
            if (IsNull(result.LeftOperand))
                return null;
            if (ReferenceEquals(theOperator.Operands, result.Operands))
                return theOperator;
            CriteriaOperatorCollection filteredOperands = RemoveEmptyOperands(result.Operands);
            if (filteredOperands.Count == 0)
                return null;
            if (filteredOperands.Count == 1)
                return new BinaryOperator(theOperator.LeftOperand, filteredOperands[0], BinaryOperatorType.Equal);
            return new InOperator(theOperator.LeftOperand, filteredOperands);
        }

        public override CriteriaOperator Visit(UnaryOperator theOperator)
        {
            theOperator = (UnaryOperator)base.Visit(theOperator);
            if (IsNull(theOperator.Operand))
                return null;
            return theOperator;
        }

        private static CriteriaOperatorCollection RemoveEmptyOperands(CriteriaOperatorCollection source)
        {
            var result = new CriteriaOperatorCollection();
            foreach (CriteriaOperator operand in source)
                if (!IsNull(operand)) result.Add(operand);
            return result;
        }
    }
}
