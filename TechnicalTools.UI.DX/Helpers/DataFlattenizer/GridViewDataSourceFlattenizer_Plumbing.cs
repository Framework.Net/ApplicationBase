﻿using System;


namespace TechnicalTools.UI.DX
{
    public interface IEnhancedGridView_GridViewDataSourceFlattenizer
    {
        Helpers.GridViewDataSourceFlattenizer DataSourceFlattenizer { get; set; }
        //void AddGetRowSubstitute(Func<object, object> getRowSubstitute);
        //void RemoveGetRowSubstitute(Func<object, object> getRowSubstitute);
    }
    public partial interface IEnhancedGridView : IEnhancedGridView_GridViewDataSourceFlattenizer
    {
    }

    public partial class EnhancedGridView : IEnhancedGridView_GridViewDataSourceFlattenizer
    {
        Helpers.GridViewDataSourceFlattenizer IEnhancedGridView_GridViewDataSourceFlattenizer.DataSourceFlattenizer { get; set; }
        //readonly List<Func<object, object>> _GetRowSubstitutes = new List<Func<object, object>>();
        //void IEnhancedGridView_GridViewDataSourceFlattenizer.AddGetRowSubstitute   (Func<object, object> getRowSubstitute) { _GetRowSubstitutes.Add(getRowSubstitute); }
        //void IEnhancedGridView_GridViewDataSourceFlattenizer.RemoveGetRowSubstitute(Func<object, object> getRowSubstitute) { _GetRowSubstitutes.Remove(getRowSubstitute); }
        //public override object GetRow(int rowHandle)
        //{
        //    var obj = base.GetRow(rowHandle);
        //    foreach (var grs in _GetRowSubstitutes)
        //        obj = grs(obj);
        //    return obj;
        //}
    }
    public partial class EnhancedBandedGridView : IEnhancedGridView_GridViewDataSourceFlattenizer
    {
        Helpers.GridViewDataSourceFlattenizer IEnhancedGridView_GridViewDataSourceFlattenizer.DataSourceFlattenizer { get; set; }
        //readonly List<Func<object, object>> _GetRowSubstitutes = new List<Func<object, object>>();
        //void IEnhancedGridView_GridViewDataSourceFlattenizer.AddGetRowSubstitute   (Func<object, object> getRowSubstitute) { _GetRowSubstitutes.Add(getRowSubstitute); }
        //void IEnhancedGridView_GridViewDataSourceFlattenizer.RemoveGetRowSubstitute(Func<object, object> getRowSubstitute) { _GetRowSubstitutes.Remove(getRowSubstitute); }
        //public override object GetRow(int rowHandle)
        //{
        //    var obj = base.GetRow(rowHandle);
        //    foreach (var grs in _GetRowSubstitutes)
        //        obj = grs(obj);
        //    return obj;
        //}
    }
}
