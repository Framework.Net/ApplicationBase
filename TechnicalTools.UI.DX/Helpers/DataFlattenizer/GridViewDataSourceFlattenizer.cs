﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools;
using TechnicalTools.Model;


namespace TechnicalTools.UI.DX.Helpers
{
    /// <summary>
    /// Helper to add automatically exploration and flattenization of data to a gridview.
    /// This class apply to a gridview where data source is List of T.
    /// This is instances of IHierarchyDescription that defines how to customize gridview
    /// <see cref="FlattenizerInstallation"/>
    /// </summary>
    public partial class GridViewDataSourceFlattenizer : IDisposable
    {
        readonly GridView _view; IEnhancedGridView _viewAsEnhanced { get { return (IEnhancedGridView)_view; } }
        readonly GridViewHierarchyManager _gridViewHierarchyManager;
        Binding _base;
        Binding _current;

        public bool IsInstalled { get { return (_view as IEnhancedGridView_GridViewDataSourceFlattenizer).DataSourceFlattenizer == this; } }
        public bool Enabled { get; set; } = true;
        public bool AllowLeftOuterJoin { get; set; } = true;
        public bool IsDataSourceCurrentlyFlattenized { get { return _current?.PreviousBinding != null; } }
        public string CurrentFlattenizationPath
        {
            get
            {
                if (_current == null || _current == _base)
                    return null;
                string res = "";
                foreach (var dp in _current.DescriptionPath)
                    res += (res.Length > 0 ? " => " : "") + dp.LevelName;
                return  res;
            }
        }

        /// <summary>
        /// Default and Minimum implementation is : e.View.PopulateColumns(e.Type);
        /// We could also use e.View.PopulateColumnsFromDataAnnotations(e.Type)
        /// </summary>
        public event EventHandler<PopulateEventArgs> PopulateColumnFor;
        public class PopulateEventArgs : EventArgs
        {
            public GridView View    { get; internal set; }
            public GridBand Band    { get; internal set; }
            public Type     Type    { get; internal set; }
            public bool     Handled { get; set; }
        }


        // I dont know how to call this class exactly for now...
        class Binding
        {
            public IReadOnlyCollection<object> DataSource { get; }
            public IReadOnlyCollection<IHierarchyDescription> ChildrenDescriptions { get; }
            public Binding PreviousBinding { get; }

            public List<GridBand> OriginalBands { get; set; }
            public List<GridColumn> OriginalColumns { get; set; }
            public Dictionary<IEnhancedGridColumn, Func<object, int, object>> OriginalUnboundValueGetter { get; set; }
            public Dictionary<IEnhancedGridColumn, Action<object, object>> OriginalUnboundValueSetter { get; set; }

            public Binding(IReadOnlyCollection<object> dataSource, IReadOnlyCollection<IHierarchyDescription> childrenDescriptions, Binding previousBinding)
            {
                DataSource = dataSource;
                ChildrenDescriptions = childrenDescriptions;
                PreviousBinding = previousBinding;
            }

            internal IReadOnlyList<IHierarchyDescription> DescriptionPath { get; set; }
        }

        public GridViewDataSourceFlattenizer(EnhancedGridView view, IReadOnlyCollection<IHierarchyDescription> originalRootHierarchyDescription)
            : this((GridView)view, originalRootHierarchyDescription)
        {
            _gridViewHierarchyManager = new GridViewHierarchyManager(view, originalRootHierarchyDescription, _GetRowBusinessObject, GetRowBusinessObject, true);
        }
        public GridViewDataSourceFlattenizer(EnhancedBandedGridView view, IReadOnlyCollection<IHierarchyDescription> originalRootHierarchyDescription)
            : this((GridView)view, originalRootHierarchyDescription)
        {
            _gridViewHierarchyManager = new GridViewHierarchyManager(view, originalRootHierarchyDescription, _GetRowBusinessObject, GetRowBusinessObject, true);
        }

        private GridViewDataSourceFlattenizer(GridView view, IReadOnlyCollection<IHierarchyDescription> originalRootHierarchyDescription)
        {
            if (view.ParentView != null && view.SourceView == null)
                throw new TechnicalException($"{nameof(GridViewDataSourceFlattenizer)} must be applied on view really displayed, not the model)!", null);
            _view = view;
            originalRootHierarchyDescription = originalRootHierarchyDescription ?? new List<IHierarchyDescription>();
            _base = new Binding(null, originalRootHierarchyDescription, null);
        }

        public void Install(IReadOnlyCollection<object> originalDatasource)
        {
            Unflattenize();
            _base = new Binding(originalDatasource, _base.ChildrenDescriptions, null);
            _current = _base;
            _gridViewHierarchyManager.Install();
            _view.PopupMenuShowing += view_PopupMenuShowing;
            _view.CellMerge += view_CellMerge;
            Debug.Assert((_view as IEnhancedGridView_GridViewDataSourceFlattenizer).DataSourceFlattenizer == null);
            (_view as IEnhancedGridView_GridViewDataSourceFlattenizer).DataSourceFlattenizer = this;
        }
        public void Uninstall()
        {
            _view.PopupMenuShowing -= view_PopupMenuShowing;
            _view.CellMerge -= view_CellMerge;
            _gridViewHierarchyManager.Uninstall();
            Unflattenize();
            _base = new Binding(null, _base.ChildrenDescriptions, null);
            _current = null;
            if ((_view as IEnhancedGridView_GridViewDataSourceFlattenizer).DataSourceFlattenizer == this)
                (_view as IEnhancedGridView_GridViewDataSourceFlattenizer).DataSourceFlattenizer = null;
        }
        public virtual void Dispose()
        {
            Uninstall();
        }

        public IHierarchyDescription FlattenizeTo(string levelName, IProgress<string> pr)
        {
            if (((_view.GridControl.DataSource as IReadOnlyCollection<object>)?.Count ?? 0) == 0)
                throw new TechnicalException("Datasource cannot be empty!", null);
            Unflattenize();
            Debug.Assert(_current.PreviousBinding == null);
            var menuWithSubMenus = BuildMenusFor(pr) as DXSubMenuItem;
            Debug.Assert(menuWithSubMenus != null);
            var interestingMenu = Search(levelName, menuWithSubMenus).OrderBy(k => k.Item1).FirstOrDefault()?.Item2;
            if (interestingMenu == null)
                throw new TechnicalException("No level/menu for \"" + levelName + "\"!", null);
            interestingMenu.GenerateClickEvent();
            return interestingMenu.Tag as IHierarchyDescription;
        }

        IEnumerable<Tuple<int, DXMenuItem>> Search(string levelName, DXSubMenuItem menu, int depth = 0)
        {
            foreach (DXMenuItem subItem in menu.Items)
                if ((subItem.Tag as IHierarchyDescription)?.LevelName == levelName)
                    yield return Tuple.Create(depth, subItem);
                else if (subItem is DXSubMenuItem)
                {
                    var res = Search(levelName, (DXSubMenuItem)subItem, depth + 1);
                    foreach(var subRes in res)
                    {
                         yield return subRes;
                    }
                }
        }


        public void Unflattenize()
        {
            if (_current?.PreviousBinding != null)
                ApplyUnflattenize();
            Debug.Assert(_base.PreviousBinding == null);
        }

        public object GetOriginalDataSource()
        {
            return _base.DataSource;
        }

        public object GetRowBusinessObject(int rowHandle)
        {
            return _GetRowBusinessObject(_view.GetRow(rowHandle));
        }

        public object GetRowBusinessObject(object rowObject)
        {
            return _GetRowBusinessObject(rowObject);
        }
        object _GetRowBusinessObject(int rowHandle)
        {
            return _GetRowBusinessObject(_view.GetRow(rowHandle));
        }

        object _GetRowBusinessObject(object rowObject)
        {
            if (_current == null || _current == _base)
                return rowObject;
            else if (rowObject is Join)
                return (rowObject as Join).parts[0];
            else
            {
                Debug.Assert(false, "In which case does it happen ?");
                return rowObject;
            }
        }

        void view_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            // We dont use sender here because of check in constructor
            if (_view.GridControl.DataSource == null)
                return;
            if (e.Menu == null)
                return;

            var menu = BuildMenusFor(null);
            if (e.Menu.Items.Count > 0)
                e.Menu.AddToGroup(ePopupMenuHeaderType.Custom, menu);
        }

        /// <summary>
        /// Build menu and give a IProgress instance to log progression.
        /// If instance is null, a dialog box is show to user
        /// </summary>
        /// <param name="pr"></param>
        /// <returns></returns>
        public DXMenuItem BuildMenusFor(IProgress<string> pr)
        {
            // Note @dev: if type of real returned object change, updat method Unflattenize
            if (_current.PreviousBinding != null)
                return new DXMenuItem(GetBackToPreviousViewCaption, (_, __) =>
                {
                    Action<IProgress<string>> action = ___ => ApplyUnflattenize();
                    if (pr == null)
                        _view.GridControl.ShowBusyWhileDoingUIWorkInPlace(action);
                    else
                        action(pr);
                });
            else
            {
                var ds = _current.DataSource; // Fix original value
                var localRoot = new HierarchyDescription("", (_) => ds, (_) => ds, null, childrenDescriptions: _current.ChildrenDescriptions);
                var mnu = BuildSubMenusFor(new DXSubMenuItem(FlattenizeAllCaption), localRoot, null, pr);
                mnu.Enabled = Enabled;

                // Don't lnow how to make it work yet... because Flattenizer cannot change _view.GridControl.DataSource
                // We have to find a way maybe using internal communication with DetailViewManager ?
                mnu.Enabled &= _view.ParentView == null;
                return mnu;
            }
        }
        public const string GetBackToPreviousViewCaption = "Get back to previous view";
        public const string FlattenizeAllCaption = "Flattenize all, at level...";

        DXSubMenuItem BuildSubMenusFor(DXSubMenuItem mnuTypeToFill, IHierarchyDescription hd, List<IHierarchyDescription> pathFromAncestorsToParent = null, IProgress<string> pr = null)
        {
            if (pathFromAncestorsToParent == null)
                pathFromAncestorsToParent = new List<IHierarchyDescription> { hd };

            if (hd.ChildrenDescriptions != null)
                foreach (var childHd in hd.ChildrenDescriptions)
                {
                    pathFromAncestorsToParent.Add(childHd);
                    var mnu = new DXSubMenuItem(childHd.LevelName);
                    BuildSubMenusFor(mnu, childHd, pathFromAncestorsToParent, pr);
                    var allParentPaths = pathFromAncestorsToParent.ToArray(); // fix values for lambda
                    mnu.Items.Insert(0, new DXMenuItem("All of them", (_, __) =>
                    {
                        Action<IProgress<string>> action = pr2 => ApplyFlattenizing(allParentPaths, pr2);
                        if (pr == null)
                            _view.GridControl.ShowBusyWhileDoingUIWorkInPlace(action);
                        else
                            action(pr);
                    })
                    { Tag = childHd }); // Tag is important for method FlattenizeTo and view_CellMerge
                    mnuTypeToFill.Items.Add(mnu);
                    pathFromAncestorsToParent.RemoveAt(pathFromAncestorsToParent.Count - 1);
                }

            return mnuTypeToFill;
        }


        void ApplyFlattenizing(IHierarchyDescription[] allParentPaths, IProgress<string> pr)
        {
            _current = _current ?? _base;
            var newDatasource = new DatasourceFlattenizationApplyer(allParentPaths, AllowLeftOuterJoin, pr.WrapReportWith("Rebuilding data...")).Apply();
            _view.BeginDataUpdate();
            var backupShowAutoFilterRow = _view.OptionsView.ShowAutoFilterRow;
            _view.OptionsView.ShowAutoFilterRow = true; // trick to prevent FocusedRowChanged event to be raised on row of intermediate type below
            _view.FocusedRowHandle = GridControl.AutoFilterRowHandle;
            _view.GridControl.DataSource = null;
            var bands = new List<GridBand>();
            List<GridColumn> originalColumns = _view.Columns.Cast<GridColumn>().ToList();
            var allFinalColumnsVisible = new Dictionary<GridColumn, bool>();
            var bview = _view as BandedGridView;
            for (int i = 0; i < allParentPaths.Length; ++i, originalColumns.Clear())
            {
                GridBand band = null;
                if (bview != null)
                {
                    band = originalColumns.Count > 0 ? bview.Bands.FirstOrDefault() : null;
                    if (bview != null && band == null)
                    {
                        band = new GridBand { Caption = allParentPaths[i].LevelName.IfBlankUse(" "), Name = "b" + LvlPropertyName(i) };
                        band.AppearanceHeader.Font = new Font(band.AppearanceHeader.Font, FontStyle.Bold);
                        band.AppearanceHeader.Options.UseFont = true;
                        band.AppearanceHeader.Options.UseTextOptions = true;
                        band.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                        band.Tag = Tuple.Create(allParentPaths[i], i);
                        bands.Add(band);
                    }
                    else
                    {
                        _current.OriginalBands = new List<GridBand>();
                        _current.OriginalBands.AddRange(bview.Bands);
                        bands.AddRange(bview.Bands);
                    }
                }


                // ReSharper disable once AccessToModifiedClosure
                //var itemSpecimen = newDatasource.Select(eo => eo.parts[i]).FirstOrDefault(item => item != null);
                var itemType = newDatasource.FirstOrDefault()?.GetPartType(i);
                if (itemType == null)
                    continue;
                if (originalColumns.Count == 0)
                {
                    if (bview != null)
                        bview.Bands.Clear();
                    _view.Columns.Clear();

                    var e = new PopulateEventArgs() { View = _view, Band = band, Type = itemType };
                    PopulateColumnFor?.Invoke(this, e);
                    if (!e.Handled)
                    {
                        // According to doc of GridViewDataSourceFlattenizer.PopulateColumnFor this is the default minimum implementation to do
                        _view.PopulateColumns(itemType);
                        foreach (GridColumn col in _view.Columns)
                        {
                            col.Caption = col.FieldName.Uncamelify();
                            col.Visible = true;
                        }
                    }
                    foreach (GridColumn col in _view.Columns.OrderBy(c => c.AbsoluteIndex))
                    {
                        col.FieldName = LvlPropertyName(i) + "." + col.FieldName;
                        if (bview != null)
                            band.Columns.Add((BandedGridColumn)col);
                        allFinalColumnsVisible.Add(col, col.Visible);
                    }
                }
                else
                {
                    _current.OriginalColumns = new List<GridColumn>();
                    _current.OriginalUnboundValueGetter = new Dictionary<IEnhancedGridColumn, Func<object, int, object>>();
                    _current.OriginalUnboundValueSetter = new Dictionary<IEnhancedGridColumn, Action<object, object>>();
                    _current.OriginalColumns.AddRange(originalColumns);
                    foreach (var col in originalColumns)
                    {
                        if (col.UnboundType == DevExpress.Data.UnboundColumnType.Bound)
                        {
                            if (col.GetTextCaption().Replace("_", " ") == col.FieldName.Uncamelify())
                                col.Caption = col.FieldName.Uncamelify(); // fix value
                            col.FieldName = LvlPropertyName(i) + "." + col.FieldName;
                        }
                        else
                        {
                            var eCol = col as IEnhancedGridColumn;
                            if (eCol.UnboundValueGetter != null)
                            {
                                var originalGetter = eCol.UnboundValueGetter;
                                _current.OriginalUnboundValueGetter[eCol] = originalGetter;
                                eCol.UnboundValueGetter = (object row, int listSourceRowIndex) => originalGetter(_GetRowBusinessObject(row), listSourceRowIndex);
                            }
                            if (eCol.UnboundValueSetter != null)
                            {
                                var originalSetter = eCol.UnboundValueSetter;
                                _current.OriginalUnboundValueSetter[eCol] = originalSetter;
                                eCol.UnboundValueSetter = (object row, object value) => originalSetter(_GetRowBusinessObject(row), value);
                            }
                        }
                    }
                    foreach (var col in originalColumns.OrderBy(c => c.AbsoluteIndex))
                        allFinalColumnsVisible.Add(col, col.Visible);
                }
                foreach (GridColumn col in _view.Columns)
                    if (string.IsNullOrWhiteSpace(col.CustomizationCaption))
                        col.CustomizationCaption = (col as BandedGridColumn)?.OwnerBand?.Caption.IfNotBlankAddSuffix(" | ") + col.FieldName.Split('.').Last();
            }
            if (bview != null)
                bview.Bands.Clear();
            _view.Columns.Clear();
            if (bview != null)
                bview.Bands.AddRange(bands.ToArray());
            _view.Columns.AddRange(allFinalColumnsVisible.Keys.ToArray());
            foreach (var kvp in allFinalColumnsVisible)
                kvp.Key.Visible = kvp.Value;
            _current = new Binding(newDatasource, allParentPaths.Last().ChildrenDescriptions, _current);
            _current.DescriptionPath = allParentPaths;
            _view.GridControl.DataSource = newDatasource; // must be after changing _current because of the event "changing the datasource" can occur
            _view.OptionsView.ShowAutoFilterRow = backupShowAutoFilterRow; // end of trick
            _view.EndDataUpdate();
        }

        void ApplyUnflattenize()
        {
            _view.BeginDataUpdate();
            _current = _current.PreviousBinding;
            var bview = _view as BandedGridView;
            if (bview != null)
                foreach (var band in bview.Bands.Except(_current.OriginalBands ?? Enumerable.Empty<GridBand>()).ToList())
                    bview.Bands.Remove(band);
            foreach (var col in _view.Columns.Except(_current.OriginalColumns ?? Enumerable.Empty<GridColumn>()).ToList())
                _view.Columns.Remove(col);
            _view.GridControl.DataSource = null;
            var b = _view.OptionsBehavior.AutoPopulateColumns;
            _view.OptionsBehavior.AutoPopulateColumns = false;
            try
            {
                _view.GridControl.DataSource = _current.DataSource;
            }
            finally
            {
                _view.OptionsBehavior.AutoPopulateColumns = b;
            }
            foreach (GridColumn col in _view.Columns)
            {
                if (col.UnboundType == DevExpress.Data.UnboundColumnType.Bound)
                {
                    col.FieldName = col.FieldName.Substring(LvlPropertyName(0).Length + ".".Length);
                    if (col.GetTextCaption().Replace("_", " ") == col.FieldName.Uncamelify())
                        col.Caption = null; // make value "free" again
                }
                else
                {
                    var eCol = col as IEnhancedGridColumn;
                    if (eCol.UnboundValueGetter != null &&
                        _current.OriginalUnboundValueGetter.ContainsKey(eCol)) // can be false if user change layout after isntalling flattenizing
                        eCol.UnboundValueGetter = _current.OriginalUnboundValueGetter[eCol];
                    if (eCol.UnboundValueSetter != null &&
                        _current.OriginalUnboundValueSetter.ContainsKey(eCol)) // can be false if user change layout after isntalling flattenizing
                        eCol.UnboundValueSetter = _current.OriginalUnboundValueSetter[eCol];
                }
            }
            _current.OriginalBands = null;
            _current.OriginalColumns = null;
            _current.OriginalUnboundValueGetter = null;
            _current.OriginalUnboundValueSetter = null;
            _view.EndDataUpdate();
        }


        void view_CellMerge(object sender, CellMergeEventArgs e)
        {
            if (_current == null || _current == _base)
                return;
            var band = (e.Column as BandedGridColumn)?.OwnerBand;
            if (band == null)
                return;

            var metadata = band.Tag as Tuple<IHierarchyDescription, int>;
            // metadata can be null for first band if we kept original band (and we flattenized of course)
            var lvl = metadata?.Item2 ?? 0;

            var join1 = (_view.DataSource as IList)[_view.GetDataSourceRowIndex(e.RowHandle1)] as Join;
            var join2 = (_view.DataSource as IList)[_view.GetDataSourceRowIndex(e.RowHandle2)] as Join;

            // If two cells display a value from the same object property
            // we allow default merging algorithm (ie: it will be merged... but it can also depends on other custom merging strategy...)
            // In all other case we deny merging (this is the only  responsability we take).
            // This make the merging intuitive even when user does not like merging cell in the first place
            if ((join2 == null) != (join1 == null) ||  // one is null and not the other
                !ReferenceEquals(join1.parts[lvl], join2.parts[lvl])) // or object on same join level are not the same
            {
                e.Merge = false;
                e.Handled = true;
            }
        }

        public static string LvlPropertyName(int i) { return Join.LvlPrefix + i; }
    }
}
