﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Model;


namespace TechnicalTools.UI.DX.Helpers
{
    /// <summary>
    /// Take a collection of IHierarchyDescription and manage to display as details view,
    /// using the master/details feature of DevExpress and a description of hierarchy.
    /// </summary>
    public class GridViewHierarchyManager
    {
        public   int MaxMemorySizeLoadableAtOnce { get; set; } = 100 * 1024 * 1024;
        public  bool Enabled                     { get; private set; }

        public GridViewHierarchyManager(EnhancedGridView view, IReadOnlyCollection<IHierarchyDescription> childrenDescriptions, bool applyRecursively = true)
            : this((GridView)view, childrenDescriptions, true)
        {
            _enhancedView = view;
        }
        public GridViewHierarchyManager(EnhancedGridView view, IReadOnlyCollection<IHierarchyDescription> childrenDescriptions, Func<int, object> getRowBusinessObjectFromRowHandle, Func<object, object> getRowBusinessObjectFromObject, bool applyRecursively = true)
            : this((GridView)view, childrenDescriptions, getRowBusinessObjectFromRowHandle, getRowBusinessObjectFromObject, true)
        {
            _enhancedView = view;
        }
        public GridViewHierarchyManager(EnhancedBandedGridView view, IReadOnlyCollection<IHierarchyDescription> childrenDescriptions, bool applyRecursively = true)
            : this((GridView)view, childrenDescriptions, true)
        {
            _enhancedView = view;
        }
        public GridViewHierarchyManager(EnhancedBandedGridView view, IReadOnlyCollection<IHierarchyDescription> childrenDescriptions, Func<int, object> getRowBusinessObjectFromRowHandle, Func<object, object> getRowBusinessObjectFromObject, bool applyRecursively = true)
            : this((GridView)view, childrenDescriptions, getRowBusinessObjectFromRowHandle, getRowBusinessObjectFromObject, true)
        {
            _enhancedView = view;
        }
        private GridViewHierarchyManager(GridView view, IReadOnlyCollection<IHierarchyDescription> childrenDescriptions, bool applyRecursively = true)
            : this(view, childrenDescriptions, null, null, true)
        {
        }
        private GridViewHierarchyManager(GridView view, IReadOnlyCollection<IHierarchyDescription> childrenDescriptions, Func<int, object> getRowBusinessObjectFromRowHandle, Func<object, object> getRowBusinessObjectFromObject, bool applyRecursively)
        {
            _view = view;
            _applyRecursively = applyRecursively;
            _getRowBusinessObjectFromRowHandle = getRowBusinessObjectFromRowHandle;
            if (_getRowBusinessObjectFromRowHandle == null)
                _getRowBusinessObjectFromRowHandle = _view.GetRow;
            _getRowBusinessObjectFromObject = getRowBusinessObjectFromObject;
            if (_getRowBusinessObjectFromObject == null)
                _getRowBusinessObjectFromObject = x => x;
            _preloadedDataHolder = childrenDescriptions == null ? null : new object[childrenDescriptions.Count];
            _childrenDescriptions = childrenDescriptions;
            _detailView = _childrenDescriptions?.Select(BuildDetailView).ToList();
        }
        readonly GridView _view;
        readonly IEnhancedGridView _enhancedView;
        readonly IReadOnlyCollection<IHierarchyDescription> _childrenDescriptions;
        readonly IReadOnlyList<GridView_DetailViewManager.DetailView> _detailView;
        readonly bool _applyRecursively;
        readonly Func<int, object> _getRowBusinessObjectFromRowHandle;
        readonly Func<object, object> _getRowBusinessObjectFromObject;
        readonly object[] _preloadedDataHolder;

        public void Install()
        {
            Enabled = true;
            if (_detailView != null)
                foreach (var dv in _detailView)
                    _enhancedView.DetailViewManager.Add(dv);
            _view.RefreshData();
        }


        public void Uninstall()
        {
            Enabled = false;
            if (_detailView != null)
                foreach (var dv in _detailView)
                    _enhancedView.DetailViewManager.Remove(dv);
            _view.RefreshData();
        }

        protected virtual DetailView BuildDetailView(IHierarchyDescription desc)
        {
            return new DetailView(this, desc);
        }

        protected class DetailView : GridView_DetailViewManager.DetailView
        {
            readonly GridViewHierarchyManager _owner;
            readonly IHierarchyDescription _desc;

            public DetailView(GridViewHierarchyManager owner, IHierarchyDescription desc)
            {
                _owner = owner;
                _desc = desc;
            }

            public override bool HasMeaningAndIsEnabledFor(GridView sender, int rowHandle)
            {
                return _owner.Enabled;
            }

            public override string GetName(GridView sender, int rowHandle)
            {
                return _desc.LevelName;
            }

            public override Type BaseTypeOfDetailObject { get { return _desc.ChildType ?? base.BaseTypeOfDetailObject;; } }

            public override bool? IsEmpty(GridView sender, int rowHandle)
            {
                return null;
            }

            public override Task<IReadOnlyCollection<object>> GetChildList(GridView view, int rowHandle)
            {
                var line = _owner._getRowBusinessObjectFromRowHandle(rowHandle);
                var st = _owner.GetStack();
                st.Push(line);
                var t = Task.Factory.StartNew(() => _desc.GetChildren(st), CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);
                t.ContinueWithAwaitFalse(_ =>
                {
                    st.Pop();
                    _owner.FreeStack(st);
                });
                return t;
            }


            public override void Expanded(GridView sender, int rowHandle, BaseView clonedDetailView)
            {
                base.Expanded(sender, rowHandle, clonedDetailView);
                GridViewHierarchyManager manager = null;
                if (clonedDetailView is EnhancedGridView)
                    manager = new GridViewHierarchyManager(clonedDetailView as EnhancedGridView, _desc.ChildrenDescriptions, null, null, _owner._applyRecursively);
                else if (clonedDetailView is EnhancedBandedGridView)
                    manager = new GridViewHierarchyManager(clonedDetailView as EnhancedBandedGridView, _desc.ChildrenDescriptions, null, null, _owner._applyRecursively);
                if (manager != null)
                    manager.Install();
            }

            protected override object GetRow(GridView sender, int rowHandle)
            {
                return _owner._getRowBusinessObjectFromObject(base.GetRow(sender, rowHandle));
            }
        }


        // Repository of recycled stack (to avoid memory short lived object allocation)
        ConcurrentStack<Stack<object>> _stst = new ConcurrentStack<Stack<object>>();
        Stack<object> GetStack() { Stack<object> st; return _stst.TryPeek(out st) ? st : new Stack<object>(); }
        void FreeStack(Stack<object> st) { _stst.Push(st); }
    }
}
