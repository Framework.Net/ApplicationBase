﻿using System;
using System.Drawing;

using DevExpress.Accessibility;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;


namespace TechnicalTools.UI.DX.Controls
{
    public partial class EnhancedSearchLookUpEditRepositoryItem : RepositoryItemSearchLookUpEdit
    {
        public EnhancedSearchLookUpEditRepositoryItem()
        {
            this.QueryPopUp += EnhancedSearchLookUpEditRepositoryItem_QueryPopUp;
        }

        protected override ColumnView CreateViewInstance()
        {
            switch (ViewType)
            {
                case GridLookUpViewType.BandedView:
                    return new EnhancedBandedGridView();
                case GridLookUpViewType.AdvBandedView:
                    return base.CreateViewInstance();
                default:
                    return new EnhancedGridView();
            }
        }

        protected override void Setup(BaseView view)
        {
            if (view is GridView gridView)
                gridView.OptionsView.ShowGroupPanel = true;
            base.Setup(view);
        }

        private void EnhancedSearchLookUpEditRepositoryItem_QueryPopUp(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var editor = (SearchLookUpEdit)sender;
            var properties = editor.Properties;
            properties.PopupFormSize = new Size(editor.Width - 4, properties.PopupFormSize.Height);
        }

    }

    #region DevExpress designer part

    public partial class EnhancedSearchLookUpEdit
    {
        static EnhancedSearchLookUpEdit() { EnhancedSearchLookUpEditRepositoryItem.RegisterEnhancedSearchLookUpEdit(); }
        public override string EditorTypeName { get { return EnhancedSearchLookUpEditRepositoryItem.EnhancedSearchLookUpEditTypeName; } }
    }

    // The attribute that points to the registration method
    [UserRepositoryItem(nameof(EnhancedSearchLookUpEditTypeName))]
    public partial class EnhancedSearchLookUpEditRepositoryItem : RepositoryItemSearchLookUpEdit
    {
        // Static constructor should call registration method
        static EnhancedSearchLookUpEditRepositoryItem()
        {
            RegisterEnhancedSearchLookUpEdit();
        }

        // Unique name for custom control
        internal const string EnhancedSearchLookUpEditTypeName = nameof(EnhancedSearchLookUpEdit);
        public override string EditorTypeName { get { return EnhancedSearchLookUpEditTypeName; } }

        public static void RegisterEnhancedSearchLookUpEdit()
        {
            if (EditorRegistrationInfo.Default.Editors.Contains(EnhancedSearchLookUpEditTypeName))
                return;

            Image image = null;
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(EnhancedSearchLookUpEditTypeName,
                                                                           typeof(EnhancedSearchLookUpEdit),
                                                                           typeof(EnhancedSearchLookUpEditRepositoryItem),
                                                                           typeof(SearchLookUpEditBaseViewInfo),
                                                                           new ButtonEditPainter(), true,
                                                                           image,
                                                                           typeof(PopupEditAccessible)));
        }
        // Created this to examine when/where Create() methods are called
        public class MyEditorClassInfo : EditorClassInfo
        {
            public MyEditorClassInfo(string name, Type editorType, Type repositoryType, Type viewInfoType, BaseEditPainter painter, bool designTimeVisible, Image image) :
                base(name, editorType, repositoryType, viewInfoType, painter, designTimeVisible, image)
            {
            }

            public override RepositoryItem CreateRepositoryItem()
            {
                return new EnhancedSearchLookUpEditRepositoryItem();
            }

            public override BaseEdit CreateEditor()
            {
                var editor = new EnhancedSearchLookUpEdit();
                return editor;
            }
        }
    }
    #endregion DevExpress designer part
}
