﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Skins;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Tools;
using TechnicalTools.UI.DX.Helpers;


namespace TechnicalTools.UI.DX.Controls
{
    [ToolboxItem(true)]
    public partial class TreeViewBound : TreeList
    {
        public void ItemsAvailablesSet<T>(IEnumerable<T> datasource)
            where T : ITreeNodeWithParentReadable
        {
            _ItemsAvailables = new FixedBindingList<ITreeNodeWithParentReadable>((IEnumerable<ITreeNodeWithParentReadable>)datasource);
            OnRebuildItem();
        }

        // Fonctionalité pour accepter aussi les type simple. Mais c'est pas aussi simple a faire car il faut faire en sorte
        // que les propriete de type enuemrable renvoie les bon types (ie si l'utilisateur donne des string il s'attend a
        // avoir une liste de string dans ItemSelected, et pas une liste de wrapper.
        //
        //public void ItemsAvailablesSet<T>(IEnumerable<T> datasource)
        //    where T : struct
        //{
        //    _ItemsAvailables = new FixedBindingList<ITreeNodeWithParentReadable>(datasource.Select(value => new ValueWrapper<T>() { Value = value }));
        //    OnRebuildItem();
        //}
        //interface IValueWrapper : INamedObject, ITreeNodeWithParentReadable
        //{
        //    object Value { get; }
        //}
        //class ValueWrapper<T> : IValueWrapper
        //{
        //    public T Value { get; set; }

        //    object IValueWrapper.Value { get { return Value; } }
        //    public string Name { get { return Value.ToString(); } }
        //    public ITreeNodeWithParentReadable ParentNode { get { return null; } }
        //}

        //public class Calendar : ITreeNodeWithParentReadable, INamedObject
        //{
        //    public int    Id { get; set; }
        //    public string Name { get; set; }
        //
        //    public ITreeNodeWithParentReadable ParentNode { get { return null; } }
        //}

        // Encapsule la datasource afin de remplir les contraintes de DevExpress :
        // la datasource ne doit pas implementer IList ou IVirtualTreeListData interface (contrat DevExpress)
        // dans le cas contraitre les events "VirtualTree*" ne seraient pas utilisés
        class DataSourceWrapper
        {
            public IEnumerable<ITreeNodeWithParentReadable> DataSource;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Category("Data")]
        //[AttributeProvider(typeof(IListSource))]
        [DefaultValue(null)]
        public new object DataSource
        {
            get { return base.DataSource is DataSourceWrapper ? (base.DataSource as DataSourceWrapper).DataSource : base.DataSource; }
            private set { base.DataSource = value; }
        }

        /// <summary>
        /// Liste de ISimpleTreeNodeReadOnly (ou IBindingTreeNode)
        /// Utilisez DatasourceSet qui est fortement typé et qui filtre les interfaces gérées
        /// </summary>
        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IEnumerable<ITreeNodeWithParentReadable> ItemsAvailables
        {
            get { return /*_enumerableConverter(*/_ItemsAvailables /*)*/; }
        }

        IFixedBindingList<ITreeNodeWithParentReadable> _ItemsAvailables = new FixedBindingList<ITreeNodeWithParentReadable>(); // Les objets peuvent aussi implémenter ITreeNodeWithChildren

        //Func<IEnumerable<ITreeNodeWithParentReadable>, IEnumerable<object>> _enumerableConverter = DefaultEnumerableConverter; // Converter to transform data before returning it to user
        //static readonly Func<IEnumerable<ITreeNodeWithParentReadable>, IEnumerable<object>> DefaultEnumerableConverter = lst => lst;

        /// <summary>
        /// List of items that are not un/selectable by user (but they can be selected by force by developper)
        /// </summary>
        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IEnumerable<ITreeNodeWithParentReadable> ItemsDisabled
        {
            get { return _ItemsDisabled; }
            set
            {
                _ItemsDisabled.Clear();
                if (value != null)
                    foreach (ITreeNodeWithParentReadable obj in value)
                        _ItemsDisabled.Add(obj);
                OnRebuildItem();
            }
        }
        readonly HashSet<ITreeNodeWithParentReadable> _ItemsDisabled = new HashSet<ITreeNodeWithParentReadable>();

        /// <summary>
        /// List of items that are completely disabeld (they can't be selected by force by developper)
        /// Used for saying that some values exist but have no meaning in the current case of use.
        /// </summary>
        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IEnumerable<ITreeNodeWithParentReadable> ItemsUnselectable
        {
            get { return _ItemsUnselectable; }
            set
            {
                _ItemsUnselectable.Clear();
                if (value != null)
                    foreach (ITreeNodeWithParentReadable obj in value)
                        _ItemsUnselectable.Add(obj);
                OnRebuildItem();
            }
        }
        readonly HashSet<ITreeNodeWithParentReadable> _ItemsUnselectable = new HashSet<ITreeNodeWithParentReadable>();



        public TreeViewBound()
        {
            InitializeComponent();

            ItemsSelected = new FixedBindingList<ITreeNodeWithParentReadable>();

            VirtualTreeGetChildNodes += OnVirtualTreeGetChildNodes;
            VirtualTreeGetCellValue += OnVirtualTreeGetCellValue;
            VirtualTreeSetCellValue += OnVirtualTreeSetCellValue;

            NodeCellStyle += OnNodeCellStyle;
            BeforeCheckNode += OnBeforeCheckNode;
            CustomDrawNodeCheckBox += OnCustomDrawNodeCheckBox;

            ParentFieldName = GetMemberName.For<ITreeNodeWithParentReadable>(n => n.ParentNode);
            RootValue = null;
            DataSource = new DataSourceWrapper() { DataSource = _ItemsAvailables };

            InitSelectionManagement();

            OnRebuildItem();
        }

        void OnRebuildItem()
        {
            var node_with_children = _ItemsAvailables.OfType<ITreeNodeWithChildrenReadable>().ToList();
            if (node_with_children.Count == _ItemsAvailables.Count)
            {
                // on récupère les noeuds qui ne sont enfants de personne (ie : "tous les noeuds - tous les enfants de tous les noeuds")
                var roots = node_with_children.Except(node_with_children.SelectMany(node => node.Children));
                _roots.Clear();
                // ReSharper disable SuspiciousTypeConversion.Global
                _roots.AddRange(roots.Cast<ITreeNodeWithParentReadable>());
                // ReSharper restore SuspiciousTypeConversion.Global
                _children.Clear();
            }
            else
            {
                var grps = ItemsAvailables.GroupBy(node => node.ParentNode).ToList();
                if (grps.Count != 0)
                {
                    var root_grps = grps.FirstOrDefault(grp => grp.Key == null);
                    // Ce test n'est pas exhaustif! En ajoutant un noeud, seul isole, l'assert fonctionne mais poutant on ne verrai que le noeud isolé
                    Debug.Assert(root_grps != null, "items are not a tree, but a graph ! There is a cycle in ParentNode property");
                    _roots.Clear();
                    _roots.AddRange(root_grps.ToList());
                    _children.Clear();
                    foreach (var grp in grps.Where(grp => grp != root_grps))
                        _children.Add(grp.Key, grp.ToFixedBindingList());
                }
            }

            ExpandAll(); // Force all nodes to be retrieved using OnVirtualTreeGetChildNodes

            RebuildItem?.Invoke(this, EventArgs.Empty);
        }
        event EventHandler RebuildItem;

        readonly FixedBindingList<ITreeNodeWithParentReadable> _roots = new FixedBindingList<ITreeNodeWithParentReadable>();
        readonly Dictionary<ITreeNodeWithParentReadable, IFixedBindingList<ITreeNodeWithParentReadable>> _children = new Dictionary<ITreeNodeWithParentReadable, IFixedBindingList<ITreeNodeWithParentReadable>>();


        public TreeListColumn AddColumn<TItem, TFieldType>(string colName, Func<TItem, TFieldType> getValue)
          where TItem : ITreeNodeWithParentReadable, IIsNamed
        {
            if (colName.Contains("*"))
                throw new ArgumentException($"Argument {nameof(colName)} cannot contains '*'!");
            OptionsView.ShowColumns = true;

            _getValues.Add("*" + colName, obj => getValue((TItem)obj));
            var col = Columns.AddVisible("*" + colName); // raise OnVirtualTreeGetChildNodes so _getValues needs to be set
            col.Caption = colName;
            col.UnboundType = typeof(TFieldType).ToDevExpressTreeListUnboundType();
            col.OptionsColumn.ReadOnly = true;
            // Col must no be unbound when we use VirtualTree
            return col;
        }
        readonly Dictionary<string, Func<object, object>> _getValues = new Dictionary<string, Func<object, object>>();


        void OnVirtualTreeGetChildNodes(object sender, VirtualTreeGetChildNodesInfo e)
        {
            e.Children = e.Node == base.DataSource
                             ? _roots
                             : _children.ContainsKey((ITreeNodeWithParentReadable)e.Node)
                                   ? _children[(ITreeNodeWithParentReadable)e.Node]
                                   : null;
        }

        void OnVirtualTreeGetCellValue(object sender, VirtualTreeGetCellValueInfo e)
        {
            if (e.Column == _hackishColForMapping) // cas de hack particulier à ignorer
                return;

            if (e.Node == null || string.IsNullOrWhiteSpace(e.Column.FieldName))
                return;

            if (_getValues.TryGetValue(e.Column.FieldName, out Func<object, object>  func))
            {
                e.CellData = func(e.Node);
                return;
            }
            // TODO : A optimiser (dans un dictionary ?)
            var objType = e.Node.GetType();
            var propInfo = objType.GetProperty(e.Column.FieldName);
            if (propInfo == null)
                foreach (var itype in objType.GetInterfaces())
                {
                    propInfo = itype.GetProperty(e.Column.FieldName);
                    if (propInfo != null)
                        break;
                }
            if (propInfo == null)
                throw new TechnicalException(string.Format("Pas de propriete \"{0}\" sur les objets de type \"{1}\" ainsi que sur leur interface", e.Column.FieldName, objType.Name), null);
            e.CellData = propInfo.GetValue(e.Node, null);
        }

        void OnVirtualTreeSetCellValue(object sender, VirtualTreeSetCellValueInfo e)
        {
            e.Cancel = true;
            //Diagnostics.DebugTools.Break();
        }

        void OnBeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            e.CanCheck = !_ItemsDisabled.Contains((ITreeNodeWithParentReadable)GetDataRecordByNode(e.Node));
        }

        void OnNodeCellStyle(object sender, GetCustomNodeCellStyleEventArgs e)
        {
            if (!_ItemsDisabled.Contains((ITreeNodeWithParentReadable)GetDataRecordByNode(e.Node)))
                return;
            var frm = FindForm() as DevExpress.XtraEditors.XtraForm;
            Debug.Assert(frm != null, "Works only when parent is DevExpress' XtraForm!");
            Skin currentSkin = CommonSkins.GetSkin(frm.LookAndFeel);
            e.Appearance.ForeColor = currentSkin.Colors[CommonColors.DisabledText];
        }

        void OnCustomDrawNodeCheckBox(object sender, CustomDrawNodeCheckBoxEventArgs e)
        {
            if (_ItemsUnselectable.Contains((ITreeNodeWithParentReadable)GetDataRecordByNode(e.Node)))
            {
                e.Handled = true;
                return;
            }
            if (_ItemsDisabled.Contains((ITreeNodeWithParentReadable)GetDataRecordByNode(e.Node)))
            {
                var frm = FindForm() as DevExpress.XtraEditors.XtraForm;
                Debug.Assert(frm != null, "Works only when parent is DevExpress' XtraForm!");

                e.DefaultDraw();
                e.Handled = true;
                Skin currentSkin = CommonSkins.GetSkin(frm.LookAndFeel);
                Color skinBackColor = currentSkin.Colors[CommonColors.DisabledControl];
                using (var brush = new SolidBrush(Color.FromArgb(100, skinBackColor.R, skinBackColor.G, skinBackColor.B)))
                {
                    e.Graphics.FillRectangle(brush, e.Bounds);
                }
            }
        }

        #region Gestion de la selection

        void InitSelectionManagement()
        {
            AfterCheckNode += OnAfterCheckNode;
            RebuildItem += (_, __) => RefreshVisualCheckStatesOfItems();

            // Hack pour faire en sorte que RefreshVisualCheckStatesOfItems fonctionne (raison : https://www.devexpress.com/Support/Center/Question/Details/T159091)
            if (!DesignTimeHelper.IsInDesignMode) // permet de ne pas voir la colonne dans le designer
            {
                KeyFieldName = "_"; // this field has an arbitrary name that should not be used as a proprerty by Business Object. It will never change
                _hackishColForMapping = Columns.AddField(KeyFieldName);

                VirtualTreeGetCellValue += (_, e) =>
                {
                    if (e.Column == _hackishColForMapping) // nécessaire au hack expliqué dans RebuildItems
                        e.CellData = e.Node;
                };
            }
        }

        TreeListColumn _hackishColForMapping; // readonly

        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IFixedBindingList<ITreeNodeWithParentReadable> ItemsSelected
        {
            get { return _ItemsSelected; }
            set
            {
                if (_ItemsSelected != null)
                    _ItemsSelected.ListChanged -= OnSelectionListChanged;
                _ItemsSelected = value ?? new FixedBindingList<ITreeNodeWithParentReadable>(); // Ainsi _ItemsSelected n'est jamais null
                _ItemsSelected.ListChanged += OnSelectionListChanged;
                SetSelectedItems(_ItemsSelected.ToList(), true);
            }
        }
        IFixedBindingList<ITreeNodeWithParentReadable> _ItemsSelected;
        public void SetSelectedItems(IReadOnlyCollection<ITreeNodeWithParentReadable> newSelectedItemsSet)
        {
            SetSelectedItems(newSelectedItemsSet, false);
        }
        void SetSelectedItems(IReadOnlyCollection<ITreeNodeWithParentReadable> newSelectedItemsSet, bool forceRefresh)
        {
            if (MaxSelectedItems != 0 && newSelectedItemsSet.Count > MaxSelectedItems)
                if (SafeMode)
                    newSelectedItemsSet = newSelectedItemsSet.Skip(newSelectedItemsSet.Count - (int)MaxSelectedItems).ToList();
                else
                    throw new Exception($"Cannot select {newSelectedItemsSet.Count} items because {nameof(MaxSelectedItems)} is set to {MaxSelectedItems}! You can fix this or set {nameof(SafeMode)} = true");
            if (ItemsSelected.Count != newSelectedItemsSet.Count ||
                newSelectedItemsSet.Except(ItemsSelected).Any())
                _ItemsSelected.WithListChangedEventsDisabled(() =>
                {
                    _ItemsSelected.Clear();
                    foreach (var item in newSelectedItemsSet)
                        _ItemsSelected.Add(item);
                }, true); // true will callback RefreshVisualCheckStatesOfItems();
            else if (forceRefresh)
                RefreshVisualCheckStatesOfItems();
        }

        [DefaultValue(typeof(uint), "0")]
        public uint MaxSelectedItems
        {
            get { return _MaxSelectedItems; }
            set
            {
                _MaxSelectedItems = value;
                if (_MaxSelectedItems != 0 && _ItemsSelected.Count > _MaxSelectedItems)
                    SetSelectedItems(_ItemsSelected.ToList());
            }
        }
        uint _MaxSelectedItems; // 0 = infinite

        [DefaultValue( false)]
        public bool SafeMode { get; set; }

        void OnSelectionListChanged(object sender, ListChangedEventArgs e)
        {
            //if (e.ListChangedType == ListChangedType.Reset) // considered always true by now.
                RefreshVisualCheckStatesOfItems();
        }

        void RefreshVisualCheckStatesOfItems()
        {
            Debug.Assert(!_ItemsSelected.Except(_ItemsAvailables).Any(), "Some selected items are not in available items");
            foreach (TreeListNode tln in Nodes) // sur les noeuds racines
                DoRecursivelyOnTreeListNode(tln, n => n.Checked = false);

            var treeNodesToCheck = _ItemsSelected.Select(FindNodeByKeyID).ToList();
            Debug.Assert(treeNodesToCheck.All(node => node != null), "Technical assert about DevExpress (see Hack in InitSelectionManagement)");
            foreach (TreeListNode tln in treeNodesToCheck)
            {
                // Comme l'assert peut foirer (merci devexpress)
                // Lorsque la treeviewbound est dans un onglet caché, et qu'un control dans un onglet visible modifie la list sur laquel est bindé la treeviewbound,
                // la methode FindNodeByKeyID ne fonctionne plus et renvoie null
                // Cependant si on retourne sur l'onglet caché, la liste est bien à jour par rapport au model
                // Il semble que RefreshVisualCheckStatesOfItems soit appelé plusieurs fois sans recocher le noeud. mais est appelé une derniere fois en cochant le noeud cette fois ci
                // (pour quoi la derniere fois ?)
                if (tln != null)
                    tln.Checked = true;
            }
            RaiseRefreshVisualInformations();
        }
        public event EventHandler RefreshVisualInformations;
        public void RaiseRefreshVisualInformations()
        {
            RefreshVisualInformations?.Invoke(this, EventArgs.Empty);
        }

        public void DoRecursivelyOnNodes<TUserDataNode>(Action<TUserDataNode> action)
        {
            foreach (TreeListNode tln in Nodes) // sur les noeuds racines
                DoRecursivelyOnTreeListNode(tln, n => action((TUserDataNode)GetDataRecordByNode(n)));
        }
        void DoRecursivelyOnTreeListNode(TreeListNode tln, Action<TreeListNode> action)
        {
            action(tln);
            foreach (TreeListNode sub_tln in tln.Nodes)
                DoRecursivelyOnTreeListNode(sub_tln, action);
        }


        public Action BeforeChange;
        public Action<Exception> OnError;

        void OnAfterCheckNode(object sender, NodeEventArgs e)
        {
            try
            {
                Debug.Assert(e.Node.CheckState == CheckState.Checked ||
                             e.Node.CheckState == CheckState.Unchecked, "Other states are not handled yet");

                var item = (ITreeNodeWithParentReadable)GetDataRecordByNode(e.Node);

                if (_ItemsUnselectable.Contains(item))
                {
                    e.Node.Checked = !e.Node.Checked; // Annule le changement d'état en remettant ce qu'il y avait avant
                    return;
                }

                BeforeChange?.Invoke();

                // TODO : Encapsuler le Add / Remove et suivre la réponse ici https://www.devexpress.com/Support/Center/Question/Details/T174774
                if (e.Node.Checked)
                {
                    Debug.Assert(!_ItemsSelected.Contains(item), "Treelist and bindinglist should be always synchronized");
                    if (MaxSelectedItems != 0 && _ItemsSelected.Count >= MaxSelectedItems)
                    {
                        // Supprime l'item de la sélection qui est le plus ancien et qui est aussi deselectionable.
                        var item_to_remove = _ItemsSelected.FirstOrDefault(item2 => !_ItemsDisabled.Contains(item2));
                        if (item_to_remove == null)
                            return; // Trop d'item sélectionné mais aucun n'est déselectionable !
                        _ItemsSelected.Remove(item_to_remove);
                    }
                    _ItemsSelected.Add(item);
                }
                else
                {
                    Debug.Assert(_ItemsSelected.Contains(item), "Treelist and bindinglist should be always synchronized");
                    _ItemsSelected.Remove(item);
                }
            }
            catch (BaseException ex) // technical or functional
            {
                OnError?.Invoke(ex);
            }
        }

        #endregion

    }
}
