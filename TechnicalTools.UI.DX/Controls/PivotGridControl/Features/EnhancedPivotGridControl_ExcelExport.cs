﻿using System;

using DevExpress.Utils.Menu;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraPrinting;

using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Tools;


namespace TechnicalTools.UI.DX
{
    public class EnhancedPivotGridControl_ExcelExport : IPivotGridControlEnhancement_Feature
    {
        public EnhancedPivotGridControl_ExcelExport(PivotGridControlEnhancement pgce)
        {
            Install(pgce);
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(EnhancedPivotGridControl_ExcelExport));
        PivotGridControlEnhancement _pgce;


        public void Install(PivotGridControlEnhancement pgce)
        {
            if (pgce == _pgce)
                return;
            Uninstall();
            _pgce = pgce;
            _pgce.PivotGrid.PopupMenuShowing += PivotGrid_PopupMenuShowing;
        }

        public void Uninstall()
        {
            if (_pgce == null)
                return;
            _pgce.PivotGrid.PopupMenuShowing -= PivotGrid_PopupMenuShowing;
            _pgce = null;
        }

        void PivotGrid_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if (e.MenuType == PivotGridMenuType.HeaderSummaries)
                return;
            var pgc = sender as PivotGridControl;
            var mnuExport = new DXSubMenuItem(ExportMenuCaption);

            Action toExcelFile = () => DefaultExportToExcel(_pgce, pgc, tryOpenExportFile: true);
            Action toCSVFile = () => DefaultExportToCSV(_pgce, pgc, tryOpenExportFile: true);
            Action toCSVClipBoard = () => EnhancedGridView_ExportExcel.DefaultExportToCSVClipBoard(tempFile => DefaultExportToCSV(_pgce, pgc, targetFilename: tempFile, tryOpenExportFile: false));

            mnuExport.Items.Add(new DXMenuItem("Export data to Excel file (*.xlsx)", (_, __) => toExcelFile()));
            mnuExport.Items.Add(new DXMenuItem("Export data to CSV file", (_, __) => toCSVFile()));
            mnuExport.Items.Add(new DXMenuItem("Copy data to clipboard (CSV format)", (_, __) => toCSVClipBoard()));

            e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuExport);
        }
        public static string ExportMenuCaption { get; } = "Export...";

        public static string DefaultExportToExcel(PivotGridControlEnhancement pgce, PivotGridControl pgc, bool tryOpenExportFile = false, string targetFilename = null)
        {
            try
            {
                string filename = targetFilename ?? EnhancedGridView_ExportExcel.ChooseFileDestination();
                if (filename == null)
                    return null;

                var options = new XlsxExportOptionsEx(TextExportMode.Value)
                {
                    ExportType = DevExpress.Export.ExportType.DataAware
                };
                var isExporting = pgce.IsExporting;
                try
                {
                    pgce.IsExporting = true;

                    pgc.ExportToXlsx(filename, options);
                }
                finally
                {
                    pgce.IsExporting = isExporting;
                }
                if (tryOpenExportFile)
                    EnhancedGridView_ExportExcel.TryOpenFile(filename);
                return filename;
            }
            catch (Exception ex)
            {
                BusEvents.Instance.RaiseUserUnderstandableMessage("Error while exporting to Excel!" + Environment.NewLine +
                                                                  "Because" + ExceptionManager.Instance.Format(ex, true), _log);
                return null;
            }
        }

        public static string DefaultExportToCSV(PivotGridControlEnhancement pgce, PivotGridControl pgc, bool withCurrentFilter = true, bool tryOpenExportFile = false, string targetFilename = null)
        {
            try
            {
                string filename = targetFilename ?? EnhancedGridView_ExportExcel.ChooseFileDestination("csv", "CSV File");
                if (filename == null)
                    return null;

                var options = new CsvExportOptionsEx()
                {
                    ExportType = DevExpress.Export.ExportType.WYSIWYG
                };
                var isExporting = pgce.IsExporting;

                try
                {
                    pgce.IsExporting = true;
                    pgc.ExportToCsv(filename, options);
                }
                finally
                {
                    pgce.IsExporting = isExporting;
                }
                if (tryOpenExportFile)
                    EnhancedGridView_ExportExcel.TryOpenFile(filename);
                return filename;
            }
            catch (Exception ex)
            {
                BusEvents.Instance.RaiseUserUnderstandableMessage("Error while exporting to Excel!" + Environment.NewLine +
                                                                  "Because" + ExceptionManager.Instance.Format(ex, true), _log);
                return null;
            }
        }

    }
}
