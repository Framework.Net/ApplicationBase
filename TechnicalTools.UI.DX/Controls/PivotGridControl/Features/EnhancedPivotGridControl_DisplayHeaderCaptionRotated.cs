﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using DevExpress.Utils.Menu;
using DevExpress.XtraPivotGrid;


namespace TechnicalTools.UI.DX
{
    // Inspired from https://supportcenter.devexpress.com/ticket/details/T870775/pivot-grid-how-to-rotate-headers-on-the-angle
    public class EnhancedPivotGridControl_DisplayHeaderCaptionRotated : IPivotGridControlEnhancement_Feature
    {
        public EnhancedPivotGridControl_DisplayHeaderCaptionRotated(PivotGridControlEnhancement pgce)
        {
            Install(pgce);
        }
        PivotGridControlEnhancement _pgce;


        public void Install(PivotGridControlEnhancement pgce)
        {
            if (pgce == _pgce)
                return;
            Uninstall();
            _pgce = pgce;
            _pgce.PivotGrid.CustomDrawFieldValue += PivotGrid_CustomDrawFieldValue;
            _pgce.PivotGrid.PopupMenuShowing += PivotGrid_PopupMenuShowing;
        }

        public void Uninstall()
        {
            if (_pgce == null)
                return;
            _pgce.PivotGrid.CustomDrawFieldValue += PivotGrid_CustomDrawFieldValue;
            _pgce.PivotGrid.PopupMenuShowing -= PivotGrid_PopupMenuShowing;
            _pgce = null;
        }

        void PivotGrid_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if (e.MenuType == PivotGridMenuType.HeaderSummaries)
                return;
            if (e.Field.Area != PivotArea.ColumnArea)
                return;
            if (!(e.Field is EnhancedPivotGridField epgf))
                return;

            var pgc = sender as PivotGridControl;
            var mnuRotation = new DXSubMenuItem("Display rotated...");
            foreach (var angle in new int [] { 0, 45, 90, }.Concat(Enumerable.Range(0, 10).Select(i => i * 10).Distinct()))
            {
                var mnu = new DXMenuItem("with an angle of " + angle.ToStringInvariant() + "°");
                mnu.Click += (_, __) => { epgf.DisplayRotationAngle = angle; pgc.LayoutChanged(); };
                mnu.Enabled = epgf.DisplayRotationAngle != angle;
                mnuRotation.Items.Add(mnu);
            }
            e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuRotation);
        }

        void PivotGrid_CustomDrawFieldValue(object sender, PivotCustomDrawFieldValueEventArgs e)
        {
            if (e.Area != PivotArea.ColumnArea)
                return;
            if (!(e.Field is EnhancedPivotGridField epgf))
                return;
            if (epgf.DisplayRotationAngle == 0)
                return;
            string caption = e.Info.Caption;
            e.Info.Caption = string.Empty;
            e.Painter.DrawObject(e.Info);
            //Rectangle rec = new Rectangle(e.Bounds.X, e.Bounds.Y + 10, e.Bounds.Width, e.Bounds.Height - 10)
            //e.GraphicsCache.DrawVString(caption, e.Appearance.GetFont(), e.Appearance.GetForeBrush(e.GraphicsCache), rec, e.Appearance.GetStringFormat(), -epgf.DisplayRotationAngle)
            DrawString(e, caption, -epgf.DisplayRotationAngle);
            e.Handled = true;
        }

        static void DrawString(PivotCustomDrawFieldValueEventArgs e, string text, int angle)
        {
            Font font = e.Appearance.GetFont();
            Brush brush = e.Appearance.GetForeBrush(e.GraphicsCache);
            Point transformPoint = new Point(e.Bounds.Left, e.Bounds.Bottom);
            SizeF size = e.GraphicsCache.UseDirectXPaint ? e.GraphicsCache.CalcTextSize(text, font) : e.GraphicsCache.Graphics.MeasureString(text, font);
            int textHeight = Convert.ToInt32(size.Height);
            Point drawStringPoint = new Point(10 + textHeight / 2, -textHeight / 2);
            if (e.GraphicsCache.UseDirectXPaint)
            {
                e.GraphicsCache.Paint.TranslateTransform(e.GraphicsCache.Graphics, transformPoint.X, transformPoint.Y);
                e.GraphicsCache.Paint.RotateTransform(e.GraphicsCache.Graphics, angle);
                e.GraphicsCache.DrawString(text, font, brush, drawStringPoint);
                e.GraphicsCache.Paint.ResetTransform(e.GraphicsCache.Graphics);
            }
            else
            {
                e.GraphicsCache.Graphics.TranslateTransform(transformPoint.X, transformPoint.Y);
                e.GraphicsCache.Graphics.RotateTransform(angle);
                e.GraphicsCache.Graphics.DrawString(text, font, brush, drawStringPoint);
                e.GraphicsCache.Graphics.ResetTransform();
            }
        }
    }

    public partial class EnhancedPivotGridField
    {
        internal int DisplayRotationAngle { get; set; }
    }
}
