﻿using System;
using System.Drawing;
using System.Windows.Forms;

using DevExpress.XtraPivotGrid;


namespace TechnicalTools.UI.DX
{
    // Inspired from https://supportcenter.devexpress.com/ticket/details/t591335/reorder-dataarea-field-at-pivotgrid-by-drag-and-drop
    public class EnhancedPivotGridControl_DataColumnDragging : IPivotGridControlEnhancement_Feature
    {
        PivotGridControl TargetPivot { get { return _pgce.PivotGrid; } }

        public EnhancedPivotGridControl_DataColumnDragging(PivotGridControlEnhancement pgce)
        {
            Install(pgce);
        }
        PivotGridControlEnhancement _pgce;

        public void Install(PivotGridControlEnhancement pgce)
        {
            if (pgce == _pgce)
                return;
            Uninstall();
            _pgce = pgce;
            TargetPivot.MouseMove += pgc_MouseMove;
            TargetPivot.MouseUp += pgc_MouseUp;
            TargetPivot.FieldAreaChanged += pgc_FieldAreaChanged;
            TargetPivot.CustomDrawFieldValue += pgc_CustomDrawFieldValue;
        }

        public void Uninstall()
        {
            if (_pgce == null)
                return;

            TargetPivot.MouseMove -= pgc_MouseMove;
            TargetPivot.MouseUp -= pgc_MouseUp;
            TargetPivot.FieldAreaChanged -= pgc_FieldAreaChanged;
            TargetPivot.CustomDrawFieldValue -= pgc_CustomDrawFieldValue;
            _pgce = null;
        }

        void pgc_CustomDrawFieldValue(object sender, PivotCustomDrawFieldValueEventArgs e)
        {
            if (_onDrag)
            {
                ////if (e.ValueType != PivotGridValueType.Value) return
                if (e.Field == _dragInfo.ValueInfo.Field && e.Bounds.Contains(_dragInfo.HitPoint))
                {
                    e.Painter.DrawObject(e.Info);
                    e.Handled = true;
                    Rectangle rec = e.Info.Bounds;
                    rec.Width--;
                    e.Graphics.DrawRectangle(Pens.Blue, rec);
                    return;
                }
                if (_targetInfo != null && e.Field == _targetInfo.ValueInfo.Field && e.Bounds.Contains(_targetInfo.HitPoint))
                {
                    e.Painter.DrawObject(e.Info);
                    e.Handled = true;
                    Rectangle rec = e.Info.Bounds;
                    rec.Width--;
                    e.Graphics.DrawRectangle(Pens.Green, rec);
                    return;
                }
            }
        }

        void pgc_FieldAreaChanged(object sender, PivotFieldEventArgs e)
        {
            //if ((e.Field.Area == PivotArea.ColumnArea && AllowDragColumn) || (e.Field.Area == PivotArea.RowArea && AllowDragRow))
            //    InitSingleFieldOrder(e.Field)
        }

        void pgc_MouseMove(object sender, MouseEventArgs e)
        {
            if (_onDrag)
                TargetPivot.FindForm().Cursor = GetDragEffect(e.Location) == DragDropEffects.None
                                              ? Cursors.No
                                              : Cursors.Hand;
            else if (e.Button == MouseButtons.Left)
            {
                PivotGridHitInfo hi = TargetPivot.CalcHitInfo(e.Location);
                if (hi.HitTest == PivotGridHitTest.Value &&
                    hi.ValueInfo.Field != null &&
                    hi.ValueInfo.Field.Area == PivotArea.DataArea)
                {
                    _dragInfo = hi;
                    _onDrag = true;
                    TargetPivot.FindForm().Cursor = Cursors.Hand;
                }
            }
            TargetPivot.Invalidate();
        }
        bool _onDrag;
        PivotGridHitInfo _dragInfo;
        PivotGridHitInfo _targetInfo;

        void pgc_MouseUp(object sender, MouseEventArgs e)
        {
            if (!_onDrag)
                return;
            if (GetDragEffect(e.Location) == DragDropEffects.Move)
            {
                PivotGridHitInfo hi = TargetPivot.CalcHitInfo(e.Location);
                _dragInfo.ValueInfo.Field.AreaIndex = hi.ValueInfo.Field.AreaIndex;
            }
            _onDrag = false;
            TargetPivot.FindForm().Cursor = Cursors.Default;
        }

        private DragDropEffects GetDragEffect(Point point)
        {
            PivotGridHitInfo hi = TargetPivot.CalcHitInfo(point);
            PivotArea dragArea = _dragInfo.ValueInfo.Field.Area;
            if (hi.HitTest != PivotGridHitTest.Value ||
                hi.ValueInfo.Field == null ||
                hi.ValueInfo.Field.Area != dragArea ||
                _dragInfo.ValueInfo.Field == hi.ValueInfo.Field)
            {
                _targetInfo = null;
                return DragDropEffects.None;
            }
            else
            {
                _targetInfo = hi;
                return DragDropEffects.Move;
            }
        }
    }
}
