﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;

using DevExpress.XtraPivotGrid;

using TechnicalTools.Extensions.Data;


namespace TechnicalTools.UI.DX
{
    #region Mixin part

    public static class PivotGridControl_Extensions
    {
        public static PivotGridControlEnhancement GetEnhancement(this PivotGridControl pgc)
        {
            return PivotGridControlEnhancement.GetFor(pgc);
        }
    }

    public partial class PivotGridControlEnhancement
    {
        public static PivotGridControlEnhancement InstallOn(PivotGridControl pgc)
        {
            var pgce = GetFor(pgc);
            if (pgce == null)
            {
                pgce = CreatePivotGridControlEnhancement(pgc);
                _pgces.Add(pgc, pgce);
            }
            return pgce;
        }
        public static PivotGridControlEnhancement GetFor(PivotGridControl pgc)
        {
            if (_pgces.TryGetValue(pgc, out PivotGridControlEnhancement pgce))
                return pgce;
            return null;
        }
        static readonly ConditionalWeakTable<PivotGridControl, PivotGridControlEnhancement> _pgces = new ConditionalWeakTable<PivotGridControl, PivotGridControlEnhancement>();

        public static Func<PivotGridControl, PivotGridControlEnhancement> CreatePivotGridControlEnhancement = DefaultCreatePivotGridControlEnhancement;
        public static PivotGridControlEnhancement DefaultCreatePivotGridControlEnhancement(PivotGridControl pgc)
        {
            return new PivotGridControlEnhancement(pgc);
        }
    }

    #endregion Mixin part

    // Important to read for layout :/ https://supportcenter.devexpress.com/ticket/details/T757175/pivot-grid-cannot-save-restore-the-field-filter-if-the-field-is-bound-to-a-column-of-a
    public partial class PivotGridControlEnhancement
    {
        public bool IsExporting { get; internal set; }

        protected PivotGridControlEnhancement(PivotGridControl pgc)
        {
            PivotGrid = pgc;

            // to read one day https://supportcenter.devexpress.com/ticket/details/T820882/hide-the-grand-total-header-in-the-row-area-in-pivotgridcontrol
            // to read one day ? https://supportcenter.devexpress.com/ticket/details/t705174/cannot-remove-pivotgrid-column-grand-totals

            FieldListFormBehavior = new EnhancedPivotGridControl_FieldListFormBehavior(this);
            CustomizeGroupInterval = new EnhancedPivotGridControl_CustomizeGroupInterval(this);
            ChangeSummaryType = new EnhancedPivotGridControl_ChangeSummaryType(this);
            ShowChart = new EnhancedPivotGridControl_ShowDynamicChart(this);
            ExcelExport = new EnhancedPivotGridControl_ExcelExport(this);
            DataColumnDragging = new EnhancedPivotGridControl_DataColumnDragging(this);
            // Not fully functional yet
            //DisplayHeaderCaptionRotated = new EnhancedPivotGridControl_DisplayHeaderCaptionRotated(this);
        }
        public PivotGridControl PivotGrid { get; }

        public EnhancedPivotGridControl_FieldListFormBehavior FieldListFormBehavior { get; }
        public EnhancedPivotGridControl_CustomizeGroupInterval CustomizeGroupInterval { get; }
        public EnhancedPivotGridControl_ChangeSummaryType ChangeSummaryType { get; }
        public EnhancedPivotGridControl_ShowDynamicChart ShowChart { get; }
        public EnhancedPivotGridControl_ExcelExport ExcelExport { get; }
        public EnhancedPivotGridControl_DataColumnDragging DataColumnDragging { get; }
        EnhancedPivotGridControl_DisplayHeaderCaptionRotated DisplayHeaderCaptionRotated { get; }



        public void PopulateFields<T>(PivotArea inThisArea = PivotArea.FilterArea)
        {
            PopulateFields(typeof(T), inThisArea);
        }
        public void PopulateFields(Type t, PivotArea inThisArea = PivotArea.FilterArea)
        {
            PivotGrid.Fields.Clear();
            var fields = new List<EnhancedPivotGridField>();
            foreach (var p in t.GetProperties())
            {
                if (p.CanRead &&
                    !(p.GetCustomAttribute<IsTechnicalPropertyAttribute>()?.HideFromUser ?? false) &&
                    (p.GetCustomAttribute<BrowsableAttribute>()?.Browsable ?? true))
                {
                    var field = new EnhancedPivotGridField(p)
                    {
                        AreaIndex = 0,
                        Caption = p.Name.Uncamelify(),
                        FieldName = p.Name,
                        AllowedAreas = PivotGridAllowedAreas.All,
                    };
                    //if (inThisArea != null)
                    //    field.Area = inThisArea.Value;
                    //else
                    //    field.Visible = false;
                    fields.Add(field);
                }
            }
            PivotGrid.Fields.AddRange(fields.ToArray());
            foreach (var field in fields)
            {
                SetFormat(field, field.BoundProperty.PropertyType);
                if (field.BoundProperty.PropertyType.RemoveNullability().IsNumericType())
                    CustomizeGroupInterval.SetLogInterval(field, eCustomGroupInterval.Log10);
            }
        }


        void SetFormat(PivotGridField field, Type type)
        {
            var t = type.RemoveNullability();
            field.GroupInterval = PivotGroupInterval.Custom;
            // Note:
            // Cell format is used for column and row area
            // ValueFormat is used for data area
            if (t.IsIntegerType())
            {
                field.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                field.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                field.CellFormat.FormatString = "#,##0";
                field.ValueFormat.FormatString = "#,##0";
            }
            else if (t.IsNumericType())
            {
                field.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                field.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                field.CellFormat.FormatString = "#,##0.00######";
                field.ValueFormat.FormatString = "#,##0.00######";
            }
            else if (t == typeof(DateTime))
            {
                field.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                field.GroupInterval = PivotGroupInterval.DateMonth;
            }
            else if (t == typeof(TimeSpan))
            {
                field.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                field.GroupInterval = PivotGroupInterval.DateHour;
            }
        }
    }

    public partial class EnhancedPivotGridField : PivotGridField
    {
        public PropertyInfo BoundProperty { get; }

        public EnhancedPivotGridField(PropertyInfo pi)
        {
            BoundProperty = pi;
            Options.AllowRunTimeSummaryChange = true;
            Options.ShowSummaryTypeName = true;
        }
    }
}
