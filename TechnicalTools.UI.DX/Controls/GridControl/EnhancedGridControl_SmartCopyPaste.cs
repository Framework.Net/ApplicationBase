﻿using System;
using System.Windows.Forms;

using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Diagnostics;


namespace TechnicalTools.UI.DX
{
    // Note that file EnhancedGridView_CopyValue.cs handles any value by using a contextual menu
    public partial class EnhancedGridControl
    {
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (!(FocusedView is GridView view)) // Tester que seule une celule a le focus, et pas une row ou plusieurs cellules
            {
                base.OnKeyDown(e);
                return;
            }

            if (e.Control && e.KeyCode == Keys.C)
            {
                base.OnKeyDown(e);
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
                {
                    var value = view.GetFocusedValue();
                    AddValueToClipBoard(value);
                });
            }
            else if (e.Control && e.KeyCode == Keys.V)
            {
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
                {
                    var value = view.GetFocusedValue();
                    if (GetValueFromClipBoard(out value))
                    {
                        view.SetRowCellValue(view.FocusedRowHandle, view.FocusedColumn, value);
                        e.Handled = true;
                    }
                });
                if (!e.Handled)
                    base.OnKeyDown(e);
            }
            else
                base.OnKeyDown(e);
        }

        bool AddValueToClipBoard(object value, int retryTimes = 0, int retryDelay = 0)
        {
            ClipBoardValue cbValue = null;

            if (value == null)
                cbValue = new ClipBoardValue() { Type = null, Value = null };
            else if (value.GetType().IsEnum)
                cbValue = new ClipBoardValue() { Type = value.GetType(), Value = value };
            else
                return false;

            // Il Semble qu'on soit obligé de creer un nouveau object.
            // Ajouter le nouveau format a l'objet existant semble ne pas marcher.
            IDataObject newData = new DataObject();
            // recopie
            IDataObject curData = Clipboard.GetDataObject();
            if (curData != null)
                foreach (var fmt in curData.GetFormats())
                    newData.SetData(fmt, curData.GetData(fmt));

            newData.SetData(typeof(ClipBoardValue).Name, false, cbValue);
            Clipboard.SetDataObject(newData, true, retryTimes, retryDelay);
            return true;
        }
        [Serializable]
        class ClipBoardValue
        {
            public Type Type;
            public object Value;
        }
        bool GetValueFromClipBoard(out object value)
        {
            IDataObject curData = Clipboard.GetDataObject();
            value = null;
            if (!curData.GetDataPresent(typeof(ClipBoardValue).Name))
                return false;
            var cbValue = curData.GetData(typeof(ClipBoardValue).Name) as ClipBoardValue;
            if (cbValue.Type == null)
                value = null;
            else
                value = cbValue.Value;
            return true;
        }
    }
    public partial class EnhancedGridView
    {
         #region Copy / Paste

        void CopyPaste_Configure()
        {
            if (!DesignTimeHelper.IsInDesignMode)
            {
                // Quand on clique une cellule, elle recoit le focus (rectangle en pointille) mais sans que l'editeur ne soit activé (curseur non visible)
                // Il faudrait recliquer pour ca
                // Quand on fait Ctrl-C, cela copie la ligne complete (+ les titres)
                // Cette ligne a pour but de ne copier que le contenu de la cellule
                OptionsSelection.MultiSelectMode = GridMultiSelectMode.CellSelect;
                OptionsSelection.MultiSelect = true;
                OptionsClipboard.CopyColumnHeaders = DevExpress.Utils.DefaultBoolean.False;
            }
        }

        internal void CopyPaste_Unconfigure()
        {
            if (!DesignTimeHelper.IsInDesignMode)
            {
                // Quand on clique une cellule, elle recoit le focus (rectangle en pointille) mais sans que l'editeur ne soit activ (curseur non visible)
                // Il faudrait recliquer pour ca
                // Quand on fait Ctrl-C, cela copie la ligne complete (+ les titres)
                // Cette ligne a pour but de ne copier que le contenu de la cellule
                OptionsSelection.MultiSelectMode = GridMultiSelectMode.RowSelect;
                OptionsSelection.MultiSelect = false;
                OptionsClipboard.CopyColumnHeaders = DevExpress.Utils.DefaultBoolean.Default;
            }
        }

        #endregion
    }
    public partial class EnhancedBandedGridView
    {
        #region Copy / Paste

        void CopyPaste_Configure()
        {
            if (!DesignTimeHelper.IsInDesignMode)
            {
                // Quand on clique une cellule, elle recoit le focus (rectangle en pointille) mais sans que l'editeur ne soit activ (curseur non visible)
                // Il faudrait recliquer pour ca
                // Quand on fait Ctrl-C, cela copie la ligne complete (+ les titres)
                // Cette ligne a pour but de ne copier que le contenu de la cellule
                OptionsSelection.MultiSelectMode = GridMultiSelectMode.CellSelect;
                OptionsSelection.MultiSelect = true;
                OptionsClipboard.CopyColumnHeaders = DevExpress.Utils.DefaultBoolean.False;
            }
        }

        internal void CopyPaste_Unconfigure()
        {
            if (!DesignTimeHelper.IsInDesignMode)
            {
                // Quand on clique une cellule, elle recoit le focus (rectangle en pointille) mais sans que l'editeur ne soit activ (curseur non visible)
                // Il faudrait recliquer pour ca
                // Quand on fait Ctrl-C, cela copie la ligne complete (+ les titres)
                // Cette ligne a pour but de ne copier que le contenu de la cellule
                OptionsSelection.MultiSelectMode = GridMultiSelectMode.RowSelect;
                OptionsSelection.MultiSelect = false;
                OptionsClipboard.CopyColumnHeaders = DevExpress.Utils.DefaultBoolean.Default;
            }
        }
        #endregion
    }
}
