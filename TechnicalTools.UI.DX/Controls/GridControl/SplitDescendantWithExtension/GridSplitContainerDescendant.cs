﻿using System;

using DevExpress.XtraGrid;


namespace TechnicalTools.UI.DX
{
    // from https://www.devexpress.com/Support/Center/Example/Details/E3045
    public class GridSplitContainerDescendant : GridSplitContainer
    {
        protected override DevExpress.XtraEditors.Drawing.SplitContainerViewInfo CreateContainerInfo()
        {
            return new SplitContainerViewInfoDescendant(this);
        }
        protected override void OnSplitterPositionChanged()
        {
            base.OnSplitterPositionChanged();
        }
    }
}
