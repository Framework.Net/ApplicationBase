﻿using System;
using System.Drawing;

using DevExpress.XtraEditors.Drawing;


namespace TechnicalTools.UI.DX
{
    // from https://www.devexpress.com/Support/Center/Example/Details/E3045
    public class SplitContainerViewInfoDescendant : SplitContainerViewInfo
    {
        public SplitContainerViewInfoDescendant(DevExpress.XtraEditors.SplitContainerControl container)
            : base(container)
        {
        }

        protected override void CalcInfo()
        {
            base.CalcInfo();
        }
        protected override void UpdatePanelBounds()
        {
            base.UpdatePanelBounds();
            Rectangle rect = Panel2Info.Bounds;
            rect.Height += Splitter.Bounds.Height;
            rect.Y -= Splitter.Bounds.Height;
            Panel2Info.Bounds = rect;
            rect = Splitter.Bounds;
            rect.Height = 0;
            Splitter.Bounds = rect;
        }
    }
}
