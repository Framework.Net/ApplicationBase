﻿using System;

using DevExpress.XtraGrid;


namespace TechnicalTools.UI.DX
{
    // Classes implementing IEnhancedGridView are currently EnhancedGridView & BandedGridView
    // These classes already implement implicitely this interface because their base class (GridView) does it
    // This avoid to manipulate IEnhancedGridView as being a GridView for Sure..
    //
    // Class Diagram :
    // GridView =>                   EnhancedGridView
    //          => BandedGridView => EnhancedBandedGridView
    // Devexpress can manipulate a BandedGridView like a "GridView"
    // With IEnhancedGridView, we can manipulate enhanced features for both enhanced classes
    public partial interface IEnhancedGridView_Inherited
    {
        GridControl GridControl { get; }
    }
}
