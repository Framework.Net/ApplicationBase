﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Tools;


namespace TechnicalTools.UI.DX
{
    /// <summary>
    /// <para>Permet la configuration d'une colonne d'une gridview pour selectionner un objet</para>
    /// <para>Utilisation :
    /// myGridColumn.ConfigureForObjects&lt;yourEnumType&gt;();</para>
    ///
    /// <para>Gère plusieurs choses :
    /// - Traduction d'un objet en nom metier)
    /// - Traduction egalement dans la row filter
    /// - Traduction dans la popup filter (Si OptionsView.ShowAutoFilterRow = true)
    /// - TODO : Le tri se fait dans l'ordre du nom et non pas des id ou de la reference vers l'objet</para>
    /// </summary>
    public static class GridColumn_ExtensionsForObjectList
    {
        interface IHelperBase { void Uninstall(); }

        class Helper<TGridObject, TCheckableItem> : IHelperBase
            where TGridObject : class
            where TCheckableItem : class
        {
            readonly GridColumn _column;
            readonly List<TCheckableItem> _checkableItems;
            readonly Func<TCheckableItem, string> _getString;
            readonly Func<TGridObject, IList<TCheckableItem>> _getListToEdit;
            RepositoryItemCheckedComboBoxEdit _repoEditor;

            public Helper(GridColumn col, Func<TGridObject, IList<TCheckableItem>> getListToEdit, Func<TCheckableItem, string> getString, List<TCheckableItem> checkableItems)
            {
                _column = col;
                _checkableItems = checkableItems;
                _getListToEdit = getListToEdit;
                _getString = getString;
            }

            public void Install()
            {
                _column.View.CustomColumnDisplayText += View_CustomColumnDisplayText;
                _column.SortMode = ColumnSortMode.DisplayText;
                _column.FilterMode = ColumnFilterMode.DisplayText;

                _column.View.GridControl.RepositoryItems.AddRange(new RepositoryItem[] { _repoEditor });
                _repoEditor.AutoHeight = false;
                _repoEditor.Name = _column.View.Name + "_autoRepoForEnum_" + typeof(TGridObject).Name;

                _repoEditor.EditValueChanged += RepositoryEditor_EditValueChanged;
                _repoEditor.CustomDisplayText += RepositoryEditor_CustomDisplayText;
                _repoEditor.DataSource = _checkableItems;
                _column.ColumnEdit = _repoEditor;

                _repoEditor = new RepositoryItemCheckedComboBoxEdit
                {
                    EditValueType = EditValueTypeCollection.List
                };

                _column.UnboundType = DevExpress.Data.UnboundColumnType.Object;
                _column.ColumnEdit = _repoEditor;

                _column.View.CustomUnboundColumnData += View_CustomUnboundColumnData;
                _column.View.CustomColumnDisplayText += View_CustomColumnDisplayText;
            }


            public void Uninstall()
            {
                if (_column.View != null)
                {
                    _column.View.CustomColumnDisplayText -= View_CustomColumnDisplayText;
                    _column.View.CustomUnboundColumnData -= View_CustomUnboundColumnData;
                    _column.View.GridControl.RepositoryItems.Remove(_repoEditor);
                }
                _repoEditor.EditValueChanged -= RepositoryEditor_EditValueChanged;
                _repoEditor.CustomDisplayText -= RepositoryEditor_CustomDisplayText;

                _column.ColumnEdit = null;
                _repoEditor.DataSource = null;
                _repoEditor = null;
            }


            private void RepositoryEditor_EditValueChanged(object sender, EventArgs e)
            {
                _column.View.PostEditor();
            }

            private void View_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
            {
                if (e.IsForGroupRow)
                    return;
                if (e.Column == _column)
                {
                    var gridObject = (TGridObject)(_column.View.DataSource as IList)[e.ListSourceRowIndex];
                    var items = _getListToEdit(gridObject);
                    e.DisplayText = string.Join(", ", items.Select(item => _getString(item)));
                }
            }
            private void RepositoryEditor_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
            {
                if (e.Value == null)
                    return;
                var items = ((IEnumerable)e.Value).Cast<TCheckableItem>();
                e.DisplayText = string.Join(", ", items.Select(item => _getString(item)));
            }

            private void View_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
            {
                if (e.Column != _column)
                    return;
                var view = sender as GridView;
                var ds = view.DataSource as IList;
                var gridObject = (TGridObject)ds[e.ListSourceRowIndex];
                var itemsToEdit = _getListToEdit(gridObject);
                if (e.IsGetData)
                {
                    e.Value = itemsToEdit;
                }
                else if (e.IsSetData)
                {
                    var itemsSelected = ((IEnumerable)e.Value).Cast<TCheckableItem>().ToList();
                    itemsToEdit.Clear();
                    foreach (var item in itemsSelected)
                        itemsToEdit.Add(item);
                }
            }
        }

        static readonly Dictionary<GridColumn, IHelperBase> Helpers = new Dictionary<GridColumn, IHelperBase>();
        public static void Install<TGridObject, TCheckableItem>(GridColumn col, Func<TGridObject, IList<TCheckableItem>> getListToEdit, Func<TCheckableItem, string> getString, List<TCheckableItem> checkableItems)
            where TGridObject : class
            where TCheckableItem : class
        {
            if (Helpers.ContainsKey(col))
                return; // déjà installé, donc on quitte

            var helper = new Helper<TGridObject, TCheckableItem>(col, getListToEdit, getString, checkableItems);
            Helpers.Add(col, helper);
            col.Disposed += col_Disposed;
            helper.Install();
        }

        static void col_Disposed(object sender, EventArgs e)
        {
            ((GridColumn)sender).Disposed -= col_Disposed;
            Uninstall((GridColumn)sender);
        }
        // Installe une colonne permettant d'editer un champs de type objet, en fournissant une collection d'objet et un moyen d'afficher un nom par objet
        public static TGridColumn ConfigureForObjects<TGridColumn, TGridObject, TCheckableItem>(this TGridColumn col, Func<TGridObject, IList<TCheckableItem>> getListToEdit, Func<TCheckableItem, string> getString, List<TCheckableItem> checkableItems, Disambiguator<TGridColumn, GridColumn> _ = null)
            where TGridColumn : GridColumn // Allow for GridColumn and BandedGridColumn
            where TGridObject : class
            where TCheckableItem : class
        {
            Install(col, getListToEdit, getString, checkableItems);
            return col;
        }

        public static void Uninstall(GridColumn col)
        {
            if (!Helpers.ContainsKey(col))
                return; // Rien à faire car déjà désinstallé

            Helpers[col].Uninstall();
            Helpers.Remove(col);
            col.Disposed -= col_Disposed;
        }


    }
}
