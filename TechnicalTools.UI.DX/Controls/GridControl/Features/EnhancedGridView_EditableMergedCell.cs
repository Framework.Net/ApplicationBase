﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

using DevExpress.Utils;
using DevExpress.XtraEditors.Container;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Registrator;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

using TechnicalTools.Diagnostics;


namespace TechnicalTools.UI.DX
{
    // Allow to edit merged cell (only tested on checkbox)
    // From https://github.com/DevExpress-Examples/how-to-edit-merged-cells-in-gridview-e4117
    public class EnhancedGridView_EditableMergedCell
    {
        readonly IEnhancedGridView _view;

        public EnhancedGridView_EditableMergedCell(IEnhancedGridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(IEnhancedGridView view)
        {
            Debug.Assert(view != null);
            view.BeforeActivateEditor -= view_AfterActivateEditor; view.BeforeActivateEditor += view_AfterActivateEditor;
            view.BeforePostEditor -= view_BeforePostEditor; view.BeforePostEditor += view_BeforePostEditor;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(IEnhancedGridView view)
        {
            view.BeforeActivateEditor -= view_AfterActivateEditor;
            view.BeforePostEditor -= view_BeforePostEditor;
        }

        internal void Assign(EnhancedGridView_EditableMergedCell f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }

        bool view_AfterActivateEditor(object sender, GridCellInfo cell)
        {
            if (cell.MergedCell != null)
            {
                var gridView = sender as GridView;
                var gv = GridViewInternalAccessor.BorrowAccessor(gridView);
                try
                {
                    ActivateMergedCellEditor(gv, cell);
                }
                finally
                {
                    GridViewInternalAccessor.ReleaseAccessor(gv);
                }
                return true;
            }
            return false;
        }
        private void ActivateMergedCellEditor(GridViewInternalAccessor gv, GridCellInfo cell)
        {
            if (cell == null)
                return;
            cell = cell.MergedCell.MergedCells[0];
            gv.fEditingCell = cell;
            Rectangle bounds = GetMergedEditorBounds(gv, cell);
            if (bounds.IsEmpty)
                return;
            RepositoryItem cellEdit = gv.RequestCellEditor(cell);
            gv.ViewInfo.UpdateCellAppearance(cell);
            gv.ViewInfo.RequestCellEditViewInfo(cell);
            AppearanceObject appearance = new AppearanceObject();
            AppearanceHelper.Combine(appearance, new AppearanceObject[] { gv.GetEditorAppearance(), gv.ViewInfo.PaintAppearance.Row, cell.Appearance });
            if (cellEdit != cell.Editor && cellEdit.DefaultAlignment != HorzAlignment.Default)
                appearance.TextOptions.HAlignment = cellEdit.DefaultAlignment;
            gv.UpdateEditor(cellEdit, new UpdateEditorInfoArgs(gv.GetColumnReadOnly(cell.ColumnInfo.Column), bounds, appearance, cell.CellValue, gv.ElementsLookAndFeel, cell.ViewInfo.ErrorIconText, cell.ViewInfo.ErrorIcon));
            gv.ViewInfo.UpdateCellAppearance(cell);
            if (cell != null)
                gv.GridView.InvalidateRow(cell.RowHandle);
            var gridView = gv.GridView; // Fix gridview because gv value will change
            cellEdit.EditValueChanged -= CellEdit_EditValueChanged;
            cellEdit.EditValueChanged += CellEdit_EditValueChanged;
        }

        private void CellEdit_EditValueChanged(object sender, EventArgs e)
        {
            var gridView = _view as GridView;
            gridView.PostEditor();
            gridView.InvalidateRow(gridView.FocusedRowHandle);
        }

        Rectangle GetMergedEditorBounds(GridViewInternalAccessor gv, GridCellInfo cell)
        {
            Rectangle r = cell.CellValueRect;
            Rectangle bounds = gv.ViewInfo.UpdateFixedRange(r, cell.ColumnInfo);
            if (bounds.Right > gv.ViewInfo.ViewRects.Rows.Right)
                bounds.Width = gv.ViewInfo.ViewRects.Rows.Right - bounds.Left;
            if (bounds.Bottom > gv.ViewInfo.ViewRects.Rows.Bottom)
                bounds.Height = gv.ViewInfo.ViewRects.Rows.Bottom - bounds.Top;
            if (bounds.Width < 1 || bounds.Height < 1)
                return Rectangle.Empty;

            for (int i = 1; i < cell.MergedCell.MergedCells.Count; i++)
                bounds.Height += cell.MergedCell.MergedCells[i].Bounds.Height;
            return bounds;
        }
        private bool view_BeforePostEditor(object sender, bool causeValidation)
        {
            var gridView = sender as GridView;
            var gv = GridViewInternalAccessor.BorrowAccessor(gridView);
            try
            {
                if (gv.GridView.IsEditing && gv.fEditingCell.MergedCell != null)
                {
                    if (causeValidation && !gridView.ValidateEditor())
                        return false;
                    // Capture because the path becomes null as soon as we change the first value
                    var mergedCells = gv.fEditingCell.MergedCell.MergedCells.Select(mc => mc.RowHandle).ToList();
                    var mergedCol = gv.fEditingCell.Column;
                    object CurValue = gv.ExtractEditingValue(mergedCol, gridView.EditingValue);
                    gridView.BeginUpdate();
                    for (int i = 0; i < mergedCells.Count; i++)
                        gridView.SetRowCellValue(mergedCells[i], mergedCol, CurValue);
                    gridView.EndUpdate();
                }
                return true;
            }
            finally
            {
                GridViewInternalAccessor.ReleaseAccessor(gv);
            }
        }

        // Make accessible some internals of GridView
        class GridViewInternalAccessor
        {
            public static GridViewInternalAccessor BorrowAccessor(GridView gv)
            {
                GridViewInternalAccessor t = null;
                int tries = 3;
                while (tries > 0)
                {
                    t = Interlocked.CompareExchange(ref _Accessor, null, _Accessor);
                    if (t != null)
                    {
                        t.GridView = gv;
                        return t;
                    }
                    // we never should be here because we are in UI layer...
                    DebugTools.Break();
                    Application.DoEvents(); // Very bad but what other choice do we have ?
                }
                return t;
            }
            public static void ReleaseAccessor(GridViewInternalAccessor accessor)
            {
                accessor.GridView = null;
                _Accessor = accessor;
            }
            static GridViewInternalAccessor _Accessor = new GridViewInternalAccessor();

            private GridViewInternalAccessor() { }
            public GridView GridView { get; private set; }

            public GridCellInfo fEditingCell
            {
                get { return (GridCellInfo)AssertionOnGridViewNames._fEditingCell.GetValue(GridView); }
                set { AssertionOnGridViewNames._fEditingCell.SetValue(GridView, value); }
            }
            public RepositoryItem RequestCellEditor(GridCellInfo cell)
            {
                return (RepositoryItem)AssertionOnGridViewNames._RequestCellEditor.Invoke(GridView, new object[] { cell });
            }
            public GridViewInfo ViewInfo
            {
                get { return (GridViewInfo)AssertionOnGridViewNames._ViewInfo.GetValue(GridView); }
                set { AssertionOnGridViewNames._ViewInfo.SetValue(GridView, value); }
            }
            public GridEmbeddedLookAndFeel ElementsLookAndFeel
            {
                get { return (GridEmbeddedLookAndFeel)AssertionOnGridViewNames._ElementsLookAndFeel.GetValue(GridView); }
            }
            public AppearanceObject GetEditorAppearance()
            {
                return (AppearanceObject)AssertionOnGridViewNames._GetEditorAppearance.Invoke(GridView, null);
            }
            public bool GetColumnReadOnly(GridColumn col)
            {
                return (bool)AssertionOnGridViewNames._GetColumnReadOnly.Invoke(GridView, new object[] { col });
            }
            public void UpdateEditor(RepositoryItem ritem, UpdateEditorInfoArgs args)
            {
                AssertionOnGridViewNames._UpdateEditor.Invoke(GridView, new object[] { ritem, args });
            }
            public object ExtractEditingValue(GridColumn column, object editingValue)
            {
                return AssertionOnGridViewNames._ExtractEditingValue.Invoke(GridView, new object[] { column, editingValue });
            }
            class AssertionOnGridViewNames : GridView
            {
                internal static readonly FieldInfo _fEditingCell = typeof(GridView).GetField(nameof(fEditingCell), BindingFlags.Instance | BindingFlags.NonPublic);
                public new GridEmbeddedLookAndFeel ElementsLookAndFeel { get; set; }
                internal static readonly PropertyInfo _ElementsLookAndFeel = typeof(GridView).GetProperty(nameof(ElementsLookAndFeel), BindingFlags.Instance | BindingFlags.NonPublic);

                protected override RepositoryItem RequestCellEditor(GridCellInfo cell) { return null; }
                internal static readonly MethodInfo _RequestCellEditor = typeof(GridView).GetMethod(nameof(RequestCellEditor), BindingFlags.Instance | BindingFlags.NonPublic);
                protected new GridViewInfo ViewInfo { get; }
                internal static readonly PropertyInfo _ViewInfo = typeof(GridView).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic).First(p => p.Name == nameof(ViewInfo));
                protected new AppearanceObject GetEditorAppearance() { return null; }
                internal static readonly MethodInfo _GetEditorAppearance = typeof(GridView).GetMethod(nameof(GetEditorAppearance), BindingFlags.Instance | BindingFlags.NonPublic);
                protected new bool GetColumnReadOnly(GridColumn col) { return false; }
                internal static readonly MethodInfo _GetColumnReadOnly = typeof(GridView).GetMethod(nameof(GetColumnReadOnly), BindingFlags.Instance | BindingFlags.NonPublic);
                protected new void UpdateEditor(RepositoryItem ritem, UpdateEditorInfoArgs args) { /*Nothing*/ }
                internal static readonly MethodInfo _UpdateEditor = typeof(GridView).GetMethod(nameof(UpdateEditor), BindingFlags.Instance | BindingFlags.NonPublic);
                protected new object ExtractEditingValue(GridColumn column, object editingValue) { return null; }
                internal static readonly MethodInfo _ExtractEditingValue = typeof(GridView).GetMethod(nameof(ExtractEditingValue), BindingFlags.Instance | BindingFlags.NonPublic);
            }
        }
    }
}
