﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


namespace TechnicalTools.UI.DX
{
    [ToolboxItem(false)]
    public partial class EnhancedGridView_FindColumn_Form : XtraUserControl
    {
        readonly GridView _view;
        XtraForm _ownForm;

        public string ColumnToFind { get { return txtColumnCaptionToFind?.Text; } }

        public EnhancedGridView_FindColumn_Form(GridView view)
        {
            _view = view;
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            Text = "Finding a column";

            DefineEnabilitiesAndVisibilities();
            txtColumnCaptionToFind.Text = TryRestoreLastSearchedCaptionFor(_view);
        }

        public void DefineEnabilitiesAndVisibilities()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            if (InvokeRequired)
            {
                BeginInvoke((Action)DefineEnabilitiesAndVisibilities);
                return;
            }
            btnFind.Enabled = !string.IsNullOrWhiteSpace(txtColumnCaptionToFind.Text);
        }

        public Form ShowInForm(Form owner)
        {
            Dock = DockStyle.Fill;
            var size = Size;

            _ownForm = new XtraForm
            {
                Text = Text,
                ClientSize = size,
                Opacity = 0,
                StartPosition = FormStartPosition.CenterParent
            };
            _ownForm.Controls.Add(this);
            _ownForm.AcceptButton = btnFind;
            _ownForm.CancelButton = btnCancel;

            _ownForm.Load += (_, __) =>
            {
                AutoSizeWindowGivenLayoutContent();
                _ownForm.ClientSize = ClientSize;
                _ownForm.Left = owner.Left + (owner.Width - _ownForm.Width) / 2;
                _ownForm.Top = owner.Top + (owner.Height - _ownForm.Height) / 2;

                _ownForm.Opacity = 100;
                txtColumnCaptionToFind.Focus();
            };
            _ownForm.ShowDialog(owner);
            return _ownForm;
        }

        void AutoSizeWindowGivenLayoutContent()
        {
            if (layoutControl1.Root == null)
                return;
            var newSize = new Size(Math.Max(ClientSize.Width, layoutControl1.Root.MinSize.Width),
                                    Math.Max(ClientSize.Height, layoutControl1.Root.MinSize.Height));
            if (newSize != ClientSize)
                ClientSize = newSize;
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            Debug.Assert(_ownForm != null);

            SaveSearchedCaptionFor(_view, txtColumnCaptionToFind.Text.Trim());

            var matchedCol = FindBestMatchColumn(_view, txtColumnCaptionToFind.Text.Trim());
            if (matchedCol == null)
                return;
            var matchedBandedCol = matchedCol as BandedGridColumn;
            if (matchedCol.GroupIndex >= 0 && // Mean the column is in grouppanel
                                              // except if the column is also with other columns when view is a bandedgridview
                matchedBandedCol?.OwnerBand == null)
            {
                _view.LeftCoord = 0;
                _view.GridControl.BeginInvoke((Action)(() => XtraMessageBox.Show("This column is in group panel!")));
                return;
            }

            if (matchedCol.VisibleIndex < 0)
            {
                matchedCol.Visible = true;
                Debug.Assert(matchedCol.VisibleIndex >= 0);
                if (matchedBandedCol != null && matchedBandedCol.OwnerBand == null)
                {
                    //band.Columns.Add(matchedBandedCol);
                    var mostRightSelectedColumn = _view.GetSelectedCells()
                                                       .GroupBy(c => c.RowHandle)
                                                       .Select(grp => grp.Last().Column)
                                                       .OrderBy(c => c.AbsoluteIndex)
                                                       .LastOrDefault();
                    if (mostRightSelectedColumn == null)
                        // Get the last visible column of the first band that have any visible columns
                        mostRightSelectedColumn = matchedBandedCol.View.Bands.Reverse()
                                                                       .SelectMany(b => b.Columns.Cast<GridColumn>())
                                                                       .Where(col => col.Visible)
                                                                       .LastOrDefault();
                    matchedBandedCol.InsertAfter(mostRightSelectedColumn);
                }
                _view.GridControl.BeginInvoke((Action)(() => XtraMessageBox.Show("This column exists but was hidden. I made it visible for you!")));
            }


            MoveScrollBarTo(_view, matchedCol);

            if (_view.OptionsSelection.MultiSelectMode == GridMultiSelectMode.CellSelect)
            {
                var selectedCells = _view.GetSelectedCells();
                var rhs = selectedCells.Select(col => col.RowHandle).ToList();
                foreach (var rh in rhs)
                    _view.SelectCell(rh, matchedCol); // add cell to selected cell in case of cell select mode
                foreach (var cell in selectedCells)
                    _view.UnselectCell(cell);
                if (selectedCells.Length > 0) // Should be always true... but we never know
                {
                    _view.FocusedRowHandle = selectedCells.First().RowHandle;
                    _view.FocusedColumn = selectedCells.First().Column;
                }
            }

            _ownForm.DialogResult = DialogResult.OK;
        }

        GridColumn FindBestMatchColumn(GridView view, string captionToFind)
        {
            var cols = view.Columns.Cast<GridColumn>().OrderBy(col => col.VisibleIndex).ToList();
            GridColumn matchedCol;
            if (view.OptionsSelection.MultiSelectMode == GridMultiSelectMode.CellSelect)
            {
                var index = cols.IndexOf(view.FocusedColumn);
                cols = cols.Skip(index).Concat(cols.Take(index)).ToList();
            }
            matchedCol = cols.FirstOrDefault(col => col.GetCaption().ToLowerInvariant().Contains(captionToFind.ToLowerInvariant()));

            if (matchedCol == null)
            {
                matchedCol = cols.OrderBy(col => TechnicalTools.Algorithm.String.DamerauLevenshteinDistance(col.GetCaption(), captionToFind, int.MaxValue)).FirstOrDefault();
                if (matchedCol != null)
                {
                    var answer = XtraMessageBox.Show(FindForm(), $"Did you mean \"{matchedCol.GetCaption()}\"?", "Not exactly found...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                    if (answer != DialogResult.Yes)
                        matchedCol = null;
                }
                else
                    XtraMessageBox.Show("No column caption match this text!");
            }
            return matchedCol;
        }

        internal static void MoveScrollBarTo(GridView view, GridColumn matchedCol)
        {
            var cols = view.Columns.Cast<GridColumn>().OrderBy(c => c.VisibleIndex).ToList();
            var offset = cols.Where(c => c.VisibleIndex < matchedCol.VisibleIndex
                                      && c.GroupIndex < 0 // column NOT in group panel (because when column are grouped VisibleIndex can be still positive
                                      && c.Visible) // maybe useless but what about column in group panel AND visible like nay other (only possible in BandedGridView i think)
                                 .Select(c => c.Width)
                                 .DefaultIfEmpty(0)
                                 .Sum();
            var viewInfo = view.GetViewInfo() as GridViewInfo;
            if (viewInfo.HScrollBarPresence == ScrollBarPresence.Visible)
                view.LeftCoord = offset + (matchedCol.Width - view.GridControl.ClientSize.Width) / 2;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Debug.Assert(_ownForm != null);
            _ownForm.DialogResult = DialogResult.Cancel;
        }

        private void txtColumnCaptionToFind_EditValueChanged(object sender, EventArgs e)
        {
            DefineEnabilitiesAndVisibilities();
        }


        string TryRestoreLastSearchedCaptionFor(GridView view)
        {
            if (_lastCaptionSearched.TryGetValue(_view, out string lastCaption) ||
                !string.IsNullOrWhiteSpace(_view.Name) && _lastCaptionSearchedByGridViewName.TryGetValue(_view.Name, out lastCaption))
                return lastCaption;
            return null;
        }
        void SaveSearchedCaptionFor(GridView view, string caption)
        {
            _lastCaptionSearched.Remove(_view);
            _lastCaptionSearched.Add(_view, caption);
            if (!string.IsNullOrWhiteSpace(_view.Name))
                _lastCaptionSearchedByGridViewName[_view.Name] = caption;
        }
        static readonly ConditionalWeakTable<GridView, string> _lastCaptionSearched = new ConditionalWeakTable<GridView, string>();
        static readonly Dictionary<string, string> _lastCaptionSearchedByGridViewName = new Dictionary<string, string>();
    }
}
