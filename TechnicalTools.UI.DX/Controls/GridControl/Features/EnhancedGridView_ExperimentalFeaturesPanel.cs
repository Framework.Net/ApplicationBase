﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;


namespace TechnicalTools.UI.DX
{
    [ToolboxItem(false)]
    public partial class EnhancedGridView_ExperimentalFeaturesPanel : XtraUserControl
    {
        readonly GridView _view;
        Form _ownForm;

        public EnhancedGridView_ExperimentalFeaturesPanel(GridView view)
        {
            _view = view;
            InitializeComponent();
            if (_ownForm != null)
            {
                btnClose_LayoutItem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                emptySpaceLeftCloseButton.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        public Form ShowInForm(Form owner)
        {
            Dock = DockStyle.Fill;
            _ownForm = new XtraForm
            {
                Text = "Advanced gridview configuration (and experimental stuff)",
                Opacity = 0,
                StartPosition = FormStartPosition.CenterParent
            };
            _ownForm.Controls.Add(this);
            _ownForm.Load += (_, __) =>
            {
                AutoSizeWindowGivenLayoutContent();
                _ownForm.ClientSize = ClientSize;
                _ownForm.Left = owner.Left + (owner.Width - _ownForm.Width) / 2;
                _ownForm.Top = owner.Top + (owner.Height - _ownForm.Height) / 2;

                _ownForm.Opacity = 100;
            };
            _ownForm.ShowDialog(owner);
            return _ownForm;
        }

        void AutoSizeWindowGivenLayoutContent()
        {
            if (layoutControl1.Root == null)
                return;
            var newSize = new Size(Math.Max(ClientSize.Width, layoutControl1.Root.MinSize.Width),
                                    Math.Max(ClientSize.Height, layoutControl1.Root.MinSize.Height));
            if (newSize != ClientSize)
                ClientSize = newSize;
        }

        private void chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor_CheckedChanged(object sender, EventArgs e)
        {
            _view.FilterEditorCreated -= OnViewFilterEditorCreated;
            if (chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor.Checked)
                _view.FilterEditorCreated += OnViewFilterEditorCreated;
        }
        static void OnViewFilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            e.FilterControl.ShowOperandTypeIcon = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Debug.Assert(_ownForm != null);
            _ownForm.DialogResult = DialogResult.OK;
        }
    }
}
