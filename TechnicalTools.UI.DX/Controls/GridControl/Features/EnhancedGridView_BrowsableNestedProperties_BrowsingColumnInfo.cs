﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

using DevExpress.Data;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Analysis;
using TechnicalTools.Annotations;
using TechnicalTools.Model;
using TechnicalTools.UI.DX.Helpers;


namespace TechnicalTools.UI.DX
{
    public abstract class BrowsingColumnInfo
    {
        /// <summary>
        /// Source allowing to chain values to get.
        /// May be null if we directly get datasource item Source or we extend valeu from a column
        /// </summary>
        internal BrowsingColumnInfo Source { get; private set; }
        /// <summary>
        /// When Source is null, this is the original column where the value is from.
        /// If OriginColumn is null too, the source is datasource item
        /// </summary>
        internal GridColumn OriginColumn { get; private set; }

        /// <summary>Column representing the current instance of BrowsingColumnInfo, may be null</summary>
        internal GridColumn DisplayColumn
        {
            get { return _displayColumn; }
            set
            {
                if (_displayColumn is IEnhancedGridColumn ecol)
                    ecol.RelatedPropertyInfo = null;
                _displayColumn = value;
                ecol = _displayColumn as IEnhancedGridColumn;
                if (ecol != null)
                    ecol.RelatedPropertyInfo = this;
            }
        }

        /// <summary> All dependencies to other BrowsingColumnInfo (except Source)
        /// Useful when multiple columns work together
        /// </summary>
        public List<BrowsingColumnInfo> Dependencies { get; protected set; }

        protected BrowsingColumnInfo(BrowsingColumnInfo source, GridColumn originColumn)
        {
            Source = source;
            OriginColumn = originColumn;
        }
        protected BrowsingColumnInfo()
        {
        }


        /// <summary> Enumerate all parents column that are BrowsingColumnInfo, from the nearest parent to the older one </summary>
        public IEnumerable<BrowsingColumnInfo> ParentSources
        {
            get
            {
                if (Source != null)
                {
                    yield return Source;
                    foreach (var p in Source.ParentSources)
                        yield return p;
                }
            }
        }

        /// <summary>
        /// Returns a unique name depending on the path of browsing in data structure explored by user.
        /// </summary>
        /// <remarks>
        /// Devexpress requires a unique fieldname even for unbound columns so we need this.
        /// The value must not point to an existing property (or sub property).
        /// To ensure this requirement, you can prefix any value character not in property name <see cref="MagicFieldNamePrefix"/>
        /// - For same two browsing paths, keys must be the same.
        /// - If two paths are different UniqueFieldName must return different values
        /// Please note there is still a corner case not handled yet :
        /// Exemple: Grid display a list of items of type "A".
        ///          B and C are also types that inherits from A.
        ///          Both B and C declare property "Foo" (but not A)
        ///          The path would be identical...
        ///          TODO : (later, not urgent) : prefix UniqueFieldName parts with Type fullpath (to make it serializable)
        /// </remarks>
        public abstract string UniqueFieldNameTarget { get; }

        /// <summary>
        /// Return the path of property name (real or imaginated), prefixed by a magic string
        /// This is useful to know if a column in gridview already exists with same path.
        /// </summary>
        public string UniqueFieldName
        {
            get
            {
                // MagicFieldNamePrefix is inserted at position where column start to not exist by default.
                // Another way to tell it is : it is where user starts exploration.
                var res = Source?.UniqueFieldName.IfNotBlankAddSuffix(".");
                if (res == null)
                {
                    res = OriginColumn?.FieldName;
                    if (!res?.Contains(MagicFieldNamePrefix) ?? true)
                        res += MagicFieldNamePrefix;
                }
                return res + UniqueFieldNameTarget;
            }
        }
        /// <summary>
        /// See <see cref="UniqueFieldName"/> for explanation.
        /// It is considered unlikely that some other code will use the same string
        /// </summary>
        protected internal const string MagicFieldNamePrefix = "|=>";
        private GridColumn _displayColumn;

        /// <summary>
        /// Just the user friendly sright tip of the path of data, useful to display browsing path as a tree / menu.
        /// Example: "Another nested property"
        /// </summary>
        /// <remarks>Same than <see cref="BrowsingPath"/></remarks>
        public abstract string BrowsingPathTarget { get; }

        /// <summary>
        /// Full user friendly string explaning the path of data the user explore.
        /// Example: "(Row Item) -> SomeNotDisplayedCompositeObjectInstanceName-> AWeirdProperty -> Another nested property"
        /// </summary>
        /// <remarks>
        /// Do not use ']' character in result because DevExpress' Expression Editor cannot handle fieldname with it.
        /// See https://www.devexpress.com/Support/Center/Question/Details/Q277221/unboundexpression-when-field-names-have-square-brackets
        /// </remarks>
        public string BrowsingPath
        {
            get
            {
                var res = Source?.BrowsingPath ?? OriginColumn?.GetCaption();
                res += FieldSeparator + BrowsingPathTarget;
                return res;
            }
        }
        public static string FieldSeparator { get; } = " " + UnicodeChars.Heavy_RoundTipped_Rightwards_Arrow + " ";

        public abstract Type GetValueType();
        public UnboundColumnType AsDevExpressType() { return GetValueType().RemoveNullability().ToDevExpressGridViewUnboundType(); }

        /// <summary> Get the value of column for one item in datasource </summary>
        public virtual object GetValue(object dsItem, int dsItemIndex)
        {
            var scol = OriginColumn as IEnhancedGridColumn;
            var obj = Source != null ? Source.GetValue(dsItem, dsItemIndex)
                    // Extending a column value...
                    // Optimized way to get the column value ?
                    : scol?.UnboundValueGetter != null ? scol.UnboundValueGetter(dsItem, dsItemIndex)
                    // No optimized way... is there a column by the way ?
                    : OriginColumn != null ? OriginColumn.View.GetListSourceRowCellValue(dsItemIndex, OriginColumn)
                    // all s null ? It means we get dsItem
                    : dsItem;
            return obj;
        }

        public virtual XElement SaveToXml(XElement node, Func<BrowsingColumnInfo, int> refToId)
        {
            if (Source != null)
                // because of topological sort, refs should always contains bci.Source...
                node.Add(new XAttribute("Source", refToId(Source)));
            if (OriginColumn != null)
                node.Add(new XAttribute("OriginColumn", OriginColumn.FieldName));
            if (DisplayColumn != null)
                node.Add(new XAttribute("DisplayColumnFieldName", DisplayColumn.FieldName));
            return node;
        }
        public virtual void RestoreFromToXml(XElement node, GridView gv, Func<int, BrowsingColumnInfo> idToRef)
        {
            var aSource = node.Attribute("Source")?.Value;
            Source = aSource == null ? null : idToRef(int.Parse(aSource));
            var aOriginColumn = node.Attribute("OriginColumn")?.Value;
            OriginColumn = aOriginColumn == null ? null : gv.Columns.SingleOrDefault(col => col.FieldName == aOriginColumn);
            var aDisplayColumnFieldname = node.Attribute("DisplayColumnFieldName")?.Value;
            if (DisplayColumn?.FieldName != aDisplayColumnFieldname)
                DisplayColumn = aDisplayColumnFieldname == null ? null : gv.Columns.SingleOrDefault(col => col.FieldName == aDisplayColumnFieldname);
            if (aSource != null && Source == null ||
                aOriginColumn != null && OriginColumn == null)
                throw new TechnicalException("Invalid data!", null);
        }
    }
    public class RelatedPropertyInfo : BrowsingColumnInfo
    {
        public RelatedPropertyInfo(BrowsingColumnInfo source, GridColumn originColumn, PropertyInfo p)
           : base(source, originColumn)
        {
            Property = p;
        }
        protected internal RelatedPropertyInfo()
        {
        }
        /// <summary>Property Info to use (GetValue) on value processed by SourceColumn (if not null), or item in datasource</summary>
        public PropertyInfo      Property     { get; private set; }
        public override XElement SaveToXml(XElement node, Func<BrowsingColumnInfo, int> refToId)
        {
            base.SaveToXml(node, refToId);
            node.Add(new XAttribute("Property", Property.DeclaringType.ToAssemblyQualifiedStringWithoutVersion() + "|" + Property.Name));
            return node;
        }
        public override void RestoreFromToXml(XElement node, GridView gv, Func<int, BrowsingColumnInfo> idToRef)
        {
            base.RestoreFromToXml(node, gv, idToRef);
            var aProperty = node.Attribute("Property")?.Value;
            if (aProperty == null)
                throw new TechnicalException("Expecting \"Property\" attribute", null);
            var parts = aProperty.Split('|');
            if (parts.Length != 2)
                // actually this is the only known case but let's be diplomatic:/
                throw new TechnicalException($"Expecting two parts in Property attribute!" + Environment.NewLine +
                                             "Did someone edit the layout data?", null);
            var type = Type.GetType(parts[0])
                    // can happen on refactoring
                    ?? throw new TechnicalException($"Cannot find type \"{parts[0]}\" anymore!", null);
            Property = type.GetProperty(parts[1])
                    ?? throw new TechnicalException($"Cannot find property \"{parts[1]}\" of type \"{parts[0]}\"! anymore", null);
        }

        public override Type GetValueType() { return Property.PropertyType; }

        public override object GetValue(object dsItem, int dsItemIndex)
        {
            var obj = base.GetValue(dsItem, dsItemIndex);
            Debug.Assert(Property.DeclaringType != null);
            // In case of null value we stop, Or in case restore layout has issue on Source
            if (!Property.DeclaringType.IsInstanceOfType(obj))
                return null;
            obj = Property.GetValue(obj);
            return obj;
        }
        public override string UniqueFieldNameTarget { get { return Property.Name; } }
        public override string BrowsingPathTarget
        {
            get
            {
                string propName;
                // See base.BrowsingPathTarget about '<' '>' characters
                if (typeof(GridViewDataSourceFlattenizer.Join).IsAssignableFrom(Property.DeclaringType) &&
                    Property.Name.StartsWith(GridViewDataSourceFlattenizer.Join.LvlPrefix))
                {
                    var genTypes = Property.DeclaringType.GetGenericArguments();
                    Debug.Assert(int.TryParse(Property.Name.Substring(GridViewDataSourceFlattenizer.Join.LvlPrefix.Length), out int i));
                    Debug.Assert(genTypes.Length == i + 1);
                    propName = EnhancedGridView_BrowsableNestedProperties.GetSmartTypeName(genTypes.Last());
                }
                else
                    propName = BusinessNameAttribute.Get(Property)?.Name
                            ?? Property.Name.Uncamelify();
                return propName + (Property.IsTechnicalProperty() ? " <Technical>" : "");
            }
        }
    }

    abstract class CustomBrowsingColumnInfo : BrowsingColumnInfo
    {
        public CustomBrowsingColumnInfo(BrowsingColumnInfo source, GridColumn originColumn, string uniqueFieldNameTarget, string browsingPathTarget)
            : base(source, originColumn)
        {
            _uniqueFieldNameTarget = uniqueFieldNameTarget;
            _browsingPathTarget = browsingPathTarget;
        }
        protected CustomBrowsingColumnInfo()
        {
        }
        public override string UniqueFieldNameTarget { get { return _uniqueFieldNameTarget;              } } string _uniqueFieldNameTarget;
        public override string BrowsingPathTarget    { get { return _uniqueFieldNameTarget.Uncamelify(); } } string _browsingPathTarget;

        public override XElement SaveToXml(XElement node, Func<BrowsingColumnInfo, int> refToId)
        {
            base.SaveToXml(node, refToId);
            node.Add(new XAttribute("UniqueFieldNameTarget", _uniqueFieldNameTarget));
            node.Add(new XAttribute("BrowsingPathTarget", _browsingPathTarget));
            return node;
        }
        public override void RestoreFromToXml(XElement node, GridView gv, Func<int, BrowsingColumnInfo> idToRef)
        {
            base.RestoreFromToXml(node, gv, idToRef);
            _uniqueFieldNameTarget = node.Attribute("UniqueFieldNameTarget")?.Value
                                  ?? throw new TechnicalException("Attribute \"UniqueFieldNameTarget\" not found!", null);
            _browsingPathTarget = node.Attribute("BrowsingPathTarget")?.Value
                               ?? throw new TechnicalException("Attribute \"UniqueFieldNameTarget\" not found!", null);
        }
    }
    class StringAnalysisColumnInfoPattern : CustomBrowsingColumnInfo
    {
        public StringAnalysisColumnInfoPattern(BrowsingColumnInfo source, GridColumn originColumn)
            : base(source, originColumn, "Pattern", "Pattern")
        {
        }
        protected internal StringAnalysisColumnInfoPattern()
        {
        }
        internal readonly StringPatternFinder<string> PatternFinder = new StringPatternFinder<string>(str => str);

        public override   Type GetValueType() { return typeof(string); }
        public override object GetValue(object dsItem, int dsItemIndex)
        {
            var obj = base.GetValue(dsItem, dsItemIndex);
            if (obj == null)
                return null;
            return PatternFinder.GetPattern((string)obj).Pattern;
        }
    }
    class StringAnalysisColumnInfoPatternId : CustomBrowsingColumnInfo
    {
        public StringAnalysisColumnInfoPatternId(BrowsingColumnInfo source, GridColumn originColumn, StringAnalysisColumnInfoPattern sibling)
            : base(source, originColumn, "PatternId", "Pattern Id")
        {
            _sibling = sibling;
            Dependencies = new List<BrowsingColumnInfo>() { _sibling };
        }
        protected internal StringAnalysisColumnInfoPatternId()
        {
            Dependencies = new List<BrowsingColumnInfo>();
        }
        StringAnalysisColumnInfoPattern _sibling;

        public override XElement SaveToXml(XElement node, Func<BrowsingColumnInfo, int> refToId)
        {
            base.SaveToXml(node, refToId);
            node.Add(new XAttribute("Sibling", refToId(_sibling)));
            return node;
        }
        public override void RestoreFromToXml(XElement node, GridView gv, Func<int, BrowsingColumnInfo> idToRef)
        {
            base.RestoreFromToXml(node, gv, idToRef);
            var aSibling = node.Attribute("Sibling")?.Value;
            if (aSibling == null || !int.TryParse(aSibling, out int id))
                throw new TechnicalException("Attribute \"Sibling\" not found or invalid!", null);
            _sibling = (StringAnalysisColumnInfoPattern)idToRef(id);
            Dependencies.Clear();
            Dependencies.Add(_sibling);
        }

        public override Type GetValueType() { return typeof(int); }
        public override object GetValue(object dsItem, int dsItemIndex)
        {
            var obj = base.GetValue(dsItem, dsItemIndex);
            if (obj == null)
                return null;
            return _sibling.PatternFinder.GetPattern((string)obj).Id;
        }
    }

    // Special class make to handle XML issue when restoring layout
    sealed class BrokenBrowsingColumnInfo : CustomBrowsingColumnInfo
    {
        public BrokenBrowsingColumnInfo(XElement badFormattedOrIncompleteNode, Exception ex)
        {
            _badFormattedOrIncompleteNode = badFormattedOrIncompleteNode;
            _ex = ex;
        }
        readonly XElement _badFormattedOrIncompleteNode;
        readonly Exception _ex;

        public override Type GetValueType()
        {
            // because string is the most versatile type,
            // so even if broken it has chance to adapt with a lot of other thing
            return typeof(string);
        }
        public override object GetValue(object dsItem, int dsItemIndex)
        {
            return null;
        }
        public override XElement SaveToXml(XElement node, Func<BrowsingColumnInfo, int> refToId)
        {
            node.Add(_badFormattedOrIncompleteNode.Elements());
            return node;
        }
    }
}
