﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;

using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Drawing;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_DisplayColumnHint
    {
        readonly GridView _view;

        public EnhancedGridView_DisplayColumnHint(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.CustomDrawColumnHeader -= view_CustomDrawColumnHeader; view.CustomDrawColumnHeader += view_CustomDrawColumnHeader;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.CustomDrawColumnHeader -= view_CustomDrawColumnHeader;
        }

        internal void Assign(EnhancedGridView_DisplayColumnHint f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
        }
        public static readonly Image InfoSignPng16X16 = Image.FromStream(typeof(EnhancedGridView_DisplayColumnHint).Assembly.GetManifestResourceStream(typeof(EnhancedGridView_DisplayColumnHint).Namespace + ".Resources.Images.info.png"));


        void view_CustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            //here we are trying to change the color of the column header if there is a filter applied
            if (e.Column == null)
                return;



            // from https://www.devexpress.com/Support/Center/Question/Details/T403870/how-can-i-show-gridview-filter-button-at-column-header-manually
            // Show always filter button
            GridColumnInfoArgs ci = e.Info;
            DrawElementInfo di = ci.InnerElements.OfType<DrawElementInfo>().FirstOrDefault(x => x.ElementInfo is GridFilterButtonInfoArgs);
            if (di != null)
            {
                ((GridFilterButtonInfoArgs)di.ElementInfo).Filtered = true;
                di.Visible = true;
                e.Painter.CalcObjectBounds(e.Info);
            }

            // Now we draw the "?" if there is a hint
            if (e.Column.ToolTip.IsNullOrWhiteSpace())
            {
                e.DefaultDraw();
                return;
            }
            // Code a nettoyer
            //Pour tester en cas de multiligne
            //e.Column.Caption = "A" + Environment.NewLine + "B" + Environment.NewLine + "B" + Environment.NewLine + "B" + Environment.NewLine + "B" + Environment.NewLine + "B" + Environment.NewLine + "B";
            //_view.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.Default;


            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            // get the rect in which we want to draw the icon
            int rightDistance = 18 + 4; // Valeur intuitive : 18 (+ 4 sinon ca sort du cadre sur la droite (ce qui n'est pas logique mais pas le temps de chercher!
            int topBottomMargin = 1;
            int radius = e.Info.Bounds.Height / 2 - 2 * topBottomMargin;
            var loc = new Point(e.Info.Bounds.Right - radius - rightDistance,
                e.Info.Bounds.Top + Math.Max(topBottomMargin + radius, e.Info.Bounds.Height / 2 - 1));
            var rect = new Rectangle(loc.X - radius, loc.Y - radius, radius + radius, radius + radius);

            // Do the default draw but move the rectangle in which the caption is drawn before doing it
            // So that when text is center, the icon does not overlap text
            var r = e.Info.CaptionRect;
            r.Width -= rect.Width;
            e.Info.CaptionRect = r;
            e.DefaultDraw();
            // Important to restore because when mouse move over caption , text blink is shrinked etc
            // Actually DX does not alwaus refresh CaptionRect before calling CustomDrawColumnHeader again
            r.Width += rect.Width;
            e.Info.CaptionRect = r;

            e.Graphics.DrawImage(InfoSignPng16X16, new Point(loc.X, e.Info.Bounds.Top + 2));

            //e.Graphics.FillEllipse(Brushes.WhiteSmoke, rect);
            //e.Graphics.DrawEllipse(Pens.WhiteSmoke, rect);
            ////DrawSphere(e.Graphics, rect, Color.White);



            ////rect.Offset(2, rect.Height / 4); // adapt so the text seems centered in the sphere
            //rect.Offset(rect.Width / 6, -rect.Height / 10); // adapt so the text seems centered in the sphere
            //string question = "❓"; // U+2753 (not U+2754)
            //question = "i"; // U+2753 (not U+2754)
            //var font = GetAdjustedFont(e.Graphics, question, SystemFonts.DefaultFont, rect) ?? SystemFonts.DefaultFont;
            //var testFont = new Font(font.Name, font.Size+3, font.Style);

            //e.Graphics.DrawString(question, testFont, Brushes.Blue, rect);

            e.Handled = true;
        }

        internal int OnCalcColumnBestWidth(Func<GridColumn, int> defaultCalcMethod, GridColumn column)
        {
            return defaultCalcMethod(column) + 20 + 3; // 12 est la taille du rect ci dessus + 3 separator
        }

        // Inspired from https://social.msdn.microsoft.com/Forums/vstudio/en-US/811c3452-1f97-452f-8af4-22219b095dd7/drawing-3d-shapes-in-2d?forum=vbgeneral
        //void DrawSphere(Graphics g, Rectangle rect, Color c)
        //{
        //    var lightlyLigther = c;//Color.White));
        //    var lightlyDarker = c.InterpolateTo(Color.FromArgb(c.A, Color.Black), (10d) / 245);
        //    RectangleF rectf = rect;
        //    GraphicsPath path = new GraphicsPath();
        //    path.AddEllipse(rectf);
        //    //rectf.X += r / 50;
        //    //rectf.Y += r / 50;
        //    g.DrawEllipse(new Pen(c, rect.Width / 16f), rectf);
        //    using (PathGradientBrush pthGrBrush = new PathGradientBrush(path))
        //    {
        //        pthGrBrush.CenterPoint = new PointF(rect.Right, rect.Bottom);
        //        pthGrBrush.CenterColor = lightlyDarker;
        //        Color[] colors = { c };
        //        pthGrBrush.SurroundColors = colors;
        //        g.FillPath(pthGrBrush, path);
        //        pthGrBrush.CenterColor = lightlyLigther;
        //        rectf.Width *= 0.2F;
        //        rectf.Height *= 0.2F;
        //        rectf.Offset(rect.Width / 3f, rect.Height / 2f);
        //        g.FillEllipse(pthGrBrush, rectf);
        //    }
        //}

        //public Font GetAdjustedFont(Graphics graphicRef, string graphicString, Font originalFont, Rectangle rect)
        //{
        //    // We utilize MeasureString which we get via a control instance
        //    for (int adjustedSize = 72; adjustedSize >= 4; adjustedSize--)
        //    {
        //        var testFont = new Font(originalFont.Name, adjustedSize, originalFont.Style);

        //        // Test the string with the new size
        //        SizeF adjustedSizeNew = graphicRef.MeasureString(graphicString, testFont);

        //        // Good font ?
        //        if (rect.Width > Convert.ToInt32(adjustedSizeNew.Width))
        //            return testFont;
        //        //if (rect.Height> Convert.ToInt32(adjustedSizeNew.Height))
        //        //    return testFont;
        //    }

        //    return null;
        //}
    }
}
