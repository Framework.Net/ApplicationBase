﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;

using DevExpress.Data;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPivotGrid;
using TechnicalTools.Logs;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_TransformData
    {
        readonly GridView _view;

        public EnhancedGridView_TransformData(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(EnhancedGridView_TransformData));

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing;  view.PopupMenuShowing += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_TransformData f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }

        static void View_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            var view = sender as GridView;
            var mnuTransform = new DXSubMenuItem(ExportMenuCaption);

            var currentDs = view.GridControl.DataSource;
            var itemType = currentDs?.GetType()
                                     .GetOneImplementationOf(typeof(IEnumerable<>))
                                     ?.GetGenericArguments()
                                     .First();
            mnuTransform.Items.Add(new DXMenuItem("Analyse all data using a pivot grid", (_, __) => AnalyseDataAndOpenInPivotGridForm(view, (currentDs as IEnumerable).Cast<object>().ToTypedList(itemType)))
            {
                Enabled = view.RowCount > 0 && itemType != null
            });
            mnuTransform.Items.Add(new DXMenuItem("Extract most inner group rows to a new Grid", (_, __) => ExtractGroupRowsAndShowInForm(view))
            {
                Enabled = view.RowCount > 0 && view.GroupCount > 0 // give the level of grouping, not the number of group row
            });

            e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuTransform);
        }
        public static string ExportMenuCaption { get; } = "Transform...";
        public static bool GroupExtractedGroupValues { get; set; }

        public static EnhancedXtraForm AnalyseDataAndOpenInPivotGridForm(GridView view, IList ds)
        {
            List<ExpandoObject> newDatasource = CreateNewDataSource(view, out Hashtable firstSummaryValues);
            var owner = view.GridControl.FindForm() ?? EnhancedXtraForm.MainForm;
            var pgc = new PivotGridControl();
            pgc.DataSource = ds;

            var getterByField = new Dictionary<string, Func<object, int, object>>();
            foreach (GridColumn col in view.Columns)
            {
                var ecol = col as IEnhancedGridColumn;
                var pgf = new PivotGridField()
                {
                    FieldName = col.FieldName,
                    UnboundType = col.UnboundType,
                    UnboundExpression = col.UnboundExpression,
                    Caption = col.GetCaption()
                };
                if (col.UnboundType != UnboundColumnType.Bound)
                    if (ecol == null)
                        continue; // don't know how to handle it
                    else if (col.UnboundType == UnboundColumnType.Object) // Returning object make pivot grid incredibly slow, don't know why
                        getterByField.Add(pgf.FieldName, (item, listSourceRowIndex) => ecol.UnboundValueGetter(item, listSourceRowIndex)?.ToString());
                    else
                        getterByField.Add(pgf.FieldName, (item, listSourceRowIndex) => ecol.UnboundValueGetter(item, listSourceRowIndex));
                pgc.Fields.Add(pgf);
            }
            pgc.CustomUnboundFieldData += (_, e) =>
            {
                if (getterByField.TryGetValue(e.Field.FieldName, out Func<object, int, object> getter))
                    e.Value = getter(ds[e.ListSourceRowIndex], e.ListSourceRowIndex).ToString();
            };

            var pgce = PivotGridControlEnhancement.InstallOn(pgc);
            pgc.Size = new System.Drawing.Size(owner.Size.Width * 80 / 100, owner.Size.Height * 80 / 100);
            var frm = EnhancedXtraForm.CreateWithInner(pgc, owner);
            frm.Text = "Data analysis";
            frm.Show(owner);

            return frm;
        }

        public static Tuple<List<ExpandoObject>, GenericGridViewerForm> ExtractGroupRowsAndShowInForm(GridView view)
        {
            List<ExpandoObject> newDatasource = CreateNewDataSource(view, out Hashtable firstSummaryValues);
            var owner = view.GridControl.FindForm() ?? EnhancedXtraForm.MainForm;
            var frm = new GenericGridViewerForm(newDatasource);
            frm.Load += (_, __) =>
            {
                var outerFroupsCols = view.GroupedColumns.ToList();
                foreach (var col in outerFroupsCols)
                {
                    var frmCol = frm.View.Columns[col.FieldName];
                    col.VisibleIndex = outerFroupsCols.IndexOf(col) + 1;
                    frmCol.Caption = col.GetTextCaption();
                    if (GroupExtractedGroupValues)
                        try
                        {
                            frmCol.GroupIndex = col.GroupIndex;
                        }
                        catch (InvalidCastException)
                        {
                            // if the grouped column is of any abstract type (in our case "object")
                            // and there is two lines in datasource where the value for this column are different dynamic type
                            // Devexpress is lost and generate an exception. We ignore this exception currently
                            // TODO : To prevent the exception, we could try to get by reflection the real type of the column in original datasource
                            // Maybe it is an interface
                            // Another way is to create column ourselves ?
                        }
                    frmCol.SortIndex = col.SortIndex;
                    frmCol.SortMode = col.SortMode;
                    frmCol.SortOrder = col.SortOrder;
                }

                if (firstSummaryValues != null)
                {
                    // Les index doivent etre restorée dans le bon ordre sinon ca marche mal
                    var orderedSv = firstSummaryValues.Keys.Cast<GridGroupSummaryItem>().Select(sv =>
                    {
                        var oldCol = view.Columns[sv.FieldName];
                        return new { sv, oldCol, VisibleIndex = oldCol.Visible ? oldCol.VisibleIndex : -1 };
                    }).OrderBy(v => v.VisibleIndex)
                      .ToList();
                    foreach (var v in orderedSv)
                    {
                        var newCol = frm.View.Columns[v.oldCol.FieldName];
                        if (v.sv.SummaryType == SummaryItemType.Count)
                        {
                            var enumerator = (view.DataSource as IEnumerable).GetEnumerator();
                            string itemName = null;
                            while (enumerator.MoveNext())
                                if (enumerator.Current != null)
                                {
                                    var t = enumerator.Current.GetType();
                                    itemName = Annotations.BusinessNameAttribute.GetNameFor(t).IfBlankUse(t.Name.Uncamelify());
                                    break;
                                }
                            newCol.Caption = "Count" + " of ".AsPrefixForIfNotBlank(itemName);
                        }
                        else if (v.sv.SummaryType.In(SummaryItemType.Min, SummaryItemType.Max, SummaryItemType.Average, SummaryItemType.Sum))
                            newCol.Caption = v.sv.SummaryType + " of " + v.oldCol.GetTextCaption();
                        newCol.DisplayFormat.FormatString = v.sv.DisplayFormat;
                        newCol.Visible = v.VisibleIndex >= 0;
                        if (newCol.Visible)
                            newCol.VisibleIndex = v.VisibleIndex + outerFroupsCols.Count + 1;
                    }
                }
            };
            frm.Show();
            return Tuple.Create(newDatasource, frm);
        }

        public static List<ExpandoObject> CreateNewDataSource(GridView view, out Hashtable firstSummaryValues)
        {
            var newDatasource = new List<ExpandoObject>();
            var outerValues = new Dictionary<int, object>();
            firstSummaryValues = null;
            for (int grh = -1; view.IsValidRowHandle(grh); grh--)
            {
                var level = view.GetRowLevel(grh);
                var groupValue = view.GetGroupRowValue(grh);
                outerValues[level] = groupValue;
                if (level == view.GroupedColumns.Count - 1) // ... which are the inner group row of the grid (in case of multiple group level )...
                {
                    var eo = new ExpandoObject() as IDictionary<string, object>; // ...we transform this group row to a line of a new datasource

                    for (int ogrhLevel = 0; ogrhLevel < level; ++ogrhLevel) // ... with all value of outer group row
                        eo.Add(view.GroupedColumns[ogrhLevel].FieldName, outerValues[ogrhLevel]);
                    // Then we add the current group row value and caption
                    var groupCaption = view.GroupedColumns[view.GetRowLevel(grh)].FieldName;
                    eo.Add(groupCaption, groupValue);

                    // Then, maybe, the summary values
                    var summaryValues = view.GetGroupSummaryValues(grh);
                    firstSummaryValues = firstSummaryValues ?? summaryValues;
                    if (summaryValues != null)
                        foreach (GridGroupSummaryItem sv in summaryValues.Keys)
                            eo.Add(view.Columns[sv.FieldName].FieldName, summaryValues[sv]);
                    newDatasource.Add(eo as ExpandoObject);
                }
            }
            return newDatasource;
        }
   }
}
