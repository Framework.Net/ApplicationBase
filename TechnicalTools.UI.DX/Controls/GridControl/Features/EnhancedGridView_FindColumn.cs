﻿using System;
using System.Diagnostics;

using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.BandedGrid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_FindColumn
    {
        readonly IEnhancedGridView _view;

        public EnhancedGridView_FindColumn(IEnhancedGridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(IEnhancedGridView view)
        {
            __Install((GridView)view);
            view.GridControlChanged -= View_GridControlChanged; view.GridControlChanged += View_GridControlChanged;
        }
        void __Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing; view.PopupMenuShowing += View_PopupMenuShowing;
        }

        void View_GridControlChanged(IEnhancedGridView view, GridControl prevControl)
        {
            if (prevControl != null)
                prevControl.PreviewKeyDown -= GridControl_PreviewKeyDown;
            if ((view as GridView).GridControl != null)
                (view as GridView).GridControl.PreviewKeyDown += GridControl_PreviewKeyDown;
        }



        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(IEnhancedGridView view)
        {
            __Uninstall(view as GridView);
            view.GridControlChanged -= View_GridControlChanged;
        }
        void __Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
            if (view.GridControl != null)
                view.GridControl.PreviewKeyDown += GridControl_PreviewKeyDown;
        }

        internal void Assign(EnhancedGridView_FindColumn f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }


        void View_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            var view = sender as GridView;
            if (e.HitInfo is GridHitInfo hitInfo)
            {
                if (hitInfo.InColumn || hitInfo.InColumnPanel || hitInfo.InGroupColumn || hitInfo.InGroupPanel ||
                    e.HitInfo is BandedGridHitInfo bHitInfo && bHitInfo.InBandPanel)
                {
                    var mnuFindColumn = new DXMenuItem("Find Column...");
                    mnuFindColumn.Click += (_, __) =>
                    {
                        ShowFindColumnForm(view);
                    };
                    e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuFindColumn);
                }
            }
        }
        private void GridControl_PreviewKeyDown(object sender, System.Windows.Forms.PreviewKeyDownEventArgs e)
        {
            if (e.Alt && e.KeyCode == System.Windows.Forms.Keys.C)
            {
                var view = (sender as GridControl).MainView;
                var findColumn = (view as EnhancedGridView)?._FindColumn
                              ?? (view as EnhancedBandedGridView)?._FindColumn;
                if (findColumn != null)
                    findColumn.ShowFindColumnForm((GridView)view);
            }

        }
        void ShowFindColumnForm(GridView view)
        {
            var ctl = new EnhancedGridView_FindColumn_Form(view);
            ctl.ShowInForm(view.GridControl.FindForm());
        }
    }
}
