﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using DevExpress.Utils.Menu;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

using TechnicalTools;
using TechnicalTools.Diagnostics;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_MultipleCellEditor
    {
        readonly GridView _view;

        public EnhancedGridView_MultipleCellEditor(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing; view.PopupMenuShowing += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_MultipleCellEditor f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }

        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if (e.HitInfo.Column == null) // click on indicator
                return;

            var view = sender as GridView;
            //Debug.Assert(view == _view); // faux dans le cas de sous vues
            var viewInfo = view.GetViewInfo() as GridViewInfo;
            Debug.Assert(viewInfo != null);

            if (!(view is IEnhancedGridView_AccesForViewFeature))
                return;


            GridCell[] selectedCells = view.GetSelectedCells().Where(cell => !view.IsGroupRow(cell.RowHandle)).ToArray();
            if (!selectedCells.Skip(1).Any())
                return; // Only one cell ? No need for this feature (and allow user to display a menu without this feature... just in case of ...)

            var mnuChange = new DXSubMenuItem($"Change column values for this {selectedCells.Length} rows to...")
            {
                Enabled = false
            };
            // Because there is multiple cells selected, we want to let user to know this feature exist.
            // It is later that it will be enable (if it make sense)
            e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuChange);

            if (selectedCells.GroupBy(cell => cell.Column).Skip(1).Any())
                return; // different columns are concerned ? We do not provide the feature as it would be unclear.

            Dictionary<GridCell, RepositoryItem> allCellEditorRepos = null;
            view.BeginUpdate(); // Make getRepoItem 1000 times faster
            try
            {
                allCellEditorRepos = selectedCells.ToDictionary(cell => cell, cell => getRepoItem(viewInfo, cell));
            }
            finally
            {
                view.EndUpdate();
            }
            foreach (var selectedCell in selectedCells)
                if (allCellEditorRepos[selectedCell] == _dummy)
                    allCellEditorRepos.Remove(selectedCell);
            selectedCells = allCellEditorRepos.Keys.ToArray();
            var allCellEditorTypes = allCellEditorRepos.Select(kvp => kvp.Value.GetType()).Distinct().ToList();

            // If user has selected cells with _ALL_ the same control to edit content, we provide a feature to change all the values in one action
            if (!allCellEditorTypes.Any() || allCellEditorTypes.Skip(1).Any())
                return; // no cell selected or too many cell with different type of editor => too complex... we quit

            if (typeof(RepositoryItemLookUpEdit).IsAssignableFrom(allCellEditorTypes.First()) && // All control are the same kind
                !allCellEditorRepos.Values.Select(repo => (repo as RepositoryItemLookUpEdit).DataSource?.GetType()).Distinct().Skip(1).Any() && // Only one kind of datasource
                (allCellEditorRepos.First().Value as RepositoryItemLookUpEdit).DataSource is IEnumerable<object>) // and datasource are enumerable
            {
                mnuChange.Items.Add(new DXMenuHeaderItem() { Caption = "The common allowed values for selected cells are:" });
                try
                {
                    // ok, now, we can propose to user a way to change all values in once action
                    // but all editor (or repositoryItem) may have different allowed values
                    // we want to find an intersection of value that would be allowed for all cells selected (i will handle more complex case later)
                    var distinctDataSources = selectedCells.Select(cell => (IEnumerable<object>)(allCellEditorRepos[cell] as RepositoryItemLookUpEdit).DataSource)
                                                           .Distinct() // usefull if repositories share the same datasource object
                                                           .ToList();
                    var commonChoices = distinctDataSources.SelectMany(ds => ds)
                                                           .GroupBy(item => item) // Let's consider items are value type (or object type that implement Equatable / Equals in the right way)
                                                           .Where(grp => grp.Count() == distinctDataSources.Count)
                                                           .Select(grp => grp.Key)
                                                           .ToList();
                    if (commonChoices.Count == 0)
                        mnuChange.Items.Add(new DXMenuItem("Sorry... no value are common for all selected cells") { Enabled = false });
                    else
                    {
                        foreach (var commonChoice in commonChoices) // TODO : Handle the case where there is a looot of choices in commonChoices => open a dialog with another lookupedit ?
                        {
                            var asLuItem = commonChoice as LookUpEdit_ExtensionsForObjects.Item;
                            var value = asLuItem == null ? commonChoice
                                      : asLuItem.IsNullItem() ? null
                                      : asLuItem.GetObjectType().IsEnum || asLuItem.GetObjectType() == typeof(Enum) ? asLuItem.GetObject()
                                      : asLuItem.GetId();
                            var caption = asLuItem != null ? asLuItem.Caption
                                      : value == null ? "[EMPTY]" // Empty is for handling AllowNull later ...
                                      : value is Enum ? (value as Enum).GetDescription()
                                      : value.ToString();
                            mnuChange.Items.Add(new DXMenuItem(caption, (_, __) =>
                                view.GridControl.ShowBusyWhileDoingUIWorkInPlace(pr =>
                                    AssignValueToCells(selectedCells, view, value, pr))));
                        }
                    }
                }
                catch (Exception ex)
                {
                    mnuChange.Items.Add(new DXMenuItem("*** Sorry, an error occur building this menu ***"));
                    mnuChange.Items.Add(new DXMenuItem("Error: " + ex.Message) { BeginGroup = true });
                }
                mnuChange.Enabled = true;
            }
            // Maybe later type of control...
        }

        void AssignValueToCells(GridCell[] selectedCells, GridView view, object value, IProgress<string> pr)
        {
            view.InvalidValueException += View_InvalidValueException;
            try
            {
                int i = 0;
                var fieldName = selectedCells.FirstOrDefault()?.Column.FieldName;
                var objectsWithErrorToRemove = new List<Tuple<IDataErrorInfoWriteable, string>>();
                // we change of corrdinate system : go to business object instead of row
                // because when cell/row object are treated they may disappear from grid
                // so at next iteration cell iterator may skip the real next one that take the place of the old current one.
                var selectedObjects = selectedCells.Select(cell => view.GetRow(cell.RowHandle)).ToList();
                Debug.Assert(selectedObjects.Select(view.FindRow).SequenceEqual(selectedCells.Select(cell => cell.RowHandle)));
                var colToEdit = selectedCells.First().Column;
                Debug.Assert(selectedCells.All(cell => cell.Column == colToEdit));

                view.BeginUpdate();
                // Needed because if we update a cell that is filtered out using CustomRowFilter,
                // the index system of gridview (rowhandle) is updated when we set focusedrowhandle, causing bugs
                // don't know about performance of this or side effect of BeginDataUpdate though
                // So if we need to remove it later we have to uncomment the two lines below
                view.BeginDataUpdate();
                foreach (var obj in selectedObjects)
                {   // methodology from https://www.devexpress.com/Support/Center/Question/Details/T167042/can-you-set-a-grid-cell-s-errortext-when-validating-all-rows-in-the-grid
                    pr.Report("Changing cell value " + ++i + " on " + selectedCells.Length);
                    // Refind the rowhandle that may haved moved
                    var newRowHandle = view.FindRow(obj);
                    view.FocusedRowHandle = newRowHandle;
                    view.FocusedColumn = colToEdit;
                    // if we do not want to use {Begin/End}DataUpdate, we have to uncomment this two lines
                    //newRowHandle = view.FindRow(obj) // find again the rowhandle because setting FocusedColumn change internal mapping between object and rowhandle
                    //view.FocusedColumn = colToEdit // set again (should be good this time)
                    view.ShowEditor();

                    var beforeValue = view.GetRowCellValue(newRowHandle, colToEdit);
                    try
                    {
                        view.SetRowCellValue(newRowHandle, colToEdit, value);
                    }
                    catch (Exception ex)
                    {
                        var msgError = $"Cannot perform change from {beforeValue} to {value} because: " + Environment.NewLine +
                                        ExceptionManager.Instance.Format(ex, true, true);
                        if (obj is IDataErrorInfoWriteable errorManager)
                        {
                            objectsWithErrorToRemove.Add(Tuple.Create(errorManager, msgError));
                            errorManager[fieldName] = msgError;
                        }
                        else
                        {
                            view.ActiveEditor.ErrorText = "Cannot perform the change because: " + Environment.NewLine +
                                                            ExceptionManager.Instance.Format(ex, true, true);
                            // Disabled because cause the setter of object to be called again and again until it works
                            view.ActiveEditor.IsModified = beforeValue != view.GetRowCellValue(newRowHandle, colToEdit);
                        }
                        view.CloseEditor();
                    }
                }
                view.EndDataUpdate();
                view.EndUpdate();

                // As soon as user change the focused row, we remove all error
                if (objectsWithErrorToRemove.Count > 0)
                {
                    void h(object ___, FocusedRowChangedEventArgs ____)
                    {
                        view.FocusedRowChanged -= h;
                        foreach (var t in objectsWithErrorToRemove)
                            if (t.Item1[fieldName] == t.Item2)
                                t.Item1[fieldName] = null;
                        objectsWithErrorToRemove.Clear();
                        view.LayoutChanged();
                    }
                    view.FocusedRowChanged += h;
                }
            }
            finally
            {
                view.InvalidValueException -= View_InvalidValueException;
            }
        }

        private void View_InvalidValueException(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.ThrowException;
        }

        static RepositoryItem getRepoItem(GridViewInfo viewInfo, GridCell cell)
        { // code from https://www.devexpress.com/Support/Center/Question/Details/Q265379
            var cellInfo = viewInfo.GetGridCellInfo(cell.RowHandle, cell.Column);
            if (cellInfo == null)
            {
                if (!(viewInfo.RowsInfo.GetInfoByHandle(cell.RowHandle) is GridDataRowInfo rowInfo))
                    rowInfo = viewInfo.CreateRowInfo(new GridRow(cell.RowHandle, 0, 0, 0, null, false)) as GridDataRowInfo;
                var colInfo = viewInfo.ColumnsInfo[cell.Column];
                if (colInfo == null) // Why it's null sometimes ?!
                    return _dummy; // === we don't know
                cellInfo = viewInfo.CreateCellInfo(rowInfo, colInfo);
                Debug.Assert(cellInfo != null);
            }
            var viewWithFeature = (IEnhancedGridView_AccesForViewFeature)viewInfo.View;
            return viewWithFeature.GetCellEditor(cellInfo);
        }
        static readonly RepositoryItem _dummy = new RepositoryItem();
    }
}
