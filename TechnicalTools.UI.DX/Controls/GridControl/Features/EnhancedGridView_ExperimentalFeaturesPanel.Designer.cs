﻿namespace TechnicalTools.UI.DX
{
    partial class EnhancedGridView_ExperimentalFeaturesPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnClose_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceLeftCloseButton = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceLeftCloseButton)).BeginInit();
            this.SuspendLayout();
            //
            // chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor
            //
            this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor.Location = new System.Drawing.Point(12, 12);
            this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor.Name = "chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor";
            this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor.Properties.Caption = "Allow compare columns value with other column value in filter editor";
            this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor.Size = new System.Drawing.Size(382, 19);
            this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor.StyleController = this.layoutControl1;
            this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor.TabIndex = 0;
            this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor.CheckedChanged += new System.EventHandler(this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor_CheckedChanged);
            //
            // layoutControl1
            //
            this.layoutControl1.Controls.Add(this.btnClose);
            this.layoutControl1.Controls.Add(this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(923, 244, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(406, 219);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            //
            // btnClose
            //
            this.btnClose.Location = new System.Drawing.Point(304, 185);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(90, 22);
            this.btnClose.StyleController = this.layoutControl1;
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            //
            // layoutControlGroup1
            //
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.btnClose_LayoutItem,
            this.emptySpaceItem1,
            this.emptySpaceLeftCloseButton});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(406, 219);
            this.layoutControlGroup1.TextVisible = false;
            //
            // layoutControlItem1
            //
            this.layoutControlItem1.Control = this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(386, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            //
            // btnClose_LayoutItem
            //
            this.btnClose_LayoutItem.Control = this.btnClose;
            this.btnClose_LayoutItem.Location = new System.Drawing.Point(292, 173);
            this.btnClose_LayoutItem.MaxSize = new System.Drawing.Size(94, 26);
            this.btnClose_LayoutItem.MinSize = new System.Drawing.Size(94, 26);
            this.btnClose_LayoutItem.Name = "btnClose_LayoutItem";
            this.btnClose_LayoutItem.Size = new System.Drawing.Size(94, 26);
            this.btnClose_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.btnClose_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.btnClose_LayoutItem.TextVisible = false;
            //
            // emptySpaceItem1
            //
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 23);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(386, 150);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            // emptySpaceLeftCloseButton
            //
            this.emptySpaceLeftCloseButton.AllowHotTrack = false;
            this.emptySpaceLeftCloseButton.Location = new System.Drawing.Point(0, 173);
            this.emptySpaceLeftCloseButton.Name = "emptySpaceLeftCloseButton";
            this.emptySpaceLeftCloseButton.Size = new System.Drawing.Size(292, 26);
            this.emptySpaceLeftCloseButton.TextSize = new System.Drawing.Size(0, 0);
            //
            // EnhancedGridView_ExperimentalFeaturesPanel
            //
            this.Controls.Add(this.layoutControl1);
            this.Name = "EnhancedGridView_ExperimentalFeaturesPanel";
            this.Size = new System.Drawing.Size(406, 219);
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceLeftCloseButton)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem btnClose_LayoutItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.CheckEdit chkAllowCompareColumnsValueWithOtherColumnValueInFilterEditor;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceLeftCloseButton;
    }
}
