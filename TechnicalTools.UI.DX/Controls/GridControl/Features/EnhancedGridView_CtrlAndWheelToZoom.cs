﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    // Permet de seelctionner l'integralité du contenu d'une cellule si on triple-click dedans
    // Marche aussi pour les treelists normalement
    // Inspired from https://www.devexpress.com/Support/Center/Question/Details/Q569292
    public class EnhancedGridView_CtrlAndWheelToZoom
    {
        readonly GridView _view;

        public EnhancedGridView_CtrlAndWheelToZoom(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.MouseWheel -= View_MouseWheel; view.MouseWheel += View_MouseWheel;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.MouseWheel -= View_MouseWheel;
        }

        internal void Assign(EnhancedGridView_CtrlAndWheelToZoom f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }


        private void View_MouseWheel(object sender, MouseEventArgs e)
        {
            if (Control.ModifierKeys.HasFlag(Keys.Control))
            {
                var view = sender as GridView;
                view.Appearance.Row.FontSizeDelta = Math.Max(-7, view.Appearance.Row.FontSizeDelta + e.Delta / 100);
                ((DevExpress.Utils.DXMouseEventArgs)e).Handled = true;
            }
        }
    }
}
