﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;

using DevExpress.Data;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_SmartSort
    {
        readonly GridView _view;

        public EnhancedGridView_SmartSort(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing; view.PopupMenuShowing += View_PopupMenuShowing;
            view.CustomColumnSort -= View_CustomColumnSort; view.CustomColumnSort += View_CustomColumnSort;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
            view.CustomColumnSort -= View_CustomColumnSort;
        }

        internal void Assign(EnhancedGridView_SmartSort f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code
        }


        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            var view = sender as GridView;
            var col = e.HitInfo.Column;
            if (e.HitInfo.InColumn && col.ColumnType == typeof(string))
            {
                var mnu = new DXMenuCheckItem("Smart sorting (handle number in text)", _sortDirections.TryGetValue(col, out ColumnInfo ci) && ci.Enabled);
                e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnu);
                mnu.Click += (_, __) =>
                {
                    ci = _sortDirections.GetOrCreateValue(col);
                    ci.Enabled = !ci.Enabled;
                    col.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
                    if (col.SortOrder == ColumnSortOrder.None)
                        col.SortOrder = ColumnSortOrder.Ascending;
                    else
                    {
                        var tmp = col.SortOrder;
                        col.SortOrder = ColumnSortOrder.None;
                        col.SortOrder = tmp;
                    }
                };
            }
        }

        private void View_CustomColumnSort(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnSortEventArgs e)
        {
           if (e.Column.SortOrder != ColumnSortOrder.None &&
               e.Column.SortMode == DevExpress.XtraGrid.ColumnSortMode.Custom &&
               _sortDirections.TryGetValue(e.Column, out ColumnInfo ci) && ci.Enabled)
            {
                e.Handled = true;
                SplitInComparableParts((string)e.Value1, _a);
                SplitInComparableParts((string)e.Value2, _b);
                int i = 0;
                while (i < _a.Length)
                {
                    e.Result = CompareStringOrDecimal(_a[i], _b[i]);
                    if (e.Result != 0 || _a[i] == null)
                    {
                        if (e.SortOrder == ColumnSortOrder.Descending)
                            e.Result = -e.Result;
                        return;
                    }
                    ++i;
                }
                e.Result = 0;
            }
        }
        readonly IComparable[] _a = new IComparable[101];
        readonly IComparable[] _b = new IComparable[101];
        static int CompareStringOrDecimal(IComparable a, IComparable b)
        {
            if (a == null)
                if (b == null)
                    return 0;
                else
                    return -1;
            if (a is string && b is decimal)
                return 1;
            if (a is decimal)
                if (b == null)
                    return 1;
                else if (b is string)
                    return -1;
            return a.CompareTo(b);
        }
        void SplitInComparableParts(string str, IComparable[] a = null)
        {
            a = a ?? new IComparable[101];
            bool partHasDigit = false;
            bool partHasLetter = false;
            int bpi = 0; // building part index
            int oldi = 0;
            for (int i = 0; i < str.Length; ++i)
            {
                var isDigit = char.IsDigit(str[i]);
                if (isDigit)
                {
                    partHasDigit = true;
                    if (partHasLetter)
                    {
                        a[bpi] = str.Substring(oldi, i - oldi);
                        oldi = i;
                        ++bpi;
                        partHasLetter = false;
                    }
                }
                else if (!isDigit)
                {
                    partHasLetter = true;
                    if (partHasDigit)
                    {
                        a[bpi] = decimal.Parse(str.Substring(oldi, i - oldi), CultureInfo.InvariantCulture);
                        oldi = i;
                        ++bpi;
                        partHasDigit = false;
                    }
                }
            }
            if (oldi != str.Length)
                if (partHasLetter)
                    a[bpi++] = str.Substring(oldi, str.Length - oldi);
                else if (partHasDigit)
                    a[bpi++] = decimal.Parse(str.Substring(oldi, str.Length - oldi), CultureInfo.InvariantCulture);
            a[bpi] = null;
        }

        class ColumnInfo
        {
            public bool Enabled;
        }
        readonly ConditionalWeakTable<GridColumn, ColumnInfo> _sortDirections = new ConditionalWeakTable<GridColumn, ColumnInfo>();

    }
}
