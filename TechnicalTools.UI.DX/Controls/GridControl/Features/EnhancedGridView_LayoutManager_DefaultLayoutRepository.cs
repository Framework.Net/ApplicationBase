﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

using TechnicalTools.Diagnostics;


namespace TechnicalTools.UI.DX
{
    public partial class EnhancedGridView_LayoutManager
    {
        public interface ILayoutRepository
        {
            bool HandleSharingLayout     { get; }
            string UIKeyPath             { get; set; }

            Tuple<string, bool> DefaultLayoutInfo { get; set; }

            List<string> GetLayoutNames(bool sharedOrNotShared);
            MemoryStream RetrieveLayout(string layoutName, bool shared);
            void         SaveLayout    (string layoutName, bool shared, MemoryStream stream);
            void         DeleteLayout  (string layoutName, bool shared);
        }
        public class DefaultLayoutRepository : ILayoutRepository
        {
            public string UIKeyPath { get; set; }

            public bool HandleSharingLayout { get { return false; } }

            public Tuple<string, bool> DefaultLayoutInfo
            {
                get
                {
                    var gridview = GetOrCreateLayoutsNode();
                    var layout = gridview.Elements("Layout")
                        .FirstOrDefault(e => e.Attribute("is_default")?.Value == "true");
                    return Tuple.Create(layout?.Attribute("name").Value, false);
                }
                set
                {
                    Debug.Assert(!value.Item2);
                    var gridview = GetOrCreateLayoutsNode();
                    foreach (var layout in gridview.Elements("Layout"))
                        layout.Attribute("is_default")?.Remove();
                    if (value.Item1 == null)
                        return;
                    var newDefaultLayout = GetOrCreateLayoutNode(value.Item1);
                    newDefaultLayout.Add(new XAttribute("is_default", "true"));
                }
            }

            readonly string _repositoryFilenameFullPath;
            readonly XDocument _sharedDocument;

            public static string DefaultRepositoryFileName { get; set; } = Path.GetDirectoryName(Application.ExecutablePath) + "\\GridViewLayouts.xml";

            public DefaultLayoutRepository(string repositoryFilenameFullPath = null)
            {
                _repositoryFilenameFullPath = repositoryFilenameFullPath ?? DefaultRepositoryFileName;
                if (!DocumentByPath.TryGetValue(_repositoryFilenameFullPath, out _sharedDocument))
                {
                    XDocument doc = null;
                    if (File.Exists(_repositoryFilenameFullPath))
                        ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => doc = XDocument.Load(_repositoryFilenameFullPath));
                    _sharedDocument = doc ?? new XDocument(new XElement("LayoutsByGridViewPath"));
                    DocumentByPath.Add(_repositoryFilenameFullPath, _sharedDocument);
                }
            }
            public static readonly Dictionary<string, XDocument> DocumentByPath = new Dictionary<string, XDocument>();


            public XElement GetOrCreateLayoutsNode()
            {
                Debug.Assert(!string.IsNullOrWhiteSpace(UIKeyPath));
                var gridview = _sharedDocument.Root.Elements("GridView")
                                                   .FirstOrDefault(e => e.Attribute("name").Value == UIKeyPath);
                if (gridview == null)
                {
                    gridview = new XElement("GridView", new XAttribute("name", UIKeyPath));
                    _sharedDocument.Root.Add(gridview);
                    _sharedDocument.Save(_repositoryFilenameFullPath); // TODO : what if we dont have enough space on hard drive is this safe (atomic) ?
                }
                return gridview;
            }
            public XElement GetOrCreateLayoutNode(string layoutName)
            {
                var gridview = GetOrCreateLayoutsNode();
                var layout = gridview.Elements("Layout")
                    .FirstOrDefault(e => e.Attribute("name").Value == layoutName);
                if (layout == null)
                {
                    layout = new XElement("Layout", new XAttribute("name", layoutName));
                    gridview.Add(layout);
                    _sharedDocument.Save(_repositoryFilenameFullPath);
                }
                return layout;
            }

            public List<string> GetLayoutNames(bool sharedOrNotShared)
            {
                Debug.Assert(!sharedOrNotShared);
                return GetOrCreateLayoutsNode().Elements("Layout").Select(layout => layout.Attribute("name").Value).ToList();
            }

            public MemoryStream RetrieveLayout(string layoutName, bool shared)
            {
                Debug.Assert(!shared);
                var layout = GetOrCreateLayoutNode(layoutName);
                return new MemoryStream(Convert.FromBase64String(layout.Value));
            }

            public void SaveLayout(string layoutName, bool shared, MemoryStream stream)
            {
                Debug.Assert(!shared);
                var layout = GetOrCreateLayoutNode(layoutName);
                layout.Value = Convert.ToBase64String(stream.ToArray());
                _sharedDocument.Save(_repositoryFilenameFullPath);
            }
            public void DeleteLayout(string layoutName, bool shared)
            {
                Debug.Assert(!shared);
                var layout = GetOrCreateLayoutNode(layoutName);
                if (layout == null)
                    return;
                layout.Remove();
                _sharedDocument.Save(_repositoryFilenameFullPath);
            }
        }
    }
}
