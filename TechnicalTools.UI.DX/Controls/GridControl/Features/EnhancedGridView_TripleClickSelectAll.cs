using System;
using System.Diagnostics;
using System.Windows.Forms;

using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    // Permet de seelctionner l'integralité du contenu d'une cellule si on triple-click dedans
    // Marche aussi pour les treelists normalement
    // Inspired from https://www.devexpress.com/Support/Center/Question/Details/Q569292
    public class EnhancedGridView_TripleClickSelectAll
    {
        readonly GridView _view;
        readonly Timer    _timer = new Timer();
        int               _buttonClickCount;

        public EnhancedGridView_TripleClickSelectAll(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            _timer.Interval = SystemInformation.DoubleClickTime + SystemInformation.DoubleClickTime / 2;
            _timer.Tick += timer_Tick;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.ShownEditor -= view_ShownEditor; view.ShownEditor += view_ShownEditor;
            view.Disposed += view_Disposed;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.ShownEditor -= view_ShownEditor;
            view.Disposed -= view_Disposed;
        }

        private void view_Disposed(object sender, EventArgs e)
        {
            // Prevent memory leak because constructor of timer does not take (sometimes) this.component as argument when created in designer
            _timer.Dispose();
        }

        internal void Assign(EnhancedGridView_TripleClickSelectAll f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }

        void view_ShownEditor(object sender, EventArgs e)
        {
            _view.ActiveEditor.MouseUp += editor_MouseDown;
        }

        private void editor_MouseDown(object sender, MouseEventArgs e)
        {
            if (!_timer.Enabled)
                _timer.Enabled = true;

            if (++_buttonClickCount == 3)
                _view.ActiveEditor.SelectAll();
        }
        void timer_Tick(object sender, EventArgs e)
        {
            if (_view.IsDisposing || (_view.GridControl?.IsDisposed ?? false))
                return;
            _buttonClickCount = 0;
            _timer.Enabled = false;
        }
    }
}
