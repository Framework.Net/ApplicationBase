﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;

using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_GroupByRange
    {
        readonly GridView _view;

        public EnhancedGridView_GroupByRange(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing        -= View_PopupMenuShowing;        view.PopupMenuShowing        += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing        -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_GroupByRange f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code
            // Copy other arguments from f to this
            //     Nothing to do for now
        }

        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;

            if (e.HitInfo.InColumn)
            {
                if (e.HitInfo.Column == null)
                    return;
                var view = sender as GridView;
                var column = e.HitInfo.Column;

                if ((column.ColumnType.TryGetNullableType() ?? column.ColumnType).IsNumericType())
                {
                    var mnuGroup = e.Menu.Items.OfType<DXSubMenuItem>().FirstOrDefault(mnu => mnu.Caption == EnhancedGridView_CollapseAllGroupRow.GroupRowMenuCaption);
                    if (mnuGroup == null)
                    {
                        mnuGroup = new DXSubMenuItem(EnhancedGridView_CollapseAllGroupRow.GroupRowMenuCaption);
                        e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuGroup);
                    }
                    var mnuGroupBy = e.Menu.Items.OfType<DXSubMenuItem>().FirstOrDefault(mnu => mnu.Caption == EnhancedGridView_GroupByTuple.GroupByAdvancedCaption);
                    bool addMnuGroupBy = mnuGroupBy == null;
                    mnuGroupBy = mnuGroupBy ?? new DXSubMenuItem(EnhancedGridView_GroupByTuple.GroupByAdvancedCaption);

                    mnuGroupBy.Items.Add(new DXMenuItem("by ⌊ Log   ⌋", (_, __) => GroupByRange(column, Math.E, null)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by ⌊ Log2  ⌋", (_, __) => GroupByRange(column, 2, null)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by ⌊ Log10 ⌋", (_, __) => GroupByRange(column, 10, null)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by ⌊ Log20 ⌋", (_, __) => GroupByRange(column, 20, null)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by    5 ranges", (_, __) => GroupByRange(column, null, 5)) { BeginGroup = true });
                    mnuGroupBy.Items.Add(new DXMenuItem("by   10 ranges", (_, __) => GroupByRange(column, null, 10)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by   12 ranges", (_, __) => GroupByRange(column, null, 12)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by   20 ranges", (_, __) => GroupByRange(column, null, 20)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by   50 ranges", (_, __) => GroupByRange(column, null, 50)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by  100 ranges", (_, __) => GroupByRange(column, null, 100)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by  480 ranges", (_, __) => GroupByRange(column, null, 480)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by  800 ranges", (_, __) => GroupByRange(column, null, 800)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by 1000 ranges", (_, __) => GroupByRange(column, null, 1000)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by 1920 ranges", (_, __) => GroupByRange(column, null, 1920)));
                    mnuGroupBy.Items.Add(new DXMenuItem("by smart ranges (auto) [Not available yet]!", (_, __) => GroupByRange(column, null, 0)) { Enabled = false, BeginGroup = true });

                    if (addMnuGroupBy)
                        mnuGroup.Items.Add(mnuGroupBy);
                }
            }
        }


        public void GroupByLogRange(GridColumn column, int logBase)
        {
            GroupByRange(column, logBase, null);
        }
        public void GroupByRange(GridColumn column, int nbParts)
        {
            GroupByRange(column, null, nbParts);
        }
        public void GroupByRangeSmartly(GridColumn column)
        {
            GroupByRange(column, null, 0);
        }
        void GroupByRange(GridColumn column, double? logBase, int? nbRanges)
        {
            var view = column.View;
            var key = logBase.HasValue ? "⌊ Log" + (logBase.Value == Math.E ? "" : logBase.Value.ToStringInvariant()) + "(" + column.FieldName + ") ⌋"
                    : nbRanges.HasValue ? nbRanges.Value.ToStringInvariant() + "-Range(" + column.FieldName + ")"
                    : "";
            var col = view.Columns[key];
            if (col != null)
            {
                if (col.GroupIndex < 0)
                    col.Group();
                return;
            }
            col = view.Columns.AddField(key);
            col.OptionsColumn.ReadOnly = true;
            if (logBase.HasValue)
            {
                col.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                ((IUnboundColumnOptimizationRequirement)col).UnboundValueGetter = (row, listSourceRowIndex) =>
                {
                    var v = ConverToDouble(view.GetListSourceRowCellValue(listSourceRowIndex, column));
                    return v == null ? (double?)null
                         : v <= 0 ? double.NaN
                         : Math.Floor(Math.Log(v.Value, logBase.Value));
                };
            }
            else if (nbRanges.HasValue && nbRanges > 0)
            {
                if (view.DataRowCount == 0)
                    return;
                double? min = ConverToDouble(view.GetRowCellValue(0, column));
                double? max = min;
                int rh = 0;
                while (view.IsValidRowHandle(rh))
                {
                    var v = ConverToDouble(view.GetRowCellValue(rh, column));
                    if (!min.HasValue || v < min)
                        min = v;
                    if (!max.HasValue || v > max)
                        max = v;
                    ++rh;
                }
                if (min == null)
                {
                    min = double.MinValue;
                    max = double.MaxValue;
                }
                col.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
                ((IUnboundColumnOptimizationRequirement)col).UnboundValueGetter = (row, listSourceRowIndex) =>
                {
                    var v = ConverToDouble(view.GetListSourceRowCellValue(listSourceRowIndex, column));
                    if (v == null)
                        return null;
                    if (v == max)
                        return nbRanges.Value;
                    v = (v - min) / ((max - min) / nbRanges.Value);
                    return 1 + (int)Math.Floor(v.Value);
                };
            }
            else
            {
                Debug.Assert(nbRanges <= 0);
                // TODO : Use algorithm X-means and cache the ranges
                col.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
                ((IUnboundColumnOptimizationRequirement)col).UnboundValueGetter = (row, listSourceRowIndex) =>
                {
                    //var v = ConverToDouble(view.GetListSourceRowCellValue(listSourceRowIndex, column));
                    // Use the range
                    return null;
                };
            }

            col.Group();
            void handler(object _, EventArgs __)
            {
                if (col.VisibleIndex < 0 && col.GroupIndex < 0)
                {
                    _view.ColumnPositionChanged -= handler;
                    view.Columns.Remove(col);
                }
            }
            _view.ColumnPositionChanged += handler;
        }
        double? ConverToDouble(object v)
        {
            if (v == null)
                return null;
            if (v is decimal)
                return (double)(decimal)v;
            if (v is double)
                return (double)v;
            if (v is float)
                return (float)v;
            if (v is long)
                return (long)v;
            if (v is ulong)
                return (ulong)v;
            if (v is int)
                return (int)v;
            if (v is uint)
                return (uint)v;
            if (v is short)
                return (short)v;
            if (v is ushort)
                return (ushort)v;
            if (v is byte)
                return (byte)v;
            if (v is sbyte)
                return (sbyte)v;
            if (v is TimeSpan)
                return ((TimeSpan)v).Ticks;
            return Convert.ToDouble(v); // should not happens, not efficient at all
        }
    }
}
