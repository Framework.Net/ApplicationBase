﻿using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Diagnostics;
using System.Windows.Forms;

using TechnicalTools.Diagnostics;


namespace TechnicalTools.UI.DX
{
    // Note that file EnhancedGridControl_SmartCopyPaste.cs handle business object on Ctrl-C / Ctrl-V
    public class EnhancedGridView_CopyValue
    {
        readonly GridView _view;

        public EnhancedGridView_CopyValue(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing;
            view.PopupMenuShowing += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_CopyValue f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }

        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu is null)
                return;

            var view = sender as GridView;

            object valueToCopy = null;
            string menuCaption = null;
            if (e.HitInfo.InGroupRow)
            {
                GridColumn columnTargeted = view.FindColumnUnderMouse(e.HitInfo);
                if (columnTargeted == null)
                    return;

                var currentSummary = view.FindSummary(columnTargeted);
                if (currentSummary != null)
                    valueToCopy = view.GetGroupSummaryValue(e.HitInfo.RowHandle, currentSummary);
                menuCaption = "Copy this group summary value";
            }
            else if (e.HitInfo.FooterCell != null)
            {
                valueToCopy = e.HitInfo.FooterCell.Value;
                menuCaption = "Copy this summary value";
            }
            else if (e.HitInfo.InRowCell)
            {
                valueToCopy = view.GetRowCellDisplayText(e.HitInfo.RowHandle, e.HitInfo.Column);
                menuCaption = "Copy this cell value";
            }

            if (valueToCopy == null)
                return;

            var copyMenu = new DXMenuCheckItem(menuCaption);
            copyMenu.Click += (_, __) =>
                {
                    try
                    {
                        Clipboard.SetText(valueToCopy.ToStringInvariant());
                    }
                    catch (Exception ex1)
                    {
                        System.Threading.Thread.Sleep(250);
                        try
                        {
                            Clipboard.SetText(valueToCopy.ToStringInvariant());
                        }
                        catch (Exception ex2)
                        {
                            Clipboard.SetText(valueToCopy.ToStringInvariant());
                            XtraMessageBox.Show("Copying to clipboard seems to not working currently because of dark magic!" + Environment.NewLine +
                                                "Try again later when dark magic is gone...");
                            ExceptionManager.Instance.NotifyException(ex1, UnexpectedExceptionManager.eExceptionKind.ManagedByDevelopper);
                            ExceptionManager.Instance.NotifyException(ex2, UnexpectedExceptionManager.eExceptionKind.ManagedByDevelopper);
                        }
                    }
                };
            e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, copyMenu);
        }
    }
}
