﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools;
using TechnicalTools.Diagnostics;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_RotateColumnHeaders
    {
        readonly GridView _view;

        public int HeaderRotation
        {
            get { return _HeaderRotation; }
            set
            {
                _HeaderRotation = value;
                SetBestHeaderHeight(_view);
                _view.LayoutChanged();
            }
        }
        private int _HeaderRotation;


        public EnhancedGridView_RotateColumnHeaders(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing       -= View_PopupMenuShowing;       view.PopupMenuShowing       += View_PopupMenuShowing;
            view.CustomDrawColumnHeader -= View_CustomDrawColumnHeader; view.CustomDrawColumnHeader += View_CustomDrawColumnHeader;
            view.ColumnPositionChanged  -= View_ColumnPositionChanged;  view.ColumnPositionChanged  += View_ColumnPositionChanged;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing       -= View_PopupMenuShowing;
            view.CustomDrawColumnHeader -= View_CustomDrawColumnHeader;
            view.ColumnPositionChanged  -= View_ColumnPositionChanged;
        }

        internal void Assign(EnhancedGridView_RotateColumnHeaders f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code
        }

        private void View_ColumnPositionChanged(object sender, EventArgs e)
        {
            if (HeaderRotation == 0)
                return;
            SetBestHeaderHeight(_view);
        }
        void SetBestHeaderHeight(GridView view)
        {
            view.BeginUpdate();
            foreach (GridColumn col in view.Columns)
            {
                // These operations change collection view.VisibleColumns
                col.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                col.AppearanceHeader.TextOptions.VAlignment = VertAlignment.Center;
            }
            var max = view.VisibleColumns.Select(col => GetVerticalTextHeight(view, col).Height).DefaultIfEmpty(20).Max();
            view.ColumnPanelRowHeight = max;
            view.EndUpdate();
        }
        Size GetVerticalTextHeight(GridView view, GridColumn col)
        {
            AppearanceObject app = view.PaintAppearance.HeaderPanel;
            StringFormat sf = new StringFormat();
            SizeF size = view.GridControl.CreateGraphics().MeasureString(col.GetCaption(), app.GetFont(), 1000, sf);
            if (col.AppearanceHeader.TextOptions.HAlignment != HorzAlignment.Center ||
                col.AppearanceHeader.TextOptions.VAlignment != VertAlignment.Center)
                size.Width = col.Width;
            double len = Math.Sqrt(size.Width * size.Width + size.Height * size.Height);
            var diag_angle = Math.Atan(size.Height / size.Width);
            var rotated_angle = diag_angle + HeaderRotation * Math.PI / 180;
            var width = Math.Abs((int)Math.Round(len * Math.Cos(rotated_angle), 0, MidpointRounding.AwayFromZero));
            var height = Math.Abs((int)Math.Round(len * Math.Sin(rotated_angle), 0, MidpointRounding.AwayFromZero));

            var rotated_angle2 = -diag_angle + HeaderRotation * Math.PI / 180;
            var width2 = Math.Abs((int)Math.Round(len * Math.Cos(rotated_angle2), 0, MidpointRounding.AwayFromZero));
            var height2 = Math.Abs((int)Math.Round(len * Math.Sin(rotated_angle2), 0, MidpointRounding.AwayFromZero));

            var required_size = new Size(Math.Max(width, width2), Math.Max(height, height2));
            return required_size;
        }

        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            var view = sender as GridView;
            // Feature not done yet, so...
            if (!DebugTools.IsForDevelopper)
                return;
            if (e.HitInfo.InColumn)
            {
                var column = e.HitInfo.Column;

                var mnuRotate = new DXSubMenuItem("Rotate Header...");
                mnuRotate.Items.Add(new DXMenuItem(" by 0°", (_, __) => { HeaderRotation = 0; }));
                mnuRotate.Items.Add(new DXMenuItem(" by 30°", (_, __) => { HeaderRotation = 30; }));
                mnuRotate.Items.Add(new DXMenuItem(" by 45°", (_, __) => { HeaderRotation = 45; }));
                mnuRotate.Items.Add(new DXMenuItem(" by 60°", (_, __) => { HeaderRotation = 60; }));
                mnuRotate.Items.Add(new DXMenuItem(" by 90°", (_, __) => { HeaderRotation = 90; }));
                mnuRotate.Items.Add(new DXMenuItem(" by 180°", (_, __) => { HeaderRotation = 180; }));
                mnuRotate.Items.Add(new DXMenuItem(" by -30°", (_, __) => { HeaderRotation = -30; }));
                mnuRotate.Items.Add(new DXMenuItem(" by -45°", (_, __) => { HeaderRotation = -45; }));
                mnuRotate.Items.Add(new DXMenuItem(" by -60°", (_, __) => { HeaderRotation = -60; }));
                mnuRotate.Items.Add(new DXMenuItem(" by -90°", (_, __) => { HeaderRotation = -90; }));
                e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuRotate);
            }
        }

        private void View_CustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            if (HeaderRotation == 0)
                return;
            // when ColumnAutoWidth is false and columns does not occupy all space, View_CustomDrawColumnHeader is called for remaining space
            if (e.Column == null)
                return;
            // According to https://www.devexpress.com/Support/Center/Question/Details/T149803/vertical-column-header-text-versus-group-by-box
            // It means that the column is in group panel (follow )
            // Follow the improvement request here: https://www.devexpress.com/Support/Center/p/T148380.aspx
            if (e.Column.GroupIndex >= 0)
            {
                //if (e.Info.Info.CellIndex == -1)
                //{
                _view.OptionsView.ColumnHeaderAutoHeight = DefaultBoolean.True;
                    //var b = e.Info.Bounds;
                    //e.Info.Bounds = new Rectangle(b.Left, b.Top, b.Width, 20);
                    e.Info.Caption = "test";
                    return;
                //}
            }
            e.Column.ToolTip = null;
            Rectangle r = e.Info.CaptionRect;
            e.Info.Caption = string.Empty;
            e.Painter.DrawObject(e.Info);
            System.Drawing.Drawing2D.GraphicsState state = e.Graphics.Save();
            StringFormat format = e.Appearance.GetTextOptions().GetStringFormat();
            // format.FormatFlags |= StringFormatFlags.DirectionVertical | StringFormatFlags.DirectionRightToLeft;
            r = Transform(e.Graphics, HeaderRotation, r);
            e.Graphics.DrawString(e.Column.GetCaption(), e.Appearance.Font, e.Appearance.GetForeBrush(e.Cache), r, format);
            e.Graphics.Restore(state);
            e.Handled = true;
            return;
        }

        private Rectangle Transform(Graphics g, int degree, Rectangle r)
        {
            // Translate
            g.TranslateTransform(r.Left + r.Width / 2, r.Top + r.Height / 2);
            g.RotateTransform(-degree);
            // Translate again
            g.TranslateTransform(-r.Left - r.Width / 2, -r.Top - r.Height / 2);
            float cos = (float)Math.Round(Math.Cos(degree * (Math.PI / 180)), 2);
            float sin = (float)Math.Round(Math.Sin(degree * (Math.PI / 180)), 2);
            //Rectangle r1 = r;
            //r1.X = (int)(r.X * cos + r.Y * sin);
            //r1.Y = (int)(r.X * (-sin) + r.Y * cos);
            //return r1;
            return r;
        }
    }
}
