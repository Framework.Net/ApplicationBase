﻿using System;
using System.Diagnostics;

using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    public abstract class GridView_Feature
    {
        protected readonly GridView _view;

        protected GridView_Feature(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install() { Install(_view); }
        protected abstract void Install(GridView view);

        public void Uninstall() { Uninstall(_view); }
        protected abstract void Uninstall(GridView view);
    }
    public abstract class GridView_Feature<TGridView_Feature> : GridView_Feature
        where TGridView_Feature : GridView_Feature<TGridView_Feature>
    {
        protected GridView_Feature(GridView view) : base(view) { }

        // See caller comments to understand this not intuitive code
        // Copy other arguments from "f" to "this"
        protected internal abstract void Assign(TGridView_Feature f, bool copyEvents);
    }
}
