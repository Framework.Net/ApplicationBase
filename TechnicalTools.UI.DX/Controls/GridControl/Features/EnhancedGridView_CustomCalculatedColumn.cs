﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Model;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_CustomCalculatedColumn
    {
        readonly GridView _view;

        public EnhancedGridView_CustomCalculatedColumn(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing; view.PopupMenuShowing += View_PopupMenuShowing;
            view.UnboundExpressionEditorCreated -= View_UnboundExpressionEditorCreated; view.UnboundExpressionEditorCreated += View_UnboundExpressionEditorCreated;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_CustomCalculatedColumn f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            _calculated = f._calculated;
        }


        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if (!e.HitInfo.InColumn || e.HitInfo.Column == null)
                return;
            var view = (GridView)sender;

            var mnus = FillMenu(view, e.HitInfo.Column);
            foreach (var mnu in mnus)
                e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnu);
        }
        int _calculated;
        const string MagicFieldNamePrefix = "|:=";

        public List<DXMenuItem> FillMenu(GridView view, GridColumn colUnderMouse)
        {
            var mnus = new List<DXMenuItem>();
            if (colUnderMouse.FieldName.StartsWith(MagicFieldNamePrefix))
            {
                var mnuRename = new DXMenuItem("Rename this calculated column...", (_, __) =>
                {
                    string newName = colUnderMouse.GetTextCaption();
                    if (System.Windows.Forms.DialogResult.OK == InputBox.ShowDialog(view.GridControl, ref newName, "Enter new name", validateOnEnter: true))
                    {
                        SetCalculatedColumnName(colUnderMouse, newName);
                    }
                });
                mnus.Add(mnuRename);
            }
            var mnuAddCalculatedColumn = new DXMenuItem("Add calculated column...", (_, __) =>
            {
                CreateCalculatedColumn(view, colUnderMouse, null, null);
            });
            mnus.Add(mnuAddCalculatedColumn);
            return mnus;
        }

        public GridColumn CreateCalculatedColumn(GridView view, GridColumn insertAfterColumn, string colCaption, string dxExpression)
        {
            var col = view.Columns.AddField(MagicFieldNamePrefix + _calculated);
            ++_calculated;
            col.Name = col.FieldName; // Important to be taken in account for layout
            col.UnboundType = UnboundColumnType.Decimal; // TODO : remettre Object quand on pourra determiner automatiquement le type avec le code en bas
            col.Caption = "Calculated " + _calculated;
            col.OptionsColumn.ReadOnly = true;
            col.OptionsColumn.AllowSort = DefaultBoolean.True;
            col.OptionsColumn.AllowGroup = DefaultBoolean.True;

            col.ShowUnboundExpressionMenu = true;
            if (string.IsNullOrWhiteSpace(dxExpression))
            {
                view.ShowUnboundExpressionEditor(col);
                if (string.IsNullOrWhiteSpace(col.UnboundExpression))
                {
                    view.Columns.Remove(col);
                    return null;
                }
            }
            else
                col.UnboundExpression = dxExpression;

            col.InsertAfter(insertAfterColumn);

            if (!string.IsNullOrEmpty(colCaption))
                SetCalculatedColumnName(col, colCaption);
            return col;
        }


        public void SetCalculatedColumnName(GridColumn colCalculated, string newName)
        {
            if (!IsColumnCalculated(colCalculated))
                throw new TechnicalException("Column is not a calculated column!", null);
            colCalculated.Caption = newName;
        }

        public static bool IsColumnCalculated(GridColumn col)
        {
            return col.FieldName.StartsWith(MagicFieldNamePrefix);
        }

        private void View_UnboundExpressionEditorCreated(object sender, UnboundExpressionEditorEventArgs e)
        {
            // TODO : en attente de version de DX >= 17.1.3 (from https://www.devexpress.com/Support/Center/Example/Details/T501883/how-to-customize-the-expression-editor)
            //if (e.ExpressionEditorContext == null)
            //{
            //    return;
            //}

            //// Exclude "Now" from the list of available functions.
            //var nowFunction = e.ExpressionEditorContext.Functions.FirstOrDefault(fi => string.Equals(fi.Name, "now", StringComparison.OrdinalIgnoreCase));
            //if (nowFunction != null)
            //{
            //    e.ExpressionEditorContext.Functions.Remove(nowFunction);
            //}

            //// Uncomment the following line to use a custom color provider.
            ////e.ExpressionEditorContext.ColorProvider = new CustomColorProvider();

            //// Implement a custom criteria validator (e.g., to forbid using a specific function).
            //e.ExpressionEditorContext.CriteriaOperatorValidatorProvider = new ValidatorProvider();

            //// Disable capitalization of function names in expressions.
            //e.ExpressionEditorContext.OptionsBehavior.CapitalizeFunctionNames = false;

            //// Rename the "Columns" category to "Fields".
            //foreach (var columnInfo in e.ExpressionEditorContext.Columns)
            //{
            //    columnInfo.Category = "Fields";
            //}

            //// Uncomment the following line to use a custom Expression Editor view.
            ////e.ExpressionEditorView = new CustomExpressionEditorView(this.LookAndFeel, new CustomExpressionEditorControl());
        }
    }

    //class ValidatorProvider : ICriteriaOperatorValidatorProvider
    //{
    //    #region Implementation of ICriteriaOperatorValidatorProvider

    //    public ErrorsEvaluatorCriteriaValidator GetCriteriaOperatorValidator(ExpressionEditorContext context)
    //    {
    //        return new Validator(context);
    //    }

    //    #endregion
    //}

    //class Validator : CriteriaOperatorValidator
    //{
    //    public Validator(ExpressionEditorContext context)
    //        : base(context, supportsAggregates: true)
    //    {
    //    }

    //    #region Overrides of CriteriaOperatorValidator

    //    public override void Visit(FunctionOperator theOperator)
    //    {
    //        if (theOperator.OperatorType == FunctionOperatorType.Now)
    //        {
    //            this.errors.Add(new CriteriaValidatorError("Invalid function: now()"));
    //        }
    //        base.Visit(theOperator);
    //    }

    //    #endregion
    //}


    //class CustomColorProvider : IExpressionEditorColorProvider
    //{
    //    public Color GetColorForElement(ExpressionElementKind elementKind)
    //    {
    //        if (elementKind == ExpressionElementKind.Column)
    //            return Color.BlueViolet;

    //        if (elementKind == ExpressionElementKind.Function)
    //            return Color.Brown;

    //        return Color.Azure;
    //    }
    //}

    //public class CustomExpressionEditorView : ExpressionEditorView
    //{
    //    public CustomExpressionEditorView(UserLookAndFeel lookAndFeel, ExpressionEditorControl expressionEditor)
    //        : base(lookAndFeel, expressionEditor)
    //    {
    //    }
    //}

    //public class CustomExpressionEditorControl : ExpressionEditorControl
    //{
    //    #region Overrides of ExpressionEditorControl

    //    protected override ExpressionDocumentationControl CreateDocumentationControl()
    //    {
    //        return null;
    //    }

    //    #endregion
    //}
}
