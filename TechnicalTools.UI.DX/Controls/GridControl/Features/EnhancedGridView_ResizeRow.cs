﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Scrolling;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


namespace TechnicalTools.UI.DX
{
    // What we want this feature to do :
    // - Allow developper to put a default max row heigh "MaxRowHeight"
    // - Allow user to resize independently any row he want
    // - Allow user to resize row even if it goes above "MaxRowHeight"
    //   (but not above view' heigh because he won't be able to reduce back the row
    // - Respect default row height calculation of gridview.
    // Inspired from https://supportcenter.devexpress.com/ticket/details/e617/how-to-provide-the-ability-to-resize-rows
    public class EnhancedGridView_ResizeRow
    {
        readonly GridView _view;

        public int? MaxRowHeight { get; set; }

        public bool Enabled { get; set; } = true;

        /// <summary>
        /// Could be usefull to developepr if he handles CalcRowHeiht himself
        /// When UserIsResizing is true, i guess he should ignore the event and return
        /// </summary>
        public bool UserIsResizing { get { return _capturedDefaultRowHeightToRestore.HasValue; } }

        public EnhancedGridView_ResizeRow(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);

            Uninstall(view);

            view.OptionsCustomization.AllowRowSizing = true;

            // Try to capture the value of default row height (when view.RowHeight = -1)
            // We can't do this because we can't call base algortihm from view class
            void UpdateMinRoWHeights(object sender, RowHeightEventArgs e)
            {
                var info = (GridViewInfo)view.GetViewInfo();
                _minRowHeight = info.ActualDataRowMinRowHeight;
                view.CalcRowHeight -= UpdateMinRoWHeights;
            }
            view.CalcRowHeight += UpdateMinRoWHeights;

            view.Layout += view_Layout;
            view.CalcRowHeight += view_CalcRowHeight;
            view.MouseDown += view_MouseDown;
            view.MouseUp += view_MouseUp;
        }
        int? _minRowHeight;

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.Layout -= view_Layout;
            view.CalcRowHeight -= view_CalcRowHeight;
            view.MouseDown -= view_MouseDown;
            view.MouseUp -= view_MouseUp;
        }

        internal void Assign(EnhancedGridView_ResizeRow f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
            MaxRowHeight = f.MaxRowHeight;
            _minRowHeight = f._minRowHeight;
            // If row object are the same in both gridviews it should not cause any problem to share the table !
            _rowSizeInfos = f._rowSizeInfos;
        }
        class SizeInfo
        {
            // Height difference (wanted by user) to apply on row height,
            // the difference is related to the default heigh of row calculated by GridView (and the max the limit we do ourself)
            public int HeightDiff;
            // Final expected row height
            // When user resizes a row for the first time this value is not assigned on MouseUp event
            // Instead it is captured later in CalcRowHeigh when we can access the default value processed by default algorithm in gridview
            // This make the next update easier
            public int? ExpectedRowHeight;
        }
        ConditionalWeakTable<object, SizeInfo> _rowSizeInfos;

        private void view_Layout(object sender, EventArgs e)
        {
            var view = (GridView)sender;
            if (_capturedDefaultRowHeightToRestore.HasValue)
            {
                // prevent a recursive call and make behavior depending of _capturedDefaultRowHeightToRestore correct
                var v = _capturedDefaultRowHeightToRestore.Value;
                _capturedDefaultRowHeightToRestore = null;
                Console.WriteLine("Restoring view.RowHeight");
                view.RowHeight = v;
                Console.WriteLine("view.RowHeight restored!");
            }
            _rowAreaHeight  = GetRowAreaHeight(view);
        }

        int GetRowAreaHeight(GridView view)
        {
            var info = (GridViewInfo)view.GetViewInfo();
            var scrollInfo = (ScrollInfo)fScrollInfo.GetValue(view);
            return info.ViewRects.Rows.Height // All available space to display rows (data or group..) according to https://supportcenter.devexpress.com/ticket/details/q350110/determine-default-row-height-before-adding-row
                 - (scrollInfo.HScrollVisible ? scrollInfo.HScrollSize : 0) // minus the horizontal scrollbar's height
                 - 15; // To let a chance user to see the row edge at bottom of grid, ensuring it is always accessible
        }
        int? _rowAreaHeight;
        [AskToDevExpress("Proper way to get this ?")]
        static readonly FieldInfo fScrollInfo = typeof(GridView).GetField("scrollInfo", BindingFlags.Instance | BindingFlags.NonPublic);

        private void view_MouseDown(object sender, MouseEventArgs e)
        {
            if (!Enabled)
                return;
            if (e.Button != MouseButtons.Left)
                return;
            var view = (GridView)sender;
            GridHitInfo hi = view.CalcHitInfo(e.X, e.Y);
            if (hi.HitTest == GridHitTest.RowEdge)
            {
                resizeStartPos = e.Y;
                _resizingRowHandle = hi.RowHandle;
            }
        }
        int resizeStartPos;
        int? _resizingRowHandle;

        private void view_MouseUp(object sender, MouseEventArgs e)
        {
            if (!_resizingRowHandle.HasValue) // User is not resizing a row
                return;
            int heightToAdd = e.Y - resizeStartPos;
            _rowSizeInfos = _rowSizeInfos ?? new ConditionalWeakTable<object, SizeInfo>();
            var view = (GridView)sender;
            var obj = _resizingRowHandle.Value == GridControl.AutoFilterRowHandle
                    ? DummyObjectForAutoFilterRow
                    : view.GetRow(_resizingRowHandle.Value);
            SizeInfo si = _rowSizeInfos.GetOrCreateValue(obj);
            si.HeightDiff += heightToAdd;

            // To understand this test see comment in view_CalcRowHeight
            if (si.ExpectedRowHeight.HasValue) // User already changed the size of this row in the past
            {
                // So DX won't do this itself anymore (we see this behavior in by analysing CalcRowHeigh RowHeight value),
                // so we do it ourselves
                si.ExpectedRowHeight += heightToAdd; // update the exepcted row heiught (initialized the first time in CalcRowHeight)
                // And we also apply a min heigh also anyway
                if (si.ExpectedRowHeight < _minRowHeight)
                {
                    si.HeightDiff += _minRowHeight.Value - si.ExpectedRowHeight.Value;
                    si.ExpectedRowHeight = _minRowHeight.Value;
                }
            }

            // Aftr MouseUp event, DX will call LayoutChanged()
            // So we plan to restore view.RowHeight because when resizing one row, DX change view.RowHeight wich affect all rows untouched using this class
            // This is a not the desired behavior... So we remember the wish of user (si.HeightDiff)
            // an restore postpone the restoration of view.RowHeight
            // Then handling correctly the event CalcRowHeigh
            _capturedDefaultRowHeightToRestore = view.RowHeight;

            // We also remember for later (CalcRowHeigh) if the sizing is decreasing because DX behaves weirdly too in that case
            SizingIsReduce = heightToAdd < 0;

            // Example of weird case : DX does not call LayoutChanged/CalcRowHeight when decrease the heigh of a row  (why ?)
            // We interpret this as user ant to make the row as tiny as possible
            // DX would do nothing
            if (heightToAdd + (si.ExpectedRowHeight ?? 0) < 0)
                view.LayoutChanged(); // To force call to view_CalcRowHeight and apply heigh minification

            //Console.WriteLine("RowHeightSupplement = " + si.HeightDiff);
            _resizingRowHandle = null;
        }
        int? _capturedDefaultRowHeightToRestore;
        static readonly object DummyObjectForAutoFilterRow = new object();
        bool SizingIsReduce;

        [AskToDevExpress]
        private void view_CalcRowHeight(object sender, RowHeightEventArgs e)
        {
            // When user resize a row, there is two block of CalcRowHeight that are raised
            // The first one is ignored because it is due to the change of View.RowHeight that we cancel right after
            // So we want to ignore the first set of event and handle the second block.
            // The arguments e from second block of events, contains the original row height
            // This is easier for us to apply the change ourselves.
            // There is a caveat though : When user reduce a row, there is only one block of event,
            // so we handle him
            if (_capturedDefaultRowHeightToRestore.HasValue &&
                !SizingIsReduce)
                return;

            //Console.WriteLine("----")
            //Console.WriteLine("For RowHandle = " + e.RowHandle + "  e.RowHeight is by default  " + e.RowHeight)

            var view = (GridView)sender;
            // Apply maximum height allowed to default heigh algortihm
            // (for example when there is a RepositoryItemMemoEdit with long text)
            // This does not prevent user to force the row sizing
            if (e.RowHeight > MaxRowHeight)
            {
                e.RowHeight = MaxRowHeight.Value;
                //Console.WriteLine("e.RowHeight maxified to " + e.RowHeight)
            }
            // If user has made no row sizing we quit
            if (_rowSizeInfos == null)
                return;

            var businessObject = e.RowHandle == GridControl.AutoFilterRowHandle
                    ? DummyObjectForAutoFilterRow // to avoid null key in conditional weak table
                    : view.GetRow(e.RowHandle);

            // just a safety measure, it should not happens, but DX bug sometimes
            if (businessObject == null)
                return;

            // If a sizing already happens or is in its finalizing state (ie: we have to capture the default heigh)
            if (_rowSizeInfos.TryGetValue(businessObject, out SizeInfo si))
            {
                // First time user resize
                if (!si.ExpectedRowHeight.HasValue)
                {
                    si.ExpectedRowHeight = e.RowHeight + si.HeightDiff;
                    //Console.WriteLine("ExpectedRowHeight memorized to " + si.ExpectedRowHeight)

                    // prevent a blank area to appear at the bottom of the grid, where next rows should be visible
                    if (SizingIsReduce)
                        view.GridControl.BeginInvokeSafely(view.RefreshData);
                }
                else
                    e.RowHeight = si.ExpectedRowHeight.Value;
                //Console.WriteLine("e.RowHeight set to " + e.RowHeight)
            }
        }
    }
}
