﻿using System;
using System.Diagnostics;

using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_FixedColumns
    {
        readonly GridView _view;

        public EnhancedGridView_FixedColumns(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing; view.PopupMenuShowing += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_FixedColumns f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }


        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if (e.HitInfo is BandedGridHitInfo bHitInfo)
            {
                if (bHitInfo.InBandPanel && bHitInfo.Band != null)
                {
                    var mnuFixBand = new DXSubMenuItem("Fixer la bande...");

                    var mnuLeft = new DXMenuCheckItem("Fixer à gauche", bHitInfo.Band.Fixed == FixedStyle.Left);
                    mnuLeft.Click += (_, __) => bHitInfo.Band.Fixed = bHitInfo.Band.Fixed == FixedStyle.Left ? FixedStyle.None : FixedStyle.Left;
                    var mnuRight = new DXMenuCheckItem("Fixer à droite", bHitInfo.Band.Fixed == FixedStyle.Right);
                    mnuRight.Click += (_, __) => bHitInfo.Band.Fixed = bHitInfo.Band.Fixed == FixedStyle.Right ? FixedStyle.None : FixedStyle.Right;
                    mnuFixBand.Items.Add(mnuLeft);
                    mnuFixBand.Items.Add(mnuRight);

                    e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuFixBand);
                }
            }
            else if (e.HitInfo.InColumn && e.HitInfo.Column != null)
            {
                //FocusedRowHandle = e.HitInfo.RowHandle;
                var mnuFixColumn = new DXSubMenuItem("Fixer la colonne...");

                var mnuLeft = new DXMenuCheckItem("Fixer à gauche", e.HitInfo.Column.Fixed == FixedStyle.Left);
                mnuLeft.Click += (_, __) => e.HitInfo.Column.Fixed = e.HitInfo.Column.Fixed == FixedStyle.Left ? FixedStyle.None : FixedStyle.Left;
                var mnuRight = new DXMenuCheckItem("Fixer à droite", e.HitInfo.Column.Fixed == FixedStyle.Right);
                mnuRight.Click += (_, __) => e.HitInfo.Column.Fixed = e.HitInfo.Column.Fixed == FixedStyle.Right ? FixedStyle.None : FixedStyle.Right;
                mnuFixColumn.Items.Add(mnuLeft);
                mnuFixColumn.Items.Add(mnuRight);

                e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuFixColumn);
            }
        }
    }
}
