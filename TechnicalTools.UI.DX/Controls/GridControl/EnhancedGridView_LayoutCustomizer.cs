﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

using DevExpress.Utils;
using DevExpress.Utils.Serializing;

using TechnicalTools.Model;


namespace TechnicalTools.UI.DX
{
    public partial class EnhancedGridView
    {
        protected EnhancedGridView_LayoutCustomizer LayoutCustomizer
        {
            get
            {
                return _layoutCustomizer ?? (_layoutCustomizer = CreateLayoutCustomizer());
            }
        }
        protected virtual EnhancedGridView_LayoutCustomizer CreateLayoutCustomizer() { return new EnhancedGridView_LayoutCustomizer(); }
        EnhancedGridView_LayoutCustomizer _layoutCustomizer;

        public event EventHandler<SaveLayoutDataEventArgs> SaveLayoutData;
        protected override void SaveLayoutCore(XtraSerializer serializer, object path, OptionsLayoutBase options)
        {
            base.SaveLayoutCore(serializer, path, options);
            if (SaveLayoutData != null)
                LayoutCustomizer.SaveLayoutCustomization(path, SaveLayoutData.GetInvocationList()
                                                                             .Cast<EventHandler<SaveLayoutDataEventArgs>>()
                                                                             .ToArray());
        }

        public event EventHandler<RestoreLayoutDataEventArgs> RestoreLayoutData;
        protected override void RestoreLayoutCore(XtraSerializer serializer, object path, OptionsLayoutBase options)
        {
            BeginUpdate();
            try
            {
                var data = RestoreLayoutData == null ? null
                         : LayoutCustomizer.GetLayoutCustomization(path, RestoreLayoutData.GetInvocationList()
                                                                                          .Cast<EventHandler<RestoreLayoutDataEventArgs>>()
                                                                                          .ToArray());
                if (data != null)
                    RestoreLayoutData?.Invoke(this, data);
                base.RestoreLayoutCore(serializer, path, options);
                if (data != null)
                {
                    data.AfterDefaultRestore = true;
                    RestoreLayoutData?.Invoke(this, data);
                }
            }
            finally
            {
                EndUpdate();
            }
        }
    }

    public partial class EnhancedBandedGridView
    {
        protected EnhancedGridView_LayoutCustomizer LayoutCustomizer
        {
            get
            {
                return _layoutCustomizer ?? (_layoutCustomizer = CreateLayoutCustomizer());
            }
        }
        protected virtual EnhancedGridView_LayoutCustomizer CreateLayoutCustomizer() { return new EnhancedGridView_LayoutCustomizer(); }
        EnhancedGridView_LayoutCustomizer _layoutCustomizer;

        public event EventHandler<SaveLayoutDataEventArgs> SaveLayoutData;
        protected override void SaveLayoutCore(XtraSerializer serializer, object path, OptionsLayoutBase options)
        {
            base.SaveLayoutCore(serializer, path, options);
            if (SaveLayoutData != null)
                LayoutCustomizer.SaveLayoutCustomization(path, SaveLayoutData.GetInvocationList()
                                                                             .Cast<EventHandler<SaveLayoutDataEventArgs>>()
                                                                             .ToArray());
        }

        public event EventHandler<RestoreLayoutDataEventArgs> RestoreLayoutData;
        protected override void RestoreLayoutCore(XtraSerializer serializer, object path, OptionsLayoutBase options)
        {
            BeginUpdate();
            try
            {
                var data = RestoreLayoutData == null ? null
                         : LayoutCustomizer.GetLayoutCustomization(path, RestoreLayoutData.GetInvocationList()
                                                                                          .Cast<EventHandler<RestoreLayoutDataEventArgs>>()
                                                                                          .ToArray());
                if (data != null)
                    RestoreLayoutData?.Invoke(this, data);
                base.RestoreLayoutCore(serializer, path, options);
                if (data != null)
                {
                    data.AfterDefaultRestore = true;
                    RestoreLayoutData?.Invoke(this, data);
                }
            }
            finally
            {
                EndUpdate();
            }
        }
    }

    public partial interface IEnhancedGridView
    {
        event EventHandler<SaveLayoutDataEventArgs> SaveLayoutData;
        event EventHandler<RestoreLayoutDataEventArgs> RestoreLayoutData;
    }
    public class SaveLayoutDataEventArgs
    {
        public XElement Data
        {
            get { return _userNode; }
            set
            {
                Container = Container ?? new XElement(EnhancedGridView_LayoutCustomizer.CustomLayoutDataRootNode);
                if (value != null && Container.Element(value.Name) != null && _userNode.Name != value.Name)
                    throw new TechnicalException("This name is already taken, please choose another!", null);
                Container.Add(value);
                if (_userNode != null)
                    _userNode.Remove();
                _userNode = value;
            }
        }
        XElement _userNode;
        internal XElement Container { get; private set; }

        internal void Empty() { _userNode = null; }

        public SaveLayoutDataEventArgs(XElement container) { Container = container; }
    }
    public class RestoreLayoutDataEventArgs
    {
        public bool BeforeDefaultRestore { get { return !AfterDefaultRestore; } }
        public bool AfterDefaultRestore  { get; protected internal set; }

        public RestoreLayoutDataEventArgs(XElement container)
        {
            _container = container;
        }
        readonly XElement _container;

        /// <summary> This property is supposed to be readonly. You have to push</summary>
        public XElement Data(string tagName) { return _container.Element(tagName); }
    }

    public class EnhancedGridView_LayoutCustomizer
    {
        protected internal const string CustomLayoutDataRootNode = "CustomLayoutDataRootNode";
        public virtual void SaveLayoutCustomization(object path, EventHandler<SaveLayoutDataEventArgs>[] handlers)
        {
            if (!(path is MemoryStream ms) || !ms.CanSeek)
                return;
            var e = new SaveLayoutDataEventArgs(null);
            foreach (var h in handlers)
            {
                h.Invoke(this, e);
                e.Empty();
            }
            if (e.Container != null)
            {
                ms.Position = 0;
                var doc = XDocument.Load(ms);
                doc.Root.Add(e.Container);
                using (var ms2 = new MemoryStream())
                {
                    var xws = new XmlWriterSettings()
                    {
                        OmitXmlDeclaration = true,
                        Indent = true,
                    };
                    using (var xw = XmlWriter.Create(ms2, xws))
                        doc.WriteTo(xw);
                    ms2.Position = 0;
                    ms.Position = 0;
                    ms2.CopyTo(ms);
                }
            }
        }

        public virtual RestoreLayoutDataEventArgs GetLayoutCustomization(object path, EventHandler<RestoreLayoutDataEventArgs>[] handlers)
        {
            XElement data = null;
            if (path is MemoryStream ms)
            {
                data = XDocument.Load(ms).Root.Element(CustomLayoutDataRootNode);
                ms.Position = 0;
            }
            return data == null ? null : new RestoreLayoutDataEventArgs(data);
        }
    }
}
