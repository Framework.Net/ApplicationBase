﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Container;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;


namespace TechnicalTools.UI.DX
{
    public partial class EnhancedGridView : IEnhancedGridView_AccesForViewFeature
    {
        protected override RepositoryItem RequestCellEditor(DevExpress.XtraGrid.Views.Grid.ViewInfo.GridCellInfo cell)
        {
            return base.RequestCellEditor(cell);
        }
        RepositoryItem IEnhancedGridView_AccesForViewFeature.GetCellEditor(DevExpress.XtraGrid.Views.Grid.ViewInfo.GridCellInfo cell)
        {
            return RequestCellEditor(cell);
        }
    }
    public partial class EnhancedBandedGridView : IEnhancedGridView_AccesForViewFeature
    {
        protected override RepositoryItem RequestCellEditor(DevExpress.XtraGrid.Views.Grid.ViewInfo.GridCellInfo cell)
        {
            return base.RequestCellEditor(cell);
        }
        RepositoryItem IEnhancedGridView_AccesForViewFeature.GetCellEditor(DevExpress.XtraGrid.Views.Grid.ViewInfo.GridCellInfo cell)
        {
            return RequestCellEditor(cell);
        }
    }

    public partial class EnhancedGridControl
    {
        protected override EditorContainerHelper CreateHelper()
        {
            return new EnhancedGridEditorContainerHelper(this);
        }
    }

    public class EnhancedGridEditorContainerHelper : GridEditorContainerHelper
    {
        public EnhancedGridEditorContainerHelper(GridControl owner) : base(owner) { }

        //protected override DefaultEditorsRepository CreateDefaultEditorsRepository()
        //{
        //    return new EnhancedEditorsRepository(Owner);
        //}

        public override BaseEdit UpdateEditor(RepositoryItem ritem, UpdateEditorInfoArgs args)
        {
            var editor = base.UpdateEditor(ritem, args);
            //FixAllowNullInputOnDatatable(Owner, editor);

            return editor;
        }

        internal static void FixAllowNullInputOnDatatable(GridControl gc, BaseEdit editor)
        {
            if (gc.DataSource is DataTable dt)
            {
                //var asTextEdit = editor as TextEdit;
                //if (asTextEdit != null)
                //    asTextEdit.NullValueMode = eNullValueMode.DBNull;

                //var asTextNumericEdit = editor as TextNumericEdit;
                //if (asTextNumericEdit != null)
                //{
                //    var gView = gc.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                //    Debug.Assert(gView != null);
                //    var fieldName = gView.FocusedColumn.FieldName;
                //    // Si on recupere les infos de schema, cette valeur peut etre a false, mais par defaut elle est a true. En revanche AllowNullInput est à false
                //    asTextNumericEdit.Properties.AllowNullInput = dt.Columns[fieldName].AllowDBNull ? DefaultBoolean.True : DefaultBoolean.False;
                //}
            }
            else
                Debug.Assert(!(gc.DataSource is DataView), "Peut etre a gerer comme la datasource ci dessus");
        }
    }

    //public class EnhancedEditorsRepository : DefaultEditorsRepository
    //{
    //    GridControl _owner;
    //    readonly Dictionary<Type, RepositoryItem>             _editorsCache;
    //    readonly Dictionary<Type, Func<bool, RepositoryItem>> _repositoryItemCreators;

    //    protected internal EnhancedEditorsRepository(GridControl owner)
    //    {
    //        _owner = owner;

    //        _editorsCache = new Dictionary<Type, RepositoryItem>();
    //        _repositoryItemCreators = new Dictionary<Type, Func<bool, RepositoryItem>>();
    //        _repositoryItemCreators.Add(typeof(string), CreateRepositoryItemForString);
    //        _repositoryItemCreators.Add(typeof(bool), CreateRepositoryItemForBool);
    //        _repositoryItemCreators.Add(typeof(byte), CreateRepositoryItemForByte);
    //        _repositoryItemCreators.Add(typeof(sbyte), CreateRepositoryItemForSignedByte);
    //        _repositoryItemCreators.Add(typeof(short), CreateRepositoryItemForShort);
    //        _repositoryItemCreators.Add(typeof(ushort), CreateRepositoryItemForUnsignedShort);
    //        _repositoryItemCreators.Add(typeof(int), CreateRepositoryItemForInt32);
    //        _repositoryItemCreators.Add(typeof(uint), CreateRepositoryItemForUnsignedInt32);
    //        _repositoryItemCreators.Add(typeof(long), CreateRepositoryItemForInt64);
    //        _repositoryItemCreators.Add(typeof(ulong), CreateRepositoryItemForUnsignedInt64);
    //        _repositoryItemCreators.Add(typeof(double), CreateRepositoryItemForDouble);
    //        _repositoryItemCreators.Add(typeof(decimal), CreateRepositoryItemForDecimal);
    //        _repositoryItemCreators.Add(typeof(float), CreateRepositoryItemForFloat);
    //        _repositoryItemCreators.Add(typeof(TimeSpan), CreateRepositoryItemForTimeSpan);
    //        _repositoryItemCreators.Add(typeof(DateTime), CreateRepositoryItemForDateTime);
    //    }

    //    public override RepositoryItem GetRepositoryItem(Type type)
    //    {
    //        if (_editorsCache.ContainsKey(type))
    //            return _editorsCache[type];
    //        Func<bool, RepositoryItem> create;
    //        var subtype = type.TryGetNullableType();
    //        if (_repositoryItemCreators.TryGetValue(subtype ?? type, out create))
    //        {
    //            // Si le type de la datasource est de type Datatable, alors on considere les type nullable.
    //            // En effet Devexpress n'utilise pas les type definit dans les colonne de la table
    //            // (comme par Exemple AllowDbNull qui est mise a false quand la requête permettant de récuperer les données à utiliser WithSchema = true)
    //            bool allowNull = subtype != null || _owner.DataSource is DataTable || _owner.DataSource is DataView;
    //            RepositoryItem result = create(allowNull);
    //            _editorsCache.Add(type, result);
    //            return result;
    //        }
    //        return base.GetRepositoryItem(type);
    //    }


    //    // Tous les types : https://msdn.microsoft.com/en-us/library/364x0z75.aspx

    //    protected virtual RepositoryItem CreateRepositoryItemForString(bool allowNull)
    //    {
    //        return base.GetRepositoryItem(typeof(string));
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForBool(bool allowNull)
    //    {
    //        var repo = base.GetRepositoryItem(allowNull ? typeof(bool?) : typeof(bool));
    //        //repo.EditValueChanged += (sender, __) =>
    //        //{
    //        //    GridView view = ((GridControl)(sender as RepositoryItemCheckEdit).ParentControl).FocusedView;
    //        //    view.PostEditor();
    //        //});

    //        return repo;
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForByte(bool allowNull)
    //    {
    //        var editor = new TextNumericGridEditRepositoryItem
    //        {
    //            AllowNullInput = allowNull ? DefaultBoolean.True : DefaultBoolean.False,
    //            VisibleDecimalCount = eDigitCount.Zero,
    //            EditDecimalCount = eDigitCount.Zero,
    //            MinValue = byte.MinValue,
    //            MaxValue = byte.MaxValue
    //        };
    //        return editor;
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForSignedByte(bool allowNull)
    //    {
    //        var editor = new TextNumericGridEditRepositoryItem
    //        {
    //            AllowNullInput = allowNull ? DefaultBoolean.True : DefaultBoolean.False,
    //            VisibleDecimalCount = eDigitCount.Zero,
    //            EditDecimalCount = eDigitCount.Zero,
    //            MinValue = sbyte.MinValue,
    //            MaxValue = sbyte.MaxValue
    //        };
    //        return editor;
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForShort(bool allowNull)
    //    {
    //        var editor = new TextNumericGridEditRepositoryItem
    //        {
    //            AllowNullInput = allowNull ? DefaultBoolean.True : DefaultBoolean.False,
    //            VisibleDecimalCount = eDigitCount.Zero,
    //            EditDecimalCount = eDigitCount.Zero,
    //            MinValue = short.MinValue,
    //            MaxValue = short.MaxValue
    //        };
    //        return editor;
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForUnsignedShort(bool allowNull)
    //    {
    //        var editor = new TextNumericGridEditRepositoryItem
    //        {
    //            AllowNullInput = allowNull ? DefaultBoolean.True : DefaultBoolean.False,
    //            VisibleDecimalCount = eDigitCount.Zero,
    //            EditDecimalCount = eDigitCount.Zero,
    //            MinValue = ushort.MinValue,
    //            MaxValue = ushort.MaxValue
    //        };
    //        return editor;
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForInt32(bool allowNull)
    //    {
    //        var editor = new TextNumericGridEditRepositoryItem
    //        {
    //            AllowNullInput = allowNull ? DefaultBoolean.True : DefaultBoolean.False,
    //            VisibleDecimalCount = eDigitCount.Zero,
    //            EditDecimalCount = eDigitCount.Zero,
    //            MinValue = int.MinValue,
    //            MaxValue = int.MaxValue
    //        };
    //        return editor;
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForUnsignedInt32(bool allowNull)
    //    {
    //        var editor = new TextNumericGridEditRepositoryItem
    //        {
    //            AllowNullInput = allowNull ? DefaultBoolean.True : DefaultBoolean.False,
    //            VisibleDecimalCount = eDigitCount.Zero,
    //            EditDecimalCount = eDigitCount.Zero,
    //            MinValue = uint.MinValue,
    //            MaxValue = uint.MaxValue
    //        };
    //        return editor;
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForInt64(bool allowNull)
    //    {
    //        var editor = new TextNumericGridEditRepositoryItem
    //        {
    //            AllowNullInput = allowNull ? DefaultBoolean.True : DefaultBoolean.False,
    //            VisibleDecimalCount = eDigitCount.Zero,
    //            EditDecimalCount = eDigitCount.Zero,
    //            MinValue = Int64.MinValue,
    //            MaxValue = Int64.MaxValue
    //        };
    //        return editor;
    //        //return base.GetRepositoryItem(allowNull ? typeof(Int64?) : typeof(Int64));
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForUnsignedInt64(bool allowNull)
    //    {
    //        var editor = new TextNumericGridEditRepositoryItem
    //        {
    //            AllowNullInput = allowNull ? DefaultBoolean.True : DefaultBoolean.False,
    //            VisibleDecimalCount = eDigitCount.Zero,
    //            EditDecimalCount = eDigitCount.Zero,
    //            MinValue = UInt64.MinValue,
    //            MaxValue = UInt64.MaxValue
    //        };
    //        return editor;
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForDouble(bool allowNull)
    //    {
    //        return CreateRepositoryItemForDecimal(allowNull);
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForDecimal(bool allowNull)
    //    {
    //        RepositoryItem editor = new TextNumericGridEditRepositoryItem
    //            {
    //                AllowNullInput = allowNull ? DefaultBoolean.True : DefaultBoolean.False,
    //            //    ShowTrailingZeroDecimal = DefaultBoolean.True,
    //                VisibleDecimalCount = eDigitCount.Eight,
    //                EditDecimalCount = eDigitCount.Eight,
    //                MinValue = -100000000000,
    //                MaxValue =  100000000000
    //            };
    //        //editor = base.GetRepositoryItem(typeof (decimal));
    //        return editor;
    //    }

    //    protected virtual RepositoryItem CreateRepositoryItemForFloat(bool allowNull)
    //    {
    //        var editor = new TextNumericGridEditRepositoryItem
    //        {
    //            AllowNullInput = allowNull ? DefaultBoolean.True : DefaultBoolean.False,
    //            //ShowTrailingZeroDecimal = DefaultBoolean.False,
    //            VisibleDecimalCount = eDigitCount.Four,
    //            EditDecimalCount = eDigitCount.Four,
    //            MinValue = -100000000000,
    //            MaxValue = 100000000000
    //        };
    //        return editor;
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForTimeSpan(bool allowNull)
    //    {
    //        var editor = new RepositoryItemTimeEdit()
    //        {
    //            AllowNullInput = allowNull ? DefaultBoolean.True : DefaultBoolean.False,
    //        };
    //        return editor;
    //    }
    //    protected virtual RepositoryItem CreateRepositoryItemForDateTime(bool allowNull)
    //    {
    //        var editor = new DateEditRepositoryItem()
    //        {
    //            AllowNullInput = allowNull ? DefaultBoolean.True : DefaultBoolean.False,
    //        };
    //        return editor;

    //        //return base.GetRepositoryItem(allowNull ? typeof(DateTime?) : typeof(DateTime));
    //    }


    //}

}
