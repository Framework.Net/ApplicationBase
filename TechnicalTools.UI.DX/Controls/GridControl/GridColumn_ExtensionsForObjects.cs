﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;

using TechnicalTools.Diagnostics;
using TechnicalTools.Tools;

namespace TechnicalTools.UI.DX
{
    /// <summary>
    /// <para>Permet la configuration d'une colonne d'une gridview pour selectionner un objet</para>
    /// <para>Utilisation :
    /// myGridColumn.ConfigureForObjects&lt;yourEnumType&gt;();</para>
    ///
    /// <para>Gère plusieurs choses :
    /// - Traduction d'un objet en nom metier)
    /// - Traduction egalement dans la row filter
    /// - Traduction dans la popup filter (Si OptionsView.ShowAutoFilterRow = true)
    /// - TODO : Le tri se fait dans l'ordre du nom et non pas des id ou de la reference vers l'objet</para>
    /// </summary>
    public static class GridColumn_ExtensionsForObjects
    {
        interface IHelperBase { void Uninstall(); }

        class Helper<TObject, TId> : IHelperBase
            where TObject : class
        {
            readonly GridColumn _column;
                     RepositoryItemLookUpEdit _repoEditor;
            readonly IEnumerable<TObject> _objects;
            readonly Func<TObject, string> _get_name;
            readonly Func<TObject, TId> _get_id;
            readonly bool _as_int;
            readonly bool _with_empty;

            public Helper(GridColumn column, IEnumerable<TObject> objects, Func<TObject, string> get_name, Func<TObject, TId> get_id, bool with_empty, bool as_int)
            {
                _column = column;
                _objects = objects;
                _get_name = get_name;
                _get_id = get_id;
                _as_int = as_int;
                _with_empty = with_empty;
            }

            public void Install()
            {
                _column.View.CustomColumnDisplayText += View_CustomColumnDisplayText;
                _column.SortMode = ColumnSortMode.DisplayText;
                _column.FilterMode = ColumnFilterMode.DisplayText;

                _repoEditor = new RepositoryItemLookUpEdit();
                _column.View.GridControl.RepositoryItems.AddRange(new RepositoryItem[] { _repoEditor });
                _repoEditor.AutoHeight = false;
                _repoEditor.Name = _column.View.Name + "_autoRepoForEnum_" + typeof(TObject).Name;

                _repoEditor.EditValueChanged += RepositoryEditor_EditValueChanged;

                //if (_as_int)
                //    _repoEditor.FillWithObjectsAsInt<TObject>();
                //else
                    _repoEditor.FillWithObjects(_objects, _get_name, _get_id, _with_empty);
                _column.ColumnEdit = _repoEditor;

                // How to make the width of the GridLookUpEdit dropdown grid be the same as that of the editor
                // http://www.devexpress.com/Support/Center/Example/Details/E1574
                //static void gridLookUpEdit1_QueryPopUp(object sender, CancelEventArgs e)
                //{
                //    GridLookUpEdit editor = (GridLookUpEdit) sender;
                //    RepositoryItemGridLookUpEdit properties = editor.Properties;
                //    properties.PopupFormSize = new Size(editor.Width - 4, properties.PopupFormSize.Height);
                //}
            }

            public void Uninstall()
            {
                if (_column.View != null)
                {
                    _column.View.CustomColumnDisplayText -= View_CustomColumnDisplayText;
                    _column.View.GridControl.RepositoryItems.Remove(_repoEditor);
                }
                _column.ColumnEdit = null;
                _repoEditor = null;
            }

            // Cet évènement a l'avantage, contrairement à CustomDrawCell, de convertir également les valeurs dans le PopupFilter
            // (la popup apparait quand on clique sur le petit signe en forme de filtre qui apparait quand la souris passe sur le titre de la colone)
            [DebuggerStepThrough]
            void View_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
            {
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(delegate // Les exceptions de cette partie du code sont silencieuses (ignoré par devexpress) et la grille se retrouve blanche !)
                {
                    if (e.Column != _column ||
                        e.Value == null ||
                        (!e.Value.GetType().IsEnum && e.Value.GetType() != typeof (Enum) && !(e.Value is int)))
                        return;
                    TObject selected_object;
                    if (_as_int)
                    {
                        Debug.Assert(!_objects.Any() || _get_id(_objects.First()).GetType() == typeof(TId), "Les types de la valeur bindée et l'id de l'objet ne sont pas les mêmes");
                        selected_object = _objects.FirstOrDefault(obj => _get_id(obj).Equals(e.Value)); // Cas rare : e.Value peut etre null (meme si on a testé avant !)
                    }
                    else
                        selected_object = (TObject) e.Value;

                    e.DisplayText = selected_object == null ? _repoEditor.NullText : _get_name(selected_object);
                });
            }


            void RepositoryEditor_EditValueChanged(object sender, EventArgs eventArgs)
            {
                // Dès que l'utilisateur change la valeur dans la combobox, la valeur est settée dans le modèle
                // Il s'agit du comportement le plus intuitif pour l'utilisateur
                _column.View.PostEditor();
            }
        }

        static readonly Dictionary<GridColumn, IHelperBase> Helpers = new Dictionary<GridColumn, IHelperBase>();
        public static void Install<TObject, TId>(GridColumn col, IEnumerable<TObject> objects, Func<TObject, string> get_name, Func<TObject, TId> get_id, bool with_empty = false, bool as_int = false)
            where TObject : class
        {
            if (Helpers.ContainsKey(col))
                return; // déjà installé, donc on quitte

            var helper = new Helper<TObject, TId>(col, objects, get_name, get_id, with_empty, as_int);
            Helpers.Add(col, helper);
            col.Disposed += col_Disposed;
            helper.Install();
        }

        static void col_Disposed(object sender, EventArgs e)
        {
            ((GridColumn)sender).Disposed -= col_Disposed;
            Uninstall((GridColumn)sender);
        }
        // Installe une colonne permettant d'editer un champs de type objet, en fournissant une collection d'objet et un moyen d'afficher un nom par objet
        public static TGridColumn ConfigureForObjects<TGridColumn, TObject>(this TGridColumn col, IEnumerable<TObject> objects, Func<TObject, string> get_name, bool with_empty = false, Disambiguator<TGridColumn, GridColumn> _ = null)
            where TGridColumn : GridColumn // Allow for GridColumn and BandedGridColumn
            where TObject : class
        {
            Install(col, objects, get_name, o => o, with_empty);
            return col;
        }
        // Installe une colonne permettant d'editer un champs de type Id (int, long...) faisant reference a une liste d'objet
        public static TGridColumn ConfigureForObjectsAsInt<TGridColumn, TObject, TId>(this TGridColumn col, IEnumerable<TObject> objects, Func<TObject, string> get_name, Func<TObject, TId> get_id, bool with_empty = false, Disambiguator<TGridColumn, GridColumn> _ = null)
            where TGridColumn : GridColumn // Allow for GridColumn and BandedGridColumn
            where TObject : class
        {
            Install(col, objects, get_name, get_id, with_empty, true);
            return col;
        }
        // Installe une colonne permettant d'editer un champs de type Id (int, long...) faisant reference a une liste d'objet
        public static TGridColumn ConfigureForTuples_ID_Name<TGridColumn, TId>(this TGridColumn col, IEnumerable<Tuple<TId, string>> objects, bool with_empty = false, Disambiguator<TGridColumn, GridColumn> _ = null)
            where TGridColumn : GridColumn // Allow for GridColumn and BandedGridColumn
        {
            Install(col, objects, tup => tup.Item2, tup => tup.Item1, with_empty,true);
            return col;
        }

        public static void Uninstall(GridColumn col)
        {
            if (!Helpers.ContainsKey(col))
                return; // Rien à faire car déjà désinstallé

            Helpers[col].Uninstall();
            Helpers.Remove(col);
            col.Disposed -= col_Disposed;
        }


    }
}
