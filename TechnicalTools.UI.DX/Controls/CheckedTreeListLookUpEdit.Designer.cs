﻿namespace TechnicalTools.UI.DX.Controls
{
    partial class CheckedTreeListLookUpEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.popupContainerControl = new DevExpress.XtraEditors.PopupContainerControl();
            this.treeList = new TechnicalTools.UI.DX.Controls.TreeViewBound();
            this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.popupContainerEdit = new DevExpress.XtraEditors.PopupContainerEdit();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.toolTipController = new DevExpress.Utils.ToolTipController();
            this.chkSelectAll = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl)).BeginInit();
            this.popupContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAll.Properties)).BeginInit();
            this.SuspendLayout();
            //
            // popupContainerControl
            //
            this.popupContainerControl.Controls.Add(this.treeList);
            this.popupContainerControl.Controls.Add(this.chkSelectAll);
            this.popupContainerControl.Location = new System.Drawing.Point(0, 27);
            this.popupContainerControl.Name = "popupContainerControl";
            this.popupContainerControl.Size = new System.Drawing.Size(200, 178);
            this.popupContainerControl.TabIndex = 4;
            //
            // treeList
            //
            this.treeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colName});
            this.treeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList.KeyFieldName = "_";
            this.treeList.Location = new System.Drawing.Point(0, 19);
            this.treeList.Name = "treeList";
            this.treeList.OptionsBehavior.AllowExpandOnDblClick = false;
            this.treeList.OptionsBehavior.Editable = false;
            this.treeList.OptionsFind.AllowIncrementalSearch = true;
            this.treeList.OptionsView.ShowAutoFilterRow = true;
            this.treeList.OptionsView.ShowButtons = false;
            this.treeList.OptionsView.ShowCheckBoxes = true;
            this.treeList.OptionsView.ShowColumns = false;
            this.treeList.OptionsView.ShowHorzLines = false;
            this.treeList.OptionsView.ShowIndicator = false;
            this.treeList.OptionsView.ShowVertLines = false;
            this.treeList.ParentFieldName = "ParentNode";
            this.treeList.RootValue = null;
            this.treeList.Size = new System.Drawing.Size(200, 159);
            this.treeList.TabIndex = 2;
            //
            // colName
            //
            this.colName.Caption = " Name";
            this.colName.FieldName = " Name";
            this.colName.MinWidth = 34;
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 91;
            //
            // popupContainerEdit
            //
            this.popupContainerEdit.Dock = System.Windows.Forms.DockStyle.Top;
            this.popupContainerEdit.Location = new System.Drawing.Point(0, 0);
            this.popupContainerEdit.Name = "popupContainerEdit";
            this.popupContainerEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit.Properties.PopupControl = this.popupContainerControl;
            this.popupContainerEdit.Size = new System.Drawing.Size(310, 20);
            this.popupContainerEdit.TabIndex = 5;
            this.popupContainerEdit.Popup += new System.EventHandler(this.popupContainerEdit_Popup);
            //
            // dxErrorProvider
            //
            this.dxErrorProvider.ContainerControl = this;
            //
            // chkSelectAll
            //
            this.chkSelectAll.Dock = System.Windows.Forms.DockStyle.Top;
            this.chkSelectAll.Location = new System.Drawing.Point(0, 0);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Properties.Caption = "Select All";
            this.chkSelectAll.Size = new System.Drawing.Size(200, 19);
            this.chkSelectAll.TabIndex = 8;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            //
            // CheckedTreeListLookUpEdit
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.popupContainerControl);
            this.Controls.Add(this.popupContainerEdit);
            this.Name = "CheckedTreeListLookUpEdit";
            this.Size = new System.Drawing.Size(310, 214);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl)).EndInit();
            this.popupContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAll.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
        public DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit;
        public TechnicalTools.UI.DX.Controls.TreeViewBound treeList;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.Utils.ToolTipController toolTipController;
        private DevExpress.XtraEditors.CheckEdit chkSelectAll;
    }
}
