using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

using DevExpress.Utils;
using DevExpress.Utils.Controls;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList;

using TechnicalTools.Model;
using TechnicalTools.Tools;


namespace TechnicalTools.UI.DX.Controls
{
    [ToolboxItem(true)]
    public partial class CheckedTreeListLookUpEdit : XtraUserControl, IXtraResizableControl
    {
        public void ItemsAvailablesSet<T>(IEnumerable<T> datasource)
            where T : ITreeNodeWithParentReadable, IIsNamed
        {
            treeList.ItemsAvailablesSet(datasource);
        }

        /// <summary>
        /// Liste de ISimpleTreeNodeReadOnly (ou IBindingTreeNode)
        /// Utilisez DatasourceSet qui est fortement typé et qui filtre les interfaces gérées
        /// </summary>
        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IEnumerable<ITreeNodeWithParentReadable> ItemsAvailables
        {
            get { return treeList.ItemsAvailables; }
        }

        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IEnumerable<ITreeNodeWithParentReadable> ItemsDisabled
        {
            get { return treeList.ItemsDisabled; }
            set { treeList.ItemsDisabled = value; }
        }

        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IEnumerable<ITreeNodeWithParentReadable> ItemsUnselectable
        {
            get { return treeList.ItemsUnselectable; }
            set { treeList.ItemsUnselectable = value; }
        }

        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IFixedBindingList<ITreeNodeWithParentReadable> ItemsSelected
        {
            get { return treeList.ItemsSelected; }
            set { treeList.ItemsSelected = value; }
        }

        [DefaultValue(0)]
        public uint MaxSelectedItems
        {
            get { return treeList.MaxSelectedItems; }
            set { treeList.MaxSelectedItems = value; if (!DesignTimeHelper.IsInDesignMode && treeList.MaxSelectedItems > 0) chkSelectAll.Visible = chkSelectAll.Enabled = false; }
        }


        public CheckedTreeListLookUpEdit()
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;

            colName.FieldName = GetMemberName.For<IIsNamed>(no => no.Name);
            colName.OptionsColumn.AllowEdit = false;
            colName.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            colName.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            // Allow to display (and select !) more operator than only "equals" and "does not equals"
            colName.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            // So we can use contains as default.
            colName.OptionsFilter.AutoFilterCondition = DevExpress.XtraTreeList.Columns.AutoFilterCondition.Contains;

            popupContainerEdit.QueryResultValue += popupContainerEdit_QueryResultValue;
            popupContainerEdit.BeforePopup += PopupContainerEdit_BeforePopup;
            treeList.RefreshVisualInformations += treeList_OnRefreshVisualInformations;
            treeList.VirtualTreeGetCellValue += treeList_VirtualTreeGetCellValue;
            //this.Resize += new System.EventHandler(this.CheckedTreeListLookUpEdit_Resize);
            //CheckedTreeListLookUpEdit_Resize(null, null);
            Size = new Size(Size.Width, ((IXtraResizableControl) this).MinSize.Height);

            treeList.BeforeChange = () => dxErrorProvider.ClearErrors();
            treeList.OnError = OnError;
        }

        public void AddColumn<TItem, TFieldType> (string colName, Func<TItem, TFieldType> getValue)
            where TItem : ITreeNodeWithParentReadable, IIsNamed
        {
            treeList.AddColumn(colName, getValue);
        }

        private void PopupContainerEdit_BeforePopup(object sender, EventArgs e)
        {
            // Make the size of popup the same  than control's one...
            popupContainerEdit.Properties.PopupFormMinSize = new Size(Width, popupContainerEdit.Properties.PopupFormMinSize.Height);
            // ...however it prevents the popup to be shown the first time user click on control,
            // (why ?) so we fixed it this way
            if (!_firstPopupShowingWithGoodWidthFixed)
            {
                _firstPopupShowingWithGoodWidthFixed = true; // prevent recursivity
                // So we schedule a new popup right after the current event
                // Note that we need to Invoke ShowPopup otherwise some weird stuff happens in DX controls about disposed control
                BeginInvoke((Action)(() =>
                {
                    popupContainerEdit.ShowPopup();
                }));
            }
        }
        bool _firstPopupShowingWithGoodWidthFixed;

        private void popupContainerEdit_Popup(object sender, EventArgs e)
        {
            treeList.FocusedNode = treeList.Nodes.AutoFilterNode;
            treeList.ShowEditor();
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSelectAll.Checked)
                treeList.SetSelectedItems(ItemsAvailables.Except(ItemsUnselectable).Except(ItemsDisabled.Except(ItemsSelected)).ToList());
            else
                treeList.SetSelectedItems(ItemsDisabled.Intersect(ItemsSelected).ToList());
        }

        void OnError(Exception ex)
        {
            dxErrorProvider.SetError(popupContainerEdit, ex.Message);
            toolTipController.ShowHint(ex.Message, popupContainerEdit, ToolTipLocation.Default);
        }

        void treeList_VirtualTreeGetCellValue(object sender, VirtualTreeGetCellValueInfo e)
        {
            Debug.Assert(e.Node != null);

            if (e.Column == colName)
                e.CellData = e.Node is IIsNamed ?
                                 (e.Node as IIsNamed).Name
                                 : e.Node.ToString();
        }

        void treeList_OnRefreshVisualInformations(object sender, EventArgs eventArgs)
        {
            popupContainerEdit.Text = Text;
        }

        void popupContainerEdit_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = Text;
            dxErrorProvider.ClearErrors();
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Bindable(true)]
        [Browsable(false)]
        public override string Text
        {
            get { return GetResumeText(); }
            /* set
             {
                 if (string.IsNullOrEmpty(value))
                 {
                     if (treeList.IsItemSelectedUserDatasource)
                     {
                         Debug.Assert(ItemsSelected != null);
                         ItemsSelected.Clear();
                     }
                     else
                         ItemsSelected = null;
                 }
                 else
                 {
                     var nodes_by_name = treeList.GetAllCheckedNodes().ToDictionary(node => node.GetDisplayText(colName), node => node);
                     ItemsSelected = value.Split(new[] {Separator})
                                          .Select(strItem => nodes_by_name[strItem.Trim())
                                          .;node.GetDisplayText(colName)


                 }
                 treeList.RefreshVisualInformations();
                 popupContainerEdit.Text =
             }
             * */
        }
        [DefaultValue(',')]
        public char Separator
        {
            get { return _Separator; }
            set
            {
                _Separator = value;
                treeList.RaiseRefreshVisualInformations();
            }
        }
        char _Separator = ',';

        string GetResumeText()
        {
            return string.Join(_Separator + " ", treeList.GetAllCheckedNodes()
                                                         .Select(node => node.GetDisplayText(colName))
                                                         .ToList());
        }

        #region Gestion du layout (taille)

        // Pour interdire le resize de la hauteur dans le designer VS (y'a surement un moyen de faire ca avec les adorners)
        // la hauteur se repositionne sur la hauteur de la combo
        //private void CheckedTreeListLookUpEdit_Resize(object sender, EventArgs e)
        //{
        //    if (_resizing)
        //        return;

        //    _resizing = true; // Sécurité pour éviter une eventuelle récursion infinie
        //    Height = popupContainerEdit.Height;
        //    popupContainerControl.Width = Width;
        //    _resizing = false;
        //}
        //bool _resizing;

        //// Limite la taille du control dans le designer
        //public new Size Size
        //{
        //    get { return new Size(Width, Height); }
        //    set { base.Size = value; }
        //}
        //public new int Height
        //{
        //    get { return popupContainerControl.Height; }
        //    set { popupContainerControl.Height = value; }
        //}

        //public new Rectangle Bounds
        //{
        //    get { return new Rectangle(Left, Top, Width, Height); }
        //    set { base.Bounds = value; }
        //}

        #region IXtraResizableControl : Gestion du comportement du control quand il est droppé dans un LayoutControl


        Size IXtraResizableControl.MinSize { get { return ((IXtraResizableControl) popupContainerEdit).MinSize; } }
        Size IXtraResizableControl.MaxSize { get { return ((IXtraResizableControl) popupContainerEdit).MaxSize; } }
        bool IXtraResizableControl.IsCaptionVisible { get { return ((IXtraResizableControl) popupContainerEdit).IsCaptionVisible; } }
        event EventHandler IXtraResizableControl.Changed
        {
            add { ((IXtraResizableControl) popupContainerEdit).Changed += value; }
            remove { ((IXtraResizableControl) popupContainerEdit).Changed -= value; }
        }

        #endregion

        #endregion

        #region IControlAccessibility Members

        [DefaultValue(true)]
        public bool IsCurrentlyMeaningful
        {
            get { return _IsCurrentlyMeaningful; }
            set
            {
                _IsCurrentlyMeaningful = value;
                if (!DesignTimeHelper.IsInDesignMode)
                {
                    treeList.Enabled = _IsCurrentlyMeaningful;
                    popupContainerControl.Enabled = _IsCurrentlyMeaningful && EditableByUser; // pas de readonly sur popupContainerControl
                }
            }
        }
        bool _IsCurrentlyMeaningful = true;

        // TODO : Lire https://www.devexpress.com/Support/Center/Question/Details/CS49838 pour laisser les bouttons clickable
        [DefaultValue(true)]
        public bool EditableByUser
        {
            get { return _EditableByUser; }
            set
            {
                _EditableByUser = value;
                if (!DesignTimeHelper.IsInDesignMode)
                {
                    popupContainerEdit.ReadOnly = !_EditableByUser;
                    treeList.OptionsBehavior.ReadOnly = !_EditableByUser;
                    popupContainerControl.Enabled = _EditableByUser && EditableByUser; // pas de readonly sur popupContainerControl
                }
            }
        }
        bool _EditableByUser = true;

        [Obsolete("Utilisez IsCurrentlyMeaningful ou EditableByUser", true)]
        public new bool Enabled
        {
            get { return base.Enabled; }
            set { base.Enabled = value; }
        }

        #endregion
    }
}
