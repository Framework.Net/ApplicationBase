﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;

using DevExpress.XtraCharts;


namespace TechnicalTools.UI.DX.Controls
{
    [ToolboxItem(true)]
    public partial class ChartStackedArea : EnhancedXtraUserControl
    {
        public ChartStackedArea()
        {
            InitializeComponent();

            if (!DesignTimeHelper.IsInDesignMode)
                ClearAllSeries();
        }

        public void ClearAllSeries()
        {
            Chart.Series.Clear();
            Chart.Titles.Clear();
        }

        public void AddSerie<T>(string title, IReadOnlyDictionary<string, Dictionary<T, decimal>> data, IReadOnlyDictionary<string, Color> colors = null)
            where T : struct
        {
            // Create a new chart.
            var series = data.Select(kvp =>
            {
                var serie = new Series(kvp.Key, ViewType.StackedArea);
                serie.Points.AddRange(kvp.Value.Select(p => new SeriesPoint(p.Key, p.Value)).ToArray());
                // Set the numerical argument scale types for the series,
                // as it is qualitative, by default.
                serie.ArgumentScaleType = (typeof(T).IsIntegerType() ? ScaleType.Numerical
                                        : typeof(T) == typeof(DateTime) ? ScaleType.DateTime
                                        : (ScaleType?)null).ThrowIfNull("Type of data for X axis not handled!");
                if (colors != null && colors.ContainsKey(kvp.Key))
                    serie.View.Color = colors[kvp.Key];
                return serie;
            });

            // Add both series to the chart.
            Chart.Series.AddRange(series.ToArray());

            // Access the view-type-specific options of the series.
            //((StackedAreaSeriesView)series1.View).Transparency = 80;

            if (data.Count > 0)
            {
                // Access the type-specific options of the diagram.
                ((XYDiagram)Chart.Diagram).EnableAxisXZooming = true;
                ((XYDiagram)Chart.Diagram).EnableAxisYZooming = true;
            }

            // Hide the legend (if necessary).
            Chart.Legend.Title.Text = title;
            Chart.Legend.UseCheckBoxes = true;
            //chart.Legend.Visibility = DefaultBoolean.False;

            // Add a title to the chart (if necessary).
            if (!string.IsNullOrWhiteSpace(title))
                Chart.Titles.Add(new ChartTitle() { Text = title });
        }


        List<ValueSet> Sets { get; set; }

        public class ValueSet
        {
            public string                                     Name { get; set; }
            public IEnumerable<KeyValuePair<string, decimal>> Values { get; set; }
            public Series                                     Serie { get; set; }
        }

        //void IVisitableTrackerControl.Accepts(IVisitorOfTrackerControls visitor) { visitor.Visits(this); }

        //#region IControlAccessibility Members

        //[DefaultValue(true)]
        //public bool IsCurrentlyMeaningful
        //{
        //    get { return _IsCurrentlyMeaningful; }
        //    set
        //    {
        //        _IsCurrentlyMeaningful = value;
        //        //if (!DesignTimeHelper.IsInDesignMode)
        //        //    ....Enabled = _IsCurrentlyMeaningful && _EditableByUser
        //    }
        //}
        //bool _IsCurrentlyMeaningful = true;

        //[DefaultValue(true)]
        //public bool EditableByUser
        //{
        //    get { return _EditableByUser; }
        //    set
        //    {
        //        _EditableByUser = value;
        //        //if (!DesignTimeHelper.IsInDesignMode)
        //        //    ....Enabled = _IsCurrentlyMeaningful && _EditableByUser
        //    }
        //}
        //bool _EditableByUser = true;

        //#endregion
    }
}
