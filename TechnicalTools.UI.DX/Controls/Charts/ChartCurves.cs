﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using DevExpress.XtraCharts;
using DevExpress.XtraEditors;

using TechnicalTools.Model;


namespace TechnicalTools.UI.DX.Controls
{
    [ToolboxItem(true)]
    public partial class ChartCurves : EnhancedXtraUserControl
    {
        public ChartCurves()
        {
            InitializeComponent();

            if (!DesignTimeHelper.IsInDesignMode)
                ClearAllSeries();
        }

        public void ClearAllSeries()
        {
            Chart.Series.Clear();
            //Sets.Clear();
            Chart.Titles.Clear();
        }
        public void AddSeries<TX, TY>(string chartTitle, Dictionary<string, Dictionary<TX, TY>> data, IReadOnlyDictionary<string, Color> colors = null)
             where TX : struct
             where TY : struct
        {
            if (typeof(TY) == typeof(decimal))
                AddSeries(chartTitle, (Dictionary<string, Dictionary<TX, decimal>>)(object)data, colors);
            else
                XtraMessageBox.Show($"Chart with Y Axis of type {typeof(TY).Name} is not implemented yet!");
        }
        public void AddSeries<TX>(string chartTitle, Dictionary<string, Dictionary<TX, decimal>> data, IReadOnlyDictionary<string, Color> colors = null)
            where TX : struct
        {
            // Create a new chart.
            var series = data.Select(kvp =>
            {
                var serie = new Series(kvp.Key, ViewType.Line);
                serie.Points.AddRange(kvp.Value.Select(p => new SeriesPoint(p.Key, p.Value)).ToArray());
                // Access the view-type-specific options of the series.
                ((LineSeriesView)serie.View).LineMarkerOptions.Kind = MarkerKind.Triangle;
                ((LineSeriesView)serie.View).LineStyle.DashStyle = DashStyle.Dash;
                if (colors != null && colors.ContainsKey(kvp.Key))
                    serie.View.Color = colors[kvp.Key];
                return serie;
            });

            // Add the series to the chart.
            Chart.Series.AddRange(series.ToArray());

            // Access the type-specific options of the diagram.
            if (data.Count > 0)
            {
                ((XYDiagram)Chart.Diagram).EnableAxisXZooming = true;
                ((XYDiagram)Chart.Diagram).EnableAxisYZooming = true;
            }

            // Hide the legend (if necessary).
            Chart.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;

            // Add a title to the chart (if necessary).
            if (!string.IsNullOrWhiteSpace(chartTitle))
                Chart.Titles.Add(new ChartTitle() { Text = chartTitle });

            // Add the chart to the form.
            Chart.Dock = DockStyle.Fill;
        }
        public void AddSeries<TX, TY>(string chartTitle, Dictionary<string, Dictionary<IReadOnlyCollection<TX>, IReadOnlyCollection<TY>>> data)
            where TX : struct
            where TY : struct
        {
            if (typeof(TY) != typeof(decimal))
                throw new UserUnderstandableException("Cannot display chart with Y axis of type " + typeof(TY) + " (not yet implemented)!", null);

            // Create a new chart.
            var series = data.Select(kvp =>
            {
                var serie = new Series(kvp.Key, ViewType.StackedArea);
                serie.Points.AddRange(kvp.Value.Select(p => new SeriesPoint(p.Key, p.Value)).ToArray());
                // Set the numerical argument scale types for the series,
                // as it is qualitative, by default.
                serie.ArgumentScaleType = (typeof(TX).IsIntegerType() ? ScaleType.Numerical
                                        : typeof(TX) == typeof(DateTime) ? ScaleType.DateTime
                                        : typeof(TX) == typeof(TimeSpan) ? ScaleType.Auto
                                        : (ScaleType?)null).ThrowIfNull("Type of data for X axis not handled!");
                return serie;
            });

            // Add the series to the chart.
            Chart.Series.AddRange(series.ToArray());

            // Access the view-type-specific options of the series.
            //((LineSeriesView)series1.View).LineMarkerOptions.Kind = MarkerKind.Triangle;
            //((LineSeriesView)series1.View).LineStyle.DashStyle = DashStyle.Dash;
            if (data.Count > 0)
            {
                // Access the type-specific options of the diagram.
                ((XYDiagram)Chart.Diagram).EnableAxisXZooming = true;
                ((XYDiagram)Chart.Diagram).EnableAxisYZooming = true;
            }

            // Hide the legend (if necessary).
            Chart.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;

            // Add a title to the chart (if necessary).
            if (!string.IsNullOrWhiteSpace(chartTitle))
                Chart.Titles.Add(new ChartTitle() { Text = chartTitle });

        }


        List<ValueSet> Sets { get; set; }

        public class ValueSet
        {
            public string                                     Name { get; set; }
            public IEnumerable<KeyValuePair<string, decimal>> Values { get; set; }
            public Series                                     Serie { get; set; }
        }

        //void IVisitableTrackerControl.Accepts(IVisitorOfTrackerControls visitor) { visitor.Visits(this); }

        //#region IControlAccessibility Members

        //[DefaultValue(true)]
        //public bool IsCurrentlyMeaningful
        //{
        //    get { return _IsCurrentlyMeaningful; }
        //    set
        //    {
        //        _IsCurrentlyMeaningful = value;
        //        //if (!DesignTimeHelper.IsInDesignMode)
        //        //    ....Enabled = _IsCurrentlyMeaningful && _EditableByUser
        //    }
        //}
        //bool _IsCurrentlyMeaningful = true;

        //[DefaultValue(true)]
        //public bool EditableByUser
        //{
        //    get { return _EditableByUser; }
        //    set
        //    {
        //        _EditableByUser = value;
        //        //if (!DesignTimeHelper.IsInDesignMode)
        //        //    ....Enabled = _IsCurrentlyMeaningful && _EditableByUser
        //    }
        //}
        //bool _EditableByUser = true;

        //#endregion
    }
}
