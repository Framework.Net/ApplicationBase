﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

using DevExpress.Utils;
using DevExpress.XtraCharts;


namespace TechnicalTools.UI.DX.Controls
{
    /// <summary>
    /// Dessine un graph en forme de camembert automatiquement en faisant :
    /// var graph = new TrkPieChart()
    /// graph.AddSerie("title", [dictionaire contenant nom et valeur à afficher])
    /// </summary>
    [ToolboxItem(true)]
    public partial class ChartNestedDoughnut : EnhancedXtraUserControl//, IVisitableTrackerControl, IControlAccessibility
    {
        public ChartNestedDoughnut()
        {
            InitializeComponent();

            if (!DesignTimeHelper.IsInDesignMode)
                ClearAllSeries();
        }

        public void ClearAllSeries()
        {
            Chart.Series.Clear();
            Chart.Titles.Clear();
        }

        public void AddSeries(string title, Dictionary<string, Tuple<IReadOnlyList<string>, IReadOnlyList<decimal>, IReadOnlyList<Color>>> data)
        {
            // Create a new chart.
            var series = data.Select(kvp =>
            {
                var serie = new Series(kvp.Key, ViewType.NestedDoughnut);
                Debug.Assert(kvp.Value.Item1.Count == kvp.Value.Item2.Count);
                Debug.Assert(kvp.Value.Item3 == null || kvp.Value.Item1.Count == kvp.Value.Item3.Count);
                for (int i = 0; i < kvp.Value.Item1.Count; ++i)
                {
                    var valueName = kvp.Value.Item1[i];
                    valueName = string.IsNullOrWhiteSpace(valueName) ? "N/A" : valueName;
                    var value = kvp.Value.Item2[i];
                    var point = new SeriesPoint(valueName, value);
                    if (kvp.Value.Item3 != null)
                        point.Color = kvp.Value.Item3[i];
                    serie.Points.Add(point);
                }

                // Specify the hole radius percentage and inner indent of the nested doughnut.
                ((NestedDoughnutSeriesView)serie.View).InnerIndent = 8;
                ((NestedDoughnutSeriesView)serie.View).HoleRadiusPercent = 30;

                // Enable a tooltip and specify the tooltip point pattern for series.
                Chart.ToolTipEnabled = DefaultBoolean.True;
                serie.ToolTipPointPattern = "{A} - {V} which is about {VP:##.##%}";
                // Hide series labels.
                serie.LabelsVisibility = DefaultBoolean.True;
                serie.Label.TextPattern = "{A}"; // which is about {VP:##.##%}";
                return serie;
            });

            // Add the series to the chart.
            Chart.Series.AddRange(series.ToArray());

            // to try..
            //chart.SeriesTemplate.Label.ResolveOverlappingMode = ResolveOverlappingMode.Default;

            // Hide the legend (if necessary).
            //chart.Legend.Visibility = DefaultBoolean.False;

            // Add a title to the chart (if necessary).
            if (!string.IsNullOrWhiteSpace(title))
                Chart.Titles.Add(new ChartTitle() { Text = title });

        }

        //void IVisitableTrackerControl.Accepts(IVisitorOfTrackerControls visitor) { visitor.Visits(this); }

        //#region IControlAccessibility Members

        //[DefaultValue(true)]
        //public bool IsCurrentlyMeaningful
        //{
        //    get { return _IsCurrentlyMeaningful; }
        //    set
        //    {
        //        _IsCurrentlyMeaningful = value;
        //        //if (!DesignTimeHelper.IsInDesignMode)
        //        //    ....Enabled = _IsCurrentlyMeaningful && _EditableByUser
        //    }
        //}
        //bool _IsCurrentlyMeaningful = true;

        //[DefaultValue(true)]
        //public bool EditableByUser
        //{
        //    get { return _EditableByUser; }
        //    set
        //    {
        //        _EditableByUser = value;
        //        //if (!DesignTimeHelper.IsInDesignMode)
        //        //    ....Enabled = _IsCurrentlyMeaningful && _EditableByUser
        //    }
        //}
        //bool _EditableByUser = true;

        //#endregion
    }
}
