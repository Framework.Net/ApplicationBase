﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;

using DevExpress.Utils;
using DevExpress.XtraCharts;


namespace TechnicalTools.UI.DX.Controls
{
    /// <summary>
    /// Dessine un graph en forme de camembert automatiquement en faisant :
    /// var graph = new TrkPieChart()
    /// graph.AddSerie("title", [dictionaire contenant nom et valeur à afficher])
    /// </summary>
    [ToolboxItem(true)]
    public partial class ChartPie3D : EnhancedXtraUserControl//, IVisitableTrackerControl, IControlAccessibility
    {
        public ChartPie3D()
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            Sets = new List<ValueSet>();
            ClearAllSeries();
        }

        public void ClearAllSeries()
        {
            Chart.Series.Clear();
            Sets.Clear();
            Chart.Titles.Clear();
        }

        public void AddSerie(string valueSet_name, IEnumerable<KeyValuePair<string, decimal>> values, IReadOnlyDictionary<string, Color> colors = null)
        {
            var vset = new ValueSet
            {
                Values = values,
                Serie = new Series(valueSet_name, ViewType.Pie3D)
            };
            foreach (var kvp in values)
            {
                var point = new SeriesPoint(string.IsNullOrWhiteSpace(kvp.Key) ? "N/A" : kvp.Key, kvp.Value);
                if (colors != null && colors.TryGetValue(kvp.Key, out Color c))
                    point.Color = c;
                vset.Serie.Points.Add(point);
            }

            vset.Serie.ToolTipPointPattern = "{A} - {V} which is about {VP:##.##%}";
            // Hide series labels.
            vset.Serie.LabelsVisibility = DefaultBoolean.True;
            vset.Serie.Label.TextPattern = "{A}"; // "{A}: {VP:P0}";

            Chart.Series.Add(vset.Serie);
            Chart.Legend.Visibility = DefaultBoolean.True;

            (Chart.Diagram as Diagram3D).ZoomPercent = 125;
            (Chart.Diagram as Diagram3D).RuntimeRotation = true;
            (Chart.Diagram as Diagram3D).RuntimeZooming = true;
            (Chart.Diagram as Diagram3D).RuntimeScrolling = true;

            if (!string.IsNullOrWhiteSpace(valueSet_name))
                Chart.Titles.Add(new ChartTitle() { Text = valueSet_name });
        }
        List<ValueSet> Sets { get; set; }

        public class ValueSet
        {
            public string                                     Name { get; set; }
            public IEnumerable<KeyValuePair<string, decimal>> Values { get; set; }
            public Series                                     Serie { get; set; }
        }

        //void IVisitableTrackerControl.Accepts(IVisitorOfTrackerControls visitor) { visitor.Visits(this); }

        //#region IControlAccessibility Members

        //[DefaultValue(true)]
        //public bool IsCurrentlyMeaningful
        //{
        //    get { return _IsCurrentlyMeaningful; }
        //    set
        //    {
        //        _IsCurrentlyMeaningful = value;
        //        //if (!DesignTimeHelper.IsInDesignMode)
        //        //    ....Enabled = _IsCurrentlyMeaningful && _EditableByUser
        //    }
        //}
        //bool _IsCurrentlyMeaningful = true;

        //[DefaultValue(true)]
        //public bool EditableByUser
        //{
        //    get { return _EditableByUser; }
        //    set
        //    {
        //        _EditableByUser = value;
        //        //if (!DesignTimeHelper.IsInDesignMode)
        //        //    ....Enabled = _IsCurrentlyMeaningful && _EditableByUser
        //    }
        //}
        //bool _EditableByUser = true;

        //#endregion
    }
}
