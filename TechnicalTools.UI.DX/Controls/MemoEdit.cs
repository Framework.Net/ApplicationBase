﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

using DevExpress.Utils.Design;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.ViewInfo;

using TechnicalTools.Diagnostics;

using HScrollBar = DevExpress.XtraEditors.HScrollBar;
using VScrollBar = DevExpress.XtraEditors.VScrollBar;


namespace TechnicalTools.UI.DX.Controls
{
    [ToolboxItem(true)]
    public class MemoEdit : DevExpress.XtraEditors.MemoEdit
    {
        // Redefinition de Text juste pour ajouter EditorAttribute qui permet de gérer plusieurs lignes dans le designer
        [EditorAttribute(typeof(MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [RefreshProperties(RefreshProperties.All)]
        [SmartTagProperty("Text", "", SmartTagActionType.RefreshBoundsAfterExecute)]
        [Bindable(true)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DXCategory("Appearance")]
        public override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        public MemoEdit()
        {
            AppendTextWithoutMovingCursorPosition_Initialize();
            ScrollPosition_Initialize();
            ShowScrollbarsOnlyWhenNeeded_Initialize();
        }

        #region pour eviter le clignotement

        // TODO : http://stackoverflow.com/questions/1333393/how-to-prevent-a-windows-forms-textbox-from-flickering-on-resize

        #endregion

        #region AppendText Sans faire bouger la position du curseur
        // From https://www.devexpress.com/Support/Center/Question/Details/A1009
        // a comparer avec la deuxieme solution decrite par le support ici https://www.devexpress.com/Support/Center/Question/Details/Q234936
        void AppendTextWithoutMovingCursorPosition_Initialize()
        {
            foreach (Control control in this.Controls)
                if (control is TextBox)
                {
                    _innerTextBox = (TextBox)control;
                    break;
                }
            Debug.Assert(_innerTextBox != null);
        }
        TextBox _innerTextBox;

        public void AppendText(string text)
        {
            _innerTextBox.AppendText(text);
        }
        #endregion

        #region Prevent autoselect on focus (from https://www.devexpress.com/Support/Center/Question/Details/Q384225/memoedit-all-the-text-is-selected-when-a-control-is-focused)

        [DefaultValue(false)] // Default value is for the behavior of base class
        public bool DisableSelectAllOnFocus { get; set; }

        protected override void OnGotFocus(EventArgs e)
        {
            _inGotFocus = !DisableSelectAllOnFocus;
            try
            {
                base.OnGotFocus(e);
            }
            finally
            {
                _inGotFocus = false;
            }
        }
        bool _inGotFocus;
        public override void SelectAll()
        {
            if (_inGotFocus)
                return;
            base.SelectAll();
        }

        #endregion

        #region Fonctionalité pour scroller le texte et pas juste faire bouger la scrollbar

        //[Obsolete("Ca ne marche pas encore")]
        void ScrollPosition_Initialize()
        {
            foreach (Control item in Controls)
            {
                if (!(item is VScrollBar bar))
                    continue;
                _vScrollBar = bar;
                _setVScrollBarValue = (Action<ScrollEventType, int>)_setScrollBarValueMethod.CreateDelegate(typeof(Action<ScrollEventType, int>), _vScrollBar);
                break;
            }
            foreach (Control item in Controls)
            {
                if (!(item is HScrollBar bar))
                    continue;
                _hScrollBar = bar;
                _setHScrollBarValue = (Action<ScrollEventType, int>)_setScrollBarValueMethod.CreateDelegate(typeof(Action<ScrollEventType, int>), _hScrollBar);
                break;
            }
        }
        VScrollBar _vScrollBar;
        HScrollBar _hScrollBar;
        static readonly MethodInfo _setScrollBarValueMethod = typeof(ScrollBarBase).GetMethod("SetScrollBarValue", BindingFlags.Instance | BindingFlags.NonPublic, Type.DefaultBinder, new [] { typeof(ScrollEventType), typeof(int) }, null);
        Action<ScrollEventType, int> _setVScrollBarValue;
        Action<ScrollEventType, int> _setHScrollBarValue;

         [Obsolete("Ca ne marche pas encore", true)]
        void SetPosition(Action<ScrollEventType, int> setScroll, int line)
        {
            int iDelta = line - _vScrollBar.Value;
	        ScrollEventType currentType = iDelta > 0 ? ScrollEventType.SmallIncrement : ScrollEventType.SmallDecrement;
	        iDelta = iDelta > 0 ? iDelta : -iDelta;
	        for (int i = 0; i <= iDelta - 1; i++)
                setScroll(currentType, currentType == ScrollEventType.SmallIncrement ? _vScrollBar.Value + _vScrollBar.SmallChange : _vScrollBar.Value - _vScrollBar.SmallChange);
        }
        [Obsolete("Ca ne marche pas encore", true)]
        public Tuple<int, int> GetScrollPosition()
        {
            return Tuple.Create(_vScrollBar.Value, _hScrollBar.Value);
        }
        [Obsolete("Ca ne marche pas encore", true)]
        public void SetScrollPosition(Tuple<int, int> positionLineCol)
        {
            BeginUpdate();
            try
            {
                _vScrollBar.Value = 0;
                _vScrollBar.Value = positionLineCol.Item1;
                SetPosition(_setVScrollBarValue, positionLineCol.Item1);
                _hScrollBar.Value = positionLineCol.Item2;
                SetPosition(_setHScrollBarValue, positionLineCol.Item2);
            }
            finally
            {
                EndUpdate();
            }
        }

        #endregion


        #region

        [Obsolete("Ca ne marche pas encore !", true)]
        [DefaultValue(false)]
        public bool ShowScrollbarsOnlyWhenMouseIsOver
        {
            get { return _ShowScrollbarsOnlyWhenMouseIsOver; }
            set
            {
                if (_ShowScrollbarsOnlyWhenMouseIsOver == value)
                    return;
                MouseEnter -= MemoMessage_MouseEnter;
                MouseLeave -= MemoMessage_MouseLeave;
                if (value)
                {
                    Properties.ScrollBars = ScrollBars.None;
                    MouseEnter += MemoMessage_MouseEnter;
                    MouseLeave += MemoMessage_MouseLeave;
                }
                _ShowScrollbarsOnlyWhenMouseIsOver = value;
                ShowScrollbarsOnlyWhenMouseIsOverChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        bool _ShowScrollbarsOnlyWhenMouseIsOver;
        public event EventHandler ShowScrollbarsOnlyWhenMouseIsOverChanged;


        public bool MouseIsOver
        {
            get { return _MouseIsOver; }
            private set
            {
                if (_MouseIsOver == value)
                    return;
                _MouseIsOver = value;
                MouseIsOverChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        bool _MouseIsOver;
        public event EventHandler MouseIsOverChanged;

        void MemoMessage_MouseEnter(object _, EventArgs __)
        {
            MouseIsOver = true;
        }

        void MemoMessage_MouseLeave(object _, EventArgs __)
        {
            MouseIsOver = false;
        }

        #endregion

        #region Fonctionalite pour afficher les scrollbars seulement quand nécessaire
                // From https://www.devexpress.com/Support/Center/Question/Details/Q403411

        void ShowScrollbarsOnlyWhenNeeded_Initialize()
        {
            ShowScrollbarsOnlyWhenMouseIsOverChanged += (_, __) => CheckIfScrollbarsNeedToBeDisplayed();
            MouseIsOverChanged += (_, __) => CheckIfScrollbarsNeedToBeDisplayed();
        }

        [DefaultValue(false)]
        public bool ShowScrollbarsOnlyWhenNeeded
        {
            get { return _ShowScrollbarsOnlyWhenNeeded; }
            set
            {
                _ShowScrollbarsOnlyWhenNeeded = value;
                TextChanged -= MemoMessage_TextChangedOrResize;
                SizeChanged -= MemoMessage_TextChangedOrResize;
                if (_ShowScrollbarsOnlyWhenNeeded)
                {
                    Properties.ScrollBars = ScrollBars.None;
                    TextChanged += MemoMessage_TextChangedOrResize;
                    SizeChanged += MemoMessage_TextChangedOrResize;
                    CheckIfScrollbarsNeedToBeDisplayed();
                }
            }
        }

        HScrollBar HScroll
        {
            get { return scrollHelper.HScroll; }
        }


        VScrollBar VScroll
        {
            get { return scrollHelper.VScroll; }
        }

        bool _ShowScrollbarsOnlyWhenNeeded;

        void CheckIfScrollbarsNeedToBeDisplayed()
        {
            if (DesignTimeHelper.IsInDesignMode) // empeche surtout le code invoké plus bas
                return;
            Graphics graphics = null;
            try // Ne pas utiliser ExceptionManager car si ca foire ça signifie que ExceptionManager throwera aussi
            {
                // Si MemoEdit est utilisé très tôt dans l'application (exemple : afficher une assertion avant même l'initialisation complete de la form principale)
                // Alors CreateGraphics peut thrower !
                // TODO : remplacer CreateGraphics par CreateHandle. Note : ca ne change rien au probleme ci dessus, c'est juste un peu plus propre
                graphics = this.CreateGraphics(); // Crée le handle pour "this" qui permet d'appeler BeginInvoke
            }
            catch
            {
                DebugTools.Break();
                // Ignored
            }

            if (this.IsHandleCreated)
            {
                this.BeginInvoke((Action) (() =>
                    {
                        SuspendLayout();
                        try
                        {
                            UpdateScrollBarVisibility(graphics);
                        }
                        finally
                        {
                            ResumeLayout();
                        }
                    }));
            }
        }

        // Code provenant de https://www.devexpress.com/Support/Center/Question/Details/Q435390
        void UpdateScrollBarVisibility(Graphics graphics)
        {
            var scrollBarsBefore = this.Properties.ScrollBars;
            //var pos = GetScrollPosition();
            //int start = SelectionStart;

            ScrollBars scrollBars;
            var vi = ViewInfo as MemoEditViewInfo;
            Debug.Assert(vi != null, "vi != null");
            var cache = new GraphicsCache(graphics);
            int h = ((IHeightAdaptable) vi).CalcHeight(cache, vi.MaskBoxRect.Width) + (HScroll.Visible ? HScroll.Height : 0);
            int defaultWidth = Screen.FromControl(this).WorkingArea.Width;
            int width = defaultWidth;
            try
            {
                // seems to not crash on big text
                // so we get an estimation first
                width = TextRenderer.MeasureText(Text, vi.Appearance.Font).Width;
                // and ask the real measure if possible
                if (width <= defaultWidth && !_gdiCrashed)
                    width = (int)graphics.MeasureString(Text, vi.Appearance.Font, 0, vi.Appearance.GetStringFormat()).Width;
            }
            catch // catch error about GDI+
            {
                _gdiCrashed = true;
            }
            width += VScroll.Visible ? VScroll.Width : 0;
            var args = new ObjectInfoArgs
            {
                Bounds = new Rectangle(0, 0, width, h)
            };
            Rectangle rect = vi.BorderPainter.CalcBoundsByClientRectangle(args);
            cache.Dispose();

            if (Properties.WordWrap)
            {
                scrollBars = rect.Height > Height ? ScrollBars.Vertical : ScrollBars.None;
                Properties.ScrollBars = scrollBars;
                scrollBars = !VScrollCanFocus() && scrollBars == ScrollBars.Vertical ? ScrollBars.None : Properties.ScrollBars;
            }
            else
            {
                scrollBars = rect.Height > Height ?
                                    (rect.Width > Width ? ScrollBars.Both : ScrollBars.Vertical) :
                                    (rect.Width > Width ? ScrollBars.Horizontal : ScrollBars.None);
                Properties.ScrollBars = scrollBars;
                scrollBars = !VScrollCanFocus() && scrollBars == ScrollBars.Vertical ? ScrollBars.None : Properties.ScrollBars;
                scrollBars = !HScrollCanFocus() && scrollBars == ScrollBars.Horizontal ? ScrollBars.None : Properties.ScrollBars;
                scrollBars = !VScrollCanFocus() && scrollBars == ScrollBars.Both ? ScrollBars.Horizontal : Properties.ScrollBars;
                scrollBars = !HScrollCanFocus() && scrollBars == ScrollBars.Both ? ScrollBars.Vertical : Properties.ScrollBars;
                scrollBars = !VScrollCanFocus() && !HScrollCanFocus() && scrollBars == ScrollBars.Both ? ScrollBars.None : Properties.ScrollBars;
            }

            if (scrollBarsBefore != scrollBars)
                Properties.ScrollBars = scrollBars;
        }
        bool _gdiCrashed;

        bool VScrollCanFocus()
        {
            return VScroll.CanFocus;
        }

        bool HScrollCanFocus()
        {
            return HScroll.CanFocus;
        }

        void MemoMessage_TextChangedOrResize(object _, EventArgs __)
        {
            CheckIfScrollbarsNeedToBeDisplayed();
        }

        #endregion Fonctionalite
    }
}
