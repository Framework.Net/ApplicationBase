﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;

using DevExpress.Utils.Controls;
using DevExpress.XtraEditors;

using TechnicalTools.Diagnostics;
using TechnicalTools.UI.DX.Forms;

namespace TechnicalTools.UI.DX.Controls
{
    [ToolboxItem(true)]
    public partial class AsyncCheckValidatorImage : PictureEdit, IXtraResizableControl//, IControlAccessibility
    {
        public AsyncCheckValidatorImage()
        {
            _resources = new ComponentResourceManager(typeof(AsyncCheckValidatorImage));
            MinimumSize = new Size(32, 32);
            MaximumSize = new Size(32, 32);
            if (!DesignTimeHelper.IsInDesignMode)
            {
                if (EditValue == null)
                    EditValue = _resources.GetObject("processing_32x32");
                if (MinimumSize == Size.Empty)
                    MinimumSize = new Size(32, 32);
                if (MaximumSize == Size.Empty)
                    MaximumSize = new Size(32, 32);
                MouseUp += OnMouseUp_ShowForm;
            }

        }
        readonly ComponentResourceManager _resources;

        public Task Validate(Action<CancellationToken> validateAction)
        {
            var cancelToken = _cancelToken; // capture current state
            if (cancelToken != null)
                ExceptionManager.Instance.IgnoreException(cancelToken.Cancel); // Ignore exception if just disposed
            EditValue = _resources.GetObject("processing_32x32");
            ToolTip = "Processing validation... Please wait";

            // Rebuild task and cancellation token
            cancelToken = new CancellationTokenSource();
            void asyncCode()
            {
                Exception result = null;
                try
                {
                    validateAction(cancelToken.Token);
                }
                catch (Exception ex)
                {
                    result = ex;
                    throw;
                }
                finally
                {
                    if (!cancelToken.IsCancellationRequested && !IsDisposed && !IsDisposing)
                        ExceptionManager.Instance.IgnoreException(() => BeginInvoke((Action)(() =>
                        {
                            cancelToken.Dispose();
                            if (cancelToken.IsCancellationRequested || IsDisposed || IsDisposing)
                                return;
                            Interlocked.CompareExchange(ref _cancelToken, null, cancelToken);
                            EditValue = result == null
                                      ? _resources.GetObject("valid_32x32")
                                      : _resources.GetObject("bad_32x32");
                            ToolTip = result?.Message;
                        })));
                    else
                    {
                        cancelToken.Dispose();
                        Interlocked.CompareExchange(ref _cancelToken, null, cancelToken);
                    }
                    _lastException = result;
                }
            }

            var task = _task == null // if another task already exist, we add new validation after, so _tas.ContinueWith call by calling developper are sequential
                    ? Task.Factory.StartNew(     asyncCode,   cancelToken.Token,     TaskCreationOptions.LongRunning, TaskScheduler.Default)
                    :    _task.ContinueWith(_ => asyncCode(), cancelToken.Token, TaskContinuationOptions.LongRunning, TaskScheduler.Default);

            // Save new state
            _cancelToken = cancelToken;
            _task = task;

            // return local built task
            return task;
        }
        volatile CancellationTokenSource _cancelToken = new CancellationTokenSource(); // NOSONAR we handle Dispose through task...
        Task _task;
        Exception _lastException;

        protected virtual void OnMouseUp_ShowForm(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (_lastException == null)
                return;
            var parentForm = FindForm();
            var memoForm = new MemoForm(_lastException.Message)
            {
                Text = "Errors",
                Size = new Size(parentForm.Width * 80 / 100, parentForm.Height * 80 / 100)
            };
            memoForm.Show(this);
        }


        #region IControlAccessibility Members

        [DefaultValue(true)]
        public bool IsCurrentlyMeaningful
        {
            get { return _IsCurrentlyMeaningful; }
            set
            {
                _IsCurrentlyMeaningful = value;
                if (!DesignTimeHelper.IsInDesignMode)
                    base.Enabled = _IsCurrentlyMeaningful;
            }
        }
        bool _IsCurrentlyMeaningful = true;

        [DefaultValue(true)]
        public bool EditableByUser
        {
            get { return _EditableByUser; }
            set
            {
                _EditableByUser = value;
                if (!DesignTimeHelper.IsInDesignMode)
                    base.ReadOnly = !_EditableByUser;
            }
        }
        bool _EditableByUser = true;

        [Obsolete("Utilisez IsCurrentlyMeaningful ou EditableByUser", true)]
        public new bool Enabled
        {
            get { return base.Enabled; }
            set { base.Enabled = value; }
        }

        #endregion
    }
}
