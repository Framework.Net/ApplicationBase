﻿using System;

using DevExpress.Data.Filtering.Helpers;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Filtering;

using DevExpress.Data.Filtering;
using DevExpress.Data;

namespace TechnicalTools.UI.DX.Controls
{
    // Voir aussi : https://www.devexpress.com/Support/Center/Question/Details/Q259821/filtercontrol-set-default-value
    // Provide event allowing to customize default operand (refactored from https://www.devexpress.com/Support/Center/Question/Details/Q361153/how-to-the-lookupedit-default-value-in-filtercontrol)
    // yourFilterControl.PopulateAdditionalOperands += (object sender, PopulateAdditionalOperandsEventArgs e)
    // {
    //    switch (e.Property.Name)
    //    {
    //        case "Field1": e.Values[0] = "A";    break;
    //        case "Field2": e.Values[1] = 1;      break;
    //    }
    // }
    public class MappedWinFilterTreeNodeModel : WinFilterTreeNodeModel
    {
        // Only if base type is FilterEditor
        public MappedWinFilterTreeNodeModel(MappedFilterControl control)
            : base(control) { }
        [Obsolete("For Compilation only (because i am testing FilterControl / FilterEditorcontrol difference at  runtime", true)]
        public MappedWinFilterTreeNodeModel(FilterControl control)
            : base(control) { }

        // Only if base type is FilterEditor
        public new MappedFilterControl Control
        {
            get { return (MappedFilterControl)base.Control; }
        }

        // Only if base type is FilterEditor
        protected override ClauseNode CreateDefaultClauseNode(IBoundProperty property)
        {
            ClauseNode result = base.CreateDefaultClauseNode(property);
            result.Operation = Control.GetDefaultOperationCore(result.Property, result.Operation);
            return result;
        }

        public override ClauseNode CreateClauseNode()
        {
            return new MappedClauseNode(this);
        }
        // Only if base type is FilterEditor
        protected override void ValidateAdditionalOperands(IClauseNode node)
        {
            base.ValidateAdditionalOperands(node);
            object[] values = Control.GetAdditionalOperandsValues(FilterProperties.GetProperty(node.FirstOperand),
                                                                  node.Operation, node.AdditionalOperands.Count);
            for (int i = 0; i < node.AdditionalOperands.Count; i++)
                node.AdditionalOperands[i] = new OperandValue(values[i]);
        }
    }

    public partial class MappedFilterControl
    {
        // TODO : (etude) A voir aussi  https://www.devexpress.com/Support/Center/Question/Details/Q451620/how-to-initialize-item-shown-in-filtercontrol
        // Only if base type is FilterEditor
        protected override WinFilterTreeNodeModel CreateModel()
        {
            return new MappedWinFilterTreeNodeModel(this);
        }

        public event EventHandler<PopulateAdditionalOperandsEventArgs> PopulateAdditionalOperands
        {
            add { Events.AddHandler(fPopulateAdditionalOperands, value); }
            remove { Events.RemoveHandler(fPopulateAdditionalOperands, value); }
        }
        static readonly object fPopulateAdditionalOperands = new object();

        internal object[] GetAdditionalOperandsValues(IBoundProperty property, ClauseType operation, int operandsCount)
        {
            if (!(Events[fPopulateAdditionalOperands] is EventHandler<PopulateAdditionalOperandsEventArgs> handler))
                return new object[operandsCount];
            var args = new PopulateAdditionalOperandsEventArgs(property, operation, operandsCount);
            handler(this, args);
            return args.Values;
        }
    }

    public class PopulateAdditionalOperandsEventArgs : EventArgs
    {
        public PopulateAdditionalOperandsEventArgs(IBoundProperty property, ClauseType operation, int operandsCount)
        {
            Property = property;
            Operation = operation;
            Values = new object[operandsCount];
        }

        public object[] Values { get; private set; }
        public IBoundProperty Property { get; private set; }
        public ClauseType Operation { get; private set; }
    }
}
