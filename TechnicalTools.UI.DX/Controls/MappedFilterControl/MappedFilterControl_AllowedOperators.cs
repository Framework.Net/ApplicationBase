﻿using System;
using System.Collections.Generic;
using System.Linq;

using DevExpress.Data.Filtering.Helpers;
using DevExpress.XtraEditors.Filtering;

using TechnicalTools;
using TechnicalTools.Diagnostics;

using DataMapper;


namespace TechnicalTools.UI.DX.Controls
{
    // Operator are represented by ClauseType in DevExpress.
    // and FilterColumnClauseClass represent some set of ClauseType
    // The documentation explaining what ClauseType item all the set contains seems to not exist for winform
    // (but already found for Apsx which seems to be the same thing anyway)
    public partial class MappedFilterControl
    {
        void ClauseTypeManagement()
        {
            // Only if base type is FilterEditor
            GetDefaultOperation += ClauseTypeManagement_GetDefaultOperation;
        }

        // Make Devexpress behavior more intuitive for developper
        private void ClauseTypeManagement_GetDefaultOperation(object sender, GetDefaultOperationEventArgs e)
        {
            var allowed = GetTypeRelatedClauseTypeSet(e.OperandProperty.Type);
            if (allowed.Any())
                e.ClauseType = allowed.First();
            else
                DebugTools.Assert(false, "Unreachable code ?");
        }

        // Indicate default set of clauses allowed for a type using DX standard definition.
        // The set must contains all the clause allowed
        // It can also contains some clause not allowed (wich will be removed later).
        // For example ClauseType.IsNull belongs too set FilterColumnClauseClass.Lookup
        // Later when we will remove ClauseType.IsNull if propertyType is a not nullable Type
        public static FilterColumnClauseClass GetTypeRelatedClauseClass(Type propertyType)
        {
            #region  Framework related

            if (typeof(DynamicEnum).IsAssignableFrom(propertyType))
                return FilterColumnClauseClass.Lookup;
            if (typeof(TechnicalTools.Model.IStaticEnum).IsAssignableFrom(propertyType))
                return FilterColumnClauseClass.Lookup;

            #endregion  Framework related

            #region Default Type

            if (typeof(string) == propertyType)
                return FilterColumnClauseClass.String;
            if ((propertyType.TryGetNullableType() ?? propertyType).IsNumericType())
                return FilterColumnClauseClass.Generic;
            if ((propertyType.TryGetNullableType() ?? propertyType).IsEnum)
                return FilterColumnClauseClass.Lookup;
            if ((propertyType.TryGetNullableType() ?? propertyType) == typeof(DateTime))
                return FilterColumnClauseClass.DateTime;
            if ((propertyType.TryGetNullableType() ?? propertyType) == typeof(TimeSpan))
                return FilterColumnClauseClass.DateTime;
            if ((propertyType.TryGetNullableType() ?? propertyType) == typeof(char))
                return FilterColumnClauseClass.Lookup;
            if ((propertyType.TryGetNullableType() ?? propertyType) == typeof(bool))
                return FilterColumnClauseClass.Lookup;

            // All Other reference type
            return FilterColumnClauseClass.Blob;

            #endregion Default Type

            //return FilterColumnClauseClass.Generic;
        }
        // Indicate precise default set of clause allowed for a type
        public static HashSet<ClauseType> GetTypeRelatedClauseTypeSet(Type propertyType)
        {
            // TODO : Create static lazy dictionary Type => hashSet
            //        and fill it with same set as default result, but with specific remove like :
            //        ClauseType.isNull hen type is not nullable

            // Default result
            return OperatorByClass[GetTypeRelatedClauseClass(propertyType)];
        }

        // Customize default clause
        // Only if base type is FilterEditor
        protected override void RaisePopupMenuShowing(DevExpress.XtraEditors.Filtering.PopupMenuShowingEventArgs e)
        {
            if (e.MenuType == FilterControlMenuType.Clause)
            {
                ClauseNode node = e.CurrentNode as ClauseNode;
                string fieldName = node.FirstOperand.PropertyName;
                var col = FilterColumns[fieldName];
                if (col == null) // Another developper could have customized.. so column may not exist
                    return;
                var allowed = GetTypeRelatedClauseTypeSet(col.ColumnType);

                // (Code inter dependant with GetPropertiesAsColumns, look for comment)
                // Because i dont know how to add missing ClauseType in e.Menu.Items and be OK with devexpress internal code
                // All the good clausetype are expected to be in e.Menu.Items and we remove the not good one
                for (int i = e.Menu.Items.Count - 1; i >= 0; i--)
                {
                    // From https://www.devexpress.com/Support/Center/Question/Details/T302358/filtercontrol-how-to-remove-bounded-items-from-the-popup-menu
                    if (!allowed.Contains((ClauseType)e.Menu.Items[i].Tag))
                        e.Menu.Items.RemoveAt(i);
                }

                // Doc for later : To check some other thing  (later)
                // if (e.MenuType == FilterControlMenuType.Group)
                //    find item with e.Menu.Items[i].Caption == Localizer.Active.GetLocalizedString(StringId.FilterGroupNotAnd)
            }
            //else
            //    DebugTools.Assert(false, "Just to know when this case happens, there is no error");

            base.RaisePopupMenuShowing(e);
        }

        static readonly Dictionary<ClauseType, HashSet<FilterColumnClauseClass>> ClassesByOperator = new Dictionary<ClauseType, HashSet<FilterColumnClauseClass>>
            {
                { ClauseType.Equals            , new HashSet<FilterColumnClauseClass> { FilterColumnClauseClass.Generic, FilterColumnClauseClass.DateTime, FilterColumnClauseClass.String, FilterColumnClauseClass.Lookup,                              } },
                { ClauseType.DoesNotEqual      , new HashSet<FilterColumnClauseClass> { FilterColumnClauseClass.Generic, FilterColumnClauseClass.DateTime, FilterColumnClauseClass.String, FilterColumnClauseClass.Lookup,                              } },
                { ClauseType.Greater           , new HashSet<FilterColumnClauseClass> { FilterColumnClauseClass.Generic, FilterColumnClauseClass.DateTime, FilterColumnClauseClass.String,                                                              } },
                { ClauseType.GreaterOrEqual    , new HashSet<FilterColumnClauseClass> { FilterColumnClauseClass.Generic, FilterColumnClauseClass.DateTime, FilterColumnClauseClass.String,                                                              } },
                { ClauseType.Less              , new HashSet<FilterColumnClauseClass> { FilterColumnClauseClass.Generic, FilterColumnClauseClass.DateTime, FilterColumnClauseClass.String,                                                              } },
                { ClauseType.LessOrEqual       , new HashSet<FilterColumnClauseClass> { FilterColumnClauseClass.Generic, FilterColumnClauseClass.DateTime, FilterColumnClauseClass.String,                                                              } },
                { ClauseType.Between           , new HashSet<FilterColumnClauseClass> { FilterColumnClauseClass.Generic, FilterColumnClauseClass.DateTime, FilterColumnClauseClass.String,                                                              } },
                { ClauseType.NotBetween        , new HashSet<FilterColumnClauseClass> { FilterColumnClauseClass.Generic, FilterColumnClauseClass.DateTime, FilterColumnClauseClass.String,                                                              } },
                { ClauseType.Contains          , new HashSet<FilterColumnClauseClass> {                                                                    FilterColumnClauseClass.String,                                                              } },
                { ClauseType.DoesNotContain    , new HashSet<FilterColumnClauseClass> {                                                                    FilterColumnClauseClass.String,                                                              } },
                { ClauseType.BeginsWith        , new HashSet<FilterColumnClauseClass> {                                                                    FilterColumnClauseClass.String,                                                              } },
                { ClauseType.EndsWith          , new HashSet<FilterColumnClauseClass> {                                                                    FilterColumnClauseClass.String,                                                              } },
                { ClauseType.Like              , new HashSet<FilterColumnClauseClass> {                                                                    FilterColumnClauseClass.String,                                                              } },
                { ClauseType.NotLike           , new HashSet<FilterColumnClauseClass> {                                                                    FilterColumnClauseClass.String,                                                              } },
                { ClauseType.IsNull            , new HashSet<FilterColumnClauseClass> { FilterColumnClauseClass.Generic, FilterColumnClauseClass.DateTime,                                 FilterColumnClauseClass.Lookup, FilterColumnClauseClass.Blob } },
                { ClauseType.IsNotNull         , new HashSet<FilterColumnClauseClass> { FilterColumnClauseClass.Generic, FilterColumnClauseClass.DateTime,                                 FilterColumnClauseClass.Lookup, FilterColumnClauseClass.Blob } },
                { ClauseType.AnyOf             , new HashSet<FilterColumnClauseClass> { FilterColumnClauseClass.Generic, FilterColumnClauseClass.DateTime, FilterColumnClauseClass.String, FilterColumnClauseClass.Lookup,                              } },
                { ClauseType.NoneOf            , new HashSet<FilterColumnClauseClass> { FilterColumnClauseClass.Generic, FilterColumnClauseClass.DateTime, FilterColumnClauseClass.String, FilterColumnClauseClass.Lookup,                              } },
                { ClauseType.IsNullOrEmpty     , new HashSet<FilterColumnClauseClass> {                                                                    FilterColumnClauseClass.String,                                                              } },
                { ClauseType.IsNotNullOrEmpty  , new HashSet<FilterColumnClauseClass> {                                                                    FilterColumnClauseClass.String,                                                              } },
                { ClauseType.IsBeyondThisYear  , new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
                { ClauseType.IsLaterThisYear   , new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
                { ClauseType.IsLaterThisMonth  , new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
                { ClauseType.IsNextWeek        , new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
                { ClauseType.IsLaterThisWeek   , new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
                { ClauseType.IsTomorrow        , new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
                { ClauseType.IsToday           , new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
                { ClauseType.IsYesterday       , new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
                { ClauseType.IsEarlierThisWeek , new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
                { ClauseType.IsLastWeek        , new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
                { ClauseType.IsEarlierThisMonth, new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
                { ClauseType.IsEarlierThisYear , new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
                { ClauseType.IsPriorThisYear   , new HashSet<FilterColumnClauseClass> {                                  FilterColumnClauseClass.DateTime,                                                                                              } },
            };
        static readonly Dictionary<FilterColumnClauseClass, HashSet<ClauseType>> OperatorByClass =
            ClassesByOperator.SelectMany(opClasses => opClasses.Value.Select(opClass => Tuple.Create(opClasses.Key, opClass)))
                             .GroupBy(t => t.Item2)
                             .ToDictionary(grp => grp.Key, grp => new HashSet<ClauseType>(grp.Select(t => t.Item1)));
    }
}
