﻿using System;
using System.Collections.Generic;
using System.Linq;

using DevExpress.Data.Filtering;

using TechnicalTools.Model;

using DataMapper;


namespace TechnicalTools.UI.DX.Controls
{
    // TODO : when DX version >= 18.1, read : https://www.devexpress.com/Support/Center/Question/Details/T320172/how-to-traverse-through-and-modify-the-criteriaoperator-instances
    // https://documentation.devexpress.com/CoreLibraries/4928/DevExpress-Data-Library/Criteria-Language-Syntax
    public class FilterCriteriaToGridVisitor : ICriteriaVisitor<object>, IClientCriteriaVisitor<object>
    {
        public static Tuple<IDictionary<string, object>, List<string>> ConvertToPropertyConditionDictionary(CriteriaOperator op, Type type)
        {
            var transformer = new FilterCriteriaToGridVisitor(type);
            var code = op.Accept(transformer);
            return Tuple.Create(transformer._result, transformer._Errors.ToList());
        }

        private FilterCriteriaToGridVisitor(Type type)
        {
            _type = type;
        }
        readonly Type _type;
        readonly List<string> _Errors = new List<string>();
        IDictionary<string, object> _result = new Dictionary<string, object>();


        #region ICriteriaVisitor Members

        object ICriteriaVisitor<object>.Visit(FunctionOperator theOperator)
        {
            foreach (CriteriaOperator operand in theOperator.Operands)
                operand.Accept(this);
            return null;
        }

        object ICriteriaVisitor<object>.Visit(GroupOperator theOperator)
        {
            foreach (CriteriaOperator operand in theOperator.Operands)
                operand.Accept(this);
            return null;
        }

        object ICriteriaVisitor<object>.Visit(InOperator theOperator)
        {
            var op = theOperator.LeftOperand as OperandProperty;
            if (ReferenceEquals(op, null))
                _Errors.Add("Dont know how to handle case where InOperator has not a an OperandProperty type at left");
            else if (theOperator.Operands.Any(ov => !(ov is OperandValue)))
                _Errors.Add("Dont know how to handle case where InOperator has not a operand which is expression");
            else
            {
                var values = theOperator.Operands.Select(ov => GetOperandValue(ov as OperandValue));
                _result.Add(op.PropertyName, values.Join(" OR "));
            }
            return null;
        }

        object ICriteriaVisitor<object>.Visit(UnaryOperator theOperator)
        {
            _Errors.Add("Unary operator not handled!");
            return null;
        }

        object ICriteriaVisitor<object>.Visit(BinaryOperator theOperator)
        {
            var op = theOperator.LeftOperand as OperandProperty;
            var ov = theOperator.RightOperand as OperandValue;
            if (ReferenceEquals(op, null))
                _Errors.Add("Dont know how to handle case where BinaryOperator has not a an OperandProperty type at left");
            else if (ReferenceEquals(ov, null))
                _Errors.Add("Dont know how to handle case where BinaryOperator has not a an OperandValue type at right");
            else
            {
                string opt = GetBinaryOperationName(theOperator.OperatorType);
                string right = GetOperandValue(ov);
                _result.Add(op.PropertyName, string.IsNullOrWhiteSpace(opt) ? right : opt + " " + right);
            }
            return null;
        }

        object ICriteriaVisitor<object>.Visit(BetweenOperator theOperator)
        {
            var res = FilterCriteriaToBeautifulExpressionVisitor.Convert(theOperator.TestExpression, _type);

            theOperator.BeginExpression.Accept(this);
            theOperator.EndExpression.Accept(this);
            return null;
        }


        object ICriteriaVisitor<object>.Visit(OperandValue theOperand)
        {
            return null;
        }

        static string GetOperandValue(OperandValue operand)
        {
            if (operand.Value == null)
                return "<Empty>";
            if (operand.Value is bool b)
                return b.ToString().ToLowerInvariant();
            if (operand.Value.GetType().IsNumericType())
                return operand.Value.ToStringInvariant();
            if (operand.Value is TimeSpan ts)
                return ts.ToHumanReadableShortNotation();
            if (operand.Value is DateTime dt)
            {
                if (dt.Date == dt)
                    return dt.ToString("d");
                if (dt.Second == 0 && dt.Millisecond == 0)
                    return dt.ToString("dd/MM/yyy HH:mm");
                return dt.ToString();
            }

            if (operand.Value is Enum e)
                return e.GetDescription();
            if (operand.Value is string str)
                return "\""  // Should be more complex but we will never need other cases.
                     + str.Replace(@"\", @"\\").Replace("\"", "\\\"")
                     + "\"";
            if (operand.Value is DynamicEnum de)
                return "\"" + de.Caption + "\"";
            if (operand.Value is IStaticEnum se)
                return "\"" + se.ToString() + "\"";
            return null;
        }


        #endregion

        #region IClientCriteriaVisitor Members

        object IClientCriteriaVisitor<object>.Visit(OperandProperty theOperand)
        {
            return null;
        }

        object IClientCriteriaVisitor<object>.Visit(AggregateOperand theOperand)
        {
            throw new NotImplementedException();
        }

        object IClientCriteriaVisitor<object>.Visit(JoinOperand theOperand)
        {
            throw new NotImplementedException();
        }

        #endregion

        string GetGroupOperatorAsCode(GroupOperatorType operatorType)
        {
            switch (operatorType)
            {
                case GroupOperatorType.And: return " && ";
                case GroupOperatorType.Or: return " || ";
                default:
                    _Errors.Add("Don't know how to handle group operator " + operatorType.ToString() + "!");
                    return operatorType.ToString();
            }
        }


        string GetFunctionName(FunctionOperatorType operatorType)
        {
            switch (operatorType)
            {
                case FunctionOperatorType.Concat: return "Concat";
                case FunctionOperatorType.Iif: return "Iif";
                case FunctionOperatorType.IsNull: return "Is null";
                case FunctionOperatorType.StartsWith: return nameof(string.StartsWith); // caller have to handle the serialization of node in the good order
                case FunctionOperatorType.EndsWith: return nameof(string.EndsWith); // Idem
                default: // TODO !
                    _Errors.Add("Don't know how to handle group operator " + operatorType.ToString() + "!");
                    return operatorType.ToString();
            }
        }

        string GetUnaryOperationName(UnaryOperatorType operatorType)
        {
            switch (operatorType)
            {
                case UnaryOperatorType.BitwiseNot: return "~";
                case UnaryOperatorType.IsNull: return "null == ";
                case UnaryOperatorType.Minus: return "-";
                case UnaryOperatorType.Not: return "!";
                case UnaryOperatorType.Plus: return "+";
                default: return string.Empty;
            }
        }

        string GetBinaryOperationName(BinaryOperatorType operatorType)
        {
            switch (operatorType)
            {
                case BinaryOperatorType.Equal: return ""; // Empty because we consider this operator the default one
                case BinaryOperatorType.NotEqual: return "Is not";
                case BinaryOperatorType.Greater: return ">";
                case BinaryOperatorType.Less: return "<";
                case BinaryOperatorType.LessOrEqual: return "<=";
                case BinaryOperatorType.GreaterOrEqual: return ">=";
                case BinaryOperatorType.BitwiseAnd: return "&";
                case BinaryOperatorType.BitwiseOr: return "|";
                case BinaryOperatorType.BitwiseXor: return "^";
                case BinaryOperatorType.Divide: return "/";
                case BinaryOperatorType.Modulo: return "%";
                case BinaryOperatorType.Multiply: return "*";
                case BinaryOperatorType.Plus: return "+";
                case BinaryOperatorType.Minus: return "-";
                default:
                    if (operatorType.ToString() == "Like") //BinaryOperatorType.Like // because is marked obsolete (dont remember the pragma...)
                        _Errors.Add("Like Operator not handled!");
                    else
                        _Errors.Add("Unknown operator " + operatorType.ToString() + "!");
                    return " ";
            }
        }

    }

}
