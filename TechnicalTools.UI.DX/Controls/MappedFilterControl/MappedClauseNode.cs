﻿using System;
using DevExpress.Data;
using DevExpress.Data.Filtering;
using DevExpress.Data.Filtering.Helpers;
using DevExpress.XtraEditors.Filtering;


namespace TechnicalTools.UI.DX.Controls
{
    // Provide event allowing to customize default operator (refactored from https://www.devexpress.com/Support/Center/Question/Details/Q361153/how-to-the-lookupedit-default-value-in-filtercontrol)
    // Exemple of use :
    // yourFilterControl.GetDefaultOperation += (object sender, GetDefaultOperationEventArgs e)
    // {
    //    switch (e.OperandProperty.Name)
    //    {
    //        case "Field1": e.ClauseType = ClauseType.Contains; break;
    //        case "Field2": e.ClauseType = ClauseType.Greater; break;
    //    }
    // }

    public class MappedClauseNode : ClauseNode
    {
        public MappedClauseNode(FilterTreeNodeModel model)
            : base(model)
        { }

        protected override void ClauseNodeFirstOperandChanged(OperandProperty newProp, int elementIndex)
        {
            base.ClauseNodeFirstOperandChanged(newProp, elementIndex);
            var model = (WinFilterTreeNodeModel)Model;
            // Only if base type is FilterEditor
            var ctl = (MappedFilterControl)model.Control;
            Operation = ctl.GetDefaultOperationCore(FilterProperties.GetProperty(newProp), Operation);
        }
    }
    public partial class MappedFilterControl
    {
        public event EventHandler<GetDefaultOperationEventArgs> GetDefaultOperation
        {
            add     { Events.AddHandler(fGetDefaultOperation, value); }
            remove  { Events.RemoveHandler(fGetDefaultOperation, value); }
        }
        static readonly object fGetDefaultOperation = new object();

        internal ClauseType GetDefaultOperationCore(IBoundProperty property, ClauseType operation)
        {
            GetDefaultOperationEventArgs args = new GetDefaultOperationEventArgs(property, operation);
            RaiseGetDefaultOperation(args);
            return args.ClauseType;
        }
        void RaiseGetDefaultOperation(GetDefaultOperationEventArgs args)
        {
            (Events[fGetDefaultOperation] as EventHandler<GetDefaultOperationEventArgs>)?.Invoke(this, args);
        }
    }
    public class GetDefaultOperationEventArgs : EventArgs
    {
        public GetDefaultOperationEventArgs(IBoundProperty property, ClauseType operation)
        {
            OperandProperty = property;
            ClauseType = operation;
        }

        public IBoundProperty OperandProperty { get; private set; }
        public ClauseType ClauseType { get; set; }
    }
}
