﻿using System;
using System.Drawing;

using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.ViewInfo;


namespace TechnicalTools.UI.DX.Controls
{
    // Complex calculator
    public class EnhancedRibbonPanelLayoutCalculator : RibbonPanelComplexLayoutCalculator
    {
        public EnhancedRibbonPanelLayoutCalculator(RibbonPanelViewInfo panelInfo)
            : base(panelInfo)
        {

        }

        public override void UpdatePanelLayout()
        {
            int xPos = ContentBounds.Left - PanelInfo.PanelScrollOffset;
            int rAx = 0;
            for (int i = 0; i < PanelInfo.Groups.Count; i++)
            {
                if (!(PanelInfo.Groups[i] is EnhancedRibbonPageGroupViewInfo rpgvi))
                    continue; // Happens with DX > 16.1.5 (ex: 17.1.11) with type DevExpress.XtraBars.Ribbon.ViewInfo.RibbonDesignTimePageGroupViewInfo at design time
                int temp = rpgvi.NewCalcViewInfo(new Rectangle(xPos, ContentBounds.Top, PanelInfo.Groups[i].PrecalculatedWidth, ContentBounds.Height), rAx);

                RibbonPageGroup pageGroup = PanelInfo.Groups[i].PageGroup;

                if (object.Equals(pageGroup.Tag, "AlignRight"))
                {
                    rAx = temp;
                    continue;
                }

                xPos += PanelInfo.Groups[i].PrecalculatedWidth + PanelInfo.DefaultIndentBetweenGroups;
            }
        }
    }
}
