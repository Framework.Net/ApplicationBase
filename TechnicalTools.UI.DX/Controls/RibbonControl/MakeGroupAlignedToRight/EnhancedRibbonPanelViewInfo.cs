﻿// Developer Express Code Central Example:
// How to align a RibbonPageGroup to the right side of a page?
//
// This example demonstrates how to create a custom RibbonControl that displays a
// group with the "AlignRight" tag assigned at the right side.
//
// You can find sample updates and versions for different programming languages here:
// http://www.devexpress.com/example=E2869

using System;

using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.ViewInfo;


namespace TechnicalTools.UI.DX.Controls
{
    public class EnhancedRibbonPanelViewInfo : RibbonPanelViewInfo
    {
        public EnhancedRibbonPanelViewInfo(RibbonViewInfo viewInfo)
            : base(viewInfo)
        {

        }

        protected override RibbonPageGroupViewInfo CreateGroupViewInfo(RibbonPageGroup group)
        {
            return new EnhancedRibbonPageGroupViewInfo(ViewInfo, group);
        }

        protected override RibbonPanelLayoutCalculator CreatePanelLayoutCalculator()
        {
            return new EnhancedRibbonPanelLayoutCalculator(this);
        }

    }
}
