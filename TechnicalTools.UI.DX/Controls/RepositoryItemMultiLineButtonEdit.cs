﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Reflection;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraEditors.Controls;
using System.ComponentModel;
using DevExpress.Utils;
using DevExpress.Utils.Drawing;
using System.Windows.Forms;
using TechnicalTools.Diagnostics;

namespace TechnicalTools.UI.DX.Controls
{
    // Developer Express Code Central Example:
    // How to create a ButtonEdit descendant that can show a multi-line text like MemoEdit
    //
    // This example shows how to create and use a ButtonEdit descendant that will
    // support multi-line text like MemoEdit.
    //
    // This descendant's repository items have three additional
    // properties:
    //   AcceptsReturn : specify whether return characters can be inserted into text (default true).
    //   AcceptsTab    : specify whether a user can insert tab characters into text (default false).
    //   ScrollBars    : specify which scrollbars are displayed (default : none).
    // See also:
    // Custom Editors
    // (https://documentation.devexpress.com/#WindowsForms/CustomDocument4716).
    // How to
    // create a ButtonEdit descendant that can show a multi-line text like MemoEdit
    // (https://www.devexpress.com/Support/Center/Question/Details/KA18954)
    //
    // You can find sample updates and versions for different programming languages here:
    // http://www.devexpress.com/example=E5150

    [UserRepositoryItem(nameof(RegisterMultiLineButtonEdit))]
    [DesignTimeVisible(true)]
    [DesignerCategory("TechnicalTools.UI.DX")]
    public class RepositoryItemMultiLineButtonEdit : RepositoryItemButtonEdit
    {
        bool _acceptsTab;
        bool _acceptsReturn;
        int _linesCount;
        ScrollBars _scrollBars;
        public static readonly string MultiLineButtonEditName = "MultiLineButtonEdit";

        static RepositoryItemMultiLineButtonEdit() { RegisterMultiLineButtonEdit(); }

        public RepositoryItemMultiLineButtonEdit()
        {
            this._acceptsTab = false;
            this._acceptsReturn = true;
            this._linesCount = 0;
            this._scrollBars = ScrollBars.None;
        }

        [Browsable(false)]
        protected override bool UseMaskBox { get { return false; } }
        [Browsable(false)]
        public override bool AutoHeight { get { return false; } }

        public override string EditorTypeName { get { return MultiLineButtonEditName; } }

        public static void RegisterMultiLineButtonEdit()
        {
            Image img = null;
            ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
            {
                img = Bitmap.FromStream(typeof(MultiLineButtonEdit).Assembly.GetManifestResourceStream("TechnicalTools.UI.DX.Controls.RepositoryItemMultiLineButtonEdit.bmp"));
            });
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(MultiLineButtonEditName,
            typeof(MultiLineButtonEdit),
            typeof(RepositoryItemMultiLineButtonEdit),
            typeof(MultiLineButtonEditViewInfo),
            new ButtonEditPainter(),
            true,
            img,
            typeof(DevExpress.Accessibility.ButtonEditAccessible)));
        }

        [DefaultValue(false)]
        public bool AcceptsTab
        {
            get { return _acceptsTab; }
            set
            {
                if (AcceptsTab == value) return;
                _acceptsTab = value;
                OnPropertiesChanged();
            }
        }

        [DefaultValue(true)]
        public bool AcceptsReturn
        {
            get { return _acceptsReturn; }
            set
            {
                if (AcceptsReturn == value) return;
                _acceptsReturn = value;
                OnPropertiesChanged();
            }
        }

        [Browsable(false)]
        public int LinesCount
        {
            get { return _linesCount; }
            set
            {
                if (value < 1) value = 0;
                if (LinesCount == value) return;
                _linesCount = value;
                OnPropertiesChanged();
            }
        }

        public ScrollBars ScrollBars
        {
            get { return _scrollBars; }
            set
            {
                if (ScrollBars == value) return;
                _scrollBars = value;
                OnPropertiesChanged();
            }
        }

        public override void Assign(RepositoryItem item)
        {
            RepositoryItemMultiLineButtonEdit source = item as RepositoryItemMultiLineButtonEdit;
            BeginUpdate();
            try
            {
                base.Assign(item);
                if (source == null) return;
                this._acceptsReturn = source.AcceptsReturn;
                this._acceptsTab = source.AcceptsTab;
                this._scrollBars = source.ScrollBars;
                this._linesCount = source.LinesCount;
            }
            finally
            {
                EndUpdate();
            }
        }

        protected override bool NeededKeysContains(Keys key)
        {
            if (AcceptsReturn && key == Keys.Enter)
                return true;
            if (AcceptsTab && key == Keys.Tab)
                return true;
            if (key == Keys.Up)
                return true;
            if (key == Keys.Down)
                return true;
            return base.NeededKeysContains(key);
        }

        protected override bool IsNeededKeyCore(Keys keyData)
        {
            if (keyData == (Keys.Enter | Keys.Control)) return false;
            bool res = base.IsNeededKeyCore(keyData);
            if (res) return true;
            if (keyData == Keys.PageUp || keyData == Keys.PageDown) return true;
            return false;
        }
    }

    public class MultiLineButtonEditViewInfo : ButtonEditViewInfo, IHeightAdaptable
    {

        public MultiLineButtonEditViewInfo(RepositoryItem item) : base(item) { }
        public new RepositoryItemMultiLineButtonEdit Item { get { return base.Item as RepositoryItemMultiLineButtonEdit; } }

        static TextOptions defaultEditTextOptions;
        static TextOptions DefaultEditTextOptions
        {
            get
            {
                if (defaultEditTextOptions == null)
                {
                    defaultEditTextOptions = new TextOptions(HorzAlignment.Near, VertAlignment.Top, WordWrap.Wrap, Trimming.None);
                }
                return defaultEditTextOptions;
            }
        }
        public override TextOptions DefaultTextOptions
        {
            get
            {
                if (OwnerEdit != null && OwnerEdit.InplaceType == InplaceType.Standalone)
                {
                    if (Item.ScrollBars == ScrollBars.Horizontal || Item.ScrollBars == ScrollBars.Both)
                    {
                        return new TextOptions(HorzAlignment.Near, VertAlignment.Top, WordWrap.NoWrap, Trimming.None);
                    }
                }
                return DefaultEditTextOptions;
            }
        }

        protected override void CalcContentRect(Rectangle bounds)
        {
            base.CalcContentRect(bounds);
            this.fMaskBoxRect = ContentRect;
            if (!(BorderPainter is EmptyBorderPainter))
                this.fMaskBoxRect.Inflate(-1, -1);
        }

        int IHeightAdaptable.CalcHeight(GraphicsCache cache, int width)
        {
            var info = new BorderObjectInfoArgs(cache)
            {
                Bounds = new Rectangle(0, 0, ContentRect.Width, 100)
            };
            Rectangle textRect = BorderPainter.GetObjectClientRectangle(info);
            if (!(BorderPainter is EmptyBorderPainter) && !(BorderPainter is InplaceBorderPainter))
                textRect.Inflate(-1, -1);
            string text = string.Empty;
            if (Item.LinesCount == 0)
            {
                text = DisplayText;
                if (!string.IsNullOrEmpty(text))
                {
                    char lastChar = text[text.Length - 1];
                    if (lastChar == 13 || lastChar == 10) text += "W";
                }
            }
            else
            {
                for (int i = 0; i < Item.LinesCount; i++)
                    text += (string.IsNullOrEmpty(text) ? "" : Environment.NewLine) + "W";
            }
            int height = CalcTextSizeCore(cache, text, textRect.Width).Height + 1;
            return (height + 100 - textRect.Bottom) + 1;
        }
    }

    [ToolboxBitmap(typeof(MultiLineButtonEdit), "RepositoryItemMultiLineButtonEdit.bmp")]
    [ToolboxItem(true)]
    [Description("Custom ButtonEdit with multi line support.")]
    public class MultiLineButtonEdit : ButtonEdit
    {
        static MultiLineButtonEdit() { RepositoryItemMultiLineButtonEdit.RegisterMultiLineButtonEdit(); }
        public MultiLineButtonEdit() { }
        public override string EditorTypeName { get { return RepositoryItemMultiLineButtonEdit.MultiLineButtonEditName; } }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new RepositoryItemMultiLineButtonEdit Properties
        {
            get { return base.Properties as RepositoryItemMultiLineButtonEdit; }
        }
        protected override bool AcceptsReturn { get { return Properties.AcceptsReturn; } }
        protected override bool AcceptsTab { get { return Properties.AcceptsTab; } }

        protected override void UpdateMaskBoxProperties(bool always)
        {
            base.UpdateMaskBoxProperties(always);
            if (MaskBox == null) return;
            if (always || !MaskBox.Multiline) MaskBox.Multiline = true;
            if (always || !MaskBox.WordWrap) MaskBox.WordWrap = true;
            if (always || Properties.AcceptsTab != MaskBox.AcceptsTab) MaskBox.AcceptsTab = Properties.AcceptsTab;
            if (always || Properties.AcceptsReturn != MaskBox.AcceptsReturn) MaskBox.AcceptsReturn = Properties.AcceptsReturn;
            if (always || Properties.ScrollBars != MaskBox.ScrollBars) MaskBox.ScrollBars = Properties.ScrollBars;
        }

        protected override bool IsInputKey(Keys keyData)
        {
            bool result = base.IsInputKey(keyData);
            if (result) return true;
            if (Properties.AcceptsReturn && keyData == Keys.Enter) return true;
            if (Properties.AcceptsTab && keyData == Keys.Tab) return true;
            return result;
        }
    }
}
