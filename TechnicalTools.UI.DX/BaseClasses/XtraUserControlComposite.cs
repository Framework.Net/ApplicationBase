﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

using DevExpress.XtraBars.Docking;
using DevExpress.XtraEditors;


namespace TechnicalTools.UI.DX
{
    [ToolboxItem(true)]
    public partial class XtraUserControlComposite : XtraUserControlBase, IHasDockUserControl
    {
        public XtraUserControlComposite()
        {
            InitializeComponent();
        }

        public List<Control> Views { get { return dockManager1.Panels.OfType<Control>().ToList(); } }

        DockPanel IHasDockUserControl.DockAsDefault(BaseEdit            ctl, string caption) { return _DockAsDefault(ctl, caption); }
        DockPanel IHasDockUserControl.DockAsDefault(XtraUserControlBase be,  string caption) { return _DockAsDefault(be,  caption); }
        DockPanel _DockAsDefault(Control ctl, string caption)
        {
            // {Begin/End}Update calls prevent the dockpanel to be visible right after AddPanel at bounds (0,0,200,200) for a brief time
            // It occurs often for the first dock panel.
            // Overload method AddPanel(Point) does not work either with a location far away from visible screen
            dockManager1.BeginUpdate();
            try
            {
                var dockPanel = dockManager1.AddPanel(DockingStyle.Float);

                dockPanel.DockedAsTabbedDocument = true;
                dockPanel.Controls.Add(ctl);
                ctl.Dock = DockStyle.Fill;
                //            dockManager1.AddPanel(DockingStyle.Fill, dockPanel);
                if (caption == null)
                {
                    ctl.TextChanged += (_, __) => dockPanel.Text = ctl.Text;
                    dockPanel.Text = ctl.Text;
                }
                else
                    dockPanel.Text = caption;
                ctl.Visible = true;
                dockPanel.Show();
                // Pour faire flotter :
                //dockPanel.DockedAsTabbedDocument = false;
                //dockPanel.MakeFloat(pt);
                //tabbedView1.Controller.Dock(dockPanel);
                return dockPanel;
            }
            finally
            {
                dockManager1.EndUpdate();
            }
        }

        private void dockManager1_ClosedPanel(object sender, DockPanelEventArgs e)
        {
            var dockPanel = e.Panel;
            if (dockPanel.Visibility == DockVisibility.Hidden)
                dockPanel.Dispose();
        }
    }
}