﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Security.Permissions;
using System.Windows.Forms;

using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraLayout.Utils;

using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.Tools;


namespace TechnicalTools.UI.DX
{
    public partial class EnhancedRibbonForm : RibbonForm
    {
        public bool IsLoaded { get; private set; }
        public bool IsClosing { get; private set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public DefaultToolTipController DefaultToolTipController { get; private set; }

        public static Form MainForm
        {
            get { return EnhancedXtraForm.MainForm; }
            set { EnhancedXtraForm.MainForm = value; }
        }

        public EnhancedRibbonForm()
        {
            this.components = new Container(); // Devexpress remove this from designer when we dropped DesignTimeSkinApplicator.. why ?
            InitializeComponent();
            DefaultToolTipController = new DefaultToolTipController(this.components);
            DefaultToolTipController.SetAllowHtmlText(this, DefaultBoolean.Default);

            if (DesignTimeHelper.IsInDesignMode)
                return;
            if (TrackInstances)
                ObjectTracker.Instance.Track(this);
            StartPosition = FormStartPosition.CenterParent; // par default on centre la form
            Load += (_, __) => CenterForm();

            Closing += (_, __) => { IsClosing = true; }; // TODO : Comment savoir si un handler a annule la fermeture ? Car dans ce cas il faudrait remettre Closign a false !

            DetectionMaximisationMinimisation();
        }
        public static bool TrackInstances { get; set; } = true;

        private void EnhancedRibbonForm_Load(object sender, EventArgs e)
        {
            // On inscrit ce handler pour l'event Shown ici (dans l'evenement Load)
            // plutot que dans le constructeur afin d'etre executé après les handlers
            // de l'event Shown défini par les construteur des classes enfant.
            Shown += (_, __) => IsLoaded = true;
        }


        // Fait en sorte que la form ne soit pas visible par defaut quand on fait un ApplicationRun(new MyMainForm()).
        // Cependant, contrairement a AllowToBeShown, elle permet de lever l'evenement Load.
        protected internal bool ShowAtStartup = true;
        protected internal bool AllowToBeShown = true;
        [DebuggerHidden, DebuggerStepThrough]
        protected override void SetVisibleCore(bool value)
        {
            if (!AllowToBeShown)
                base.SetVisibleCore(false);
            else if (!IsHandleCreated && !ShowAtStartup)
            {
                CreateHandle();
                base.SetVisibleCore(false);
            }
            else
                base.SetVisibleCore(value);
        }

        internal void ForceCreateHandle()
        {
            CreateHandle();
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        [DebuggerHidden, DebuggerStepThrough]
        protected override void WndProc(ref Message msg)
        {
            if (_OnWndProc.Count != 0)
                foreach (var action in _OnWndProc)
                    if (action(ref msg))
                        return;
            base.WndProc(ref msg);  // Il faut toujours executer ça
        }
        delegate bool WndProcHandler(ref Message msg);
        readonly List<WndProcHandler> _OnWndProc = new List<WndProcHandler>();

        #region Text & Infos diverse
        [DefaultValue("")]
        public new virtual string Text
        {
            get { return _text; }
            set { _text = value; RefreshText(); }
        }
        string _text = "";

        /// <summary>
        /// Raffraichit base.Text
        /// </summary>
        protected void RefreshText()
        {
            base.Text = CustomizeDisplayedText(Text, DesignTimeHelper.IsInDesignMode);
        }
        protected virtual string CustomizeDisplayedText(string normal_text, bool is_in_design_mode)
        {
            if (is_in_design_mode ||
                !ShowDelay ||
                !LastActionDuration.HasValue)
                return normal_text;

            return normal_text + string.Format(" ({0} : {1})",
                LastActionName ?? " last action : ",
                LastActionDuration.Value.ToHumanReadableShortNotation());
        }
        protected bool ShowDelay = DebugTools.IsDebug;

        [DefaultValue(null)]
        public string LastActionName
        {
            get { return _LastActionName; }
            set { _LastActionName = value; RefreshText(); }
        }
        string _LastActionName;

        [DefaultValue(null)]
        public TimeSpan? LastActionDuration
        {
            get { return _LastActionDuration; }
            set { _LastActionDuration = value; RefreshText(); }
        }
        TimeSpan? _LastActionDuration;

        #endregion Text & Infos diverse

        #region Helpeurs

        void CenterForm() { EnhancedXtraForm.CenterForm(this); }

        public Control FindFocusedControl()
        {
            return Control_Extensions.FindFocusedControl(this);
        }

        public static LayoutVisibility  VisibleIf    (bool  cond) { return EnhancedXtraUserControl.VisibleIf(cond); }
        public static BarItemVisibility MenuVisibleIf(bool  cond) { return EnhancedXtraUserControl.MenuVisibleIf(cond); }
        public static DefaultBoolean    EnabledIf    (bool? cond) { return EnhancedXtraUserControl.EnabledIf(cond); }

        public new void Close() // On veut cacher le Close parent, donc on ne peut pas faire une methode close avec un argument optionnel
        {
            Close(DialogResult.OK);
        }
        public void Close(DialogResult result)
        {
            if (Modal)
            {
                DialogResult = result;
                base.Close();
            }
            else
                base.Close();
        }

        protected internal virtual DialogResult ShowError(string msg, string title = null, bool ask_try_again = false)
        {
            msg = msg + (ask_try_again ? Environment.NewLine + Environment.NewLine + "Try again ?" : "");
            // Si msg est trop long (genre 3 Mo de texte la string n'est plus du tout affiché et on voit suelement une boite vide toute petite)
            return Tools.MessageBoxExt.Show(msg.Truncate(6000), title ?? "Error", ask_try_again ? MessageBoxButtons.YesNo : MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        protected internal virtual DialogResult ShowError(Exception ex, string title = null, bool ask_try_again = false)
        {
            if (ex is ISilentBusinessException)
                return DialogResult.OK;
            return ShowError(ExceptionManager.Instance.Format(ex, !DebugTools.IsDebug || ex is IUserUnderstandableException), title, ask_try_again);
        }

        #endregion Helpeurs

        #region Detection Maximisation / Minimisation

        [DebuggerHidden, DebuggerStepThrough]
        void DetectionMaximisationMinimisation()
        {
            SizeChanged += DetectionMaximisationMinimisation_SizeChanged;

            _CurrentWindowState = this.WindowState;

            _OnWndProc.Add(DetectionMaximisationMinimisation_OnWndProc);
        }

        public event EventHandler FormMaximized;
        public event EventHandler FormMinimized;

        private FormWindowState _CurrentWindowState;
        private void DetectionMaximisationMinimisation_SizeChanged(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized && _CurrentWindowState != FormWindowState.Maximized)
                FormMaximized.RaiseAndIgnoreException(this);
            if (WindowState == FormWindowState.Minimized && _CurrentWindowState != FormWindowState.Minimized)
                FormMinimized.RaiseAndIgnoreException(this);

            _CurrentWindowState = WindowState;
        }

        [DebuggerHidden, DebuggerStepThrough]
        bool DetectionMaximisationMinimisation_OnWndProc(ref Message m)
        {
            if (m.Msg == WM_SYSCOMMAND)
            {
                int command = m.WParam.ToInt32() & 0xfff0;
                if (command == SC_MINIMIZE)
                    FormMinimized.RaiseAndIgnoreException(this);
            }
            return false;
        }


        const int WM_SYSCOMMAND = 0x0112;
        const int SC_MINIMIZE = 0xF020;

        #endregion
    }
}
