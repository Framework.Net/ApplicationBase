﻿using System;
using System.Windows.Forms;

using DevExpress.XtraBars.Docking;
using DevExpress.XtraEditors;
using TechnicalTools.Model;

namespace TechnicalTools.UI.DX
{
    // Interface taguant les UserControl (View) afin de leur apporter des fonctionalités supplémentaire par extensions
    public interface IBaseControl
    {
        Control Parent      { get; }
        /// <summary> User is allowed to see data (data are not sensible) </summary>
        bool UserCanSee     { get; }
        /// <summary> User is allowed to insert / update / delete of data </summary>
        bool UserCanEdit    { get; }
    }
    public static class IBaseControl_Extensions
    {
        //[Obsolete("Make your control inherits from XtraUserControlBase, instead UserControl or XtraUserControl !", true)]
        public static DockPanel DockAsDefault(this IBaseControl @this, Control ctl, string caption = null)             { throw new InvalidOperationException("Make your control type " + ctl.GetType() + " inherit from " + nameof(XtraUserControlBase)); }
        public static DockPanel DockAsDefault(this IBaseControl @this, BaseEdit ctl, string caption = null)            { return FindParentAbleToDockControl((Control)@this).DockAsDefault(ctl, caption ?? (ctl is TextEdit ? ctl.Name : ctl.Text ?? ctl.Name)); }
        public static DockPanel DockAsDefault(this IBaseControl @this, XtraUserControlBase ctl, string caption = null) { return FindParentAbleToDockControl((Control)@this).DockAsDefault(ctl, caption ?? ctl.Text ?? ctl.Name); }
        static IHasDockUserControl FindParentAbleToDockControl(Control ctl)
        {
            if (ctl == null)
                throw new ArgumentNullException(nameof(ctl));
            var c = ctl;
            while (c != null)
            {
                if (c is IHasDockUserControl)
                    return c as IHasDockUserControl;
                c = ctl.Parent;
            }
            throw new TechnicalException($"Instance of {ctl.GetType().Name} \"ctl\" and all its parents are not able to dock any control", null);
        }

        //public static Action<Exception> ShowErrorWithTitle(this IBaseControl ctl, string actionName)
        //{
        //    var parentForm = ((Control)ctl).FindForm();
        //    Debug.Assert(parentForm != null, "parentForm != null");
        //    var frm = parentForm as FormBase // Si un panel est détaché de la form parentForm est de type DevExpress.XtraBars.Docking.FloatForm
        //           ?? Application.OpenForms.OfType<EnhancedXtraForm>().FirstOrDefault();
        //    if (frm != null)
        //        return ex =>
        //        {
        //            if (ex != null)
        //                frm.HandleExceptionInUI(actionName, ex);
        //        };

        //    return ex =>
        //        {
        //            XtraMessageBox.Show(parentForm, ex.Message, actionName);
        //        };
        //}
    }
}
