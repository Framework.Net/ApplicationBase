﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;

using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;

using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.Tools;


namespace TechnicalTools.UI.DX
{
    public partial class EnhancedXtraForm : XtraForm
    {
        public bool IsLoaded { get; private set; }
        public bool IsClosing { get; private set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public DefaultToolTipController DefaultToolTipController { get; private set; }

        public static Form MainForm
        {
            get
            {
                if (_MainForm == null)
                    _MainForm = Application.OpenForms.OfType<Form>().FirstOrDefault();
                return _MainForm ?? Form.ActiveForm;
            }
            set { _MainForm = value; }
        }
        static Form _MainForm;




        public EnhancedXtraForm()
        {
            components = new Container(); // Devexpress remove this from designer when we dropped DesignTimeSkinApplicator.. why ?
            InitializeComponent();
            DefaultToolTipController = new DefaultToolTipController(this.components);
            DefaultToolTipController.SetAllowHtmlText(this, DefaultBoolean.Default);

            if (DesignTimeHelper.IsInDesignMode)
                return;
            if (TrackInstances)
                ObjectTracker.Instance.Track(this);
            StartPosition = FormStartPosition.CenterParent; // par default on centre la form
            Load += (_, __) => CenterForm();

            Closing += (_, __) => { IsClosing = true; }; // TODO : Comment savoir si un handler a annule la fermeture ? Car dans ce cas il faudrait remettre Closign a false !

            DetectionMaximisationMinimisation();
            DetectionOfRightClickOnFormBar();
        }
        public static bool TrackInstances { get; set; } = true;

        public static EnhancedXtraForm CreateWithInner(Control view, Control centerOn = null)
        {
            var frm = new EnhancedXtraForm() { Text = " " };
            view.Dock = DockStyle.Fill;
            view.Visible = true;
            frm.ClientSize = view.Size;
            if (centerOn != null)
                frm.Location = new Point((centerOn.Width - frm.Width) / 2, (centerOn.Height - frm.Height) / 2);
            frm.Controls.Add(view);
            return frm;
        }
        private void EnhancedXtraForm_Load(object sender, EventArgs e)
        {
            // On inscrit ce handler pour l'event Shown ici (dans l'evenement Load)
            // plutot que dans le constructeur afin d'etre executé après les handlers
            // de l'event Shown défini par les construteur des classes enfant.
            Shown += (_, __) => IsLoaded = true;
        }


        // Fait en sorte que la form ne soit pas visible par defaut quand on fait un ApplicationRun(new MyMainForm()).
        // Cependant, contrairement a AllowToBeShown, elle permet de lever l'evenement Load.
        protected internal bool ShowAtStartup = true;
        protected internal bool AllowToBeShown = true;
        [DebuggerHidden, DebuggerStepThrough]
        protected override void SetVisibleCore(bool value)
        {
            if (!AllowToBeShown)
                base.SetVisibleCore(false);
            else if (!IsHandleCreated && !ShowAtStartup)
            {
                CreateHandle();
                base.SetVisibleCore(false);
            }
            else
                base.SetVisibleCore(value);
        }

        internal void ForceCreateHandle()
        {
            CreateHandle();
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        [DebuggerHidden, DebuggerStepThrough]
        protected override void WndProc(ref Message msg)
        {
            if (_OnWndProc.Count != 0)
                foreach (var action in _OnWndProc)
                    if (action(ref msg))
                        return;
            base.WndProc(ref msg);  // Il faut toujours executer ça
        }
        delegate bool WndProcHandler(ref Message msg);
        readonly List<WndProcHandler> _OnWndProc = new List<WndProcHandler>();

        // Permet d'interdir formellement l'affichage de la fenêtre.
        // Particulierement utile si il s'agit de la fenetre principale lancé dans Program.cs
        // car les autres techniques font que la form est visible un court instant (clignotement) => tres moche
        // Il est egalement possible de mettre son WindowsState a Minimized
        // from http://stackoverflow.com/a/4210040
        //protected internal bool AllowToBeShown = true;
        //protected override void SetVisibleCore(bool value)
        //{
        //    base.SetVisibleCore(AllowToBeShown && value);
        //}

        #region Text & Infos diverse
        [DefaultValue("")]
        public new virtual string Text
        {
            get { return _text; }
            set { _text = value; RefreshText(); }
        }
        string _text = "";

        /// <summary>
        /// Raffraichit base.Text
        /// </summary>
        protected void RefreshText()
        {
            base.Text = CustomizeDisplayedText(Text, DesignTimeHelper.IsInDesignMode);
        }
        protected virtual string CustomizeDisplayedText(string normal_text, bool is_in_design_mode)
        {
            if (is_in_design_mode ||
                !ShowDelay ||
                !LastActionDuration.HasValue)
                return normal_text;

            return normal_text + string.Format(" ({0} : {1})",
                LastActionName ?? " last action : ",
                LastActionDuration.Value.ToHumanReadableShortNotation());
        }
        protected bool ShowDelay = DebugTools.IsDebug;

        [DefaultValue(null)]
        public string LastActionName
        {
            get { return _LastActionName; }
            set { _LastActionName = value; RefreshText(); }
        }
        string _LastActionName;

        [DefaultValue(null)]
        public TimeSpan? LastActionDuration
        {
            get { return _LastActionDuration; }
            set { _LastActionDuration = value; RefreshText(); }
        }
        TimeSpan? _LastActionDuration;

        #endregion Text & Infos diverse

        #region Helpeurs

        void CenterForm() { CenterForm(this); }
        internal static void CenterForm(Form @this)
        {
            if (!@this.Modal && @this.StartPosition == FormStartPosition.CenterParent)
            {
                // Windows ne gère pas StartPosition dans le cas des fenetre non modal, on le fait nous même
                if ((@this.ParentForm ?? @this.Owner) != null) // As-t-on renseigné la fenêtre parent lors du Show ?
                {
                    var parent = @this.ParentForm ?? @this.Owner;
                    @this.Location = new Point(parent.Location.X + (parent.Width - @this.Width) / 2, parent.Location.Y + (parent.Height - @this.Height) / 2);
                }
            }
        }

        public Control FindFocusedControl()
        {
            return Control_Extensions.FindFocusedControl(this);
        }

        public static LayoutVisibility  VisibleIf    (bool  cond) { return EnhancedXtraUserControl.VisibleIf(cond); }
        public static BarItemVisibility MenuVisibleIf(bool  cond) { return EnhancedXtraUserControl.MenuVisibleIf(cond); }
        public static DefaultBoolean    EnabledIf    (bool? cond) { return EnhancedXtraUserControl.EnabledIf(cond); }

        public new void Close() // On veut cacher le Close parent, donc on ne peut pas faire une methode close avec un argument optionnel
        {
            Close(DialogResult.OK);
        }
        public void Close(DialogResult result)
        {
            if (Modal)
            {
                DialogResult = result;
                base.Close();
            }
            else
                base.Close();
        }

        [DebuggerHidden, DebuggerStepThrough]
        public TimeSpan TakeMeasure(params Action[] actions)
        {
            var measures = TakeMeasures(actions);
            return new TimeSpan(measures.Sum(time => time.Ticks));
        }

        [DebuggerHidden, DebuggerStepThrough]
        List<TimeSpan> TakeMeasures(params Action[] actions)
        {
            var measures = new List<TimeSpan>();

            DateTime start = DateTime.Now;
            TimeSpan total = TimeSpan.Zero;

            foreach (Action action in actions)
            {
                action();
                var dt = DateTime.Now.Subtract(start) - total;
                total += dt;
                measures.Add(dt);
            }
            return measures;
        }

        protected void DisableControlsWhileDoing(IEnumerable<Control> ctls, Action action)
        {
            var undo = DisableControlsAndGetUndo(ctls);

            try
            {
                action();
            }
            finally
            {
                undo();
            }
        }

        protected Action DisableControlsAndGetUndo(IEnumerable<Control> ctls)
        {
            var controls = ctls.ToList();
            Debug.Assert(controls.All(ctl => ctl != null)); // Ca parait con mais si on en lock la moité et que ca plante, les controls resteront locké !

            Debug.Assert(controls.Distinct().Count() == controls.Count);
            var enabilities = new Dictionary<Control, bool>();
            foreach (var ctl in controls)
            {
                enabilities.Add(ctl, ctl.Enabled);
                ctl.Enabled = false;
            }

            return () =>
                {
                    foreach (var ctl in controls)
                        ctl.Enabled = enabilities[ctl];
                };
        }

        #endregion Helpeurs

        //protected PostponingExecuter CreateEnhancedPostponer(string action, Action ui_action)
        //{
        //    return new PostponingExecuter(() =>
        //        {
        //            if (IsClosing)
        //                return;
        //            ExecuteAndDisplayException(action, ui_action);
        //        });
        //}


        #region Detection Maximisation / Minimisation

        [DebuggerHidden, DebuggerStepThrough]
        void DetectionMaximisationMinimisation()
        {
            SizeChanged += DetectionMaximisationMinimisation_SizeChanged;

            _CurrentWindowState = this.WindowState;

            _OnWndProc.Add(DetectionMaximisationMinimisation_OnWndProc);
        }



        public event EventHandler FormMaximized;
        public event EventHandler FormMinimized;

        private FormWindowState _CurrentWindowState;
        private void DetectionMaximisationMinimisation_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized && _CurrentWindowState != FormWindowState.Maximized)
                FormMaximized.RaiseAndIgnoreException(this);
            if (this.WindowState == FormWindowState.Minimized && _CurrentWindowState != FormWindowState.Minimized)
                FormMinimized.RaiseAndIgnoreException(this);

            _CurrentWindowState = this.WindowState;
        }

        [DebuggerHidden, DebuggerStepThrough]
        bool DetectionMaximisationMinimisation_OnWndProc(ref Message m)
        {
            if (m.Msg == WM_SYSCOMMAND)
            {
                int command = m.WParam.ToInt32() & 0xfff0;
                if (command == SC_MINIMIZE)
                    FormMinimized.RaiseAndIgnoreException(this);
            }
            return false;
        }


        const int WM_SYSCOMMAND = 0x0112;
        const int SC_MINIMIZE = 0xF020;

        #endregion

        #region Gestion de la bordure resizable quand FormBorderStyle est à None
        // from http://stackoverflow.com/questions/2575216/resize-winform-with-no-border

        [DefaultValue(false)]
        public bool EnableResizableBorder
        {
            get { return _EnableResizableBorder; }
            set
            {
                if (_EnableResizableBorder == value)
                    return;
                _EnableResizableBorder = value;
                if (_EnableResizableBorder)
                {
                    Debug.Assert(this.FormBorderStyle == FormBorderStyle.None, "La fonctionalite EnableResizableBorder est destiné a palier au style de bordure invisible");
                    _OnWndProc.Add(InitResizableBorderFeatures_OnWndProc);
                    this.DoubleBuffered = true;
                    this.SetStyle(ControlStyles.ResizeRedraw, true);
                }
                else
                    _OnWndProc.Remove(InitResizableBorderFeatures_OnWndProc);
                Invalidate();
            }
        }

        bool _EnableResizableBorder;
        private const int cGrip = 16;      // Grip size
        private const int cCaption = 32;   // Caption bar height;

        protected override void OnPaint(PaintEventArgs e)
        {
            if (_EnableResizableBorder)
            {
                var rc = new Rectangle(this.ClientSize.Width - cGrip, this.ClientSize.Height - cGrip, cGrip, cGrip);
                ControlPaint.DrawSizeGrip(e.Graphics, this.BackColor, rc);
                //rc = new Rectangle(0, 0, this.ClientSize.Width, cCaption);
                //e.Graphics.FillRectangle(Brushes.DarkBlue, rc);
            }
            else
                base.OnPaint(e);
        }

        bool InitResizableBorderFeatures_OnWndProc(ref Message m)
        {
            if (m.Msg == 0x84) // Trap WM_NCHITTEST
            {
                var pos = new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16);
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption)
                {
                    m.Result = (IntPtr)2;  // HTCAPTION
                    return true;
                }

                if (pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip)
                {
                    m.Result = (IntPtr)17; // HTBOTTOMRIGHT
                    return true;
                }
            }
            return false;
        }

        #endregion
        #region Detection Of Right Click On Form Bar

        void DetectionOfRightClickOnFormBar()
        {
            Load += DetectionOfRightClickOnFormBar_OnFormLoad;
            _OnWndProc.Add(DetectionOfRightClickOnFormBar_OnWndProc);
        }

        private void DetectionOfRightClickOnFormBar_OnFormLoad(object sender, EventArgs e)
        {
            _customMenuItems = new List<Tuple<string, Action>>();
            if (_customMenuItems.Count > 0) // rare case ... possible ?
                SetCustomizedFormMenuItems(_customMenuItems.ToArray());
        }
        List<Tuple<string, Action>> _customMenuItems;

        protected virtual void SetCustomizedFormMenuItems(params Tuple<string, Action>[] items)
        {
            if (_customMenuItems == null)
                throw new TechnicalException("This operation is not valid until Load event!", null);

            RemoveCurrentMenuItems();

            int i = 0;
            foreach (var kvp in items)
            {
                TryInsertMenu(i, kvp.Item1);
                _customMenuItems.Add(Tuple.Create(kvp.Item1, (Action)(() => { kvp.Item2(); })));
                ++i;
            }
            if (_customMenuItems.Count > 0)
                TryInsertMenu(i, "-");
        }


        void TryInsertMenu(int index, string text)
        {
            // Obligé de re-recuperer le menu handle car entre l'evenement HandleCreated (qu'on utilise plus ) et Load il change... vu que c'est suspect
            __lastMenuHandle = __lastMenuHandle == IntPtr.Zero ? GetSystemMenu(Handle, false) : __lastMenuHandle;

            // https://msdn.microsoft.com/en-us/library/windows/desktop/ms647987(v=vs.85).aspx
            var success = InsertMenu(__lastMenuHandle, index, MF_BYPOSITION | (text == "-" ? MF_SEPARATOR : 0), MenuItemDescriptorBase + index, text);
            if (!success)
            {
                var lastError = new Win32Exception(Marshal.GetLastWin32Error());
                //var sss = lastError.GetExceptionMessageInEnglish();
                lastError.EnrichDiagnosticWith("While inserting menu item \"" + text.Replace("\"", "\\\"") + "\" in form menu", eExceptionEnrichmentType.Technical);
                throw lastError;
            }
            __lastMenuHandle = IntPtr.Zero;
        }
        IntPtr __lastMenuHandle;

        void RemoveCurrentMenuItems()
        {
            if (_customMenuItems.Count > 0)
            {
                __lastMenuHandle = __lastMenuHandle == IntPtr.Zero ? GetSystemMenu(Handle, false) : __lastMenuHandle;
                RemoveMenu(__lastMenuHandle, _customMenuItems.Count, MF_BYPOSITION); // remove "-"
                for (int i = _customMenuItems.Count - 1; i >= 0; --i)
                    RemoveMenu(__lastMenuHandle, i, MF_BYPOSITION);
                __lastMenuHandle = IntPtr.Zero;
                _customMenuItems.Clear();
            }
        }

        [DebuggerStepThrough]
        bool DetectionOfRightClickOnFormBar_OnWndProc(ref Message m)
        {
            if (m.Msg == WM_SYSCOMMAND)
            {
                var i = m.WParam.ToInt32() - MenuItemDescriptorBase;
                if (i >= 0 && i < _customMenuItems.Count)
                    _customMenuItems[i].Item2();
            }
            return false;
        }

        const int MenuItemDescriptorBase = 1000; // custom value from 0 (not sure, it was 1000 when i get code), up to 0xF000 (not included as said in documentation)
        const Int32 MF_BYPOSITION = 0x400;
        const Int32 MF_SEPARATOR = 0x800;

        [DllImport("user32.dll")]
        static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool InsertMenu(IntPtr hMenu, Int32 wPosition, Int32 wFlags, Int32 wIDNewItem, string lpNewItem);
        [DllImport("user32.dll")]
        static extern bool RemoveMenu(IntPtr hMenu, Int32 wPosition, Int32 wFlags);


        #endregion Detection Of Right Click On Form Bar

    }
}
