﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.Runtime.InteropServices;


namespace TechnicalTools.UI.DX.Fonts
{
    public partial class CustomFonts
    {
        public static CustomFonts Instance { get; } = new CustomFonts();

        public Font SueEllenFrancisco   { get; private set; }
        public Font WorstveldSlingExtra { get; private set; }
        public Font SpaceAge { get; private set; }
        public Font BordofixedTryout { get; private set; }
        public Font MonospacedBold { get; private set; }
        public Font PWThaiMonospacedEG { get; private set; }
        public Font TangoMacabre { get; private set; }
        public Font JFWildWood { get; private set; }
        public Font SourceSansPro_Black { get; private set; }
        public Font SourceSansPro_BlackItalic { get; private set; }
        public Font SourceSansPro_Bold { get; private set; }
        public Font SourceSansPro_BoldItalic { get; private set; }
        public Font SourceSansPro_ExtraLight { get; private set; }
        public Font SourceSansPro_ExtraLightItalic { get; private set; }
        public Font SourceSansPro_Italic { get; private set; }
        public Font SourceSansPro_Light { get; private set; }
        public Font SourceSansPro_LightItalic { get; private set; }
        public Font SourceSansPro_Regular { get; private set; }
        public Font SourceSansPro_SemiBold { get; private set; }
        public Font SourceSansPro_SemiBoldItalic { get; private set; }

        public Font Vera { get; private set; }
        public Font VeraBd { get; private set; }
        public Font VeraBI { get; private set; }
        public Font VeraIt { get; private set; }
        public Font VeraMoBd { get; private set; }
        public Font VeraMoBI { get; private set; }
        public Font VeraMoIt { get; private set; }
        public Font VeraMono { get; private set; }
        public Font VeraSe { get; private set; }
        public Font VeraSeBd { get; private set; }


        private CustomFonts()
        {
            SueEllenFrancisco = Load("Sue Ellen Francisco.ttf");
            WorstveldSlingExtra = Load("Worstveld Sling Extra.ttf");
            SpaceAge = Load("Space Age.ttf");
            BordofixedTryout = Load("Bordofixed Tryout.ttf");
            MonospacedBold = Load("Monospaced Bold.ttf");
            PWThaiMonospacedEG = Load("PWThaiMonospacedEG.ttf");
            TangoMacabre = Load("TangoMacabre.ttf");
            JFWildWood = Load("JFWildWood.ttf");

            SourceSansPro_Black = Load("Source_Sans_Pro__Google_.SourceSansPro-Black.ttf");
            SourceSansPro_BlackItalic = Load("Source_Sans_Pro__Google_.SourceSansPro-BlackItalic.ttf");
            SourceSansPro_Bold = Load("Source_Sans_Pro__Google_.SourceSansPro-Bold.ttf");
            SourceSansPro_BoldItalic = Load("Source_Sans_Pro__Google_.SourceSansPro-BoldItalic.ttf");
            SourceSansPro_ExtraLight = Load("Source_Sans_Pro__Google_.SourceSansPro-ExtraLight.ttf");
            SourceSansPro_ExtraLightItalic = Load("Source_Sans_Pro__Google_.SourceSansPro-ExtraLightItalic.ttf");
            SourceSansPro_Italic = Load("Source_Sans_Pro__Google_.SourceSansPro-Italic.ttf");
            SourceSansPro_Light = Load("Source_Sans_Pro__Google_.SourceSansPro-Light.ttf");
            SourceSansPro_LightItalic = Load("Source_Sans_Pro__Google_.SourceSansPro-LightItalic.ttf");
            SourceSansPro_Regular = Load("Source_Sans_Pro__Google_.SourceSansPro-Regular.ttf");
            SourceSansPro_SemiBold = Load("Source_Sans_Pro__Google_.SourceSansPro-SemiBold.ttf");
            SourceSansPro_SemiBoldItalic = Load("Source_Sans_Pro__Google_.SourceSansPro-SemiBoldItalic.ttf");

            Vera = Load("Vera_Space_mono.Vera.ttf");
            VeraBd = Load("Vera_Space_mono.VeraBd.ttf");
            VeraBI = Load("Vera_Space_mono.VeraBI.ttf");
            VeraIt = Load("Vera_Space_mono.VeraIt.ttf");
            VeraMoBd = Load("Vera_Space_mono.VeraMoBd.ttf");
            VeraMoBI = Load("Vera_Space_mono.VeraMoBI.ttf");
            VeraMoIt = Load("Vera_Space_mono.VeraMoIt.ttf");
            VeraMono = Load("Vera_Space_mono.VeraMono.ttf");
            VeraSe = Load("Vera_Space_mono.VeraSe.ttf");
            VeraSeBd = Load("Vera_Space_mono.VeraSeBd.ttf");
        }

        Font Load(string resourceName)
        {
            byte[] bs;
            using (var stream = GetType().Assembly.GetManifestResourceStream(GetType().Namespace + "." + resourceName))
            {
                bs = new byte[stream.Length];
                stream.Read(bs, 0, bs.Length);
            }

            IntPtr fontPtr = Marshal.AllocCoTaskMem(bs.Length);
            Marshal.Copy(bs, 0, fontPtr, bs.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, bs.Length/* Properties.Resources.MyFontName.Length*/);
            AddFontMemResourceEx(fontPtr, (uint)bs.Length /*(uint)Properties.Resources.MyFontName.Length*/, IntPtr.Zero, ref dummy);

            // According to https://stackoverflow.com/questions/43813073/what-is-the-proper-method-for-embedding-multiple-truetype-fonts-in-a-winforms-ap
            // We should not free memory... IT seems to work in both case anyway
            //Marshal.FreeCoTaskMem(fontPtr);

            return new Font(fonts.Families[0], 12.0F);
        }

        private PrivateFontCollection fonts = new PrivateFontCollection();

        [DllImport("gdi32.dll")]
        static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);
    }
}
