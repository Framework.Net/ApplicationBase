﻿using System;
using System.Collections.Generic;


namespace TechnicalTools.Algorithm.Graph.TopologicalOrder
{
    public class StronglyConnectedComponent<T> : IEnumerable<Vertex<T>>
    {
        readonly LinkedList<Vertex<T>> _list;

        public StronglyConnectedComponent()
        {
            _list = new LinkedList<Vertex<T>>();
        }

        public StronglyConnectedComponent(IEnumerable<Vertex<T>> collection)
        {
            _list = new LinkedList<Vertex<T>>(collection);
        }

        public void Add(Vertex<T> vertex)
        {
            _list.AddLast(vertex);
        }

        public IEnumerator<Vertex<T>> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        public int Count
        {
            get
            {
                return _list.Count;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        public bool IsCycle { get { return _list.Count > 1; } }
    }
}