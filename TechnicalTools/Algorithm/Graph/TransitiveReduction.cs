﻿using System;
using System.Collections.Generic;


namespace TechnicalTools.Algorithm.Graph
{
    // From jgrapht Library : jgrapht-core/src/main/java/org/jgrapht/alg/TransitiveReduction.java
    public class TransitiveReduction
    {
        /// <summary> Public, Thread safe, Instance of the algorithm </summary>
        public static readonly TransitiveReduction Instance = new TransitiveReduction();

        private TransitiveReduction() { }

        /// <summary>
        /// <para>This method will remove all transitive edges from the graph passed as input parameter.</para>
        /// <para>You may want to clone the graph before, as transitive edges will be pitilessly removed.</para>
        /// </summary>
        /// <param name="vertex">set of all vertex/nodes</param>
        /// <param name="edges">set of all edges</param>
        /// <param name="getSource">Given an edge, give the source/starting vertex/node</param>
        /// <param name="getTarget">Given an edge, give the target/destination vertex/node</param>
        /// <param name="getEdges">Given a vertex, gives all edges startign from this vertex</param>
        /// <param name="removeEdge">give the lambda that alow to edit/remove edge to do the transitive reduction</param>
        public void Reduce<TNode, TEdge>(IReadOnlyCollection<TNode> vertex, IReadOnlyCollection<TEdge> edges, Func<TEdge, TNode> getSource, Func<TEdge, TNode> getTarget, Func<TNode, IReadOnlyList<TEdge>> getEdges, Action<TNode, TNode> removeEdge)
        {
            List<TNode> vertices = new List<TNode>(vertex);
            int n = vertices.Count;
            bool[,] originalMatrix = new bool[n, n]; // By default, all bits in the set initially have the value false.'

            // initialize matrix with edges
            foreach (var edge in edges)
            {
                TNode v1 = getSource(edge);
                TNode v2 = getTarget(edge);
                int v_1 = vertices.IndexOf(v1);
                int v_2 = vertices.IndexOf(v2);
                originalMatrix[v_1, v_2] = true;
            }
            // create path matrix from original matrix
            bool[,] pathMatrix = originalMatrix;
            transformToPathMatrix(pathMatrix, n);
            // create reduced matrix from path matrix
            bool[,] transitivelyReducedMatrix = pathMatrix;
            transitiveReduction(transitivelyReducedMatrix, n);
            // remove edges from the DirectedGraph which are not in the reduced
            // matrix
            for (int i = n - 1; i >= 0; --i)
                for (int j = n - 1; j >= 0; --j)
                    if (!transitivelyReducedMatrix[i, j])
                        removeEdge(vertices[i], vertices[j]);
        }

        /// <summary>
        /// <para>
        /// The matrix passed as input parameter will be transformed into a path matrix.
        /// </para>
        /// <para>
        /// This method is package visible for unit testing, but it is meant as a private method.
        /// </para>
        /// </summary>
        /// <param name="matrix">the original matrix to transform into a path matrix</param>
        /// <param name="length"></param>
        static void transformToPathMatrix(bool[,] matrix, int length)
        {
            // compute path matrix
            for (int i = 0; i < length; i++)
                for (int j = 0; j < length; j++)
                {
                    if (i == j)
                        continue;
                    if (matrix[j, i])
                        for (int k = 0; k < length; k++)
                            if (!matrix[j, k])
                                matrix[j, k] = matrix[i, k];
                }
        }

        /// <summary>
        /// <para>
        /// The path matrix passed as input parameter will be transformed into a transitively reduced matrix.
        /// </para>
        /// <para>
        /// This method is package visible for unit testing, but it is meant as a private method.
        ///  </para>
        /// </summary>
        /// <param name="pathMatrix">the path matrix to reduce</param>
        /// <param name="length"></param>
        static void transitiveReduction(bool[,] pathMatrix, int length)
        {
            // transitively reduce
            for (int j = 0; j < length; j++)
                for (int i = 0; i < length; i++)
                    if (pathMatrix[i, j])
                        for (int k = 0; k < length; k++)
                            if (pathMatrix[j, k])
                                pathMatrix[i, k] = false;
        }
    }

}
