﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TechnicalTools.Algorithm.Graph
{
    /// <summary> Simple implementation of topological sort algorithm </summary>
    public static class TopologicalOrderSimple
    {
        /// <inheritdoc cref="DoTopologicalSort{TNode}(IEnumerable{TNode}, Func{TNode, IEnumerable{TNode}}, Action{TNode, TNode, Action{TNode, TNode}})"/>
        public static List<TNode> DoTopologicalSort<TNode>(TNode root, Func<TNode, IEnumerable<TNode>> getTargetedNodes, bool ignoreNodeThatCycleToItself = false)
            where TNode : class
        {
            var nodes = new HashSet<TNode>();
            var toTreat = new Queue<TNode>();
            toTreat.Enqueue(root);
            while (toTreat.Count > 0)
            {
                var n = toTreat.Dequeue();
                if (nodes.Contains(n))
                    continue;
                nodes.Add(n);
                foreach (var subNode in getTargetedNodes(n))
                    toTreat.Enqueue(subNode);
            }
            return DoTopologicalSort(nodes.ToList(), getTargetedNodes, ignoreNodeThatCycleToItself);
        }

        /// <inheritdoc cref="DoTopologicalSort{TNode}(IEnumerable{TNode}, Func{TNode, IEnumerable{TNode}}, Action{TNode, TNode, Action{TNode, TNode}})"/>
        public static List<TNode> DoTopologicalSort<TNode>(IEnumerable<TNode> nodes, Func<TNode, IEnumerable<TNode>> getTargetedNodes, bool ignoreNodeThatCycleToItself = false)
            where TNode : class
        {
            return DoTopologicalSort(nodes, getTargetedNodes, (from, to, defaultAction) =>
            {
                if (ignoreNodeThatCycleToItself && from == to)
                    return;
                defaultAction(from, to);
            });
        }

        static void DefaultCycleBehaviour<TNode>(TNode from, TNode to, Action<TNode, TNode> _)
        {
            DefaultCycleBehaviour(from, to);
        }
        static void DefaultCycleBehaviour<TNode>(TNode from, TNode to)
        {
            throw new HasCycleException<TNode>(from, to);
        }

        /// <summary>
        /// Return list of node in topological order
        /// Root node(s) (that reference(s) others) is first item(s) of the returned list.
        /// (ie : So for inserting object in database that references each other with Foreign Key, you would need to insert them beginning by the ned of the list)
        /// </summary>
        /// <exception cref="HasCycleException{TNode}">In case of detected cycle an exception Throw en cas de cycle</exception>
        public static List<TNode> DoTopologicalSort<TNode>(IEnumerable<TNode> nodes, Func<TNode, IEnumerable<TNode>> getTargetedNodes, Action<TNode, TNode, Action<TNode, TNode>> onCycle)
            where TNode : class
        {
            var result = new List<TNode>();
            var visitStatuses = nodes.ToDictionary(n => n, n => 0); // Le tableau qui indiquera la couleur et donc le traitement d'un sommet.
            // Toutes les états sont initialisés à 0
            // 0 = non traité, 1 = en cours de visite, 2 = visité et traité
            void visit(TNode from, TNode to)
            {
                if (visitStatuses[to] == 1) // if node has a temporary mark then stop (not a DAG)
                    onCycle(from, to, DefaultCycleBehaviour);
                else if (visitStatuses[to] == 0) // if node is not marked (i.e. has not been visited yet)
                {
                    visitStatuses[to] = 1;
                    foreach (var x in getTargetedNodes(to))
                        if (x != to) // Auto référencement
                            visit(to, x);
                        else
                            onCycle(to, x, DefaultCycleBehaviour);

                    visitStatuses[to] = 2;
                    result.Insert(0, to);
                }
            }

            foreach (var to in nodes)
                if (visitStatuses[to] == 0)
                    visit(null, to);

            return result;
        }
        public class HasCycleException<TNode> : Exception
        {
            public TNode From { get; }
            public TNode To   { get; }
            public HasCycleException(TNode from, TNode to)
                : base("Graph contains a cycle!")
            {
                From = from;
                To = to;
            }
        }
    }
}
