﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using TechnicalTools.Model;


namespace TechnicalTools.Algorithm
{
    /// <summary>
    /// Class that browses a graph (or more simple : a tree) and do something on each nodes in parallel.
    /// </summary>
    /// <remarks>This class helps to name different use cases and allows developer to use anonymous type</remarks>
    public class GraphParallelBrowser
    {
        /// <param name="itemsConsideredVisited">
        /// If specified this container will be filled so after browsing you can get all nodes visited.
        /// You can fill with items you want to ignore. Their descendant will not be browsed and won't be in this dictionary at the end.
        /// </param>
        /// <param name="maxDegreeOfParallelism"></param>
        /// <param name="failFast">Stop all browsing as soon as there is an error</param>
        public static GraphParallelBrowser<T> CreateForGraph<T>(ConcurrentDictionary<T, T> itemsConsideredVisited = null, int? maxDegreeOfParallelism = null, bool failFast = true)
        {
            itemsConsideredVisited = itemsConsideredVisited ?? new ConcurrentDictionary<T, T>(); // MUST be non null
            return new GraphParallelBrowser<T>(itemsConsideredVisited, maxDegreeOfParallelism, failFast);
        }

        /// <summary>Slightly more optimized version because there is no cycle</summary>
        /// <param name="itemsConsideredVisited">Items you want to ignore. Descendant are also ignored!
        ///                                      If specified this container will be filled so after browsing you can get all nodes visited</param>
        /// <param name="maxDegreeOfParallelism"></param>
        /// <param name="failFast">Stop all browsing as soon as there is an error</param>
        public static GraphParallelBrowser<T> CreateForTree<T>(ConcurrentDictionary<T, T> itemsConsideredVisited = null, int? maxDegreeOfParallelism = null, bool failFast = true)
        {
            // "itemsConsideredVisited" can be null here (if user will really browse a tree)
            // useful to have null if user does not care about getting all the node of tree
            // Thus visiting a large graph does not waste any memory.
            return new GraphParallelBrowser<T>(itemsConsideredVisited, maxDegreeOfParallelism, failFast);
        }
    }

    /// <summary>
    /// Class that browses a graph (or more simple : a tree) and do something on each nodes in parallel.
    /// </summary>
    /// <remarks>Inspired from  http://stackoverflow.com/a/71715489/294998</remarks>
    public class GraphParallelBrowser<T>
    {
        /// <param name="itemsConsideredVisited">Must be non null if you graph is really a graph and not a tree. So item won't be treated multiple times</param>
        /// <param name="maxDegreeOfParallelism"></param>
        /// <param name="failFast">Stop all browsing as soon as there is an error</param>
        internal GraphParallelBrowser(ConcurrentDictionary<T, T> itemsConsideredVisited = null, int? maxDegreeOfParallelism = null, bool failFast = true)
        {
            if (itemsConsideredVisited != null)
                EnsureModelIsReady(itemsConsideredVisited);
            _itemsVisited = itemsConsideredVisited;
            _maxDegreeOfParallelism = maxDegreeOfParallelism ?? Environment.ProcessorCount;
            _failFast = failFast;
        }
        readonly ConcurrentDictionary<T, T> _itemsVisited;
        readonly int _maxDegreeOfParallelism;
        readonly bool _failFast;

        /// <summary>
        /// Make sure model is ok so we can build path if needed. See <see cref="GetBrowsingPathTo(T)"/>
        /// </summary>
        static void EnsureModelIsReady(ConcurrentDictionary<T, T> itemsConsideredVisited)
        {
            foreach (var kvp in itemsConsideredVisited)
                itemsConsideredVisited.TryUpdate(kvp.Key, kvp.Key, kvp.Value);
        }


        int? _maxDepth;
        BlockingCollection<(T, int)> _queue;
        int _nbItemsRemainingToTreat;
        CancellationTokenSource _cancelBecauseOfException;
        volatile Exception _ex;
        public async Task RunBrowsing(IEnumerable<T> initialItemsToTreat, Func<T, CancellationToken, IEnumerable<T>> consumeAndProduce, int? maxDepth = null, CancellationToken? userCancel = null)
        {
            if (_queue != null)
                throw new TechnicalException("An browse operation is already running!");
            _queue = new BlockingCollection<(T, int)>();
            _maxDepth = maxDepth;
            _nbItemsRemainingToTreat = 0;
            AddPotentialNewItems(initialItemsToTreat, 0, default(T));
            if (_nbItemsRemainingToTreat == 0)
                return; // actually there is nothing to do

            _cancelBecauseOfException = new CancellationTokenSource();
            var anyCancelReasons = userCancel.HasValue
                                 ? CancellationTokenSource.CreateLinkedTokenSource(userCancel.Value, _cancelBecauseOfException.Token)
                                 : _cancelBecauseOfException;
            var tasks = Enumerable.Range(0, _maxDegreeOfParallelism)
                                  .Select(_ => Task.Run(() => ConsumeUntilTheEnd(consumeAndProduce, anyCancelReasons.Token)));
            try
            {
                await Task.WhenAll(tasks).ConfigureAwait(false);
            }
            finally
            {
                _cancelBecauseOfException.Dispose();
                anyCancelReasons.Dispose();
                _queue.Dispose();
                _queue = null;
            }
        }

        /// <summary>
        /// Get one arbitrary browsing path (among possibilities) leading to "item".
        /// Because of nature of browsing (parallelized) this method is not guaranteed to retunr always the same result.
        /// To have a deterministic return, set maxDegreeOfParallelism to 1 when creating <see cref="GraphParallelBrowser{T}"/>
        /// </summary>
        /// <returns>Null if no browsing path exists. A non null non empty list for other case</returns>
        public List<T> GetBrowsingPathTo(T item)
        {
            if (!_itemsVisited.ContainsKey(item))
                return null;
            var current = item;
            var reverseResult = new List<T>() { current };
            while (!current.Equals(_itemsVisited[current]))
            {
                var previousInVisitingOrder = _itemsVisited[current];
                if (Equals(previousInVisitingOrder, default(T))) // In case T is a anonymous Tuple, "null" instead default(T) does not work
                    break;
                reverseResult.Add(previousInVisitingOrder);
                current = previousInVisitingOrder;
            }
            return reverseResult.AsEnumerable().Reverse().ToList();
        }

        void AddPotentialNewItems(IEnumerable<T> newItems, int curDepth, T originItem)
        {
            foreach (var newItem in newItems)
                if (_itemsVisited == null ||
                    _itemsVisited.TryAdd(newItem, originItem))
                {
                    Interlocked.Increment(ref _nbItemsRemainingToTreat);
                    _queue.Add((newItem, curDepth));
                }
        }

        void ConsumeUntilTheEnd(Func<T, CancellationToken, IEnumerable<T>> consumeAndProduce, CancellationToken anyCancelReasons)
        {
            try
            {
                while (_queue.TryTake(out var item, Timeout.Infinite, anyCancelReasons))
                {
                    try
                    {
                        if (item.Item2 < (_maxDepth ?? int.MaxValue))
                        {
                            var newItems = consumeAndProduce(item.Item1, anyCancelReasons);
                            if (newItems != null)
                                AddPotentialNewItems(newItems, item.Item2 + 1, item.Item1);
                        }
                    }
                    finally
                    {
                        // Counter never reach zero until the last thread has finished.
                        // This is because we add new items before decrementing treated ones
                        if (Interlocked.Decrement(ref _nbItemsRemainingToTreat) == 0)
                            _queue.CompleteAdding(); // unlock & ends all tasks
                    }
                }
            }
            catch (OperationCanceledException) when (_cancelBecauseOfException.IsCancellationRequested)
            {
                // fail fasts option has cancelled this task, so nothing here...
                // It limits the number of exception of this type in AggregateException the user will have to handle
                // (not a guarantee though, because two tasks can throw at same time)
            }
            catch (Exception ex) when (CancelOtherTaskAndDoNotCatch(ex))
            {
                // code never reached
            }
        }

        bool CancelOtherTaskAndDoNotCatch(Exception ex)
        {
            if (_failFast)
            {
                _ex = ex;
                _cancelBecauseOfException.Cancel();
            }
            return false;
        }
    }
}
