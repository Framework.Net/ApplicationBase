﻿using System;

using WinPoint = System.Drawing.Point;


namespace TechnicalTools.Algorithm.Geometry2D
{
    public struct PointD : IElement
    {
        public double X { get; set; }
        public double Y { get; set; }

        public PointD(double x, double y) : this()
        {
            X = x;
            Y = y;
        }
        public PointD(WinPoint p) : this()
        {
            X = p.X;
            Y = p.Y;
        }
        public static implicit operator PointD(WinPoint p)
        {
            return new PointD(p);
        }

        public double SquareDistance(PointD b) { return (X - b.X) * (X - b.X) + (Y - b.Y) * (Y - b.Y); }
        public double SquareDistance(SegmentD segment) { return segment.SquareDistance(this); }
        public double SquareDistance(PointD p1, PointD p2) { return new SegmentD(p1, p2).SquareDistance(this); }

        /**
         * Calculate the cross product of two points.
         * @param a first point
         * @param b second point
         * @return the value of the cross product
         */
        public double CrossProduct(PointD p)
        {
            return X * p.Y - p.X * Y;
        }

        public WinPoint ToWindowPoint()
        {
            return new WinPoint((int)(X+0.5), (int)(Y + 0.5));
        }
    }

}
