﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;


namespace TechnicalTools.MetaProgramming.ExpressionVisitors
{
    class ExpressionConverterCallDispatcher<T> : ExpressionVisitor
    {
        public ExpressionConverterCallDispatcher(IExpressionConverterVisitor<T> owner)
        {
            _owner = owner;
        }
        readonly IExpressionConverterVisitor<T> _owner;

        public class BoxedConvertedExpression : Expression
        {
            public BoxedConvertedExpression(T value)
            {
                Value = value;
            }
            internal T Value;
            public T UnboxedValue() { return Value; }
        }
        BoxedConvertedExpression Box(T valueToReturnToOwner)
        {
            BoxedConvertedExpression box;
            if (_boxes.Count == 0)
                return new BoxedConvertedExpression(valueToReturnToOwner);
            box = _boxes.Pop();
            box.Value = valueToReturnToOwner;
            return box;
        }
        // Avoid memory allocation of tiny object
        readonly Stack<BoxedConvertedExpression> _boxes = new Stack<BoxedConvertedExpression>();

        // Handle case where we cannot return the box directly because overriden method return sealed type
        BoxedConvertedExpression _dirtyReturnedBox;

        public virtual T VisitDispatch(Expression node)
        {
            var box = (BoxedConvertedExpression)Visit(node);
            if (box == null)
                return default(T);
            var v = box.Value;
            box.Value = default(T); // not mandatory but clean and help to debug
            _boxes.Push(box);
            return v;
        }

        public override Expression Visit(Expression node)
        {
            var res = base.Visit(node);
            if (_dirtyReturnedBox != null)
            {
                var box = _dirtyReturnedBox;
                _dirtyReturnedBox = null;
                return box;
            }
            return res;
        }
        #region Originals methods are protected internal

        protected override Expression VisitBinary(BinaryExpression node) { return Box(_owner.VisitBinary(node)); }
        protected override Expression VisitBlock(BlockExpression node) { return Box(_owner.VisitBlock(node)); }
        protected override Expression VisitConditional(ConditionalExpression node) { return Box(_owner.VisitConditional(node)); }
        protected override Expression VisitConstant(ConstantExpression node) { return Box(_owner.VisitConstant(node)); }
        protected override Expression VisitDebugInfo(DebugInfoExpression node) { return Box(_owner.VisitDebugInfo(node)); }
        protected override Expression VisitDynamic(DynamicExpression node) { return Box(_owner.VisitDynamic(node)); }
        protected override Expression VisitDefault(DefaultExpression node) { return Box(_owner.VisitDefault(node)); }
#pragma warning disable CS0809 // Un membre obsolète se substitue à un membre non obsolète
        [Obsolete("Do not know how to migrate!", true)]
        protected override Expression VisitExtension(Expression node) { return Box(_owner.VisitExtension(node)); }
#pragma warning restore CS0809 // Un membre obsolète se substitue à un membre non obsolète
        protected override Expression VisitGoto(GotoExpression node) { return Box(_owner.VisitGoto(node)); }
        protected override Expression VisitInvocation(InvocationExpression node) { return Box(_owner.VisitInvocation(node)); }
        protected override Expression VisitLabel(LabelExpression node) { return Box(_owner.VisitLabel(node)); }

        protected override Expression VisitLambda<T2>(Expression<T2> node) { return Box(_owner.VisitLambda<T2>(node)); }
        protected override Expression VisitLoop(LoopExpression node) { return Box(_owner.VisitLoop(node)); }
        protected override Expression VisitMember(MemberExpression node) { return Box(_owner.VisitMember(node)); }
        protected override Expression VisitIndex(IndexExpression node) { return Box(_owner.VisitIndex(node)); }
        protected override Expression VisitMethodCall(MethodCallExpression node) { return Box(_owner.VisitMethodCall(node)); }
        protected override Expression VisitNewArray(NewArrayExpression node) { return Box(_owner.VisitNewArray(node)); }
        protected override Expression VisitNew(NewExpression node) { return Box(_owner.VisitNew(node)); }
        protected override Expression VisitParameter(ParameterExpression node) { return Box(_owner.VisitParameter(node)); }
        protected override Expression VisitRuntimeVariables(RuntimeVariablesExpression node) { return Box(_owner.VisitRuntimeVariables(node)); }
        protected override Expression VisitSwitch(SwitchExpression node) { return Box(_owner.VisitSwitch(node)); }
        protected override Expression VisitTry(TryExpression node) { return Box(_owner.VisitTry(node)); }
        protected override Expression VisitTypeBinary(TypeBinaryExpression node) { return Box(_owner.VisitTypeBinary(node)); }
        protected override Expression VisitUnary(UnaryExpression node) { return Box(_owner.VisitUnary(node)); }
        protected override Expression VisitMemberInit(MemberInitExpression node) { return Box(_owner.VisitMemberInit(node)); }
        protected override Expression VisitListInit(ListInitExpression node) { return Box(_owner.VisitListInit(node)); }

        #endregion Originals methods are protected internal

        #region Originals methods are protected

        protected override LabelTarget VisitLabelTarget(LabelTarget node) { _dirtyReturnedBox = Box(_owner.VisitLabelTarget(node)); return node; }
        protected override SwitchCase VisitSwitchCase(SwitchCase node) { _dirtyReturnedBox = Box(_owner.VisitSwitchCase(node)); return node; }
        protected override CatchBlock VisitCatchBlock(CatchBlock node) { _dirtyReturnedBox = Box(_owner.VisitCatchBlock(node)); return node; }
        protected override ElementInit VisitElementInit(ElementInit node) { _dirtyReturnedBox = Box(_owner.VisitElementInit(node)); return node; }
        protected override MemberBinding VisitMemberBinding(MemberBinding node) { _dirtyReturnedBox = Box(_owner.VisitMemberBinding(node)); return node; }
        protected override MemberAssignment VisitMemberAssignment(MemberAssignment node) { _dirtyReturnedBox = Box(_owner.VisitMemberAssignment(node)); return node; }
        protected override MemberMemberBinding VisitMemberMemberBinding(MemberMemberBinding node) { _dirtyReturnedBox = Box(_owner.VisitMemberMemberBinding(node)); return node; }
        protected override MemberListBinding VisitMemberListBinding(MemberListBinding node) { _dirtyReturnedBox = Box(_owner.VisitMemberListBinding(node)); return node; }

        #endregion Originals methods are protected
    }
}
