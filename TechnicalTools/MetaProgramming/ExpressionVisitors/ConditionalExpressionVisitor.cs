﻿using System;
using System.Linq.Expressions;


namespace TechnicalTools.MetaProgramming.ExpressionVisitors
{
    public partial class ConditionalExpressionVisitor : ExpressionVisitor
    {
        public override Expression Visit(Expression node)
        {
            return base.Visit(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            return base.VisitBinary(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            return base.VisitConstant(node);
        }

        protected override Expression VisitLambda<TFunc>(Expression<TFunc> node)
        {
            return base.VisitLambda(node);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            return base.VisitMember(node);
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            return base.VisitMethodCall(node);
        }

        protected override Expression VisitNewArray(NewArrayExpression node)
        {
            return base.VisitNewArray(node);
        }
        protected override Expression VisitParameter(ParameterExpression node)
        {
            return base.VisitParameter(node);
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            return base.VisitUnary(node);
        }

        protected override Expression VisitNew(NewExpression node)
        {
            // for value like "new DateTime(...)"
            return base.VisitNew(node);
        }
    }

    /// <summary>
    /// Expressions that are not in part of a conditional expressions
    /// </summary>
    public partial class ConditionalExpressionVisitor
    {
        protected override CatchBlock VisitCatchBlock(CatchBlock node)
        {
            throw new Exception();
            //return base.VisitCatchBlock(node);
        }
        protected override ElementInit VisitElementInit(ElementInit node)
        {
            throw new Exception();
            //return base.VisitElementInit(node);
        }
        protected override LabelTarget VisitLabelTarget(LabelTarget node)
        {
            throw new Exception();
            //return base.VisitLabelTarget(node);
        }
        protected override MemberAssignment VisitMemberAssignment(MemberAssignment node)
        {
            throw new Exception();
            //return base.VisitMemberAssignment(node);
        }
        protected override MemberBinding VisitMemberBinding(MemberBinding node)
        {
            throw new Exception();
            //return base.VisitMemberBinding(node);
        }
        protected override MemberListBinding VisitMemberListBinding(MemberListBinding node)
        {
            throw new Exception();
            //return base.VisitMemberListBinding(node);
        }
        protected override MemberMemberBinding VisitMemberMemberBinding(MemberMemberBinding node)
        {
            throw new Exception();
            //return base.VisitMemberMemberBinding(node);
        }
        protected override SwitchCase VisitSwitchCase(SwitchCase node)
        {
            throw new Exception();
            //return base.VisitSwitchCase(node);
        }


        protected override Expression VisitBlock(BlockExpression node)
        {
            throw new Exception();
            //return base.VisitBlock(node);
        }
        protected override Expression VisitConditional(ConditionalExpression node)
        {
            throw new Exception();
            //return base.VisitConditional(node);
        }


        protected override Expression VisitDebugInfo(DebugInfoExpression node)
        {
            throw new Exception();
            //return base.VisitDebugInfo(node);
        }
        protected override Expression VisitDefault(DefaultExpression node)
        {
            throw new Exception();
            //return base.VisitDefault(node);
        }
        protected override Expression VisitDynamic(DynamicExpression node)
        {
            throw new Exception();
            //return base.VisitDynamic(node);
        }
        protected override Expression VisitExtension(Expression node)
        {
            throw new Exception();
            //return base.VisitExtension(node);
        }
        protected override Expression VisitGoto(GotoExpression node)
        {
            throw new Exception();
            //return base.VisitGoto(node);
        }
        protected override Expression VisitIndex(IndexExpression node)
        {
            throw new Exception();
            //return base.VisitIndex(node);
        }
        protected override Expression VisitInvocation(InvocationExpression node)
        {
            throw new Exception();
            //return base.VisitInvocation(node);
        }
        protected override Expression VisitLabel(LabelExpression node)
        {
            throw new Exception();
            //return base.VisitLabel(node);
        }


        protected override Expression VisitListInit(ListInitExpression node)
        {
            throw new Exception();
            //return base.VisitListInit(node);
        }
        protected override Expression VisitLoop(LoopExpression node)
        {
            throw new Exception();
            //return base.VisitLoop(node);
        }


        protected override Expression VisitMemberInit(MemberInitExpression node)
        {
            throw new Exception();
            //return base.VisitMemberInit(node);
        }


        protected override Expression VisitRuntimeVariables(RuntimeVariablesExpression node)
        {
            throw new Exception();
            //return base.VisitRuntimeVariables(node);
        }
        protected override Expression VisitSwitch(SwitchExpression node)
        {
            throw new Exception();
            //return base.VisitSwitch(node);
        }
        protected override Expression VisitTry(TryExpression node)
        {
            throw new Exception();
            //return base.VisitTry(node);
        }
        protected override Expression VisitTypeBinary(TypeBinaryExpression node)
        {
            throw new Exception();
            //return base.VisitTypeBinary(node);
        }
    }

}
