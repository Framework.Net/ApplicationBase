﻿using System;
using System.Diagnostics;


namespace TechnicalTools.Diagnostics
{
    public static partial class ArithmeticExceptionHelper
    {
        [Conditional("SUPER_DEBUG")]
        [DebuggerHidden, DebuggerStepThrough]
        public static void CheckDoubleValue(object value)
        {
            if (value == null || value == DBNull.Value) return;
            if (value is double? && ((double?)value).HasValue)
                value = ((double?)value).Value;

            bool checkValue;
            if (value is double)
            {
                checkValue = (!double.IsInfinity((double)value) && !double.IsNaN((double)value) && !double.IsNegativeInfinity((double)value) && !double.IsPositiveInfinity((double)value));
                if (!checkValue)
                    throw new Exception("Division by Zero");
            }
            // Pas d'equivalent pour le type decimal ? Est ce que le type decimal throw directement une erreur ?
            //else if (value is decimal)
            //    Debug.Assert(!decimal.IsNaN(((decimal)value) &&
            //                 !decimal.IsNegativeInfinity((decimal)value) &&
            //                 !decimal.IsPositiveInfinity((decimal)value));

        }
        [Obsolete("Utiliser ArithmeticExceptionHelper.EnableExceptionOnNaN")]
        public static void Initialize()
        {
            float zero = 0.0f; // Want 0.0f. Fool compiler...
            //Console.WriteLine("zero = " + zero.ToString());

            // A NaN which does not throw exception
            float firstNaN = zero / 0.0f;
            //Console.WriteLine("firstNaN= " + firstNaN.ToString());

            // Now turn on floating-point exceptions
            TurnOnNanValueCheck();

            // A NaN which does throw exception
            // test :
            //float secondNaN = 0;
            //try
            //{
            //    // Put as much code here as you like.
            //    // Enable "break when an exception is thrown" in the debugger
            //    // for system exceptions to get to the line where it is thrown
            //    // before catching it below.
            //    secondNaN = zero / 0.0f;
            //}
            //catch (ArithmeticException ex)
            //{
            //    _clearfp(); // Clear floating point error word.
            //}

            //System.Console.WriteLine("secondNaN= " + secondNaN.ToString());
        }
        //[DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        //public static extern uint _control87(uint a, uint b);
        //[DllImport("msvcrt.dll")]
        //public static extern uint _clearfp();

        [Obsolete("Utiliser ArithmeticExceptionHelper.EnableExceptionOnNaN")]
        public static void TurnOnNanValueCheck()
        {
            uint empty = 0;
            uint cw = _control87(empty, empty); // Debugger halts on this one and complains about false signature, but continue works.
            //Console.WriteLine(cw.ToString());
            uint MCW_EM = 0x0008001f; // From float.h
            uint _EM_INVALID = 0x00000010; // From float.h (invalid corresponds to NaN
            // See http://www.fortran-2000.com/ArnaudRecipes/CompilerTricks.html#x86_FP

            cw &= ~(_EM_INVALID);
            _clearfp(); // Clear floating point error word.
            _control87(cw, MCW_EM); // Debugger halts on this one and complains about false signature, but continue works.
            //Console.WriteLine(cw.ToString());

        }
        //static void CnADO_Disconnect(ref ADODB.EventStatusEnum adStatus, ADODB.Connection pConnection)
        //{
        //    // INfo a propos de ADODB.EventStatusEnum (from http://www.w3schools.com/ado/ev_rs_recordchange.asp)
        //    // adStatusOK	            	The operation that caused the event was successful
        //    // adStatusErrorsOccurred		The operation that caused the event failed
        //    // adStatusCantDeny	        	The operation that caused the event cannot be cancelled
        //    // adStatusCancel	        	The operation that caused the event is cancelled
        //    // adStatusUnwantedEvent		Prevents subsequent notifications before the event method has finished executing
        //    if (adStatus.In(ADODB.EventStatusEnum.adStatusErrorsOccurred,
        //                    ADODB.EventStatusEnum.adStatusUnwantedEvent,
        //                    ADODB.EventStatusEnum.adStatusCancel,
        //                    ADODB.EventStatusEnum.adStatusCantDeny))
        //    {
        //        Break();
        //    }
        //}

    }
}
