﻿using System;
using System.Threading;


namespace TechnicalTools.Tools
{
    public class PostponingExecuter
    {
        /// <summary>
        /// Indique si l'execution de l'action est necessaire
        /// </summary>
        public bool ExecutionIsNeeded { get; private set; }

        /// <summary>
        /// Indique qu'une execution a ete plannifié (elle ne peut pas etre deplanifié)
        /// En revanche si ExecutionIsNeeded est à false au moment ou elle est finalement executé, l'action n'est pas effectué.
        /// </summary>
        bool ExecutionIsScheduledOnIdle { get; set; }

        /// <summary> Fait typiquement pour appeler BeginUpdate </summary>
        public event EventHandler ExecutionScheduled;

        /// <summary> Fait typiquement pour appeler EndUpdate </summary>
        public event EventHandler ExecutionUnscheduled;

        readonly SynchronizationContext _context;
        readonly Action _actionToExecute;

        public PostponingExecuter(Action action_to_execute, SynchronizationContext context = null)
        {
            _context = context ?? SynchronizationContext.Current;
            if (_context == null)
                throw new ArgumentNullException(nameof(context));
            _actionToExecute = action_to_execute;
        }

        public void ExecuteNowIfNeeded()
        {
            if (ExecutionIsNeeded)
                ExecuteNow();
        }
        public void ExecuteNow()
        {
            _actionToExecute();
            ExecutionIsNeeded = false; // Indique à l'exexcution planifié (si il y en a une) que l'execution n'est plus nécessaire
        }

        public void SchedulePostponedExecutionAsEventHandler(object sender, EventArgs e)
        {
            SchedulePostponedExecution();
        }
        public void SchedulePostponedExecution(bool delay_execution = true)
        {
            // Gestion un peu complexe mais gère tous les cas
            // Exemple : durant un meme evenement, on apelle ScheduleExecutionOf
            //           avec delay_execution = true, puis false, puis true, puis false => on ne doit executer que deux fois action durant les appel avec true
            if (delay_execution)
            {
                if (!ExecutionIsNeeded) // Aucune execution de planifié
                { // Alors on en planifie une
                    ExecutionIsNeeded = true;
                    ExecutionIsScheduledOnIdle = true;
                    ExecutionScheduled?.Invoke(this, EventArgs.Empty);
                    _context.Post(_ =>
                    {
                        if (ExecutionIsNeeded) // Si le refresh planifié n'a pas deja été forcé
                            ExecuteNow();
                        ExecutionIsScheduledOnIdle = false;
                        ExecutionUnscheduled?.Invoke(this, EventArgs.Empty);
                    }, null);
                }
                return;
            }
            ExecuteNow();
        }
    }
}
