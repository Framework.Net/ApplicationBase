﻿using System;


namespace TechnicalTools.Tools
{
    /// <summary>
    /// Classe permettant de désambiguïser des méthodes.
    /// Si deux methodes ont le même nom et les mêmes type d'arguments, autrement dit la même signature. Le compilateur ne sait pas quel methode choisir.
    /// Les contraintes generiques et les types de retour ne font pas parti de la signature.
    /// Pourtant la plupart du temps les contraintes generiques permettraient de distinguer a coup sur la methode a appeler
    /// Exemple : http://stackoverflow.com/a/14624904
    /// Il suffit de rajouter a la fin de votre methode, un argument de type Disambiguator dont les parametres generique sont ceux utilisé dans les contraintes
    /// <example>
    /// class BaseA {}
    /// class BaseB {}
    /// class A : BaseA {}
    /// class B : BaseB {}
    /// T foo[T] (T, Disambiguator[BaseA, T] _ = null) where T : BaseA
    /// T foo[T] (T, Disambiguator[BaseB, T] _ = null) where T : BaseB
    /// </example>
    /// ReSharper disable UnusedTypeParameter
    /// ReSharper disable InconsistentNaming
    /// </summary>
    public class Disambiguator<TInherited, T>
        where TInherited : T
    {
        [Obsolete("It does not have sense to create an instance of this type. See class comment", true)]
        public Disambiguator() { }
    }

    public class Disambiguator<TInherited, T, I>
        where TInherited : T, I
    {
        [Obsolete("It does not have sense to create an instance of this type. See class comment", true)]
        public Disambiguator() { }
    }
    public class Disambiguator<TInherited, T, I1, I2>
        where TInherited : T, I1, I2
    {
        [Obsolete("It does not have sense to create an instance of this type. See class comment", true)]
        public Disambiguator() { }
    }
}
