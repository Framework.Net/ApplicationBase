﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using TechnicalTools.Logs;


namespace TechnicalTools.Tools
{
    /// <summary>
    /// Provide a way to notify other code that an error/issue has been _ignored_ and current execution flow continue.
    /// There is no need to log the error in that case, just specify the ILogger when raising event.
    /// The default behavior is to log problem / issue.
    /// However you can provide a custom message when raising error/issue,
    /// and also exception instance.
    /// You can enrich exception diangostic / message by calling <see cref="Exception_Extensions.EnrichDiagnosticWith{TException}(TException, ExceptionEnrichmentInfo, bool)"/>
    /// </summary>
    public class BusEvents
    {
        public static readonly BusEvents Instance = new BusEvents();

        private BusEvents() { }

        public event EventHandler<MessageEventArgs> BusinessMessage;
        public void RaiseBusinessMessage(string business_problem, ILogger log)
        {
            var e = new BusinessMessageEventArgs(business_problem, log);
            BusinessMessage?.Invoke(this, e);
            if (!e.Handled)
                e.DefaultHandle();
        }

        public event EventHandler<MessageEventArgs> UserUnderstandableMessage;
        public void RaiseUserUnderstandableMessage(string understandable_problem, ILogger log)
        {
            var e = new UserUnderstandableMessageEventArgs(understandable_problem, log);
            UserUnderstandableMessage?.Invoke(this, e);
            if (!e.Handled)
                e.DefaultHandle();
        }

        public event EventHandler<BusinessWarningEventArgs> BusinessWarning;
        public void RaiseBusinessWarning(string problem, ILogger log, IEnumerable<Exception> exceptions = null)
        {
            var e = new BusinessWarningEventArgs(problem, log, exceptions);
            BusinessWarning?.Invoke(this, e);
            if (!e.Handled)
                e.DefaultHandle();
        }
        public void RaiseBusinessWarning(string problem, ILogger log, params Exception[] exceptions)
        {
            RaiseBusinessWarning(problem, log, (IEnumerable<Exception>)exceptions);
        }
        public event EventHandler<TechnicalErrorEventArgs> TechnicalError;
        public void RaiseTechnicalError(string technical_problem, ILogger log, IEnumerable<Exception> exceptions = null)
        {
            var e = new TechnicalErrorEventArgs(technical_problem, log, exceptions);
            TechnicalError?.Invoke(this, e);
            if (!e.Handled)
                e.DefaultHandle();
        }
        public void RaiseTechnicalError(string technical_problem, ILogger log, params Exception[] exceptions)
        {
            RaiseTechnicalError(technical_problem, log, (IEnumerable<Exception>)exceptions);
        }
    }

    public abstract class MessageEventArgs : EventArgs
    {
        public string  Message { get; }
        public ILogger Log     { get; }
        public bool    Handled { get; set; }

        public MessageEventArgs(string message, ILogger log)
        {
            Debug.Assert(!string.IsNullOrWhiteSpace(message));

            Message = message;
            Log = log;
        }

        public abstract void DefaultHandle();
    }
    public class BusinessMessageEventArgs : MessageEventArgs
    {
        public BusinessMessageEventArgs(string message, ILogger log)
            : base(message, log)
        {
        }

        public override void DefaultHandle()
        {
            Handled = true;
            Log.Audit(Message, LogTags.Ignored);
        }
    }
    public class UserUnderstandableMessageEventArgs : MessageEventArgs
    {
        public UserUnderstandableMessageEventArgs(string message, ILogger log)
            : base(message, log)
        {
        }

        public override void DefaultHandle()
        {
            Handled = true;
            Log.Info(Message, LogTags.Ignored);
        }
    }



    public abstract class IssueEventArgs : EventArgs
    {
        public string          Problem    { get; private set; }
        /// <summary> Logger of origina class that raise the event </summary>
        public ILogger         Log        { get; }
        public List<Exception> Exceptions { get; private set; } = new List<Exception>();
        public bool            Handled    { get; set; }

        protected IssueEventArgs(string problem, ILogger log, IEnumerable<Exception> exceptions = null)
        {
            Debug.Assert(!string.IsNullOrWhiteSpace(problem));

            Problem = problem;
            Log = log;
            if (exceptions != null)
                Exceptions.AddRange(exceptions);
        }

        public abstract void DefaultHandle();
    }

    public class TechnicalErrorEventArgs : IssueEventArgs
    {
        public TechnicalErrorEventArgs(string problem, ILogger log, IEnumerable<Exception> exceptions = null)
            : base(problem, log, exceptions)
        {
        }

        public override void DefaultHandle()
        {
            Handled = true;
            Log.Error(Problem);
            foreach (var ex in Exceptions)
                Log.Error("for previous error", ex, LogTags.Ignored);
        }
    }
    public class BusinessWarningEventArgs : IssueEventArgs
    {
        public BusinessWarningEventArgs(string problem, ILogger log, IEnumerable<Exception> exceptions = null)
            : base(problem, log, exceptions)
        {
        }

        public override void DefaultHandle()
        {
            Handled = true;
            Log.Warn(Problem);
            foreach (var ex in Exceptions)
                Log.Warn("for previous warning", ex, LogTags.Ignored);
        }
    }
}
