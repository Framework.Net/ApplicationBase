﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;


namespace TechnicalTools.Tools
{
    // From https://stackoverflow.com/a/35520207/294998
    // Maybe to replace by System.Data.Entity.Infrastructure.ObjectReferenceEqualityComparer.Default ?
    public sealed class ReferenceEqualityComparer<T> : IEqualityComparer, IEqualityComparer<T>
        where T : class
    {
        public static readonly ReferenceEqualityComparer<T> Default = new ReferenceEqualityComparer<T>();

        private ReferenceEqualityComparer() { }

        public int GetHashCode(object obj)
        {
            return RuntimeHelpers.GetHashCode(obj);
        }
        public int GetHashCode(T obj)
        {
            return RuntimeHelpers.GetHashCode(obj);
        }
        bool IEqualityComparer.Equals(object x, object y)
        {
            return x == y; // This is reference equality! (See explanation below.)
        }
        bool IEqualityComparer<T>.Equals(T x, T y)
        {
            return x == y; // This is reference equality! (See explanation below.)
        }
        /*
         * For anyone wondering why x == y is reference equality,
         * it is because the == operator is a static method,
         * which means it is resolved at compile-time,
         * and at compile-time x and y are of type object so here it resolves to the == operator of object
         * - which is the real reference equality method. (In fact the Object.ReferenceEquals(object, object)
         *  method is simply a redirect to the object equals operator.)
         */
    }
}
