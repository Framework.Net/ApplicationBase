﻿using System;
using System.Linq;


namespace TechnicalTools.Tools
{
    public static class EmptyLookup<TKey, TItem>
    {
        public static ILookup<TKey, TItem> Instance = Create();
        public static ILookup<TKey, TItem> Create() { return new TItem[0].ToLookup(x => default(TKey)); }
    }
}
