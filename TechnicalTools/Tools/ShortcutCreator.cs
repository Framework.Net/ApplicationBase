﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using TechnicalTools.Diagnostics;

namespace TechnicalTools.Tools
{
    // From https://stackoverflow.com/a/14632782/294998
    public static class ShortcutCreator
    {
        public static void CreateOnDesktop(string shortcutTitle, string targetAppPath, string workingDirectory = null, string args = null)
        {
            IShellLink link = (IShellLink)new ShellLink();

            string desktopPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory),
                                              shortcutTitle + ".lnk");

            // Recuperer la position pour la restaurer ? ca marche pas ...
            // Piste serieuse  https://www.codeproject.com/Articles/639486/Save-and-restore-icon-positions-on-desktop
            //                 http://www.williballenthin.com/forensics/shellbags/
            //                 https://superuser.com/a/1225056
            //                 https://social.msdn.microsoft.com/Forums/windows/en-US/d7df8a4d-fc0f-4b62-80c9-7768756456e6/how-can-i-get-desktops-icons-information-?forum=winforms
            //                 https://autohotkey.com/boards/viewtopic.php?f=5&t=27473
            //var sb = new StringBuilder();
            //sb.Append(desktopPath);
            //int f;



            // setup shortcut information
            link.SetDescription(shortcutTitle);
            link.SetPath(targetAppPath);
            workingDirectory = workingDirectory ?? Path.GetDirectoryName(targetAppPath);
            link.SetWorkingDirectory(workingDirectory);
            link.SetArguments(args);
            // link.GetIconLocation(sb, sb.Length, out f); .. marche pas :(

            // save it
            IPersistFile file = (IPersistFile)link;
            file.Save(desktopPath, false);

            ExceptionManager.Instance.IgnoreExceptionAndBreak(() =>
            {
                // Send like F5 to desktop so the position of icon is really commited in system
                // It seems also that it allow us to overwrite the desktop shortcut without the position to be lost (maybe)
                SHChangeNotify(0x8000000, 0x1000, IntPtr.Zero, IntPtr.Zero);
            });
        }

        [System.Runtime.InteropServices.DllImport("Shell32.dll")]
        private static extern int SHChangeNotify(int eventId, int flags, IntPtr item1, IntPtr item2);

        [ComImport]
        [Guid("00021401-0000-0000-C000-000000000046")]
        internal class ShellLink
        {
        }

        [ComImport]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("000214F9-0000-0000-C000-000000000046")]
        internal interface IShellLink
        {
            void GetPath([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszFile, int cchMaxPath, out IntPtr pfd, int fFlags);
            void GetIDList(out IntPtr ppidl);
            void SetIDList(IntPtr pidl);
            void GetDescription([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszName, int cchMaxName);
            void SetDescription([MarshalAs(UnmanagedType.LPWStr)] string pszName);
            void GetWorkingDirectory([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszDir, int cchMaxPath);
            void SetWorkingDirectory([MarshalAs(UnmanagedType.LPWStr)] string pszDir);
            void GetArguments([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszArgs, int cchMaxPath);
            void SetArguments([MarshalAs(UnmanagedType.LPWStr)] string pszArgs);
            void GetHotkey(out short pwHotkey);
            void SetHotkey(short wHotkey);
            void GetShowCmd(out int piShowCmd);
            void SetShowCmd(int iShowCmd);
            void GetIconLocation([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszIconPath, int cchIconPath, out int piIcon);
            void SetIconLocation([MarshalAs(UnmanagedType.LPWStr)] string pszIconPath, int iIcon);
            void SetRelativePath([MarshalAs(UnmanagedType.LPWStr)] string pszPathRel, int dwReserved);
            void Resolve(IntPtr hwnd, int fFlags);
            void SetPath([MarshalAs(UnmanagedType.LPWStr)] string pszFile);
        }
    }
}
