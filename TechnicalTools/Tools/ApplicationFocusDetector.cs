﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;


namespace TechnicalTools.Tools
{
    // from http://stackoverflow.com/a/7162873
    // this code has the advantage of being thread-safe,
    // not requiring the main form (or its handle) and is not WPF or WinForms specific.
    // It will work with child windows (even independent ones created on a separate thread).
    // Also, there's zero setup required.
    public class ApplicationFocusDetector
    {
        /// <summary>Returns true if the current application has focus, false otherwise</summary>
        public static bool IsApplicationActivated()
        {
            var activatedHandle = GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero)
            {
                return false;       // No window is currently activated
            }

            var procId = Process.GetCurrentProcess().Id;
            GetWindowThreadProcessId(activatedHandle, out int activeProcId);

            return activeProcId == procId;
        }


        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);
    }
}
