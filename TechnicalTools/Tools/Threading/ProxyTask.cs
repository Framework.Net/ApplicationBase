﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

using TechnicalTools.Diagnostics;


namespace TechnicalTools.Tools.Threading.Tasks
{
    public class ProxyTask : Task
    {


        #region Action

        #region Wraping des Constructeurs

        public ProxyTask(Action action)
            : base (Proxy.CreateAction(action))
        {
            Proxy proxy = Proxy.GetCreatedInstance(this);
            proxy.Lambda = CompositeAction(proxy, action);
        }
        public ProxyTask(Action action, CancellationToken cancellationToken)
            : base(Proxy.CreateAction(action), cancellationToken)
        {
            Proxy proxy = Proxy.GetCreatedInstance(this);
            proxy.Lambda = CompositeAction(proxy, action);
        }
        public ProxyTask(Action action, TaskCreationOptions creationOptions)
            : base(Proxy.CreateAction(action), creationOptions)
        {
            Proxy proxy = Proxy.GetCreatedInstance(this);
            proxy.Lambda = CompositeAction(proxy, action);
        }
        public ProxyTask(Action action, CancellationToken cancellationToken, TaskCreationOptions creationOptions)
            : base(Proxy.CreateAction(action), cancellationToken, creationOptions)
        {
            Proxy proxy = Proxy.GetCreatedInstance(this);
            proxy.Lambda = CompositeAction(proxy, action);
        }

        public ProxyTask(Action<object> action, object state)
            : base(Proxy.CreateAction(action), state)
        {
            Proxy proxy = Proxy.GetCreatedInstance(this);
            proxy.Lambda = CompositeAction(proxy, action);
        }
        public ProxyTask(Action<object> action, object state, CancellationToken cancellationToken)
            : base(Proxy.CreateAction(action), state, cancellationToken)
        {
            Proxy proxy = Proxy.GetCreatedInstance(this);
            proxy.Lambda = CompositeAction(proxy, action);
        }
        public ProxyTask(Action<object> action, object state, TaskCreationOptions creationOptions)
            : base(Proxy.CreateAction(action), state, creationOptions)
        {
            Proxy proxy = Proxy.GetCreatedInstance(this);
            proxy.Lambda = CompositeAction(proxy, action);
        }
        public ProxyTask(Action<object> action, object state, CancellationToken cancellationToken, TaskCreationOptions creationOptions)
            : base(Proxy.CreateAction(action), state, cancellationToken, creationOptions)
        {
            Proxy proxy = Proxy.GetCreatedInstance(this);
            proxy.Lambda = CompositeAction(proxy, action);
        }
        #endregion

        #region Wrapping des methodes statiques

        public static new Task Run(Action action)
        {
            return Run(action, default(CancellationToken));
        }
        public static new Task Run(Action action, CancellationToken cancellationToken)
        {
            var proxy = Proxy.CreateInstance();
            Task task = null;
            // we assign Task before CompositeAction raise Events
            // We do this in lambda because task is not created yet
            proxy.OriginalLambda = action;
            proxy.Lambda = (Action)(() => { proxy.Task = task; _Proxies.Add(task, proxy); CompositeAction(proxy, action)(); });
            task = System.Threading.Tasks.Task.Run((Action)proxy.Lambda, cancellationToken);
            return task;
        }

        #endregion

        #region Composition des Actions à executer

        static Action         CompositeAction(Proxy proxy, Action action)
        {
            return () =>
            {
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => OnProxyBegin(proxy));
                try
                {
                    action();
                    ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => OnProxySuccess<object>(proxy, false));
                }
                catch (Exception ex)
                {
                    ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => OnProxyFail(proxy, ex));
                    throw;
                }
            };
        }
        static Action<object> CompositeAction(Proxy proxy, Action<object> action)
        {
            return x =>
            {
                CompositeAction(proxy, () => action(x));
            };
        }

        #endregion

        #region Hack specifique pour les constructeurs
        // Cette classe est un hack permettant de passer implicitement une reference vers this dans au constructeur de la classe de base
        // Pour cela elle passe un wrapper, puis on change après coup la lambda wrappé
        public class Proxy
        {
            public object OriginalLambda { get; internal set; } // not intended to be executed !
            public object Lambda         { get; internal set; }
            public Task   Task           { get; internal set; }
            public long   UniqueId       { get; } = Interlocked.Increment(ref UniqueIdSeed);  static long UniqueIdSeed; // why we use our own unique Id ? Because http://blog.stephencleary.com/2013/03/a-few-words-on-taskid-and.html
            public bool   IsFunction     { get { return FuncTypes.Contains(Lambda.GetType()); } }

            // note : Le nombre de type Functions est moins grand que le nombre de type Action
            static readonly HashSet<Type> FuncTypes = new HashSet<Type>(new []{ typeof(Func<>), typeof(Func<,>), typeof(Func<,,>), typeof(Func<,,,>), typeof(Func<,,,,>), typeof(Func<,,,,,>), typeof(Func<,,,,,,>), typeof(Func<,,,,,,,>), typeof(Func<,,,,,,,,>) });

            internal static Action CreateAction(Action action)
            {
                var proxy = new Proxy(true);
                void proxifiedAction()
                {
                    ((Action)proxy.Lambda)();
                }
                proxy.OriginalLambda = action;
                proxy.Lambda = action;
                return proxifiedAction;
            }
            internal static Action<T> CreateAction<T>(Action<T> action)
            {
                var proxy = new Proxy(true);
                void proxifiedAction(T x)
                {
                    ((Action<T>)proxy.Lambda)(x);
                }
                proxy.OriginalLambda = action;
                proxy.Lambda = action;
                return proxifiedAction;
            }
            internal static Proxy CreateInstance()
            {
                return new Proxy(false);
            }

            private Proxy(bool fromTaskConstructor)
            {
                if (fromTaskConstructor)
                {
                    Monitor.Enter(_lock); // assuming GetCreatedInstance will be called right after
                    _current = this;
                }
            }
            static readonly object _lock = new object();
            static Proxy _current;
            internal static Proxy GetCreatedInstance(Task task)
            {
                Proxy current = _current;
                _current = null; // no needed but to be clean and to debug easily
                Monitor.Exit(_lock);
                current.Task = task;
                _Proxies.Add(task, current);
                return current;
            }
        }
        static readonly ConditionalWeakTable<Task, Proxy> _Proxies = new ConditionalWeakTable<Task, Proxy>();

        #endregion

        #endregion Action

        #region Func

        #region Wrapping des méthodes statiques

        public static new Task<TResult> Run<TResult>(Func<TResult> function, CancellationToken cancellationToken)
        {
            var proxy = Proxy.CreateInstance();
            Task<TResult> task = null;
            var taskAssigned = new ManualResetEvent(false);
            // We assign Task before CompositeAction raise Events
            // We do this in lambda because task is not created yet, but will be in lambda
            proxy.Lambda = (Func<TResult>)(() => { taskAssigned.WaitOne(); taskAssigned.Dispose(); proxy.Task = task; _Proxies.Add(task, proxy); return CompositeFunc(proxy, function)(); });
            proxy.OriginalLambda = function;
            task = Task.Run((Func<TResult>)proxy.Lambda, cancellationToken);
            taskAssigned.Set();
            return task;
        }
        public static new Task<TResult> Run<TResult>(Func<TResult> function)
        {
            return Run(function, default(CancellationToken));
        }
        public static new Task Run(Func<Task> function)
        {
            return Run(function, default(CancellationToken));
        }
        public static new Task Run(Func<Task> function, CancellationToken cancellationToken)
        {
            return Run<Task>(function, cancellationToken);
        }
        public static new Task<TResult> Run<TResult>(Func<Task<TResult>> function)
        {
            return Run(function, default(CancellationToken));
        }
        public static new Task<TResult> Run<TResult>(Func<Task<TResult>> function, CancellationToken cancellationToken)
        {
            // @Dev : We cant write the following line to factorize
            //        because the wrapping of Task<Result> is not handled the same way by default (see lookForOce in UnwrapPromise internal class):
            //return Run<Task<TResult>>(function, cancellationToken).Unwrap();

            var proxy = Proxy.CreateInstance();
            Task<TResult> task = null;
            var taskAssigned = new ManualResetEvent(false);
            // See comment in method above
            proxy.Lambda = (Func<Task<TResult>>)(() => { taskAssigned.WaitOne(); taskAssigned.Dispose(); proxy.Task = task; _Proxies.Add(task, proxy); return CompositeFunc(proxy, function)(); });
            proxy.OriginalLambda = function;
            task = Task.Run((Func<Task<TResult>>)proxy.Lambda, cancellationToken);
            taskAssigned.Set();
            return task;
        }

        #endregion


        #region Composition des functions à executer

        static Func<            TResult> CompositeFunc<            TResult>(Proxy proxy, Func<            TResult> func)
        {
            return () =>
            {
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => OnProxyBegin(proxy));
                try
                {
                    var result = func();
                    ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => OnProxySuccess(proxy, true, result));
                    return result;
                }
                catch (Exception ex)
                {
                    ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => OnProxyFail(proxy, ex));
                    throw;
                }
            };
        }
        static Func<T1,         TResult> CompositeFunc<T1,         TResult>(Proxy proxy, Func<T1,         TResult> func)
        {
            return x =>
            {
                return CompositeFunc(proxy, () => func(x))();
            };
        }
        static Func<T1, T2,     TResult> CompositeFunc<T1, T2,     TResult>(Proxy proxy, Func<T1, T2,     TResult> func)
        {
            return (x, y) =>
            {
                return CompositeFunc(proxy, () => func(x, y))();
            };
        }
        static Func<T1, T2, T3, TResult> CompositeFunc<T1, T2, T3, TResult>(Proxy proxy, Func<T1, T2, T3, TResult> func)
        {
            return (x, y, z) =>
            {
                return CompositeFunc(proxy, () => func(x, y, z))();
            };
        }

        #endregion

        #endregion Func

        #region Fonctionalités finales !

        public static Proxy GetProxyOf(Task task)
        {
            _Proxies.TryGetValue(task, out var proxy);
            return proxy;
        }

        public static event Action<Proxy> ProxyBegin;
        static void OnProxyBegin(Proxy proxy)
        {
            if (ProxyBegin != null)
                ProxyBegin.Invoke(proxy);
            Debug.Assert(proxy.Task != null);
            if (proxy.Task is ProxyTask pTask)
                pTask.OnBegin(proxy);
        }
        public event Action<ProxyTask, Proxy> Begin;
        void OnBegin(Proxy proxy)
        {
            if (Begin != null)
                Begin.Invoke(this, proxy);
        }


        public static event Action<Proxy, bool, object> ProxySuccess;
        static void OnProxySuccess<TResult>(Proxy proxy, bool hasResult, TResult result = default(TResult))
        {
            if (ProxySuccess != null)
                ProxySuccess.Invoke(proxy, hasResult, result);
            if (proxy.Task is ProxyTask pTask)
                pTask.OnSuccess(proxy);
        }
        public event Action<ProxyTask, Proxy> Success;
        void OnSuccess(Proxy proxy)
        {
            if (Success != null)
                Success.Invoke(this, proxy);
        }

        public static event Action<Proxy, Exception> ProxyFail;
        static void OnProxyFail(Proxy proxy, Exception ex)
        {
            if (ProxyFail != null)
                ProxyFail.Invoke(proxy, ex);
            if (proxy.Task is ProxyTask pTask)
                pTask.OnFail(proxy);
        }
        public event Action<ProxyTask, Proxy> Fail;
        void OnFail(Proxy proxy)
        {
            if (Fail != null)
                Fail.Invoke(this, proxy);
        }

        #endregion
    }


}
