﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;


namespace TechnicalTools.Tools.Threading
{
    /// <summary>
    /// Permet de configurer les threads plus finement qu'avec le ThreadPool classique.
    /// car les treads utilisés sont dans TechnicalTools
    /// </summary>
    public class ThreadPool : IDisposable
    {
        public string Name { get; private set; }

        public ThreadPool(string name)
        {
            Name = name;
            _Pool = new ConcurrentPool<EndlessThread>(CreateThread);
        }
        protected int _thread_created_count;

        protected virtual EndlessThread CreateThread()
        {
            return new EndlessThread(this, Interlocked.Increment(ref _thread_created_count));
        }

        public virtual IThreadInfo Start(string name, Action start)
        {
            // ReSharper disable AccessToModifiedClosure
            EndlessThread thread = null;
            void managed_start()
            {
                bool success = false;
                try
                {
                    start();
                    success = true;
                }
                catch (Exception ex)
                {
                    // uncatchable Exception Occured
                    if (ex is ThreadAbortException || ex is StackOverflowException || ex is ThreadInterruptedException || ex is TypeInitializationException /*|| ex is OutOfMemoryException*/)
                    {
                        success = false;
                        _Pool.PutInstance(CreateThread());
                    }
                    else
                        throw;
                }
                finally
                {
                    Debug.Assert(thread != null, "thread != null");
                    thread.Clean(); // Détache le traitement qui potentiellement garde des reference sur des objet (fenetres etc)

                    // Si ca c'est bien terminé, on rend le thread, sinon on le ne rend pas, un autre thread sera créé.
                    // Cela permet de gérer l'erreur AbortThreadException et toutes les autres exceptions qui ne peuvent être catchées définitivement.
                    // En outre cela fait remonter l'information d'erreur (crash... ou autre)
                    lock (_Pool)
                    {
                        _RunningThreads.Remove(thread);
                        if (success)
                            _Pool.PutInstance(thread);
                    }
                }
            }
            lock (_Pool)
            {
                if (_disposed)
                    return null;
                thread = _Pool.GetOrCreateInstance();
                _RunningThreads.Add(thread);
                thread.Setup(name, managed_start);
                thread.Run();
            }
            return thread.CreateInfo();
            // ReSharper restore AccessToModifiedClosure
        }


        public interface IThreadInfo
        {
            bool       IsDone { get; }
            ThreadPool Owner  { get; }
        }

        protected class ThreadInfo : IThreadInfo
        {
            protected volatile EndlessThread Thread;

            public ThreadInfo(EndlessThread thread)
            {
                Thread = thread;
            }

            public bool IsDone
            {
                get
                {
                    var th = Thread;
                    return th == null || th.IsDone;
                }
            }

            public ThreadPool Owner
            {
                get
                {
                    return Thread?.Owner;
                }
            }
        }

        public void Dispose()
        {
            if (!_disposed)
                lock (_Pool)
                {
                    // Empeche que le process reste dans le gestionaire des tâches car tous les threads doivent terminer ?
                    var all_threads = Enumerable.Range(0, _Pool.Count).Select(_ => _Pool.GetOrCreateInstance()).ToList();
                    all_threads.AddRange(_RunningThreads);
                    foreach (var th in all_threads)
                        th.AskForProperTermination();
                    var max_wait = new TimeSpan(0, 3, 0);  // 180 secondes pour terminer tout les threads, apres on kill tout !
                    var time_when_asked = DateTime.Now;
                    foreach (var th in all_threads)
                        th.WaitTerminationOrKill(max_wait - (DateTime.Now - time_when_asked));
                    _disposed = true;
                }
        }
        bool _disposed;

        readonly ConcurrentPool<EndlessThread> _Pool;
        readonly List<EndlessThread> _RunningThreads = new List<EndlessThread>();



        protected class EndlessThread
        {
            public bool IsDone
            {
                get { return _start != null; }
            }

            public ThreadPool Owner { get; private set; }

            public virtual void Setup(string name, Action start)
            {
                Debug.Assert(start != null);
                _start = RealThread.PrepareAsThreadTask(name, _ => start(), true);
            }
            public void Clean() // Prevent memory leak
            {
                _start = null;
            }
            public void Run()
            {
                Debug.Assert(_start != null);
                _autoReset.Set();
            }

            public void AskForProperTermination()
            {
                _leaving = true; // Il y a aussi une propriete "IsBackground" que l'on peut mettre a true sur les thread .Net. A creuser...
                _start = Action_Extensions.DoNothing; // si jamais la boucle while de la methode ThreadStart reboucle
                _autoReset.Set();
            }

            public void WaitTerminationOrKill(TimeSpan max_time_to_wait)
            {
                if (max_time_to_wait.TotalMilliseconds < 0)
                    max_time_to_wait = TimeSpan.Zero;
                if (!RealThread.Join((int) max_time_to_wait.TotalMilliseconds))
#pragma warning disable CS0618 // Le type ou le membre est obsolète
                    RealThread.Abort();
#pragma warning restore CS0618 // Le type ou le membre est obsolète
            }

            public EndlessThread(ThreadPool owner, int index)
            {
                Owner = owner;
                RealThread = new Thread(owner.Name + "'s " + index, ThreadStart, ThreadPriority.BelowNormal, true);
                RealThread.Start();
            }

            protected readonly Thread RealThread;
            protected volatile ParameterizedThreadStart _start;
            readonly AutoResetEvent _autoReset = new AutoResetEvent(false);
            volatile bool _leaving;

            void ThreadStart()
            {
                while (!_leaving)
                {
                    _autoReset.WaitOne();
                    Debug.Assert(_start != null);
                    _start(null);
                }
            }

            public ThreadInfo CreateInfo()
            {
                return BuildInfo(); // TODO : Faire en sorte que quand _start est remis à null, on supprime le field Thread de ThreadInfo
            }

            public virtual ThreadInfo BuildInfo()
            {
                return new ThreadInfo(this);
            }
        }
    }
}
