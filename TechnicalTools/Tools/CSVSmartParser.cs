﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Dynamic;


namespace TechnicalTools.Tools
{
    /// <summary>
    /// A thread safe class to parse CSV file smartly
    /// </summary>
    public class CSVSmartParser
    {
        /// <summary>
        /// Parse file and returns records, titles and warnings.
        /// </summary>
        /// <param name="filename">csv File to parse</param>
        /// <param name="findTitles">True to detected title &amp; super-titles in the 2 first line(s)</param>
        /// <param name="useTitleAsField">True to change the fieldname as closed as possible than the title. This is epscially usefull for UI library that has interesting default behavior</param>
        /// <returns>records, warning, and titles. If <paramref name="findTitles"/> is false, returned titles have no useful title but metadata are stil useful.</returns>
        public (IEnumerable<ExpandoObject> records, IReadOnlyList<IColumnInfo> titles, List<string> warnings)
               Read(string filename, bool findTitles = true, bool useTitleAsField = false)
        {
            var lines = File.ReadLines(filename);
            var cellEnumerator = TokenizeCellValuesAndNewLines(lines);
            return DoRead(cellEnumerator, findTitles, useTitleAsField);
        }
        /// <summary>Same as <see cref="Read(string, bool, bool)"/> but with an array of string instead of a fileanme</summary>
        public (IEnumerable<ExpandoObject> records, IReadOnlyList<IColumnInfo> titles, List<string> warnings)
            Read(IEnumerable<string> lines, bool findTitles = true, bool useTitleAsField = false)
        {
            var cellEnumerator = TokenizeCellValuesAndNewLines(lines);
            return DoRead(cellEnumerator, findTitles, useTitleAsField);
        }
        (IEnumerable<ExpandoObject> records, IReadOnlyList<IColumnInfo> titles, List<string> warnings)
               DoRead(IEnumerable<string> cellEnumerator, bool findTitles, bool useTitleAsField)
        {
            var iterator = cellEnumerator.GetEnumerator();

            // Capture first lines to handle them later as title line (or not)
            var firstLines = findTitles
                           ? CaptureFirstLines(iterator, 2)
                           : new List<List<string>>();

            // Capture [other] record lines

            var defaultTitles = new List<ColumnInfo>(); // will be build on the go and will be replaced (maybe) by content of firstlines later
            var records = new List<ExpandoObject>();
            if (findTitles) // Anticipate space for inserting firstLines in the beginning later
                if (firstLines.Count == 0) // Case when file is empty
                    return (records, defaultTitles, null);
                else
                    records.AddRange(firstLines.Select(_ => (ExpandoObject)null));
            else if (!iterator.MoveNext()) // go to same state than when findTitles is enabled
                return (records, defaultTitles, null); // Case when file is empty
            while (true)
            {
                var item = ReadDataLine(iterator, defaultTitles, records);
                if (item == null) // end of file met
                    break;
                records.Add(item);
            }


            (List<ColumnInfo>           finalTitles,
             IEnumerable<ExpandoObject> finalRecords) = findTitles  // Now deal with all cases about titles
                                                      ? DispatchFirstLineToDataOrTitles(firstLines, defaultTitles, records)
                                                      : (defaultTitles, records);
            while (records.Last() != null && records.Last().All(kvp => kvp.Value == null || kvp.Value is string str && string.IsNullOrWhiteSpace(str)))
                records.RemoveAt(records.Count - 1);

            if (useTitleAsField)
                finalRecords = RenameFieldsWithTitles(finalRecords, finalTitles).ToList();


            var warnings = new List<string>();
            foreach (var col in finalTitles)
                warnings.AddRange(col.FinalizeAndGetAllWarnings(finalRecords));

            return (finalRecords, finalTitles, warnings);
        }

        IEnumerable<ExpandoObject> RenameFieldsWithTitles(IEnumerable<ExpandoObject> records, IReadOnlyList<ColumnInfo> columns)
        {
            var oldNames = new string[columns.Count];

            for (int c = 0; c < columns.Count; ++c)
            {
                var col = columns[c];
                var fieldBasename = string.IsNullOrWhiteSpace(col.Title) ? "unkown" : col.Title.Trim();
                var newFieldName = fieldBasename;
                int i = 1;
                while (oldNames.Contains(newFieldName))
                    newFieldName = fieldBasename + " (" + ++i + ")";
                oldNames[c] = col.FieldName;
                col.FieldName = newFieldName;
            }
            foreach (var col in columns)
                System.Diagnostics.Debug.Assert(!oldNames.Contains(col.FieldName));

            int r = 0;
            List<ColumnInfo> cols = null;
            foreach (IDictionary<string, object> record in records)
            {
                ++r;
                // In case there are more titles thant field in object
                cols = cols ?? columns.Take(record.Count).ToList();
                for (int c = 0; c < cols.Count; ++c)
                {
                    var col = columns[c];
                    var oldName = oldNames[c];
                    object value;
                    if (record.TryGetValue(oldName, out value))
                    {
                        record.Remove(oldName);
                        record[col.FieldName] = value;
                    }
                }
                yield return (ExpandoObject)record;
            }
        }


        static List<List<string>> CaptureFirstLines(IEnumerator<string> iterator, int lineCountToCapture)
        {
            List<List<string>> firstLines = new List<List<string>>();
            while (firstLines.Count < lineCountToCapture) // title and supertitles lines or one line of data
            {
                var line = new List<string>();
                while (iterator.MoveNext())
                {
                    line.Add(iterator.Current);
                    if (iterator.Current == null)
                        break;
                }
                if (line.Count <= 1) // only the "null" termination token means we are already at end of file
                    break;
                // Skip empty lines at beginning of file
                if (firstLines.Count > 0 || line.Any(cell => !string.IsNullOrWhiteSpace(cell)))
                    firstLines.Add(line);
            }

            return firstLines;
        }

        // Fill currentTitles & records with the content of firstLines and return the final couple of record and titles
        (List<ColumnInfo> finalTitles, IEnumerable<ExpandoObject> finalRecords) DispatchFirstLineToDataOrTitles(List<List<string>> firstLines, List<ColumnInfo> currentTitles, List<ExpandoObject> records)
        {
            var line1AsTitles = ReadTitleLine(firstLines[0]);
            var line1Types = new List<ColumnInfo>();
            var item1 = ReadDataLine(firstLines[0].GetEnumerator(), line1Types, new List<ExpandoObject>());

            // only one line in stream and no records :/
            if (firstLines.Count == 1)
            {
                // Heuristics: a line of only string is probably a title line
                if (LineLookLikeTitleLine(line1Types, records))
                    return (line1AsTitles, records.Skip(1));
                records[0] = item1;
                return (line1Types, records.Skip(item1 == null ? 1 : 0));
            }

            var line2AsTitles = ReadTitleLine(firstLines[1]);
            var line2Types = new List<ColumnInfo>();
            var item2 = ReadDataLine(firstLines[1].GetEnumerator(), line2Types, new List<ExpandoObject>());

            // Heuristics: lines of only string are probably title and supertitle lines
            if (LineLookLikeTitleLine(line1Types, records))
                if (LineLookLikeTitleLine(line2Types, records))
                {
                    ColumnInfo currentSuperTitle = new ColumnInfo(null, line1Types) { Title = "" };
                    for (int i = 0; i < Math.Max(line2AsTitles.Count, currentTitles.Count); ++i)
                    {
                        var curCol = currentTitles.ElementAtOrDefault(i);
                        if (curCol == null)
                            currentTitles.Add(curCol = line2AsTitles[i]);
                        else if (i < line2AsTitles.Count)
                            curCol.Title = line2AsTitles[i].Title;
                        var scol = line1AsTitles.ElementAtOrDefault(i) ?? currentSuperTitle;
                        if (!scol.WasEmpty)
                            currentSuperTitle = scol;
                        curCol.SuperTitle = currentSuperTitle.Title;
                    }
                    return (currentTitles, records.Skip(2));
                }
                else
                {
                    // Parse again to update column meta data
                    item1 = ReadDataLine(firstLines[1].GetEnumerator(), currentTitles, records);
                    records[1] = item1;
                    for (int i = 0; i < line1AsTitles.Count; ++i)
                    {
                        var curCol = currentTitles.ElementAtOrDefault(i);
                        if (curCol == null)
                            currentTitles.Add(line1AsTitles[i]);
                        else
                            curCol.Title = line1AsTitles[i].Title;
                    }
                    return (currentTitles, records.Skip(item1 == null ? 2 : 1));
                }
            // All lines seem to be data

            // Parse again to update column meta data
            item1 = ReadDataLine(firstLines[0].GetEnumerator(), currentTitles, records);
            records[0] = item1;
            item2 = ReadDataLine(firstLines[1].GetEnumerator(), currentTitles, records);
            records[1] = item2;
            return (currentTitles, records);
        }

        static bool LineLookLikeTitleLine(List<ColumnInfo> titles, List<ExpandoObject> records)
        {
            int titleAsString = 0;
            int notString = 0;
            foreach (var title in titles)
            {
                if (title.SupposedType == typeof(string))
                    ++titleAsString;
                else if (!title.IsNullable)
                    ++notString;
            }
            return titleAsString == titles.Count || titleAsString > 0 && notString == 0 ||
                 records.Count > 2 && titles.Count > records[2].Count();
        }
        static List<ColumnInfo> ReadTitleLine(IEnumerable<string> tokens)
        {
            var cols = new List<ColumnInfo>();
            foreach (var cellValue in tokens)
            {
                if (cellValue == null)
                    break; // means we are at the end of columns line
                var col = new ColumnInfo(cellValue, cols);
                cols.Add(col);
            }
            return cols;
        }

        // Parsing line of data
        static ExpandoObject ReadDataLine(IEnumerator<string> iterator, List<ColumnInfo> columns, IReadOnlyCollection<ExpandoObject> records)
        {
            bool canContinue = true;
            IDictionary<string, object> item;
            do
            {
                item = new ExpandoObject();
                int colindex = 0;

                while ((canContinue = iterator.MoveNext()) &&
                       iterator.Current != null) // means we finished to parse a line of data
                {
                    // Get column or be tolerant with shifted column data (too many cell separator) or missing title (at the right of file)
                    var col = columns.ElementAtOrDefault(colindex);
                    if (col == null)
                        columns.Add(col = new ColumnInfo(null, columns));
                    bool needConvertOldValues = false;
                    while (true)
                        try
                        {
                            // Try to parse using the most optimistic parsing conversion method
                            var value = col.ParsingStrategy.ParseFunction(iterator.Current, col, records);
                            // In case of success we store the result in item (so we build "item" little by little)
                            if (value == null)
                                col.IsNullable = true;
                            item[col.FieldName] = value;
                            break;
                        }
                        catch
                        {
                            // col.Parse failed, the estimed type of this column data does not fit the real data (ex: we estimated column stores integer, but we found text ..)
                            // Upgrade the supposed type of column to a more permissive one (maybe still not the good one though)
                            col.ParsingStrategy = ParsingStrategies[col.ParsingStrategy.FallbackTypeToParse];
                            needConvertOldValues = true;
                        }
                    // If supposed type has changed,
                    // we need to convert old parsed values, to ensure the consistency of parsed values' runtime types.
                    if (needConvertOldValues)
                        foreach (IDictionary<string, object> oldItem in records)
                            if (oldItem != null)
                                oldItem[col.FieldName] = !oldItem.TryGetValue(col.FieldName, out object obj) || obj == null
                                                       ? null
                                                       : Convert.ChangeType(oldItem[col.FieldName], col.SupposedType);
                    ++colindex;
                }
            } while (canContinue &&  // not end of stream
                     (item.Count == 0 || item.Count == 1 && item.Values.All(v => v is string str && string.IsNullOrWhiteSpace(str))) // but current item is empty
                     ); // tolerant to blank line => ignored
            if (item.Count() == 0) // end of file met
                return null;
            return (ExpandoObject)item;
        }

        // Return cell values, null value are returned at end of data line
        // (not physical end of line, because a record can be stored on multiple lines)
        static IEnumerable<string> TokenizeCellValuesAndNewLines(IEnumerable<string> lines)
        {
            IEnumerator<string> lineIterator = lines.GetEnumerator();
            if (!lineIterator.MoveNext())
                yield break;
            char sep = '\0';

            string truncatedValue = null;
            do
            {
                var line = lineIterator.Current;
                int i = 0;
                while (i < line.Length)
                {
                    int next;
                    if (line[i] == '"' || truncatedValue != null)
                    {
                        var q = i + (truncatedValue == null ? 1 : 0);
                        while ((next = line.IndexOf('"', q)) > 0 &&
                               next + 1 < line.Length && line[next + 1] == '"')
                            q = next + 2; // there is two double quotes, it is actually one double quote escaped
                        i += truncatedValue == null ? 1 : 0;
                        if (next == -1)
                        {
                            truncatedValue += line.Substring(i, line.Length - i);
                            i = line.Length; // neutralize final sweep
                            break;
                        }
                        yield return (truncatedValue + line.Substring(i, next - i)).Replace("\"\"", "\"");
                        truncatedValue = null;
                        i = next + 2; // jump over final " and next sep (if it exist)
                        continue;
                    }
                    if (sep == '\0')
                    {
                        next = line.IndexOfAny(knownSeparators, i);
                        if (next >= 0)
                            sep = line[next];
                    }
                    if ((next = line.IndexOf(sep, i)) >= 0)
                    {
                        yield return line.Substring(i, next - i);
                        i = next + 1;
                    }
                    else
                        break;
                }
                if (truncatedValue != null)
                    truncatedValue += Environment.NewLine;
                else
                {
                    if (i < line.Length || // sweep final value
                        line.Length > 0 && line[line.Length - 1] == sep || // special case when last cellvalue is empty
                        i == 0) // special case when blank line
                        yield return line.Substring(i, line.Length - i);
                    yield return null;
                }
            } while (lineIterator.MoveNext());
        }
        static readonly char[] knownSeparators = new[] { '\t', ';', ',' };
        public interface IColumnInfo
        {
            string  SuperTitle      { get; }
            string  Title           { get; }
            string  FieldName       { get; } // fieldname is unique
            bool    WasEmpty        { get; }
            Type    SupposedType    { get; }
            bool    IsNullable      { get; }
            int     MaxLength       { get; } // Data when SupposedType == typeof(string)
        }
        class ColumnInfo : IColumnInfo
        {
            public string   SuperTitle   { get; set; }
            public string   Title        { get; set; }
            public string   FieldName    { get; internal set; }
            public bool     WasEmpty     { get; set; }
            public Type     SupposedType { get { return ParsingStrategy.ParsedType; } }
            public bool     IsNullable   { get; set; }
            public int      MaxLength    { get; set; }

            public ParsingStrategy ParsingStrategy { get; set; }

            // Data when SupposedType == typeof(DateTime)
            public string DateFormat;
            public bool? DayAndMonthInEnglishFormat;
            public bool HourIsMaybeInEnglishFormat = true;

            public ColumnInfo(string cellValue, IReadOnlyCollection<ColumnInfo> existingColumns)
            {
                WasEmpty = string.IsNullOrWhiteSpace(cellValue);
                var baseName = WasEmpty
                             ? ColumnDefaultTitle
                             : cellValue;
                var uniqueTitle = baseName;
                int i = 0;
                // Not mandatory but disambiguise in case there is multiple column with same name (or empty)
                while (existingColumns.Any(c => c.Title == uniqueTitle))
                    uniqueTitle = baseName + " (" + ++i + ")";
                Title = uniqueTitle;
                FieldName = "field" + existingColumns.Count.ToStringInvariant();
                ParsingStrategy = ParsingStrategies[typeof(DateTime)];
            }
            public const string ColumnDefaultTitle = "<no title>";

            internal void AddWarning(Func<ColumnInfo, string> finalizeMessage)
            {
                _warnings = _warnings ?? new List<Func<ColumnInfo, string>>();
                _warnings.Add(finalizeMessage);
            }
            List<Func<ColumnInfo, string>> _warnings;

            public IEnumerable<string> FinalizeAndGetAllWarnings(IEnumerable<IDictionary<string, object>> records)
            {
                if (_warnings != null)
                    foreach (var warningConstructor in _warnings)
                        yield return warningConstructor(this);

                if (SupposedType == typeof(DateTime))
                    if (string.IsNullOrWhiteSpace(DateFormat)) // means all values are null
                        IsNullable = true; // to fallback to next if
                    else
                    {
                        if (DayAndMonthInEnglishFormat == null)
                            yield return $"There is no way to decide if date format on column \"{Title}\" is in english format or not (ie: day and month inverted). Assuming it will be ALWAYS in french (ie: day before month)!";
                        if (HourIsMaybeInEnglishFormat)
                            yield return $"There is no way to decide if hour format on column \"{Title}\" is 12 or 24 format. Assuming it will ALWAYS be in 24h format!";
                    }

                if (IsNullable && records.Cast<IDictionary<string, object>>()
                                         .All(it => !it.TryGetValue(FieldName, out object value) || value == null))
                    ParsingStrategy = ParsingStrategies[typeof(string)];
                if (SupposedType == typeof(string))
                {
                    MaxLength += MaxLength / 4; // add 25% just in case
                    if (MaxLength == 0)
                        MaxLength = int.MaxValue;
                }
            }
        }

        #region Static part about value parsing

        static readonly Dictionary<Type, ParsingStrategy> ParsingStrategies = new List<ParsingStrategy>()
            {
                new ParsingStrategy(typeof(DateTime), ParseDateTime, typeof(TimeSpan)),
                new ParsingStrategy(typeof(TimeSpan), ParseTimeSpan, typeof(bool)),
                new ParsingStrategy(typeof(bool),     ParseBool,     typeof(byte)),
                new ParsingStrategy(typeof(byte),     ParseByte,     typeof(short)),
                new ParsingStrategy(typeof(short),    ParseShort,    typeof(int)),
                new ParsingStrategy(typeof(int),      ParseInt,      typeof(long)),
                new ParsingStrategy(typeof(long),     ParseLong,     typeof(decimal)),
                new ParsingStrategy(typeof(decimal),  ParseDecimal,  typeof(string)),
                new ParsingStrategy(typeof(string),   ParseString,   null),
            }.ToDictionary(pf => pf.ParsedType);
        struct ParsingStrategy
        {
            public readonly Type      ParsedType;
            public readonly ParseFunc ParseFunction;
            public readonly Type      FallbackTypeToParse;

            public ParsingStrategy(Type pt, ParseFunc pf, Type fbt)
            {
                ParsedType = pt;
                ParseFunction = pf;
                FallbackTypeToParse = fbt;
            }
        }
        delegate object ParseFunc(string cellValue, ColumnInfo col, IReadOnlyCollection<ExpandoObject> previousItemToFix);

        // Just to allow developper to ignore this type of exception at runtime
        class ParsingException : Exception
        {
            public ParsingException(string msg, Exception innerException) : base(msg, innerException) { }
        }

        static object ParseDateTime(string str, ColumnInfo col, IReadOnlyCollection<ExpandoObject> items)
        {
            if (string.IsNullOrEmpty(str))
                return null;
            DateTime value = DateTime.MinValue;
            string format = null;
            try
            {
                if (DateTime.TryParseExact(str, (format = "yyyy/MM/dd HH:mm:ss"), CultureInfo.InvariantCulture, DateTimeStyles.None, out value) ||
                    DateTime.TryParseExact(str, (format = "yyyy/MM/dd"), CultureInfo.InvariantCulture, DateTimeStyles.None, out value) ||
                    DateTime.TryParseExact(str, (format = "yyyy-MM-dd HH:mm:ss"), CultureInfo.InvariantCulture, DateTimeStyles.None, out value) ||
                    DateTime.TryParseExact(str, (format = "yyyy-MM-dd"), CultureInfo.InvariantCulture, DateTimeStyles.None, out value))
                {
                    col.DayAndMonthInEnglishFormat = false; // no ambiguity
                    return value;
                }
                if (DateTime.TryParseExact(str, (format = "dd/MM/yyyy HH:mm:ss"), CultureInfo.InvariantCulture, DateTimeStyles.None, out value) ||
                    DateTime.TryParseExact(str, (format = "dd/MM/yyyy"), CultureInfo.InvariantCulture, DateTimeStyles.None, out value) ||
                    DateTime.TryParseExact(str, (format = "dd-MM-yyyy HH:mm:ss"), CultureInfo.InvariantCulture, DateTimeStyles.None, out value) ||
                    DateTime.TryParseExact(str, (format = "dd-MM-yyyy"), CultureInfo.InvariantCulture, DateTimeStyles.None, out value))
                {
                    if (value.Day > 12)
                        col.DayAndMonthInEnglishFormat = false;
                    return value;
                }
                if (DateTime.TryParseExact(str, (format = "MM/dd/yyyy HH:mm:ss"), CultureInfo.InvariantCulture, DateTimeStyles.None, out value) ||
                    DateTime.TryParseExact(str, (format = "MM/dd/yyyy"), CultureInfo.InvariantCulture, DateTimeStyles.None, out value) ||
                    DateTime.TryParseExact(str, (format = "MM-dd-yyyy HH:mm:ss"), CultureInfo.InvariantCulture, DateTimeStyles.None, out value) ||
                    DateTime.TryParseExact(str, (format = "MM-dd-yyyy"), CultureInfo.InvariantCulture, DateTimeStyles.None, out value))
                {
                    if (value.Day > 12)
                    {
                        if (col.DateFormat != format && col.DayAndMonthInEnglishFormat != true)
                        {
                            foreach (IDictionary<string, object> item in items)
                            {
                                if (item == null)
                                    continue;
                                var dt = (DateTime)item[col.FieldName];
                                item[col.FieldName] = new DateTime(dt.Year, dt.Day /*reversed*/, dt.Month /*reversed*/, dt.Hour, dt.Minute, dt.Second)
                                                        .AddTicks(dt.Ticks % TimeSpan.TicksPerSecond);
                            }
                        }
                        col.DayAndMonthInEnglishFormat = true;
                    }
                    return value;
                }
                format = null;
                throw new ParsingException($"Value \"{str}\" is not defining an date or a datetime!", null);
            }
            finally
            {
                if (format != null && format.Length > 0)
                {
                    if (col.DateFormat != null && col.DateFormat != format)
                        col.AddWarning(c => $"Date format of column \"{c.Title}\" is not the same on all line! It was \"{c.DateFormat}\" then it changed to \"{format}\"!");
                    if (value.Hour > 12)
                        col.HourIsMaybeInEnglishFormat = false;
                    col.DateFormat = format;
                }
            }
        }
        static object ParseTimeSpan(string str, ColumnInfo col, IReadOnlyCollection<ExpandoObject> items)
        {
            if (str.Trim().Length == 0)
                return null;
            if (!TimeSpan.TryParseExact(str, "hh':'mm':'ss", CultureInfo.InvariantCulture, TimeSpanStyles.None, out TimeSpan value))
                throw new ParsingException($"Value \"{str}\" is not defining a time!", null);
            return value;
        }
        static object ParseBool(string str, ColumnInfo col, IReadOnlyCollection<ExpandoObject> items)
        {
            if (str.Trim().Length == 0)
                return null;
            if (trueValues.Contains(str.ToLowerInvariant()))
                return true;
            if (falseValues.Contains(str.ToLowerInvariant()))
                return false;
            throw new ParsingException($"Value \"{str}\" is not defining a boolean!", null);
        }
        static readonly string[] trueValues = new string[] { "true", "y", "yes", "1" };
        static readonly string[] falseValues = new string[] { "false", "n", "no", "0" };
        static object ParseByte(string str, ColumnInfo col, IReadOnlyCollection<ExpandoObject> items)
        {
            if (str.Trim().Length == 0)
                return null;
            if (!byte.TryParse(str.Replace(" ", ""), NumberStyles.Any, CultureInfo.InvariantCulture, out byte value))
                throw new ParsingException($"Argument \"{str}\" is not defining a byte number!", null);
            return value;
        }
        static object ParseShort(string str, ColumnInfo col, IReadOnlyCollection<ExpandoObject> items)
        {
            if (str.Trim().Length == 0)
                return null;
            if (!short.TryParse(str.Replace(" ", ""), NumberStyles.Any, CultureInfo.InvariantCulture, out short value))
                throw new ParsingException($"Argument \"{str}\" is not defining a short number!", null);
            return value;
        }
        static object ParseInt(string str, ColumnInfo col, IReadOnlyCollection<ExpandoObject> items)
        {
            if (str.Trim().Length == 0)
                return null;
            if (!int.TryParse(str.Replace(" ", ""), NumberStyles.Any, CultureInfo.InvariantCulture, out int value))
                throw new ParsingException($"Argument \"{str}\" is not defining a int number!", null);
            return value;
        }
        static object ParseLong(string str, ColumnInfo col, IReadOnlyCollection<ExpandoObject> items)
        {
            if (str.Trim().Length == 0)
                return null;
            if (!long.TryParse(str.Replace(" ", ""), NumberStyles.Any, CultureInfo.InvariantCulture, out long value))
                throw new ParsingException($"Argument \"{str}\" is not defining a long number!", null);
            return value;
        }
        static object ParseDecimal(string str, ColumnInfo col, IReadOnlyCollection<ExpandoObject> items)
        {
            //long value;
            //if (!long.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
            //    throw new UserUnderstandableException($"Argument \"{str}\" is not defining an number!", null);
            //prop.SetValue(this, Convert.ChangeType(value, nonNullablePropType));
            if (str.Trim().Length == 0)
                return null;
            if (!decimal.TryParse(str.Replace(" ", ""), NumberStyles.Any, CultureInfo.InvariantCulture, out decimal value))
                throw new ParsingException($"Value \"{str}\" is not defining a decimal number!", null);
            return value;
        }
        static object ParseString(string str, ColumnInfo col, IReadOnlyCollection<ExpandoObject> items)
        {
            var s  = str.Trim();
            if (s.Length == 0)
                return null;
            col.MaxLength = Math.Max(col.MaxLength, s.Length);
            return s;
        }

        #endregion
    }
}
