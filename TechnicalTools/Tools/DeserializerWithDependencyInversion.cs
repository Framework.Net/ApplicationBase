﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

using TechnicalTools.Model;


namespace TechnicalTools.Tools
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ConfigTypeAttribute : Attribute
    {
        public string AssemblyName { get; set; }
        public string FullTypeName { get; set; }

        public ConfigTypeAttribute() { }
        public ConfigTypeAttribute(Type type)
        {
            FullTypeName = type.FullName;
        }
    }

    /// <summary>
    /// This class allows to deserialize object with dependency inversion
    /// Example with the following classes :
    /// <code>
    /// namespace MyApplication.Business // in project/assembly MyApplication.Business
    /// {
    ///     public class Config // let's say this class represent your global application config
    ///     {
    ///         // That is composed from other tinier business module that have their owns config class too
    ///         public SpecificTinyBusinessDomain.Config ForSpecific { get; set; } = new SpecificTinyBusinessDomain.Config();
    ///
    ///         // if we want to include also a config object about UI, 
    ///         // we need to use an attribute to overcome the circular dependency it would cause to reference UI project in business project
    ///         // And  make sure at runtime when we deserialize, the assembly UI is already loaded in memory.
    ///         // This is the case because Program class has probably loaded your main form (or a splash screen) from UI assembly
    ///         // _before_ loading config...
    ///         [ConfigType(AssemblyName = "MyApplication.UI", FullTypeName = "Config")]
    ///         public object Foo { get; set; }
    ///
    ///         public int asUsual; // this value will be deserialized as usual
    ///     }
    ///     namespace SpecificTinyBusinessDomain
    ///     {
    ///         public class Config
    ///         {
    ///             // ...
    ///         }
    ///     }
    /// }
    ///  // in project/assembly MyApplication.UI (that references MyApplication.Business)
    /// namespace MyApplication.UI
    /// {
    ///     public class Config
    ///     {
    ///         public int bar;
    ///     }
    /// }
    /// </code>
    /// <para>
    /// You can deserialize just by calling:
    /// <code>
    ///   var keyValuesPairs = LoadSettingsFromElsewhere();
    ///   var configLoaded = new MyApplication.Business.Config();
    ///   DeserializerWithDependencyInversion.AssignKeyValuePairs(configLoaded, keyValuesPairs);
    /// </code>
    /// </para>
    /// <para>
    /// Note: It works because all properties have setters (they can be private or declard with "init" if you want immutability...)
    /// </para>
    /// <remarks>Code is from https://gitlab.com/Framework.Net/ApplicationBase/-/blob/master/TechnicalTools/Tools/DeserializerWithDependencyInversion.cs?ref_type=heads</remarks>
    /// </summary>
    public static class DeserializerWithDependencyInversion
    {
        public static T DeserializeFromFile<T>(string xmlFile, bool ignoreOnError = false)
        {
            return DeserializeFromXmlString<T>(File.ReadAllText(xmlFile), ignoreOnError);
        }
        public static T DeserializeFromXmlString<T>(string xmlContent, bool ignoreOnError = false)
        {
            return (T)Deserialize(typeof(T), new StreamReader(xmlContent.AsStream()), ignoreOnError);
        }
        static object Deserialize(Type type, StreamReader stream, bool ignoreOnError)
        {
            var serializer = new XmlSerializer(type);
            // Pour debugger la deserialization http://www.hanselman.com/blog/HOWTODebugIntoANETXmlSerializerGeneratedAssembly.aspx
            object obj = serializer.Deserialize(stream);
            SearchIncompleteDeserialization(ignoreOnError, obj);
            return obj;
        }

        static void SearchIncompleteDeserialization(bool ignoreOnError, object obj)
        {
            foreach (var p in obj.GetType().GetProperties())
            {
                if (p.IsStatic())
                    continue;
                if (!p.CanRead)
                    continue;
                if (p.GetSetMethod() == null) // Setter can be private so we do not use CanWrite property
                    continue;
                TryDeserializeRec(p, p.GetValue(obj), v => p.SetValue(obj, v), ignoreOnError);
            }
            foreach (var f in obj.GetType().GetFields())
            {
                if (f.IsStatic)
                    continue;
                if (f.IsInitOnly)
                    continue;
                TryDeserializeRec(f, f.GetValue(obj), v => f.SetValue(obj, v), ignoreOnError);
            }
        }

        static void TryDeserializeRec(MemberInfo mi, object pValue, Action<object> setValue, bool ignoreOnError)
        {
            var subType = FindType(mi);
            if (subType == null)
                return;

            if (pValue is XmlNode[] subObj)
            {
                string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
                           + "<" + subType.Name + ">" + subObj.Select(n => n.OuterXml).Join("") + "</" + subType.Name + ">";
                var subCfgObj = Deserialize(subType, new StreamReader(xml.AsStream()), ignoreOnError);
                setValue(subCfgObj);
            }
            else if (pValue != null)
            {
                if (pValue.GetType() == typeof(object)) // XMl Serializer create an object because the xml is present but empty
                    setValue(Activator.CreateInstance(subType)); // We replace it with a value of the good type
                else
                    SearchIncompleteDeserialization(ignoreOnError, pValue);
            }
        }

        /// <summary>
        /// Recherche le Type désigné par ConfigTypeAttribute sur un field ou une property
        /// </summary>
        /// <returns>null if ConfigTypeAttribute is not used, the type if used, throw an exception if used and not found</returns>
        static Type FindType(MemberInfo mi)
        {
            if (!(mi as FieldInfo)?.FieldType.IsMicrosoftType() ?? false)
                return ((FieldInfo)mi).FieldType;
            if (!(mi as PropertyInfo)?.PropertyType.IsMicrosoftType() ?? false)
                if (!mi.IsOverride())
                    return ((PropertyInfo)mi).PropertyType;
            // Because ConfigTypeAttribute has itself an attribute AttributeUsage.Inherited that equals true
            // GetCustomAttribute is looking for the most specific attribute on the most overriden member (even if _argument_ "inherit" is not set to true)
            var att = mi.GetCustomAttribute<ConfigTypeAttribute>();
            if (att == null)
                return null;
            Debug.Assert(mi.DeclaringType != null, "mi.DeclaringType != null");
            if (string.IsNullOrWhiteSpace(att.FullTypeName))
                throw new ArgumentException(mi.DeclaringType.Name + "." + mi.Name + "'s attribute " + typeof(ConfigTypeAttribute).Name + " must declare TypeName property");
            if (att.FullTypeName.StartsWith(".") || att.FullTypeName.EndsWith("."))
                throw new ArgumentException(mi.DeclaringType.Name + "." + mi.Name + "'s attribute is invalid!");
            Type subType = null;
            if (!string.IsNullOrWhiteSpace(att.AssemblyName))
            {
                subType = Type.GetType(att.FullTypeName + ", " + att.AssemblyName.Trim());
                if (subType == null)
                    throw new ArgumentException($"Cannot find type \"{att.FullTypeName}, {att.AssemblyName}\"");
            }
            else
            {
                var parts = att.FullTypeName.Trim().Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                for (int s = 0; s < parts.Length && subType == null; ++s)
                    for (int i = parts.Length - 1; i > 0 && subType == null; --i)
                        subType = Type.GetType(att.FullTypeName + ", " + parts.Skip(s).Take(i).Join("."));
                if (subType == null)
                    throw new ArgumentException(mi.DeclaringType.Name + "." + mi.Name + "'s attribute is invalid! FullTypeName is invalid, try specify AssemblyName which can be not the same of the type path!");
            }
            return subType;
        }



        public static void AssignCommandLineArgs(object obj, IEnumerable<string> args, string defaultValue = "true", char assignCharacter = '=', string doubleDash = "--")
        {
            var keyValues = new Dictionary<string, string>();
            foreach (var arg in args)
            {
                if (!arg.StartsWith(doubleDash, StringComparison.InvariantCulture))
                    throw new ArgumentException($"Argument not recognized: {arg}");
                var pieces = arg.Split(assignCharacter);
                var value = pieces.Skip(1).Join(string.Empty + assignCharacter);
                if (value.Length == 0)
                    value = defaultValue;
                keyValues.Add(pieces[0], value);
            }
            DeserializerWithDependencyInversion.AssignKeyValuePairs(obj, keyValues);
        }

        /// <summary>
        /// Deserialize a key/value pair list where key is the "path" of property from <paramref name="obj"/> 
        /// and value a string intuitively representing the value in the good type, without ambiguity
        /// </summary>
        /// <remarks>Write only properties are always set after properties that can be both read or written.</remarks>
        /// <exception cref="TechnicalException">A problem occurred with one setting. You can provide a logger to substitue this behavior</exception>
        public static void AssignKeyValuePairs(object obj, IReadOnlyDictionary<string, string> fullPathPropertiesToValues, Action<string, string> log = null)
        {
            if (log == null)
                log = (badKey, msg) => throw new TechnicalException($"Something wrong happen with setting/property \"{badKey}\":" + Environment.NewLine + msg);
            var writeOnlySettings = AssignKeyValuePairs(obj, fullPathPropertiesToValues, log, true);
            AssignKeyValuePairs(obj, writeOnlySettings, log, false);

        }
        static Dictionary<string, string> AssignKeyValuePairs(object obj, IReadOnlyDictionary<string, string> fullPathPropertiesToValues, Action<string, string> log, bool postponeWriteOnlyProperties)
        {
            var postponedProperties = new Dictionary<string, string>();
            foreach (var keyValue in fullPathPropertiesToValues)
            {
                object cur = obj;
                var path = keyValue.Key;
                int index;
                while ((index = path.IndexOf('.')) > 0)
                {
                    var nestedRefProperty = index == -1 ? path : path.Remove(index);
                    path = path.Substring(nestedRefProperty.Length + 1);

                    var pis = cur.GetType().GetProperties().Where(p => p.Name == nestedRefProperty).ToList();
                    PropertyInfo pi = null;
                    if (pis.Count > 1)
                    {
                        // We get properties in multiple class,
                        // we have to take the one that is the most specific regarding in class hierarchy.
                        // MSDN says we must not rely on GetProperties() order, beside we are not sure that the property is overridden (maybe it is hide with keyword "new")
                        var curType = cur.GetType();
                        while (curType != typeof(object))
                        {
                            var candidate = pis.Where(p => p.DeclaringType == curType && p.GetGetMethod(true) != null).ToList();
                            if (candidate.Count == 1)
                            {
                                pi = candidate.First();
                                break;
                            }
                            Debug.Assert(curType != null, nameof(curType) + " != null");
                            curType = curType.BaseType;
                        }
                        Debug.Assert(pi != null);
                    }
                    else
                        pi = pis.FirstOrDefault();

                    object newCur;
                    FieldInfo fi = null;
                    if (pi != null)
                        newCur = pi.GetValue(cur);
                    else
                    {
                        fi = cur.GetType().GetField(nestedRefProperty);
                        if (fi == null)
                        {
                            log(keyValue.Key, $"Setting \"{keyValue.Key}\"- cannot be loaded because it is not mapped on type {obj.GetType().FullName} as a property or a field!");
                            path = null;
                            break;
                        }
                        newCur = fi.GetValue(cur);
                    }
                    var mi = pi ?? (MemberInfo)fi;
                    var type = FindType(mi);
                    if (newCur == null || type != newCur.GetType())
                    {
                        if (type == null)
                        {
                            path = null;
                            break;
                        }
                        var tmp = Activator.CreateInstance(type);
                        if (newCur == null)
                            newCur = tmp;
                        else
                        {
                            if (!newCur.GetType().IsInstanceOfType(tmp))
                                throw new ArgumentException($"Attribute {nameof(ConfigTypeAttribute)} on {mi.DeclaringType.ToSmartString(true) + "." + mi.Name} must declare a subtype of {newCur.GetType().ToSmartString(true)}");
                            if (!(newCur is ICopyable))
                                throw new ArgumentException($"Type {newCur.GetType().ToSmartString(true)} must implement " + typeof(ICopyable).MakeGenericType(newCur.GetType()).ToSmartString(true));
                            ((ICopyable)tmp).CopyFrom((ICopyable)newCur);
                            newCur = tmp;
                        }
                        if (pi != null)
                            pi.SetValue(cur, newCur);
                        else
                            fi.SetValue(cur, newCur);
                    }
                    Debug.Assert(newCur != null, "La hierarchy d'objet devrait déjà exister dans Config !");
                    Debug.Assert(newCur.GetType().IsClass);
                    cur = newCur;
                }
                if (string.IsNullOrEmpty(path))
                    continue;

                string key = null;
                int i = path.IndexOf("[", StringComparison.Ordinal);
                if (i > 0)
                {
                    int e = path.IndexOf("]", i, StringComparison.Ordinal);
                    key = path.Substring(i + 1, e - i - 1);
                    path = path.Remove(i);
                }

                var t = cur.GetType();
                var propInfo = t.GetProperty(path);
                if (propInfo != null)
                {
                    if (key != null)
                    {
                        var dico = (System.Collections.IDictionary)propInfo.GetValue(cur);
                        if (dico == null)
                        {
                            log(keyValue.Key, $"Setting \"{keyValue.Key}\" cannot be loaded because no dictionary is found!");
                            continue;
                        }
                        dico[key] = dico.GetType().Implements(typeof(IDictionary<,>))
                                  ? ConvertValue(keyValue.Value, dico.GetType().GetGenericArguments()[1])
                                  : keyValue.Value;
                    }
                    else if (propInfo.GetGetMethod() == null && postponeWriteOnlyProperties)
                        postponedProperties.Add(keyValue.Key, keyValue.Value);
                    else
                        propInfo.SetValue(cur, ConvertValue(keyValue.Value, propInfo.PropertyType));
                }
                else
                {
                    var fieldInfo = t.GetField(path);
                    if (fieldInfo == null)
                    {
                        log(keyValue.Key, $"Setting \"{keyValue.Key}\" cannot be loaded because it is not mapped on type {obj.GetType().FullName} as a property or a field!");
                        continue;
                    }
                    if (key != null)
                    {
                        var dico = (System.Collections.IDictionary)fieldInfo.GetValue(cur);
                        if (dico == null)
                        {
                            log(keyValue.Key, $"Setting \"{keyValue.Key}\" cannot be loaded because no dictionary is found!");
                            continue;
                        }
                        dico[key] = dico.GetType().Implements(typeof(IDictionary<,>))
                                  ? ConvertValue(keyValue.Value, dico.GetType().GetGenericArguments()[1])
                                  : keyValue.Value;
                    }
                    else
                        fieldInfo.SetValue(cur, ConvertValue(keyValue.Value, fieldInfo.FieldType));
                }
            }
            return postponedProperties;
        }

        static object ConvertValue(string value, Type asType)
        {
            var nonNullableType = asType.RemoveNullability();
            if (nonNullableType != asType && string.IsNullOrWhiteSpace(value))
                return null;
            asType = nonNullableType;
            // Because i dont want "01/11/2016" to be interpreted as "2016-01-11".
            // English way of representing date is not consistent
            if (asType == typeof(DateTime))
                return Convert.ChangeType(value, asType, CultureInfo.GetCultureInfo("fr-FR"));
            if (asType == typeof(TimeSpan))
                return TimeSpan.Parse(value);
            if (asType.IsEnum)
                return Enum.Parse(asType, value);
            return Convert.ChangeType(value, asType, CultureInfo.InvariantCulture);
        }
    }
}
