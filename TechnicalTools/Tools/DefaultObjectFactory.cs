﻿using System;
using System.Linq;
using System.Reflection;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace TechnicalTools.Tools
{
    /// <summary>
    /// Class that give access to a fast default constructor for type T
    /// </summary>
    /// <typeparam name="T">Type for which we want to have a constructor</typeparam>
    /// <example>
    /// <code>
    /// class Foo
    /// {
    ///     public Foo(string dummy = "", int? dummy2 = 42) { }
    ///     private Foo(uint dummy = 42) { } // this constructor is not used because a public one is callable
    /// }
    /// static void Main(string[] args_)
    /// {
    ///     int n = 1000000;
    ///     var foos = new Foo[n];
    ///     foos[0] = null;
    ///     foos[n-1] = null;
    ///     var cons = DefaultObjectFactory&lt;Foo&gt;.ConstructorSlow;
    ///     var sw = Stopwatch.StartNew();
    ///     for (int i = 0; i &lt; n; ++i)
    ///         foos[i] = cons();
    ///     sw.Stop();
    ///     var ts1 = sw.ElapsedMilliseconds;
    ///     cons = DefaultObjectFactory&lt;Fooagt;.Constructor;
    ///     sw.Restart();
    ///     for (int i = 0; i &lt; n; ++i)
    ///         foos[i] = cons();
    ///     sw.Stop();
    ///     var ts2 = sw.ElapsedMilliseconds; // this value show that Constructor is about 6 time faster than ConstructorSlow
    ///     Console.WriteLine("Time taken with reflection: " + ts1.ToString());
    ///     Console.WriteLine("Time taken with expression-called constructor : " + ts2.ToString());
    /// }
    /// </example>
    /// </code>
    public static class DefaultObjectFactory
    {
        internal static ConcurrentDictionary<Type, Func<object>> Constructors = new ConcurrentDictionary<Type, Func<object>>();
        internal static ConcurrentDictionary<Type, Func<object>> ConstructorsSlow = new ConcurrentDictionary<Type, Func<object>>();

        public static Func<object> ConstructorFor(Type type)
        {
            if (!Constructors.TryGetValue(type, out Func<object> func))
            {
                var factoryType = typeof(DefaultObjectFactory<>).MakeGenericType(type);
                RuntimeHelpers.RunClassConstructor(factoryType.TypeHandle);
                Constructors.TryGetValue(type, out func);
                Debug.Assert(func != null);
            }
            return func;
        }
        public static Func<object> ConstructorSlowFor(Type type)
        {
            if (!ConstructorsSlow.TryGetValue(type, out Func<object>  func))
            {
                var factoryType = typeof(DefaultObjectFactory<>).MakeGenericType(type);
                RuntimeHelpers.RunClassConstructor(factoryType.TypeHandle);
                ConstructorsSlow.TryGetValue(type, out func);
                Debug.Assert(func != null);
            }
            return func;
        }
    }
    public static class DefaultObjectFactory<T>
    {
        public static readonly Func<T> Constructor = BuildFastConstructor();
        public static readonly Func<T> ConstructorSlow = BuildConstructorSlow();

        static Func<T> BuildFastConstructor()
        {
            Func<T> res;
            if (typeof(T).IsValueType)
            {
                res = () => Activator.CreateInstance<T>();
                DefaultObjectFactory.Constructors.TryAdd(typeof(T), () => Activator.CreateInstance<T>());
            }
            else
            {
                var ctor = typeof(T).GetDefaultConstructor(true);
                var args = ctor.GetParameters().Select(p => p.RawDefaultValue).ToArray();
                var create = DefaultObjectFactoryHelper.GetActivator<T>(ctor);
                res = () => create(args);
                DefaultObjectFactory.Constructors.TryAdd(typeof(T), (Func<object>)(object)res);
            }
            return res;
        }
        static Func<T> BuildConstructorSlow()
        {
            Func<T> res;
            if (typeof(T).IsValueType)
            {
                res = () => Activator.CreateInstance<T>();
                DefaultObjectFactory.Constructors.TryAdd(typeof(T), () => Activator.CreateInstance<T>());
            }
            else
            {
                var ctor = typeof(T).GetDefaultConstructor(true);
                var args = ctor.GetParameters().Select(p => p.RawDefaultValue).ToArray();
                res = () => (T)ctor.Invoke(args);
                DefaultObjectFactory.ConstructorsSlow.TryAdd(typeof(T), (Func<object>)(object)res);
            }
            return res;
        }
    }

    // Code from https://rogerjohansson.blog/2008/02/28/linq-expressions-creating-objects/
    static class DefaultObjectFactoryHelper
    {
        internal delegate T ObjectActivator<T>(params object[] args);
        internal static ObjectActivator<T> GetActivator<T>(ConstructorInfo ctor)
        {
            Type type = ctor.DeclaringType;
            ParameterInfo[] paramsInfo = ctor.GetParameters();

            //create a single param of type object[]
            ParameterExpression param = Expression.Parameter(typeof(object[]), "args");

            Expression[] argsExp = new Expression[paramsInfo.Length];

            //pick each arg from the params array
            //and create a typed expression of them
            for (int i = 0; i < paramsInfo.Length; i++)
            {
                Expression index = Expression.Constant(i);
                Type paramType = paramsInfo[i].ParameterType;
                Expression paramAccessorExp = Expression.ArrayIndex(param, index);
                Expression paramCastExp = Expression.Convert(paramAccessorExp, paramType);
                argsExp[i] = paramCastExp;
            }

            //make a NewExpression that calls the
            //ctor with the args we just created
            NewExpression newExp = Expression.New(ctor, argsExp);

            //create a lambda with the New
            //Expression as body and our param object[] as arg
            LambdaExpression lambda = Expression.Lambda(typeof(ObjectActivator<T>), newExp, param);

            //compile it
            ObjectActivator<T> compiled = (ObjectActivator<T>)lambda.Compile();
            return compiled;
        }
    }
}
