﻿using System;
using System.Net;
using System.Net.Sockets;


namespace TechnicalTools.Tools
{
    // From https://stackoverflow.com/a/27376368/294998
    // We could use this too : string.Join("|", Dns.GetHostAddresses(Dns.GetHostName()).Select(ia => ia.ToString()));
    public static class PublicIpFinder
    {
        public static string GetIp()
        {
            return GetIpFor("8.8.8.8");
        }
        public static string GetIpFor(string hostNameOrIp)
        {
            // There is a more accurate way when there are multi ip addresses available on local machine.
            // Just try to make a UDP connection to a target, the target no need to be existed at all.
            // And you will receive the preferred outbound IP address of local machine.
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect(hostNameOrIp, 65530);
                var endPoint = socket.LocalEndPoint as IPEndPoint;
                return endPoint.Address.ToString();
            }
        }

    }
}
