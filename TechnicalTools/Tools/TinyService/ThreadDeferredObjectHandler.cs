﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using TechnicalTools.Logs;
using TechnicalTools.Model;


namespace TechnicalTools.Tools
{
    public abstract class ThreadDeferredObjectHandler<TObject> : ITinyService, IMultipleTimeOwnable
        where TObject : class
    {
        public bool     Enabled         { get; set; } = true;

        /// <summary> In milliseconds </summary>
        public TimeSpan MinDeferredTime { get; set; } = new TimeSpan(0, 0, 2);
        /// <summary> In milliseconds </summary>
        public TimeSpan RetryInterval   { get; set; } = new TimeSpan(0, 0, 5);

        public bool     LogException    { get; set; } = true;
        public bool     AggregateSequentialAndSimilarExceptions { get; set; }

        public int      ItemScheduledOrHandledCount { get; private set; }

        /// <summary>
        /// Status to only postpone item processing at start (until a resource is available in code for example).
        /// This value must not be set to false on exiting, except if you want to discard item handling and possibly loose data.
        /// </summary>
        public virtual bool IsReady { get; protected set; } = true;

        /// <summary>
        /// Note that the constructor is calling <see cref="AddOwner(object)"/>. So developper must follow the assignment of this method.
        /// </summary>
        /// <param name="mainOwner">The main owner of the class, or null if method <see cref="AddOwner(object)"/> is, later, manually called with a non null value (at least once)</param>
        /// <param name="dependencies">Contains any other services "this" depends on. Usually at least the db log sender instance</param>
        public ThreadDeferredObjectHandler(object mainOwner, params ITinyService[] dependencies)
        {
            _log = LogManager.Default.CreateLogger(GetType());
            _thLoop = new Thread(ThreadLoop_Main);
            _thLoop.Name = GetType().Name;
            if (dependencies != null)
                foreach (var dep in dependencies)
                    if (dep != null)
                        AddDependencies(dep);
            // mainOwner may be null if calling code add owner itself
            // This may be the case in a child class that wants to add a dependency before adding first owner.
            if (mainOwner != null)
                AddOwner(mainOwner);
        }
        readonly ILogger _log;
        readonly Thread _thLoop;

        /// <summary>
        /// Add a dependency to be handled automatically upong <see cref="IMultipleTimeOwnable"/> interface.
        /// All dependencies must be added before the first owner.
        /// </summary>
        /// <param name="deps"></param>
        protected void AddDependencies(params ITinyService[] deps)
        {
            if (_thLoop.ThreadState != ThreadState.Unstarted)
                throw new TechnicalException($"Dependencies must be added before first owner is added!", null);
            if (deps != null)
                foreach (var dep in deps.NotNull())
                {
                    dep.AddOwner(this);
                    var success = _dependencies.TryAdd(dep, DateTime.UtcNow);
                    if (!success)
                        throw new TechnicalException($"Inconsistency detected, you cannot add same dependency to the current object in different thread", null);
                }
        }
        public List<ITinyService> GetDependencies()
        {
            return _dependencies.Keys.ToList();
        }
        readonly ConcurrentDictionary<ITinyService, DateTime> _dependencies = new ConcurrentDictionary<ITinyService, DateTime>();

        /// <summary>
        /// Read first <see cref="IMultipleTimeOwnable.AddOwner(object)">
        /// This method is usually called by consuming developper when owner does NOT implement <see cref="ITinyService"/>
        /// and "this" cannot be added as a dependency by using owner.<see cref="AddDependencies(ITinyService[])"/>
        /// When first owner is added, the service really start.
        /// </summary>
        public virtual bool AddOwner(object owner)
        {
            if (owner == null)
                throw new TechnicalException($"{nameof(owner)} cannot be null!", null);
            bool shouldStart;
            lock (_holders)
            {
                if (_holders.ContainsKey(owner))
                    throw new TechnicalException($"{nameof(owner)} already added as owner!", null);
                _holders.Add(owner, DateTime.UtcNow);
                shouldStart = _holders.Count == 1;
            }
            if (shouldStart && _thLoop.ThreadState == ThreadState.Unstarted)
            {
                InitializeThread(_thLoop);
                _thLoop.Start();
            }
            return true;
        }

        protected virtual void InitializeThread(Thread thLoop)
        {
        }

        /// <summary>
        /// Read first<see cref="IMultipleTimeOwnable.Release(object)">.
        /// When last owner calls this, the service stops (and cannot be start again).
        /// </summary>
        public virtual bool Release(object owner)
        {
            bool removed;
            lock (_holders)
                removed = _holders.Remove(owner);
            if (removed)
                WakeUp();
            return removed;
        }
        readonly Dictionary<object, DateTime> _holders = new Dictionary<object, DateTime>(ReferenceEqualityComparer<object>.Default);
        public virtual bool IsStopping { get { lock (_holders) return _holders.Count == 0; } }

        public void ScheduleHandle(TObject item)
        {
            if (IsStopping)
                throw new TechnicalException($"There is no remaining owner for the current instance of {GetType().Name}." + Environment.NewLine +
                                             $"Add the caller as owner by calling {nameof(AddOwner)}.", null);
            if (!Enabled)
                return;

            if (!IsObjectInteresting(item))
                return;

            // Lock without allowing dotnetFramework to run any other task
            _withoutSwitchingContextLock.WaitOneNonAlertable();
            // We choose to not handle case where developper would kill current thread here.
            try
            {
                _objectsToHandle.Add(item);
                ++ItemScheduledOrHandledCount;
            }
            finally
            {
                _withoutSwitchingContextLock.Set();
                WakeUp();
            }
        }
        public void ScheduleHandle(IReadOnlyCollection<TObject> items)
        {
            // This code (method, not the next if) is duplicated because the try part is different and not factorizable without using a technique that create object :
            // 1) wrapping one item in a collection
            // 2) creating a lambda Action (capturing item or items) to customize the try part
            // We should probably do an one-item-container thread safe pool  to do 1) without creating object a lot of time.
            // But is a lock/unlock faster that allocating ? Hard to now and it would need a real measure to be sure it is useful
            // So we do nothing until needed...
            if (IsStopping)
                throw new TechnicalException($"There is no remaining owner for the current instance of {GetType().Name}." + Environment.NewLine +
                                             $"Add the caller as owner by calling {nameof(AddOwner)}.", null);
            if (!Enabled)
                return;


            // We choose to not handle case where developper would kill current thread here.
            bool lockToUnlock = false;
            var e = items.GetEnumerator();
            try
            {
                int cnt = 0;
                while (e.MoveNext())
                {
                    if (IsObjectInteresting(e.Current))
                    {
                        // Lock without allowing dotnetFramework to run any other task
                        _withoutSwitchingContextLock.WaitOneNonAlertable();
                        lockToUnlock = true;
                        // EnsureCapacity : ensure atomicity of Add and so "ScheduleHandle" (this is useful to manager Next later)
                        _objectsToHandle.EnsureCapacity(_objectsToHandle.Count + items.Count - cnt);

                        _objectsToHandle.Add(e.Current);
                        ++ItemScheduledOrHandledCount;
                        break;
                    }
                    ++cnt;
                }
                while (e.MoveNext())
                {
                    if (IsObjectInteresting(e.Current))
                    {
                        _objectsToHandle.Add(e.Current);
                        ++ItemScheduledOrHandledCount;
                        break;
                    }
                }
            }
            finally
            {
                if (lockToUnlock)
                {
                    _withoutSwitchingContextLock.Set();
                    WakeUp();
                }
                e.Dispose();
            }
        }
        volatile List<TObject> _objectsToHandle = new List<TObject>();
        volatile List<TObject> _objectsBeingHandled = new List<TObject>();
        readonly AutoResetEvent _withoutSwitchingContextLock = new AutoResetEvent(true);


        /// <summary>
        /// Indicate to thread there is something to do and it can wake up
        /// </summary>
        protected void WakeUp()
        {
            _somethingTodo.Set();
        }
        bool WaitForSomethingToDo()
        {
            return _somethingTodo.WaitOne();
        }
        readonly AutoResetEvent _somethingTodo = new AutoResetEvent(false);


        void ThreadLoop_Main()
        {
            BeforeThreadStart();

            try
            {
                while (WaitForSomethingToDo())
                {
                    if (IsStopping)
                    {
                        if (_objectsToHandle.Count == 0 || !IsReady)
                            break;
                        WakeUp(); // unlock ourself to do next loop iteration after the current one
                    }

                    // Wait for _objectsToHandle to be more filled
                    // or give time between multiple try on exiting when problem occurs (ex: a resource to treat item is temporarily unaivailable)
                    Thread.Sleep(MinDeferredTime);

                    if (!IsReady)
                        continue;

                    ThreadLoop_TreatData();
                }
            }
            finally
            {
                foreach (var dep in _dependencies.OrderByDescending(kvp => kvp.Value))
                    dep.Key.Release(this);
            }
        }

        protected virtual void BeforeThreadStart()
        {
        }

        void ThreadLoop_TreatData()
        {
            // using WaitOneNonAlertable because of... (see comment on WaitOneNonAlertable)
            // Not totally sure it may happen for non-UI thread. But it does not harm to use anyway
            _withoutSwitchingContextLock.WaitOneNonAlertable();
            System.Diagnostics.Debug.Assert(_objectsBeingHandled.Count == 0);
            var tmp = _objectsToHandle;
            _objectsToHandle = _objectsBeingHandled;
            _objectsBeingHandled = tmp;
            _withoutSwitchingContextLock.Set();

            bool lastTryInError = false;
            while (_objectsBeingHandled.Count > 0)
            {
                try
                {
                    var handledCount = Handle(_objectsBeingHandled);
                    _objectsBeingHandled.RemoveRange(0, Math.Min(_objectsBeingHandled.Count, handledCount));
                    if (handledCount >= _objectsBeingHandled.Count)
                        break;
                    if (lastTryInError)
                        OnNextSuccessfulHandlingAfterException();
                    lastTryInError = false;
                }
                catch (Exception ex)
                {
                    lastTryInError = true;
                    OnLogException(ex);
                }
            }
        }

        class ErrorInfo
        {
            public DateTime FirstUtc { get; set; }
            public DateTime LastUtc  { get; set; }
            public int      Count    { get; set; }
        }

        protected virtual void OnLogException(Exception ex)
        {
            if (LogException)
            {
                if (!_occurences.ContainsKey(ex))
                {
                    _log.Error($"Will retry in {RetryInterval.TotalSeconds}", ex, LogTags.Ignored);
                    if (AggregateSequentialAndSimilarExceptions)
                    {
                        var nowUtc = DateTime.UtcNow;
                        _occurences[ex] = new ErrorInfo()
                        {
                            FirstUtc = nowUtc,
                            LastUtc = nowUtc,
                            Count = 1
                        };
                    }
                }
                else
                {
                    _occurences[ex].LastUtc = DateTime.UtcNow;
                    _occurences[ex].Count++;
                }
            }
            // Because sometimes there are issues
            // (for example if objects are sent to a database, tools like LogStash / Kibana / some users can lock tables...)
            Thread.Sleep(RetryInterval);
        }
        Dictionary<Exception, ErrorInfo> _occurences = new Dictionary<Exception, ErrorInfo>(ExceptionEqualityComparer.Instance);

        protected virtual void OnNextSuccessfulHandlingAfterException()
        {
            foreach (var kvp in _occurences.OrderBy(kvp => kvp.Value.FirstUtc))
                if (kvp.Value.Count > 1)
                    _log.Error($"This exception has been met again {kvp.Value.Count-1} times in time interval {kvp.Value.FirstUtc.ToString("o")} to {kvp.Value.LastUtc.ToString("o")} ", kvp.Key, LogTags.Ignored);
            _occurences.Clear();
        }

        /// <summary>
        /// Any fast operation to prefilter item to handle
        /// </summary>
        protected virtual bool IsObjectInteresting(TObject obj)
        {
            return true;
        }

        /// <summary>
        /// Any operation the child class want to "handle" the objects
        /// This method must returned the "n" first items of list that have been handled.
        /// In case of exception, the exception is logged (if LogException is true).
        /// and the method will be call again later (with same set of object) a little time later.
        /// if returned value is less than count of items passed in argument,
        /// method will be called again minus the already handled elements.
        /// </summary>
        /// <remarks>This method is executed in another thread !</remarks>
        /// <param name="objectsToHandle">The objects to handle</param>
        protected abstract int Handle(IReadOnlyCollection<TObject> objectsToHandle);
    }
}
