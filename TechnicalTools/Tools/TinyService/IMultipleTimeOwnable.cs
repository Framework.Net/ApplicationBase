﻿using System;


namespace TechnicalTools.Tools
{
    /// <summary>
    /// Interface to provide multiple ownership to an object.
    /// This is to allow management like smart pointer
    /// </summary>
    public interface IMultipleTimeOwnable
    {
        /// <summary>
        /// Adding a owner to object
        /// <see cref="Release(object)"/> must be call later.
        /// </summary>
        /// <param name="owner">Any non null object</param>
        /// <returns>true if owner was not already an owner, and it is one now</returns>
        bool AddOwner(object owner);

        /// <summary>
        /// When <see cref="AddOwner(object)"/> has been called, this method must be called with same object.
        /// </summary>
        /// <param name="owner"></param>
        /// <returns>true if owner still was an owner, and it is not anymore now</returns>
        bool Release(object owner);
    }
}
