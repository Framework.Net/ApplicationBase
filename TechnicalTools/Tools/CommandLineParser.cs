﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;


namespace TechnicalTools.Tools
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
    public class UsageAttribute : Attribute
    {
        public string Usage { get; }

        public UsageAttribute(string usage)
        {
            Usage = usage;
        }
    }
    public class CommandLineParser
    {
        string _prefix;
        readonly bool _ignoreUnknownArg;
        readonly bool _consumeArgs;

        public CommandLineParser(bool ignoreUnknownArg = false, bool consumeArgs = false, string prefix = "--")
        {
            _ignoreUnknownArg = ignoreUnknownArg;
            _consumeArgs = consumeArgs;
            _prefix = prefix;
        }

        public bool MatchArg(string argContent, string argNameToFind, Action<string> action)
        {
            Debug.Assert(argContent != null);
            Debug.Assert(argNameToFind != null);
            Debug.Assert(action != null);
            argContent = argContent.Trim();
            if (!argContent.ToLowerInvariant().StartsWith(argNameToFind.ToLowerInvariant()))
                return false;
            if (argContent.Length == argNameToFind.Length)
                return true; // but here is no values to parse
            if (argContent[argNameToFind.Length] != '=') // argNameToFind is just a prefix
                return false;
            argContent = argContent.Substring(argNameToFind.Length + "=".Length);
            action(argContent);
            return true;
        }

        public bool MatchArg(string argContent, string argNameToFind, Action<int> hydrate)
        {
            return MatchArg(argContent, argNameToFind, str =>
            {
                if (!int.TryParse(str, out int i))
                    throw new ArgumentOutOfRangeException(argNameToFind, $"{str} is not recognised as a valid value for argument {argNameToFind}. An integer is expected!");
                hydrate(i);
            });
        }
        public bool MatchArg(string argContent, string argNameToFind, Action<decimal> hydrate)
        {
            return MatchArg(argContent, argNameToFind, str =>
            {
                if (!decimal.TryParse(str, out decimal d))
                    throw new ArgumentOutOfRangeException(argNameToFind, $"{str} is not recognised as a valid value for argument {argNameToFind}. An integer is expected!");
                hydrate(d);
            });
        }
        public bool MatchArg(string argContent, string argNameToFind, Action<Version> hydrate)
        {
            return MatchArg(argContent, argNameToFind, str => hydrate(new Version(str)));
        }
        public bool MatchArg(string argContent, string argNameToFind, Action<bool> hydrate)
        {
            return MatchArg(argContent, argNameToFind, str =>
            {
                if (!bool.TryParse(str, out bool value))
                    throw new ArgumentOutOfRangeException(argNameToFind, $"{str} is not recognised as a valid boolean value for argument {argNameToFind}");
                hydrate(value);
            });
        }
        public bool MatchArg<TEnum>(string argContent, string argNameToFind, Action<TEnum> hydrate)
            where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum)
                throw new Exception($"Internal Error : {typeof(TEnum)} must be an enum type!");
            return MatchArgAsEnum(argContent, argNameToFind, typeof(TEnum), obj => { hydrate((TEnum)obj); });
        }
        bool MatchArgAsEnum(string argContent, string argNameToFind, Type enumType, Action<object> hydrate)
        {
            Debug.Assert(enumType.IsEnum);
            object value;
            return MatchArg(argContent, argNameToFind, str =>
            {
                if (!_enumTryParseMethods.TryGetValue(enumType, out Func<string, bool, Enum>  tryParse))
                {
                    var m = _genericEnumTryParseMethod.MakeGenericMethod(enumType);
                    tryParse = (strValue, ignoreCase) =>
                    {
                        var a = new object[] {strValue, ignoreCase, null};
                        if ((bool)m.Invoke(null, a))
                            return (Enum)a[2];
                        return null;
                    };
                    _enumTryParseMethods.Add(enumType, tryParse);
                }
                value = tryParse(str, true);
                if (value == null)
                {
                    value = enumType.GetEnumValues().Cast<Enum>().FirstOrDefault(v => v.GetDescription().ToLowerInvariant() == str.ToLowerInvariant());
                    if (value == null)
                        throw new ArgumentOutOfRangeException($"{str} is not recognised as a valid value for argument {argNameToFind}");
                }
                hydrate(value);
            });
        }
        readonly MethodInfo _genericEnumTryParseMethod = typeof(Enum).GetMethods(BindingFlags.Public | BindingFlags.Static).Single(m => m.Name == nameof(Enum.TryParse) && m.GetParameters().Length == 3);
        readonly Dictionary<Type, Func<string, bool, Enum>> _enumTryParseMethods = new Dictionary<Type, Func<string, bool, Enum>>();

        public bool MatchArgs(string argContent, string argNameToFind, Action<ICollection<string>> hydrate)
        {
            return MatchArg(argContent, argNameToFind, str =>
            {
                if (!str.StartsWith("{") || !str.EndsWith("}"))
                    throw new ArgumentOutOfRangeException(argNameToFind, string.Format("{0} is not structured as {2}{1}={{ ...arguments... }}", str, argNameToFind, _prefix));
                str = str.Substring(1, str.Length - 2).Trim();
                if (str.Length != 0)
                    hydrate(str.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                         .Select(arg => arg.Trim())
                                         .ToArray());
            });
        }

        /// <summary>
        /// Parse une chaine de caractere de type "--foo={param1=bar, param2=something}"
        /// <code>
        /// var args = new Dictionary&lt;string, string&gt;();
        /// if (MatchArgs("la chaine ci dessus", "foo", out args))
        /// {
        ///     // you can use "args" here
        /// }
        /// </code>
        /// </summary>
        /// <param name="argContent"></param>
        /// <param name="argNameToFind"></param>
        /// <returns>null if argument match, a dictionary otherwise</returns>
        public Dictionary<string, string> MatchArgsAsDictionary(string argContent, string argNameToFind)
        {
            Dictionary<string, string> values = null;
            MatchArgs(argContent, argNameToFind, subArgs =>
            {
                values = new Dictionary<string, string>();
                foreach (var pair in subArgs)
                {
                    int index = pair.IndexOf('=');
                    if (index != -1)
                        values.Add(pair.Remove(index).Trim(), pair.Substring(index + 1).Trim());
                    else if (pair.Trim().Length > 0)
                        values.Add(pair.Trim(), "");
                }
            });
            return values;
        }

        // Just to have a signature consistent with other signatures of this class
        public T ParseAndHydrate<T>(ICollection<string> args)
            where T : new()
        {
            T objectToFill = new T();
            ParseAndHydrate(args, objectToFill);
            return objectToFill;
        }

        public void ParseAndHydrate(ICollection<string> args, object objectToFill)
        {
            var hydrateActionsByPropertyName = GetHydrateActionsForObject(objectToFill.GetType(), objectToFill);
            foreach (var arg in args.ToList())
            {
                var argTrimmed = arg.Trim();
                if (!argTrimmed.StartsWith(_prefix))
                    throw new ArgumentException(argTrimmed, $"{argTrimmed} is not in a valid format. Argument must begin with \"{_prefix}\"!");
                if (argTrimmed.IndexOf('=') == -1)
                    throw new ArgumentException(argTrimmed, $"{argTrimmed} is not in a valid format. Expecting a \"=\" and a value at the end!");
                var argName = argTrimmed.Remove(argTrimmed.IndexOf('=')).Substring(_prefix.Length);
                if (hydrateActionsByPropertyName.TryGetValue(argName.ToLowerInvariant(), out Tuple<PropertyInfo, Func<string, bool>>  piAndMatchFun))
                {
                    var res = piAndMatchFun.Item2(argTrimmed);
                    Debug.Assert(res);
                    if (_consumeArgs)
                        args.Remove(argTrimmed);
                }
                else if (!_ignoreUnknownArg)
                    throw new ArgumentException(argName, $"{_prefix}{argName} is not recognised as a valid argument !");
            }
        }
        // Return A dictionary : lower(hydratable property name) => ( TryparseAndHydrate(string fullArgument) => bool success)
        Dictionary<string, Tuple<PropertyInfo, Func<string, bool>>> GetHydrateActionsForObject(Type objectToFillType, object objectToFill)
        {
            var propertiesHydratable = objectToFillType
                                        .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy)
                                        .Select(p => // Select the right PropertyInfo when property is overriden
                                        {
                                            var curType = objectToFillType;
                                            while (p.DeclaringType != curType && p.GetSetMethod(true) == null)
                                            {
                                                curType = p.DeclaringType;
                                                Debug.Assert(curType != null);
                                                p = curType.GetProperty(p.Name);
                                            }
                                            return p.GetSetMethod(true) == null ? null : p;
                                        })
                                        .Where(p => p != null)
                                        .Where(p => p.PropertyType == typeof(string) || p.PropertyType.IsValueType
                                                 || propertyValueIsAlreadyCreated(p, objectToFill)
                                                 || propertyValueMayBeNullButAssignable(p))
                                        .ToDictionary(p => p.Name.ToLowerInvariant(),
                                                      p => Tuple.Create(p, GetTryMatchAndHydrateForTypedArg(p, objectToFill)));
            return propertiesHydratable;
        }
        Func<string, bool> GetTryMatchAndHydrateForTypedArg(PropertyInfo pi, object objectToFill)
        {
            var realType = pi.PropertyType.TryGetNullableType() ?? pi.PropertyType;
            if (realType == typeof(string))
                return argValue => MatchArg(argValue, _prefix + pi.Name, (string str) => pi.SetValue(objectToFill, str));
            if (realType == typeof(bool))
                return argValue => MatchArg(argValue, _prefix + pi.Name, (bool b) => pi.SetValue(objectToFill, b));
            if (realType == typeof(int))
                return argValue => MatchArg(argValue, _prefix + pi.Name, (int i) => pi.SetValue(objectToFill, i));
            if (realType == typeof(decimal))
                return argValue => MatchArg(argValue, _prefix + pi.Name, (decimal d) => pi.SetValue(objectToFill, d));
            if (realType == typeof(Version))
                return argValue => MatchArg(argValue, _prefix + pi.Name, (Version v) => pi.SetValue(objectToFill, v));
            if (realType.IsEnum)
                return argValue => MatchArgAsEnum(argValue, _prefix + pi.Name, realType, enumValue => pi.SetValue(objectToFill, enumValue));
            if (pi.PropertyType.IsClass)
                return argValue => MatchArgAsObjectProperty(argValue, pi, objectToFill);

            throw new NotImplementedException($"{pi.PropertyType.Name} type is not handled yet in {nameof(ParseAndHydrate)}");
        }
        bool MatchArgAsObjectProperty(string arg, PropertyInfo pi, object parentObject)
        {
            Debug.Assert(pi != null);
            Debug.Assert(pi.ReflectedType != null);
            Debug.Assert(pi.ReflectedType.IsClass);

            ICollection<string> subArgs = null;
            if (!MatchArgs(arg, _prefix + pi.Name, subArgs2 => subArgs = subArgs2))
                return false;
            object propObject;
            if (propertyValueIsAlreadyCreated(pi, parentObject))
                propObject = pi.GetValue(parentObject);
            else // property value of objectToFill is not created but assignable
            {
                Debug.Assert(propertyValueMayBeNullButAssignable(pi));
                string typeNameOfObjectToCreate = null;
                var matchingTypes = pi.PropertyType.GetAllInheritingTypes().ToList();
                if (!pi.PropertyType.IsAbstract)
                    matchingTypes.Add(pi.PropertyType);
                if (matchingTypes.Count == 0)
                    throw new ArgumentException(); // Aucun type trouve qui implemente le type abstrait pi.PropertyType
                if (matchingTypes.Count > 1)
                {
                    if (subArgs.Count == 0 || !MatchArg(subArgs.First(), "--type", str => typeNameOfObjectToCreate = str))
                        throw new ArgumentException(); // pi.PropertyType fait référence à plusieurs type possible,

                    matchingTypes = matchingTypes.Where(t => t.Name == typeNameOfObjectToCreate
                                                          || t.FullName == typeNameOfObjectToCreate)
                                                 .ToList();
                }
                if (matchingTypes.Count == 0)
                    throw new ArgumentException(); // Pas de type qui matche le nom ... typeNameOfobjectTocreate
                if (matchingTypes.Count > 1)
                    throw new ArgumentException(); // Ambiguité
                Type tObjectToCreate = matchingTypes[0];
                var cons = DefaultObjectFactory.ConstructorFor(tObjectToCreate);
                if (cons == null)
                    throw new ArgumentException(); // Pas de constructeur par defaut sur le type specifié
                propObject = cons();
            }
            var prefixBefore = _prefix;
            try
            {
                _prefix = "";
                ParseAndHydrate(subArgs.Skip(1).ToArray(), propObject);
            }
            finally
            {
                _prefix = prefixBefore;
            }
            if (!propertyValueIsAlreadyCreated(pi, parentObject))
                pi.SetValue(parentObject, propObject);
            return true;
        }
        readonly Func<PropertyInfo, object, bool> propertyValueIsAlreadyCreated       = (p, parentObject) => p.PropertyType.IsClass && p.GetGetMethod() != null && p.GetGetMethod().IsPublic && (parentObject == null || p.GetValue(parentObject) != null);
        readonly Func<PropertyInfo,         bool> propertyValueMayBeNullButAssignable =  p                => p.PropertyType.IsClass && p.GetSetMethod() != null && p.GetSetMethod().IsPublic;


        public string DisplayUsageForParsing<T>(T objectToFill)
        {
            var dico = new Dictionary<Type, string>
            {
                { objectToFill.GetType(), "" }
            };
            return DisplayUsageForParsing(objectToFill.GetType(), objectToFill, dico);
        }
        string DisplayUsageForParsing(Type type, object objectToFill, Dictionary<Type, string> typesMet)
        {
            string nl = Environment.NewLine; // Makes the code shorter
            var propMatchable = GetHydrateActionsForObject(type, objectToFill);
            string beginning = "Usage:" + nl + nl;
            string res = beginning;
            foreach (var pi in propMatchable.Values.Select(tuple => tuple.Item1))
            {
                string line = _prefix + pi.Name;
                var isObject = pi.PropertyType.IsClass && pi.PropertyType != typeof(string);
                line += pi.PropertyType.IsEnum ? "={" + Enum.GetNames(pi.PropertyType).Join("|") + "}"
                      : isObject               ? "={A=1, B=2...}"
                      :                          "=<" + pi.PropertyType.Name + ">";
                string fullUsage = GetFullUsageFor(pi);
                if (!string.IsNullOrEmpty(fullUsage) || isObject)
                    line += " :";
                string padding = new string(' ', line.Length + 1);
                if (!string.IsNullOrEmpty(fullUsage))
                    line += " " + fullUsage.Replace("\n", nl + padding);  // On utilise "\n" car les developpeurs écrivent souvent juste \n
                if (isObject)
                {
                    var matchingTypes = pi.PropertyType.GetAllInheritingTypes().ToList();
                    if (!pi.PropertyType.IsAbstract)
                        matchingTypes.Add(pi.PropertyType);

                    line += nl;
                    line += padding + "Use (as first parameter) --Type={ " + matchingTypes.Select(t => t.Name).Join() + " }" + nl +
                            padding + "to create object of specified type (only allowed if the program let this value empty)" + nl;
                    line += padding + "Here are the usage of each type :" + nl;
                    foreach (var t in matchingTypes)
                    {
                        line += nl;
                        line += padding + "For " + t.Name;
                        if (typesMet.ContainsKey(t))
                            if (typesMet[t] == "")
                                line += " : etc... recursively" + nl;
                            else
                                line += " : etc ... See previous type arguments for type " + t.Name + " and arg " + typesMet[t] + nl;
                        else
                        {
                            typesMet.Add(t, pi.Name);
                            line += nl + padding + "   ";

                            line += ChangePrefixTemporarily("", () => DisplayUsageForParsing(t, null, typesMet))
                                        .Substring(beginning.Length)
                                        .Replace(nl, nl + padding + "   ");
                        }
                    }
                }
                res += line + nl;
                res += nl;
            }
            return res.Trim();
        }

        T ChangePrefixTemporarily<T>(string newPrefixValue, Func<T> fun)
        {
            var prefixBefore = _prefix;
            try
            {
                _prefix = newPrefixValue;
                return fun();
            }
            finally
            {
                _prefix = prefixBefore;
            }
        }

        static string GetFullUsageFor(PropertyInfo pi)
        {
            var usageAtt = pi.GetCustomAttribute<UsageAttribute>();
            var defaultAtt = pi.GetCustomAttribute<DefaultValueAttribute>();
            return new []
               {
                    usageAtt?.Usage,
                    defaultAtt?.Value == null ? null : "Default value is \"" + defaultAtt.Value.ToString() + "\""
               }.NotBlank().Join(Environment.NewLine);
        }
    }
}
