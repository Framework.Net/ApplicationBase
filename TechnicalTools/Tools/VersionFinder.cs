﻿using System;


namespace TechnicalTools.Tools
{
    public static class VersionFinder
    {
        public static Version GetVersionOfExecutable()
        {
            var dt = AssemblyHelper.RetrieveLinkerTimestamp(System.Reflection.Assembly.GetEntryAssembly());
            dt += new TimeSpan(0, 30, 0); // Pour arrondir à la prochaine demi heure
            if (dt.Hour >= 21)
                dt = dt.Date.AddBusinessDays(1);
            if (dt.TimeOfDay < new TimeSpan(6, 0, 0))
                dt = dt.Date + new TimeSpan(6, 0, 0);

            return new Version(dt.Year % 100, dt.Month, dt.Day, dt.Hour);
        }
    }
}
