﻿using System;
using System.IO;
using System.Text;


namespace TechnicalTools.Tools
{
    public class TeeTextWriter : TextWriter
    {
        public static TeeTextWriter MirrorConsoleOutputTo(TextWriter t)
        {
            var oldOne = Console.Out;
            var res = new TeeTextWriter(oldOne, t, preventDisposingFirstOnClose: true);
            res.Disposed += (_, __) => Console.SetOut(oldOne);
            Console.SetOut(res);
            return res;
        }
        public static TeeTextWriter MirrorConsoleErrorTo(TextWriter t)
        {
            var oldOne = Console.Error;
            var res = new TeeTextWriter(oldOne, t, preventDisposingFirstOnClose: true);
            res.Disposed += (_, __) => Console.SetError(oldOne);
            Console.SetError(res);
            return res;
        }

        TextWriter one;
        TextWriter two;
        readonly bool _preventDisposingFirstOnClose;

        public TeeTextWriter(TextWriter one, TextWriter two, bool preventDisposingFirstOnClose = false)
        {
            _preventDisposingFirstOnClose = preventDisposingFirstOnClose;
            this.one = one;
            this.two = two;
        }

        public override Encoding Encoding
        {
            get { return one.Encoding; }
        }

        public override void Flush()
        {
            one.Flush();
            two.Flush();
        }

        // The base method is mark as virtual but should be abstract
        // All the other write method redirect to the implementation of this one
        public override void Write(char value)
        {
            one.Write(value);
            two.Write(value);
        }

        public event EventHandler Disposed;

        protected override void Dispose(bool disposing)
        {
            if (!_preventDisposingFirstOnClose)
                one.Dispose();
            two.Dispose();
            Disposed?.Invoke(this, EventArgs.Empty);
        }
    }
}
