﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

using TechnicalTools.Annotations.Design;
using TechnicalTools.Model;


namespace TechnicalTools.Tools
{
    public interface IPropertyNotifierObjectWrapper : IPropertyNotifierObject
    {

    }
    public class PropertyNotifierObjectWrapper<TWrappedObject> : IPropertyNotifierObjectWrapper
         where TWrappedObject : class, IPropertyNotifierObject
    {
        protected PropertyNotifierObjectWrapper(TWrappedObject wrapped_object)
        {
            Debug.Assert(wrapped_object != null);
            _wrappedObject = wrapped_object;
        }

        // Révons un peu en espérant un jour voir ce genre de chose : http://stackoverflow.com/a/255621)
        [NotNull] protected readonly TWrappedObject _wrappedObject;

        // Le code de cette région n'a aucune intelligence autre que d'implementer IPropertyNotifierObject en deleguant les appels à _propertyNotifierObject
        #region "Implementation de IPropertyNotifierObject => Delegation complete à l'objet _propertyNotifierObject"

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void SetProperty<T>(ref T backingStore, T value, Action onChanged = null, Action<T> onChanging = null, [CallerMemberName] string propertyName = null)
        {
            _wrappedObject.SetProperty<T>(ref backingStore, value, onChanged, onChanging, propertyName);
        }

        // Tout le reste sont des implementations explicites : car on veut garder l'acces "protected" sur les membres appelés
        // mais permettre de faire des mixins en passant par l'interface IPropertyNotifierObject
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.SetTechnicalProperty<T>(ref T backingStore, T value, Action onChanged, Action<T> onChanging, string propertyName)
        {
            _wrappedObject.SetTechnicalProperty(ref backingStore, value, onChanged, onChanging, propertyName);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void CurrentPropertyIsDependantOn<T>(Expression<Func<T, object>> dependency_prop_has_expression, Func<bool> refresh_current_property = null, [CallerMemberName] string dependent_propertyName = null)
            where T : class, IPropertyNotifierObject
        {
            _wrappedObject.CurrentPropertyIsDependantOn<T>(dependency_prop_has_expression, refresh_current_property, dependent_propertyName);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Touch()
        {
            _wrappedObject.Touch();
        }

        #region propriétés IsDirty, State et autres

        public eState State { get { return _wrappedObject.State; } }
        public event Action<object, eState> StateChanged
        {
            add { _wrappedObject.StateChanged += value; }
            remove { _wrappedObject.StateChanged -= value; }
        }

        public bool IsDirty { get { return _wrappedObject.IsDirty; } }
        public event EventHandler DirtyChanged
        {
            add { _wrappedObject.DirtyChanged += value; }
            remove { _wrappedObject.DirtyChanged -= value; }
        }

        public void InitializeState(eState new_state)
        {
            _wrappedObject.InitializeState(new_state);
        }
        public void UpdateState(eState? new_state = null)
        {
            _wrappedObject.UpdateState(new_state);
        }

        protected ePropertyEventLock LockedEvents { get { return _wrappedObject.LockedEvents; } set { _wrappedObject.LockedEvents = value; } }
        protected bool LockState { get { return _wrappedObject.LockState; } set { _wrappedObject.LockState = value; } }

        public bool IsReadOnly
        {
            get { return _wrappedObject.IsReadOnly; }
            set { _wrappedObject.IsReadOnly = value; }
        }

        public bool IsNew { get { return _wrappedObject.IsNew; } }
        public bool CreatedInCurrentSession { get { return _wrappedObject.CreatedInCurrentSession; } }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CheckIsNotReadOnlyForAction(string action_name = null)
        {
            try
            {
                _wrappedObject.CheckIsNotReadOnlyForAction(action_name);
            }
            catch (Exception ex)
            {
                // On ne fournit pas ex en tant qu'inner exception car on souhaite simplement renommer le message
                throw new Exception(ex.Message.Replace(typeof(PropertyNotifierObject).Name, GetType().FullName));
            }
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void LockWhileUpdatingWith(Action action)
        {
            _wrappedObject.LockWhileUpdatingWith(action);
        }
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void LockWhileUpdatingWith(ePropertyEventLock lck, Action action)
        {
            _wrappedObject.LockWhileUpdatingWith(lck, action);
        }

        #endregion propriétés IsDirty, State et autres


        #region Gestion de l'event PropertyChanged

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PropertyChangedFor<T>(Expression<Func<T, object>> expression, Action onPropertyChanged)
            where T : class, IPropertyNotifierObject
        {
            _wrappedObject.PropertyChangedFor(expression, onPropertyChanged);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PropertyChangedFor_Remove<T>(Expression<Func<T, object>> expression, Action onPropertyChanged)
            where T : class, IPropertyNotifierObject
        {
            _wrappedObject.PropertyChangedFor_Remove(expression, onPropertyChanged);
        }

        #endregion Gestion de l'event PropertyChanged

        #region Gestion de l'event PropertyChanging

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.PropertyChangingFor<T>(Expression<Func<T, object>> expression, Action<object, Model.PropertyChangingEventArgs> onPropertyChanging)
        //FYI : where T : class, IPropertyNotifierObject
        {
            _wrappedObject.PropertyChangingFor(expression, onPropertyChanging);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.PropertyChangingFor_Remove<T>(Expression<Func<T, object>> expression, Action<object, Model.PropertyChangingEventArgs> onPropertyChanging)
        //FYI : where T : class, IPropertyNotifierObject
        {
            _wrappedObject.PropertyChangingFor_Remove(expression, onPropertyChanging);
        }

        #endregion Gestion de l'event PropertyChanging


        #region (Super prive) Simulation de l'heritage des membres protégés
        // Cf les explications dans la même region dans PropertyNotifierObject
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.SetProperty<T>(ref T backingStore, T value, Action onChanged, Action<T> onChanging, string propertyName)
        {
            // ReSharper disable ExplicitCallerInfoArgument
            SetProperty(ref backingStore, value, onChanged, onChanging, propertyName);
            // ReSharper restore ExplicitCallerInfoArgument
        }

        void IPropertyNotifierObject.CurrentPropertyIsDependantOn<T>(Expression<Func<T, object>> dependency_prop_has_expression, Func<bool> refresh_current_property, string dependent_propertyName)
        {
            // ReSharper disable ExplicitCallerInfoArgument
            CurrentPropertyIsDependantOn(dependency_prop_has_expression, refresh_current_property, dependent_propertyName);
            // ReSharper restore ExplicitCallerInfoArgument
        }


        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { _wrappedObject.PropertyChanged += value; }
            remove { _wrappedObject.PropertyChanged -= value; }
        }
        event Action<PropertyNotifierObject, ePropertyEventLock> IPropertyNotifierObject.OnResetBindings
        {
            add    { _wrappedObject.OnResetBindings += value; }
            remove { _wrappedObject.OnResetBindings -= value; }
        }

        ePropertyEventLock IPropertyNotifierObject.LockedEvents { get { return LockedEvents; } set { LockedEvents = value; } }
        bool               IPropertyNotifierObject.LockState    { get { return LockState;    } set { LockState = value; } }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.LockWhileUpdatingWith(Action action)
        {
            _wrappedObject.LockWhileUpdatingWith(action);
        }
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.LockWhileUpdatingWith(ePropertyEventLock lck, Action action)
        {
            _wrappedObject.LockWhileUpdatingWith(lck, action);
        }
        #endregion (Super prive) Simulation de l'heritage des membres protégés

        #endregion


    }

}
