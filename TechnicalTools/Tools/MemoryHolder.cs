﻿using System;


namespace TechnicalTools.Tools
{
    /// <summary>
    /// Class that does nothing but keep an object in memory
    /// This class avoid the use of GC.KeepAlive this way :
    /// <example>
    /// var massDataLoaded = ...
    /// // This code is expected to load instances already loaded in mass data loaded using cache
    /// for (var i ...)
    ///     any_action();
    /// GC.KeepAlive(massDataLoaded);
    /// </example>
    /// Replaced by
    /// <example>
    /// using (var massDataLoadedHolder = new MemoryHolder(...))
    /// {
    ///     // This code is expected to load instances already loaded in mass data loaded using cache
    ///     for (var i ...)
    ///         any_action();
    /// }
    /// </example>
    /// </summary>
    public class MemoryHolder : GenericDisposableContainer
    {
        public MemoryHolder(object dataToKeepInMemory)
            : base(Action_Extensions.DoNothing)
        {
            _dataToKeepInMemory = dataToKeepInMemory;
        }
        readonly object _dataToKeepInMemory;
    }
}
