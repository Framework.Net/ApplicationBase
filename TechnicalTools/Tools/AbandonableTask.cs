﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TechnicalTools.Tools
{
    // Inspired from
    class AbandonableTask<TData>
    {
        readonly object _lock = new object();
        CancellationTokenSource _cancelToken = new CancellationTokenSource();
        readonly Task _task;
        Thread _currentThread;

        public bool IsCancellationRequested { get { return _cancelToken.IsCancellationRequested; } }
        public bool IsCompleted             { get { return _task.IsCompleted; } }

        public AbandonableTask(Func<Func<bool>, TData> getFreshData, Action<TData> onSuccessfullyDone)
        {
            _task = new Task(() =>
            {
                _currentThread = Thread.CurrentThread;
                try
                {
                    var data = getFreshData(() => _cancelToken.Token.IsCancellationRequested);
                    lock (_lock)
                        if (!_cancelToken.IsCancellationRequested)
                            onSuccessfullyDone(data);
                }
                finally
                {
                    Clean();
                }
            }, _cancelToken.Token);
        }

        public void Start()
        {
            _task.Start();
        }

        public void Cancel()
        {
            lock (_lock)
            {
                if (_cancelToken != null)
                    _cancelToken.Cancel();
                Clean();
            }
        }

        public void WaitOrKill(int ms)
        {
            if (!Task.WaitAll(new[] { _task }, ms))
                _currentThread.Abort();
        }

        void Clean()
        {
            lock (_lock)
            {
                _cancelToken.Dispose(); // mandatory because seems to cause a memory leak if not called (see http://stackoverflow.com/a/12474762)
                // TODO : _task should be disposed but it seems complex to make it anyway (see http://blogs.msdn.com/b/pfxteam/archive/2012/03/25/10287435.aspx)
                // We could call it later but it make the more complex for nothing
                // Best way : make impossible the need to call Dispose by preventing the user/developper code to access the task instance
                // (see http://stackoverflow.com/a/3734298)
            }
        }
    }
}
