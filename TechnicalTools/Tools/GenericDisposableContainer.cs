﻿using System;


namespace TechnicalTools.Tools
{
    /// <summary>
    /// To allow to create/configure a disposable object at runtime by using a lambda
    /// without having to create a class just for this
    /// Example:
    /// <code>
    /// var th = Thread.CurrentThread;
    /// var cc = th.CurrentCulture;
    /// var cuc = th.CurrentUICulture;
    /// using (new GenericDisposableContainer(() => th.CurrentCulture = CultureInfo.InvariantCulture, () => th.CurrentCulture = cc))
    /// using (new GenericDisposableContainer(() => th.CurrentUICulture = CultureInfo.InvariantCulture, () => th.CurrentUICulture = cuc))
    /// {
    ///    // some stuff
    /// }
    /// </code>
    /// </summary>
    public class GenericDisposableContainer : IDisposable
    {
        public GenericDisposableContainer(Action toDoOnDispose)
        {
            _toDoOnDispose = toDoOnDispose;
        }
        /// <summary>
        /// Specific constructor to emphasize the dependency/relation between the install / uninstall parts of code
        /// </summary>
        public GenericDisposableContainer(Action install, Action uninstallToDoOnDispose)
            : this(uninstallToDoOnDispose)
        {
            install();
        }
        readonly Action _toDoOnDispose;

        public virtual void Dispose()
        {
            if (!_disposed)
            {
                _toDoOnDispose?.Invoke();
                _disposed = true;
            }
        }
        bool _disposed;
    }
}
