﻿using System;


namespace TechnicalTools.Tools
{
    /// <summary>
    /// With System.Progress&lt;String&gt;, report executions are deferred
    /// so we can return or even leave the application before all report are done.
    /// Tis can be annoying if we want to log report...
    /// This class is intended to redirect data report (of type T) to another handler synchronously
    /// <code>
    /// //using TechnicalTools.Logs;
    ///
    /// protected ILogger _log = LogManager.Default.CreateLogger(GetType());
    /// var pr = new ProgressSynchronousRedirect&lt;string&gt;(_log.Info)
    /// for (int i = 0; i &lt; 1000; ++i)
    ///    pr.Report("test " + i);
    /// return; // we are sure all "test" string are logged here
    /// </code>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ProgressSynchronousRedirect<T> : IProgress<T>
    {
        public ProgressSynchronousRedirect(Action<T> handler)
        {
            _handler = handler;
        }
        readonly Action<T> _handler;

        void IProgress<T>.Report(T value)
        {
            _handler(value);
        }
    }
}
