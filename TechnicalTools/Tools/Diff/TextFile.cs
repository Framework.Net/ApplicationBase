using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace TechnicalTools.Tools.Diff
{
	public class TextLine : IComparable
	{
		public string Line;
		public int _hash;

		public TextLine(string str)
		{
			Line = str.Replace("\t","    ");
			_hash = str.GetHashCode();
		}
		#region IComparable Members

		public int CompareTo(object obj)
		{
			return _hash.CompareTo(((TextLine)obj)._hash);
		}

		#endregion
	}


	public class DiffList_TextFile : IDiffList
	{
		private const int MaxLineLength = 1024;
        private readonly List<TextLine> _lines;

        public DiffList_TextFile(IEnumerable<string> lines)
        {
            _lines = lines.Select(line => new TextLine(line)).ToList();
            foreach (var line in _lines)
                if (line.Line.Length > MaxLineLength)
                    throw new InvalidOperationException(string.Format("File contains a line greater than {0} characters.", MaxLineLength.ToString(CultureInfo.InvariantCulture)));
        }
		public DiffList_TextFile(string fileName)
            : this(File.ReadAllLines(fileName))
		{
		}
		#region IDiffList Members

		public int Count()
		{
			return _lines.Count;
		}

		public IComparable GetByIndex(int index)
		{
			return _lines[index];
		}

		#endregion
	
	}
}