﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;


namespace TechnicalTools.Tools
{
    public class IntervalIntegerInclusiveSet : IntervalIntegerInclusiveSet<IntervalIntegerInclusiveSet, IntervalIntegerInclusive>
    {
        public IntervalIntegerInclusiveSet()                                              { }
        public IntervalIntegerInclusiveSet(long lower, long upper)   : base(lower, upper) { }
        public IntervalIntegerInclusiveSet(IEnumerable<long> points) : base(points)       { }
        protected IntervalIntegerInclusiveSet(IntervalIntegerInclusiveSet<IntervalIntegerInclusiveSet, IntervalIntegerInclusive> from) : base(from) { }

        static readonly Func<long, long, IntervalIntegerInclusive> CreateInterval = (l, u) => new IntervalIntegerInclusive(l, u);

        protected override IntervalIntegerInclusive    CreateNewInterval(long l, long u) { return new IntervalIntegerInclusive(l, u); }
        protected override IntervalIntegerInclusiveSet CreateNewSet()                    { return new IntervalIntegerInclusiveSet(); }
        protected override IntervalIntegerInclusiveSet CreateNewSet(IntervalIntegerInclusiveSet<IntervalIntegerInclusiveSet, IntervalIntegerInclusive> set) { return new IntervalIntegerInclusiveSet((IntervalIntegerInclusiveSet)set); }
    }

    public class IntervalIntegerInclusive
    {
        public long LowerBound { get; protected internal set; }
        public long UpperBound { get; protected internal set; }

        /// <summary> Convert to mathematical notation using open and closed bracket parenthesis. </summary>
        public string AsString { get { return string.Format("[{0}, {1}]", LowerBound, UpperBound); } }

        public IntervalIntegerInclusive(long lowerbound, long upperbound)
        {
            Debug.Assert(lowerbound <= upperbound);

            LowerBound = lowerbound;
            UpperBound = upperbound;
        }

        /// <summary> Check if given point lies within the interval. </summary>
        /// <param name="point">Point to check</param>
        /// <returns>True if point lies within the interval, otherwise false</returns>
        public bool Contains(long point)
        {
            return LowerBound <= point && point <= UpperBound;
        }

        public virtual IntervalIntegerInclusive Clone()
        {
            return new IntervalIntegerInclusive(LowerBound, UpperBound);
        }

#pragma warning disable 809
        [Obsolete("use AsString", true)]
        public override string ToString() { return AsString; }
#pragma warning restore 809

    }

    public abstract class IntervalIntegerInclusiveSet<TSet, TInterval>
        where TInterval : IntervalIntegerInclusive
        where TSet : IntervalIntegerInclusiveSet<TSet, TInterval>
    {
        public IReadOnlyList<TInterval> Intervals { get { return _Intervals; } }
        readonly List<TInterval> _Intervals = new List<TInterval>();

        public bool IsEmpty { get { return _Intervals.Count == 0; } }

        public string AsString
        {
            get
            {
                if (_Intervals.Count == 0)
                    return "[EMPTY]";
                return _Intervals.Select(interval => interval.AsString).Join(" + ");
            }
        }

        public IntervalIntegerInclusiveSet()
        {
        }
        public IntervalIntegerInclusiveSet(long lower, long upper)
        {
            Debug.Assert(lower <= upper);
            _Intervals.Add(CreateNewInterval(lower, upper));
        }
        public IntervalIntegerInclusiveSet(IEnumerable<long> points)
        {
            if (points.Any())
            {
                var ordered = points.OrderBy(point => point);
                var interval = CreateNewInterval(ordered.First(), ordered.First());
                foreach (var p in ordered.Skip(1))
                {
                    if (interval.UpperBound + 1 == p)
                        ++interval.UpperBound;
                    else if (interval.UpperBound < p) // ignore if interval.UpperBound == p
                    {
                        _Intervals.Add(interval);
                        interval = CreateNewInterval(p, p);
                    }
                }
                _Intervals.Add(interval);
            }
        }
        protected IntervalIntegerInclusiveSet(IntervalIntegerInclusiveSet<TSet, TInterval> from)
        {
            _Intervals.AddRange(from._Intervals.Select(interval => CreateNewInterval(interval.LowerBound, interval.UpperBound)));
        }

        protected abstract TInterval CreateNewInterval(long l, long u);
        protected abstract TSet CreateNewSet();
        protected abstract TSet CreateNewSet(IntervalIntegerInclusiveSet<TSet, TInterval> set);

        public void Add(long point)
        {
            TInterval previous = null;
            for (int i = 0; i < _Intervals.Count; ++i)
            {
                if (previous != null && point <= previous.UpperBound)
                    return; // point already in interval
                var interval = _Intervals[i];
                if (point < interval.LowerBound)
                {
                    bool toStickWithPrevious = previous != null && point == previous.UpperBound + 1;
                    bool toStickWithNext = point + 1 == interval.LowerBound;
                    if (toStickWithPrevious)
                        if (toStickWithNext)
                        {
                            previous.UpperBound = interval.UpperBound;
                            _Intervals.RemoveAt(i);
                        }
                        else
                            previous.UpperBound = point;
                    else if (toStickWithNext)
                        interval.LowerBound = point;
                    else
                        _Intervals.Insert(i, CreateNewInterval(point, point));
                    return;
                }
                previous = _Intervals[i];
            }
        }

        public IEnumerable<long> EnumerateAll()
        {
            foreach (var interval in Intervals)
                for (long i = interval.LowerBound; i <= interval.UpperBound; ++i)
                    yield return i;
        }

        public long Count { get { return _Intervals.Select(interval => interval.UpperBound - interval.LowerBound + 1).DefaultIfEmpty(0).Sum(); } }
        public bool Contains(long point)
        {
            foreach (var interval in _Intervals)
            {
                if (point < interval.LowerBound)
                    return false;
                if (point <= interval.UpperBound)
                    return true;
            }
            return false;
        }

        public TSet Exclude(long lower, long upper)
        {
            Debug.Assert(lower <= upper);
            return Exclude(CreateNewInterval(lower, upper));
        }
        public TSet Exclude(TInterval interval)
        {
            var tmp = CreateNewSet();
            tmp._Intervals.Add(interval);
            return this.Exclude(tmp);
        }
        public TSet Exclude(IntervalIntegerInclusiveSet<TSet, TInterval> setToRemove)
        {
            var res = CreateNewSet(this);
            Exclude(res, setToRemove);
            Debug.Assert(res.IsSame(this.Reverse().Union(setToRemove).Reverse()));
            return res;
        }
        public void ExcludeInPlace(long lower, long upper)
        {
            Debug.Assert(lower <= upper);
            ExcludeInPlace(CreateNewInterval(lower, upper));
        }
        public void ExcludeInPlace(TInterval interval)
        {
            var tmp = CreateNewSet();
            tmp._Intervals.Add(interval);
            ExcludeInPlace(tmp);
        }
        public void ExcludeInPlace(IntervalIntegerInclusiveSet<TSet, TInterval> setToRemove)
        {
            var test = this.Reverse().Union(setToRemove).Reverse();
            Exclude(this, setToRemove);
            Debug.Assert(this.IsSame(test));
        }
        static void Exclude(IntervalIntegerInclusiveSet<TSet, TInterval> toUpdate, IntervalIntegerInclusiveSet<TSet, TInterval> setToRemove)
        {
            int r = 0; // intervale to remove
            int c = 0; // current interval
            while (r < setToRemove._Intervals.Count && c < toUpdate._Intervals.Count)
            {
                var remove = setToRemove._Intervals[r];
                var current = toUpdate._Intervals[c];

                if (remove.UpperBound < current.LowerBound) // l'intervale a enlever est completement a gauche => rien a faire sur cet interval
                {
                    ++r;
                }
                else if (current.UpperBound < remove.LowerBound) // l'intervale a enlever est completement a droite => rien a faire sur cet interval (pour le moment)
                {
                    ++c;
                }
                else if (remove.UpperBound < current.UpperBound) //  l'intervale à enlever mort le bord gauche, il restera forcement un bout a droite
                {
                    if (remove.LowerBound > current.LowerBound) // l'intervale a enlever est a l'interieur de l'intervale courant
                    {
                        toUpdate._Intervals.Insert(c, toUpdate.CreateNewInterval(current.LowerBound, remove.LowerBound - 1));
                        current.LowerBound = remove.UpperBound + 1;
                        ++c; // go on the remaining part (maybe the next intervale to remove is still inside)
                        ++r;
                    }
                    else
                    {
                        // Il faut supprimer toute la partie gauche de l'intervale
                        current.LowerBound = remove.UpperBound + 1;
                        ++r;
                    }
                }
                else if (current.LowerBound < remove.LowerBound) // l'intervale a enlever mort le bord droit, il reste forcement un bout a gauche
                {
                    Debug.Assert(             remove.LowerBound <= current.UpperBound , "Other cases already handled above!");
                    Debug.Assert(                                  current.UpperBound <= remove.UpperBound, "Other cases already handled above!");
                    // Il faut supprimer toute la partie droite de l'intervale
                    current.UpperBound = remove.LowerBound - 1;
                    ++c;
                }
                else // Recouvre tout
                {
                    Debug.Assert(remove.LowerBound <= current.LowerBound && current.UpperBound <= remove.UpperBound, "Other cases already handled above!");
                    toUpdate._Intervals.RemoveAt(c);
                }
            }
        }

        public TSet Union(long lower, long upper)
        {
            return this.Union(CreateNewInterval(lower, upper));
        }
        public TSet Union(TInterval interval)
        {
            var tmp = CreateNewSet();
            tmp._Intervals.Add(interval);
            return Union(tmp);
        }
        public TSet Union(IntervalIntegerInclusiveSet<TSet, TInterval> setToAdd)
        {
            var res = CreateNewSet(this);
            Union(res, setToAdd);
            return res;
        }
        public void UnionInPlace(long lower, long upper)
        {
            UnionInPlace(CreateNewInterval(lower, upper));
        }
        public void UnionInPlace(TInterval interval)
        {
            var tmp = CreateNewSet();
            tmp._Intervals.Add(interval);
            UnionInPlace(tmp);
        }
        public void UnionInPlace(IntervalIntegerInclusiveSet<TSet, TInterval> setToAdd)
        {
            Union(this, setToAdd);
        }
        static void Union(IntervalIntegerInclusiveSet<TSet, TInterval> toUpdate, IntervalIntegerInclusiveSet<TSet, TInterval> setToAdd)
        {
            int a = 0; // intervale to add
            int c = 0; // current interval
            while (a < setToAdd._Intervals.Count && c < toUpdate._Intervals.Count)
            {
                var add = setToAdd._Intervals[a];
                var current = toUpdate._Intervals[c];

                if (current.UpperBound != long.MaxValue && current.UpperBound + 1 < add.LowerBound) // l'intervale a ajouter est completement a droite => rien a faire sur cet interval (pour le moment)
                {
                    ++c;
                }
                else if (current.LowerBound != long.MinValue && add.UpperBound < current.LowerBound-1) // l'intervale a ajouter est completement a gauche // Math.Max : handle overflow
                {
                    toUpdate._Intervals.Insert(c, (TInterval)add.Clone());
                    Debug.Assert(c == 0 || toUpdate._Intervals[c - 1].UpperBound + 1 < add.LowerBound, "No need to merge!");
                    ++a;
                }
                else if (add.UpperBound <= current.UpperBound) //  l'intervale à ajouter mort le bord gauche (peut etre recouvre tout)
                {
                    if (add.LowerBound >= current.LowerBound) // l'intervale a ajouter est inclus a l'interieur de l'intervale courant  (peut etre recouvre tout)
                    {
                        ++a;
                    }
                    else
                    {
                        current.LowerBound = add.LowerBound;
                        ++a;
                        Debug.Assert(c == 0 || toUpdate._Intervals[c - 1].UpperBound < current.LowerBound, "Should have been merged the iteration before!");
                    }
                }
                else
                {
                    if (current.LowerBound < add.LowerBound) //  l'intervale a ajouter mort le bord droit
                    {
                        Debug.Assert(add.LowerBound <= Math.Max(current.UpperBound + 1, current.UpperBound), "Other cases already handled above!");
                        Debug.Assert(current.UpperBound <= add.UpperBound, "Other cases already handled above!");
                        current.UpperBound = add.UpperBound;
                    }
                    else // intervale to add is all over the current interval
                    {
                        Debug.Assert(add.LowerBound <= current.LowerBound && current.UpperBound <= add.UpperBound, "Other cases already handled above!");
                        Debug.Assert(c == 0 || toUpdate._Intervals[c-1].UpperBound < add.LowerBound, "Should be handled in iteration before!");
                        current.LowerBound = add.LowerBound;
                        current.UpperBound = add.UpperBound;
                    }

                    TryMergeAtRight(toUpdate, c);
                    ++a;
                }
            }
            while (a < setToAdd._Intervals.Count)
            {
                toUpdate._Intervals.Add((TInterval)setToAdd._Intervals[a].Clone());
                ++a;
            }
        }

        public TSet Reverse()
        {
            var res = CreateNewSet();
            var shift = _Intervals.Count > 0 && _Intervals[0].LowerBound == long.MinValue;
            long lower = shift ? _Intervals[0].UpperBound+1 : long.MinValue;
            foreach (var interval in _Intervals.Skip(shift ? 1 : 0))
            {
                res._Intervals.Add(CreateNewInterval(lower, interval.LowerBound - 1));
                lower = interval.UpperBound + 1;
            }
            if (lower != long.MinValue || _Intervals.Count == 0)
                res._Intervals.Add(CreateNewInterval(lower, long.MaxValue));
            return res;
        }

        public TSet Intersect(IntervalIntegerInclusiveSet<TSet, TInterval> otherSet)
        {
            return this.Reverse().Union(otherSet.Reverse()) // De Morgan's laws
                       .Reverse(); // double complement or Involution law:
        }

        public bool IsSame(IntervalIntegerInclusiveSet<TSet, TInterval> set)
        {
            Defragment();
            set.Defragment();

            if (_Intervals.Count != set._Intervals.Count)
                return false;
            for (int i = 0; i < _Intervals.Count; ++i)
                if (_Intervals[i].LowerBound != set._Intervals[i].LowerBound ||
                    _Intervals[i].UpperBound != set._Intervals[i].UpperBound)
                    return false;
            return true;
        }
        public void Defragment()
        {
            for(int i = 0; i < _Intervals.Count; ++i)
                TryMergeAtRight(this, i);
        }

        static int TryMergeAtLeft(IntervalIntegerInclusiveSet<TSet, TInterval> set, int c)
        {
            Debug.Assert(c >= 0 && c < set._Intervals.Count);
            int count = 0;
            while (--c >= 0 && set._Intervals[c].UpperBound + 1 >= set._Intervals[c+1].LowerBound)
            {
                set._Intervals[c].UpperBound = Math.Max(set._Intervals[c].UpperBound, set._Intervals[c+1].UpperBound);
                set._Intervals.RemoveAt(c+1);
                ++count;
            }
            return count;
        }
        static int TryMergeAtRight(IntervalIntegerInclusiveSet<TSet, TInterval> set, int c)
        {
            Debug.Assert(c >= 0 && c < set._Intervals.Count);
            int count = 0;
            while (c+1 < set._Intervals.Count && set._Intervals[c].UpperBound + 1 >= set._Intervals[c+1].LowerBound)
            {
                set._Intervals[c].UpperBound = Math.Max(set._Intervals[c].UpperBound, set._Intervals[c+1].UpperBound);
                set._Intervals.RemoveAt(c+1);
                ++count;
            }
            return count;
        }

#pragma warning disable 809
        [Obsolete("use AsString", true)]
        public override string ToString() { return AsString; }
#pragma warning restore 809

        public static void RunTests()
        {
            var empty = new IntervalIntegerInclusiveSet();
            Debug.Assert(empty.Exclude(empty).IsSame(empty));
            Debug.Assert(empty.Reverse().Reverse().IsSame(empty));
            Debug.Assert(empty.Reverse().Exclude(empty.Reverse()).IsSame(empty));

            var set = new IntervalIntegerInclusiveSet(Enumerable.Range(5, 5)
                                              .Concat(Enumerable.Range(15, 5))
                                              .Concat(Enumerable.Range(25, 5))
                                              .Concat(Enumerable.Range(35, 5))
                                              .Concat(Enumerable.Range(45, 5))
                                              .Concat(Enumerable.Range(55, 5))
                                              .Concat(Enumerable.Range(65, 5))
                                              .Concat(Enumerable.Range(75, 5))
                                              .Concat(Enumerable.Range(85, 5))
                                              .Concat(Enumerable.Range(95, 5))
                                              .Concat(Enumerable.Range(105, 5))
                                              .Concat(Enumerable.Range(115, 5))
                                              .Select(v => (long)v)
                                              );

            var add = new IntervalIntegerInclusiveSet(Enumerable.Range(0, 1) // new interval
                                              .Concat(Enumerable.Range(1, 1)) // intervale touching the previous one
                                              .Concat(Enumerable.Range(2, 3)) // intervale touching previous new ones  and first current
                                              .Concat(Enumerable.Range(13, 3)) // intervale mordant le coté gauche

                                              .Concat(Enumerable.Range(36, 3)) // intervale a l'interieur

                                              .Concat(Enumerable.Range(46, 9)) // intervale a mordant le cote droit (et devant fusionner avec le prochain courrant)

                                              .Concat(Enumerable.Range(60, 1)) // intervale touchant le coté droit d'un interval
                                              .Concat(Enumerable.Range(61, 4)) // intervale touchant les deux intervales des deux cotés

                                              .Concat(Enumerable.Range(73, 29)) // intervale recouvrant tout
                                              .Concat(Enumerable.Range(102, 15)) // intervale recouvrant tout (et touchant le precedent)
                                              .Select(v => (long)v)
                                              );
            Debug.Assert(set.Reverse().Reverse().IsSame(set));
            Debug.Assert(add.Reverse().Reverse().IsSame(add));

            var res = set.Union(add);
            Debug.Assert(res.AsString == "[0, 9] + [13, 19] + [25, 29] + [35, 39] + [45, 69] + [73, 119]");
            Debug.Assert(res.Exclude(add).AsString == "[5, 9] + [16, 19] + [25, 29] + [35, 35] + [39, 39] + [45, 45] + [55, 59] + [65, 69] + [117, 119]");
        }
    }

}
