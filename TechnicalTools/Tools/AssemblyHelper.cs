﻿using System;
using System.Reflection;

namespace TechnicalTools.Tools
{
    public static class AssemblyHelper
    {
        public static DateTime RetrieveLinkerTimestamp(Assembly assembly = null)
        {
            string filePath = (assembly ?? Assembly.GetCallingAssembly()).Location;
            const int peHeaderOffset = 60;
            const int linkerTimestampOffset = 8;
            var b = new byte[2048];
            System.IO.Stream s = null;

            try
            {
                s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                s.Read(b, 0, 2048);
            }
            finally
            {
                if (s != null)
                    s.Close();
            }

            int i = System.BitConverter.ToInt32(b, peHeaderOffset);
            int secondsSince1970 = System.BitConverter.ToInt32(b, i + linkerTimestampOffset);
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0);
            dt = dt.AddSeconds(secondsSince1970);
            dt = dt.AddHours(TimeZoneInfo.Local.GetUtcOffset(dt).Hours);
            return dt;
        }
    }
}
