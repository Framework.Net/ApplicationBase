﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

using TechnicalTools.Model;


namespace TechnicalTools.Analysis
{
    public partial class XmlSkeletonFinder
    {
        public string GenerateParsingCodeCSharp(TableDef tdef, bool classOnly)
        {
            //var varNames = new Dictionary<string, string>();
            //varNames.Add(tdef.RelatedNode.Document.Root.Name.LocalName, "root");

            // Generate unique names for tables
            var classNames = new Dictionary<string, TableDef>();
            foreach (var def in EnumerateAllHierarchy(tdef, subtdef => subtdef.Children))
            {
                var basename = def.Name.Capitalize();
                var name = basename;
                int i = 0;
                while (classNames.ContainsKey(name))
                    name = basename + ++i;
                classNames.Add(name, def);
            }
            _classNames = classNames.ToDictionary(kvp => kvp.Value, kvp => kvp.Key);

            var parserClassCode = GenerateParserClass("Parser", tdef);
            if (!classOnly)
                parserClassCode = BuildCode(parserClassCode);
            return parserClassCode.RemoveTrailingSpaces();
        }
        Dictionary<TableDef, string> _classNames;

        string ClassName(TableDef table)
        {
            return _classNames[table];
        }
        string PropertyName(ColumnDef col)
        {
            var res = col.Name.Capitalize();
            if (res == ClassName(col.Owner))
                return "Text";
            return res;
        }

        [DebuggerHidden, DebuggerStepThrough]
        public static string BuildCode(string parserClassCode)
        {
            return WrapNamespace("Parsing", parserClassCode);
        }

        [DebuggerHidden, DebuggerStepThrough]
        public static bool PiecesOfCodeAreSimilar(string code1, string code2)
        {
            code1 = code1.RemoveTrailingSpaces().RemoveBlankLines().TrimInternalSpaces();
            code2 = code2.RemoveTrailingSpaces().RemoveBlankLines().TrimInternalSpaces();
            return code1 == code2;
        }

        string GenerateParserClass(string className, TableDef tdef)
        {
            return @"public class " + className + " : " + typeof(BaseParser).ToSmartString(true) + @"
{
    " + _classNames.Select(kvp => GenerateClassFor(kvp.Key).Replace(Environment.NewLine, Environment.NewLine + "    "))
            .Join(Environment.NewLine + Environment.NewLine + "    ") + @"

    protected override void DoParse(XElement node)
    {" + _classNames.Select(kvp => @"
        Result.Add(typeof(" + ClassName(kvp.Key) + "), new List<object>());").Join("") + @"
        var a = Parse" + _classNames.First().Value + @"(node);
        AddResult(a);
    }
    public override void SaveAll(Dictionary<Type, List<object>> results, Action<List<object>> saveAll)
    {
        List<object> lst;" + _classNames.Select(kvp => @"
        if (results.TryGetValue(typeof(" + ClassName(kvp.Key) + @"), out lst))
        {
            saveAll(lst);" + (kvp.Key.Children.Count == 0 ? "" : @"
            foreach (var obj in lst.Cast<" + ClassName(kvp.Key) + @">())
            {" + kvp.Key.Children.Select(childTable => @"
                foreach (var child in ChildrenOf<" + ClassName(childTable) + @">(obj))
                    child." + PropertyName(childTable.Fk) + " = obj." + PropertyName(kvp.Key.Pk) + ";").Join("") + @"
            }") + @"
        }").Join(Environment.NewLine + "        ") + @"
    }

    " + _classNames.Select(kvp => GenerateParsingMethodFor(kvp.Key).Replace(Environment.NewLine, Environment.NewLine + "    "))
            .Join(Environment.NewLine + Environment.NewLine + "    ") + @"
}";

        }

        /// <summary>
        ///  Generate a topological order of table but also sorted by depth.
        ///  This sort will be particularly useful if we merge table that have same name and are child of different parent node.
        ///  The merged table will take the max depth of all node merged.
        ///  So generated code still work (for example "Save" method that need to save table in insert order)
        /// </summary>
        static IEnumerable<T> EnumerateAllHierarchy<T>(T node, Func<T, IEnumerable<T>> getChildren)
        {
            var q = new Queue<T>();
            q.Enqueue(node);
            while (q.Count > 0)
            {
                var n = q.Dequeue();
                yield return n;
                foreach (var child in getChildren(n))
                    q.Enqueue(child);
            }
        }

        string GenerateClassFor(TableDef tdef)
        {
            return @"public class " + ClassName(tdef) + @"
{
    " + tdef.Columns.Select(c => "public " + SqlTypeToCSharpType(c.SqlType) + " " + PropertyName(c) + " { get; set; }")
                    .Join(Environment.NewLine + "    ") + @"
}";
        }
        string SqlTypeToCSharpType(string type)
        {
            if (type.ToLowerInvariant() == "nvarchar(max)")
                return "string";
            if (type.ToLowerInvariant() == "bigint")
                return "long";
            if (type.ToLowerInvariant() == "int")
                return "int";
            return type;
        }

        string GenerateParsingMethodFor(TableDef table)
        {
            var nl = Environment.NewLine;
            return ClassName(table) + " Parse" + ClassName(table) + @"(XElement node)
{
    _metNodes.Add(node);
    var parsedObj = new " + ClassName(table) + "();" + table.RegularColumns.Select(c => (@"
    parsedObj." + PropertyName(c) + " = ").AddIndented(GenerateColumnValueReadingExpression(c, "node") + ";")).Join("")
.IfNotBlankAddSuffix(nl) + table.Children.Select(childTable => @"

    ".AddIndented(GenerateReadAndAttachChildrenOf(table, childTable, "parsedObj"))).Join("")
.IfNotBlankAddSuffix(nl) + @"
    return parsedObj;
}";
        }


        string GenerateReadAndAttachChildrenOf(TableDef parentTable, TableDef childTable, string parentObjVarName)
        {
            var nl = Environment.NewLine;
            var p = childTable.RelatedNode;
            string path = "node";
            while (p != parentTable.RelatedNode)
            {
                path = "GetElements(" + path + ", \"" + p.Name.LocalName + "\")";
                p = p.Parent;
            }
            return "foreach (var child in " + path + ")" + nl +
                   "{" + nl +
                   "    var parsedChild = Parse" + ClassName(childTable) + "(child);" + nl +
                   "    AddResult(parsedChild);" + nl +
                   "    AddChild(" + parentObjVarName + ", parsedChild);" + nl +
                   "}";
        }

        string GenerateColumnValueReadingExpression(ColumnDef col, string nodeVarName)
        {
            var nl = Environment.NewLine;
            var nodePathBeforeSibling = new List<XElement>();
            XElement sibling = (col.SiblingNode as XElement)?.Parent ?? col.RelatedNode.Parent;
            XElement p = sibling;
            while (p != col.Owner.RelatedNode)
            {
                nodePathBeforeSibling.Add(p);
                p = p.Parent;
            }
            nodePathBeforeSibling.Reverse();

            bool exprIsList = false;
            bool extractionHasPrefixPath = false;
            string expr = nodeVarName;
            foreach (var elt in nodePathBeforeSibling)
                if (elt.NextNode is XElement && (elt.NextNode as XElement).Name == elt.Name)
                {
                    if (exprIsList)
                        expr += ".SelectMany(n => GetElements(n, \"" + elt.Name.LocalName + "\"))";
                    else
                        expr = "GetElements(" + expr + ", \"" + elt.Name.LocalName + "\")";
                    exprIsList = true;
                }
                else
                {
                    var proba = elt.Annotation<ProbabilityAnnotation>();
                    var alwaysExist = proba?.ProbabilityOfAppearance == 100m ? "true" : "false";
                    if (exprIsList)
                        expr += ".Select(n => GetElement(n, \"" + elt.Name.LocalName + "\", " + alwaysExist + "))";
                    else
                        expr = "GetElement(" + expr + ", \"" + elt.Name.LocalName + "\", " + alwaysExist + ")";
                    extractionHasPrefixPath = true;
                }

            var nodePathAfterSibling = new List<XElement>();
            if (col.SiblingNode is XElement)
            {
                p = col.RelatedNode.Parent;
                while (p != col.SiblingNode)
                {
                    nodePathAfterSibling.Add(p);
                    p = p.Parent;
                }
                nodePathAfterSibling.Reverse();
            }
            string getPathAfterSibling(string nodeName)
            {
                string expr2 = nodeName;
                foreach (var elt in nodePathAfterSibling)
                {
                    var alwaysExist = elt.Annotation<ProbabilityAnnotation>()?.ProbabilityOfAppearance == 100m ? "true" : "false";
                    expr2 = "GetElement(" + expr2 + ", \"" + elt.Name.LocalName + "\", " + alwaysExist + ")";
                }
                return expr2;
            }


            Func<string, string> getText;
            if (col.RelatedNode is XText)
                getText = nodeName => "GetText(" + getPathAfterSibling(nodeName) + ")";
            else if (col.RelatedNode is XAttribute)
                getText = nodeName => "GetAttribute(" + getPathAfterSibling(nodeName) + ", \"" + (col.RelatedNode as XAttribute).Name.LocalName + "\", false)?.Value";
            else
                getText = nodeName => getPathAfterSibling(nodeName) + "?.Value";


            if (col.SiblingNode is XElement)
            {
                if (exprIsList)
                    expr += ".SelectMany(n => GetElements(n, \"" + (col.SiblingNode as XElement).Name.LocalName + "\"))";
                else
                    expr = "GetElements(" + expr + ", \"" + (col.SiblingNode as XElement).Name.LocalName + "\")";
                exprIsList = true;
            }
            if (exprIsList)
            {
                string indent = nl;
                if (extractionHasPrefixPath)
                    expr = " " + expr;
                else
                    indent += new string(' ', "node".Length);
                expr += indent;
                expr += ".Select(elt => " + getText("elt").Replace(nl, indent + "               ") + ")" + indent
                      + ".Where(str => !string.IsNullOrWhiteSpace(str))";
                expr = "Join(\" \", " + expr.Replace(nl, nl + "          ") + ")";
            }
            else
                expr = getText(expr);

            return expr;
        }

        [DebuggerHidden, DebuggerStepThrough]
        public static string WrapNamespace(string @namespace, string code)
        {
            return @"using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Linq;


namespace Parsing
{
    " + code.Replace(Environment.NewLine, Environment.NewLine + "    ") + @"
}
";
        }

        public abstract class BaseParser
        {
            public Dictionary<Type, List<object>> Parse(XElement root, Action<List<string>> handleWarnings = null)
            {
                _root = root;
                Result = new Dictionary<Type, List<object>>();
                _metNodes = new HashSet<XObject>();
                _itemChildren = new Dictionary<object, List<object>>();
                _metNodes.Add(root);
                DoParse(root);
                var warnings = CheckForgottenNodes(root);
                if (warnings?.Count > 0)
                    if (handleWarnings == null)
                        throw new UserUnderstandableException("Data is present in this XML content which is not parsed thus simply ignored!" + Environment.NewLine +
                                                              "It may contain valuable information... Specify a warning handle to ignore this data!" + Environment.NewLine +
                                                              "Unparsed data: " + Environment.NewLine +
                                                              warnings.Select(str => " - " + str).Join(Environment.NewLine), null);
                    else
                        handleWarnings(warnings);
                return Result;
            }
            protected abstract void DoParse(XElement node);

            public abstract void SaveAll(Dictionary<Type, List<object>> results, Action<List<object>> saveAll);

            XElement _root;
            protected Dictionary<Type, List<object>> Result { get; private set; }
            protected HashSet<XObject> _metNodes;
            Dictionary<object, List<object>> _itemChildren;

            List<string> CheckForgottenNodes(XObject node)
            {
                if (node is XComment)
                    return null;
                var elt = node as XElement;
                if (!_metNodes.Contains(node))
                {
                    string errMsg;
                    if (node is XAttribute att)
                        errMsg = "Attribute \"" + att.Name.LocalName + "\" (of node \"" + att.Parent.PathOf(_root) + "\")";
                    else if (node is XText txt)
                        errMsg = "Node \"" + txt.Parent.PathOf(_root) + "\"";
                    else
                    {
                        Debug.Assert(elt != null);
                        errMsg = "Node \"" + elt.PathOf(_root) + "\"";
                    }
                    if (node is IXmlLineInfo info && info.HasLineInfo())
                        errMsg += ", at line " + info.LineNumber + ", column " + info.LinePosition;
                    errMsg += ", contains unexpected text.";
                    errMsg += " Please check if these data are valuable!";
                    return new List<string>() { errMsg };
                }

                if (elt == null)
                    return null;
                return elt.Nodes().Select(CheckForgottenNodes)
                                    .Where(err => err != null)
                                    .SelectMany(lst => lst)
                                    ?.ToList();
            }

            protected void AddResult<T>(T obj)
            {
                if (!Result.TryGetValue(typeof(T), out List<object>  lst))
                    _itemChildren.Add(typeof(T), lst = new List<object>());
                lst.Add(obj);
            }
            protected void AddChild(object parent, object child)
            {
                if (!_itemChildren.TryGetValue(parent, out List<object> lst))
                    _itemChildren.Add(parent, lst = new List<object>());
                lst.Add(child);
            }
            protected IEnumerable<T> ChildrenOf<T>(object parent)
            {
                if (_itemChildren.TryGetValue(parent, out List<object> lst))
                    foreach (var child in lst.OfType<T>())
                        yield return child;
            }

            protected string GetAggregatedText(XElement node, string separator = " ")
            {
                if (node == null)
                    return null;
                foreach (var n in node.Nodes().OfType<XText>())
                    _metNodes.Add(n);
                return node.GetAggregatedText(separator);
            }
            protected IEnumerable<XElement> GetElements(XElement node, string name)
            {
                if (node == null)
                    return Enumerable.Empty<XElement>();
                return node.Elements().Where(n => n.Name.LocalName == name)
                    .Select(n =>
                    {

                        if (!_metNodes.Contains(n))
                            _metNodes.Add(n);
                        return n;
                    });
            }
            protected XElement GetElement(XElement node, string name, bool expectedToExist)
            {
                if (node == null)
                    return null;
                var res = GetElements(node, name).SingleOrDefault();
                if (res == null && expectedToExist)
                    throw new Exception("Node \"" + name + "\" is supposed to exist in " + node.PathOf(_root));
                if (res != null && !_metNodes.Contains(res))
                    _metNodes.Add(res);
                return res;
            }
            protected XAttribute GetAttribute(XElement node, string name, bool expectedToExist)
            {
                if (node == null)
                    return null;
                var res = node.Attribute(name);
                if (expectedToExist && res == null)
                    throw new Exception("Attribute \"" + name + "\" is supposed to exist for node " + node.PathOf(_root));
                if (res != null && !_metNodes.Contains(res))
                    _metNodes.Add(res);
                return res;
            }

            protected string Join(string separator, IEnumerable<string> values)
            {
                if (values == null)
                    return null;
                return string.Join(separator, values);
            }
        }
    }
}
