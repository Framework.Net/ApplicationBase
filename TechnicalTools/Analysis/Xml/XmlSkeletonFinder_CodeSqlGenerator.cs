﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Linq;


namespace TechnicalTools.Analysis
{
    public partial class XmlSkeletonFinder
    {
        /// <summary>
        /// Imperfect algorithm to generate SQL but pretty useful currently to get 80% of the job done
        /// See Test class in project root /Tests/Analysis/Xml/XmlSkeletonFinder_CodeSqlGenerator
        /// </summary>
        public virtual TableDef GenerateSqlTableScriptForXmlNode(XElement node, string schemaName = "Dbo")
        {
            var tableDef = GenerateSqlTableScript(node, null);
            Reduce(tableDef, null);
            tableDef.SetSchemaRecursive(schemaName);
            return tableDef;
        }

        TableDef GenerateSqlTableScript(XElement node, TableDef parent)
        {
            var tDef = new TableDef()
            {
                Name = node.Name.LocalName,
                RelatedNode = node,
                SiblingNode = node.NextElement()?.IsEmpty ?? false ? node : null // IsEmpty return true only on "<foo/>", not on <foo></foo>
            };
            tDef.Columns.Add(new ColumnDef() // PK
            {
                Name = "Id_" + tDef.Name,
                SqlType = "BIGINT",
                IsPK = true,
                IsAutoIncrementable = true,
                Owner = tDef,
                Nullable = false
            });

            if (parent != null)
                tDef.Columns.Add(new ColumnDef() // FK
                {
                    Name = parent.Name + "_Id",
                    SqlType = "bigint",
                    FkTo = parent.Columns.Single(c => c.IsPK),
                    Owner = parent
                });

            foreach (var att in node.Attributes())
            {
                var col = new ColumnDef() { RelatedNode = att, Name = att.Name.LocalName, Owner = tDef, SqlType = "nvarchar(max)", Nullable = true };
                tDef.Columns.Add(col);
            }
            var textNodes = node.Nodes().OfType<XText>();
            if (node.Elements().IsEmpty() && !textNodes.Any())
            {
                var emptyText = new XText("");
                var annotation = typeof(XText).Assembly.GetTypes()
                                                       .First(t => t.Name == "LineInfoAnnotation")
                                                       .GetConstructors().First()
                                                       .Invoke(new object[] { (node as IXmlLineInfo).LineNumber, (node as IXmlLineInfo).LinePosition + node.Name.LocalName.Length });
                emptyText.AddAnnotation(annotation);
                node.Add(emptyText);
                textNodes = node.Nodes().OfType<XText>();
            }
            if (textNodes.Any()) // case <a> foo [anything]</a>  or  <a></a>
            {
                var col = new ColumnDef() { RelatedNode = textNodes.First(), SiblingNode = textNodes.Count() > 1 ? textNodes.First() : null, Name = node.Name.LocalName, Owner = tDef, SqlType = "nvarchar(max)", Nullable = true };
                tDef.Columns.Add(col);
            }


            foreach (var subNode in node.Nodes())
            {
                if (subNode.NodeType == XmlNodeType.Comment)
                    continue;
                if (subNode.NodeType == XmlNodeType.Text)
                    continue;
                Debug.Assert(subNode.NodeType == XmlNodeType.Element, "What else ?! (I mean... other than Nespresso)");
                var subElement = (XElement)subNode;
                if (subElement.IsEmpty) // check for node <a/> only, not <a></a>
                    continue;
                var subTable = GenerateSqlTableScript(subElement, tDef);
                tDef.Children.Add(subTable);
            }
            return tDef;
        }

        bool Reduce(TableDef table, TableDef parent)
        {
            bool hasChanged = true;
            while (hasChanged)
            {
                hasChanged = false;
                foreach (var subTable in table.Children.ToList())
                    if (Reduce(subTable, table))
                    {
                        hasChanged = true;
                        break;
                    }
            }
            if (parent == null)
                return false;
            return ReduceAction1(table, parent)
                || ReduceAction2(table, parent);
        }

        bool ReduceAction1(TableDef table, TableDef parent)
        {
            // Case : table is only text so we concatenate in a column
            //  <parent>
            //     [anything]
            //     <table>text, or tag with no inner xml structure</table>
            //     // table does not repeat
            //     [anything]
            // </parent>
            // or
            //  <parent>
            //     [anything]
            //     <table>text only</table>
            //     <table />
            //     [anything]
            // </parent>
            if (table.Children.Count() == 0 &&
                (!table.HasSibling ||
                  table.HasSibling && table.RegularColumns.Count() == 1
                                   && table.RegularColumns.First().RelatedNode?.NodeType == XmlNodeType.Text
                                   && (table.RegularColumns.First().SiblingNode == null ||
                                       table.RegularColumns.First().SiblingNode.NodeType == XmlNodeType.Text))
                )
            {
                foreach (var colData in table.RegularColumns)
                {
                    colData.Owner = parent;
                    if (table.HasSibling)
                        colData.SiblingNode = table.SiblingNode; // will prevent  ReduceActionµ1 to be applied again
                    if (table.Name != colData.Name)
                        colData.Name = table.Name + "_" + colData.Name;
                    parent.Children.Remove(table);
                }
                // Insert columns by preserving natural order in xml (result is more user friendly)
                parent.Columns = parent.Columns.Concat(table.RegularColumns)
                                       .OrderBy(c => c.RelatedNode == null ? -1
                                                   : ((IXmlLineInfo)c.RelatedNode).LineNumber * 10000
                                                   + ((IXmlLineInfo)c.RelatedNode).LinePosition)
                                       .ToList();
                return true;
            }
            return false;
        }
        bool ReduceAction2(TableDef table, TableDef parent)
        {
            // Case : table contains multiple innerTable but nothing else,
            //        we merge "table" with "innerTable" => "table_innerTable"
            // <parent>
            //      <table>
            //         [anything complex]
            //      </table>
            //      <table />
            // </parent>
            //var innerTable = table.Children.FirstOrDefault();
            if (parent.Children?.Count == 1 &&
                parent.RegularColumns.Count() == 0 &&
                table.HasSibling)
            {
                parent.Children.Clear();
                parent.Children.AddRange(table.Children);
                foreach (var child in table.Children)
                {
                    var fk = child.Fk;
                    fk.Owner = parent;
                }
                foreach (var col in table.RegularColumns)
                {
                    if (table.Name != col.Name)
                        col.Name = table.Name + "_" + col.Name;
                    col.Owner = parent;
                    parent.Columns.Add(col);
                }
                return true;
            }
            return false;
        }
    }
}
