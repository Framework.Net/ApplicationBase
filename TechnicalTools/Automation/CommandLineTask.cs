﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.Tools;


namespace TechnicalTools.Automation
{
    /// <summary>
    /// Base class for all command line tasks.
    /// Command line task are instanciated by <see cref="AutomationTaskBuilder"/>
    /// See for example the task <see cref="Help" />
    /// </summary>
    public abstract partial class CommandLineTask
    {
        // No Argument here because this is AutomationTaskBuilder that captures the value and set it
        // We do this to avoid to duplicate description in documentation.
        protected DateTime                       AtDate { get; set; }

        /// <summary>
        /// Get the arguments of tasks. It is often all the remaining arguments on command line not parsed by executable.
        /// Filled by AutomationTaskBuilder on task creation/recognition
        /// </summary>
        protected internal IReadOnlyList<string> Args   { get; internal set; }

        public const int SUCCESS = 0; // NOSONAR : because we can use the value in Doc Attribute
        public const int ERROR = 1; // NOSONAR : Idem
        public const int UNCATCHED_ERROR = -100; // NOSONAR : Idem

        protected CommandLineTask(DateTime atDate)
        {
            _log = LogManager.Default.CreateLogger(GetType());
            AtDate = atDate;
        }
        protected ILogger _log { get; }

        public virtual int Run()
        {
            SubscribeLog();

            try
            {
                return DoWork();
            }
            catch (Exception ex)
            {
                if (ex is IBaseException)
                {
                    _log.Error($"A manually raised error of type {ex.GetType().Name} occured while running task {GetType().Name}!", ex);
                    return ERROR;
                }
                else
                {
                    _log.Error($"An unexpected exception of type {ex.GetType().Name} occured while running task {GetType().Name}!", ex);
                    return UNCATCHED_ERROR; // Unexpected error !
                }
            }
            finally
            {
                Console.Out.Flush();
                Console.Error.Flush();

                UnsubscribeLog();
            }
        }

        /// <summary>
        /// Do the main work of your task.
        /// Every main things to do are done before and will be done after the call to this method.
        /// </summary>
        /// <returns><see cref="SUCCESS"/> (0) in case of Success, any other value otherwise. Note that you also can use <see cref="SUCCESS_BUT_WITH_WARNING"/></returns>
        public abstract int DoWork();

        /// <summary>Assign argument values to task's properties </summary>
        protected internal virtual void AssignArgumentValuesFrom(IReadOnlyList<string> args)
        {
            var argProps = ArgumentAttribute.GetSetOn(this)
                                            .Select(kvp => new
                                                {
                                                    Property = kvp.Key,
                                                    Attribute = kvp.Value,
                                                    EffectiveArgument = kvp.Value.GetEffectiveArgumentName(kvp.Key),
                                                    CommandLinePrefix = kvp.Value.AsCommandLinePrefix(kvp.Key),
                                                })
                                            .ToList();

            // Parse args and not argsDefs because we want to assign properties in the command line order
            // We never know if this is important or not so we consider it is
            foreach (var arg in args)
            {
                var argProp = argProps.FirstOrDefault(ap => arg.ToLowerInvariant().StartsWith(ap.CommandLinePrefix.ToLowerInvariant()));
                if (argProp == null)
                {
                    OnUnrecognisedArgument(arg);
                    continue;
                }
                var argValue = arg.Remove(0, argProp.CommandLinePrefix.Length);
                var prop = argProp.Property;
                var propType = prop.PropertyType;
                var nonNullablePropType = propType.TryGetNullableType() ?? propType;
                if (argValue.Length == 0)
                {
                    if (propType.TryGetNullableType() == null && typeof(string) != propType)
                        throw new TechnicalException($"Argument \"{arg}\" would assign an empty value which is not allowed!", null);

                    // Interesting case to handle: mandatory and nullable value
                    // This case forces developper to take responsability on the value to use even if it seems useless at first sight
                    prop.SetValue(this, null);
                }
                else if (typeof(string) == propType)
                {
                    prop.SetValue(this, argValue);
                }
                else if (nonNullablePropType == typeof(bool))
                {
                    if (argValue.ToLowerInvariant().In("true", "y", "yes", "1"))
                        prop.SetValue(this, true);
                    else if (argValue.ToLowerInvariant().In("false", "n", "no", "0"))
                        prop.SetValue(this, false);
                    else
                        throw new UserUnderstandableException($"Argument \"{arg}\" is not defining a boolean!", null);
                }
                else if (nonNullablePropType.IsIntegerType())
                {
                    if (!long.TryParse(argValue, NumberStyles.Any, CultureInfo.InvariantCulture, out long value))
                        throw new UserUnderstandableException($"Argument \"{arg}\" is not defining a number!", null);
                    prop.SetValue(this, Convert.ChangeType(value, nonNullablePropType));
                }
                else if (nonNullablePropType.IsNumericType())
                {
                    if (!decimal.TryParse(argValue, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal value))
                        throw new UserUnderstandableException($"Argument \"{arg}\" is not defining a number!", null);
                    prop.SetValue(this, Convert.ChangeType(value, nonNullablePropType));
                }
                else if (nonNullablePropType == typeof(DateTime))
                {
                    if (!DateTime.TryParseExact(argValue, "yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime value) &&
                        !DateTime.TryParseExact(argValue, "yyyy/MM/dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out value) &&
                        !DateTime.TryParseExact(argValue, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out value) &&
                        !DateTime.TryParseExact(argValue, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out value) &&
                        !DateTime.TryParseExact(argValue, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out value) &&
                        !DateTime.TryParseExact(argValue, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out value) &&
                        !DateTime.TryParseExact(argValue, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out value) &&
                        !DateTime.TryParseExact(argValue, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out value))
                        throw new UserUnderstandableException($"Argument \"{arg}\" is not defining a date or a datetime!", null);
                    prop.SetValue(this, value);
                }
                else if (nonNullablePropType == typeof(TimeSpan))
                {
                    if (!TimeSpan.TryParseExact(argValue, "hh':'mm':'ss", CultureInfo.InvariantCulture, TimeSpanStyles.None, out TimeSpan value))
                        throw new UserUnderstandableException($"Argument \"{arg}\" is not defining a time!", null);
                    prop.SetValue(this, value);
                }
                else if (nonNullablePropType.IsEnum)
                {
                    if (nonNullablePropType != propType && string.IsNullOrWhiteSpace(argValue))
                        prop.SetValue(this, null);

                    Enum value;
                    try
                    {
                        value = (Enum)Enum.Parse(nonNullablePropType, argValue.Replace("|", ","), true);
                    }
                    catch
                    {
                        throw new UserUnderstandableException($"Argument \"{arg}\" is not defining a valid value for type {nonNullablePropType.ToSmartString()}!", null);
                    }
                    if (long.TryParse(argValue, out long v))
                    {
                        string wellFormatedValue = value.ToString();
                        if (wellFormatedValue.Contains(","))
                            wellFormatedValue = wellFormatedValue.Replace(", ", " | ");
                        throw new UserUnderstandableException($"Integer values are not allowed here, use words! Replace the argument \"{arg}\" by \"{argProp.Attribute.AsCommandLineArgument(prop, wellFormatedValue)}\".", null);
                    }
                    prop.SetValue(this, value);
                }
                else
                    throw new UserUnderstandableException($"Does not know how to assign argument \"{arg}\"!", null);

                argProps.Remove(argProp); // responsability taken
            }

            foreach (var argProp in argProps)
                if (argProp.Attribute.IsMandatory)
                    OnMandatoryArgumentMissing(argProp.Property, argProp.Attribute);
        }

        protected virtual void OnMandatoryArgumentMissing(PropertyInfo property, ArgumentAttribute attribute)
        {
            throw new TechnicalException($"Argument \"{attribute.GetEffectiveArgumentName(property)}\" expected on command line." + Environment.NewLine +
                                         $"Add this argument this way : {attribute.AsCommandLineArgument(property, "your_value")}" + Environment.NewLine +
                                         "Note : You can write \"your_value\" (ie: surrounded by double quote) if value contains any non alphanumeric character (like a space)", null);
        }

        /// <summary>
        /// Executed on argument not recognised, default implementation is to raise an exception.
        /// You are free to override this behavior to ignore argument.
        /// </summary>
        /// <exception cref="TechnicalException">Because IT guys are responsible of providing good argument</exception>
        /// <param name="argNameAndValue">full name of unrecognised argument (ie "--foo=bar" for example)</param>
        protected virtual void OnUnrecognisedArgument(string argNameAndValue)
        {
            throw new TechnicalException($"Command line argument \"{argNameAndValue}\" not recognised!", null);
        }

        #region Handling Progress

        /// <summary>
        /// Helper method that create a IProgressa&lt;string&gt; that redirect progress to a log handler.
        /// Usefull for business classes that often ask for a IProgress instance to report their progressions.
        /// </summary>
        /// <typeparam name="T">Often string, just to be generic</typeparam>
        /// <param name="log">the handler where to delegate the progress</param>
        public virtual IProgress<T> CreateProgressRedirect<T>(Action<T> log) { return new ProgressSynchronousRedirect<T>(log); }

        #endregion

        #region Linking to Bus Event message

        /// <summary> Indicate, while executing (method Run) and at the end of task number of warnings raise by code using <see cref="BusEvents.Instance.RaiseBusinessWarning"/>.</summary>
        public int CountOfWarning                   { get; private set; }
        /// <summary> Indicate, while executing (method Run) and at the end of task number of warnings raise by code using <see cref="BusEvents.Instance.RaiseBusinessMessage"/>.</summary>
        public int CountOfBusinessMessage           { get; private set; }
        /// <summary> Indicate, while executing (method Run) and at the end of task number of warnings raise by code using <see cref="BusEvents.Instance.RaiseUserUnderstandableMessage"/>.</summary>
        public int CountOfUserUnderstandableMessage { get; private set; }
        /// <summary> Indicate, while executing (method Run) and at the end of task number of warnings raise by code using <see cref="BusEvents.Instance.RaiseTechnicalError"/>.</summary>
        public int CountOfTechnicalError            { get; private set; }

        void SubscribeLog()
        {
            BusEvents.Instance.BusinessWarning           += BusinessEvents_Warning;
            BusEvents.Instance.BusinessMessage           += BusinessEvents_BusinessMessage;
            BusEvents.Instance.UserUnderstandableMessage += BusinessEvents_UserUnderstandableMessage;
            BusEvents.Instance.TechnicalError            += BusinessEvents_TechnicalError;
            LogManager.Default.Log += OnAnyLog;

            CountOfWarning = 0;
            CountOfBusinessMessage = 0;
            CountOfUserUnderstandableMessage = 0;
            CountOfTechnicalError = 0;
        }
        void UnsubscribeLog()
        {
            BusEvents.Instance.BusinessWarning           -= BusinessEvents_Warning;
            BusEvents.Instance.BusinessMessage           -= BusinessEvents_BusinessMessage;
            BusEvents.Instance.UserUnderstandableMessage -= BusinessEvents_UserUnderstandableMessage;
            BusEvents.Instance.TechnicalError            -= BusinessEvents_TechnicalError;
            LogManager.Default.Log -= OnAnyLog;
        }

        // Dont make this method static so method reference in Log is different for each task
        void OnAnyLog(ILogger logger, ILog log)
        {
            if (log.Level == Level.Warn ||
                log.Level == Level.Error ||
                log.Level == Level.Fatal)
                Console.Error.WriteLine(log.ToFormatedMessage(false));
            else
                Console.WriteLine(log.ToFormatedMessage(false));
        }

        void BusinessEvents_Warning(object sender, BusinessWarningEventArgs e)
        {
            ++CountOfWarning;
        }
        void BusinessEvents_BusinessMessage(object sender, MessageEventArgs e)
        {
            ++CountOfBusinessMessage;
        }
        void BusinessEvents_UserUnderstandableMessage(object sender, MessageEventArgs e)
        {
            ++CountOfUserUnderstandableMessage;
        }
        void BusinessEvents_TechnicalError(object sender, TechnicalErrorEventArgs e)
        {
            ++CountOfTechnicalError;
        }

        #endregion Linking to Bus Event message
    }
}
