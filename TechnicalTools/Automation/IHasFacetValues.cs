﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TechnicalTools.Automation
{
    public interface IHasFacetValues
    {
        IReadOnlyDictionary<string, object> Values { get; }
    }
}
