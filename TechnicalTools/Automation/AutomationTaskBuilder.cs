﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

using TechnicalTools.Algorithm.Graph;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;


namespace TechnicalTools.Automation
{
    [TaskDescription(BusinessBehaviorAndDescription = "Bootstrap task that understands the real task to run.")]
    public class AutomationTaskBuilder : CommandLineTask
    {
        public CommandLineTask  RecognisedTask                   { get; private set; }
        public string           InvalidTaskCommandLineReason     { get; protected set; }

        public AutomationTaskBuilder(List<string> args)
            : base(DateTime.Now)
        {
            _args = args;
        }
        readonly List<string> _args;

        /// <summary>
        /// Give the default task to execute if argument "--execute=..." is not provided on command line.
        /// Default implementation return null to indicate there is not default task.
        /// </summary>
        /// <returns></returns>
        protected virtual CommandLineTask CreateDefaultTask(DateTime date)
        {
            return null;
        }
        // ReSharper disable UnusedAutoPropertyAccessor.Local

        /// <remarks>This property is filled, by the class itself, the same way other task argument are filled.</remarks>
        [Argument(IsMandatory = true,
                  Description = "Indicate which TaskName to execute. The name can be short (class name) or full (class full name).\r\n" +
                                "Full name can be useful if a task is hidden by a more specific business task from a higher level assembly.\r\n" +
                                "But usually if a task is hidden by another with same name this has been done on purpose.\r\n" +
                                "Not mandatory if \"" + nameof(CreateDefaultTask) + "\" is overridden by developer (which is almost never the case).")]
        public string   Execute    { get; private set; }

        [Argument(IsMandatory = false,
          DefaultValue = "System.DateTime.Now (unless overridden through \"GetDefaultDate() for example for systems that want UTC date by default\")",
          Description = "Allow to define the date of execution used by task in case a task handles data by day or if the task generate data for a day.\r\n"
                      + "Time part is allowed but can be useless given the purpose of task.\r\n"
                      + "This property also allow execution retry in past or for simulation (past or futur).")]
        public DateTime Date { get { return base.AtDate; } protected set { base.AtDate = value; } }


        // ReSharper restore UnusedAutoPropertyAccessor.Local

        public override int DoWork()
        {
            _argsToDelegate = new List<string>();

            Date = GetDefaultDate();
            AssignArgumentValuesFrom(_args);

            CommandLineTask task;
            if (string.IsNullOrWhiteSpace(Execute))
            {
                var defaultTask = CreateDefaultTask(Date);
                if (defaultTask == null)
                {
                    InvalidTaskCommandLineReason = "No argument on command line specifying task name to run!";
                    return ERROR;
                }
                task = defaultTask;
            }
            else
            {
                var cmdType = AllExecutableTasks().FirstOrDefault(t => t.FullName.ToLowerInvariant() == Execute.ToLowerInvariant())
                           ?? AllExecutableTasks().FirstOrDefault(t => t.Name.ToLowerInvariant() == Execute.ToLowerInvariant());
                if (cmdType == null)
                {
                    InvalidTaskCommandLineReason = $"Command {Execute} not recognised as valid!";
                    return ERROR;
                }

                try
                {
                    task = BuildTask(cmdType);
                    Debug.Assert(task != null);
                }
                catch (Exception ex)
                {
                    InvalidTaskCommandLineReason = ExceptionManager.Instance.Format(ex);
                    return ERROR;
                }
            }
            task.Args = _argsToDelegate.ToList();
            task.AssignArgumentValuesFrom(task.Args);
            RecognisedTask = task;

            return SUCCESS;
        }
        List<string> _argsToDelegate;

        /// <summary>
        /// Default implementation is DateTime.Now.
        /// Allow developper to override use Date part only or UTC etc
        /// </summary>
        protected virtual DateTime GetDefaultDate()
        {
            return DateTime.Now;
        }

        /// <summary>
        /// Enumerate, in a specific order, all the available tasks.
        /// Note that some tasks belonging to coampnycan hide the default ones.
        /// Even if the default one are not abstract!
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Type> AllExecutableTasks()
        {
            var allTypes = typeof(CommandLineTask).GetAllInheritingTypes().ToList();
            foreach (var t in allTypes)
                if (!t.IsAbstract && !typeof(AutomationTaskBuilder).IsAssignableFrom(t))
                    yield return t;
        }

        public static List<Type> AllAccessibleTasks(bool showHidden = false)
        {
            var all = AllExecutableTasks().ToList();

            // Sort all namespace by dependencies:
            // Task "help" should be the first one and higher business level tasks the last ones.
            var allAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToDictionary(ass => ass.FullName);
            IEnumerable<Assembly> getDependencies(Assembly ass)
            {
                return ass.GetReferencedAssemblies()
                          // Because assembly System and System.Configuration auto reference each other
                          .Where(an => !an.FullName.StartsWith("System."))
                          .SelectNotNull(an => allAssemblies.TryGetValueClass(an.FullName));
            }
            var assembliesOrders = TopologicalOrderSimple.DoTopologicalSort(Assembly.GetEntryAssembly(), getDependencies)
                                    .AsEnumerable()
                                    .Reverse()
                                    .Select((ass, i) => new { ass, i })
                                    .ToDictionary(t => t.ass, t => t.i);
            Debug.Assert(all.All(type => assembliesOrders.ContainsKey(type.Assembly)),
                         "Not a big deal if this assert fails, just curious to know why it would!");

            var sorted = all
                            .Where(type => showHidden
                                        || !(type.GetCustomAttributes<TaskDescriptionAttribute>(true).LastOrDefault()?.Hidden ?? false))
                            // Sort task using assembly dependencies
                            .OrderBy(type => assembliesOrders.TryGetValueStruct(type.Assembly) ?? int.MaxValue) // protected from potential bug with int.MaxValue
                            // then by natural order
                            .ThenBy(type => type.Assembly.GetName().Name)
                            .ThenBy(type => type.Name)
                            // Group by name so we remove the hidden/overriden tasks
                            .GroupBy(type => type.Name)
                            .Select(grp => grp.Last())
                            .ToList();
            return sorted;
        }

        /// <summary>
        /// Create an instance of the task referenced by cmdType.
        /// Task constructor of type is expected to have only one parameter <see cref="Date"/>
        /// To override if application's tasks have more parameters.
        /// </summary>
        protected virtual CommandLineTask BuildTask(Type cmdType)
        {
            var cons = cmdType.GetConstructor(new[] { typeof(DateTime) });
            if (cons == null)
                throw new TechnicalException($"Internal Error: Task {Execute} has an unexpected constructor signature for a {nameof(CommandLineTask)}!", null);
            var task = (CommandLineTask)cons.Invoke(new object[] { Date });
            return task;
        }

        protected override void OnUnrecognisedArgument(string argNameAndValue)
        {
            _argsToDelegate.Add(argNameAndValue);
        }

        protected override void OnMandatoryArgumentMissing(PropertyInfo property, ArgumentAttribute attribute)
        {
            if (property.Name == nameof(Execute) && CreateDefaultTask(DateTime.MinValue) != null)
                return;
            base.OnMandatoryArgumentMissing(property, attribute);
        }
    }
}
