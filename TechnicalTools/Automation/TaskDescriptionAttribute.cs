﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TechnicalTools.Automation
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class TaskDescriptionAttribute : Attribute, IHasFacetValues
    {
        [Description("Explain what the task does from a business point of view.")]
        public string BusinessBehaviorAndDescription  { get => GetClassFacetValue<string>(); set => SetClassFacetValue(value); }
        [Description("Explain what the task does from a technical point of view (which data are created / moved / altered, etc).")]
        public string TechnicalBehaviorAndDescription { get => GetClassFacetValue<string>(); set => SetClassFacetValue(value); }
        [Description("Tell if task is designed to be be executed each day, \r\n" +
                     "or if the task is able to catch up late work\r\n" +
                     "(saturday / sunday can generate works to do, and the task is able to do all monday).")]
        public bool  MustBeExecutedEachDay            { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }
        [Description("Tell if this task is designed to be executable more than once in same day (only in a normal behavior of task, not crash).")]
        public bool  RelaunchableInSameDay            { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }
        [Description("Tell if this task can be run multiple time concurrently (if ture, it implies RelaunchableInSameDay is true).")]
        public bool  ParallelExecutable               { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }
        [Description("For Support team: We can tell our life here...")]
        public string KnownProductionProblems         { get => GetClassFacetValue<string>(); set => SetClassFacetValue(value); }
        [Description("By default 0 is success and other values are for error.\r\n" +
                     "If some values, different than 0, mean kind of \"succes but with warnings\",\r\n" +
                     "indicate values here with an optional explanation...")]
        public string WarningResultCode               { get => GetClassFacetValue<string>(); set => SetClassFacetValue(value); }
        [Description("Hide the task from documentation except if " + nameof(AutoDoc.Help.ShowHiddenTasks) + " is true")]
        public bool   Hidden                          { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }

        public    IReadOnlyDictionary<string, object>   Values { get { return  _Values; } }
        protected          Dictionary<string, object>  _Values { get { return __Values ?? (__Values = new Dictionary<string, object>()); } }
           [NonSerialized] Dictionary<string, object> __Values;
        protected void SetClassFacetValue<T>(T value, [CallerMemberName] string facetName = null)
        {
            _Values[facetName] = value;
        }
        protected T    GetClassFacetValue<T>(         [CallerMemberName] string facetName = null)
        {
            if (_Values.TryGetValue(facetName, out object v))
                return v is T ? (T)v : default(T);
            return default(T);
        }

    }
}
