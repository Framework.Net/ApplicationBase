﻿using System;
using System.Collections.Generic;


namespace TechnicalTools.Automation.AutoDoc.Visitors
{
    public class BrowseVisitor<T> : IDocVisitor<T>
    {
        public static List<DocBlock> InfixOrderedList()
        {
            var ts = new List<DocBlock>();
            var browser = new BrowseVisitor<T>(t => ts.Add(t), _ => { });
            return ts;
        }
        public static List<DocBlock> SuffixOrderedList()
        {
            var ts = new List<DocBlock>();
            var browser = new BrowseVisitor<T>(_ => { }, t => ts.Add(t));
            return ts;
        }

        public BrowseVisitor(Action<DocBlock> infix, Action<DocBlock> suffix)
        {
            _infix = infix;
            _suffix = suffix;
        }
        readonly Action<DocBlock> _infix;
        readonly Action<DocBlock> _suffix;

        T IDocVisitor<T>.Visit(DocText           b) { _infix(b); _suffix(b); return default(T); }
        T IDocVisitor<T>.Visit(DocContainer      b) { return VisitContainer(b); }
        protected virtual T VisitContainer(DocContainer b)
        {
            _infix(b);
            foreach (var sb in b.Blocks)
                sb.Accept(this);
            _suffix(b);
            return default(T);
        }

        T IDocVisitor<T>.Visit(DocBold           b) { return VisitContainer(b); }
        T IDocVisitor<T>.Visit(DocItalic         b) { return VisitContainer(b); }
        T IDocVisitor<T>.Visit(DocUnderlined     b) { return VisitContainer(b); }
        T IDocVisitor<T>.Visit(DocColor          b) { return VisitContainer(b); }
        T IDocVisitor<T>.Visit(DocParagraph      b) { return VisitContainer(b); }
        T IDocVisitor<T>.Visit(DocBulletList     b) { _infix(b);
                                                        foreach (var sb in b.Bullets) sb.Accept(this);
                                                        _suffix(b);
                                                        return default(T); }
        T IDocVisitor<T>.Visit(DocDocumentTitle  b) { _infix(b); _suffix(b); return default(T); }
        T IDocVisitor<T>.Visit(DocMainTitle      b) { _infix(b); _suffix(b); return default(T); }
        T IDocVisitor<T>.Visit(DocTinyTitle      b) { _infix(b); _suffix(b); return default(T); }
    }
}
