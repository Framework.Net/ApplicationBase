﻿using System;


namespace TechnicalTools.Automation.AutoDoc.Visitors
{
    public interface IDocVisitor<T>
    {
        T Visit(DocText           b);
        T Visit(DocContainer      b);
        T Visit(DocBold           b);
        T Visit(DocItalic         b);
        T Visit(DocUnderlined     b);
        T Visit(DocColor          b);
        T Visit(DocParagraph      b);
        T Visit(DocBulletList     b);

        T Visit(DocDocumentTitle  b);
        T Visit(DocMainTitle      b);
        T Visit(DocTinyTitle      b);
    }
}
