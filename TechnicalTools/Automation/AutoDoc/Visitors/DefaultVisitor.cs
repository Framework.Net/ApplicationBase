﻿using System;


namespace TechnicalTools.Automation.AutoDoc.Visitors
{
    public class DefaultVisitor<T> : IDocVisitor<T>
    {
        T IDocVisitor<T>.Visit(DocText           b) { return default(T); }
        T IDocVisitor<T>.Visit(DocContainer      b) { return VisitContainer(b); }
        protected virtual T VisitContainer(DocContainer b)
        {
            foreach (var sb in b.Blocks)
                sb.Accept(this);
            return default(T);
        }

        T IDocVisitor<T>.Visit(DocBold           b) { return VisitContainer(b); }
        T IDocVisitor<T>.Visit(DocItalic         b) { return VisitContainer(b); }
        T IDocVisitor<T>.Visit(DocUnderlined     b) { return VisitContainer(b); }
        T IDocVisitor<T>.Visit(DocColor          b) { return VisitContainer(b); }
        T IDocVisitor<T>.Visit(DocParagraph      b) { return VisitContainer(b); }
        T IDocVisitor<T>.Visit(DocBulletList     b) { foreach (var sb in b.Bullets) sb.Accept(this);
                                                        return default(T); }

        T IDocVisitor<T>.Visit(DocDocumentTitle  b) { return default(T); }
        T IDocVisitor<T>.Visit(DocMainTitle      b) { return default(T); }
        T IDocVisitor<T>.Visit(DocTinyTitle      b) { return default(T); }
    }
}
