﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace TechnicalTools.Automation.AutoDoc.Visitors
{
    // TODO : Spearate in two visitors :
    //        - one that change/fix/arrange the tree (see DocBulletList for example) to get immutable tree,
    //        - the other that just displays it
    public class RawTextPrinterVisitor : IDocVisitor<object>
    {
        public RawTextPrinterVisitor() { }

        public StringBuilder Print(IReadOnlyCollection<DocBlock> blocks)
        {
            _maxTitleLength = 0;
            var browse = new BrowseVisitor<object>(b => _maxTitleLength = Math.Max(_maxTitleLength, (b as DocAbstractTitle)?.Text.Length ?? 0), b => { });
            foreach (var b in blocks)
                b.Accept(browse);
            _maxTitleLength += MarginAroundTitle.Length * 2;
            _maxTitleLength = Math.Max(_maxTitleLength, 78);

            _sb = new StringBuilder();
            foreach(var b in blocks)
                b.Accept(this);
            return _sb;
        }
        int _maxTitleLength;
        StringBuilder _sb;
        const string MarginAroundTitle = "  ";

        object IDocVisitor<object>.Visit(DocText b)
        {
            var trimmed = b.NoTrim ? b.Text : b.Text?.Trim(' ');
            if (trimmed?.Length > 0)
            {
                if (_sb.Length > 0 &&
                    !char.IsWhiteSpace(_sb[_sb.Length - 1]) && !char.IsWhiteSpace(trimmed[0]))
                    _sb.Append(' ');

                _sb.Append(trimmed);
            }
            return null;
        }
        object IDocVisitor<object>.Visit(DocContainer  b) { return VisitContainer(b); }
        object IDocVisitor<object>.Visit(DocBold       b) { return VisitContainer(b); }
        object IDocVisitor<object>.Visit(DocItalic     b) { return VisitContainer(b); }
        object IDocVisitor<object>.Visit(DocUnderlined b) { return VisitContainer(b); }
        object IDocVisitor<object>.Visit(DocColor      b) { return VisitContainer(b); }
        object IDocVisitor<object>.Visit(DocParagraph  b) { var res = VisitContainer(b); _sb.Append(CompleteLastBlankLineUpTo(2)); return res; }
        protected virtual string VisitContainer(DocContainer b)
        {
            foreach (var sb in b.Blocks)
                sb.Accept(this);
            return null;
        }

        object IDocVisitor<object>.Visit(DocBulletList b)
        {
            _sb.Append(CompleteLastBlankLineUpTo(1));
            var bullet = new DocText("  - ") { NoTrim = true };
            foreach (var sb in b.Bullets)
            {
                var backup = _sb;
                _sb = new StringBuilder();
                bullet.Accept(this);
                sb.Accept(this);
                backup.Append(_sb.ToString().Replace(Environment.NewLine, Environment.NewLine + new string(' ', bullet.Text.Length)));
                _sb = backup;
                _sb.Append(Environment.NewLine);
                _sb.Append(Environment.NewLine);
            }
            return null;
        }
        object IDocVisitor<object>.Visit(DocDocumentTitle b) { return VisitTitle(b, '*', true); }
        object IDocVisitor<object>.Visit(DocMainTitle     b) { return VisitTitle(b, '=', false); }
        object IDocVisitor<object>.Visit(DocTinyTitle     b) { return VisitTitle(b, '-', false); }
        protected virtual object VisitTitle(DocAbstractTitle b, char c, bool doLastLine)
        {
            var line = MarginAroundTitle + b.Text?.Trim() + MarginAroundTitle;
            line = new string(c, (_maxTitleLength - line.Length) / 2)
                    + line
                    + new string(c, _maxTitleLength - line.Length - (_maxTitleLength - line.Length) / 2);
            if (_sb.Length > 0)
                _sb.Append(CompleteLastBlankLineUpTo(3 - b.Depth));
            var res = new string(c, line.Length) + Environment.NewLine
                    + line
                    + (doLastLine ? Environment.NewLine + new string(c, line.Length) : "")
                    + Environment.NewLine;
            _sb.Append(res);
            _sb.Append(CompleteLastBlankLineUpTo(3 - b.Depth));
            return null;
        }
        protected string CompleteLastBlankLineUpTo(int nbNewLineWished)
        {
            var nbNewLineCurrent = CountLastExistingEmptyLines();
            var spaces = Enumerable.Range(0, nbNewLineWished).Select(_ => Environment.NewLine).Join(string.Empty);
            var nbSpaces = Math.Max(nbNewLineWished - nbNewLineCurrent, 0);
            return spaces.Truncate(nbSpaces * Environment.NewLine.Length);
        }

        int CountLastExistingEmptyLines()
        {
            int count = 0;
            var nl = Environment.NewLine;

            int i = _sb.Length - 1;
            if (i == -1)
                return 0;

            int n = nl.Length - 1;
            while (_sb[i] == nl[n])
                {
                    --n;
                    --i;
                    if (n == -1)
                    {
                        n = nl.Length - 1;
                        ++count;
                    }
                }
            return count
                    - (i == -1 ? 0 : 1); // We don't count the end of line after any text
        }
    }
}
