﻿using System;
using System.Collections.Generic;
using System.Drawing;

using TechnicalTools.Automation.AutoDoc.Visitors;


namespace TechnicalTools.Automation.AutoDoc
{
    public abstract class DocBlock
    {
        public abstract T Accept<T>(IDocVisitor<T> v);

        public override string ToString()
        {
            var printer = new RawTextPrinterVisitor();
            return printer.Print(new[] { this }).ToString();
        }
    }
    public class DocText : DocBlock
    {
        public string Text          { get; set; }
        protected internal bool NoTrim { get; set; }

        public DocText(string text) { Text = text; }
        //public static implicit operator DocText(string text) { return new DocText(text); }

        public override T Accept<T>(IDocVisitor<T> v) { return v.Visit(this); }
    }

    public class DocContainer : DocBlock
    {
        public List<DocBlock> Blocks { get; } = new List<DocBlock>();
        public DocContainer() { }
        public DocContainer(IEnumerable<DocBlock> blocks) { Blocks.AddRange(blocks); }
        public DocContainer(params DocBlock[] blocks) { Blocks.AddRange(blocks); }
        public override T Accept<T>(IDocVisitor<T> v) { return v.Visit(this); }
    }
    public class DocBold : DocContainer
    {
        public DocBold(string text) : this(new DocText(text).WrapInList()) { }
        public DocBold(IEnumerable<DocBlock> blocks) : base(blocks) { }
        public override T Accept<T>(IDocVisitor<T> v) { return v.Visit(this); }
    }
    public class DocItalic : DocContainer
    {
        public DocItalic(string text) : this(new DocText(text).WrapInList()) { }
        public DocItalic(IEnumerable<DocBlock> blocks) : base(blocks) { }

        public override T Accept<T>(IDocVisitor<T> v) { return v.Visit(this); }
    }
    public class DocUnderlined : DocContainer
    {
        public DocUnderlined(string text) : this(new DocText(text).WrapInList()) { }
        public DocUnderlined(IEnumerable<DocBlock> blocks) : base(blocks) { }

        public override T Accept<T>(IDocVisitor<T> v) { return v.Visit(this); }
    }
    public class DocColor : DocContainer
    {
        public Color Color { get; set; }

        public DocColor(Color c, string text) : this(c, new DocText(text).WrapInList()) { }
        public DocColor(Color c, IEnumerable<DocBlock> blocks) : base(blocks) { Color = c; }

        public override T Accept<T>(IDocVisitor<T> v) { return v.Visit(this); }
    }
    public class DocParagraph : DocContainer
    {
        public DocParagraph(string text) : this(new DocText(text).WrapInList()) { }
        public DocParagraph(IEnumerable<DocBlock> blocks) : base(blocks) { }

        public override T Accept<T>(IDocVisitor<T> v) { return v.Visit(this); }
    }
    public class DocBulletList : DocBlock
    {
        public List<DocBlock> Bullets { get; } = new List<DocBlock>();
        public DocBulletList(IEnumerable<DocBlock> bullets) { Bullets.AddRange(bullets); }
        public override T Accept<T>(IDocVisitor<T> v) { return v.Visit(this); }
    }


    public abstract class DocAbstractTitle : DocBlock
    {
        public abstract int Depth { get;  }
        public string Text { get; set;  }
        public DocAbstractTitle(string text) { Text = text; }
    }
    public class DocDocumentTitle : DocAbstractTitle
    {
        public override int Depth { get { return 0; } }
        public DocDocumentTitle(string text) : base(text) { }
        public override T Accept<T>(IDocVisitor<T> v) { return v.Visit(this); }
    }
    public class DocMainTitle : DocAbstractTitle
    {
        public override int Depth { get { return 1; } }
        public DocMainTitle(string text) : base(text) { }
        public override T Accept<T>(IDocVisitor<T> v) { return v.Visit(this); }
    }
    public class DocTinyTitle : DocAbstractTitle
    {
        public override int Depth { get { return 2; } }
        public DocTinyTitle(string text) : base(text) { }
        public override T Accept<T>(IDocVisitor<T> v) { return v.Visit(this); }
    }
}
