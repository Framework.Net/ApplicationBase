﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

using TechnicalTools.Automation.AutoDoc.Visitors;


namespace TechnicalTools.Automation.AutoDoc
{
    [TaskDescription(BusinessBehaviorAndDescription = "Tell user about all tasks available in application with details about task and arguments.\r\n" +
                                                      "This is this task that is responsible of writting the documentation you are currently reading!",
                     TechnicalBehaviorAndDescription = "Should do nothing (no log etc) except dumping all informations about available task on standard output.")]
    public class Help : CommandLineTask
    {
        [Argument(Description = "Tell if output must be html formatted. When false, output is raw text formatted.",
                  DefaultValue = false)]
        public bool Html { get; set; }

        [Argument(Description = "Display also hidden task(s).",
                  DefaultValue = false)]
        public bool ShowHiddenTasks { get; set; }
        [Argument(Description = "Display also hidden argument(s) on displayed task(s).",
                  DefaultValue = false)]
        public bool ShowHiddenArguments { get; set; }

        public virtual ColorizerVisitor.Colors Options { get; }

        public Help(DateTime atDate)
            : base(atDate)
        {
        }

        public override int DoWork()
        {
            string report;
            var blocks = GenerateDocumentation();
            if (Html)
            {
                var colorizer = new ColorizerVisitor(Options ?? new ColorizerVisitor.Colors());
                blocks = blocks.Select(b => b.Accept(colorizer)).ToList();
                var title = blocks.OfType<DocDocumentTitle>().FirstOrDefault()?.Text
                         ?? ProductName + (ProductName.ToLowerInvariant().EndsWith("s") ? "'" : "'s") + " tasks documentation";
                report = new HtmlPrinterVisitor().Print(blocks, title);
            }
            else
                report = new RawTextPrinterVisitor().Print(blocks).ToString();
            Console.WriteLine(report);
            return SUCCESS;
        }

        protected virtual string ProductName
        {
            get
            {
                if (_ProductName == null)
                {
                    var att = (AssemblyProductAttribute)Attribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof(AssemblyProductAttribute), false);
                    _ProductName = att?.Product
                                ?? Assembly.GetEntryAssembly().GetName().Name;
                }
                return _ProductName;
            }
        }
        string _ProductName;

        protected List<DocBlock> GenerateDocumentation()
        {
            var bs = new List<DocBlock>
            {
                new DocDocumentTitle($"{ProductName}'{(ProductName.ToLowerInvariant().EndsWith("s") ? "" : "s")} command line tasks"),
                new DocMainTitle("Summary"),
                new DocParagraph($"This document explain all technical and business available tasks that can be run from software {ProductName} in command line.\r\n" +
                                  "All tasks' arguments an their meanings are also detailed.\r\n" +
                                  "Please note that one of these task allows you to fully regenerate this document!"),
                new DocMainTitle("How tasks are described in this document"),
                new DocTinyTitle("Tasks are described with these criteria") // note: singular of criteria is criterion
            };
            var taskProps = GetDocumentedTaskFacets();
            bs.Add(new DocBulletList(taskProps.Select(p => new DocContainer(
                                            new DocBold(p.Name.Uncamelify() + ": " + Environment.NewLine),
                                            new DocText(p.GetCustomAttribute<DescriptionAttribute>()?.Description ?? "TODO!")))));
            bs.Add(new DocTinyTitle("Tasks' argument are described with these criteria")); // note: singular of criteria is criterion
            var argProps = GetDocumentedTaskArgumentFacets();
            bs.Add(new DocBulletList(argProps.Select(p => new DocContainer(
                                        new DocBold(p.Name.Uncamelify() + ": " + Environment.NewLine),
                                        new DocText(p.GetCustomAttribute<DescriptionAttribute>()?.Description ?? "TODO!") ))));

            bs.AddRange(BeforeTasksDescriptions());
            bs.Add(new DocMainTitle("Tasks descriptions"));

            bs.Add(new DocParagraph("All arguments of all tasks are in the form --NameofArgument=Value or --NameofArgument=\"Value with space\". " +
                                    "Short format for argument does not exist so all command lines always remain human-readable. " +
                                    "Here is the list of tasks slightly ordered from the most simple / technical one to the most complex / business ones"));
            foreach (var dt in GetDocumentedTasks(ShowHiddenTasks))
            {
                bs.Add(new DocTinyTitle($"Task \"{dt.Name}\"  (assembly: " + dt.Assembly.FullName.Remove(dt.Assembly.FullName.IndexOf(',')) + ")"));
                bs.AddRange(DocumentedTaskToDoc(dt));
                bs.Add(new DocText(Environment.NewLine));
                bs.Add(new DocBold("Arguments:"));
                bs.Add(new DocText(Environment.NewLine));

                var argsProps = GetDocumentedPropertyArguments(dt, ShowHiddenArguments);
                bs.Add(new DocBulletList(argsProps.Select(ap => new DocContainer(
                                new DocBold(ap.Name + ": ").WrapInList()
                                .Concat(DocumentedPropertyArgumentToDoc(ap))))));
            }
            return bs;
        }

        protected virtual List<PropertyInfo> GetDocumentedTaskFacets()
        {
            var props = typeof(TaskDescriptionAttribute)
                            .GetProperties()
                            .Where(p => p.DeclaringType != typeof(Attribute))
                            .Where(p => p.GetSetMethod(false) != null)
                            .ToList();
            return props;
        }

        protected virtual List<PropertyInfo> GetDocumentedTaskArgumentFacets()
        {
            var props = typeof(ArgumentAttribute)
                            .GetProperties()
                            .Where(p => p.DeclaringType != typeof(Attribute))
                            .Where(p => p.GetSetMethod() != null)
                            .ToList();
            return props;
        }

        protected virtual List<Type> GetDocumentedTasks(bool showHidden = false)
        {
            return AutomationTaskBuilder.AllAccessibleTasks(showHidden);
        }
        protected virtual IEnumerable<DocBlock> DocumentedTaskToDoc(Type t)
        {
            var descAtts = t.GetCustomAttributes<TaskDescriptionAttribute>(true).DefaultIfEmpty(new TaskDescriptionAttribute());
            var facets = GetDocumentedTaskFacets();

            var bullets = new List<DocBlock>();
            foreach (var facet in facets)
            {
                var v = AggregateFacetValue(facet, descAtts);
                if (v == null && facet.Name == nameof(TaskDescriptionAttribute.BusinessBehaviorAndDescription))
                    v = "TODO!";
                if (v == null)
                    continue;
                var quote = v is string && string.IsNullOrWhiteSpace((string)v) ? "\""
                          : v is char ? "'"
                          : "";
                bullets.Add(new DocContainer(
                                new DocBold(facet.Name.Uncamelify() + ": " + Environment.NewLine),
                                new DocText(quote + v.ToString() + quote)));
            }
            yield return new DocBulletList(bullets);
        }

        protected virtual List<PropertyInfo> GetDocumentedPropertyArguments(Type documentedTask, bool showHidden = false)
        {
            return documentedTask
                    .GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
                    .Where(p => p.GetCustomAttributes<ArgumentAttribute>(true).Any())
                    .Where(p => showHidden
                             || !(p.GetCustomAttributes<ArgumentAttribute>(true).LastOrDefault()?.Hidden ?? false))
                    .ToList();
        }
        protected virtual IEnumerable<DocBlock> DocumentedPropertyArgumentToDoc(PropertyInfo documentedArgument)
        {
            var descAtts = documentedArgument.GetCustomAttributes<ArgumentAttribute>(true);
            var facets = GetDocumentedTaskArgumentFacets();

            var bullets = new List<DocBlock>();
            foreach (var facet in facets)
            {
                var v = AggregateFacetValue(facet, descAtts);
                if (v == null && facet.Name == nameof(ArgumentAttribute.Description))
                    v = "TODO!";
                if (v == null)
                    continue;
                var quote = v is string && string.IsNullOrWhiteSpace((string)v) ? "\""
                          : v is char ? "'"
                          : "";
                bullets.Add(new DocContainer(
                                new DocBold(facet.Name.Uncamelify() + ": " + Environment.NewLine),
                                new DocText(quote + v.ToString() + quote)));
            }
            yield return new DocBulletList(bullets);
        }

        protected virtual IEnumerable<DocBlock> BeforeTasksDescriptions()
        {
            yield return new DocMainTitle("How to run a task");
            yield return new DocParagraph("There is a minimum set of arguments to know / fill to run a task:");
            var argsProps = GetDocumentedPropertyArguments(typeof(AutomationTaskBuilder), ShowHiddenArguments); // note AutomationTaskBuilder is also inherit from CommandLineTask
            yield return new DocBulletList(argsProps.Select(ap => new DocContainer(
                        new DocBlock[] { new DocBold(ap.Name + ": " + Environment.NewLine) }
                        .Concat(DocumentedPropertyArgumentToDoc(ap)))));
        }

        protected static object AggregateFacetValue<TFacetContainer>(PropertyInfo p, IEnumerable<TFacetContainer> descAtts)
            where TFacetContainer : IHasFacetValues
        {
            return AggregateFacetValue(p.Name, p.PropertyType, descAtts);
        }
        protected static object AggregateFacetValue<TFacetContainer>(string propertyName, Type propName, IEnumerable<TFacetContainer> descAtts)
            where TFacetContainer : IHasFacetValues
        {
            object v;
            if (propName == typeof(string))
            {
                string vv = null;
                foreach (var descAtt in descAtts)
                {
                    var hasContent = vv?.Length > 0 && vv.Last().In('.', '!', '?');
                    if (!descAtt.Values.TryGetValue(propertyName, out object newContent))
                        newContent = null;
                    var hasNewContent = ((string)newContent)?.Trim().Length > 0;
                    if (hasNewContent)
                    {
                        vv = vv ?? "";
                        if (hasContent)
                            vv += Environment.NewLine;
                        vv += ((string)newContent).Trim(); // for next iteration
                    }
                }
                v = vv;
            }
            else
            {
                v = descAtts.Reverse()
                            .FirstOrDefault(descAtt => descAtt.Values.TryGetValue(propertyName, out _))
                            ?.Values.TryGetValueClass(propertyName);
            }
            return v;
        }

    }
}
