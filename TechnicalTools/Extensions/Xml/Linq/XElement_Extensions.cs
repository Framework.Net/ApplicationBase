﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;


namespace TechnicalTools
{
    public static class XElement_Extensions
    {
        #region Standard method "overload" for ignoring namespace 
        // kind of "overload"... because names are different,
        // this is because:
        // - using string instead XName does not make compiler happy, it still select the XName version and use implicit operator
        // - Even if it would works, in case user forgot to include extension namespace it still compile which make bug/ambiguity hard to find
        public static IEnumerable<XElement> ElementsNamed(this XContainer root, string localName)
        {
            return root.Elements().Where(node => node.Name.LocalName == localName);
        }
        public static XElement ElementNamed(this XContainer root, string localName)
        {
            return root.Elements().FirstOrDefault(node => node.Name.LocalName == localName);
        }

        public static IEnumerable<XElement> DescendantsNamed(this XContainer root, string localName)
        {
            return root.Descendants().Where(node => node.Name.LocalName == localName);
        }
        public static IEnumerable<XElement> AncestorsNamed(this XContainer root, string localName)
        {
            return root.Ancestors().Where(node => node.Name.LocalName == localName);
        }
        public static IEnumerable<XElement> ElementsNamedAfterSelf(this XContainer root, string localName)
        {
            return root.ElementsAfterSelf().Where(node => node.Name.LocalName == localName);
        }
        public static IEnumerable<XElement> ElementsNamedBeforeSelf(this XContainer root, string localName)
        {
            return root.ElementsBeforeSelf().Where(node => node.Name.LocalName == localName);
        }

        public static IEnumerable<XElement> DescendantsNamedAndSelf(this XElement root, string localName)
        {
            yield return root;
            foreach (var node in root.DescendantsNamed(localName))
                yield return node;
        }
        public static IEnumerable<XElement> AncestorsNamedAndSelf(this XElement root, string localName)
        {

            yield return root;
            foreach (var node in root.AncestorsNamed(localName))
                yield return node;
        }

        #endregion Standard method "overload" for ignoring namespace 

        public static XDocument WithNormalizedNamespace(this XDocument doc)
        {
            WithNormalizedNamespace(doc.Root);
            return doc;
        }
        public static XElement WithNormalizedNamespace(this XElement element, XNamespace ns = null)
        {
            ns = ns ?? element?.Name.Namespace;
            foreach (var node in element.DescendantsAndSelf())
                if (node.Name.Namespace != ns)
                    node.Name = ns + node.Name.LocalName;
            return element;
        }

        

        public static string GetPath(this XElement elt)
        {
            return elt.AncestorsAndSelf().Reverse().Select(n => n.Name.LocalName).Join("/");
        }

        public static string GetAggregatedText(this XElement node, string separator = " ")
        {
            return string.Join(separator, node.Nodes()
                                              .OfType<XText>()
                                              .Select(txt => txt.Value)
                                              .Where(str => !string.IsNullOrWhiteSpace(str)));
        }

        public static string PathOf(this XElement node, XElement root = null)
        {
            if (node == null)
                return null;
            if (node == (root ?? node.Document.Root))
                return (root ?? node.Document.Root).Name.LocalName;
            var path = PathOf(node.Parent, root);
            return path + (path == null ? "" : "/") + node.Name.LocalName;
        }

        /// <summary>
        /// Get the index of the given XElement relative to its
        /// siblings with identical names. If the given element has no parent (-1 is returned).
        /// </summary>
        /// <param name="element">
        /// The element to get the index of.
        /// </param>
        public static int IndexForThisName(this XElement element)
        {
            if (element == null)
                throw new ArgumentNullException("element");

            if (element.Parent == null)
                return -1;

            int i = 0;
            foreach (var sibling in element.Parent.Elements(element.Name))
            {
                if (sibling == element)
                    return i;
                i++;
            }

            throw new InvalidOperationException("element has been removed from its parent.");
        }

    }
}
