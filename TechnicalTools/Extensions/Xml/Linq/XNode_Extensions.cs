﻿using System;
using System.Xml.Linq;


namespace TechnicalTools
{
    public static class XNode_Extensions
    {
        public static XElement NextElement(this XNode node)
        {
            do
                node = node.NextNode;
            while (node != null && !(node is XElement));
            return (XElement)node;
        }
    }
}
