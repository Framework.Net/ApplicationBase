﻿using System;
using System.IO;
using System.Text;
using System.Xml.Linq;


namespace TechnicalTools
{
    public static class XDocument_Extensions
    {
        public static string ToStringWithProlog(this XDocument doc)
        {
            StringBuilder builder = new StringBuilder();
            using (TextWriter writer = new StringWriter(builder))
                doc.Save(writer);
            return builder.ToString();
        }
    }
}
