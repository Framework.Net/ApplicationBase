﻿using System;
using System.Text;


namespace TechnicalTools
{
    public static class StringBuilder_Extensions
    {
        public static void TrimEnd(this StringBuilder sb, bool trimNewLineToo = false)
        {
            if (sb.Length == 0)
                return;
            int i = sb.Length;
            while (--i >= 0)
                if (!char.IsWhiteSpace(sb[i]) ||
                    !trimNewLineToo && (sb[i] == '\n' || sb[i] == '\r'))
                    break;
            ++i;
            if (i < sb.Length)
                sb.Length = i;

            return;
        }
    }
}
