﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;


namespace TechnicalTools
{
    public static class Type_Extensions
    {
        public static bool IsIntegerType(this Type type)
        {
            return IntegerTypes.Contains(type);
        }
        public static bool IsIntegerTypeOrNullable(this Type type)
        {
            return IntegerTypesWithNullable.Contains(type);
        }
        public static readonly HashSet<Type> IntegerTypes = new HashSet<Type>
        {
            typeof(byte), typeof(sbyte),
            typeof(ushort), typeof(short),
            typeof(ulong), typeof(long),
            typeof(uint), typeof(int)
        };
        public static bool IsNumericType(this Type type)
        {
            return NumericTypes.Contains(type);
        }
        public static bool IsNumericTypeOrNullable(this Type type)
        {
            return NumericTypesWithNullable.Contains(type);
        }
        static readonly HashSet<Type> NumericTypes = new HashSet<Type>(IntegerTypes)
        {
            typeof(float),
            typeof(double),
            typeof(decimal)
        };

        public static readonly HashSet<Type> IntegerTypesWithNullable = new HashSet<Type>(IntegerTypes)
        {
            typeof(byte?), typeof(sbyte?),
            typeof(ushort?), typeof(short?),
            typeof(ulong?), typeof(long?),
            typeof(uint?), typeof(int?)
        };
        public static readonly HashSet<Type> NumericTypesWithNullable = new HashSet<Type>(NumericTypes.Concat(IntegerTypesWithNullable))
        {
            typeof(float?),
            typeof(double?),
            typeof(decimal?)
        };

        public static bool IsImplementingAnyOf(this Type type, params Type[] types)
        {
            return types.Any(t => t.IsAssignableFrom(type));
        }

        public static ConstructorInfo GetDefaultConstructor(this Type type, bool lookForPrivate = false)
        {
            Debug.Assert(type != null);

            var constructor = type.GetConstructor(Type.EmptyTypes);
            if (constructor == null)
            {
                constructor = type.GetConstructors(BindingFlags.Instance | (lookForPrivate ? BindingFlags.NonPublic : BindingFlags.Default) | BindingFlags.Public)
                                  .Where(ctor => ctor.GetParameters().All(p => p.IsOptional))
                                  // we prefer public constructor
                                  .OrderByDescending(ctor => ctor.IsPublic)
                                  // and constructor with the least parameters possible.
                                  // Because it is possible to have one constructor with zero parameters and another with only optional parameter(s)
                                  .ThenBy(ctor => ctor.GetParameters().Length)
                                  .FirstOrDefault();
            }
            return constructor;
        }

        public static PropertyInfo[] GetPublicFlattenizedProperties(this Type type)
        {
            if (!type.IsInterface)
                return type.GetProperties(BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Instance);
            // BindingFlags.FlattenHierarchy does not work with interface

            var tri = Algorithm.Graph.TopologicalOrderSimple.DoTopologicalSort(type, t => t.GetInterfaces());
            var result = tri.SelectMany(t => t.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                            .GroupBy(p => p.Name)
                            .Select(grp => grp.First()) // First() take the closest property from "type"
                            .ToArray();
            return result;
        }

        public static bool HasDefaultConstructor(this Type type, bool lookForPrivate = false)
        {
            return type.GetDefaultConstructor(lookForPrivate) != null;
        }

        /// <summary> Retrieves the default value for a given Type </summary>
        public static object GetDefaultValue(this Type type)
        {
            Debug.Assert(type != null && type != typeof(void));

            if (!type.IsValueType)
                return null;

            return Activator.CreateInstance(type);
        }

        public static bool IsMicrosoftType(this Type type)
        {
            return type.Assembly.IsMicrosoftAssembly();
        }

        public static bool IsDevExpressType(this Type type)
        {
            return type.Assembly.IsDevExpressAssembly();
        }

        public static bool IsDefinedInCurrentAssembly(this Type type)
        {
            return type.Assembly == Assembly.GetCallingAssembly();
        }


        public static bool IsCompilerGenerated(this Type t)
        {
            if (t == null)
                return false;

            return t.IsDefined(typeof(CompilerGeneratedAttribute), false)
                || IsCompilerGenerated(t.DeclaringType);
        }

        public static TAttribute GetAttribute<TAttribute>(this Type type)
            where TAttribute : Attribute
        {
            return type.GetCustomAttributes(typeof(TAttribute), true).SingleOrDefault() as TAttribute;
        }

        public static bool IsDelegateType(this Type type)
        {
            return type.IsSubclassOf(typeof(MulticastDelegate));
        }

        public static bool IsStaticClass(this Type type)
        {
            return type.IsClass && type.IsAbstract && type.IsSealed;
        }
        public static bool IsVBModule(this Type type)
        {
            // Nom écrit en dur afin d'éviter d'avoir une reference à Microsoft.VisualBasic.CompilerServices
            return type.CustomAttributes.Any(att => att.AttributeType.Name == "StandardModuleAttribute");
        }

        public static bool IsLeafType(this Type type, bool using_cache = true)
        {
            if (IsTypeLeaf.TryGetValue(type, out bool answer))
                return answer;
            answer = type == typeof (void) || !type.IsStaticClass() && !type.IsGenericType && !type.IsInterface && type.GetAllInheritingTypes().IsEmpty();
            IsTypeLeaf.Add(type, answer);
            return answer;
        }
        static readonly Dictionary<Type, bool> IsTypeLeaf = new Dictionary<Type, bool>();
        public static void BuildIsLeafDB(IEnumerable<Type> types)
        {
            foreach (Type type in types)
                IsLeafType(type);
        }


        public static bool IsAttributeType(this Type type)
        {
            return type.IsSubclassOf(typeof(Attribute));
        }
        public static bool IsExceptionType(this Type type)
        {
            return typeof(Exception).IsAssignableFrom(type);
        }
        public static bool Implements<TInterface>(this Type type)
        {
            return type.Implements(typeof (TInterface));
        }

        public static bool Implements(this Type type, Type interfaceType)
        {
            return GetOneImplementationOf(type, interfaceType) != null;
        }
        public static Type GetOneImplementationOf(this Type type, Type interfaceType)
        {
            if (interfaceType == null)
                throw new ArgumentNullException(nameof(interfaceType));
            if (interfaceType.IsAssignableFrom(type))
                return interfaceType;
            if (!interfaceType.IsGenericTypeDefinition)
                return null;
            if (type.IsGenericType && interfaceType.IsAssignableFrom(type.GetGenericTypeDefinition()))
                return interfaceType;
            return type.GetInterfaces().FirstOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == interfaceType);
        }
        public static bool IsSubclassOfRawGeneric(this Type toCheck, Type generic)
        {
            while (toCheck != null && toCheck != typeof(object))
            {
                var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur)
                    return true;
                toCheck = toCheck.BaseType;
            }
            return false;
        }

        public static IEnumerable<Type> GetAllInheritingTypes(this Type type, bool localOnly = false)
        {
            if (type.IsInterface)
                return type.GetAllImplementingTypes(localOnly);
            return AppDomain.CurrentDomain.GetAssemblies()
                            .Where(ass => !localOnly || !ass.IsMicrosoftAssembly() && !ass.IsDevExpressAssembly())
                            .SelectMany(assembly => assembly.GetTypes())
                            .Where(t => t.IsSubclassOf(type));
        }

        public static IEnumerable<Type> GetAllImplementingTypes(this Type type, bool localOnly = false)
        {
            Debug.Assert(type.IsInterface);

            return AppDomain.CurrentDomain.GetAssemblies()
                            .Where(ass => !localOnly || !ass.IsMicrosoftAssembly() && !ass.IsDevExpressAssembly())
                            .SelectMany(assembly => assembly.GetTypes())
                            .Where(t => t.Implements(type));
        }

        public static bool IsComVisible(this Type type, bool defaultValue)
        {
            var att = type.GetCustomAttribute<ComVisibleAttribute>(true);
            return att != null
                ? att.Value
                : type.Assembly.IsComVisible(defaultValue);
        }

        /// <summary>
        /// Retourne le membre (ou les membre si il y a des overloads) qui hide(nt) un membre d'une classe parente.
        /// Par hide ou entends bien l'utilisation explicite ou pas des mots clefs "new" (C#) ou "Shadows" (VB.Net).
        /// Autrement dit tous les membre de meme nom qui n'override pas le membre mais qui porte le même nom.
        /// </summary>
        public static List<MemberInfo> GetMembersHiding(this Type type, MemberInfo member, bool caseSensitive)
        {
            return member.FindMembersThatHide(caseSensitive)
                     .Where(mi => mi.DeclaringType == type).ToList();
        }

        public static bool HasMemberThatHide(this Type type, MemberInfo member, bool caseSensitive)
        {
            return GetMembersHiding(type, member, caseSensitive).Count > 0;
        }

        /// <summary>
        /// Exemple : my_type.GetGenericInterface(typeof(IEnumerable&lt;&gt;))
        /// </summary>
        /// <param name="pluralType"></param>
        /// <param name="genericInterfaceType"></param>
        /// <returns></returns>
        public static Type GetGenericInterface(this Type pluralType, Type genericInterfaceType)
        {
            if (pluralType.IsGenericType && pluralType.GetGenericTypeDefinition() == genericInterfaceType)
                return pluralType;
            return pluralType.GetInterfaces().SingleOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == genericInterfaceType);
        }

        // Parce que BindingFlags.FlattenHierarchy ne fonctionne pas :(
        public static List<MemberInfo> GetInterfaceMembers(this Type interfaceType, bool inherits)
        {
            Debug.Assert(interfaceType.IsInterface, "Méthode designée pour ce besoin uniquement !");

            var members = new List<MemberInfo>();

            var interfacesMet = new HashSet<Type>();
            var interfacesToExplore = new Queue<Type>();

            interfacesToExplore.Enqueue(interfaceType);

            while (interfacesToExplore.Count > 0)
            {
                interfaceType = interfacesToExplore.Dequeue();
                members.AddRange(interfaceType.GetMembers());
                if (inherits)
                    foreach (var subInterface in interfaceType.GetInterfaces())
                        if (!interfacesMet.Contains(subInterface))
                        {
                            interfacesMet.Add(subInterface);
                            interfacesToExplore.Enqueue(subInterface);
                        }
            }
            return members;
        }
        // Parce que BindingFlags.FlattenHierarchy ne fonctionne pas :(
        public static HashSet<Type> GetAllInheritedInterfaces(this Type iType)
        {
            var considered = new HashSet<Type>();
            Queue<Type> queue = new Queue<Type>();
            queue.Enqueue(iType);
            while (queue.Count > 0)
            {
                Type type = queue.Dequeue();
                foreach (Type tmp in type.GetInterfaces())
                {
                    if (!considered.Contains(tmp))
                    {
                        considered.Add(tmp);
                        queue.Enqueue(tmp);
                    }
                }
            }
            return considered;
        }

        public static int GetLevelOfHierarchy(this Type type)
        {
            int level = 0;
            while (type != typeof (void) && type != typeof (object))
            {
                ++level;
                type = type.BaseType;
                if (type == null)
                    return level - 1;
            }
            return level;
        }

        /// <summary>
        /// Determine le nombre de level de sous type dans un type.
        /// Exemples :
        /// - GetGenericityNestingLevel(typeof(int)) => 0
        /// - GetGenericityNestingLevel(typeof(List(of int))) => 1
        /// - GetGenericityNestingLevel(typeof(List(of List(of int)))) => 2
        /// - GetGenericityNestingLevel(typeof(Dictionary(of int, List(of int)))) => 2
        /// </summary>
        public static int GetGenericityNestingLevel(this Type type)
        {
            if (!type.IsGenericType)
                return 0;
            return 1 + type.GetGenericArguments().Select(GetGenericityNestingLevel).Max();
        }


        public static bool IsRelatedToGenericity(this Type type)
        {
            return type.IsGenericTypeDefinition || type.IsGenericParameter || type.ContainsGenericParameters || type == typeof(Type);
        }

        /// <summary> Retourne le type sous jacent d'un type nullable, ou null si le type n'est pas nullable </summary>
        public static Type TryGetNullableType(this Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                return type.GetGenericArguments()[0];
            return null;
        }
        public static Type RemoveNullability(this Type type)
        {
            return TryGetNullableType(type) ?? type;
        }
        public static bool IsNullable(this Type type)
        {
            return TryGetNullableType(type) != null;
        }

        public static string ToSmartString(this Type type, bool fullName = false)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                return ToSmartString(type.GenericTypeArguments[0], fullName) + "?";
            if (type.IsGenericParameter)
                return type.Name;
            if (_typeToCsNames.TryGetValue(type, out string res))
                return res;
            res = fullName ? type.FullName.Replace("+", ".")
                : type.IsNested ? ToSmartString(type.DeclaringType, fullName) + "." + type.Name
                : type.Name;
            int index = res.IndexOf('`');
            if (index >= 0)
                res = res.Remove(index);
            if (type.IsGenericType)
            {
                res += "<";
                res += type.GetGenericArguments().Select(t => ToSmartString(t, fullName)).Join();
                res += ">";
            }
            return res;
        }
        static readonly Dictionary<Type, string> _typeToCsNames = new Dictionary<Type, string>()
        {
            { typeof(bool), "bool" },
            { typeof(byte), "byte" },
            { typeof(sbyte), "sbyte" },
            { typeof(short), "short" },
            { typeof(ushort), "ushort" },
            { typeof(int), "int" },
            { typeof(uint), "uint" },
            { typeof(long), "long" },
            { typeof(ulong), "ulong" },
            { typeof(decimal), "decimal" },
            { typeof(float), "float" },
            { typeof(double), "double" },
            { typeof(char), "char" },
            { typeof(DateTime), "DateTime" },
            { typeof(TimeSpan), "TimeSpan" },
        }.SelectMany(kvp => new[] { kvp, new KeyValuePair<Type, string>(typeof(Nullable<>).MakeGenericType(kvp.Key), kvp.Value + "?")})
        .Concat(new Dictionary<Type, string>()
        {
            { typeof(void), "void" },
            { typeof(string), "string" },
            { typeof(object), "object" }
        })
        .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

        #region Serialization
        public static string ToAssemblyQualifiedStringWithoutVersion(this Type t)
        {
            var args = t.GetGenericArguments();
            return (args.Length > 0 ? t.FullName.Remove(t.FullName.IndexOf('`')) : t.FullName)
                 + (args.Length == 0 ? "" : "`" + args.Length + "[[" + args.Select(ToAssemblyQualifiedStringWithoutVersion).Join("],[") + "]]")
                 + ", " + t.Assembly.FullName.Remove(t.Assembly.FullName.IndexOf(','));
        }
        public static Type ToTypeFromAssemblyQualifiedStringWithoutVersion(this string str)
        {
            var lst = new List<Type>();
            int i = 0;
            ParseTypeListRec(str, lst, ref i);
            return lst.Single();
        }
        static void ParseTypeListRec(string str, List<Type> types, ref int i)
        {
            int start = i;
            string firstPart = null;
            List<Type> subTypes = null;
            int comma = int.MaxValue;
            while (true)
            {
                if (i == str.Length || str[i] == ']')
                {
                    if (firstPart == null) // means '`' has not been met
                        types.Add(Type.GetType(str.Substring(start, i - start)));
                    else // means '`' has been met, subTypes is filled
                    {
                        var assemblyName = str.Substring(comma, i - comma);
                        var genericType = Type.GetType(firstPart + assemblyName);
                        types.Add(genericType.MakeGenericType(subTypes.ToArray()));
                        firstPart = null;
                        subTypes = null;
                    }
                    if (i == str.Length)
                        return;
                    if (str[i + 1] != ',')
                        return;
                    i += 2;
                    Debug.Assert(str[i + 2] == '[');
                    start = i;
                }
                else if (str[i] == ',')
                {
                    comma = i;
                    ++i;
                }
                else if (str[i] == '`')
                {
                    i = str.IndexOf("[[", i);
                    firstPart = str.Substring(start, i - start);
                    i += 2;
                    subTypes = new List<Type>();
                    ParseTypeListRec(str, subTypes, ref i);
                    i += 2;
                }
                else
                    ++i;
            }
        }
        #endregion Serialization
    }
}
