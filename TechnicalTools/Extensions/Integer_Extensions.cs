﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnicalTools
{
    public static class Integer_Extensions
    {
        public static ushort ToGrayCode(this ushort num)
        {
            return (ushort)((num >> 1) ^ num);
        }

        public static ushort FromGrayCode(this ushort num)
        {
            uint temp = (uint)(num ^ (num >> 8));
            temp ^= (temp >> 4);
            temp ^= (temp >> 2);
            temp ^= (temp >> 1);
            return (ushort)temp;
        }
        public static uint ToGrayCode(this uint num)
        {
            return (num >> 1) ^ num;
        }

        public static uint FromGrayCode(this uint num)
        {
            uint temp = num ^ (num >> 16);
            temp ^= temp >> 8;
            temp ^= temp >> 4;
            temp ^= temp >> 2;
            temp ^= temp >> 1;
            return temp;
        }

        public static ulong ToGrayCode(this ulong num)
        {
            return (uint)((num >> 1) ^ num);
        }

        public static ulong FromGrayCode(this ulong num)
        {
            ulong temp = num ^ (num >> 32);
            temp ^= temp >> 16;
            temp ^= temp >> 8;
            temp ^= temp >> 4;
            temp ^= temp >> 2;
            temp ^= temp >> 1;
            return temp;
        }

        /// <summary>
        /// Convertit une taille en octet en une chaine exprimant cette taille dans un format plus facilement lisible
        /// et dont l'unité (B, KiB, Gib etc) est adaptée
        /// </summary>
        public static string ToReadableByteSize(this long size)
        {
            const long k = 1024;
            const long m = k * 1024;
            const long g = m * 1024;
            const long t = g * 1024;
            const long p = t * 1024;
            const long e = p * 1024;
            if (size < k) return size + "B";
            if (size < m) return global::System.Math.Round((float)size / k, 2) + "KiB";
            if (size < g) return global::System.Math.Round((float)size / m, 2) + "MiB";
            if (size < t) return global::System.Math.Round((float)size / g, 2) + "GiB";
            if (size < p) return global::System.Math.Round((float)size / t, 2) + "TiB";
            if (size < e) return global::System.Math.Round((float)size / p, 2) + "PiB";
            return global::System.Math.Round((float)size / e, 2) + "EiB";
        }

        public static string ToRoman(this uint n)
        {
            // Code inspired from https://stackoverflow.com/a/4986630
            var sb = new StringBuilder();
            string[] huns = new[] { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
            string[] tens = new[] { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
            string[] ones = new[] { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };

            //  Add 'M' until we drop below 1000.
            while (n >= 1000)
            {
                sb.Append('M');
                n -= 1000;
            }

            // Add each of the correct elements, adjusting as we go.
            sb.Append(huns[n / 100]); n = n % 100;
            sb.Append(tens[n / 10]); n = n % 10;
            sb.Append(ones[n]);
            return sb.ToString();
        }
    }
}
