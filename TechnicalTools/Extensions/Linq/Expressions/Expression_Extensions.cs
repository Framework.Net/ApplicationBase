﻿using System;
using System.Linq.Expressions;

using TechnicalTools.MetaProgramming.ExpressionVisitors;


namespace TechnicalTools
{
    public static class Expression_Extensions
    {
        /// <summary>
        /// Convert expression (conditional expression only) to graphviz representation (optimized for dot engine)
        /// <see cref="ConditionalExpressionToGraphvizVisitor"/>
        /// </summary>
        /// <param name="expr"></param>
        /// <returns></returns>
        public static string ConditionalExpressionToGraphviz(this Expression expr)
        {
            var v = new ConditionalExpressionToGraphvizVisitor();
            v.Visit(expr);
            return v.Result;
        }
    }
}
