﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;


namespace TechnicalTools
{
    public static class MemberInfo_Extensions
    {
        public static string GetMethodOrPropertyName(this MemberInfo mi)
        {
            if (mi.Name.StartsWith("get_") || mi.Name.StartsWith("set_"))
                return mi.Name.Substring(4);
            return mi.Name;
        }

        public static bool IsComVisible(this MemberInfo mi, bool defaultValue)
        {
            var att = mi.GetCustomAttribute<ComVisibleAttribute>(true);
            return att != null
                ? att.Value
                : mi.DeclaringType.IsComVisible(defaultValue);
        }


        // TODO : A factoriser avec le code de la fenêtre FormDebugStaticReport
        public static List<MemberInfo> FindMembersThatHide(this MemberInfo mi, bool caseSensitive)
        {
            var derived_types = mi.DeclaringType.GetAllInheritingTypes().ToList();

            Func<string, string> fixStr = ((str) => caseSensitive ? str : str.ToLowerInvariant());

            var results = new List<MemberInfo>();

            foreach (Type type in derived_types)
            {
                Debug.Assert(type.BaseType != null);
                var members = type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).ToList();
                //var baseMembers = type.BaseType.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).ToList();
                // Using info from http://stackoverflow.com/questions/288357/how-does-reflection-tell-me-when-a-property-is-hiding-an-inherited-member-with-t

                foreach (var member in members)
                    if (member.DeclaringType == type &&
                        fixStr(member.GetMethodOrPropertyName()) == fixStr(mi.GetMethodOrPropertyName()))
                    {
                        bool found = false;
                        if (member.Attributes.HasFlag(MethodAttributes.Virtual) &&
                            !member.Attributes.HasFlag(MethodAttributes.NewSlot))
                        {
                            // Member override base method  => OK
                        }
                        else if (member.IsHideBySig)
                        {
                            var flags = BindingFlags.Instance | (member.IsPublic ? BindingFlags.Public : BindingFlags.NonPublic);
                            var paramTypes = member.GetParameters().Select(p => p.ParameterType).ToArray();
                            if (type.BaseType.GetMethod(member.Name, flags, null, paramTypes, null) != null)
                            {
                                // member shadows / hides base member by signature
                                found = true;
                            }
                        }
                        else
                        {
                            // member shadows / hides base member by name
                            found = true;
                        }
                        if (found)
                        {
                            if (mi is PropertyInfo)
                            {
                                Debug.Assert(member is MethodInfo, "Cas a etudier.. Si member est un PropertyInfo c'est OK (mais je ne vois pas comment ca pourrait arriver !");
                                Debug.Assert(member.Name.StartsWith("get_") || member.Name.StartsWith("set_"), "Heu ca a ete compilé avec quel compilateur ?!");
                                var props = member.DeclaringType.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
                                found = false;
                                foreach(var prop in props)
                                    if (prop.Name == member.GetMethodOrPropertyName() &&
                                        (prop.GetGetMethod() == member || prop.GetSetMethod() == member))
                                    {
                                        results.Add(prop);
                                        found = true;
                                        break;
                                    }
                                Debug.Assert(found, "Comment ca on a pas trouvé !!");
                            }
                            else
                                results.Add(member);
                        }
                    }
            }

            return results;
        }



        public static string CheckAndCorrectCollidingMembers<T>(this IEnumerable<T> members)
            where T : MemberInfo
        {
            var lst = members.ToList();
            return CheckAndCorrectCollidingMembers(lst);
        }

        public static bool IsOverride(this MethodInfo mi)
        {
            return mi.Attributes.HasFlag(MethodAttributes.Virtual) &&
                   !mi.Attributes.HasFlag(MethodAttributes.NewSlot);
        }


        public static bool IsOverride(this MemberInfo mi)
        {
            return mi is PropertyInfo && (mi as PropertyInfo).IsOverride() ||
                   mi is MethodInfo && (mi as MethodInfo).IsOverride();
        }

        /// <summary>
        /// Prend une liste de membre et verifie s'il y a des collisions de noms (ie : les membre ont le meme nom et ne s'override pas les uns les autres
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="members"></param>
        /// <param name="correct_by_taking_more_specialized"></param>
        /// <returns>une chaine indiquant les probleme (si il y en a)</returns>
        public static string CheckAndCorrectCollidingMembers<T>(this List<T> members, bool correct_by_taking_more_specialized = false)
            where T : MemberInfo
        {
            var result = new List<IGrouping<string, MemberInfo>>();
            // Pour chaque groupe de membre qui ont le même nom...
            foreach (var duplicate_grp in members.FindDuplicates(mi => mi.Name.ToLowerInvariant())) // ToLowerInvariant car VB est insensible à la casse
            {
                // On regarde si l'un dentre eux cache un entre d'entre eux
                // TODO : Ca marche pas pour Item :( a cause de FindMembersThatHide qui considere que get_Item (la méthode override la propriété)
                var hidders = duplicate_grp.SelectMany(dup => dup.FindMembersThatHide(false));
                if (hidders.Intersect(duplicate_grp).Any())
                    result.Add(duplicate_grp);
            }

            if (result.Count == 0)
                return null;

            string badInheriting = result.Select(grp => "For name \"" + grp.Key + "\" : " +
                                                        grp.Select(member => Environment.NewLine + " - " + member.DeclaringType.Name)
                                                            .Join())
                                            .Join(Environment.NewLine + Environment.NewLine);

            //Debug.Fail(badInheriting); // Commenté car y'en a trop !

            if (correct_by_taking_more_specialized)
                foreach (var grp in result)
                    foreach (var member in grp)
                        if (member.DeclaringType != grp.First().DeclaringType)// Le premier membre est toujours le plus spécialisé
                            members.Remove((T)member);

            return badInheriting;
        }
    }
}
