﻿using System;
using System.Reflection;


namespace TechnicalTools
{
    public static class FieldInfo_Extensions
    {
        public static bool IsBackingFieldOfAutoProperty(this FieldInfo field)
        {
            // Le Format d'une champs provenant d'une auto property est :
            // "<PropertyName>_BackingField"
            return field.Name.Contains("<") && field.Name.Contains(">") && field.Name.Contains("Backing");
        }
    }
}
