﻿using System;
using System.Diagnostics;

using TechnicalTools.Model;


namespace TechnicalTools
{
    public static class Action_Extensions
    {
        public static void DoNothing()                             { /* Nothing */ }
        public static void DoNothing<T>(T value)                   { /* Nothing */ }
        public static void DoNothing<T1, T2>(T1 value1, T2 value2) { /* Nothing */ }
        /* Put as many overloads as you want */

        /// <summary>
        /// Transform a Task without a returning type to a task that return something dummy.
        /// This help reducing the override to write in generic classes
        /// </summary>
        public static Func<Nothing> AsFunc(this Action action)
        {
            return () =>
            {
                action();
                return Nothing.Value;
            };
        }
        /// <summary>
        /// Transform a Task without a returning type to a task that return something dummy.
        /// This help reducing the override to write in generic classes
        /// </summary>
        public static Func<T, Nothing> AsFunc<T>(this Action<T> action)
        {
            return arg =>
            {
                action(arg);
                return Nothing.Value;
            };
        }
        /// <summary>
        /// Helper method that avoid writing lambda in method (so Edit and Continue work great) *
        /// <remark>
        /// Currently for class methods the syntax is not the extension syntax: 
        /// <code>
        /// var curried = Action_Extensions.Curry&lt;T1, ...&gt;(a_class_method, ...);
        /// </code>
        /// But it will become nicer with C# 10.0
        /// </remark>
        /// </summary>
        // or to replace with method PartialApply defined in https://github.com/leandromoh/Curryfy
        public static Action<T2> Curry<T1, T2>(this Action<T1, T2> actionToCurryfy, T1 arg)
        {
            [DebuggerHidden]
            [DebuggerStepThrough]
            void curried(T2 t2)
            {
                actionToCurryfy(arg, t2);
            }
            return curried;
        }

        /// <summary><inheritdoc cref="Curry{T1, T2}(Action{T1, T2}, T1)" /></summary>
        public static Action<T3> Curry<T1, T2, T3>(this Action<T1, T2, T3> actionToCurryfy, T1 arg, T2 arg2)
        {
            [DebuggerHidden]
            [DebuggerStepThrough]
            void curried(T3 arg3)
            {
                actionToCurryfy(arg, arg2, arg3);
            }
            return curried;
        }

    }
}
