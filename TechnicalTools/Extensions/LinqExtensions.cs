﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TechnicalTools.Model.Cache;

namespace TechnicalTools
{
    public static class LinqExtensions
    {
        [DebuggerDisplay("{AsDebugString,nq}")]
        public class MatchResult<TItem>
        {
            public TItem Left  { get; set; }
            public TItem Right { get; set; }
        }
        [DebuggerDisplay("{AsDebugString,nq}")]
        public class MatchResultDbg<TItem, TKey> : MatchResult<TItem>
        {
            internal static Func<TItem, TKey> LastKeySelectorUsed { get; set; }
            private string AsDebugString
            {
                get
                {
                    return "Left: " + (Left == null ? "<NULL>" : (LastKeySelectorUsed(Left) as ITypedId)?.AsDebugString ?? LastKeySelectorUsed(Left).ToString()) +
                           ", " +
                           "Right: " + (Right == null ? "<NULL>" : (LastKeySelectorUsed(Right) as ITypedId)?.AsDebugString ?? LastKeySelectorUsed(Right).ToString());
                }
            }
        }

        public static IEnumerable<MatchResult<TItem>> MatchById<TItem, TKey>(
            this IEnumerable<TItem> left,
            IEnumerable<TItem> right,
            Func<TItem, TKey> keySelector)
        {
            MatchResultDbg<TItem, TKey>.LastKeySelectorUsed = keySelector;
            return left.FullOuterJoin(right, keySelector, keySelector,
                                     (l, r, key) => new MatchResultDbg<TItem, TKey>() { Left = l, Right = r });
        }


        public static IEnumerable<TResult> FullOuterJoin<TLeft, TRight, TKey, TResult>(
            this IEnumerable<TLeft> left,
            IEnumerable<TRight> right,
            Func<TLeft, TKey> leftKeySelector,
            Func<TRight, TKey> rightKeySelector,
            Func<TLeft, TRight, TKey, TResult> resultSelector,
            IEqualityComparer<TKey> comparator = null,
            TLeft defaultLeft = default(TLeft),
            TRight defaultRight = default(TRight))
        {
            if (left == null) throw new ArgumentNullException(nameof(left));
            if (right == null) throw new ArgumentNullException(nameof(right));
            if (leftKeySelector == null) throw new ArgumentNullException(nameof(leftKeySelector));
            if (rightKeySelector == null) throw new ArgumentNullException(nameof(rightKeySelector));
            if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

            comparator = comparator ?? EqualityComparer<TKey>.Default;
            return FullOuterJoinIterator(left, right, leftKeySelector, rightKeySelector, resultSelector, comparator, defaultLeft, defaultRight);
        }

        internal static IEnumerable<TResult> FullOuterJoinIterator<TLeft, TRight, TKey, TResult>(
            this IEnumerable<TLeft> left,
            IEnumerable<TRight> right,
            Func<TLeft, TKey> leftKeySelector,
            Func<TRight, TKey> rightKeySelector,
            Func<TLeft, TRight, TKey, TResult> resultSelector,
            IEqualityComparer<TKey> comparator,
            TLeft defaultLeft,
            TRight defaultRight)
        {
            var leftLookup = left.ToLookup(leftKeySelector, comparator);
            var rightLookup = right.ToLookup(rightKeySelector, comparator);
            var keys = leftLookup.Select(g => g.Key).Union(rightLookup.Select(g => g.Key), comparator);

            foreach (var key in keys)
                foreach (var leftValue in leftLookup[key].DefaultIfEmpty(defaultLeft))
                    foreach (var rightValue in rightLookup[key].DefaultIfEmpty(defaultRight))
                        yield return resultSelector(leftValue, rightValue, key);
        }
    }
}
