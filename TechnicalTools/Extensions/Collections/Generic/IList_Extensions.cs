﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace TechnicalTools
{
    public static partial class IList_Extensions
    {
        /// <summary>
        /// Permet de proteger toutes les itérations d'une boucle en faisant
        /// </summary>
        /// <typeparam name="T">Type d'objet a parcourir</typeparam>
        /// <param name="lst">Liste contenant les objet à parcourir</param>
        /// <param name="action">Action to do on each object.</param>
        public static List<ForEachSafelyResult<T, Exception>> ForEachIgnoreException<T>(this IList<T> lst, Action<T> action)
        {
            var errors = new List<ForEachSafelyResult<T, Exception>>();
            int i = 0;
            do
            {
                try
                {
                    while (i < lst.Count)
                    {
                        action(lst[i]);
                        ++i;
                    }
                }
                catch (Exception ex)
                {
                    errors.Add(new ForEachSafelyResult<T, Exception>() { Source = lst[i], Result = ex, Exception = ex });
                    ++i;
                }
            } while (i < lst.Count);
            return errors;
        }
        public static List<ForEachSafelyResult<T, TResult>> ForEachIgnoreException<T, TResult>(this IList<T> lst, Func<T, TResult> fun)
        {
            var results = new List<ForEachSafelyResult<T, TResult>>();
            int i = 0;
            do
            {
                try
                {
                    while (i < lst.Count)
                    {
                        var res = fun(lst[i]);
                        results.Add(new ForEachSafelyResult<T, TResult>() { Source = lst[i], Result = res });
                        ++i;
                    }
                }
                catch (Exception ex)
                {
                    results.Add(new ForEachSafelyResult<T, TResult>() { Source = lst[i], Exception = ex });
                    ++i;
                }
            } while (i < lst.Count);
            return results;
        }

        public static Task<ForEachSafelyResult<TSource, bool>[]> ForEachParallelSafely<TSource>(this IEnumerable<TSource> elements, Action<TSource> doWork, int maxConcurrent = 0)
        {
            return ForEachParallelSafely(elements, null, elt => { doWork(elt); return true; }, maxConcurrent);
        }
        public static Task<ForEachSafelyResult<TSource, bool>[]> ForEachParallelSafely<TSource>(this IEnumerable<TSource> elements, string parallelActionName, IProgress<string> progress, Action<TSource> doWork, int maxConcurrent = 0)
        {
            var cnt = elements.Count();
            return ForEachParallelSafely(elements,
                                        (started, done, inError) => progress.Report(parallelActionName + " (" + started + " started"
                                                                                                       + " / " + done + " done"
                                                                                                               + (inError == 0 ? "" : " (" + inError + " with error)")
                                                                                                       + " / " + cnt + " total)"),
                                         elt => { doWork(elt); return true; },
                                         maxConcurrent);
        }
        public static Task<ForEachSafelyResult<TSource, TResult>[]> ForEachParallelSafely<TSource, TResult>(this IEnumerable<TSource> elements, Func<TSource, TResult> doWork, int maxConcurrent = 0)
        {
            return ForEachParallelSafely(elements, null, doWork, maxConcurrent);
        }
        public static Task<ForEachSafelyResult<TSource, TResult>[]> ForEachParallelSafely<TSource, TResult>(this IEnumerable<TSource> elements, string parallelActionName, IProgress<string> progress, Func<TSource, TResult> doWork, int maxConcurrent = 0)
        {
            var cnt = elements.Count();
            return ForEachParallelSafely(elements,
                                        (started, done, inError) => progress.Report(parallelActionName + " (" + started + " started"
                                                                                                       + " / " + done + " done"
                                                                                                               + (inError == 0 ? "" : " (" + inError + " with error)")
                                                                                                       + " / " + cnt + " total)"),
                                        doWork, maxConcurrent);
        }
        public static Task<ForEachSafelyResult<TSource, TResult>[]> ForEachParallelSafely<TSource, TResult>(this IEnumerable<TSource> elements, Action<int, int, int> progress, Func<TSource, TResult> doWork, int maxConcurrent = 0)
        {
            int started = 0;
            int done = 0;
            int inError = 0;
            progress?.Invoke(started, done, inError);
            var concurrencySemaphore = new SemaphoreSlim(maxConcurrent <= 0
                                                        ? MaxConcurrentParallelBusinessWorkers ?? DefaultMaxConcurrentParallelBusinessWorkers
                                                        : Math.Min(maxConcurrent, MaxConcurrentParallelBusinessWorkers ?? DefaultMaxConcurrentParallelBusinessWorkers));
            var task = Task.WhenAll(elements.Select(elt => Task.Run(() =>
            {
                concurrencySemaphore.Wait();
                var start = DateTime.UtcNow;
                try
                {
                    Interlocked.Increment(ref started);
                    progress?.Invoke(started, done, inError);
                    return new ForEachSafelyResult<TSource, TResult>() { Source = elt, Result = doWork(elt), Duration = DateTime.UtcNow - start };
                }
                catch (Exception ex)
                {
                    Interlocked.Increment(ref inError);
                    return new ForEachSafelyResult<TSource, TResult>() { Source = elt, Exception = ex, Duration = DateTime.UtcNow - start };
                }
                finally
                {
                    Interlocked.Increment(ref done);
                    progress?.Invoke(started, done, inError);
                    concurrencySemaphore.Release();
                }
            })));

            task.ContinueWith(_ =>
            {
                concurrencySemaphore.Dispose();
            }, TaskScheduler.Default);

            return task;
        }
        public static Task<Tuple<Dictionary<TSource, TResult>, Dictionary<TSource, Exception>>>
            ForEachParallelSafelyAsDictionary<TSource, TResult>(this IEnumerable<TSource> elements, string parallelActionName, IProgress<string> progress, Func<TSource, TResult> doWork, int maxConcurrent = 0)
        {
            return ForEachParallelSafely(elements, parallelActionName, progress, doWork, maxConcurrent)
                .ContinueWith(res =>
                {
                    var results = res.Result.Where(r => r.Exception == null).ToDictionary(r => r.Source, r => r.Result);
                    var errors = res.Result.Where(r => r.Exception != null).ToDictionary(r => r.Source, r => r.Exception);
                    return Tuple.Create(results, errors);
                }, TaskScheduler.Default);
        }
        public class ForEachSafelyResult<TSource, TResult>
        {
            public TSource   Source    { get; internal set; }
            public TResult   Result    { get; internal set; }
            public Exception Exception { get; internal set; }
            public TimeSpan  Duration  { get; internal set; }
        }
        public static int? MaxConcurrentParallelBusinessWorkers { get; set; }
        public static readonly int DefaultMaxConcurrentParallelBusinessWorkers = int.MaxValue;
    }
}
