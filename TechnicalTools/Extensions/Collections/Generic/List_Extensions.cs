﻿using System;
using System.Collections.Generic;


namespace TechnicalTools
{
    public static class List_Extensions
    {
        // Ensures that the capacity of this list is at least the given minimum
        // value. If the currect capacity of the list is less than min, the
        // capacity is increased to twice the current capacity or to min,
        // whichever is larger.
        public static void EnsureCapacity<T>(this List<T> lst, int min)
        {
            // @dev: all this code is from microsoft private source of List<T>
            // This code is useful to get an atomic behavior : ensure all items (or none) will be added to a list.
            // Contrary to property "Capacity", if there is enough storage, no allocation/resize is done.
            const int _defaultCapacity = 4;
            const int Array_MaxArrayLength = 0X7FEFFFFF;
            if (lst.Capacity < min)
            {
                int newCapacity = lst.Capacity == 0 ? _defaultCapacity : lst.Capacity * 2;
                // Allow the list to grow to maximum possible capacity (~2G elements) before encountering overflow.
                // Note that this check works even when _items.Length overflowed thanks to the (uint) cast
                if ((uint)newCapacity > Array_MaxArrayLength) newCapacity = Array_MaxArrayLength;
                if (newCapacity < min) newCapacity = min;
                lst.Capacity = newCapacity; // let this code throw if min is too high
            }
        }
    }

    public static class List_Extensions<T>
    {
        public static List<T> Empty { get; } = new List<T>();
    }
}
