﻿using System.Collections.Generic;
using System.Linq;

using TechnicalTools.Model;

namespace TechnicalTools
{
    public static class ICollection_Extensions
    {
        // Cannot use "AddRange" name because already used in Gda.Tools in namespace System :'S
        public static ICollection<TItem> AddRangeInPlace<TItem, TItemToAdd>(this ICollection<TItem> dst, IEnumerable<TItemToAdd> src)
            where TItemToAdd : TItem
        {
            if (ReferenceEquals(src, dst))
                throw new TechnicalException($"Use \"{nameof(Enumerable.ToList)}()\" on src before calling this method!");
            foreach (var item in src)
                dst.Add(item);
            return dst;
        }

        public static void RemoveRange<T>(this ICollection<T> src, IEnumerable<T> eltsToRemove)
        {
            foreach (var elt in eltsToRemove)
                src.Remove(elt);
        }
    }
}
