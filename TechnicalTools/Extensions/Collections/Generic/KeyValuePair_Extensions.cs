﻿using System;
using System.Collections.Generic;


namespace TechnicalTools
{
    public static class KeyValuePair_Extensions
    {
        /// <summary>
        /// Allow to write foreach ((var key, var value) in dictionary)
        /// </summary>
        public static void Deconstruct<K, V>(this KeyValuePair<K, V> kvp, out K key, out V value)
        {
            key = kvp.Key;
            value = kvp.Value;
        }
    }
}
