﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using TechnicalTools.Model;


namespace TechnicalTools
{
    public static class Dictionary_Extensions
    {
        /// <summary>
        /// This extension method allows to chain call dic.TryGetValueClass(42)?.TryGetValueClass(51)?....
        /// or doing this:
        /// var instance = dic.TryGetValueClass(42)
        ///             ?? dic2.TryGetValueClass(42);
        /// </summary>
        public static TValue TryGetValueClass<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dic, TKey key)
            where TValue : class
        {
            if (dic.TryGetValue(key, out TValue value))
                return value;
            return null;
        }
        /// <summary>
        /// Same than <see cref="TryGetValueClass"/> but for value type (named struct here)
        /// </summary>
        public static TValue? TryGetValueStruct<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dic, TKey key)
            where TValue : struct
        {
            if (dic.TryGetValue(key, out TValue value))
                return value;
            return null;
        }

        public static TValue GetValueOrCreateDefault<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key)
            where TValue : class, new()
        {
            if (dic.TryGetValue(key, out TValue value))
                return value;
            value = new TValue();
            dic.Add(key, value);
            return value;
        }
        public static TValue? GetValueOrCreateDefaultStruct<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key)
            where TValue : struct
        {
            if (dic.TryGetValue(key, out TValue value))
                return value;
            value = new TValue();
            dic.Add(key, value);
            return value;
        }

        // Merge / overwrite / addRange
        public static TDictionary OverwriteWith<TDictionary, TKey, TValue>(this TDictionary dic, IEnumerable<KeyValuePair<TKey, TValue>> pairEnumerator)
            where TDictionary : IDictionary<TKey, TValue>
        {
            foreach (var kvp in pairEnumerator)
                dic[kvp.Key] = kvp.Value;
            return dic;
        }


        // Merge / overwrite / addRange
        public static Dictionary<TKey, TValue> Clone<TKey, TValue>(this IDictionary<TKey, TValue> dic)
        {
            return new Dictionary<TKey, TValue>(dic);
        }

        public static DictionaryAsLookup<TKey, TValue> AsLookUp<TKey, TValue>(this Dictionary<TKey, TValue> dic)
        {
            return new DictionaryAsLookup<TKey, TValue>(dic);
        }

        public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> dico, IDictionary<TKey, TValue> dico2, bool overwrite = false)
        {
            Debug.Assert(dico != null);
            Debug.Assert(dico2 != null);
            Debug.Assert(dico != dico2);

            if (overwrite)
                foreach (var pair in dico2)
                    dico[pair.Key] = pair.Value;
            else
                foreach (var pair in dico2)
                    dico.Add(pair.Key, pair.Value);
        }

        public static TValue AddAndReturn<TKey, TValue>(this IDictionary<TKey, TValue> dico, TKey key, TValue value)
        {
            dico.Add(key, value);
            return value;
        }
        public static TValue GetOrCreate<TKey, TValue>(this IDictionary<TKey, TValue> dico, TKey key, Func<TValue> createValue)
        {
            if (dico.TryGetValue(key, out TValue value))
                return value;
            value = createValue();
            dico.Add(key, value);
            return value;
        }

        public static void RemoveRange<TKey, TValue>(this IDictionary<TKey, TValue> dico, IEnumerable<TKey> keys)
        {
            Debug.Assert(dico != null);
            Debug.Assert(keys != null);

            foreach (TKey key in keys)
                dico.Remove(key);
        }
        public static void RemoveRange<TKey, TValue>(this IDictionary<TKey, TValue> dico, IEnumerable<KeyValuePair<TKey, TValue>> pairs)
        {
            Debug.Assert(dico != null);
            Debug.Assert(pairs != null);

            foreach (KeyValuePair<TKey, TValue> pair in pairs)
                dico.Remove(pair.Key);
        }
        public static void RemoveAll<TKey, TValue>(this IDictionary<TKey, TValue> dico, Func<KeyValuePair<TKey, TValue>, bool> filterPairs)
        {
            Debug.Assert(dico != null);
            Debug.Assert(filterPairs != null);
            dico.RemoveRange(dico.Where(filterPairs).ToList());
        }



        public static IReadOnlyDictionary<TKey, TValue> AsReadOnly<TKey, TValue>(this IDictionary<TKey, TValue> dico)
        {
            return new ReadOnlyDictionary<TKey, TValue>(dico);
        }
    }
}
