﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;


namespace TechnicalTools
{
    public static class Assembly_Extensions
    {
        public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly)
        {
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                return e.Types.Where(t => t != null);
            }
        }

        public static void CleanCache()
        {
            _IsMicrosoftAssembly = new ConditionalWeakTable<Assembly, BoolResult>();
            _IsDevExpressAssembly = new ConditionalWeakTable<Assembly, BoolResult>();
            _IsComVisible = new ConditionalWeakTable<Assembly, BoolResult>();
        }
        class BoolResult
        {
            public bool IsInitialized;
            public bool Value;
            public bool ValueIsDefined;
        }

        public static bool IsMicrosoftAssembly(this Assembly assembly)
        {
            if (_IsMicrosoftAssembly.TryGetValue(assembly, out BoolResult result) && result.IsInitialized)
                return result.Value;
            var res = GetIsMicrosoftAssembly(assembly);
            result = _IsMicrosoftAssembly.GetOrCreateValue(assembly); /// We avoid lambda here...
            result.Value = res;
            result.ValueIsDefined = true;
            result.IsInitialized = true; // ...and make sure concurrent accesses do not see a dirty value
            return res;
        }
        static ConditionalWeakTable<Assembly, BoolResult> _IsMicrosoftAssembly = new ConditionalWeakTable<Assembly, BoolResult>();
        static bool GetIsMicrosoftAssembly(this Assembly assembly)
        {
            return assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false)
                           .Cast<AssemblyCompanyAttribute>()
                           .Any(attr => attr.Company == "Microsoft Corporation");
        }


        public static bool IsDevExpressAssembly(this Assembly assembly)
        {
            if (_IsDevExpressAssembly.TryGetValue(assembly, out BoolResult result) && result.IsInitialized)
                return result.Value;
            var res = GetIsDevExpressAssembly(assembly);
            result = _IsDevExpressAssembly.GetOrCreateValue(assembly); // We avoid lambda here...
            result.Value = res;
            result.ValueIsDefined = true;
            result.IsInitialized = true; // ...and make sure concurrent accesses do not see a dirty value
            return res;
        }
        static ConditionalWeakTable<Assembly, BoolResult> _IsDevExpressAssembly = new ConditionalWeakTable<Assembly, BoolResult>();
        static bool GetIsDevExpressAssembly(this Assembly assembly)
        {
            return assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false)
                           .Cast<AssemblyCompanyAttribute>()
                           .Any(attr => attr.Company == "Developer Express Inc.");
        }

        public static bool IsComVisible(this Assembly assembly, bool defaultValue)
        {
            if (_IsComVisible.TryGetValue(assembly, out BoolResult result) && result.IsInitialized)
                return result.ValueIsDefined ? result.Value : defaultValue;
            var res = GetIsComVisible(assembly);
            result = _IsComVisible.GetOrCreateValue(assembly);
            result = _IsDevExpressAssembly.GetOrCreateValue(assembly); // We avoid lambda here...
            result.Value = res ?? defaultValue;
            result.ValueIsDefined = res.HasValue;
            result.IsInitialized = true; // ...and make sure concurrent accesses do not see a dirty value
            return result.Value;
        }
        static ConditionalWeakTable<Assembly, BoolResult> _IsComVisible = new ConditionalWeakTable<Assembly, BoolResult>();
        static bool? GetIsComVisible(this Assembly assembly)
        {
            var att = assembly.GetCustomAttribute<ComVisibleAttribute>();
            return att == null
                ? (bool?)null
                : att.Value;
        }
    }
}
