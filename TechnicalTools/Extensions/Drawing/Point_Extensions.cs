﻿using System;
using System.Drawing;


namespace TechnicalTools
{
    public static class Point_Extensions
    {
        public static Point Add      (this Point a, Point b)  { return new Point(a.X + b.X, a.Y + b.Y); }
        public static Point Substract(this Point a, Point b)  { return new Point(a.X - b.X, a.Y - b.Y); }
        public static Point Multiply (this Point a, int b)    { return new Point(a.X * b, a.Y * b); }
        public static Point Divide   (this Point a, int b)    { return new Point(a.X / b, a.Y / b); }
        public static Point Divide   (this Point a, double b) { return new Point((int)(a.X / b), (int)(a.Y / b)); }

        public static Rectangle ToRectangle(this Point a, Size s) { return new Rectangle(a, s); }

        public static long SquareDistance(this Point a, Point b) { return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y); }
    }
}
