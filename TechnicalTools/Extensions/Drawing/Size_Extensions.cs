﻿using System;
using System.Drawing;


namespace TechnicalTools
{
    public static class Size_Extensions
    {
        public static Size Add(this Size a, Size b)            { return new Size(a.Width + b.Width, a.Height + b.Height); }
        public static Size Add(this Size a, Rectangle b)       { return new Size(a.Width + b.Width, a.Height + b.Height); }
        public static Size Add(this Size a, Point b)           { return new Size(a.Width + b.X,     a.Height + b.Y); }
        public static Size Add(this Size a, int w, int h)      { return new Size(a.Width + w,       a.Height + h); }

        public static Size Substract(this Size a, Size b)      { return new Size(a.Width - b.Width, a.Height - b.Height); }
        public static Size Substract(this Size a, Rectangle b) { return new Size(a.Width - b.Width, a.Height - b.Height); }
        public static Size Substract(this Size a, Point b)     { return new Size(a.Width - b.X,     a.Height - b.Y); }
        public static Size Substract(this Size a, int w, int h){ return new Size(a.Width - w,       a.Height - h); }

        public static Size Multiply(this Size a, int b)        { return new Size(      a.Width * b,        a.Height * b); }
        public static Size Multiply(this Size a, double b)     { return new Size((int)(a.Width * b), (int)(a.Height * b)); }
        public static Size Divide(this Size a, int b)          { return new Size(      a.Width / b,        a.Height / b); }
        public static Size Divide(this Size a, double b)       { return new Size((int)(a.Width / b), (int)(a.Height / b)); }

        public static Point ToPoint(this Size s) { return new Point(s.Width, s.Height); }
        public static Rectangle ToRectangle(this Size s) { return new Rectangle(0, 0, s.Width, s.Height); }
    }
}
