﻿using System;
using System.Collections.Generic;
using System.Dynamic;


namespace TechnicalTools
{
    public static class ExpandoObject_Extensions
    {
        public static ExpandoObject Clone(this ExpandoObject obj)
        {
            IDictionary<string, object> clone = new ExpandoObject();
            foreach (var kvp in obj)
                clone.Add(kvp);
            return (ExpandoObject)clone;
        }
    }
}
