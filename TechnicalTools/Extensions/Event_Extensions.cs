﻿using System;

using TechnicalTools.Diagnostics;


namespace TechnicalTools
{
    public static class Event_Extensions
    {
        public static void Raise(this EventHandler evnt, object sender, EventArgs e = null)
        {
            evnt?.Invoke(sender, e ?? EventArgs.Empty);
        }
        public static void RaiseAndIgnoreException(this EventHandler evnt, object sender, EventArgs e = null)
        {
            ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() => Raise(evnt, e));
        }
    }
}
