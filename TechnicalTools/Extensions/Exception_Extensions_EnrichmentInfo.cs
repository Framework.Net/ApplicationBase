﻿using System;
using System.Collections.Generic;


namespace TechnicalTools
{
    public class ExceptionEnrichmentInfo
    {
        public eExceptionEnrichmentType Type { get; }
        public bool                     BeforeMessage { get; }
        public string                   Info { get; }

        public ExceptionEnrichmentInfo(eExceptionEnrichmentType type, string info, bool beforeMessage)
        {
            Info = info;
            Type = type;
            BeforeMessage = beforeMessage;
        }
    }
    [Flags]
    public enum eExceptionEnrichmentType
    {
        Technical = 1,
        UserUnderstandable = 2
    }
    public static partial class Exception_Extensions
    {
        public static bool HasEnrichments(this Exception ex)
        {
            return ex.Data.Contains(DataEnrichmentsKey);
        }


        public static IReadOnlyList<ExceptionEnrichmentInfo> GetEnrichments(this Exception ex)
        {
            return GetEnrichmentsList(ex);
        }
        static List<ExceptionEnrichmentInfo> GetEnrichmentsList(Exception ex)
        {
            if (!ex.Data.Contains(DataEnrichmentsKey))
                ex.Data[DataEnrichmentsKey] = new List<ExceptionEnrichmentInfo>();
            return (List<ExceptionEnrichmentInfo>)ex.Data[DataEnrichmentsKey];
        }
        const string DataEnrichmentsKey = "Enrichments";

        /// <summary>
        /// Add any context information on an instance of Exception,
        /// so when using <see cref="DefaultExceptionManager.Format(Exception)">DefaultExceptionManager.Format</see> (and overloads)
        /// this information will be dumped too.
        /// The information are stored in the call order, in case of multiple calls.
        /// You can priorize some information by setting <paramref name="insertBeforeOthers"/> to true
        /// </summary>
        /// <param name="ex">The exception on which we want to enrich diagnostic information</param>
        /// <param name="msg">Diagnostic information to add</param>
        /// <param name="insertBeforeOthers">Makes the information to be displayed before any other information currently added.</param>
        public static TException EnrichDiagnosticWith<TException>(this TException ex, string msg, eExceptionEnrichmentType type, bool insertBeforeOthers = false, bool insertBeforeExceptionMessage = false)
            where TException : Exception
        {
            return EnrichDiagnosticWith(ex, new ExceptionEnrichmentInfo(type, msg, insertBeforeExceptionMessage), insertBeforeOthers);
        }
        static TException EnrichDiagnosticWith<TException>(this TException ex, ExceptionEnrichmentInfo info, bool insertBeforeOthers = false)
            where TException : Exception
        {
            var enrichments = GetEnrichmentsList(ex);
            if (insertBeforeOthers)
                enrichments.Insert(0, info);
            else
                enrichments.Add(info);
            return ex;
        }
        public static bool EnrichDiagnosticWithAndReturnFalse(this Exception ex, string msg, eExceptionEnrichmentType type, bool insertBeforeOthers = false, bool insertBeforeExceptionMessage = false)
        {
            EnrichDiagnosticWith(ex, msg, type, insertBeforeOthers, insertBeforeExceptionMessage);
            return false;
        }
        static bool EnrichDiagnosticWithAndReturnFalse(this Exception ex, ExceptionEnrichmentInfo info, bool insertBeforeOthers = false)
        {
            EnrichDiagnosticWith(ex, info, insertBeforeOthers);
            return false;
        }
    }
}
