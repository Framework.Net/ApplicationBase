﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.ComponentModel;

namespace TechnicalTools
{
    public static class BindingList_Extensions
    {
        [DebuggerHidden, DebuggerStepThrough]
        public static void AddRange<TBase, TElement>(this BindingList<TBase> lst, IEnumerable<TElement> values)
            where TElement : TBase
        {
            foreach (var v in values)
                lst.Add(v);
        }
    }
}
