﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;


namespace TechnicalTools
{
    public static class Win32Exception_Extensions
    {
        // From http://stackoverflow.com/a/34622953/294998
        /// <summary>
        /// Ne marche pas trop...
        /// Voir https://msdn.microsoft.com/en-us/library/windows/desktop/ms681381(v=vs.85).aspx ?
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string GetExceptionMessageInEnglish(this Win32Exception ex)
        {
            const int nCapacity = 820; // max error length
            const uint FORMAT_MSG_FROM_SYS = 0x01000;
            const uint engLangID = (0x01 << 10) | 0x09;
            const uint defLangID = 0x0;
            StringBuilder engSb = new StringBuilder(nCapacity);
            StringBuilder defSb = new StringBuilder(nCapacity);
            return WithAllThreadCultureForcedToEnglish(() =>
            {
                FormatMessage(FORMAT_MSG_FROM_SYS, IntPtr.Zero, (uint)ex.ErrorCode, defLangID, defSb, nCapacity, IntPtr.Zero);
                FormatMessage(FORMAT_MSG_FROM_SYS, IntPtr.Zero, (uint)ex.ErrorCode, engLangID, engSb, nCapacity, IntPtr.Zero);
                string sDefMsg = defSb.ToString().TrimEnd(' ', '.', '\r', '\n');
                string sEngMsg = engSb.ToString().TrimEnd(' ', '.', '\r', '\n');
                if (sDefMsg == sEngMsg) //message already in English (or no english on machine?)
                {
                    //nothing left to do:
                    return ex.Message;
                }
                else
                {
                    string msg = ex.Message.Replace(sDefMsg, sEngMsg);
                    if (msg == ex.Message)
                    {
                        //replace didn't worked, can be message with arguments in the middle.
                        //I such as case print both: original and translated. to not lose the arguments.
                        return ex.Message + " (In English: " + sEngMsg + ")";
                    }
                    else
                    {
                        //successfully replaced!
                        return msg;
                    }
                }
            });
        }

        static T WithAllThreadCultureForcedToEnglish<T>(Func<T> action)
        {
            return WithAllThreadCultureForcedTo(ciEN, action);
        }
        static readonly CultureInfo ciEN = new CultureInfo("en-US");
        static T WithAllThreadCultureForcedTo<T>(CultureInfo ci, Func<T> action)
        {
            CultureInfo curCi = Thread.CurrentThread.CurrentCulture;
            CultureInfo curUICi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
            try
            {
                fsUserDefaultCulturetype.SetValue(null, ci);
                fsUserDefaultUICulturetype.SetValue(null, ci);

                return action();
            }
            finally
            {
                fsUserDefaultCulturetype.SetValue(null, curCi);
                fsUserDefaultUICulturetype.SetValue(null, curUICi);
            }
        }
        static FieldInfo fsUserDefaultCulturetype = typeof(CultureInfo).GetField("s_userDefaultCulture", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Static);
        static FieldInfo fsUserDefaultUICulturetype = typeof(CultureInfo).GetField("s_userDefaultUICulture", BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Static);


        [DllImport("kernel32.dll")]
        static extern uint FormatMessage(uint dwFlags, IntPtr lpSource, uint dwMessageId, uint dwLanguageId, StringBuilder lpBuffer, uint nSize, IntPtr Arguments);
    }
}
