﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

using TechnicalTools.Diagnostics;


namespace TechnicalTools
{
    public static class Object_Extensions
    {
        public static bool In<T>(this T item, params T[] values)
        {
            return values.Contains(item);
        }
        public static bool In<T>(this T item, IReadOnlyCollection<T> values)
        {
            return values.Contains(item);
        }

        public static bool NotIn<T>(this T item, params T[] values)
        {
            return !values.Contains(item);
        }
        public static bool NotIn<T>(this T item, IReadOnlyCollection<T> values)
        {
            return !values.Contains(item);
        }

        public static T[] WrapInArray<T>(this T item)
        {
            return new[] { item };
        }
        public static List<T> WrapInList<T>(this T item)
        {
            return new List<T>() { item };
        }
        public static BindingList<T> WrapInBindingList<T>(this T item)
        {
            return new BindingList<T>() { item };
        }

        // Allow to cast beautifully :
        /// <summary>
        ///  instead of
        /// <code>var view = (GridView)((GridControl)((BaseControl)sender).Parent).MainView;</code>
        /// we can write :
        /// <code>var view = sender.As&lt;BaseControl&gt;().Parent.As&lt;GridControl&gt;().MainView.As&lt;GridView&gt;();</code>
        /// The main interest is to make code more readable AND make resharper quiet
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        [DebuggerHidden, DebuggerStepThrough]
        public static T As<T>(this object obj)
        {
            return (T)obj;
        }

        public static bool IsOfType(this object obj, Type type)
        {
            return type.IsInstanceOfType(obj);
        }

        public static T InlineInit<T>(this T obj, Action<T> init)
        {
            init(obj);
            return obj;
        }

        public static string ToStringInvariant(this object obj)
        {
            return obj is IConvertible ? ((IConvertible)obj).ToString(CultureInfo.InvariantCulture)
                : obj is IFormattable ? ((IFormattable)obj).ToString(null, CultureInfo.InvariantCulture)
                : obj.ToString();
        }

        [DebuggerStepThrough]
        public static T ThrowIfNull<T>(this T value, string msg = "Cannot be null")
            where T : class
        {
            if (value == null)
            {
                DebugTools.Break();
                throw new Exception(msg);
            }
            return value;
        }
        [DebuggerStepThrough]
        public static T ThrowIfNull<T>(this T? value, string msg = "Cannot be null")
            where T : struct
        {
            if (!value.HasValue)
            {
                DebugTools.Break();
                throw new Exception(msg);
            }
            return value.Value;
        }

        /// <summary>
        /// Wraps this object instance into an IEnumerable&lt;T&gt;
        /// consisting of a single item.
        /// </summary>
        /// <typeparam name="T"> Type of the object. </typeparam>
        /// <param name="item"> The instance that will be wrapped. </param>
        /// <returns> An IEnumerable&lt;T&gt; consisting of a single item. </returns>
        /// <see cref="https://stackoverflow.com/questions/1577822/passing-a-single-item-as-ienumerablet"/>
        public static IEnumerable<T> Yield<T>(this T item)
        {
            yield return item;
        }

        public static T SubstituteByIfEquals<T>(this T v, T substitute, T equals)
        {
            if (v.Equals(equals))
                return substitute;
            return v;
        }
        public static T? SubstituteByIfEquals<T>(this T v, T substitute, T? equals)
             where T  : struct
        {
            if (v.Equals(equals))
                return substitute;
            return v;
        }
    }
}
