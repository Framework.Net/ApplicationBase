﻿using System;
using System.Diagnostics;


namespace TechnicalTools
{
    public static class Func_Extensions
    {
        [DebuggerHidden][DebuggerStepThrough] public static T Identity<T>(T x) { return x; }
        [DebuggerHidden][DebuggerStepThrough] public static bool ReturnTrue() { return true; }
        [DebuggerHidden][DebuggerStepThrough] public static bool ReturnTrue<T>(T x) { return true; }
        [DebuggerHidden][DebuggerStepThrough] public static bool ReturnFalse() { return false; }
        [DebuggerHidden][DebuggerStepThrough] public static bool ReturnFalse<T>(T x) { return false; }

        [DebuggerHidden]
        [DebuggerStepThrough]
        public static Func<TConstantReturnedValue> Return<TConstantReturnedValue>(TConstantReturnedValue value)
        {
            [DebuggerHidden][DebuggerStepThrough]
            TConstantReturnedValue returnValue()
            {
                return value;
            }
            return returnValue;
        }

        [DebuggerHidden]
        [DebuggerStepThrough]
        public static Func<TConstantReturnedValue> Return<TAny1, TConstantReturnedValue>(TAny1 _, TConstantReturnedValue value)
        {
            [DebuggerHidden][DebuggerStepThrough]
            TConstantReturnedValue returnValue()
            {
                return value;
            }
            return returnValue;
        }
    }
}
