﻿using System;
using System.Diagnostics;


namespace TechnicalTools.Annotations
{
    /// <summary>
    /// Le mot clef readonly fait perdre parfois en performance
    /// cf : https://codeblog.jonskeet.uk/2014/07/16/micro-optimization-the-surprising-inefficiency-of-readonly-fields/
    /// Cet attribut sert a garder l'idée que le champs est readonly (pour la lecture de code) sans utiliser le mot clef
    /// Par abus, un champs peut aussi beneficier de cet attribute si il est initialisé plus tard, mais une seule fois (exemple : Lazy init definitive)
    /// <remarks>ReadOnlyAttribute est deja pris dans System.ComponentModel !</remarks>
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class AsReadOnlyAttribute : System.Attribute
    {
    }
}
