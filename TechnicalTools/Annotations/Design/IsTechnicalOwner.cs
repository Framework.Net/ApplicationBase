﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TechnicalTools.Annotations.Design
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple=false)]
    public class IsTechnicalOwner : DesignAttribute
    {
        public IsTechnicalOwner()
        {
        }
    }
}
