﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TechnicalTools.Annotations.Design
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple=true)]
    public class WhereParamImplements : EntityAttribute
    {
        public Type ParamType      { get { Debug.Assert(IsBuilt); return _ParamType; } }
        public Type ClassInterface { get { Debug.Assert(IsBuilt); return _ClassInterface; } }

        public WhereParamImplements(string paramName, Type interfaceType, params object[] genericParams)
        {
            _paramName = paramName;
            _BaseInterfaceType = interfaceType;
            if (genericParams != null && !genericParams.All(p => p is string || p is Type))
                throw new Exception("Seul les parametres de tpe Type ou string sont acceptés !");
            if (genericParams != null && genericParams.Any(p => p is string) && !_BaseInterfaceType.IsGenericTypeDefinition)
                throw new Exception("La classe ou l'interface n'est pas générique ! Il ne peut y avoir de parametres");
            _GenericParamsUnresolved = genericParams;
            _GenericParams = new Type[genericParams.Length];
        }
        readonly string _paramName;
        readonly Type _BaseInterfaceType;
        readonly object[] _GenericParamsUnresolved;  // Soit un Type, soit une string dans le cou on reference un parametre generique
        Type[] _GenericParams;

        Type _ParamType;
        Type _ClassInterface;

        public override bool IsBuilt
        {
            get { return _ClassInterface != null; }
        }

        public override void Build(Type thisClass)
        {
            _ParamType = thisClass.GetGenericArguments().FirstOrDefault(p => p.Name == _paramName);
            _ClassInterface = BuildType(thisClass, _BaseInterfaceType, _GenericParamsUnresolved, ref _GenericParams);
        }
    }
}
