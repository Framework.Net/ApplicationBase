﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TechnicalTools.Annotations.Design
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class CompositeAttribute : DesignAttribute
    {
        public CompositeAttribute()
        {
        }
    }

    //[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    //public class ReferenceAttribute : DesignAttribute
    //{
    //    public Reference()
    //    {
    //    }
    //}

}
