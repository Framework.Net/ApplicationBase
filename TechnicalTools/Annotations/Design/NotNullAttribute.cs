﻿using System;


namespace TechnicalTools.Annotations.Design
{
    /// <summary>
    /// Permet de marquer les attributs / propriete consideré comme non null.
    /// Resharper permet de prendre en consideration ce genre d'attribut pour aider / conseiller le developpeur.
    /// Il suffit de remplacer le using TechnicalTools.Annotations par JetBrains.Annotations.dll
    /// Voir http://charlass.wordpress.com/2009/07/30/resharper-using-notnull-to-generate-better-code/
    /// P.S : Pour l'appliquer à la valeur retournée par une fonction, il faut ecrire : "[return: NotNull]" au dessus
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
    public class NotNullAttribute : DesignAttribute, IArgumentValidationAttribute
    {
        // Example :
        /*
        [NotNull] int _foo1;

        [NotNull] public int Foo { get { return _foo1; } set { _foo1 = value; } }

        [return: NotNull]
        public int? bar([NotNull] int baz)
        {
            return baz;
        }
        */

        public void Validate(object value, string argumentName)
        {
            if (value == null)
            {
                throw new ArgumentNullException(argumentName);
            }
        }
    }
}
