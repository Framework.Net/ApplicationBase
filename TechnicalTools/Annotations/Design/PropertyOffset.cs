﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TechnicalTools.Annotations.Design
{
    // Comme le design impose d'utiliser des propriete et non des champs
    // Cet attribut a pour but de remplacer FieldOffset
    [AttributeUsage(AttributeTargets.Property, AllowMultiple=false)]
    public class PropertyOffset : DesignAttribute
    {
        public int FieldOffset { get; private set; }

        public PropertyOffset(int fieldOffset)
        {
            FieldOffset = fieldOffset;
        }
    }
}
