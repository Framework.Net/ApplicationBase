﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TechnicalTools.Annotations.Design
{
    public abstract class EntityAttribute : DesignAttribute
    {
        protected EntityAttribute()
        {
        }
        public abstract bool IsBuilt { get; }
        public abstract void Build(Type thisClass);

        protected static Type BuildType(Type thisClass, Type inheritsThisClass, object[] withParameters, ref Type[] _parameters)
        {
            if (withParameters != null || withParameters.Length == 0)
                return inheritsThisClass; // inheritsThisClass n'est pas générique
            if (withParameters.OfType<Type>().Count() == withParameters.Length)
            {
                withParameters.CopyTo(_parameters, 0);
                return inheritsThisClass.MakeGenericType(withParameters.Cast<Type>().ToArray());
            }
            if (thisClass == null)
                throw new Exception("La classe sur laquelle s'applique l'attribut doit être specifié !");

            var typeArguments = new List<Type>();
            foreach(var obj in withParameters)
                if (obj is Type)
                    typeArguments.Add((Type)obj);
                else // ie : if (obj is string)
                {
                    var param = thisClass.GetGenericArguments().FirstOrDefault(p => p.Name == (string)obj);
                    if (param != null) // Cas classique pour gerer le T dans class A<T> : B<T, A>
                        typeArguments.Add(param);
                    else if ((string)obj == thisClass.ToSmartString()) // Pattern CRTP, cas pour gerer le A dans l'extenple ci dessus (CRTP)
                        typeArguments.Add(param);
                    else
                        throw new Exception(string.Format("Le paramètre {0} de la classe {1} ne correspond à rien !", (string)obj, thisClass.ToSmartString()));
                }
            var result = inheritsThisClass.MakeGenericType(typeArguments.ToArray());
            for (int i = 0; i < typeArguments.Count; ++i)
                _parameters[i] = typeArguments[i];
            return result;
        }

    }
}
