﻿using System;


namespace TechnicalTools.Annotations
{
    /// <summary>
    /// Indique sous quelle forme sont enregistré les valeurs 0% et 100%
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
    public class PercentStorageAttribute : Attribute
    {
        public object Percent0StoredAs;
        public object Percent100StoredAs;
    }
}
