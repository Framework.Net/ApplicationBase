﻿using System;
using System.Diagnostics;


namespace TechnicalTools.Annotations
{
    /// <summary>
    /// Indique qu'une valeur de type DateTime n'est pas censé contenir de partie Time
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
    public class DateOnlyAttribute : System.Attribute, IArgumentValidationAttribute
    {
        public void Validate(object value, string argumentName)
        {
            Debug.Assert(value  == null || value is DateTime);
            Check((DateTime?)value, argumentName);
        }
        public static void Check(DateTime? date, string argumentName = null)
        {
            if (date.HasValue && date.Value.TimeOfDay != TimeSpan.Zero)
                throw new Exception(string.Format("Date {0} should not contain time part", argumentName ?? string.Empty));
        }

    }
}
