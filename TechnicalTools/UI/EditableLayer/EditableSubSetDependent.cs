﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.UI;
using TechnicalTools.UI.EditableLayer;


namespace DataMapper.Helpers
{
    /// <summary>
    /// EditableSubSetDependent is not an EditableSet where the GetFreshItem is just "more filtered"
    /// EditableSubSetDependent is dependant on another EditableSet so that items are shared and not loaded twice in memory
    /// EditableSubSetDependent is to EditableSet as DataView Type is to DataTable
    /// </summary>
    public abstract class EditableSubSetDependent<TItem, IItem> : EditableSet<TItem, IItem>
        where TItem : class, IItem
        where IItem : class, ICopyable<IItem>, IHasTypedIdReadable
    {
        readonly EditableSet<TItem, IItem> _source; // dependency

        protected abstract Func<TItem, bool> GetFilter();

        protected EditableSubSetDependent(EditableSet<TItem, IItem> source, bool doNotLoadItems = false)
            : base(source.BuildClosedId, source.AllowAccessOnlyToMainInterface, source.ProvidedTypeBuilder, true)
        {
            _source = source;
            AllowNew = _source.AllowNew;
            AllowEdit = _source.AllowEdit;
            AllowRemove = _source.AllowRemove;

            if (!doNotLoadItems && !DesignTimeHelper.IsInDesignMode)
                Refresh();
        }


        protected sealed override List<TItem> GetFreshItems(IProgress<string> pr)
        {
            lock (_lock)
            {
                if (!_avoidSourceRefresh)
                    _source.Refresh(pr);
                return GetFreshItemsFromLocalData();
            }
        }
        readonly object _lock = new object();
        bool _avoidSourceRefresh;

        List<TItem> GetFreshItemsFromLocalData()
        {
            var filter = GetFilter();
            return _source.OriginalSet.Where(filter).ToList();
        }


        protected override Action ApplyToDatabaseAndModel(IProgress<string> pr = null)
        {
            // because EditableSet class is supposed to works using diff,
            // We can use ApplyToDatabaseAndModel the items not inside "this" won't be touched
            Action baseApply = base.ApplyToDatabaseAndModel(pr);
            return () =>
            {
                baseApply();
                // Notify base source that some object have been modified
                // TODO : Generate a back event instead of a method call because if _source is also a EditableSubSetDependent...
                _source.NotifyDependentSetHaveChanged(this);
                // Note : if we would call NotifyDependentSetHaveChanged(this) here,
                // the content of "this" should not change because this is already updated
            };
        }

        // Called by another dependent set
        public override void NotifyDependentSetHaveChanged(EditableSet<TItem, IItem> dependentSet = null)
        {
            // Probably call _source.Refresh() but we let _source to decide what it want to do
            _source.NotifyDependentSetHaveChanged(dependentSet);
            // Here we want to update / sync locally the result without doing a call to real source
            var b = _avoidSourceRefresh;
            _avoidSourceRefresh = true;
            try
            {
                Refresh();
            }
            finally
            {
                _avoidSourceRefresh = b;
            }
        }

        // TODO : Write other redirection ?
        protected internal override string ValidateProperty(IItem item, Type interfaceType, PropertyInfo property)
        {
            return _source.ValidateProperty(item, interfaceType, property);
        }

        protected internal override string ValidateSet(IEnumerable<IItem> set, IProgress<string> pr = null)
        {
            return _source.ValidateSet(set);
        }
    }

}
