﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

using Microsoft.CSharp;

using TechnicalTools.Diagnostics;
using TechnicalTools.Extensions.Data;
using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.Model.Attributes;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;


namespace TechnicalTools.UI.EditableLayer
{
    public class EditableSubstituteBuilder<TItem, IItem>
        where TItem : class, IItem, IHasTypedIdReadable
        where IItem : class, ICopyable<IItem>, IHasTypedIdReadable
    {
        public Type GetSubstituteTypeToEditForObject(Func<IItem, IIdTuple> buildClosedId, bool allowAccesOnlyToMainInterface)
        {
            Debug.Assert(typeof(IItem).IsInterface);
            return GetSubstituteInfoFor(typeof(TItem), buildClosedId, allowAccesOnlyToMainInterface).EditableType;
        }

        public IItem CreateSubstitueObjectToEdit(EditableSet<TItem, IItem> owner, TItem obj, Func<IItem, IIdTuple> buildClosedId, bool allowAccesOnlyToMainInterface)
        {
            Debug.Assert(typeof(IItem).IsInterface);
            return CreateEditableFor(owner, obj, buildClosedId, allowAccesOnlyToMainInterface);
        }
        public List<IItem> CreateSubstitueObjectToEdit(EditableSet<TItem, IItem> owner, List<TItem> objects, Func<IItem, IIdTuple> buildClosedId, bool allowAccesOnlyToMainInterface)
        {
            Debug.Assert(typeof(IItem).IsInterface);
            return objects.Select(obj => CreateSubstitueObjectToEdit(owner, obj, buildClosedId, allowAccesOnlyToMainInterface))
                          .ToList();
        }

        protected IItem CreateEditableFor(EditableSet<TItem, IItem> owner, TItem obj, Func<IItem, IIdTuple> buildClosedId, bool allowAccesOnlyToMainInterface)
        {
            var info = GetSubstituteInfoFor(typeof(TItem), buildClosedId, allowAccesOnlyToMainInterface);
            return info.CreateInstance(owner, obj) as IItem;
        }

        private EditableTypeInfo GetSubstituteInfoFor(Type objType, Func<IItem, IIdTuple> buildClosedId, bool allowAccesOnlyToMainInterface)
        {
            var objInterface = typeof(IItem);

            Debug.Assert(objInterface.IsInterface);

            var info = _editableTypes.GetOrAdd(objType, ot => BuildEditableTypeInfo(ot, objInterface, buildClosedId, allowAccesOnlyToMainInterface));
            // If this assert fail, it does not necessary means something is wrong
            // It may be becasue two instance of lambda are different.
            // If they do the same thing it is ok.
            // let's see here to make this assert do its job better https://stackoverflow.com/questions/673205/how-to-check-if-two-expressionfunct-bool-are-the-same
            Debug.Assert(info.BuildClosedId.Equals(buildClosedId));
            return info;
        }
        readonly ConcurrentDictionary<Type, EditableTypeInfo> _editableTypes = new ConcurrentDictionary<Type, EditableTypeInfo>();
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(EditableSubstituteBuilder<TItem, IItem>));

        class EditableTypeInfo
        {
            public Type For { get; set; }
            public Type EditableType { get; set; }
            public Func<IItem, IIdTuple> BuildClosedId { get; set; }
            public Func<EditableSet<TItem, IItem>, IItem, EditableSubstitute> CreateInstance { get; set; }
        }

        string TempPathForGeneratedAssembly()
        {
            return Environment.GetEnvironmentVariable("TEMP");
        }
        EditableTypeInfo BuildEditableTypeInfo(Type objType, Type objInterface, Func<IItem, IIdTuple> buildClosedId, bool allowAccesOnlyToMainInterface)
        {
            var parameters = new CompilerParameters
            {
                GenerateExecutable = false, // True - exe file generation, false - dll file generation
                                            // Ces trois lignes de code permettent de voir la ligne qui a buggé en live dans Visual Studio!
                GenerateInMemory = !DebugTools.IsDebug || !DebugTools.IsForDevelopper, // True - memory generation, false - external file generation
                TempFiles = new TempFileCollection(TempPathForGeneratedAssembly(), false),
                IncludeDebugInformation = DebugTools.IsDebug,
                TreatWarningsAsErrors = true
            };

            var assembliesToRefer = GetAssemblyDependencies(objInterface);
            var assembliesToReferLocations = assembliesToRefer
                 .Where(ass =>
                 {
                     try
                     {
                         return ass.Location?.Length > 0;
                     }
                     catch (Exception ex) // this happens for Microsoft.GeneratedCode and DevExpress.Data.????.Dynamic assembly
                                          // and for DevExpress.Data.v17.1.Dynamic_839e518e-e659-40a3-a214-5809694080c7
                     {
                         try
                         {
                             _log.Info($"Cannot add assembly {ass.FullName} because: {ex}");
                         }
                         catch
                         {
                             _log.Info($"Cannot add assembly {ass.ToString()} (toString) because: {ex}");
                         }
                         return false;
                     }
                 })
                .Select(ass => //ass.IsMicrosoftAssembly()
                               //? Path.GetFileName(ass.Location)
                               //:
                             ass.Location)
                .Distinct()
                .ToList();
            // Usually it is "C:\\WINDOWS\\Microsoft.Net\\assembly\\GAC_MSIL\\System.Runtime\\v4.0_4.0.0.0__b03f5f7f11d50a3a\\System.Runtime.dll"
            if (assembliesToReferLocations.All(loc => !loc.Contains("System.Runtime.dll")))
                assembliesToReferLocations.Add("System.Runtime.dll"); // is missing sometimes but needed
            parameters.ReferencedAssemblies.AddRange(assembliesToReferLocations.ToArray());

            string nl = Environment.NewLine;
            var assemblyName = "EditableLayer";
            string className = "Editable" + objType.Name;
            string buildClosedIdStaticField = "BuildClosedId";
            // ReSharper disable PossibleNullReferenceException
            string source = UsingsDeclarationsForReferencedNamespaces(/*assembliesToRefer*/) + nl +
"[assembly: " + typeof(DynamicCodeCompiledByTechnicalToolsAttribute).FullName + $@"]
namespace {assemblyName}
{{
    public sealed class {className} : {typeof(EditableSubstitute<,,>).FullName.Replace("`3", "")}<{className},{objType.FullName},{objInterface.FullName}>, {objInterface.FullName}
    {{
        protected override {typeof(IIdTuple).Name} GetClosedId() {{ return BuildClosedId(this); }}
        private static Func<{typeof(IItem).FullName}, {typeof(IIdTuple).Name}> {buildClosedIdStaticField} = null;

        public {className}({typeof(EditableSet<,>).FullName.Replace("`2", "")}<{objType.FullName},{objInterface.FullName}> owner, {objType.FullName} obj)
            : base(owner, obj)
        {{
        }}
        public void CopyFrom({objInterface.FullName} source)
        {{
            base.CopyFrom(source);
        }}

        #region Editable properties
";
            // ReSharper restore PossibleNullReferenceException
            foreach (var prop in objInterface.GetProperties())
                if (prop.CanWrite)
source += $@"
        public {prop.PropertyType.ToSmartString(true)} {prop.Name} {{ get {{ return ({prop.PropertyType.ToSmartString(true)})Get({AsCsString(prop.Name)}); }} set {{ Set({AsCsString(prop.Name)}, value); }} }}";

source += $@"
        #endregion Editable properties

        {GenerateReadOnlyMembersAndOthers(objInterface, allowAccesOnlyToMainInterface, "        ")}
";

source += @"

    }
}";

            // The method CompileAssemblyFromSource internally calls System.Threading.WaitHandle.WaitOne(int millisecondsTimeout, bool exitContext)
            // which can cause message pumping and call OnPaint event of some controls...
            // Sometimes the compilation is done inside the constructor of a singleton, and this singleton is used by handler called through OnPaint event
            // (for example the event CustomUnboundColumnData). Because we are in same thread, this cause a null exception
            // So we let the responsability to handle any UI refresh issues to caller, and not here by having code that cause some Application.DoEvents evil stuff inside technical and business code
            CompilerResults results = null;
            using (var mre = new ManualResetEvent(false))
            {
                // ReSharper disable once AccessToDisposedClosure
                var provider = new CSharpCodeProvider();
                var th = new Thread(() => { try { results = provider.CompileAssemblyFromSource(parameters, source); } finally { mre.Set(); } });
                th.Start();
                mre.WaitOneNonAlertable(); // This does not cause message pumping of some event (tested :))
                results.TempFiles?.Delete();
            }
            //CompilerResults results = provider.CompileAssemblyFromSource(parameters, source);

            if (results.Errors.HasErrors)
            {
                var driveLetter = Path.GetTempPath().Remove(1);
                if (new DriveInfo(driveLetter).AvailableFreeSpace < 10*1024*1024) // 10 Mo is totally arbitrary
                    throw new Exception($"Error compiling script! Please free some space on drive \"{driveLetter.ToUpper()}):\" and retry", null);
                string errMsg = "Error compiling script !" + Environment.NewLine +
                                results.Errors.Cast<CompilerError>().Where(error => !error.IsWarning).Select(error => string.Format("Error {0} (Line {1}, Column {2}) : {3}", error.ErrorNumber, error.Line, error.Column, error.ErrorText)).Join(Environment.NewLine) +
                                Environment.NewLine +
                                results.Errors.Cast<CompilerError>().Where(error => error.IsWarning).Select(error => string.Format("Warning {0} (Line {1}, Column {2}) : {3}", error.ErrorNumber, error.Line, error.Column, error.ErrorText)).Join(Environment.NewLine);
                throw new TechnicalException(errMsg, null);
            }

            Assembly assembly = results.CompiledAssembly;

            var editableType = assembly.GetType(assemblyName + "." + className);
            var fFuildClosedId = editableType.GetField(buildClosedIdStaticField, BindingFlags.NonPublic | BindingFlags.Static);
            Debug.Assert(fFuildClosedId != null, nameof(fFuildClosedId) + " != null");
            fFuildClosedId.SetValue(null, buildClosedId);
            var cons = editableType.GetConstructor(new[] { typeof(EditableSet<TItem, IItem>), objType });
            Debug.Assert(cons != null, nameof(cons) + " != null");
            return new EditableTypeInfo()
            {
                For = objType,
                EditableType = editableType,
                BuildClosedId = buildClosedId,
                CreateInstance = (editableSet, obj) => (EditableSubstitute)cons.Invoke(new[] { (object)editableSet, obj })
            };
        }
        //static int _nbGeneratedClass;

        static string AsCsString(string str) { return '"' + str + '"'; }

        protected virtual HashSet<Type> GetTypeDependencies(Type objType)
        {
            return _TypeDependencies.GetOrAdd(objType, CreateTypeDependency);
        }
        protected virtual HashSet<Type> CreateTypeDependency(Type objType)
        {
            // from http://stackoverflow.com/a/2384679/294998
            //var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies()
            //                                // Pour pouvoir utiliser le mot clef "dynamic" dans les scripts ! Parfois necessaire pour les objets COM
            //                                .Concat(typeof(CSharpCodeProvider).Assembly)
            //                                .ToArray();
            var typesAnalysed = new HashSet<Type>();
            var typesToAnalyse = new HashSet<Type>
            {
                typeof(BrowsableAttribute),
                typeof(IsTechnicalPropertyAttribute),
                typeof(IDataErrorInfo),
                typeof(Enumerable),
                objType
            };

            while (typesToAnalyse.Count > 0)
            {
                var type = typesToAnalyse.First();
                typesToAnalyse.Remove(type);
                if (typesAnalysed.Contains(type)) // type already visited or currently in visit
                    continue;
                typesAnalysed.Add(type); // flag type as visited / currently in visit

                var allTypes = type.GetProperties().Select(prop => prop.PropertyType).ToList();
                allTypes.AddRange(type.GetMethods().Where(meth => meth.ReturnType != typeof(void))
                                                   .Select(meth => meth.ReturnType));
                allTypes.AddRange(type.GetMethods().SelectMany(meth => meth.GetParameters().Select(p => p.ParameterType)));
                allTypes.AddRange(type.GetMethods().SelectMany(meth => meth.GetParameters().Select(p => p.ParameterType)));
                allTypes.Add(typeof(EditableSubstitute));

                if (type.BaseType != null)
                    allTypes.Add(type.BaseType);
                allTypes.AddRange(type.GetInterfaces().Except(type.BaseType == null ? new Type[0] : type.BaseType.GetInterfaces()));

                foreach (var t in allTypes)
                    if (!typesToAnalyse.Contains(t))
                        typesToAnalyse.Add(t);
            }
            return typesAnalysed;
        }
        // ReSharper disable once StaticMemberInGenericType
        static readonly ConcurrentDictionary<Type, HashSet<Type>> _TypeDependencies = new ConcurrentDictionary<Type, HashSet<Type>>();

        protected virtual Assembly[] GetAssemblyDependencies(Type objType)
        {
            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies()
                                                          //.Where(ass => !ass.IsDynamic); // Does not work !
                                                          //.Where(ass => !ass.Location.StartsWith(TempPathForGenereatedAssembly())) // not sage neither
                                                          .Where(ass => ass.CustomAttributes.All(ca => ca.AttributeType.Name != nameof(DynamicCodeCompiledByTechnicalToolsAttribute))) // not sage neither
                                                          .Where(ass => ass.FullName != "Anonymously Hosted DynamicMethods Assembly, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null")
                                                          // Ignore assembly generated by XmlParser
                                                          .Where(ass => ass.ManifestModule.ScopeName != "Microsoft.GeneratedCode")
                                                          .ToList();
            return loadedAssemblies.Concat(GetTypeDependencies(objType).Select(t => t.Assembly)).Distinct().ToArray();
        }
        static string UsingsDeclarationsForReferencedNamespaces(/*Assembly[] assemblies*/)
        {
            //return assemblies.SelectMany(ass => ass.GetTypes().SelectNotNull(t => t.Namespace)) // anonymous type does not have namespace
            //                 .Where(ns => ns != "Windows.Foundation.Diagnostics")
            //                 .Where(ns => ns != "<CppImplementationDetails>")
            //                 .Where(ns => ns != "<CrtImplementationDetails>")
            //                 .OrderBy(ns => ns.StartsWith("System"))
            //                 .ThenBy(ns => ns)
            //                 .GroupBy(ns => ns.IndexOf('.') == -1 ? ns : ns.Remove(ns.IndexOf('.')))
            //                 .Select(grp => grp.Select(ns => "using " + ns + ";").Distinct().Join(Environment.NewLine))
            //                 .Where(@using => @using != "using Windows.Foundation.Diagnostics;")
            //                 .Where(@using => @using != "using Windows.Foundation.Diagnostics;")
            //                 .Join(Environment.NewLine + Environment.NewLine);

            return "using System;" + Environment.NewLine
            + "using System.CodeDom.Compiler;" + Environment.NewLine
            + "using System.Collections.Generic;" + Environment.NewLine
            + "using System.Diagnostics;" + Environment.NewLine
            + "using System.IO;" + Environment.NewLine
            + "using System.Linq;" + Environment.NewLine
            + "using System.Reflection;" + Environment.NewLine
            + "using System.ComponentModel;" + Environment.NewLine
            + Environment.NewLine
            + "using TechnicalTools;" + Environment.NewLine
            + "using TechnicalTools.Diagnostics;" + Environment.NewLine
            + "using TechnicalTools.Model;" + Environment.NewLine
            + "using TechnicalTools.Model.Cache;";
        }


        static string GenerateReadOnlyMembersAndOthers(Type iType, bool allowAccesOnlyToMainInterface, string indent)
        {
            Debug.Assert(iType.IsInterface);

            string res = "";
            var nl = Environment.NewLine + indent;
            var iMembers = iType.GetProperties().Where(p => p.CanWrite).ToDictionary(p => p.Name);
            var members = iType.GetAllInheritedInterfaces().ToDictionary(i => i, i => i.GetMembers());
            members.Add(iType, iType.GetProperties().Where(p => !p.CanWrite).Cast<MemberInfo>().ToArray());

            // Remove interface already implemented by EditableSubstitute or manually handled (like IHasTypedIdReadable, IHasClosedId, ICopyable, etc)
            foreach (var i in typeof(EditableSubstitute<TItem, IItem>).GetAllInheritedInterfaces())
                members.Remove(i);

            string set(PropertyInfo p)
            {
                return !p.CanWrite ? ""
                        : allowAccesOnlyToMainInterface ? $"set {{ throw InvalidPropertyWriteAccess(\"{p.Name}\"); }}"
                        : $"set {{ Set(\"{p.Name}\", value, typeof({p.DeclaringType.ToSmartString(true)})); }}";
            }

            var properties = "";
            foreach (var typeMembers in members) // in order from bottom interface to top interface
            {
                foreach (var p in typeMembers.Value.OfType<PropertyInfo>()) // in order from bottom interface to top interface
                {
                    // TODO : Cas à gérer : Il peut y avoir plusieurs property avec le meme nom si elles appartiennent à différentes interfaces héritées
                    //                      mais avec des signatures différentes etc...
                    if (!iMembers.ContainsKey(p.Name))
                    {
                        if (allowAccesOnlyToMainInterface)
                            properties += $"[{typeof(ReadOnlyAttribute).Name}(true)] ";
                        properties += $"public {p.PropertyType.ToSmartString(true)} {p.Name}"
                                    + " {"
                                    + $" get {{ return ({p.PropertyType.ToSmartString(true)})Get(\"{p.Name}\", typeof({typeMembers.Key.ToSmartString(true)})); }}"
                                    + $" {set(p)}"
                                    + " }"
                                    + nl;
                        iMembers.Add(p.Name, p);
                    }
                }
                properties += nl;
            }

            var propertiesRedeclaredWithDifferentType =
                             members.Select(typeMembers => typeMembers.Value.OfType<PropertyInfo>()
                                                                      .Where(p => iMembers.ContainsKey(p.Name) && iMembers[p.Name].PropertyType != p.PropertyType)
                                                                      .Select(p => (allowAccesOnlyToMainInterface ? $"[{typeof(ReadOnlyAttribute).Name}(true)] " : "") +
                                                                                   $"{p.PropertyType.ToSmartString(true)} {p.DeclaringType.ToSmartString(true)}.{p.Name} {{ get {{ return ({p.PropertyType.ToSmartString(true)})Get(\"{p.Name}\", typeof({typeMembers.Key.ToSmartString(true)})); }} {set(p)} }}")
                                                                      .Join(nl))
                                    .Join(nl + nl);

            if (properties.Length > 0)
            {
                res += "#region Read only properties of other interfaces" + nl;
                res += nl;
                res += properties;
                res += nl;
                res += propertiesRedeclaredWithDifferentType;
                res += nl;
                res += "#endregion Read only properties of other interfaces" + nl;
            }

            var methods = members.Select(typeMembers => typeMembers.Value.OfType<MethodInfo>()
                                                                   .Where(m => !m.IsSpecialName) // filter out get_Foo and set_foo from properties in interface
                                                                   .Select(m => $"{m.ReturnType.ToSmartString(true)} {typeMembers.Key.ToSmartString(true)}.{m.Name}" +
                                                                                m.GetGenericArguments().Select(a => a.Name).Join().IfNotBlankSurroundWith("<", ">") +
                                                                                "(" +
                                                                                m.GetParameters().Select(p => p.ParameterType.ToSmartString(true) + " " + p.Name).Join() +
                                                                                ") { throw new InvalidOperationException(); }")
                                                                   .Join(nl))
                                    .Join(nl + nl);
            if (methods.Length > 0)
            {
                if (res.Length > 0)
                    res += nl + nl + nl;
                res += "#region Forbidden methods" + nl;
                res += nl;
                res += methods;
                res += nl;
                res += "#endregion Forbidden methods" + nl;
            }

            bool hasEvent = false;
            foreach (var i in new[] { iType }.Concat(iType.GetAllInheritedInterfaces()))
                foreach (var evt in i.GetEvents())
                {
                    if (!hasEvent)
                        res += "#region Events" + nl + nl;
                    res += $@"event {evt.EventHandlerType.ToSmartString(true)} {evt.DeclaringType.ToSmartString(true)}.{evt.Name} {{ add {{ }} remove {{ }} }}";
                    hasEvent = true;
                }
            if (hasEvent)
                res += nl + "#endregion Events" + nl;

            return res;
        }
    }
}
