﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;


namespace TechnicalTools.UI.EditableLayer
{
    public interface IEditableSet
    {
        bool AllowNew    { get; }
        bool AllowRemove { get; }
        bool AllowEdit   { get; }

        bool HasChanges  { get; }
        eSetState State  { get; }

        int Refresh(IProgress<string> pr);
        void ResetAllEdits();

        string ValidateSet(IProgress<string> pr = null);
        void Commit(SynchronizationContext context = null, IProgress<string> pr = null);

        event EventHandler<object> ItemReseted;
        event EventHandler Commited;

        object CloneObject(object item);
        event Model.PropertyChangingEventHandler PropertyChanging;
        event Model.PropertyHasChangedEventHandler PropertyChanged;
    }
    public enum eSetState
    {
        NeverLoaded = 0,
        Loading,
        Loaded
    }
    public abstract class EditableSet : IEditableSet
    {
        public abstract bool AllowNew    { get; set; }
        public abstract bool AllowRemove { get; set; }
        public abstract bool AllowEdit   { get; set; }
        public abstract bool AllowDuplicateItemWithSameTypedId { get; set; }

        public abstract bool HasChanges { get; }
        public abstract eSetState State { get; }
        public abstract int  Refresh(IProgress<string> pr = null);
        public abstract void ResetAllEdits();

        public abstract string ValidateSet(IProgress<string> pr = null);
        public abstract void Commit(SynchronizationContext context = null, IProgress<string> pr = null);
        public /*protected*/ abstract void ValidateBeforeCommit(IProgress<string> pr = null);

        public abstract event EventHandler<object> ItemReseted;
        public abstract event EventHandler Commited;

        public /*protected*/ abstract void SimplifyChangesOnId();

        //protected abstract void ApplyToDatabaseInTransaction(SoftwareTransaction tran = null);
        protected abstract SoftwareTransaction OpenSoftwareTransaction();
        protected abstract Action ApplyToDatabaseAndModel(IProgress<string> pr = null);
        public /*protected*/ abstract Action ApplyChangedItemToDatabase(IProgress<string> pr = null);
        public /*protected*/ abstract Action ApplyCreatedItemToDatabase(IProgress<string> pr = null);
        public /*protected*/ abstract Action ApplyRemovedItemToDatabase(IProgress<string> pr = null);

        public abstract object CloneObject(object item);

        public abstract event Model.PropertyChangingEventHandler   PropertyChanging;
        public abstract event Model.PropertyHasChangedEventHandler PropertyChanged;

        protected static readonly ConcurrentDictionary<Type, PropertyDescriptorCollection> PropertiesBySubstituteType = new ConcurrentDictionary<Type, PropertyDescriptorCollection>();
    }

    public interface IEditableSet<in TItem, IItem> : IEditableSet, ISetValidator<IItem>
        where TItem : class, IItem
        where IItem : class, ICopyable<IItem>, IHasTypedIdReadable
    {
        IItem AddNew(TItem newItem = null);
        FixedBindingList<IItem> EditingSet { get; }
        FixedBindingList<IItem> Added { get; }
        FixedBindingList<IItem> Removed { get; }

        bool IsPropertyDirty(IItem item, string propertyName, Type interfaceType = null);
        bool IsDirty(IItem item);
        void ResetEdits(IItem item);
        IReadOnlyDictionary<string, object> GetChanges(IItem item, Type interfaceType = null);
        IReadOnlyDictionary<Tuple<Type, string>, object> GetAllChanges(IItem item);

        void AllowEditingSetAction(Action action);
        string GetAllChangesAsLog();

        void Remove(IItem item);
        void ResetEditsOrRemove(IItem item);
    }

    public abstract class EditableSet<TItem, IItem> : EditableSet, IEditableSet<TItem, IItem>
        where TItem : class, IItem
        where IItem : class, ICopyable<IItem>, IHasTypedIdReadable
    {
        // ReSharper disable once InconsistentNaming
        public EditableSubstituteBuilder<TItem, IItem> EffectiveTypeBuilder
        {
            get
            {
                if (ProvidedTypeBuilder == null)
                    lock (_typeBuilderLockAccess)
                    {
                        ProvidedTypeBuilder = _effectiveTypeBuilderDefault = _effectiveTypeBuilderTask.Result;
                        _effectiveTypeBuilderTask.Dispose();
                        _effectiveTypeBuilderTask = null;
                    }
                return ProvidedTypeBuilder;
            }
        }
        protected internal EditableSubstituteBuilder<TItem, IItem> ProvidedTypeBuilder { get; private set; }

        static      EditableSubstituteBuilder<TItem, IItem>  _effectiveTypeBuilderDefault { get; set;}
        static Task<EditableSubstituteBuilder<TItem, IItem>> _effectiveTypeBuilderTask;
        // ReSharper disable once StaticMemberInGenericType
        static readonly object _typeBuilderLockAccess = new object();

        protected internal Func<IItem, IIdTuple> BuildClosedId { get; }
        protected internal bool AllowAccessOnlyToMainInterface { get; }

        readonly PropertyDescriptorCollection _pdc;

        protected EditableSet(Func<IItem, IIdTuple> buildClosedId, bool allowAccessOnlyToMainInterface = false, EditableSubstituteBuilder<TItem, IItem> providedTypeBuilder = null, bool doNotLoadItems = false)
        {
            Debug.Assert(typeof(IItem).IsInterface);

            // This block of code (and content of property EffectiveTypeBuilder) are equivalent to a normal property and this line :
            // EffectiveTypeBuilder = providedTypeBuilder ?? new EditableSubstituteBuilder<TItem, IItem>();
            // But because this is slow, we delay the execution
            // Moreover the typeBuilder is cached in _effectiveTypeBuilderDefault which allow other set (especially subset) to reuse it
            ProvidedTypeBuilder = providedTypeBuilder;
            if (ProvidedTypeBuilder == null)
                lock (_typeBuilderLockAccess)
                {
                    if (_effectiveTypeBuilderDefault != null)
                        ProvidedTypeBuilder = _effectiveTypeBuilderDefault;
                    else if (_effectiveTypeBuilderTask == null) // Can take up to 1s in JIT to create this instance !
                        _effectiveTypeBuilderTask = Task.Run(() => new EditableSubstituteBuilder<TItem, IItem>());
                }

            BuildClosedId = buildClosedId;
            AllowAccessOnlyToMainInterface = allowAccessOnlyToMainInterface;

            if (!DesignTimeHelper.IsInDesignMode)
                _pdc = PropertiesBySubstituteType.GetOrAdd(SubstituteType, st => TypeDescriptor.GetProperties(st, new Attribute[] { new BrowsableAttribute(true) }));

            EditingSet = new TypedFixedBindingList(this);
            AllowNew = true;
            AllowRemove = true;
            AllowEdit = true;

            if (!doNotLoadItems && !DesignTimeHelper.IsInDesignMode)
                Refresh();
        }

        protected abstract List<TItem> GetFreshItems(IProgress<string> pr);
        public override bool AllowNew    { get { return EditingSet.AllowNew;    } set { EditingSet.WithListChangedEventsDisabled(() => EditingSet.AllowNew = value, RaiseResetEventOnChangingAllowedAction); } }
        public override bool AllowRemove { get { return EditingSet.AllowRemove; } set { EditingSet.WithListChangedEventsDisabled(() => EditingSet.AllowRemove = value, RaiseResetEventOnChangingAllowedAction); } }
        public override bool AllowEdit   { get { return EditingSet.AllowEdit;   } set { EditingSet.WithListChangedEventsDisabled(() => EditingSet.AllowEdit = value, RaiseResetEventOnChangingAllowedAction); } }
        public override bool AllowDuplicateItemWithSameTypedId { get; set; } = true;
        public bool RaiseResetEventOnChangingAllowedAction { get; set; }

        public Type SubstituteType { get { return EffectiveTypeBuilder.GetSubstituteTypeToEditForObject(BuildClosedId, AllowAccessOnlyToMainInterface); } }

        protected internal FixedBindingList<TItem> OriginalSet
        {
            get
            {
                if (!LastRefreshDate.HasValue)
                    Refresh();
                return _originalSet;
            }
        }
        protected readonly FixedBindingList<TItem> _originalSet = new FixedBindingList<TItem>();
        public DateTime? LastRefreshDate { get; protected set; }

        // This class allow UI layer to know which property exist, because with only interface type for reflection , some ui library are lost
        class TypedFixedBindingList : FixedBindingList<IItem>, ITypedList
        {
            readonly EditableSet<TItem, IItem> _owner;

            public TypedFixedBindingList(EditableSet<TItem, IItem> owner)
            {
                _owner = owner;
            }

            // This method is only used in the design-time framework
            // and by the obsolete DataGrid control.
            public string GetListName(PropertyDescriptor[] listAccessors)
            {
                return _owner.SubstituteType.Name;
            }

            public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
            {
                if (listAccessors != null && listAccessors.Length > 0)
                    DebugTools.Should(false, "dont know the case where we got here !");
                return _owner._pdc;
            }
        }

        // Contains all items currently edited  (even the removed one and added one)
        public FixedBindingList<IItem> EditingSet             { get; }
        public FixedBindingList<IItem> Added                  { get; } = new FixedBindingList<IItem>();
        public FixedBindingList<IItem> Removed                { get; } = new FixedBindingList<IItem>(); // TODO : ajouter un handler quand les liste change pour verifier que Removed.Intersect(Added) is empty et que Added et Removed sont dans EditingSet
        // ReSharper disable once PossibleNullReferenceException
        protected IEnumerable<IItem>   Modified(IEnumerable<IItem> lst)        { return lst.Except(Added).Except(Removed).Where(item => (item as EditableSubstitute).IsDirty); }
           static IEnumerable<IItem>   WithIdEdited(IEnumerable<IItem> lst)    { return lst.Where(c => !Equals(GetEditedObject(c).TypedId, c.TypedId)); }
           static IEnumerable<IItem>   WithIdUntouched(IEnumerable<IItem> lst) { return lst.Where(c => Equals(GetEditedObject(c).TypedId, c.TypedId)); }

        public override bool HasChanges { get { return Added.Any() || Removed.Any() || EditingSet.Any(IsDirty); } }
        public override eSetState State { get { return _State; } } eSetState _State;

        // ReSharper disable PossibleNullReferenceException
        protected void Set(IItem item, string propertyName, object value, Type interfaceType = null, bool onlyIfNotEdited = false)
        {
            if (!onlyIfNotEdited || !IsPropertyDirty(item, propertyName, interfaceType))
                (item as EditableSubstitute).Set(propertyName, value, interfaceType);
        }
        public bool IsPropertyDirty(IItem item, string propertyName, Type interfaceType = null)
        {
            return (item as EditableSubstitute).IsPropertyDirty(propertyName, interfaceType);
        }
        public bool IsDirty(IItem item)
        {
            return (item as EditableSubstitute).IsDirty || Removed.Contains(item) || Added.Contains(item);
        }
        public void ResetEdits(IItem item)
        {
            (item as EditableSubstitute).ResetEdits();
            ItemReseted?.Invoke(this, item);
        }
        public override event EventHandler<object> ItemReseted;

        public IReadOnlyDictionary<string, object> GetChanges(IItem item, Type interfaceType = null)
        {
            return (item as EditableSubstitute).GetChanges(interfaceType);
        }
        public IReadOnlyDictionary<Tuple<Type, string>, object> GetAllChanges(IItem item)
        {
            return (item as EditableSubstitute).GetAllChanges();
        }
        public IReadOnlyDictionary<Tuple<Type, string>, object> GetAllCurrentValues(IItem item)
        {
            return (item as EditableSubstitute).GetAllCurrentValues();
        }

        // ReSharper restore PossibleNullReferenceException
        public override void ResetAllEdits()
        {
            Added.Clear();
            Removed.Clear();
            EditingSet.Clear();
            using (var tmp = new MemoryHolder(OriginalSet.ToList()))
            {
                OriginalSet.Clear();
                Refresh();
            }
        }

        public string GetAllChangesAsLog()
        {
            string toStr(object obj)
            {
                return obj == null ? "<NULL>"
                     : (obj as IHasTypedIdReadable)?.TypedId.ToSmartString()
                    ?? obj.ToString();
            }
            string nl = Environment.NewLine;
            string logs = "";
            foreach (var a in Added)
            {
                var values = GetAllCurrentValues(a);
                logs += "Creating new " + typeof(TItem).Name + " { " + nl;
                foreach (var kvp in values)
                    logs += "   " + kvp.Key.Item1.Name + "." + kvp.Key.Item2 + " = " + toStr(kvp.Value);
                logs += " }" + nl;
            }
            foreach (var a in Removed)
            {
                var values = GetAllCurrentValues(a);
                logs += "Deleting " + typeof(TItem).Name + " with Id " + a.Id.ToString() + " { " + nl;
                foreach (var kvp in values)
                    logs += "   " + kvp.Key.Item1.Name + "." + kvp.Key.Item2 + " = " + toStr(kvp.Value);
                logs += " }" + nl;
            }
            foreach (var a in EditingSet)
            {
                if (IsDirty(a) && !Added.Contains(a) && !Removed.Contains(a))
                {
                    logs += " For " + a.TypedId.ToSmartString() + nl;
                    var changes = GetAllChanges(a);
                    foreach (var grp in changes.GroupBy(change => change.Key.Item1))
                    {
                        var interfaceType = grp.Key;
                        foreach (var change in grp)
                        {
                            var rProp = interfaceType.GetProperty(change.Key.Item2);
                            Debug.Assert(rProp != null, nameof(rProp) + " != null");
                            var oldValue = rProp.GetValue(GetEditedObject(a));
                            logs += "  Changing " + grp.Key.Name + "." + change.Key.Item2 + " = " + toStr(change.Value) + " (instead \"" + toStr(oldValue) + "\")" + nl;
                        }
                    }
                }
            }
            return logs.Length > 0 ? logs.Remove(logs.Length - 2) : logs;
        }

        public virtual IItem AddNew(TItem newItem = null)
        {
            newItem = newItem ?? CreateNew();
            var editor = CreateEditing(newItem);

            if (!AllowDuplicateItemWithSameTypedId)
            {
                var index = EditingSet.FindIndex(item => Equals(item.TypedId, editor.TypedId) && !ReferenceEquals(item, editor));
                if (index >= 0)
                    throw new DuplicateItemAddedException("Another identical item exist", EditingSet[index]);
            }
            Added.Add(editor);
            EditingSet.Add(editor);
            return editor;
        }
        public class DuplicateItemAddedException : UserUnderstandableException
        {
            public IItem Item { get; }
            public DuplicateItemAddedException(string msg, IItem alreadyExistingItem)
                : base(msg, null)
            {
                Item = alreadyExistingItem;
            }
        }
        /// <summary>
        /// Create a new business object where all properties must be just _technically_ valid
        /// (regarding the business interpretation of the properties and/or your UML model)
        /// One of the most important thing is to make sure non nullable properties are filled.
        /// If a value is taken at random to fullfill this condition,
        /// You don't have to care yet, it will be checked at runtime later (by you), in ValidateProperty method
        /// </summary>
        /// <returns></returns>
        protected abstract TItem CreateNew();

        public override object CloneObject(object item)
        {
            var typedItem = (EditableSubstitute)(object)(IItem)item;
            var clone = AddNew();
            var substitute = (EditableSubstitute)(object)clone;
            substitute.CopyFrom(typedItem);
            return clone;
        }

        public virtual void Remove(IItem item)
        {
            if (Added.Contains(item))
            {
                AllowEditingSetAction(() =>
                {
                    Added.Remove(item);
                    EditingSet.Remove(item);
                });
                ItemReseted?.Invoke(this, item);
            }
            else if (AllowRemove)
                Removed.Add(item); // flag the object to be shown as deleted
            else
                throw new UserUnderstandableException("Removing is forbidden!", null);
        }

        public void ResetEditsOrRemove(IItem item)
        {
            if (Added.Contains(item))
                Remove(item);
            else
            {
                // unflag the object to be shown as not deleted (if it was in this list)
                Removed.Remove(item);
                ResetEdits(item);
            }
        }

        protected IItem CreateEditing(TItem item) { return EffectiveTypeBuilder.CreateSubstitueObjectToEdit(this, item, BuildClosedId, AllowAccessOnlyToMainInterface); }

        public virtual void NotifyDependentSetHaveChanged(EditableSet<TItem, IItem> dependentSet = null)
        {
            Refresh();
        }
        public override int Refresh(IProgress<string> pr = null)
        {
            var nbMerge = 0;
            _State = eSetState.Loading;
            AllowEditingSetAction(
                () => LockEventsOnContainersWhile(
                    () => nbMerge = DoRefresh(pr ?? new Progress<string>())));
            _State = eSetState.Loaded;
            return nbMerge;
        }
        int DoRefresh(IProgress<string> pr)
        {
            var editingSetEmpty = EditingSet.Count == 0;
            if (editingSetEmpty)
                SimplifyChangesOnId();
            var freshItems = GetFreshItems(pr);
            LastRefreshDate = DateTime.Now;
            var matches = _originalSet.MatchById(freshItems, elt => elt.TypedId).ToList();
            var editingSetByOriginalId = EditingSet.ToDictionary(item => GetEditedObject(item).TypedId);
            var removedSetByOriginalId = Removed.ToDictionary(item => GetEditedObject(item).TypedId);
            var nbMerge = 0;
            // ReSharper disable PossibleNullReferenceException
            foreach (var m in matches)
            {
                if (m.Left == null) // fresh item is a new one !
                {
                    _originalSet.Add(m.Right);
                    // Does current user wanted to create the same ?
                    var similar = Added.FirstOrDefault(a => Equals(a.TypedId, m.Right.TypedId));
                    if (similar != null) // yes
                    {
                        // replace the user's current by the real one
                        Added.Remove(similar);
                        if (!EditingSet.Contains(similar))
                            EditingSet.Add(similar);
                        Debug.Assert(GetEditedObject(similar) == null);
                        (similar as EditableSubstitute).EditedObject = m.Right;
                    }
                    else
                        EditingSet.Add(CreateEditing(m.Right));
                    ++nbMerge;
                }
                else if (m.Right == null) // current edited item has been removed
                {
                    _originalSet.Remove(m.Left);
                    var similar = removedSetByOriginalId.TryGetValueClass(m.Left.TypedId);
                    if (similar != null)
                    {
                        Removed.Remove(similar);
                        EditingSet.Remove(similar);
                    }
                    else
                    {
                        similar = editingSetByOriginalId.TryGetValueClass(m.Left.TypedId);
                        if (similar != null &&  // item could have been directly removed from EditingSet
                            (similar as EditableSubstitute).IsDirty)
                        {
                            (similar as EditableSubstitute).EditedObject = null;
                            Added.Add(similar);
                        }
                        else
                            EditingSet.Remove(similar);
                    }
                    ++nbMerge;
                }
                else // maybe changed
                {
                    var similar = editingSetByOriginalId.TryGetValueClass(m.Left.TypedId);
                    if (similar != null)
                    {
                        Debug.Assert(similar != null);
                        m.Left.CopyFrom(m.Right);
                        (similar as EditableSubstitute).CleanDictionary();
                        m.Left.CopyFrom(m.Right);
                    }
                }
            }
            // ReSharper restore PossibleNullReferenceException
            if (nbMerge > 0 && !editingSetEmpty)
                SimplifyChangesOnId();
            return nbMerge;
        }


        // Sometimes the Pk are editable by user (for example when it is a business name)
        // This method try to merge edit change and generate equivalent change so that no object has a PK that has changed
        // Only instance on PK have a PK that have changed (null => value generated automaticaly or entered by user if PK is editable)
        public /*protected*/ override void SimplifyChangesOnId()
        {
            Debug.Assert(ValidateItems().Count == 0, "Ne pas lancer cet algo sinon car les erreurs pourraient thrower en plein milieu de l'algo !");
            AllowEditingSetAction(() => SimplifyChangesOnId(EditingSet, Added, Removed, CreateNew));
        }
        // this method must be called inside AllowEditingSetAction
        static void SimplifyChangesOnId(IList<IItem> editingSet, IList<IItem> addedSet, IList<IItem> removedSet, Func<TItem> createNew)
        {
            // ReSharper disable PossibleNullReferenceException
            foreach (var item in editingSet)
                (item as EditableSubstitute).CleanDictionary();

            foreach (var r in removedSet)
                if (!Equals(r.TypedId, (r as EditableSubstitute).RealTypedId))
                {
                    // This line is not possible
                    //r.TypedId = (r as EditableSubstitute).RealTypedId;
                    // So we do this...
                    (r as EditableSubstitute).ResetEdits();
                }
            // ReSharper restore PossibleNullReferenceException

            // simplify edits of user so technically it is more simpler
            // It may be more complicated for user if edited object are displayed to user after this simplification
            // but the changes are equivalent
            int fixDoneInThisLoop;
            do
            {
                fixDoneInThisLoop = 0;

                // Annihilate Added And Removed instances (if user delete object with closedId 42 and create another one with id 42 ==> It's equivalent to just change the original just changed the original !)
                foreach (var a in addedSet.ToList())
                {
                    // We should not use a.RealTypedId because we dont care if user change again after initial value
                    // So we only use a.TypedId
                    var removed = removedSet.FirstOrDefault(r => Equals(GetEditedObject(r).TypedId, a.TypedId));
                    if (removed != null)
                    {
                        // So we will keep the removed real object instance, and the new wrapper instance
                        // because these instance could be referenced by developper in complex scenario
                        removed.CopyFrom(a); // Copy (backup) all values the user want on removed instance
                        SetEditedObject(a, GetEditedObject(removed)); // Inject the old object on the new wrapper
                        a.CopyFrom(removed); // Now "a" is ok
                        // Clean list
                        addedSet.Remove(a); // a is not anymore added but changed form original object
                        removedSet.Remove(removed); // removed is a old duplicate of a, so we remove it from both list
                        editingSet.Remove(removed);

                        ++fixDoneInThisLoop;
                    }
                }

                // Find the item that have a changed id
                foreach (var changed in WithIdEdited(editingSet.Except(addedSet).Except(removedSet)).ToList())
                {
                    // if "changed" had a PK "X" and now has a PK "Y"
                    // and "added" has a PK "X"
                    // it's like changing X, and create Y
                    var added = addedSet.FirstOrDefault(a => Equals(a.TypedId, GetEditedObject(changed).TypedId));
                    if (added != null)
                    {
                        var tmp = createNew();
                        tmp.CopyFrom(added); // backup field of  X
                        added.CopyFrom(changed); // X <= Y
                        changed.CopyFrom(tmp); // Y <= X(backup)
                        // added instance has no backing instance so there is nothing to do more than that
                        ++fixDoneInThisLoop;
                    }
                    else
                    {
                        // if "changed" had a PK X and now has a PK Y
                        // and "removed" an instance with PK "Y"
                        // it's like removing X and changing Y
                        var removed = removedSet.FirstOrDefault(r => //Equals(r.TypedId, r.RealTypedId) && // make sure the removed has not been modified before
                                                                  Equals(GetEditedObject(r).TypedId, changed.TypedId));
                        if (removed != null)
                        {
                            var tmp = createNew();
                            tmp.CopyFrom(changed); // Backup Y fields as user want them
                            // swap editedobject instance
                            SetEditedObject(tmp, GetEditedObject(removed));
                            SetEditedObject(removed, GetEditedObject(changed));
                            SetEditedObject(changed, GetEditedObject(tmp));
                            removed.CopyFrom(tmp); // Restore value of user

                            removedSet.Remove(removed);  // swap roles
                            removedSet.Add(changed);
                            ++fixDoneInThisLoop;
                        }
                        else
                        {
                            // if "changed" had a PK X and now has a PK Y
                            // if "otherChanged" had a PK Y and now has a PK Z
                            // we just need to swap wrapper and changes
                            // Note : Z can be X or not, we use Z and not X i nthis case to handle case like thnaks to multipass :
                            //  changed X => Y, otherChanged Y => Z, otherOtherChanged  Z => X (ie : any cycles of change on Id)
                            var otherChanged = WithIdEdited(  // Here we check that Z != Y. This condition should be tru every time but we reuse it to prevent infinite recursion
                                                    editingSet.Except(removedSet).Except(addedSet))
                                                    .FirstOrDefault(oc => Equals(GetEditedObject(oc).TypedId, changed.TypedId)); // changed.Y == otherChanged.originalY
                            if (otherChanged != null)
                            {
                                var tmp = createNew();
                                // Saved changed editedobject
                                SetEditedObject(tmp, GetEditedObject(changed));

                                // Set edited object of changed to otherChanged.EditedbOject
                                tmp.CopyFrom(changed); // save new Y data
                                SetEditedObject(changed, GetEditedObject(otherChanged));
                                changed.CopyFrom(tmp); // restore new Y data edit on Y original object

                                // Set edited object of otherChanged to changed.EditedbOject
                                tmp.CopyFrom(otherChanged); // save new Z data
                                SetEditedObject(otherChanged, GetEditedObject(tmp));
                                otherChanged.CopyFrom(tmp); // save new Z data (now, if Z was X, change X => Z should disappears)

                                ++fixDoneInThisLoop;
                            }
                        }
                    }
                }
            } while (fixDoneInThisLoop > 0);

            DebugTools.CostlyAssert(editingSet.Except(removedSet).Except(addedSet).All(c => Equals(c.TypedId, GetEditedObject(c).TypedId)));
            DebugTools.CostlyAssert(removedSet.All(r => Equals(r.TypedId, GetEditedObject(r).TypedId)));
        }


        // Allow to mask the edited property
        protected internal virtual void OnGettingValue(IItem item, Type interfaceType, string propertyName, ref object value)
        {
        }

        public override event Model.PropertyChangingEventHandler PropertyChanging;

        // Prevent editing by returning the reason to forbid the value in item property
        protected internal virtual string OnPropertyAssigning(IItem item, Type interfaceType, string propertyName, object value)
        {
            if (PropertyChanging != null)
            {
                var p = interfaceType.GetProperty(propertyName);
                Debug.Assert(p != null, nameof(p) + " != null");
                var e = new Model.PropertyChangingEventArgs(interfaceType.FullName + "." + propertyName, p.GetValue(item), value);
                try
                {
                    PropertyChanging?.Invoke(item, e);
                }
                catch (UserUnderstandableException ex)
                {
                    return ExceptionManager.Instance.Format(ex, true, false);
                }
            }
            return null;
        }

        protected internal virtual void OnPropertyAssigned(IItem item, Type interfaceType, string propertyName)
        {
        }
        public override event PropertyHasChangedEventHandler PropertyChanged;
        internal void RaisePropertyChanged(IItem item, Type interfaceType, string propertyName, object previousValue)
        {
            if (PropertyChanged != null)
            {
                var e = new PropertyHasChangedEventArgs(interfaceType.FullName + "." + propertyName, previousValue);
                PropertyChanged?.Invoke(item, e);
            }
        }

        protected internal virtual string ValidateProperty(IItem item, Type interfaceType, PropertyInfo property) { return null; }
        public virtual Dictionary<Type, Dictionary<PropertyInfo, string>> ValidateProperties(IItem item)
        {
            var res = new Dictionary<Type, Dictionary<PropertyInfo, string>>();
            if (!Removed.Contains(item)) // ReSharper disable once PossibleNullReferenceException
                foreach (var typeProps in (item as EditableSubstitute).AllPropertiesEditable)
                {
                    Dictionary<PropertyInfo, string> validations = null;
                    foreach (var prop in typeProps.Value)
                    {
                        string error = ValidateProperty(item, typeProps.Key, prop.Value);
                        if (!string.IsNullOrWhiteSpace(error))
                        {
                            validations = validations ?? new Dictionary<PropertyInfo, string>();
                            validations.Add(prop.Value, error);
                        }
                    }
                    if (validations != null)
                        res.Add(typeProps.Key, validations);
                }
            return res;
        }

        public virtual string PreValidateItem(IItem item)
        {
            return null;
        }
        string ISetValidator.PreValidateItem(object item) { return PreValidateItem((IItem)item); }

        public virtual string ValidateItem(IItem item)
        {
            return ValidateProperties(item).Any() ? "Please correct values" : null;
        }
        string ISetValidator.ValidateItem(object item) { return ValidateItem((IItem)item); }

        public Dictionary<IItem, string> ValidateItems()
        {
            return ISetValidator_Extensions.ValidateItems(this, EditingSet.Where(item => !Removed.Contains(item)).ToList());
        }

        public sealed override string ValidateSet(IProgress<string> pr = null)
        {
            return ValidateSet(EditingSet, pr);
        }
        protected internal virtual string ValidateSet(IEnumerable<IItem> set, IProgress<string> pr = null)
        {
            return null;
        }
        string ISetValidator       .ValidateSet(IReadOnlyCollection<object> set,                             IProgress<string> pr/* = null*/) {            return ValidateSet(set.Cast<IItem>(), pr); }
        bool   ISetValidator<IItem>.ValidateSet(IReadOnlyCollection<IItem>  set, out string errorOrWarnings, IProgress<string> pr/* = null*/) { errorOrWarnings = ValidateSet(set, pr); return string.IsNullOrEmpty(errorOrWarnings); }
        bool   ISetValidator       .ValidateSet(IReadOnlyCollection<object> set, out string errorOrWarnings, IProgress<string> pr/* = null*/) { errorOrWarnings = ValidateSet(set.Cast<IItem>(), pr); return string.IsNullOrEmpty(errorOrWarnings); }

        public /*protected*/  override void ValidateBeforeCommit(IProgress<string> pr = null)
        {
            var errors = ValidateItems();
            if (errors.Count > 0)
                throw new UserUnderstandableException("There are still some error:" + errors.Select(error => Environment.NewLine + " - Item with Id " + error.Key.Id.ToString() + ": " + error.Value).Join(""), null);

            string globalError = ISetValidator_Extensions.ValidateAll(this, EditingSet.Where(item => !Removed.Contains(item)).ToList(), EditingSet, pr);
            if (!string.IsNullOrWhiteSpace(globalError))
                throw new UserUnderstandableException(globalError, null);
        }

        public override event EventHandler Commited;
        public override void Commit(SynchronizationContext context = null, IProgress<string> pr = null)
        {
            ValidateBeforeCommit(pr);
            Action applyOnModel = ApplyToDatabaseAndModel(pr);
            if (context == null)
            {
                applyOnModel();
                Commited?.Invoke(this, EventArgs.Empty);
            }
            else
                context.Post(_ => { applyOnModel(); Commited?.Invoke(this, EventArgs.Empty); }, null);
        }
        // TODO : to rewrite
        //protected override void ApplyToDatabaseInTransaction(SoftwareTransaction tran = null)
        //{
        //    var nbMerge = Refresh();
        //    if (nbMerge > 0)
        //        throw new BusinessException("Please check data, someone changed them before you !", null);
        //    // Important Note :
        //    //   Keep in mind that this transaction can be real (ie : the commit will really happens at the end of the using)
        //    //   or virtual (ie : an outer transaction exists, so the commit will happen later)
        //    using (tran = tran ?? OpenSoftwareTransaction())
        //    {
        //        Action applyOnModel = ApplyToDatabaseAndModel();
        //        // So this may seems idiot but it is not
        //        tran.OnCommit += applyOnModel;
        //        tran.Commit();
        //    }
        //}
        protected override SoftwareTransaction OpenSoftwareTransaction()
        {
            return null;
        }
        protected override Action ApplyToDatabaseAndModel(IProgress<string> pr = null)
        {
            var mergeRemovedToModel = ApplyRemovedItemToDatabase(pr);
            var mergeChangedToModel = ApplyChangedItemToDatabase(pr);
            var mergeCreatedToModel = ApplyCreatedItemToDatabase(pr);
            return () => LockEventsOnContainersWhile(() =>
            {
                mergeRemovedToModel?.Invoke();
                mergeChangedToModel?.Invoke();
                mergeCreatedToModel?.Invoke();
            });
        }

        protected static void SetEditedObject(IItem editableSubstitute, TItem editedObject)
        {
            SetEditedObject(editableSubstitute as EditableSubstitute, editedObject);
        }
        protected static void SetEditedObject(EditableSubstitute editableSubstitute, object editedObject)
        {
            editableSubstitute.EditedObject = editedObject;
        }
        protected static TItem GetEditedObject(IItem editableSubstitute)
        {
            return (TItem)GetEditedObject(editableSubstitute as EditableSubstitute);
        }
        protected static object GetEditedObject(EditableSubstitute editableSubstitute)
        {
            return editableSubstitute.EditedObject;
        }

        public void AllowEditingSetAction(Action action)
        {
            var allowNew = AllowNew;
            var allowRemove = AllowRemove;
            var allowEdit = AllowEdit;
            AllowNew = true;
            AllowRemove = true;
            AllowEdit = true;
            try
            {
                action();
            }
            finally
            {
                AllowNew = allowNew;
                AllowRemove = allowRemove;
                AllowEdit = allowEdit;
            }
        }

        protected void LockEventsOnContainersWhile(Action action)
        {
            _originalSet.WithListChangedEventsDisabled(
                () => EditingSet.WithListChangedEventsDisabled(
                    () => Removed.WithListChangedEventsDisabled(
                        () => Added.WithListChangedEventsDisabled(action))));
        }

        protected TKey[] GetDuplicates<TKey>(IEnumerable<IItem> items, Func<IItem, TKey> getUniqueKey)
        {
            return items.Except(Removed)
                        .GroupBy(getUniqueKey)
                        .Where(grp => grp.Count() >= 2)
                        .Select(grp => grp.Key)
                        .ToArray();
        }

        protected string CheckUnicityOnEditingSet<TKey>(IEnumerable<IItem> items, Func<IItem, TKey> geValueThatShouldBeUnique, string businessErrorMsg, Func<TKey, string> toString = null)
        {
            var duplicates = GetDuplicates(items, geValueThatShouldBeUnique);
            if (duplicates.Length == 0)
                return null;
            return businessErrorMsg + Environment.NewLine +
                        (duplicates.Length == 1 ? "This value is used several times: "
                                                : "These values are used multiple times: ") + Environment.NewLine +
                         duplicates.Select(key => toString == null ? key == null ? "<EMPTY>" : key.ToString() : toString(key))
                                   .Join(Environment.NewLine);
        }

    }
}
