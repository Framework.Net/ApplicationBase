﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace TechnicalTools.UI.EditableLayer
{
    public interface IBusinessObjectEditableSet : IEditableSet
    {
        Type GetTItemType();
        Type GetIItemType();

        IEnumerable<EditableSubstitute> EditingSet { get; }
        IEnumerable<EditableSubstitute> Removed    { get; }
        IEnumerable<EditableSubstitute> Added      { get; }
    }
    // Class based on implementation of EditableDTOSet
    public abstract class BusinessObjectEditableSet<TItem, IItem> : EditableSet<TItem, IItem>, IBusinessObjectEditableSet
        where TItem : class, IItem, IHasTypedIdReadable, ICloneable, new()
        where IItem : class, ICopyable<IItem>, IHasTypedIdReadable
    {
        Type IBusinessObjectEditableSet.GetTItemType() { return typeof(TItem); }
        Type IBusinessObjectEditableSet.GetIItemType() { return typeof(IItem); }
        IEnumerable<EditableSubstitute> IBusinessObjectEditableSet.EditingSet { get { return EditingSet.Cast<EditableSubstitute>(); } }
        IEnumerable<EditableSubstitute> IBusinessObjectEditableSet.Removed    { get { return Removed.Cast<EditableSubstitute>(); } }
        IEnumerable<EditableSubstitute> IBusinessObjectEditableSet.Added      { get { return Added.Cast<EditableSubstitute>(); } }

        protected internal new Func<IItem, IIdTuple>     BuildClosedId                  { get { return base.BuildClosedId; } }
        protected internal new bool                      AllowAccessOnlyToMainInterface { get { return base.AllowAccessOnlyToMainInterface; } }
        protected internal new FixedBindingList<TItem>   OriginalSet                    { get { return base.OriginalSet; } }

        readonly IList<TItem> _editedDataSource;
        readonly Func<IItem, IIdTuple> _buildClosedIdFromInterface;
        readonly Func<TItem, IIdTuple> _buildClosedIdFromRealType;

        protected BusinessObjectEditableSet(IList<TItem> editedDataSource, bool itemPKIsEditableOrPartiallyEditable,
                                            Func<IItem, IIdTuple> buildClosedIdFromInterface, Func<TItem, IIdTuple> buildClosedIdFromRealType,
                                            bool allowAccesOnlyToMainInterface = false, EditableSubstituteBuilder<TItem, IItem> typebuilder = null)
             : base(itemPKIsEditableOrPartiallyEditable ? buildClosedIdFromInterface : iitem => buildClosedIdFromRealType(GetEditedObject(iitem)), allowAccesOnlyToMainInterface, typebuilder ?? new EditableSubstituteBuilder<TItem, IItem>(), true)
        {
            _buildClosedIdFromInterface = buildClosedIdFromInterface;
            _buildClosedIdFromRealType = buildClosedIdFromRealType;
            _editedDataSource = editedDataSource;
            Refresh();
        }

        protected override List<TItem> GetFreshItems(IProgress<string> pr)
        {
            return _editedDataSource.ToList();
        }

        protected internal override string ValidateProperty(IItem item, Type interfaceType, PropertyInfo property)
        {
            if (Removed.Contains(item))
                return null;
            // Redirect to another class for now, to make code editable at runtime
            return PropertyValidator.ValidateProperty(typeof(TItem), typeof(IItem), item, interfaceType, property);
        }
        [EditorBrowsable(EditorBrowsableState.Never)]
        internal string _ValidateProperty(IItem item, Type interfaceType, PropertyInfo property)
        {
            return ValidateProperty(item, interfaceType, property);
        }
        [EditorBrowsable(EditorBrowsableState.Never)]
        internal string _ValidateSet(IEnumerable<IItem> set, IProgress<string> pr/* = null*/)
        {
            return ValidateSet(set, pr);
        }

        // This method just save all the list without looking for dependent data
        public override void Commit(SynchronizationContext context = null, IProgress<string> pr = null)
        {
            base.Commit(context, pr);
        }

        protected override SoftwareTransaction OpenSoftwareTransaction()
        {
            return new SoftwareTransaction();
        }
        public /*protected*/  override Action ApplyRemovedItemToDatabase(IProgress<string> pr = null)
        {
            var actions = new List<Action>();
            foreach (var r in Removed)
                for (int i = _editedDataSource.Count - 1; i >= 0; --i)
                {
                    var item = GetEditedObject(r);
                    if (_buildClosedIdFromRealType(_editedDataSource[i]).Equals(_buildClosedIdFromRealType(item)))
                    {
                        _editedDataSource.RemoveAt(i);

                        actions.Add(() =>
                        {
                            _originalSet.Remove(item);
                            Removed.Remove(r);
                            EditingSet.Remove(r);
                        });
                        break;
                    }
                }
            return () => actions.ForEach(a => a());
        }
        public /*protected*/  override Action ApplyCreatedItemToDatabase(IProgress<string> pr = null)
        {
            var actions = new List<Action>();
            foreach (var a in Added)
            {
                var item = GetEditedObject(a);
                if (item == null)
                {
                    DebugTools.Break("Should never happen, rest of code is just a secure way to avoid bug");
                    item = new TItem();
                }
                item.CopyFrom(a);
                _editedDataSource.Add(item);
                actions.Add(() =>
                {
                    SetEditedObject(a, item); //we set object so if there is technical id (auto incremented, we have it !)
                    _originalSet.Add(item);
                    Added.Remove(a);
                });
            }

            // Instance with a PK that has changed
            var newFromChanged = EditingSet.Except(Added).Except(Removed).Where(c => !Equals(GetEditedObject(c).TypedId, c.TypedId)).ToList();
            foreach (var changed in newFromChanged)
            {
                var item = GetEditedObject(changed);
                var newItem = (TItem)item.Clone();
                newItem.CopyFrom(changed);
                _editedDataSource.Add(newItem);
                actions.Add(() =>
                {
                    item.CopyFrom(newItem);
                });
            }

            return () => actions.ForEach(a => a());
        }
        public /*protected*/  override Action ApplyChangedItemToDatabase(IProgress<string> pr = null)
        {
            var actions = new List<Action>();
            foreach (var c in EditingSet.Except(Added).Except(Removed).Where(c => Equals(GetEditedObject(c).TypedId, c.TypedId)).ToList())
            {
                if (IsDirty(c))
                {
                    var item = GetEditedObject(c);
                    var clone = (TItem)item.Clone();
                    clone.CopyFrom(c);

                    for (int i = 0; i < _editedDataSource.Count; ++i)
                        if (_buildClosedIdFromRealType(_editedDataSource[i]).Equals(_buildClosedIdFromRealType(item)))
                        {
                            _editedDataSource[i].CopyFrom(clone);
                            break;
                        }
                    actions.Add(() =>
                    {
                        item.CopyFrom(clone);
                    });
                }
            }
            return () => actions.ForEach(a => a());
        }

    }


    static class PropertyValidator
    {
        public static string ValidateProperty(Type titem, Type iitem, object item, Type interfaceType, PropertyInfo property)
        {
            var value = property.GetValue(item);

            var propWithAtt = titem.GetProperty(property.Name);
            Debug.Assert(propWithAtt != null, nameof(propWithAtt) + " != null");

            if (value == null && !(propWithAtt.GetCustomAttribute<RequiredAttribute>()?.IsValid(value) ?? true))
                return "The " + property.Name + " field is required!";

            var att = propWithAtt.GetCustomAttribute<StringLengthAttribute>();
            if (att != null)
            {
                if (value == null)
                    if (att.MinimumLength > 0)
                        return "The " + property.Name + " field is required!";
                    else
                        return null;

                if (value is string)
                {
                    if ((value as string).Length < att.MinimumLength)
                        return "Length must be at least " + att.MinimumLength + " characters!";
                    else if ((value as string).Length > att.MaximumLength)
                        return "Length must be " + att.MaximumLength + " characters top!";
                }
                else if (value is Array)
                {
                    if ((value as Array).Length < att.MinimumLength)
                        return "Length must be at least " + att.MinimumLength + " characters!";
                    else if ((value as Array).Length > att.MaximumLength)
                        return "Length must be " + att.MaximumLength + " characters top!";
                }
                else
                {
                    DebugTools.Should(false, "Does not know how to handle this !");
                }
            }

            // Autre attribut a gérer un jour
            // (from https://documentation.devexpress.com/#WPF/CustomDocument9770)
            //System.ComponentModel.DataAnnotations.CustomValidationAttribute Uses a custom method for validation.
            //System.ComponentModel.DataAnnotations.DataTypeAttribute Specifies a particular type of data, such as an e - mail address or phone number.
            //System.ComponentModel.DataAnnotations.EnumDataTypeAttribute Ensures that the value exists in an enumeration.
            //System.ComponentModel.DataAnnotations.RangeAttribute    Designates minimum and maximum constraints.
            //System.ComponentModel.DataAnnotations.RegularExpressionAttribute    Uses a regular expression to determine valid values.

            // Old code
            //try
            //{
            //    // Make setter validate technical constraint (length, nullability etc)
            //    property.SetValue(new TItem(), property.GetValue(item));
            //    return null;
            //}
            //catch (Exception ex)
            //{
            //    while (ex is TargetInvocationException)
            //        ex = (ex as TargetInvocationException).InnerException;
            //    return ex.Message;
            //}

            return null;
        }
    }
}
