﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace TechnicalTools.UI.EditableLayer
{
    public abstract class EditableSubstitute : IHasTypedIdReadable, ICopyable<EditableSubstitute>
    {
        IHasTypedIdReadable _obj;
        readonly Dictionary<Type, Dictionary<string, object>> _allChanges = new Dictionary<Type, Dictionary<string, object>>();

        protected EditableSubstitute(IHasTypedIdReadable obj)
        {
            _obj = obj;
        }

                 IIdTuple IHasClosedIdReadable.Id          { get { return GetClosedId(); } }
        public   ITypedId                      TypedId     { get { return _obj.MakeTypedId(GetClosedId()); } }
                 ITypedId IHasTypedIdReadable .MakeTypedId(IIdTuple id) { return _obj.MakeTypedId(id); }
        public   ITypedId                      RealTypedId { get { return _obj.TypedId; } }
        protected internal abstract IIdTuple GetClosedId();

        protected          abstract Type MainEditableInterface { get; }
        protected          abstract Dictionary<Type, Dictionary<string, PropertyInfo>> AllProperties { get; }
        protected internal abstract Dictionary<Type, Dictionary<string, PropertyInfo>> AllPropertiesEditable { get; }
        protected internal abstract Dictionary<Type, Dictionary<string, PropertyInfo>> RecursivePropertiesEditable { get; }

        protected internal object EditedObject { get { return _obj; } set { _obj = (IHasTypedIdReadable)value; } }

        protected internal virtual void Set(string propertyName, object value, Type interfaceType = null)
        {
            interfaceType = interfaceType ?? MainEditableInterface;

            Debug.Assert(interfaceType.IsAssignableFrom(MainEditableInterface));
            Debug.Assert(AllPropertiesEditable[interfaceType].ContainsKey(propertyName), "Only editable property can be changed !");

            bool same = Equals(GetReal(propertyName, interfaceType), value);
            if (!_allChanges.TryGetValue(interfaceType, out var values))
            {
                if (!same)
                {
                    _allChanges[interfaceType] = values = new Dictionary<string, object>();
                    values[propertyName] = value;
                }
            }
            else if (same)
                values.Remove(propertyName);
            else
                values[propertyName] = value;
        }
        protected internal virtual object Get(string propertyName, Type interfaceType = null)
        {
            interfaceType = interfaceType ?? MainEditableInterface;
            Debug.Assert(interfaceType.IsAssignableFrom(MainEditableInterface));
            if (_allChanges.TryGetValue(interfaceType, out var values))
                if (values.TryGetValue(propertyName, out var value))
                    return value;
            return GetReal(propertyName, interfaceType);
        }

        protected object GetReal(string propertyName, Type interfaceType = null)
        {
            interfaceType = interfaceType ?? MainEditableInterface;
            return AllProperties[interfaceType][propertyName].GetValue(_obj);
        }

        protected internal virtual bool IsDirty
        {
            get
            {
                CleanDictionary();
                return _allChanges.Values.Any(changes => changes.Any())
                    || IsAnyCompositeDirty;
            }
        }
        protected internal virtual bool IsAnyCompositeDirty
        {
            get
            {
                return RecursivePropertiesEditable.Any(inter => inter.Value.Any(kvp => ((EditableSet)kvp.Value.GetValue(_obj)).HasChanges));
            }
        }
        protected internal virtual bool IsPropertyDirty(string propertyName, Type interfaceType = null)
        {
            interfaceType = interfaceType ?? MainEditableInterface;
            return _allChanges.TryGetValue(interfaceType, out var changes) &&
                   changes.TryGetValue(propertyName, out var value) &&
                   !Equals(GetReal(propertyName, interfaceType), value);
        }

        protected internal virtual IReadOnlyDictionary<string, object> GetChanges(Type interfaceType = null)
        {
            interfaceType = interfaceType ?? MainEditableInterface;
            return _allChanges[interfaceType];
        }
        protected internal virtual IReadOnlyDictionary<Tuple<Type, string>, object> GetAllChanges()
        {
            var res = new Dictionary<Tuple<Type, string>, object>();
            foreach (var typeChanges in _allChanges)
                foreach (var propertyChange in typeChanges.Value)
                    res.Add(Tuple.Create(typeChanges.Key, propertyChange.Key), propertyChange.Value);
            return res;
        }
        protected internal virtual bool CleanDictionary()
        {
            bool hasBeenCleaned = false;
            foreach (var change in GetAllChanges())
                if (!IsPropertyDirty(change.Key.Item2, change.Key.Item1))
                {
                    _allChanges[change.Key.Item1].Remove(change.Key.Item2);
                    hasBeenCleaned = true;
                }
            return hasBeenCleaned;
        }
        protected internal virtual void ResetEdits()
        {
            _allChanges.Clear();
        }

        protected internal virtual IReadOnlyDictionary<Tuple<Type, string>, object> GetAllCurrentValues()
        {
            var res = new Dictionary<Tuple<Type, string>, object>();
            foreach (var typeProperties in AllPropertiesEditable)
                foreach (var property in typeProperties.Value)
                    res.Add(Tuple.Create(typeProperties.Key, property.Key), Get(property.Key, typeProperties.Key));
            return res;
        }

        public void CopyFrom(EditableSubstitute source)
        {
            var isCopying = IsCopying;
            IsCopying = true;
            try
            {
                Debug.Assert(GetType() == source.GetType());
                foreach (var typeProperties in AllPropertiesEditable)
                    foreach (var property in typeProperties.Value)
                        Set(property.Key, source.Get(property.Key, typeProperties.Key), typeProperties.Key);
            }
            finally
            {
                IsCopying = isCopying;
            }
        }
        protected bool IsCopying { get; private set; }
        public void CopyFrom(ICopyable source) { CopyFrom((EditableSubstitute)source); }
    }
    public abstract class EditableSubstitute<TItem, IItem> : EditableSubstitute, IDataErrorInfo // IDataErrorInfo because UI layer are aware of this interface (example : DevExpress)
        where TItem : class, IItem, IHasTypedIdReadable
        where IItem : class, ICopyable<IItem>, IHasTypedIdReadable
    {
        readonly EditableSet<TItem, IItem> _owner;
        //readonly Dictionary<Tuple<Type, string>, object> _indirectChanges = new Dictionary<Tuple<Type, string>, object>();

        protected override Type MainEditableInterface { get { return typeof(IItem); } }

        protected override Dictionary<Type, Dictionary<string, PropertyInfo>> AllProperties { get { return _AllProperties; } }
        static readonly Dictionary<Type, Dictionary<string, PropertyInfo>> _AllProperties = new[] { typeof(IItem) }.Concat(typeof(IItem).GetAllInheritedInterfaces())
                                                                                       .ToDictionary(t => t,
                                                                                                     t => t.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
                                                                                                           .ToDictionary(p => p.Name));

        protected internal override Dictionary<Type, Dictionary<string, PropertyInfo>> AllPropertiesEditable { get { return _AllPropertiesEditable; } }
        static readonly Dictionary<Type, Dictionary<string, PropertyInfo>> _AllPropertiesEditable = _AllProperties.ToDictionary(t => t.Key,
                                                                                                                                t => t.Value.Where(kvp => kvp.Value.CanWrite || typeof(EditableSet).IsAssignableFrom(kvp.Value.PropertyType))
                                                                                                                                            .ToDictionary(kvp => kvp.Key,
                                                                                                                                                          kvp => kvp.Value));

        protected internal override Dictionary<Type, Dictionary<string, PropertyInfo>> RecursivePropertiesEditable { get { return _RecursivePropertiesEditable; } }
        static readonly Dictionary<Type, Dictionary<string, PropertyInfo>> _RecursivePropertiesEditable = _AllProperties.ToDictionary(t => t.Key,
                                                                                                                                      t => t.Value.Where(kvp => typeof(EditableSet).IsAssignableFrom(kvp.Value.PropertyType))
                                                                                                                                                  .ToDictionary(kvp => kvp.Key,
                                                                                                                                                                kvp => kvp.Value));

        protected internal override void Set(string propertyName, object value, Type interfaceType = null)
        {
            interfaceType = interfaceType ?? MainEditableInterface;
            if (!IsCopying)
            {
                var reasonToNotDo = _owner.OnPropertyAssigning(this as IItem, interfaceType, propertyName, value);
                if (!string.IsNullOrWhiteSpace(reasonToNotDo))
                    throw new BusinessException(reasonToNotDo, null);
            }
            var previousValue = base.Get(propertyName, interfaceType);
            base.Set(propertyName, value, interfaceType);
            if (!IsCopying)
            {
                _owner.OnPropertyAssigned(this as IItem, interfaceType, propertyName);
                _owner.RaisePropertyChanged(this as IItem, interfaceType, propertyName, previousValue);
            }
        }
        protected internal override object Get(string propertyName, Type interfaceType = null)
        {
            interfaceType = interfaceType ?? MainEditableInterface;
            object value = base.Get(propertyName, interfaceType);
            _owner.OnGettingValue(this as IItem, interfaceType, propertyName, ref value);
            return value;
        }

        string IDataErrorInfo.Error { get { return _owner.ValidateItem(this as IItem); } }
        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                if (AllPropertiesEditable[MainEditableInterface].ContainsKey(columnName))
                    return _owner.ValidateProperty(this as IItem, MainEditableInterface, AllPropertiesEditable[MainEditableInterface][columnName]);
                else
                    foreach (var typeProperties in AllPropertiesEditable)
                        if (typeProperties.Value.ContainsKey(columnName))
                            return _owner.ValidateProperty(this as IItem, typeProperties.Key, typeProperties.Value[columnName]);
                return null;
            }
        }

        protected EditableSubstitute(EditableSet<TItem, IItem> owner, TItem obj)
            : base(obj)
        {
            _owner = owner;
        }

        protected internal new TItem EditedObject { get { return (TItem)base.EditedObject; } set { base.EditedObject = value; } }

        protected Exception InvalidPropertyWriteAccess(string propName)
        {
            return new Exception("Writting property \"" + propName + "\" is not allowed!");
        }


    }
    public abstract class EditableSubstitute<TSelf, TItem, IItem> : EditableSubstitute<TItem, IItem>
        where TSelf : EditableSubstitute<TSelf, TItem, IItem>, IItem
        where TItem : class, IItem, IHasTypedIdReadable
        where IItem : class, ICopyable<IItem>, IHasTypedIdReadable
    {
        protected EditableSubstitute(EditableSet<TItem, IItem> owner, TItem obj)
            : base(owner, obj)
        {
        }
    }
}
