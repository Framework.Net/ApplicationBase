﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TechnicalTools.Model
{
    public interface IContravariantLookup<in TKey>
    {
        bool Contains(TKey key);
    }
    public class DictionaryAsLookup<TKey, TValue> : IContravariantLookup<TKey>
    {
        readonly Dictionary<TKey, TValue> _dic;

        public DictionaryAsLookup(Dictionary<TKey, TValue> dic)
        {
            _dic = dic;
        }

        public bool Contains(TKey key)
        {
            return _dic.ContainsKey(key);
        }
    }
    public class LookupAsLookup<TKey, TValue> : IContravariantLookup<TKey>
    {
        readonly ILookup<TKey, TValue> _lu;

        public LookupAsLookup(ILookup<TKey, TValue> lu)
        {
            _lu = lu;
        }

        public bool Contains(TKey key)
        {
            return _lu.Contains(key);
        }

    }
}
