﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace TechnicalTools.Model
{
    // from http://blogs.microsoft.co.il/shimmy/2010/12/26/observabledictionarylttkey-tvaluegt-c/
    // TKey is always considered as aggregate !
    public class CompositeDictionary<TKey, TValue> : ObservableDictionary<TKey, TValue>, IPropertyNotifierObject
    {
        #region Constructors
        public CompositeDictionary()
        {
            _propertyNotifierObject = new PropertyNotifierObject(this);
            Init();
        }
        public CompositeDictionary(IDictionary<TKey, TValue> dictionary)
            : base(dictionary)
        {
            _propertyNotifierObject = new PropertyNotifierObject(this);
            Init();
        }
        public CompositeDictionary(IEqualityComparer<TKey> comparer)
            : base(comparer)
        {
            _propertyNotifierObject = new PropertyNotifierObject(this);
            Init();
        }
        public CompositeDictionary(int capacity)
            : base(capacity)
        {
            _propertyNotifierObject = new PropertyNotifierObject(this);
            Init();
        }
        public CompositeDictionary(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer)
            : base(dictionary, comparer)
        {
            _propertyNotifierObject = new PropertyNotifierObject(this);
            Init();
        }
        public CompositeDictionary(int capacity, IEqualityComparer<TKey> comparer)
            : base(capacity, comparer)
        {
            _propertyNotifierObject = new PropertyNotifierObject(this);
            Init();
        }
        #endregion

        void Init()
        {
            ManageElementImplementingIPropertyNotifierObject();
            RememberDeletedObjectsFeature_Initialize();
        }

        public Func<IEnumerable<KeyValuePair<TKey, TValue>>> LoadItems { get; set; }
        public bool HasBeenLoaded { get; private set; }

        public void AddRange(IEnumerable<KeyValuePair<TKey , TValue>> values)
        {
            base.AddRange(values.ToDictionary(v => v.Key, v => v.Value));
        }

        public void Reset()
        {
            HasBeenLoaded = false;
            _DeletedObjects.Clear();
            base.Clear();
            _DeletedObjects.Clear();
            InitializeState(eState.New);
        }

        public void Reload()
        {
            if (LoadItems != null)
            {
                LockWhileUpdatingWith(DoReload);
                UpdateState(eState.Synchronized);
            }
        }
        private void DoReload()
        {
            Reset(); // set State to New !
            AddRange(LoadItems());
            HasBeenLoaded = true;
            UpdateState(eState.Synchronized);
        }

        public void ResetEdits()
        {
            LockWhileUpdatingWith(() =>
            {
                Reset();
                Reload();
                HasBeenLoaded = true;
            });
            InitializeState(eState.Synchronized);
        }




        // Cette partie permet de garder une trace des element supprimé de la liste afin de pouvoir facilement faire un update des items un par un
        // Cela permet de faciliter le code de tout interface qui editerait cette liste.
        #region  Gestion pour garder une trace des elements supprimés
        public IReadOnlyDictionary<TKey, TValue> DeletedObjects { get { return _DeletedObjects; } }
        public readonly Dictionary<TKey, TValue> _DeletedObjects = new Dictionary<TKey, TValue>();

        public bool RememberDeletedObjects { get; set; }

        void RememberDeletedObjectsFeature_Initialize()
        {
            CollectionChanged += RememberDeletedObjectsFeature_CollectionChanged;
        }

        void RememberDeletedObjectsFeature_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                RememberDeletedObjectsFeature_OnRemovedItem(e.OldItems);
                OnRemovedItem_SubscribePropertyChange(e.OldItems);
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                RememberDeletedObjectsFeature_OnAddedItem(e.NewItems);
                OnAddedItem_SubscribePropertyChange(e.NewItems);
            }
            else if (e.Action == NotifyCollectionChangedAction.Replace)
            {
                RememberDeletedObjectsFeature_OnRemovedItem(e.OldItems);
                OnRemovedItem_SubscribePropertyChange(e.OldItems);
                RememberDeletedObjectsFeature_OnAddedItem(e.NewItems);
                OnAddedItem_SubscribePropertyChange(e.NewItems);
            }
        }
        protected override void ClearItems()
        {
            if (RememberDeletedObjects)
                RememberDeletedObjectsFeature_OnRemovedItem(this.ToList());
            base.Clear();
        }

        private void RememberDeletedObjectsFeature_OnAddedItem(IList kvps)
        {
            if (!RememberDeletedObjects)
                return;
            foreach (KeyValuePair<TKey, TValue> kvp in kvps)
            {
                if (kvp.Value is IObjectWithState pnoValue)
                {
                    if (pnoValue.State == eState.ToDelete)
                        pnoValue.UpdateState(eState.Unsynchronized); // Impossible de savoir si l'etat d'avant était Synchronized ou Unsynchronized donc par securité on met toujours Unsynchronized
                    if (pnoValue.State == eState.Deleted)
                        pnoValue.InitializeState(eState.New);
                }
                if (_DeletedObjects.TryGetValue(kvp.Key, out TValue deletedValue))
                    if (deletedValue.Equals(kvp.Value)) // Quand TValue est un objet, il se peut que chacun ait la meme clef mais que les objets soit différents
                        _DeletedObjects.Remove(kvp.Key);
            }
        }

        private void RememberDeletedObjectsFeature_OnRemovedItem(IList kvps)
        {
            if (!RememberDeletedObjects)
                return;
            foreach (KeyValuePair<TKey, TValue> kvp in kvps)
            {
                if (kvp.Value is IObjectWithState pnoValue)
                {
                    if (pnoValue.State.In(eState.New, eState.Deleted))
                        continue;
                    if (pnoValue.State.In(eState.Synchronized, eState.Unsynchronized))
                        pnoValue.UpdateState(eState.ToDelete);

                    Debug.Assert(!_DeletedObjects.ContainsKey(kvp.Key));
                    _DeletedObjects.Add(kvp.Key, kvp.Value);
                }
            }
        }


        #endregion

        // Cette partie permet de gerer le relayage des évènements Dirty des sous objet de la liste vers la liste elle même et vers l'objet parent de la liste
        #region Gestion des elements en tant qu'objet implementant IPropertyNotifierObject
        /// <summary>
        /// Indique si, dès qu'une item est ajouté ou supprimé de la liste, l'état de la liste doit être considéré comme modifié (IsDirty == true).
        /// Par default a true
        /// </summary>
        public bool ConsiderDirtyOnAddRemoveEvents { get; set; } // On n'utilise pas SetProperty<bool> car il s'agit d'une propriété technique


        /// <summary>
        /// Indique si, dès qu'un item est modifié (une de ses proprietés a été altéré), on considère la liste comme modifié (IsDirty == true)
        /// Par default a false.
        ///
        /// Cette proprieté est interessante dans le cas suivant :
        /// Partons du principe que :
        ///   - On a un objet Eleve qui contient une liste de ContrôleScolaire.
        ///   - La vue qui affiche l'eleve affiche également des informations aggregés sur les controles scolaires (exemple la moyenne de l'eleve ...)
        ///   - Que la vue delegue l'edition des notes a une autre vue
        ///
        /// Lorsque l'utilisateur change les notes dans la sous vue, il est maintenant possible, simplement, de raffraichir dans la vue principale,
        /// uniquement la moyenne (et pas toutes les informations de l'eleve), comme ceci :
        ///
        /// <example>
        /// Eleve eleve = ...;
        /// eleve.Note_scolaire.ConsiderDirtyOnItemChanged = true;
        /// eleve.Note_scolaire.OnDirty += () => RaffraichirMoyennes() // On évite de tout raffraichir !</example>
        /// new FrmEditionDesNotes(eleve.Note_scolaire).Show(); // plus besoin de faire un ShowDialog() !
        /// </summary>
        public bool ConsiderDirtyOnItemChanged { get; set; } // Idem

        void ManageElementImplementingIPropertyNotifierObject()
        {
            ConsiderDirtyOnAddRemoveEvents = true;
        }


        public void AcceptChanges()
        {
            UpdateItemStates();
            _DeletedObjects.Clear();
            UpdateState();
        }

        public void UpdateItemStates()
        {
            if (typeof(IObjectWithState).IsAssignableFrom(typeof(TValue)))
            {
                foreach (IObjectWithState item in Values)
                    item.UpdateState();
                foreach (IObjectWithState item in _DeletedObjects.Values)
                    if (item.State == eState.ToDelete)
                        item.UpdateState();
            }
        }


        private void OnAddedItem_SubscribePropertyChange(IList kvps)
        {
            if (ConsiderDirtyOnAddRemoveEvents)
                SetDirtyBy(this);
            if (!ConsiderDirtyOnItemChanged)
                return;
            foreach (KeyValuePair<TKey, TValue> kvp in kvps)
            {
                if (kvp.Value is IPropertyNotifierObject pnoValue)
                {
                    pnoValue.PropertyChanged -= PnoValue_PropertyChanged;
                    pnoValue.PropertyChanged += PnoValue_PropertyChanged;
                }
            }
        }

        private void OnRemovedItem_SubscribePropertyChange(IList kvps)
        {
            if (ConsiderDirtyOnAddRemoveEvents)
                SetDirtyBy(this);
            if (!ConsiderDirtyOnItemChanged)
                return;
            foreach (KeyValuePair<TKey, TValue> kvp in kvps)
            {
                if (kvp.Value is IPropertyNotifierObject pnoValue)
                    pnoValue.PropertyChanged -= PnoValue_PropertyChanged;
            }
        }
        void PnoValue_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e is TechnicalPropertyChangedEventArgs te)
                return;
            SetDirtyBy(sender);
        }

        #endregion


        // Le code de cette région n'a aucune intelligence autre que d'implementer IPropertyNotifierObject en deleguant les appels à _propertyNotifierObject
        // Toute cette region n'a que pour but de simuler un heritage de PropertyNotifierObject
        #region "Implementation de IPropertyNotifierObject => Delegation complete à l'objet _propertyNotifierObject"

        // Révons un peu en esperant un jour voir ce genre de chose : http://stackoverflow.com/a/255621)
        readonly PropertyNotifierObject _propertyNotifierObject;

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void SetProperty<T>(ref T backingStore, T value, Action onChanged = null, Action<T> onChanging = null, [CallerMemberName] string propertyName = null)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).SetProperty<T>(ref backingStore, value, onChanged, onChanging, propertyName);
        }
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void SetTechnicalProperty<T>(ref T backingStore, T value, Action onChanged = null, Action<T> onChanging = null, [CallerMemberName] string propertyName = null)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).SetTechnicalProperty(ref backingStore, value, onChanged, onChanging, propertyName);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void CurrentPropertyIsDependantOn<T>(Expression<Func<T, object>> dependency_prop_has_expression, Func<bool> refresh_current_property = null, [CallerMemberName] string dependent_propertyName = null)
            where T : class, IPropertyNotifierObject
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).CurrentPropertyIsDependantOn(dependency_prop_has_expression, refresh_current_property, dependent_propertyName);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [Obsolete("Utilisez SetDirtyBy si possible !")]
        public void Touch()
        {
            _propertyNotifierObject.Touch();
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetDirtyBy(object contaminating_object)
        {
            _propertyNotifierObject.SetDirtyBy(contaminating_object);
        }

        #region propriétés IsDirty, State et autres

        public eState State { [DebuggerHidden, DebuggerStepThrough] get { return _propertyNotifierObject.State; } }

        public event Action<object, eState> StateChanged
        {
            [DebuggerHidden, DebuggerStepThrough]
            add { _propertyNotifierObject.StateChanged += value; }
            [DebuggerHidden, DebuggerStepThrough]
            remove { _propertyNotifierObject.StateChanged -= value; }
        }
        public bool IsDirty { [DebuggerHidden, DebuggerStepThrough]get { return _propertyNotifierObject.IsDirty; } }
        public event EventHandler DirtyChanged
        {
            [DebuggerHidden, DebuggerStepThrough]
            add { _propertyNotifierObject.DirtyChanged += value; }
            [DebuggerHidden, DebuggerStepThrough]
            remove { _propertyNotifierObject.DirtyChanged -= value; }
        }

        [DebuggerHidden, DebuggerStepThrough]
        public void InitializeState(eState new_state)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).InitializeState(new_state);
        }
        [DebuggerHidden, DebuggerStepThrough]
        public void UpdateState(eState? new_state = null)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).UpdateState(new_state);
        }
        protected ePropertyEventLock LockedEvents { [DebuggerHidden, DebuggerStepThrough] get { return ((IPropertyNotifierObject)_propertyNotifierObject).LockedEvents; } set { ((IPropertyNotifierObject)_propertyNotifierObject).LockedEvents = value; } }
        protected bool LockState { [DebuggerHidden, DebuggerStepThrough] get { return ((IPropertyNotifierObject)_propertyNotifierObject).LockState; } set { ((IPropertyNotifierObject)_propertyNotifierObject).LockState = value; } }

        public override bool IsReadOnly
        {
            [DebuggerHidden, DebuggerStepThrough]
            get { return ((IPropertyNotifierObject)_propertyNotifierObject).IsReadOnly; }
            [DebuggerHidden, DebuggerStepThrough]
            set
            {
                ((IPropertyNotifierObject)_propertyNotifierObject).IsReadOnly = value;
            }
        }
        public bool IsNew { [DebuggerHidden, DebuggerStepThrough] get { return ((IPropertyNotifierObject)_propertyNotifierObject).IsNew; } }
        public bool CreatedInCurrentSession { [DebuggerHidden, DebuggerStepThrough] get { return ((IPropertyNotifierObject)_propertyNotifierObject).CreatedInCurrentSession; } }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CheckIsNotReadOnlyForAction(string action_name = null)
        {
            try
            {
                ((IPropertyNotifierObject)_propertyNotifierObject).CheckIsNotReadOnlyForAction(action_name);
            }
            catch (Exception ex)
            {
                // On ne fournit pas ex en tant qu'inner exception car on souhaite simplement renommer le message
                throw new Exception(ex.Message.Replace(typeof(PropertyNotifierObject).Name, GetType().FullName));
            }
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void LockWhileUpdatingWith(Action action)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).LockWhileUpdatingWith(action);
        }
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void LockWhileUpdatingWith(ePropertyEventLock lck, Action action)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).LockWhileUpdatingWith(lck, action);
        }

        #endregion propriétés IsDirty, State et autres


        #region Gestion de l'event PropertyChanged

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PropertyChangedFor<T>(Expression<Func<T, object>> expression, Action onPropertyChanged)
            where T : class, IPropertyNotifierObject
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).PropertyChangedFor(expression, onPropertyChanged);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PropertyChangedFor_Remove<T>(Expression<Func<T, object>> expression, Action onPropertyChanged)
            where T : class, IPropertyNotifierObject
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).PropertyChangedFor_Remove(expression, onPropertyChanged);
        }

        #endregion Gestion de l'event PropertyChanged

        #region Gestion de l'event PropertyChanging

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.PropertyChangingFor<T>(Expression<Func<T, object>> expression, Action<object, PropertyChangingEventArgs> onPropertyChanging)
        //FYI : where T : class, IPropertyNotifierObject
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).PropertyChangingFor(expression, onPropertyChanging);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.PropertyChangingFor_Remove<T>(Expression<Func<T, object>> expression, Action<object, PropertyChangingEventArgs> onPropertyChanging)
        //FYI : where T : class, IPropertyNotifierObject
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).PropertyChangingFor_Remove(expression, onPropertyChanging);
        }

        #endregion Gestion de l'event PropertyChanging


        #region (Super prive) Simulation de l'heritage des membres protégés
        // Cf les explications dans la même region dans PropertyNotifierObject
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.SetProperty<T>(ref T backingStore, T value, Action onChanged, Action<T> onChanging, string propertyName)
        {
            // ReSharper disable ExplicitCallerInfoArgument
            SetProperty(ref backingStore, value, onChanged, onChanging, propertyName);
            // ReSharper restore ExplicitCallerInfoArgument
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.SetTechnicalProperty<T>(ref T backingStore, T value, Action onChanged, Action<T> onChanging, string propertyName)
        {
            SetTechnicalProperty(ref backingStore, value, onChanged, onChanging, propertyName);
        }

        void IPropertyNotifierObject.CurrentPropertyIsDependantOn<T>(Expression<Func<T, object>> dependency_prop_has_expression, Func<bool> refresh_current_property, string dependent_propertyName)
        {
            // ReSharper disable ExplicitCallerInfoArgument
            ((IPropertyNotifierObject)_propertyNotifierObject).CurrentPropertyIsDependantOn(dependency_prop_has_expression, refresh_current_property, dependent_propertyName);
            // ReSharper restore ExplicitCallerInfoArgument
        }


        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { ((IPropertyNotifierObject)_propertyNotifierObject).PropertyChanged += value; }
            remove { ((IPropertyNotifierObject)_propertyNotifierObject).PropertyChanged -= value; }
        }
        event Action<PropertyNotifierObject, ePropertyEventLock> IPropertyNotifierObject.OnResetBindings
        {
            add { ((IPropertyNotifierObject)_propertyNotifierObject).OnResetBindings += value; }
            remove { ((IPropertyNotifierObject)_propertyNotifierObject).OnResetBindings -= value; }
        }

        ePropertyEventLock IPropertyNotifierObject.LockedEvents { get { return LockedEvents; } set { LockedEvents = value; } }
        bool IPropertyNotifierObject.LockState { get { return LockState; } set { LockState = value; } }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.LockWhileUpdatingWith(Action action)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).LockWhileUpdatingWith(action);
        }
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.LockWhileUpdatingWith(ePropertyEventLock lck, Action action)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).LockWhileUpdatingWith(lck, action);
        }

        /// <summary> Fournit une méthode pour gérer la contamination du dirty </summary>
        public void DirtyChangedHandler(object sender, EventArgs e)
        {
            _propertyNotifierObject.DirtyChangedHandler(sender, e);
        }

        #endregion (Super prive) Simulation de l'heritage des membres protégés

        #endregion

    }

}
