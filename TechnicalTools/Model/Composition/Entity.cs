﻿using System;
using System.Diagnostics;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;


namespace TechnicalTools.Model.Composition
{

    public interface IEntityBase : IHasMutableTypedId, ILoadableById
    {

    }

    public abstract class EntityBase : PropertyNotifierObject
    {
        internal static readonly string IdPropertyName = GetMemberName.For<IHasMutableClosedId>(o => o.Id);
#pragma warning disable 618
        //[Obsolete("TOUJOURS éviter d'overrider ToString. Utilisez l'attribut DebuggerDisplay pour faciliter le debugging.  (TODO : voir http://tiku.io/questions/234065/forcing-visual-studio-debugging-tools-to-display-useful-information)" +
        //    "Ou bien créez une méthode \"AsBusinessString\" etc que vous appelerez explicitement. " +
        //    "Raison : " +
        //    " 1) ToString() est ce qui est utilisé par default pour beaucoup de chose. Exemple vous remplissez une listbox avec des objets. ToString sera appelé automatiquement par le framework.net" +
        //    "    C'est pratique mais il est impossible de savoir qui utilise réellement ToString 6 mois plus tard (recherche de reference)" +
        //    "    Dans ces cas là, il est preferable que ca plante le plus tôt possible..." +
        //    " 2) ToString() est egalement appelé par le Debugger (onglet Watch). Donc si le code présent dans ToString a des effets de bord, cela affecte votre exécutable sans que vous ne vous en rendiez compte." +
        //    "    Exemple : Vous utilisez une propriété Lazy dans ToString() qui fait appel à la base de donné" +
        //    "    Personne ne pense à desactiver Tools -> Options -> Debugger -> \"Call string conversion on objects in variable windows\"" +
        //    "    Ce problème est a l'origine d'un probleme de freeze connu en cas de multithreading car ToString est appellé sur les forms ce qui fait freezer Visual Studio pendant quelques secondes" +
        //    " 3) Même problème potentiel (en plus compliqué) avec le Designer de Visual Studio. Car celui ci execute du code pendant le mode Design (cf l'utilisation de DesignTimeHelper)" +
        //    " 4) Il existe un problème similaire car le debugger appelle ToString sur les form. Ce qui peut provoquer des problème Même problème potentiel (en plus compliqué) avec le Designer de Visual Studio. Car celui ci execute du code pendant le mode Design (cf l'utilisation de DesignTimeHelper)" +
        //    " 5) Similaire au point 1), On ne sait pas si ToString est utilisé par les scripts (Exemple on passe un objet a une fonction VBScript qui attend normalement une string.) Est-ce que ToString est appelé ?", true)]
        public /*sealed*/ override string ToString()
        {
            return base.ToString();
        }
#pragma warning restore 618

        public abstract void CancelChanges();
    }

    //public interface IEntity<in TId> : IHasTypedId, ILoadableById<TId>
    //  where TId : struct, IEquatable<TId>
    //{

    //}


    public abstract class Entity<TId> : EntityBase, IEntityBase, ILoadableById<TId>, IHasMutableTypedId
        where TId : struct, IEquatable<TId>, IIdTuple
    {
        /// <summary>
        /// Représente l'id de l'objet. L'id est soit un int, soit un IIdTuple (composition de plusieur donné de type simple : int string date etc)
        /// Cette propriete est nommé ClosedId (bientot ClusterId) pour eviter un conflit de nom avec une propriete ID deja existante dans les classes filles
        /// ATTENTION : RaiseIdChanged doit être appelé à chaque fois que ClosedId change !
        /// </summary>
        protected abstract TId? ClosedId { get; set; }      IIdTuple IHasClosedIdReadable.Id { get { return ClosedId; } }

        public virtual ITypedId TypedId
        {
            get
            {
                Debug.Assert(ClosedId.HasValue, "A priori je ne vois pas (encore) de cas _valide_ où TypedId pourrait être appelé quand Id est null. Mais ca pourrait arriver !)" + Environment.NewLine +
                                                "Si cela est normal que l'ID soit a zero (exemple : La classe represete un genre d'enumeration), overridez TypedId pour eviter cet assert");
                return new TypedId<TId>(GetType(), ClosedId.Value); // ClosedId renvoie Null si l'objet a un Id a 0, ca peut arriver parfois,
            }
        }

        ITypedId IHasTypedIdReadable.MakeTypedId(IIdTuple id)
        {
            return new TypedId<TId>(GetType(), (TId)id);
        }



        //[DebuggerHidden, DebuggerStepThrough]
        protected virtual void RaiseIdChanged()
        {
            Debug.Assert(!_OldClosedId.Equals(ClosedId), "RaiseIdChanged n'aurait pas du être appelé pour rien !");

            IdChanging?.Invoke(this, new PropertyHasChangedEventArgs(IdPropertyName, _OldClosedId.HasValue ? (TId?)_OldClosedId.Value : null));
            if (!_OldClosedId.HasValue)
                if (ClosedId.HasValue)
                    CacheManager.Instance.Add(this);
                    //CacheManager.Instance.AddIfNotAlreadyInside(this);
            _OldClosedId = ClosedId;
          }
        protected TId? _OldClosedId { get; private set; }
        event PropertyHasChangedEventHandler                     IdChanging;
        event PropertyHasChangedEventHandler IHasMutableClosedId.IdHasChanged { add { IdChanging += value; } remove { IdChanging -= value; } }

        public abstract void LoadById(TId id);         void ILoadableById.LoadById(object id) { LoadById((TId)id); }
        public abstract void Reload();

        // S'occupe de créer / updater / supprimer
        public abstract void SynchronizeToDatabase();

        public override void CancelChanges()
        {
            if (!ClosedId.HasValue || State == eState.New)
                throw new TechnicalException(string.Format("Cannot undo changes on an object of type {0} that is new!", GetType().Name), null);
            if (State == eState.Deleted)
                throw new TechnicalException(string.Format("Cannot undo changes on an object of type {0} already deleted!", GetType().Name), null);
            if (State != eState.Synchronized)
                LoadById(ClosedId.Value);
        }

        public override Action CreateActionToRestoreState()
        {
            var action = base.CreateActionToRestoreState();
            var closed_id = ClosedId;
            return () =>
            {
                if (!ClosedId.Equals(closed_id))
                {
                    ClosedId = closed_id;
                    RaiseIdChanged();
                }
                action();
            };
        }
    }
}

