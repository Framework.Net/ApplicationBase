﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ComponentModel;
using System.Reflection;
using TechnicalTools.Tools;


namespace TechnicalTools.Model
{
    /// <summary>
    /// Cette classe permet d'ameliorer la gestion de l'evenement ListChanged.
    /// On souhaite ajouter la capacité de savoir si la propriete d'un item qui a changé est technique ou non.
    /// En fait cette classe permet de relayer l'information géré par PropertyNotifierObject.
    /// En effet, dans le cas de propriete qui change, la classe BindingList ne stocke pas l'objet PropertyChangedEventArgs qui provoque l'event ListChanged
    /// </summary>
    [Serializable]
    public class FixedBindingListWithNotifyProperty<T> : FixedBindingList<T>
    {
        public FixedBindingListWithNotifyProperty()
        {
            InitializeImprovement();
        }

        public FixedBindingListWithNotifyProperty(IList<T> list)
            : base(list)
        {
            InitializeImprovement();
        }
        public FixedBindingListWithNotifyProperty(IEnumerable<T> values)
            : base(values.ToList())
        {
            InitializeImprovement();
        }

        // On utilise la réflection car il n'y a pas, à ma connaissance, de moyen plus propre pour faire ce qui suit :(
        // Si vous trouvez, ne pas hesiter à en discuter.

        void InitializeImprovement()
        {
            // Check for INotifyPropertyChanged
            // On va défaire et refaire ce qui a été fait dans BindingList<T>
            if (typeof(INotifyPropertyChanged).IsAssignableFrom(typeof(T)))
            {
                Debug.Assert(_childPropertyChangedMethod != null);
                Debug.Assert(_propertyChangedEventHandlerField != null);
                _childPropertyChangedMethodDelegate = (Action<object, PropertyChangedEventArgs>)_childPropertyChangedMethod.CreateDelegate(typeof(Action<object, PropertyChangedEventArgs>), this);

                var newHandler = new PropertyChangedEventHandler(SubstituteOf_Child_PropertyChanged);

                if (Count > 0)
                {
                    var oldHandler = (PropertyChangedEventHandler)_propertyChangedEventHandlerField.GetValue(this);
                    Debug.Assert(oldHandler != null, "La classe doit toujours avoir instancier normalement etant le if precedent");

                    // Loop thru the items already in the collection and hook their change notification.
                    foreach (var item in Items)
                    {
                        if (item is INotifyPropertyChanged inpc) // Note: inpc may be null if item is null, so always check.
                        {
                            inpc.PropertyChanged -= oldHandler;
                            inpc.PropertyChanged += newHandler;
                        }
                    }
                }
                _propertyChangedEventHandlerField.SetValue(this, newHandler);
            }
        }

// ReSharper disable StaticFieldInGenericType
        static readonly FieldInfo _propertyChangedEventHandlerField = typeof(BindingList<T>).GetField("propertyChangedEventHandler", BindingFlags.Instance | BindingFlags.NonPublic);
        static readonly MethodInfo _childPropertyChangedMethod = typeof (BindingList<T>).GetMethod("Child_PropertyChanged", BindingFlags.Instance | BindingFlags.NonPublic);
        Action<object, PropertyChangedEventArgs> _childPropertyChangedMethodDelegate;
// ReSharper restore StaticFieldInGenericType

        void SubstituteOf_Child_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (RaiseListChangedEvents)
                _original_eventargs = e; // Memorise l'instance
            // Puis on fait comme avant
            _childPropertyChangedMethodDelegate(sender, e);
        }
        PropertyChangedEventArgs _original_eventargs;
        protected override void OnListChanged(ListChangedEventArgs e)
        {
            // AFAIK : Le seul cas (parmis les 4 dans le code source de BindingList) qui est utilisé quand une propriete change sur un item est celui qui suit (sans _original_eventargs) :
            // TODO : Verifier si c'est le cas quand T est un IPropertyNotifierObject
            if (e.PropertyDescriptor != null ||
                (typeof(IPropertyNotifierObjectWrapper).IsAssignableFrom(typeof(T)) && _original_eventargs != null)) // Cas particulier : les wrapper de INotifyPropertyChanged n'ont pas la propriété)
            {
                Debug.Assert(_original_eventargs != null, "Le property descriptor devrait toujours exister");
                e = new ListChangedEventArgs2(e.ListChangedType, e.NewIndex, e.PropertyDescriptor, _original_eventargs);
            }
            // On remet a null car la methode privée FireListChanged de BindingList peut aussi declencher des evenements ListChanged
            // Il ne faudrait pas reutiliser le _original_event_args de l'event precedent
            _original_eventargs = null;
            base.OnListChanged(e);
        }
    }


    public class ListChangedEventArgs2 : ListChangedEventArgs
    {
        public PropertyChangedEventArgs SourceEventArgs { get; private set; }

        public ListChangedEventArgs2(ListChangedType listChangedType, int newIndex, PropertyDescriptor propDesc, PropertyChangedEventArgs e)
            : base(listChangedType, newIndex, propDesc)
        {
            SourceEventArgs = e;
        }
    }

    public static partial class ListChangedEventArgs_Extensions
    {
        public static bool? IsTechnicalProperty(this ListChangedEventArgs e)
        {
            if (e is ListChangedEventArgs2)
                return (e as ListChangedEventArgs2).SourceEventArgs is TechnicalPropertyChangedEventArgs;
            return null; // On n'en sait rien
        }
    }
}
