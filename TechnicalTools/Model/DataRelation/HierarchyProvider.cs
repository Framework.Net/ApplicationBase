﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;
using TechnicalTools.Logs;


namespace TechnicalTools.Model
{
    /// <summary>
    /// Helper class providing to developper a way to create/store a graph of hierarchies.
    /// A hierarchy is kind of BI or UI relation where an object has childs.
    /// <p>
    /// It is important to note that this relation "parent -> child" is purely "BI" ou just "UI"
    /// and does not always match the technical relation of underlying object (poco object used to map Database)
    /// (especially when the db is dirty and FK are messy / inexisting)
    /// </p>
    /// <p>
    /// It is often recommended to create hierarchies for both directions :
    /// - 1 Book => n Pages
    /// - 1 Page => 1 Owning book
    /// This makes user free to navigate everywhere, in any direction among the database data
    /// </p>
    /// </summary>
    public abstract partial class GenericHierarchyGraphProvider
    {
        public int DefaultMaxRelationRecursivity { get; set; } = 3;

        protected GenericHierarchyGraphProvider(TypeSizeEstimator estimator = null)
        {
            _estimator = estimator ?? new TypeSizeEstimator();
            _log = LogManager.Default.CreateLogger(GetType());
        }
        protected readonly ILogger _log;
        protected readonly TypeSizeEstimator _estimator;

        /// <summary>
        /// Just to add hierarchies created by developpers in the whole graph of BI/UI relations
        /// </summary>
        protected void AddConstructor<TFor, TRelated>(HierarchyBuilder<TFor, TRelated> cons)
            where TFor : class
            where TRelated : class
        {
            var lst = Constructors.TryGetValueClass(typeof(TFor));
            if (lst == null)
            {
                lst = new List<HierarchyBuilder>();
                Constructors.Add(typeof(TFor), lst);
            }
            lst.Add(g => cons(g));
        }
        protected readonly Dictionary<Type, List<HierarchyBuilder>> Constructors = new Dictionary<Type, List<HierarchyBuilder>>();

        public delegate HierarchyDescriptionTyped HierarchyBuilder(HierarchyDescriptionGraph ownerGraph);
        public delegate HierarchyDescriptionTyped<TFor, TRelated> HierarchyBuilder<TFor, TRelated>(HierarchyDescriptionGraph ownerGraph)
            where TFor : class
            where TRelated : class;
        /// <summary>
        /// Main helper to build a hierarchy in case you want to create/display a BI/UI relation Parent => Child.
        /// <example>
        /// A "Body" table contains two FKs: LegLeft, LegtRight
        /// In this case this not the "Leg" table that contains a FK to Body
        /// </example>
        /// <example>
        /// For example you want to show "page" objects to users where a rare word is used.
        /// And you want to display the book of each page. Book are the parent object in database (ie: pages references their owning book using a FK)
        /// But you want to create a relation Page => Book where the book is the "child" from the UI perspective.
        /// </example>
        /// In case child objects reference their parent in database, it is advised to use instead
        /// <seealso cref="BuildCachedHierarchyFromParentToChild{TParent, TChild, TParentKey}(string, Func{IReadOnlyCollection{TParent}, List{TChild}}, Func{TChild, TParentKey}, int, int?)"/>
        /// </summary>
        /// <typeparam name="TParent">The class that is the parent (in term of Bi/UI)</typeparam>
        /// <typeparam name="TChild">The class that is child (in term of Bi/UI)</typeparam>
        protected HierarchyDescriptionTyped<TParent, TChild> BuildCachedHierarchy<TParent, TChild>(string name, Func<IEnumerable<TParent>, Dictionary<TParent, List<TChild>>> getDataForPartnerGroupByParentKey, int averageChildrenCount, int? averageChildSizeInMemory = null)
            where TParent : class
            where TChild : class
        {
            // internal cache
            Dictionary<TParent, List<TChild>> childrenByParent = null;

            // We strongly type because a small mistake can happen very quickly with generic type
            // This is the signature to match to make other classes working
            Func<IEnumerable<TParent>, IReadOnlyCollection<TChild>> getAllChildren = allParents =>
            {
                childrenByParent = getDataForPartnerGroupByParentKey(allParents);
                return childrenByParent.Values.SelectMany(children => children).ToList();
            };
            Func<Stack<object>, IReadOnlyCollection<TChild>> getChildren = parentsStack =>
            {
                var parent = (TParent)parentsStack.Peek();
                if (childrenByParent == null)
                    return getDataForPartnerGroupByParentKey(parent.WrapInList())?.TryGetValueClass(parent);
                return childrenByParent.TryGetValueClass(parent);
            };

            var h = new HierarchyDescriptionTyped<TParent, TChild>(name,
                        getChildren, allParents => getAllChildren(allParents.Cast<TParent>()),
                        averageChildrenCount: averageChildrenCount,
                        averageChildSizeInMemory: averageChildSizeInMemory ?? _estimator.GetMeanInstanceSize(typeof(TChild)));
            return h;
        }

        /// <summary>
        /// Helper to build a hierarchy in case you want to create/display a BI/UI relation Parent => Child
        /// where child(s) reference the parent
        /// This method is more optimized than using BuildCachedHierarchy
        /// <seealso cref="BuildCachedHierarchy{TParent, TChild}(string, Func{IEnumerable{TParent}, Dictionary{TParent, List{TChild}}}, int, int?)"/>
        /// Otherwise, use
        /// <seealso cref="BuildCachedHierarchyFromParentToChild{TParent, TChild, TParentKey}(string, Func{IReadOnlyCollection{TParent}, List{TChild}}, Func{TChild, TParentKey}, int, int?)"/>
        /// </summary>
        /// <typeparam name="TParent">The class that is the parent (in term of Bi/UI)</typeparam>
        /// <typeparam name="TChild">The class that is child (in term of Bi/UI)</typeparam>
        /// <typeparam name="TParentKey">Type of the unique id of the Parent Class (this is often the C# type for the PK in Parent class</typeparam>
        protected HierarchyDescriptionTyped<TParent, TChild> BuildCachedHierarchyFromParentToChild<TParent, TChild, TParentKey>(string name, Func<IReadOnlyCollection<TParent>, List<TChild>> getData, Func<TChild, TParentKey> getParentKey, int averageChildrenCount, int? averageChildSizeInMemory = null)
            where TParent : class, IHasTypedIdReadable<IdTuple<TParentKey>>, IHasClosedIdReadable
            where TParentKey : IEquatable<TParentKey>, IComparable<TParentKey>, IComparable
            where TChild : class
        {
            // internal cache
            Dictionary<TParentKey, List<TChild>> cachedChildren = null;

            var h = new HierarchyDescriptionTyped<TParent, TChild>(name,
                   getAllChildren: allParents =>
                   {
                       var results = getData(allParents as IReadOnlyCollection<TParent> ?? allParents.Cast<TParent>().ToList());
                       cachedChildren = results.GroupBy(getParentKey).ToDictionary(grp => grp.Key, grp => grp.ToList());
                       return results;
                   },
                   getChildren: parentsStack =>
                   {
                       var parent = (TParent)parentsStack.Peek();
                       if (cachedChildren == null)
                           return getData(parent.WrapInList());
                       return cachedChildren.TryGetValueClass((TParentKey)parent.Id.Keys[0]);
                   },
                   averageChildrenCount: averageChildrenCount,
                   averageChildSizeInMemory: averageChildSizeInMemory ?? _estimator.GetMeanInstanceSize(typeof(TChild)));
            return h;
        }

        public IReadOnlyCollection<IHierarchyDescription> GetHierarchies(Type t, int? maxRelationRecursivity = null)
        {
            return BuildHierarchyGraph(t, maxRelationRecursivity).TopHierarchies.ToList();
        }
        public HierarchyDescriptionGraph BuildHierarchyGraph(Type t, int? maxRelationRecursivity = null)
        {
            var graph = new HierarchyDescriptionGraph();
            FillBuildHierarchyGraph(graph, t, maxRelationRecursivity ?? DefaultMaxRelationRecursivity, new Dictionary<Type, int>());
            return graph;
        }
        List<HierarchyDescriptionTyped> FillBuildHierarchyGraph(HierarchyDescriptionGraph graph, Type t, int maxRelationRecursivity, Dictionary<Type, int> recursivities)
        {
            var results = new List<HierarchyDescriptionTyped>();
            foreach ((var type, var constructors) in Constructors)
                if (type.IsAssignableFrom(t) && (recursivities.TryGetValueStruct(type) ?? 0) < maxRelationRecursivity)
                {
                    graph.Types.Add(type);
                    foreach (var cons in constructors)
                    {
                        var h = cons(graph);
                        IHierarchyDescription ih = h;
                        ih.OwnerGraph = graph;
                        graph.AllHierarchies.Add(h);
                        if (recursivities.Values.DefaultIfEmpty(0).Sum() == 0)
                            graph.TopHierarchies.Add(h);
                        recursivities[type] = (recursivities.TryGetValueStruct(type) ?? 0) + 1;
                        var subHierarchies = FillBuildHierarchyGraph(graph, h.ChildType, maxRelationRecursivity, recursivities);
                        recursivities[type] = recursivities[type] - 1;
                        h.SetChildren(subHierarchies);
                        results.Add(h);
                    }
            }
            return results;
        }
    }
}
