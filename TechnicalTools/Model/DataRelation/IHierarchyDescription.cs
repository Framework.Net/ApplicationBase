﻿using System;
using System.Collections.Generic;


namespace TechnicalTools.Model
{

    /// <summary>
    /// Allow you to describe a hierarchy (ie: object with parent/child relationship) and describe a way to get children given a parent object.
    /// This is essentially used by class <see cref="ApplicationBase.UI.GridViewDataSourceFlattenizer"/>
    /// Typically if you have a hierarchy of object like this :
    /// Authors 1
    ///    Book 1
    ///    Book 2
    ///       Reference 1
    ///    Book 3
    ///       Reference 2
    ///       Reference 3
    /// Author 2
    ///    Book 4
    ///      Reference 4
    /// where each object have a FK to its parent
    /// then you could define a simple and intuitive hierarchy like this, considering you display a list of authors to user.
    /// <code>
    /// new HierarchyDescription("Her books", parents => get books with Author_Id = ((Author)parents.Peek()).Id,
    ///     childrenDescriptions:
    ///         new HierarchyDescription("Book's references", parents => get reference where Book_Id = ((Book)parents.Peek()).Id)
    ///         .WrapInList())
    /// .WrapInList();
    /// </code>
    /// As you noted parents contains the hierarchy of all ancestors. Peek() or ElementAt(0) get the  most inner one.
    /// You can get grand father object by using parents.ElementAt(1), etc.
    /// This description allow you to change the perspective of a hierarchy. For example you could describe a hierarchy starting from the list of book :
    /// <code>
    /// return new [] {
    ///   new HierarchyDescription("Book's author", parents => get Author with Id = ((Book)parents.Peek()).Author_Id) and wrap it in a list,
    ///   new HierarchyDescription("Book's references", parents => get reference where Book_Id = ((Book)parents.Peek()).Id)
    /// }
    /// </code>
    /// As you can see the hierarchy you describe is just a matter of perspective comparing to how it is stored in a DB.
    /// </summary>
    public interface IHierarchyDescription
    {
        /// <summary>
        /// A good name for the set of children objects
        /// Ex: if we consider the current object is a book then we can call the hierarchy "Pages")
        /// </summary>
        string LevelName { get; }

        /// <summary> Estimated type of master object, null means you don't know </summary>
        Type For       { get; }
        /// <summary> Estimated type of related object, null means you don't know </summary>
        Type ChildType { get; }

        /// <summary>
        /// Returns list of children for a parent object stored in /returned by arg.Peek() or arg.ElementAt(0)
        /// You can also use arg.ElementAt(1), arg.ElementAt(2) to grand parent, grand grand parent etc
        /// </summary>
        Func<Stack<object>, IReadOnlyCollection<object>> GetChildren      { get; }
        /// <summary>
        /// Return the number of element that GetChildren would return with the same input.
        /// </summary>
        Func<Stack<object>, int>   GetChildrenCount { get; }

        /// <summary>
        /// Define recursively the inner hierarchy of object
        /// </summary>
        IReadOnlyCollection<IHierarchyDescription> ChildrenDescriptions { get; }

        HierarchyDescriptionGraph OwnerGraph { get; set; }
    }

    public enum eRelationCardinality : byte
    {
        Unknown = 0,
        Zero = 64,
        N = 128,
        One = 1,
        Two = 2,
        Three = 4,
        Four = 8,
        Five = 16,
        Six = 32,

        Zero_Or_One = Zero | One,
        Zero_Or_N = Zero | N,
        One_Or_N = One | N,

        Two_Or_N = Two | N,
        Three_Or_N = Three | N,
        Four_Or_N = Four | N,
        Five_Or_N = Five | N,
        Six_Or_N = Six | N
    }
    public struct Relation
    {
        public eRelationCardinality Source { get; }
        public eRelationCardinality Target { get; }

        public Relation(eRelationCardinality s, eRelationCardinality t)
        {
            Source = s;
            Target = t;
        }
    }

    public interface IHierarchyDescriptionOptimized : IHierarchyDescription
    {
        /// <summary> Describe more precisely the relation Parent to Children</summary>
        Relation Relation { get; set; }

        /// <summary>
        /// Indicate the average (statistical) number of child for each parent.
        /// This information can be used by some algorithm to decide if it interesting to call PreLoadAllChildren.
        /// </summary>
        int AverageChildrenCount { get; }

        /// <summary>
        /// Indicate the average size of a child in memory (in bytes)
        /// This information can be used by some algorithm to decide if it interesting to call PreLoadAllChildren.
        /// </summary>
        int AverageChildSizeInMemory { get; }

        /// <summary>
        /// Allow user to add a set operation that preload operation.
        /// GetChildren will still be called but is expected to use a cache.
        /// </summary>
        /// <param name="allParents"></param>
        IReadOnlyCollection<object> PreLoadAllChildren(IReadOnlyCollection<object> allParents);
    }
}