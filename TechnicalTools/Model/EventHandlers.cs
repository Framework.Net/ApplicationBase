﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace TechnicalTools.Model
{
    public delegate void PropertyHasChangedEventHandler(object sender, PropertyHasChangedEventArgs e);
    public class PropertyHasChangedEventArgs : PropertyChangedEventArgs
    {
        public object PreviousValue { get; protected set; }

        public PropertyHasChangedEventArgs(string propertyName, object previousValue)
            : base(propertyName)
        {
            PreviousValue = previousValue;
        }
    }

    public delegate void PropertyHasChangedEventHandler<T>(object sender, PropertyHasChangedEventArgs<T> e);
    public class PropertyHasChangedEventArgs<T> : PropertyHasChangedEventArgs
    {
        public new T PreviousValue { get { return (T)base.PreviousValue; } }

        public PropertyHasChangedEventArgs(string propertyName, T previousValue)
            : base(propertyName, previousValue)
        {
            base.PreviousValue = previousValue;
        }
    }

    public delegate void PropertyChangingEventHandler(object sender, PropertyChangingEventArgs e);
    public class PropertyChangingEventArgs : System.ComponentModel.PropertyChangingEventArgs
    {
        public object OriginalValue { get; protected set; }
        public object NewValue { get; protected set; }

        public bool Cancel { get; set; }
        public Exception CancelException { get; set; } // Si setté, cette exception est déclenchée au lieu que le traitement en cours soit simplement annulé / ignoré

        public PropertyChangingEventArgs(string propertyName, object originalValue, object newValue)
            : base(propertyName)
        {
            OriginalValue = originalValue;
            NewValue = newValue;
        }

        [DebuggerHidden, DebuggerStepThrough]
        public PropertyChangingEventArgs<T> As<T>()
        {
            return (PropertyChangingEventArgs<T>)this;
        }
    }

    public delegate void PropertyChangingEventHandler<T>(object sender, PropertyChangingEventArgs<T> e);
    public class PropertyChangingEventArgs<T> : PropertyChangingEventArgs
    {
        public new T OriginalValue { get { return (T)base.OriginalValue; } protected set { base.OriginalValue = value; } }
        public new T NewValue { get { return (T)base.NewValue; } protected set { base.NewValue = value; } }

        public PropertyChangingEventArgs(string propertyName, T originalValue, T newValue)
            : base(propertyName, originalValue, newValue)
        {
        }
    }
}
