﻿using System;


namespace TechnicalTools.Model
{
    /// <summary>
    /// It is the same meaning than System.Void
    /// (but here this class is accessible, sadly System.Void is not)
    /// </summary>
    [Serializable]
    public class Nothing
    {
        public static Nothing Value { get; } = new Nothing();
        private Nothing() { }
    }
}
