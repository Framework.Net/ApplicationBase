﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;


namespace TechnicalTools.Model.Cache
{
    public static class IdTuple
    {
        public static string Serialize(IIdTuple idTuple, string separator = "|")
        {
            return Serialize(idTuple.Keys.Cast<IConvertible>(), separator);
        }
        public static string Serialize(IEnumerable<IConvertible> keys, string separator = "|")
        {
            string res = string.Empty;
            foreach (var key in keys)
            {
                res += res.Length > 0 ? separator : string.Empty;
                res += key?.ToString(CultureInfo.InvariantCulture);
            }
            return res;
        }
        public static IIdTuple Deserialize(string aggregatedKeys, IReadOnlyCollection<Type> keyTypes, string separator = "|")
        {
            var keyValues = aggregatedKeys.Split(new string[] { separator }, StringSplitOptions.None);
            Debug.Assert(keyValues.Length == keyTypes.Count);
            var keys = new object[keyTypes.Count];
            int i = 0;
            foreach (var keyType in keyTypes)
            {
                var keyValue = keyValues[i];
                if (keyType == typeof(string))
                    keys[i] = keyValue;
                else
                {
                    if (!ParseMethods.TryGetValue(keyType, out MethodInfo mParse))
                    {
                        mParse = keyType.GetMethod(nameof(int.Parse), BindingFlags.Static | BindingFlags.Public, null, new[] { typeof(string), typeof(IFormatProvider) }, null);
                        var newParseMethods = new Dictionary<Type, MethodInfo>(ParseMethods)
                        {
                            { keyType, mParse }
                        };
                        ParseMethods = newParseMethods;
                    }
                    keys[i] = mParse.Invoke(null, new object[] { keyValue, CultureInfo.InvariantCulture });
                }
                ++i;
            }
            var idTupleType = IdTupleTypes[keyTypes.Count].MakeGenericType(keyTypes.ToArray());
            var idTuple = (IIdTuple)idTupleType.GetConstructors().Single().Invoke(keys);
            return idTuple;
        }
        public static readonly IReadOnlyDictionary<int, Type> IdTupleTypes = typeof(IIdTuple).GetAllImplementingTypes()
                                                                                             .Where(t => t.Name.StartsWith(nameof(IdTuple<int>) + "`"))
                                                                                             .ToDictionary(t => t.GetGenericArguments().Length);
        static Dictionary<Type, MethodInfo> ParseMethods = new Dictionary<Type, MethodInfo>();
    }

    public static class TypedId
    {
        public static readonly IReadOnlyCollection<Type> TypesWithInteger = new HashSet<Type>(Type_Extensions.IntegerTypes.Select(t => typeof(TypedId<>).MakeGenericType(typeof(IdTuple<>).MakeGenericType(t))));
    }

    public static class ITypedId_Extensions
    {
        public static string ToSmartString(this ITypedId tid)
        {
            string res = "[";
            if (tid == null)
                res += NullString;
            else
            {
                res += tid.Type == null ? "_ " : tid.Type.ToSmartString();

                if (tid.Key is IIdTuple tuple)
                    foreach (object key in tuple.Keys)
                    {
                        res += ", ";
                        if (key is ITypedId)
                            res += (key as ITypedId).ToSmartString();
                        else if (key == null)
                            res += NullString;
                        else
                            res += key.ToString();
                    }
                else if (tid.Key == null)
                    res += ", " + NullString;
                else
                    res += ", " + tid.Key.ToString();
            }

            return res + "]";
        }

        internal const string NullString = "<NULL>";
    }
}
