﻿using System;
using System.Diagnostics;

using TechnicalTools.Extensions.Data;


namespace TechnicalTools.Model.Cache
{
    // Pour les objets qui n'ont pas d'id propre mais qui implemente une partie d'autre objet
    public interface IHasCompositeTypedIdReadable : IHasTypedIdReadable
    {
        [IsTechnicalProperty(HideFromUser = true)]
        ICompositeTypedId CompositeId { get; set; }
    }
    public interface IHasMutableCompositeTypedId : IHasCompositeTypedIdReadable, IHasMutableTypedId
    {
    }
    public interface IHasMutableTypedId : IHasTypedIdReadable, IHasMutableClosedId
    {
    }


    // Pour rendre le code plus lisible
    public interface ICompositeTypedId : ITypedId<ITypedId>
    {
    }

    [DebuggerDisplay("{" + nameof(AsDebugString) + ",nq}")]
    [Serializable]
    public struct CompositeTypedId<TOwner> : ICompositeTypedId, IEquatable<CompositeTypedId<TOwner>>
        where TOwner : class, IHasMutableTypedId
    {
        public Type Type { get { return _Type; } }
        readonly Type _Type;
        public ITypedId Key { get { return _OwnerKey; } }
        readonly ITypedId _OwnerKey; object ITypedId.Key { get { return Key; } }

        public CompositeTypedId(Type type, ITypedId owner_typed_key)
        {
            _Type = type;
            _OwnerKey = owner_typed_key;
        }

        ITypedId ITypedId.AsOrphan
        {
            get { return new CompositeTypedId<TOwner>(Type, Key.AsOrphan); }
        }

        public string AsDebugString
        {
            get
            {
                return ToString();
            }
        }
        public override string ToString()
        {
            return (Key == null ? ITypedId_Extensions.NullString : Key.ToSmartString()) +
                   "--> " +
                   (Type == null ? ITypedId_Extensions.NullString : Type.ToSmartString());
        }

        #region Equality members

        public bool Equals(ITypedId other)
        {
            if (other == null)
                return false;
            if (_Type != other.Type)
                return false;
            return ReferenceEquals(Key, other.Key)
                   || Key != null && Key.Equals(other.Key);
        }
        public bool Equals(CompositeTypedId<TOwner> other)
        {
            return Equals((ITypedId)other);
        }
        public override bool Equals(object obj)
        {
            return obj != null && Equals((ITypedId)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_OwnerKey.GetHashCode() * 397) ^ _Type.GetHashCode();
            }
        }

        public static bool operator ==(CompositeTypedId<TOwner> left, CompositeTypedId<TOwner> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(CompositeTypedId<TOwner> left, CompositeTypedId<TOwner> right)
        {
            return !left.Equals(right);
        }
        #endregion
    }

}
