﻿using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace TechnicalTools.Model.Cache
{
    public interface ITypedId : IEquatable<ITypedId>
    {
        Type     Type          { get; }
        object   Key           { get; }
        ITypedId AsOrphan      { get; }
        string   AsDebugString { get; }
    }

    public interface ITypedId<out T1> : ITypedId
        where T1 : IEquatable<T1>
    {
        new T1 Key { get; }
    }


    [DebuggerDisplay("{" + nameof(AsDebugString) + ",nq}")]
    [Serializable]
    public struct TypedId<T1> : ITypedId<T1>, IEquatable<TypedId<T1>>
        where T1 : struct, IEquatable<T1>
    {
        public TypedId(Type type, T1 key)
        {
            _Type = type;
            _Key = key;
        }

        public Type Type { get { return _Type; } } readonly Type _Type;
        public T1 Key { get { return _Key; } } readonly T1   _Key; object ITypedId.Key { get { return _Key; } }

        public ITypedId AsOrphan
        {
            get { return new TypedId<T1>(Type, default(T1)); }
        }

        public string AsDebugString { get { return this.ToSmartString(); } }
        public override string ToString() { return this.ToSmartString(); }

        #region Equality members

        public override int GetHashCode()
        {
            unchecked
            {
                return (_Type.GetHashCode() * 397) ^ EqualityComparer<T1>.Default.GetHashCode(_Key);
            }
        }

        public static bool operator ==(TypedId<T1> left, TypedId<T1> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(TypedId<T1> left, TypedId<T1> right)
        {
            return !left.Equals(right);
        }

        public bool Equals(TypedId<T1> other)
        {
            Debug.Assert(other != null);
            return _Type == other._Type && EqualityComparer<T1>.Default.Equals(_Key, other._Key);
        }
        public bool Equals(ITypedId other)
        {
            Debug.Assert(other != null);
            return other is TypedId<T1> && Equals((TypedId<T1>)other);
        }
        public override bool Equals(object obj)
        {
            return !(obj is null) && obj is TypedId<T1> && Equals((TypedId<T1>)obj);
        }

        #endregion
    }
}
