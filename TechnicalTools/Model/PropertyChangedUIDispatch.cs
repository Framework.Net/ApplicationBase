﻿using System;
using System.ComponentModel;
using System.Threading;


namespace TechnicalTools.Model
{
    public class PropertyChangedUIDispatch<T> : INotifyPropertyChanged, IDisposable
        where T : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected PropertyChangedUIDispatch(T item)
        {
            _ctx = SynchronizationContext.Current;
            if (_ctx == null)
                throw new Exception($"Class {GetType().ToSmartString()} cannot be used in this context, because context does not exist!");
            Item = item;
            InstallEventRelay();
        }

        readonly SynchronizationContext _ctx;
        protected T Item { get; }

        // Binding "View.Model": Note that event are from other thread so we need to sync..
        void InstallEventRelay()
        {
            Item.PropertyChanged += Execution_PropertyChanged;
        }

        public void Dispose()
        {
            UninstallEventRelay();
        }
        void UninstallEventRelay()
        {
            Item.PropertyChanged -= Execution_PropertyChanged;
        }
        protected virtual void Execution_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            _ctx.Post(delegate { PropertyChanged?.Invoke(this, e); }, null);
        }
    }
}
