﻿using System;
using System.ComponentModel;


namespace TechnicalTools.Model
{
    public enum eAutomationTrustLevel
    {
        [Description("")]
        Undefined,
        [Description("Automatically Suggested")]
        GuessedByComputer = 1,
        [Description("Edited By User")]
        HumanFilledOrEdited = 2,
    }
}
