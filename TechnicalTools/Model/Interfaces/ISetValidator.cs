﻿using System;
using System.Linq;
using System.Collections.Generic;


namespace TechnicalTools.Model
{
    public interface ISetValidator
    {
        // Return a string indicating if this line _SEEMS_ to be good for selection
        // This method is intended to show user what items could be a potential choice in user interface displaying a lot of items,
        // so this method should be really fast.
        // The true validation will occurs in ValidateItem
        // So, a default valid implementation could be "return null;"
        // let x an item, it is _STRONGLY_ recommended that :
        //    (string.IsNullOrWhitespace(PreValidateItem(x)) ? 0 : 1)
        // <= (string.IsNullOrWhitespace(   ValidateItem(x)) ? 0 : 1)
        string PreValidateItem(object item);

        // Perfom check(s) that need only one item to be able to pass the check
        // Return null if everything is ok about this item, otherwise a string, potentially multiline, explaining why the item is not valid.
        // This method should only check that need only this item to do the check (not comparing to other item)
        // Thus ValidateItem and ValidateSet should return different errors.
        string ValidateItem(object item);

        // Performs check(s) that need at least two items to be able to pass the check.
        // Return null if everything is fine, otherwise a string, potentially multiline, explaining why subset of item are not valid.
        // Thus ValidateItem and ValidateSet should return different errors.
        string ValidateSet(IReadOnlyCollection<object> set, IProgress<string> pr = null);
        bool ValidateSet(IReadOnlyCollection<object> set, out string errorOrWarnings, IProgress<string> pr = null);
    }

    public interface ISetValidator<in TItem> : ISetValidator
    {
        string PreValidateItem(TItem item);
        string ValidateItem(TItem item);
        bool ValidateSet(IReadOnlyCollection<TItem> set, out string explanation, IProgress<string> pr = null);
    }

    public abstract class SetValidator<TItem> : ISetValidator<TItem>
    {
        public virtual string PreValidateItem(TItem item) { return null; }
        public abstract string ValidateItem(TItem item);
        public abstract bool ValidateSet(IReadOnlyCollection<TItem> set, out string explanation, IProgress<string> pr = null);


        string ISetValidator.PreValidateItem(object item) { return PreValidateItem((TItem)item); }
        string ISetValidator.ValidateItem(object item) { return ValidateItem((TItem)item); }
        string ISetValidator.ValidateSet(IReadOnlyCollection<object> set, IProgress<string> pr/* = null*/)                             { return ValidateSet(set.Cast<TItem>().ToList(), out string errorOrWarnings, pr) ? null : errorOrWarnings; }
        bool   ISetValidator.ValidateSet(IReadOnlyCollection<object> set, out string errorOrWarnings, IProgress<string> pr/* = null*/) { return ValidateSet(set.Cast<TItem>().ToList(), out        errorOrWarnings, pr); }

        public static readonly SetValidator<TItem> ValidateAll = new ValidateAnything();
        public static readonly SetValidator<TItem> ValidateNothing = new DontValidate();

        sealed class ValidateAnything : SetValidator<TItem>
        {
            public override string ValidateItem(TItem item) { return null; }
            public override bool ValidateSet(IReadOnlyCollection<TItem> set, out string explanation, IProgress<string> pr = null) { explanation = null; return true; }
        }
        sealed class DontValidate : SetValidator<TItem>
        {
            public override string ValidateItem(TItem item) { return "Cannot be validated"; }
            public override bool ValidateSet(IReadOnlyCollection<TItem> set, out string explanation, IProgress<string> pr = null) { explanation = "Cannot be validated"; return false; }
        }
    }

    public static class ISetValidator_Extensions
    {
        public static Dictionary<TItem, string> ValidateItems<TItem>(ISetValidator<TItem> validator, IReadOnlyCollection<TItem> set)
        {
            return set.Select(item => new { item, Error = validator.ValidateItem(item) })
                      .Where(check => !string.IsNullOrWhiteSpace(check.Error))
                      .ToDictionary(check => check.item, check => check.Error);
        }
        public static string ValidateAll<TItem>(ISetValidator<TItem> validator, IReadOnlyCollection<TItem> set, IProgress<string> pr = null)
        {
            return ValidateAll(validator, set, set, pr);
        }
        public static string ValidateAll<TItem>(ISetValidator<TItem> validator, IReadOnlyCollection<TItem> itemsNotRemoved, IReadOnlyCollection<TItem> allSet, IProgress<string> pr = null)
        {
            string errors = itemsNotRemoved.Select(validator.ValidateItem)
                                            .NotBlank()
                                            .Join(Environment.NewLine);
            if (string.IsNullOrWhiteSpace(errors))
                return validator.ValidateSet(allSet, out errors, pr) ? null : errors;

            if (validator.ValidateSet(allSet, out string setErrors, pr))
                return errors;
            return errors + Environment.NewLine + setErrors;
        }

    }
}
