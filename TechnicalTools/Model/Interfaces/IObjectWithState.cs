﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace TechnicalTools.Model
{
    /// <summary>
    /// Voir <see cref="PropertyNotifierObject">PropertyNotifierObject</see> pour une explication complete
    /// Cette interface sert juste à pouvoir faire des mixins
    /// </summary>
    public interface IObjectWithState : IDirtyableObject
    {
        eState State { get; }

        void InitializeState(eState new_state);
        void UpdateState(eState? new_state = null);
    }

    public interface IObjectWithStateWithNotification : IDirtyableObjectWithNotification
    {
        // Provide the state before the change
        event Action<object, eState> StateChanged;
    }

    public enum eState
    {
        // Even => No Change // Odd => has Pending Changes     [See extension methods below)

        /// <summary>L'objet vient juste d'etre créé. Il n'est pas cohérent et peut contenir des données invalides (non initialisé, nul, ...).
        ///          cet etat peut permettre à certain outils de suivre les objets "vivant" et d'ignorer ceux qui ont été abandonné par leur owner mais qui réside toujours en mémoire </summary>
        [Description("New")]
        Virgin = 0,
                        /// <summary>L'objet courant est nouveau, coherent (techniquement), et la base de données ne possède aucun état de l'objet courant.</summary>
                        [Description("New")]
                        New = 1,
        /// <summary>Synchronisé avec la base de données.</summary>
        [Description("Synchronized with DB")]
        Synchronized = 2,
                        /// <summary>Désynchronisé avec la base de données, certaine des propriétés de l'objet courant peuvent avoir été modifiées</summary>
                        [Description("Unsynchronized with DB")]
                        Unsynchronized = 3,
        // No 4... yet ?
                        /// <summary>Marqué comme a supprimer.</summary>
                        [Description("To Delete in DB")]
                        ToDelete = 5,
        /// <summary>La base de donné ne possède plus d'ancien état de l'objet courant, et l'objet courant n'a pas été utilisé.</summary>
        [Description("Deleted in DB")]
        Deleted = 6
    }
    public static class eState_Extensions
    {
        public static bool HasPendingChanges(this eState state)
        {
            // "Virgin" returns false because a non intialised object is like a non existant thing taking 0 bytes to store. DB already has it ;-)
            return ((int)state & 1) != 0;
        }
    }
}
