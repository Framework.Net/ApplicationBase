﻿using System;


namespace TechnicalTools.Model
{
    public interface IIsNamed
    {
        string Name { get; }
    }
    public class NamedObject : IIsNamed
    {
        public string Name { get; private set; }

        public NamedObject(string name)
        {
            Name = name;
        }
    }
}
