﻿using System;


namespace TechnicalTools.Model
{
    // Designe une relation de __Composition__ au sens UML, en opposition à une relation d'aggregation.
    // L'objet implémentant IHasCompositeOwner depend de son proprietaire et est géré entièrement par lui.
    // Exemple : Si l'objet propriétaire est supprimé par l'utilisateur, alors l'objet implémentant IHasCompositeOwner devrait l'etre aussi.
    public interface IHasCompositeOwner
    {
        object TechnicalOwner { get; set; }
    }
    public interface IHasCompositeOwner<out T> : IHasCompositeOwner
        where T : class
    {
        new T TechnicalOwner { get; }
    }
}
