﻿using System;


namespace TechnicalTools.Model
{
    // Interface qu'implement les objet qui peuvnet maigrir un peu en terme de mémoire
    // Exemple : Les proprietes Lazy accédé en lazy pourrait être vidé.
    public interface IHasReleasableMemory
    {
        void ReleaseMemory(bool unload = false);
    }

    public class ReleaseMemoryEventArgs : EventArgs
    {
        public bool Unload { get; private set; }

        public ReleaseMemoryEventArgs(bool unload)
        {
            Unload = unload;
        }
    }
}
