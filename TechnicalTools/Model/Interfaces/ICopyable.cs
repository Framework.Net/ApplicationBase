﻿using System;


namespace TechnicalTools.Model
{
    public interface ICopyable
    {
        void CopyFrom(ICopyable source);
    }
    public interface ICopyable<in T> : ICopyable
    {
        void CopyFrom(T source);
    }
}
