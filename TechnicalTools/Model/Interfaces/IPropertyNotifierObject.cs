﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace TechnicalTools.Model
{
    /// <summary>
    /// Voir <see cref="PropertyNotifierObject">PropertyNotifierObject</see> pour une explication complete
    /// Cette interface sert juste à pouvoir faire des mixins
    /// </summary>
    public interface IPropertyNotifierObject : INotifyPropertyChanged, IObjectWithState, IObjectWithStateWithNotification
    {
        bool IsReadOnly { get; set; }
        bool IsNew      { get; } // TODO a virer : Car c'est un raccourci
        bool CreatedInCurrentSession     { get; }

        void PropertyChangedFor       <T>(Expression<Func<T, object>> expression, Action onPropertyChange) where T : class, IPropertyNotifierObject;
        void PropertyChangedFor_Remove<T>(Expression<Func<T, object>> expression, Action onPropertyChange) where T : class, IPropertyNotifierObject;

        void PropertyChangingFor       <T>           (Expression<Func<T, object>>    expression, Action<object, PropertyChangingEventArgs>            onPropertyChanging) where T : class, IPropertyNotifierObject;
        void PropertyChangingFor_Remove<T>           (Expression<Func<T, object>>    expression, Action<object, PropertyChangingEventArgs>            onPropertyChanging) where T : class, IPropertyNotifierObject;

        // Les definitions qui suivent sont destiné à etre implementé explicitement car en temps normal leur visibilité devrait etre protected
        // On met donc ces methode dans cette interface uniquement afin de pouvoir faire des Mixin
        void SetProperty<T>(ref T backingStore, T value, Action onChanged, Action<T> onChanging, string propertyName);
        void SetTechnicalProperty<T>(ref T backingStore, T value, Action onChanged, Action<T> onChanging, string propertyName);

        void CurrentPropertyIsDependantOn<T>(Expression<Func<T, object>> dependency_prop_has_expression, Func<bool> refresh_current_property = null, string dependent_propertyName = null) where T : class, IPropertyNotifierObject;

        bool LockState { get; set; }
        ePropertyEventLock LockedEvents { get; set; }

        void CheckIsNotReadOnlyForAction(string action_name = null);
        void LockWhileUpdatingWith(Action action);
        void LockWhileUpdatingWith(ePropertyEventLock lck, Action action);

        event Action<PropertyNotifierObject, ePropertyEventLock> OnResetBindings;
    }
}
