﻿using System.Collections;
using System.Collections.Generic;


namespace TechnicalTools.Model
{
    // Note : Dans un monde parfait ces interfaces seraient uniquement génériques, sans les versions "non génériques"
    //        Les versions non génériques existent car ce sont elles qui doivent être utilisé dans les user-controls.
    //        Les user-controls ne peuvnet en effet pas utiliser lesi nterface générique car ils seraient alors incompatible avec Visual Studio
    // Note : Pour les mots clefs "in" et "out" voir http://msdn.microsoft.com/en-us/library/dd997386.aspx

    // Interface minimaliste permettant de créer une hierarchy
    public interface ITreeNodeWithParentReadable
    {
        ITreeNodeWithParentReadable ParentNode { get; }
    }
    public interface ITreeNodeWithParentReadable<out T> : ITreeNodeWithParentReadable
        where T : ITreeNodeWithParentReadable<T>
    {
        new T ParentNode { get; }
    }


    public interface ITreeNodeWithParentWriteable
    {
        ITreeNodeWithParentWriteable ParentNode { set; }
    }
    public interface ITreeNodeWithParentWriteable<in T> : ITreeNodeWithParentWriteable
        where T :  ITreeNodeWithParentWriteable<T>
    {
        new T ParentNode { set; }
    }


// ReSharper disable PossibleInterfaceMemberAmbiguity
    public interface ITreeNodeWithParent : ITreeNodeWithParentReadable, ITreeNodeWithParentWriteable
// ReSharper restore PossibleInterfaceMemberAmbiguity
    {
    }

// ReSharper disable PossibleInterfaceMemberAmbiguity
    public interface ITreeNodeWithParent<T> : ITreeNodeWithParent, ITreeNodeWithParentReadable<T>, ITreeNodeWithParentWriteable<T>
// ReSharper restore PossibleInterfaceMemberAmbiguity
        where T : ITreeNodeWithParent<T>
    {
    }




    public interface ITreeNodeWithChildrenReadable
    {
        IEnumerable<ITreeNodeWithChildrenReadable> Children { get; }
    }
    public interface ITreeNodeWithChildrenReadable<out T> : ITreeNodeWithChildrenReadable
        where T : ITreeNodeWithChildrenReadable<T>
    {
        new IEnumerable<T> Children { get; }
    }

    public interface ITreeNodeWithChildrenWriteable
    {
        IEnumerable<ITreeNodeWithChildrenReadable> Children { set; }
    }
    public interface ITreeNodeWithChildrenWriteable<in T> : ITreeNodeWithChildrenWriteable
        where T : ITreeNodeWithChildrenWriteable<T>
    {
        new IEnumerable<T> Children { set; }
    }

    // ReSharper disable PossibleInterfaceMemberAmbiguity
    public interface ITreeNodeWithChildren : ITreeNodeWithChildrenReadable, ITreeNodeWithChildrenWriteable
    // ReSharper restore PossibleInterfaceMemberAmbiguity
    {
    }

    // ReSharper disable PossibleInterfaceMemberAmbiguity
    public interface ITreeNodeWithChildren<T> : ITreeNodeWithChildren, ITreeNodeWithChildrenReadable<T>, ITreeNodeWithChildrenWriteable<T>
    // ReSharper restore PossibleInterfaceMemberAmbiguity
        where T : ITreeNodeWithChildren<T>
    {
    }

    public interface ITreeNodeWithChildrenEditable
    {
        IList Children { get; }
    }
    public interface ITreeNodeWithChildrenBindable : ITreeNodeWithChildrenEditable
    {
        new IFixedBindingList<ITreeNodeWithChildrenBindable> Children { get; }
    }

    // TODO : (Experimental) : Créer une bindinglist qui encapsule une autre binding list, et qui permet d'encapsuler le typage fort
    //   (car le parametre de IFixedBindingList n'est pas covariant :( ) et en synchronisant completement les deux
    //  => Avantage : typage bcp plus fort, gestion de dependance entre bindinglist...
    //   Dans le futur mise à jour automatique de cache interdépendant
    // Quand ce sera fait on devrait pouvoir supprimer cette interface
    public interface ITreeNodeWithChildrenBindableWithParent : ITreeNodeWithChildrenEditable, ITreeNodeWithParent
    {
    }



    // Va disparaitre tres tres prochainement
    public interface IBindingTreeNode
    {
        object Id { get; }
        object ParentNodeUntyped { get; }
        IFixedBindingList<IBindingTreeNode> ChildrenUntyped { get; }
        string Name { get; }
    }

}
