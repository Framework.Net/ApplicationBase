﻿using System;

namespace TechnicalTools.Model.Attributes
{
    /// <summary>
    /// A simple attribute to identify assembly dinamycally compield by TechnicalTool.
    /// is there any other way to detect assembly compiled by CSharpCodeProvider ?
    /// </summary>
    public class DynamicCodeCompiledByTechnicalToolsAttribute : Attribute
    {
    }
}
