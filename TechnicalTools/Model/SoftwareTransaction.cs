﻿using System;


namespace TechnicalTools.Model
{
    public class SoftwareTransaction : IDisposable
    {
        public event Action OnCommit;
        public event Action OnRollback;

        public SoftwareTransaction()
        {
            _open = true;
        }
        bool _open;

        public void Commit()
        {
            if (!_open)
                return;
            _open = false;
            if (OnCommit != null)
                OnCommit.Invoke();
        }

        public void Rollback()
        {
            if (!_open)
                return;
            _open = false;
            if (OnRollback != null)
                OnRollback.Invoke();
        }

        public virtual void Dispose()
        {
            Rollback();
        }
    }

}
