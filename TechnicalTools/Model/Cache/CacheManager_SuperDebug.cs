﻿using System;
using System.Diagnostics;

using TechnicalTools.Logs;


#if SUPER_DEBUG
using System.Reflection;
using System.Collections.Generic;
#endif

namespace TechnicalTools.Model.Cache
{

    // Inspiré de ObjectTracker
    public partial class CacheManager
    {
#if SUPER_DEBUG
        public StackTrace GetCreationStack<TObject>(object id)
            where TObject : class, IHasTypedId, ILoadableById
        {
            CacheListUI<TObject> lst = GetCounterByTypeOrCreate<TObject>();
            Type type = typeof(TObject);
            var key = id is int ? new TypedId<int>(type, (int)id) : ((IIdTuple)id).ToTypedId(type);
            return lst.GetCreationStack(key);
        }

        public StackTrace GetCreationStack(ITypedId id)
        {
            MethodInfo gmi = _genericGetCreationStackMethod.MakeGenericMethod(id.Type);
            return (StackTrace)gmi.Invoke(this, new[] { id.Key });
        }
        static readonly MethodInfo _genericGetCreationStackMethod = typeof(CacheManager).GetMethod("GetCreationStack", BindingFlags.Instance | BindingFlags.Public, null, new[] { typeof(object) }, null);

        public partial interface ICacheListReadOnly
        {
            StackTrace GetCreationStack(ITypedId id);
        }
#endif
        partial class CacheListUI<T>
            where T : class, IHasMutableTypedId
        {
            #if SUPER_DEBUG
            readonly Dictionary<ITypedId, StackTrace> _instancesCreationStack = new Dictionary<ITypedId, StackTrace>();

            public StackTrace GetCreationStack(ITypedId id)
            {
                StackTrace st;
                lock (this)
                    _instancesCreationStack.TryGetValue(id, out st);
                return st;
            }
            #endif

            [Conditional("SUPER_DEBUG")]
            void DebugOnAdd(ITypedId id, T obj)
            {
                #if SUPER_DEBUG
                _instancesCreationStack[id] = new StackTrace(1, true);
                #else
                /*Nothing*/
                #endif
            }
            [Conditional("SUPER_DEBUG")]
            void DebugOnRemove(ITypedId id)
            {
                #if SUPER_DEBUG
                _instancesCreationStack.Remove(id);
                #else
                /*Nothing*/
                #endif
            }
            [Conditional("SUPER_DEBUG")]
            void DebugOnClean()
            {
                #if SUPER_DEBUG
                lock (this)
                    _instancesCreationStack.RemoveAll(pair => _instances.ContainsKey(pair.Key));
                #else
                /*Nothing*/
                #endif
            }
        }

        [Conditional("SUPER_DEBUG")]
        static void Log(string str)
        {
            _log.Info(str);
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(CacheManager));
    }


}

