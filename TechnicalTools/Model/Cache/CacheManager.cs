﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;

using TechnicalTools.Tools;


namespace TechnicalTools.Model.Cache
{
    // TODO : Voir : https://github.com/ironyx/sharpmemorycache/blob/master/SharpMemoryCache/TrimmingMemoryCache.cs
    //         via : http://stackoverflow.com/a/22900208/1942901
    // Inspiré de ObjectTracker
    public partial class CacheManager //: IMessageFilter necessite une reference vers System.Windows.Forms
    {
        public static CacheManager Instance { get { return CurrentInstance.Value; } private set { CurrentInstance.Value = value; } }

        public static readonly ThreadLocal<CacheManager>  CurrentInstance = new ThreadLocal<CacheManager>(() => GlobalInstance);


        static readonly CacheManager GlobalInstance = new CacheManager(null, true);


        CacheManager(CacheManager outerManager, bool manageAllRequests)
        {
            _outerManager = outerManager;
            _manageAllRequests = manageAllRequests;
            if (_outerManager == null)
                InitAutoClean();
        }
        readonly CacheManager _outerManager;
        readonly bool _manageAllRequests;

        [Obsolete("Me prévenir avant d'utiliser ca ! Ca peut aider à certaines choses mais en casser d'autre dans certain cas (synchro inter poste)", true)]
        public static void DoWithTemporaryCache(Action action, bool manageAllRequests = true)
        {
            var instanceBefore = Instance;
            var newCache = new CacheManager(instanceBefore, manageAllRequests);
            Instance = newCache;
            try
            {
                action();
            }
            finally
            {
                Instance = instanceBefore;
            }
        }

        /// <summary>
        /// Retourne la liste de tous les objets encore vivants.
        /// Note @Dev : GetAllAliveObjects retourne un tableau car cela force la copie de tous les éléments.
        ///             On evite de fournir un moyen a l'appelant de generer des effet de bord sur le lock (blocage) en fonction de la vitesse à laquelle il itere
        /// En fait ce probleme est résolue de la même maniere que quand le framework déclenche un event :
        /// les subscribers (handler) sont copiés dans une liste temporaire puis on itère dessus pour les executer.
        /// </summary>
        public List<object> GetObjectsInMemory()
        {
            return AllAliveObjectsEnumerator().ToList();
        }


        public List<TObject> GetObjectsInMemory<TObject>()
            where TObject : class, IHasMutableTypedId, ILoadableById
        {
            CacheListUI<TObject> lst = GetCounterByTypeOrCreate<TObject>();
            lock (lst)
                return lst.GetInstances();
        }

        /// <summary>
        /// ATTENTION ! Enumerateur privée seulement. Ne pas passer en public.
        /// La valeur enumerable renvoyée doit _TOUJOURS_ être completement parcouru avant qu'une methode public ne retourne.
        /// On peut donc appeler ToList ou ToArray avant de retourner.
        /// </summary>
        /// <returns>Un énumerateur privé qui ne doit jamais être retourné en dehors de la classe</returns>
        IEnumerable<object> AllAliveObjectsEnumerator()
        {
            lock (_typesMet)
                foreach (var lst in _typesMet.Values)
                    foreach (var obj in lst.GetInstances())
                        yield return obj;
        }

        #region Get an object by its ID

        [DebuggerHidden, DebuggerStepThrough]
        public TObject Get<TObject>(int id, bool preventLoading = false)
            where TObject : class, IHasMutableTypedId, ILoadableById
        {
            return _Get<TObject>(id, preventLoading);
        }
        [DebuggerHidden, DebuggerStepThrough]
        public TObject Get<TObject>(ulong id, bool preventLoading = false)
            where TObject : class, IHasMutableTypedId, ILoadableById
        {
            return _Get<TObject>(id, preventLoading);
        }
        [DebuggerHidden, DebuggerStepThrough]
        public TObject Get<TObject>(long id, bool preventLoading = false)
            where TObject : class, IHasMutableTypedId, ILoadableById
        {
            return _Get<TObject>(id, preventLoading);
        }
        public void TryGetAndDo<TObject>(int id, Action<TObject> action)
            where TObject : class, IHasMutableTypedId, ILoadableById
        {
            var obj = _Get<TObject>(id, true);
            if (obj != null)
                action(obj);
        }

        public TObject Get<TObject>(IIdTuple id, bool preventLoading = false)
            where TObject : class, IHasMutableTypedId, ILoadableById
        {
            return _Get<TObject>(id, preventLoading);
        }

        public void TryGetAndDo<TObject>(IIdTuple id, Action<TObject> action)
            where TObject : class, IHasMutableTypedId, ILoadableById
        {
            var obj = _Get<TObject>(id, true);
            if (obj != null)
                action(obj);
        }

       // [DebuggerHidden, DebuggerStepThrough]
        public object Get(ITypedId id, bool preventLoading = false)
        {
            // BIG TODO : a explorer et etudier (c'est peut etre moins de code mais des perf pourri ) http://stackoverflow.com/questions/2898925/casting-an-object-to-two-interfaces-at-the-same-time-to-call-a-generic-method
            Debug.Assert(id.Type.IsClass, "Type of typed id must be a class");
            Debug.Assert(typeof(IHasMutableClosedId).IsAssignableFrom(id.Type), "Type of key must be IHasClosedId");
            Debug.Assert(typeof(ILoadableById).IsAssignableFrom(id.Type), "Type of key must be ILoadableById");

            MethodInfo gmi;
            if (!_genericGets.TryGetValue(id.Type, out gmi))
            {
                gmi = _genericGetMethod.MakeGenericMethod(id.Type);
                _genericGets.Add(id.Type, gmi);
            }
            return gmi.Invoke(this, new[] { id.Key, preventLoading });
        }
        static readonly MethodInfo _genericGetMethod = typeof(CacheManager).GetMethod("_Get", BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { typeof(object), typeof(bool) }, null);
        static readonly Dictionary<Type, MethodInfo> _genericGets = new Dictionary<Type, MethodInfo>();

        // id is always int or an IIdTuple, nothing else
        private TObject _Get<TObject>(object id, bool preventLoading)
            where TObject : class, IHasMutableTypedId, ILoadableById
        {
            if (!_manageAllRequests && _outerManager != null)
            {
                var res = _outerManager._Get<TObject>(id, true);
                if (res != null)
                    return res;
            }
            CacheListUI<TObject> lst = GetCounterByTypeOrCreate<TObject>();

            Type type = typeof(TObject);
            TObject obj;
            var key = id is int  ? new TypedId<int>(type, (int)id) 
                    //: id is long ? new TypedId<long>(type, (long)id) 
                    :              ((IIdTuple)id).ToTypedId(type);

            lock (lst)
            {
                if (lst.TryGetValue(key, out obj))
                {
                    Log(string.Format("Asking for {0} with id {1} ==> Found", typeof(TObject).Name, id));
                    return obj;
                }
                if (preventLoading)
                {
                    Log(string.Format("Asking for {0} with id {1} ==> Not Found, return null.", typeof(TObject).Name, id));
                    return null;
                }

                Log(string.Format("Asking for {0} with id {1} ==> Not Found, creating it...", typeof(TObject).Name, id));
                var constructor = DefaultObjectFactory.ConstructorFor(type);
                Debug.Assert(constructor != null, "Pas de constructeur par defaut pour le type " + type.Name);
                obj = (TObject)constructor();

                obj.LoadById(id);
                lst.Add(key, obj);
                Debug.Assert(ReferenceEquals(obj, _Get<TObject>(id, true)), "After loading, cache should get the object loaded !");
            }
            Debug.Assert(id.Equals(obj.Id), "Le chargement de l'objet aurait du mettre son Id à jour !");
            return obj;
        }

        #endregion

        #region GetAll

        [DebuggerHidden, DebuggerStepThrough]
        public List<TObject> GetAll<TObject>(IEnumerable<int> ids, bool preventLoading = false)
            where TObject : class, IHasMutableTypedId, ILoadableById
        {
            CacheListUI<TObject> lst = GetCounterByTypeOrCreate<TObject>();
            lock (lst)
                return ids.Select(id => _Get<TObject>(id, preventLoading)).ToList();
        }

        [DebuggerHidden, DebuggerStepThrough]
        public List<TObject> GetAll<TObject>(IEnumerable<IIdTuple> ids, bool preventLoading = false)
            where TObject : class, IHasMutableTypedId, ILoadableById
        {
            CacheListUI<TObject> lst = GetCounterByTypeOrCreate<TObject>();
            lock (lst)
                return ids.Select(id => _Get<TObject>(id, preventLoading)).ToList();
        }

        #endregion GetAll

        #region Get Composite object

        [DebuggerHidden, DebuggerStepThrough]
        public TCompositeObject GetComposite<TCompositeObject, TOwner>(int owner_id, object owner = null, bool preventLoading = false)
            where TCompositeObject : class, IHasCompositeOwner, IHasMutableCompositeTypedId, ILoadableByCompositeId
            where TOwner : class, IHasMutableTypedId
        {
            Type type = typeof(TCompositeObject);

            CacheListUI<TCompositeObject> lst = GetCounterByTypeOrCreate<TCompositeObject>();

            TCompositeObject obj;
            var key = new CompositeTypedId<TOwner>(type, new TypedId<int>(typeof(TOwner), owner_id));

            lock (lst)
            {
                if (lst.TryGetValue(key, out obj))
                {
                    Log(string.Format("Asking for {0} with id {1}{2} ==> Found", typeof(TCompositeObject).Name, typeof(TOwner).Name, owner_id));
                    return obj;
                }
                if (preventLoading)
                {
                    Log(string.Format("Asking for {0} with id {1}{2} ==> Not Found, return null.", typeof(TCompositeObject).Name, typeof(TOwner).Name, owner_id));
                    return null;
                }

                Log(string.Format("Asking for {0} with id {1}{2} ==> Not Found, creating it...", typeof(TCompositeObject).Name, typeof(TOwner).Name, owner_id));
                var constructor = type.GetConstructor(new[] { typeof(TypedId<int>) });
                Debug.Assert(constructor != null, "Pas de constructeur par defaut pour le type " + type.Name);
                obj = (TCompositeObject)constructor.Invoke(new object[] { key.Key });
                if (owner != null)
                    obj.TechnicalOwner = owner;
                obj.LoadById(key);
                lst.Add(key, obj); // Seulement après que le owner a été assigné, on ajoute le owner au cache
                Debug.Assert(ReferenceEquals(obj, GetComposite<TCompositeObject, TOwner>(owner_id, null, true)), "Owner of composite object must be obj");
            }
            Debug.Assert(key.Equals(obj.Id), "Le chargement de l'objet aurait du mettre son Id à jour !");
            return obj;
        }

        [DebuggerHidden, DebuggerStepThrough]
        public object GetComposite(ICompositeTypedId id, object owner = null, bool preventLoading = false)
        {
            Debug.Assert(id.Type.IsClass, "Type of key must be a class");
            Debug.Assert(typeof(IHasCompositeOwner).IsAssignableFrom(id.Type), "Type of key must be IHasCompositeOwner");
            Debug.Assert(typeof(IHasMutableCompositeTypedId).IsAssignableFrom(id.Type), "Type of key must be IHasCompositeTypedId");
            Debug.Assert(typeof(ILoadableByCompositeId).IsAssignableFrom(id.Type), "Type of key must be ILoadableByCompositeId");

            MethodInfo gmi;
            var key = id.AsOrphan;
            if (!_genericGetComposites.TryGetValue(key, out gmi))
            {
                gmi = _genericGetCompositeMethod.MakeGenericMethod(new[] { id.Type, id.Key.Type });
                _genericGetComposites.Add(key, gmi);
            }
            return gmi.Invoke(this, new[] { id.Key.Key, owner, preventLoading });
        }
        static readonly MethodInfo _genericGetCompositeMethod = typeof(CacheManager).GetMethod("GetComposite", new[] { typeof(int), typeof(object), typeof(bool) });
        static readonly Dictionary<ITypedId, MethodInfo> _genericGetComposites = new Dictionary<ITypedId, MethodInfo>();

        #endregion

        // Si l'application est parfaitement coherente, cette methode devrait toujours renvoyer true.
        // Si elle renvoie et un objet non null et different de obj alors utilisez plutot AddIfNotAlreadyInside dans CacheManager_DuplicateHandling
        [DebuggerHidden, DebuggerStepThrough]
        public object Add(IHasMutableTypedId obj)
        {
            var id = obj.TypedId;
            Debug.Assert(obj != null, "obj is not null");
            Debug.Assert(id.Type.IsInstanceOfType(obj), "obj must be the type of the key");
            Type type = id.Type;

            var lst = GetCounterByTypeOrCreate(type);
            return lst.AddIfNotAlreadyInside(id, obj);
        }




        public IEnumerable<ICacheListReadOnly> Counters
        {
            [DebuggerHidden, DebuggerStepThrough]
            get
            {
                lock (_typesMet)
                    return _typesMet.Values.ToList();
            }
        }

        #region Private

        ICacheList GetCounterByTypeOrCreate(Type type)
        {
            Debug.Assert(type.IsClass, "type of key must be a class");

            ICacheList cnt;
            lock (_typesMet)
            {
                if (!_typesMet.TryGetValue(type, out cnt))
                {
                    var gType = typeof(CacheListUI<>).MakeGenericType(new[] { type });
                    var cons = DefaultObjectFactory.ConstructorFor(gType);
                    cnt = (ICacheList)cons();
                    _typesMet.Add(type, cnt);
                }
            }
            return cnt;
        }

        CacheListUI<T> GetCounterByTypeOrCreate<T>()
            where T : class, IHasMutableTypedId
        {
            Debug.Assert(!typeof(T).IsGenericType && !typeof(T).IsAbstract);
            var type = typeof(T);
            ICacheList cnt;
            lock (_typesMet)
            {
                if (!_typesMet.TryGetValue(type, out cnt))
                {
                    cnt = new CacheListUI<T>();
                    _typesMet.Add(type, cnt);
                }
            }
            return (CacheListUI<T>)cnt;
        }
        static readonly Dictionary<Type, ICacheList> _typesMet = new Dictionary<Type, ICacheList>();

        public partial interface ICacheListReadOnly
        {
            Type Type  { get; }
            uint Count { get; }
            IList GetInstances();
        }

        public partial interface ICacheList : ICacheListReadOnly
        {
            object AddIfNotAlreadyInside(ITypedId key, object obj);
        }
        #endregion Private

        partial class CacheListUI<T> : ICacheList
            where T : class, IHasMutableTypedId
        {
            public Type Type
            {
                get { return typeof(T); }
            }

            public uint Count
            {
                get { return (uint)GetInstances().Count; }
            }

            public bool TryGetValue(ITypedId id, out T obj)
            {
                Debug.Assert(id != null, "id != null");
                lock (this)
                {
                    WeakReference wr;
                    if (_instances.TryGetValue(id, out wr))
                    {
                        obj = (T)wr.Target;
                        if (obj == null)
                        {
                            Log("Previous object with uid " + id.AsDebugString + " has been garbage collected (sooner) !");
                            _instances.Remove(id);
                            DebugOnRemove(id);
                        }
                        return obj != null;
                    }
                }
                obj = null;
                return false;
            }

            [DebuggerHidden, DebuggerStepThrough]
            public object AddIfNotAlreadyInside(ITypedId id, object obj)
            {
                return Add(id, (T)obj);
            }

            //[DebuggerHidden, DebuggerStepThrough]
            public object Add(ITypedId id, T obj)
            {
                Debug.Assert(obj != null, "obj != null");
                lock (this)
                {
                    WeakReference wr;

                    if (_instances.TryGetValue(id, out wr))
                    {
                        object present = wr.Target;
                        if (present != null)
                        {
                            Debug.Assert(ReferenceEquals(present, obj),
                                         string.Format("Du code essaye de charger un objet {0} or un objet similaire existe déjà dans le cache !" + Environment.NewLine +
                                                       "Remontez la callstack et modifiez le code pour demander au cache cet objet plutôt que d'en charger un nouveau", id.AsDebugString));
                            if (!ReferenceEquals(present, obj))
                            {
                                Log("Collision for " + id.AsDebugString);
                                return present;
                            }
                            return obj;
                        }
                        Log("Adding again " + id.AsDebugString + " (this object have already been loaded but also garbage collected sooner, optimisation is maybe possible) !");
                        _instances.Remove(id);
                        DebugOnRemove(id);
                    }
                    else
                        Log("Adding for the first time " + id.AsDebugString);

                    wr = new WeakReference(obj);
                    _instances.Add(id, wr);
                    if (_cacheHolders.Count > 0)
                        _instancesHeld.Add(id, obj);
                    DebugOnAdd(id, obj);
                    obj.IdHasChanged += IdOfManagedObjectHasChanged;

                    //if (_instances.Capacity == _instances.Count)
                    //    Clean();
                }
                return null;
            }

            [DebuggerHidden, DebuggerStepThrough]
            void IdOfManagedObjectHasChanged(object sender, PropertyHasChangedEventArgs e)
            {
                var obj = (T)sender;
                lock (this)
                {
                    var oldTypedId = e.PreviousValue is ITypedId ? (ITypedId)e.PreviousValue : obj.MakeTypedId((IIdTuple)e.PreviousValue);
                    Debug.Assert(_instances.ContainsKey(oldTypedId), "_instances.ContainsKey(oldTypedId)");
                    Debug.Assert(!e.PreviousValue.Equals(obj.Id), "L'Id n'as pas reelement changé : Soit il est inutile que l'event soit declenché, soit l'id retourné n'est pas le nouvel ID :(");
                    Log(string.Format("Replacing {0} by {1}", oldTypedId.AsDebugString, obj.TypedId.AsDebugString));
                    _instances.Remove(oldTypedId);
                    if (_cacheHolders.Count > 0)
                        _instancesHeld.Remove(oldTypedId);
                    DebugOnRemove(oldTypedId);
                    obj.IdHasChanged -= IdOfManagedObjectHasChanged;
                    if (obj.Id != null)
                        Add(obj.TypedId, obj);
                }
            }

            /// <summary>
            /// Retourne la liste de tous les objets encore vivants.
            /// Note @Dev : GetInstances retourne un tableau car cela force la copie de tous les éléments.
            ///             On evite de fournir un moyen a l'appelant de generer des effet de bord sur le lock (blocage) en fonction de la vitesse a laquelle il itere
            /// En fait ce probleme est résolue de la même maniere que quand le framework déclenche un event :
            /// les subscribers (handler) sont copiés dans une liste temporaire puis on itère dessus pour les executer.
            /// </summary>
            /// <returns></returns>
            public List<T> GetInstances()
            {
                lock (this)
                    return GetEnumerator().ToList();
            }
            IList ICacheListReadOnly.GetInstances() { return GetInstances(); }

            #region Private

            readonly Dictionary<ITypedId, WeakReference> _instances = new Dictionary<ITypedId, WeakReference>();


            public void Clean()
            {
                lock (this)
                {
                    _instances.RemoveAll(pair => !pair.Value.IsAlive);
                }
                DebugOnClean();
            }

            /// <summary>
            /// ATTENTION ! Enumerateur privée seulement. Ne pas passer en public.
            /// La valeur enumerable renvoyée doit _TOUJOURS_ être completement parcouru avant qu'une methode public ne retourne.
            /// On peut donc appeler ToList ou ToArray avant de retourner.
            /// </summary>
            /// <returns>Un énumerateur privé qui ne doit jamais être retourné en dehors de la classe</returns>
            IEnumerable<T> GetEnumerator()
            {
                // Debug.Assert(_lock must be locked !)
                return _instances.Values.Select(value => (T)value.Target).Where(obj => obj != null);
            }

            #endregion Private


        }

        #region InitAutoClean

        void InitAutoClean()
        {
            // TODO : http://www.codeproject.com/Articles/30345/Application-Idle
            //System.Windows.Forms.Application.Idle += ApplicationOnIdle;
            //System.Windows.Forms.Application.AddMessageFilter(this);
            //System.Windows.Forms.Application.Idle -= ApplicationOnIdle;
            //System.Windows.Forms.Application.RemoveMessageFilter(this);
        }
        //bool IMessageFilter.PreFilterMessage(ref Message m)
        //{
        //    //if (m.HWnd == ownerWindow && interestedMessages.ContainsKey(m.Msg))
        //    //{
        //    //    MessageReceived(m.Msg);
        //    //}
        //    return true;
        //}
        //void ApplicationOnIdle(object sender, EventArgs eventArgs)
        //{

        //}

        #endregion

    }
}

