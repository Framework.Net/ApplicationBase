﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace TechnicalTools.Model
{
    public class DictionaryWithMissEvent<TKey, TValue> : IDictionary<TKey, TValue>, IReadOnlyDictionary<TKey, TValue>
    {
        public bool GenerateEventForTryGetValue { get; set; }

        public event MissingKeyEventHandler MissingKey;
        public delegate void MissingKeyEventHandler(object sender, MissingKeyEventArgs<TKey> e);

        public DictionaryWithMissEvent()
            : this(0)
        {
        }
        public DictionaryWithMissEvent(int capacity)
        {
            _dic = new Dictionary<TKey, TValue>(capacity);
        }
        readonly Dictionary<TKey, TValue> _dic;

        public TValue this[TKey key]
        {
            get
            {
                if (_dic.TryGetValue(key, out TValue value))
                    return value;
                var e = new MissingKeyEventArgs<TKey>(key);
                MissingKey?.Invoke(this, e);
                return _dic[key]; // Let dictionary throw if nothing changed
            }
            set
            {
                _dic[key] = value;
            }
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            if (_dic.TryGetValue(key, out value))
                return true;
            if (!GenerateEventForTryGetValue)
                return false;
            var e = new MissingKeyEventArgs<TKey>(key);
            MissingKey?.Invoke(this, e);
            return _dic.TryGetValue(key, out value);
        }

        #region Other regular members for IDictionary<TKey, TValue> and inherited interfaces

        public ICollection<TKey>   Keys                                { get { return _dic.Keys; } }
        public ICollection<TValue> Values                              { get { return _dic.Values; } }

        public int  Count                                              { get { return _dic.Count; } }
        public void Add(TKey key, TValue value)                        {              _dic.Add(key, value); }
        public void Clear()                                            {              _dic.Clear(); }
        public bool ContainsKey(TKey key)                              {       return _dic.ContainsKey(key); }
        public bool Remove(TKey key)                                   {       return _dic.Remove(key); }
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {       return _dic.GetEnumerator(); }

        IEnumerator                      IEnumerable.GetEnumerator()                                              {       return                             ((IEnumerable)_dic).GetEnumerator(); }
        bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly                                                   { get { return ((ICollection<KeyValuePair<TKey, TValue>>)_dic).IsReadOnly; } }
        void ICollection<KeyValuePair<TKey, TValue>>.Add     (KeyValuePair<TKey, TValue> item)                    {              ((ICollection<KeyValuePair<TKey, TValue>>)_dic).Add(item); }
        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)                    {       return ((ICollection<KeyValuePair<TKey, TValue>>)_dic).Contains(item); }
        void ICollection<KeyValuePair<TKey, TValue>>.CopyTo  (KeyValuePair<TKey, TValue>[] array, int arrayIndex) {              ((ICollection<KeyValuePair<TKey, TValue>>)_dic).CopyTo(array, arrayIndex); }
        bool ICollection<KeyValuePair<TKey, TValue>>.Remove  (KeyValuePair<TKey, TValue> item)                    {       return ((ICollection<KeyValuePair<TKey, TValue>>)_dic).Remove(item); }

        #endregion

        #region IReadOnlyDictionary<TKey, TValue>

        IEnumerable<TKey>   IReadOnlyDictionary<TKey, TValue>.Keys   { get { return ((IReadOnlyDictionary<TKey, TValue>)_dic).Keys; } }
        IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values { get { return ((IReadOnlyDictionary<TKey, TValue>)_dic).Values ; } }

        #endregion
    }

    public class MissingKeyEventArgs<TKey> : EventArgs
    {
        public TKey Key { get; }

        public MissingKeyEventArgs(TKey key) { Key = key; }
    }
}
