﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TechnicalTools.Logs
{
    public class Level : IComparable<Level>, IComparable
    {
        public string Name   { get; }
        public int    Value  { get; private set; }
        public bool IsCommon { get; private set; }
        public bool IsLimit  { get; private set; }

        private Level(string name, int value)
        {
            Name = name;
            Value = value;
        }

        public override string ToString()
        {
            return Name;
        }
        public int CompareTo(Level other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            if (obj is Level lvl)
                return CompareTo(lvl);
            return Value.CompareTo(obj);
        }

        // Values are borrowed to log4net

        internal static readonly Level All = new Level("All", int.MinValue) { IsLimit = true };

        internal static readonly Level Verbose = new Level("Verbose", 10000);

        internal static readonly Level Finest = new Level("Finest", 10000);
        internal static readonly Level Trace = new Level("Trace", 20000);
        internal static readonly Level Finer = new Level("Finer", 20000);
        internal static readonly Level Fine = new Level("Fine", 30000);
        public   static readonly Level Debug = new Level("Debug", 30000) { IsCommon  = true };

        public   static readonly Level Info = new Level("Info", 40000) { IsCommon = true };
        public   static readonly Level Notice = new Level("Notice", 50000);

        public   static readonly Level Warn = new Level("Warn", 60000) { IsCommon = true };
        public   static readonly Level Error = new Level("Error", 70000) { IsCommon = true };
        internal static readonly Level Severe = new Level("Severe", 80000);
        internal static readonly Level Critical = new Level("Critical", 90000);

        internal static readonly Level Alert = new Level("Alert", 100000);
        public   static readonly Level Fatal = new Level("Fatal", 110000) { IsCommon = true };
        internal static readonly Level Emergency = new Level("Emergency", 120000);

        internal static readonly Level Off = new Level("Off", int.MaxValue) { IsLimit = true };

        public static readonly List<Level> AllLevels = new List<Level>()
        {
            Verbose,

            Finest,
            Trace,
            Finer,
            Fine,
            Debug,


            Info,
            Notice,

            Warn,
            Error,
            Severe,
            Critical,


            Alert,
            Fatal,
            Emergency,
        };

        public static readonly ILookup<int, Level> ByValue = AllLevels.ToLookup(lvl => lvl.Value);
        public static Level GetByValue(int value)
        {
            return ByValue[value].OrderBy(lvl => !lvl.IsCommon).FirstOrDefault();
        }


    }
}
