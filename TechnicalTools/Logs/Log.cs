﻿using System;
using System.Globalization;

using TechnicalTools.Annotations.Design;


namespace TechnicalTools.Logs
{
    public class Log : ILog
    {
        public DateTime TimestampUTC   { get; set; }
        public Level    Level          { get; set; }
        public int      ThreadId       { get; set; }
        public int?     ParentThreadId { get; set; }
        public string   Hostname       { get; set; }
        public string   Type           { get; set; }
        public string   Message        { get; set; }
        [NotNull]
        public string   Tags           { get; set; }

#pragma warning disable 809
        [Obsolete("Use ToFormatedMessage", true)]
        public override string ToString()
        {
            return this.ToFormatedMessage();
        }
#pragma warning restore 809

    }
}
