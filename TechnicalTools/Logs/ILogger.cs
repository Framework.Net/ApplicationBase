﻿using System;
using System.Runtime.CompilerServices;

namespace TechnicalTools.Logs
{
    public interface ILogger
    {
        string Name { get; }

        void Audit(string message, Exception ex = null, params string[] tags);
        void Audit(string message, params string[] tags);

        void Debug(string message, Exception ex = null, params string[] tags);
        void Debug(string message,                      params string[] tags);
        void Info (string message, Exception ex = null, params string[] tags);
        void Info (string message,                      params string[] tags);
        void Warn (string message, Exception ex = null, params string[] tags);
        void Warn (string message,                      params string[] tags);
        void Error(string message, Exception ex = null, params string[] tags);
        void Error(                Exception ex       , params string[] tags);
        void Error(string message,                      params string[] tags);
        void Fatal(string message, Exception ex = null, params string[] tags);
        void Fatal(string message,                      params string[] tags);
    }

    /// <summary>
    /// Extensions to be used in code "catch(Exception ex) when (...)" to log fluently
    /// </summary>
    public static class ILogger_Extensions
    {
        public static bool ErrorAndReturnFalse(this ILogger log, string message, Exception ex = null) { log.Error(message, ex); return false; }
        public static bool ErrorAndReturnFalse(this ILogger log,                 Exception ex)        { log.Error(         ex); return false; }
        public static bool FatalAndReturnFalse(this ILogger log, string message, Exception ex = null) { log.Fatal(message, ex); return false; }

        public static void LogErrorThroughException(this ILogger log, Action action, [CallerMemberName] string methodName = "")
        {
            LogErrorThroughException(log, "in method " + methodName, action);
        }
        public static void LogErrorThroughException(this ILogger log, string message, Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex) when (log.ErrorAndReturnFalse(message, ex))
            {
                throw; // Never reached
            }
        }
        public static void LogFatalThroughException(this ILogger log, string message, Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex) when (log.FatalAndReturnFalse(message, ex))
            {
                throw; // Never reached
            }
        }
    }

}
