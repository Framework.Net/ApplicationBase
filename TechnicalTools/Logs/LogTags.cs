﻿using System;


namespace TechnicalTools.Logs
{
    public static class LogTags
    {
        // For exception Type
        public static string DummySilenced          { get; } = "Dummy Silenced";
        public static string BusinessCheck          { get; } = "Business Check";
        public static string TechnicalError         { get; } = "Technical Error";
        public static string DetectedBug            { get; } = "Detected Bug";
        public static string PotentialDetectedBug   { get; } = "Potential Detected Bug";
        public static string UnknownBug             { get; } = "Unknown Bug";

        // Other
        public static string Ignored                { get; } = "Ignored";
    }
}

