CREATE FUNCTION [Security].[FlattenizedEnvironmentSettingsEnriched](@envId int, @firstKey as varchar(890), @isAdmin bit, @isReadOnly bit)
RETURNS TABLE
AS
RETURN
(
    -- This will throw exception if calling user has no right on FlattenizedEnvironmentSettings.
	-- GetFlattenizedEnvironmentSettings is useful for autologin feature in developper environement
	-- @firstKey must be returned first
	select top 1000000 FES.Environment_Id, FES.FullKey, FES.[Description],
			case 
			   when FullKey = @firstKey and @isAdmin = 0 and @isReadOnly = 1 then
						[clrTools].[RegEx_Replace] (N'[pP][aA][sS][sS][wW][oO][rR][dD] *= *(''([^'']|'''')*''|"([^"]|"")*"|[^;''"]([^; ]| +[^;])*) *;?',
													'Password=''readonly_user_password'';',
						[clrTools].[RegEx_Replace] (N'[uU][sS][eE][rR] +[iI][dD] *= *(''([^'']|'''')*''|"([^"]|"")*"|[^;''"]([^; ]| +[^;])*) *;?',
													'User ID=''app_user_readonly'';',
													Value))
			   when E.Domain = 1  -- 1 = Prod, because the current view is declared on a prod environement, important when user is redirected to another env
			    and FullKey = @firstKey and @isAdmin = 1  then
						[clrTools].[RegEx_Replace] (N'[pP][aA][sS][sS][wW][oO][rR][dD] *= *(''([^'']|'''')*''|"([^"]|"")*"|[^;''"]([^; ]| +[^;])*) *;?',
													'Password=''YourProdPassword'';',
						[clrTools].[RegEx_Replace] (N'[uU][sS][eE][rR] +[iI][dD] *= *(''([^'']|'''')*''|"([^"]|"")*"|[^;''"]([^; ]| +[^;])*) *;?',
													'User ID=''app_admin_prod_login'';',
													Value))
			   when E.Domain <> 1
			    and FullKey = @firstKey and @isAdmin = 1  then
						[clrTools].[RegEx_Replace] (N'[pP][aA][sS][sS][wW][oO][rR][dD] *= *(''([^'']|'''')*''|"([^"]|"")*"|[^;''"]([^; ]| +[^;])*) *;?',
													'Password=''NonProdPassword'';',
						[clrTools].[RegEx_Replace] (N'[uU][sS][eE][rR] +[iI][dD] *= *(''([^'']|'''')*''|"([^"]|"")*"|[^;''"]([^; ]| +[^;])*) *;?',
													'User ID=''app_admin_dev_login'';',
													Value))
			   else value
			end as Value
	from Deployment.FlattenizedEnvironmentSettings(@envId) as FES
	inner join Deployment.Environments as E on E.Id = FES.Environment_Id
	order by case when FullKey = @firstKey then 0 else 1 end
	)

GO


