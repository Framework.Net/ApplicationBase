CREATE TABLE [Deployment].[EnvironmentSettings]
(
	[Environment_Id] [int] NOT NULL,
	[FullKey] [varchar](890) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK__TMT_EnvironmentSettings] PRIMARY KEY CLUSTERED 
(
	[Environment_Id] ASC,
	[FullKey] ASC
)
)

GO

ALTER TABLE [Deployment].[EnvironmentSettings]  WITH CHECK ADD  CONSTRAINT [FK__TMT_EnvironmentSettings__TMT_Environments__Environment_Id] FOREIGN KEY([Environment_Id])
REFERENCES [Deployment].[Environments] ([Id])
GO

ALTER TABLE [Deployment].[EnvironmentSettings] CHECK CONSTRAINT [FK__TMT_EnvironmentSettings__TMT_Environments__Environment_Id]
GO
