CREATE TABLE [Deployment].[Files]
(
	[Hash] [char](32) NOT NULL,
	[FileName] [nvarchar](256) NOT NULL,
	[Content] [varbinary](max) NOT NULL,
 CONSTRAINT [PK__TMT_Files__Hash] PRIMARY KEY CLUSTERED 
(
	[Hash] ASC
)
)
GO
