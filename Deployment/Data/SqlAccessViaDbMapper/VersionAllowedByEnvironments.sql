CREATE TABLE [Deployment].[VersionAllowedByEnvironments]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Environment_Id] [int] NOT NULL,
	[Version_From] [nvarchar](50) NULL,
	[Version_To] [nvarchar](50) NULL,
	[EffectiveFromDate] [datetime2](7) NULL,
	[EffectiveToDate] [datetime2](7) NULL,
 CONSTRAINT [PK__TMT_VersionAllowedByEnvironments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)

GO

ALTER TABLE [Deployment].[VersionAllowedByEnvironments]  WITH CHECK ADD  CONSTRAINT [FK__TMT_VersionAllowedByEnvironments__TMT_Environments__Environment_Id] FOREIGN KEY([Environment_Id])
REFERENCES [Deployment].[Environments] ([Id])
GO

ALTER TABLE [Deployment].[VersionAllowedByEnvironments] CHECK CONSTRAINT [FK__TMT_VersionAllowedByEnvironments__TMT_Environments__Environment_Id]
GO


