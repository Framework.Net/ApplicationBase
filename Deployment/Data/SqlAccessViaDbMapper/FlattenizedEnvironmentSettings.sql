CREATE FUNCTION [Deployment].[FlattenizedEnvironmentSettings](@envId int)
RETURNS TABLE
AS
RETURN
(
	WITH rec(Lvl, Id, InheritedEnvironment_Id) AS
	(
			SELECT 0, E.Id, E.InheritedEnvironment_Id FROM Deployment.Environments E 
			WHERE E.Id = @envId
	   UNION ALL
			SELECT Lvl + 1, E.Id, E.InheritedEnvironment_Id FROM Deployment.Environments E
			inner join rec on rec.InheritedEnvironment_Id = E.Id 
	),
	Flatten AS
	(
		SELECT S.*, rec.InheritedEnvironment_Id, rec.Lvl, ROW_NUMBER() OVER(PARTITION BY FullKey ORDER BY rec.Lvl ASC) as 'Priority'
		FROM rec
		inner join Deployment.EnvironmentSettings S on S.Environment_Id = rec.Id
	)	
	select Environment_Id, FullKey, Value, [Description]
	from Flatten
	where Priority = 1 
	--order by Lvl, FullKey

	/* TEST
create table TestEnv
(
EnvId int not null,
ParentEnvId int null,
Name nvarchar(255) not null
)

create table TestSetting
(
Id int not null,
EnvId int not null,
Name nvarchar(255) not null,
Value nvarchar(255) null
)

insert into TestEnv(EnvId, ParentEnvId, Name)
values 
(5, 1, 'dervived5'),
(1, null, 'base'),
(2, 1, 'dervived'),
(3, 2, 'dervived again'),
(4, null, 'other')

select * from TestSetting
insert into TestSetting (EnvId, Id, Name, Value)
values 
(1, 1, 'A', 'A'),
(1, 2, 'B', 'B'),
(1, 3, 'C', 'C'),
(1, 4, 'D', null),
(1, 5, 'E', null),

(2, 6, 'B', 'b'),
(2, 7, 'C', null),
(2, 8, 'E', 'e'),
(2, 9, 'F', 'f'),

(3, 10, 'B', '_'),
(3, 12, 'A', null),
(3, 13, 'G', 'gg'),

(4, 14, 'X', 'X'),
(5, 15, 'Y', 'Y')

	*/
 )




GO


