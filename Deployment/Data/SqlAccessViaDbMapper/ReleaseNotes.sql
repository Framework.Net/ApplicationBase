CREATE TABLE [Deployment].[ReleaseNotes]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Version] [nvarchar](50) NOT NULL,
	[IsPublic] [bit] NOT NULL,
	[Note] [nvarchar](max) NOT NULL,
	[NoteTechnical] [nvarchar](max) NULL,
	[ExplainingGIF] [varbinary](max) NULL,
	[HasGIF] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)
GO


