CREATE TABLE [Deployment].[ReleasedFiles]
(
	[Year] [tinyint] NOT NULL,
	[Month] [tinyint] NOT NULL,
	[Day] [tinyint] NOT NULL,
	[Hour] [tinyint] NOT NULL,
	[Hash] [char](32) NOT NULL,
	[LocalPath] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK__TMT_ReleasedFiles] PRIMARY KEY CLUSTERED 
(
	[Year] ASC,
	[Month] ASC,
	[Day] ASC,
	[Hour] ASC,
	[Hash] ASC,
	[LocalPath] ASC
)
)

GO

ALTER TABLE [Deployment].[ReleasedFiles]  WITH CHECK ADD  CONSTRAINT [FK__TMT_ReleasedFiles__TMT_Files__Hash] FOREIGN KEY([Hash])
REFERENCES [Deployment].[Files] ([Hash])
GO

ALTER TABLE [Deployment].[ReleasedFiles] CHECK CONSTRAINT [FK__TMT_ReleasedFiles__TMT_Files__Hash]
GO

