﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model;

using DataMapper;
using DataMapper.Tools;


namespace ApplicationBase.Deployment.Data
{
    public class DefaultDataAccessor : DbMapperWrapper, IDataAccessor
    {
        public DefaultDataAccessor(Config cfg)
            : this(DbMapperFactory.CreateSqlMapper(new NamedObject(cfg.ApplicationName + " deployment connection"), cfg.InitialConnectionString))
        {

        }
        public DefaultDataAccessor(IDbMapper mapper) : base(mapper)
        {
        }

        public virtual bool IsAuthenticationRequired { get { return false; } }
        public virtual bool IsAuthenticated          { get; set; }

        public virtual List<EnvironmentSetting> Authenticate(EnvironmentConfig env, string login, string password)
        { throw new NotSupportedException($"{nameof(IDataAccessor)}.{nameof(Authenticate)} must be implemented in class \"{GetType().Name}\"!"); }

        T                IDataAccessor.CreateInstance<T>()                                                          { return Activator.CreateInstance<T>(); }
        void             IDataAccessor.CreateInDatabase(IDeploymentData item)                                       { CreateInDatabase(item, false); }
        void             IDataAccessor.CreateInDatabaseCollection<T>(IReadOnlyCollection<T> items)                  { CreateInDatabaseCollection(items, false); }
        long             IDataAccessor.GetFileLength(ReleasedFile file)                                             { return DefaultDataAccessor_ForSql.GetFileLength(this, file); }
        IEnumerable<int> IDataAccessor.DownloadFileByChunk(ReleasedFile file, byte[] uniqueChunk, long totalLength) { return DefaultDataAccessor_ForSql.DownloadFileByChunk(this, file, uniqueChunk, totalLength); }

        int IDataAccessor.UpdateToDatabase<T>(T item) 
        { return base.UpdateToDatabase(item, true); }

        void IDataAccessor.DeleteInDatabase<T>(T item)
        { base.DeleteInDatabase(item, true); }

        List<T> IDataAccessor.LoadCollection<T>() 
        { return base.LoadCollection<T>(); }

        List<Version> IDataAccessor.GetAvailableVersions()
        {
            var mtable = GetTableMapping(typeof(ReleasedFile));
            var versionFields = GetColumnNameFor((ReleasedFile rf) => rf.Year) + ", "
                              + GetColumnNameFor((ReleasedFile rf) => rf.Month) + ", "
                              + GetColumnNameFor((ReleasedFile rf) => rf.Day) + ", "
                              + GetColumnNameFor((ReleasedFile rf) => rf.Hour);
            var sqlQuery = "select " + versionFields
                         + " from " + mtable.FullNameProtected
                         + " group by " + versionFields;
            var dt = base.GetDataTable(sqlQuery);
            var res = dt.AsEnumerable()
                        .Select(row => new Version((byte)row[0], (byte)row[1], (byte)row[2], (byte)row[3]))
                        .ToList();
            return res;
        }


        List<ReleasedFile> IDataAccessor.LoadReleasedFilesFor(Version v)
        {
            var sqlFilter = GetColumnNameFor((ReleasedFile rf) => rf.Year) + " = " + DbMappedField.ToSql(v.Major)
                          + " and " + GetColumnNameFor((ReleasedFile rf) => rf.Month) + " = " + DbMappedField.ToSql(v.Minor)
                          + " and " + GetColumnNameFor((ReleasedFile rf) => rf.Day) + " = " + DbMappedField.ToSql(v.Build)
                          + " and " + GetColumnNameFor((ReleasedFile rf) => rf.Hour) + " = " + DbMappedField.ToSql(v.Revision);
            return base.LoadCollection<ReleasedFile>(sqlFilter);
        }

        List<ReleasedFile> IDataAccessor.LoadRecentReleasedFilesWithPathStartingWith(string pathPrefix)
        {
            var mtable = GetTableMapping(typeof(ReleasedFile));
            List<ReleasedFile> res;
            int top = 0;
            do
            {
                top += 3;
                var sqlQuery = "SELECT top " + top + " " + mtable.AllFields.Select(f => f.ColumnNameProtected).Join()
                             + " from " + mtable.FullNameProtected
                             + " where left(" + GetColumnNameFor((ReleasedFile rf) => rf.LocalPath) + ", 2) = " + DbMappedField.ToSql(pathPrefix)
                             + " order by "
                             + GetColumnNameFor((ReleasedFile rf) => rf.Year) + " desc,"
                             + GetColumnNameFor((ReleasedFile rf) => rf.Month) + " desc,"
                             + GetColumnNameFor((ReleasedFile rf) => rf.Day) + " desc,"
                             + GetColumnNameFor((ReleasedFile rf) => rf.Hour) + " desc";
                res = base.EnumerateCollectionFromSqlQuery<ReleasedFile>(sqlQuery).ToList();
            } while (res.Count < top || 
                     res.First().Version == res.Last().Version);
            return res.Where(rf => rf.Version == res.First().Version).ToList();
        }

        List<ReleaseNoteWithContent> IDataAccessor.LoadAllReleaseNotes(bool withTechnicalOne)
        {
            var sqlFilter = GetColumnNameFor((ReleaseNoteWithContent rnwc) => rnwc.IsPublic) + " = " + DbMappedField.ToSql(withTechnicalOne);
            return base.LoadCollection<ReleaseNoteWithContent>(sqlFilter);
        }
        List<ReleaseNoteWithContent> IDataAccessor.LoadLastReleaseNote(bool withTechnicalOne)
        {
            var mtable = base.GetTableMapping(typeof(ReleaseNoteWithContent));
            var query = "select top 1 " + mtable.GetSqlFieldNames(mtable.AllFields) 
                      + " from " + mtable.FullNameProtected
                      + (withTechnicalOne ? "" : " where " + GetColumnNameFor((ReleaseNoteWithContent rnwc) => rnwc.IsPublic) + " = 1")
                      + " order by " + GetColumnNameFor((ReleaseNoteWithContent rnwc) => rnwc.Id) + " desc";
            return base.EnumerateCollectionFromSqlQuery<ReleaseNoteWithContent>(query).ToList();
        }
        List<ReleaseNoteWithContent> IDataAccessor.LoadReleaseNotesAfter(bool withTechnicalOne, Version byVersion, int? byId)
        {
            if (byVersion != null)
            {
                var rn = base.LoadCollection<ReleaseNote>(GetColumnNameFor((ReleaseNoteWithContent rnwc) => rnwc.Version) + " = " + DbMappedField.ToSql(byVersion.ToString())).SingleOrDefault();
                if (rn == null) // a developper probably remove the release :/ (bad but possible)
                {
                    // dont want to bother with comparing version in database cause sql sucks at this
                    // so we return all
                    return base.LoadCollection<ReleaseNoteWithContent>();
                }
                byId = byId.HasValue 
                     ? Math.Max(byId.Value, rn.Id) 
                     : rn.Id;
            }
            var mtable = base.GetTableMapping(typeof(ReleaseNoteWithContent));
            Debug.Assert(byVersion != null || byId.HasValue);
            var conditions = new[]
            {
                withTechnicalOne ? null : GetColumnNameFor((ReleaseNoteWithContent rnwc) => rnwc.IsPublic) + " = 1",
                byId == null ? null : GetColumnNameFor((ReleaseNoteWithContent rnwc) => rnwc.Id) + " > " + byId.Value.ToStringInvariant(),
            }.NotNull().Join(" and ");
            Debug.Assert(conditions.Length > 0);
            var query = "select top 1 " + mtable.GetSqlFieldNames(mtable.AllFields)
                      + " from " + mtable.FullNameProtected
                      + " where " + conditions
                      + " order by " + GetColumnNameFor((ReleaseNoteWithContent rnwc) => rnwc.Id) + " desc";
            return base.EnumerateCollectionFromSqlQuery<ReleaseNoteWithContent>(query).ToList();
        }

    }

    /// <summary>
    /// Help you to implement IDataAccessor if you are :
    /// - using a IDbMapper to connect to a your database 
    /// - Your database is Sql Server
    /// - You keep the default mapping of classes and properties
    /// </summary>
    public static class DefaultDataAccessor_ForSql
    {
        public static long GetFileLength(IDbMapper mapper, ReleasedFile file)
        {
            using (SqlConnection conn = SqlConnectionTracer.Create(mapper.GetConnectionString()))
            {
                conn.OpenOrWaitUntilOpen();

                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "Select datalength(" + mapper.GetColumnNameFor((FileWithContent f) => f.Content) + ")"
                                      + " from " + mapper.GetTableMapping(typeof(FileWithContent)).FullName
                                      + " where " + mapper.GetColumnNameFor((FileWithContent f) => f.Hash) + " = " + DbMappedField.ToSql(file.Hash);
                    return (int)(long)cmd.ExecuteScalar();
                }
            }
        }
        public static IEnumerable<int> DownloadFileByChunk(IDbMapper mapper, ReleasedFile file, byte[] uniqueChunk, long total)
        {
            Debug.Assert(uniqueChunk.LongLength <= int.MaxValue);
            using (SqlConnection conn = SqlConnectionTracer.Create(mapper.GetConnectionString()))
            {
                conn.OpenOrWaitUntilOpen();

                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "Select " + mapper.GetColumnNameFor((FileWithContent f) => f.Content)
                                    + " from " + mapper.GetTableMapping(typeof(FileWithContent)).FullName
                                    + " where " + mapper.GetColumnNameFor((FileWithContent f) => f.Hash) + " = " + DbMappedField.ToSql(file.Hash);

                    using (SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.SequentialAccess))
                        if (reader.Read())
                        {
                            var stream = reader.GetStream(0);
                            int cumulatedRead = 0;
                            while (true)
                            {
                                int read = stream.Read(uniqueChunk, 0, uniqueChunk.Length);
                                if (read == 0)
                                    break;
                                cumulatedRead += read;
                                yield return read;
                            }
                        }
                }
            }
        }
    }
}
