CREATE TABLE [Deployment].[Environments]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Domain] [tinyint] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[InheritedEnvironment_Id] [int] NULL,
	[IsHiddenFromUser] [bit] NOT NULL CONSTRAINT [DF_Environments_IsHiddenFromUser]  DEFAULT ((1)),
 CONSTRAINT [PK__TMT_Environments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
)

GO

ALTER TABLE [Deployment].[Environments]  WITH CHECK ADD  CONSTRAINT [FK__TMT_Environments__TMT_Environments__InheritedEnvironment_Id] FOREIGN KEY([InheritedEnvironment_Id])
REFERENCES [Deployment].[Environments] ([Id])
GO

ALTER TABLE [Deployment].[Environments] CHECK CONSTRAINT [FK__TMT_Environments__TMT_Environments__InheritedEnvironment_Id]
GO

